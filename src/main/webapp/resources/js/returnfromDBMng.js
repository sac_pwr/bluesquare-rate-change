var returnFromDeliveryboyListOld=[];
var totalDamageQuantity=0;
var totalReceivingQuantity=0;
$(document).ready(function(){
	
	$('#receivingDone').click(function(){
		
		var updateOrderProductList="";
		for(var j=0; j<returnFromDeliveryboyListOld.length; j++)
		{
			var orderProduct=returnFromDeliveryboyListOld[j];
			
			if(orderProduct.damageQuantity>0 && (orderProduct.damageQuantityReason=='' || orderProduct.damageQuantityReason==undefined)){
				Materialize.toast('Enter '+orderProduct.product.productName+' damage quantiy reason', '2000', 'teal lighten-2');
				return false;
			}
			
			updateOrderProductList =updateOrderProductList + 
									orderProduct.returnFromDeliveryBoyId+","+
									orderProduct.damageQuantity+","+
									orderProduct.nonDamageQuantity+","+
									orderProduct.damageQuantityReason+"-";
		}
		updateOrderProductList=updateOrderProductList.slice(0,-1);
		$('#updateOrderProductListId').val(updateOrderProductList);		
		
	});
	
		//here get list of product	
		$.ajax({
			  type: 'POST',
			  url: myContextPath+'/fetchReturnFromDeliveryBoyAjax?returnFromDeliveryBoyMainId='+orderId,
			  dataType : "json",
			}).done(function(data) {
				//returnFromDeliveryboyListOld=data;
				for(var i=0; i<data.length; i++)
				{
					var returnFromDeliveryboy=data[i];
					
					returnFromDeliveryboy.nonDamageQuantity=parseInt(
							parseInt(returnFromDeliveryboy.issuedQuantity)
							-parseInt(returnFromDeliveryboy.deliveryQuantity)
							);
					
					returnFromDeliveryboyListOld.push(returnFromDeliveryboy);
					
					$('#damageQtyFree_'+returnFromDeliveryboy.product.product.productId).unbind().on("keyup keypress",function(event){
						
						var key = event.which;						    
					    if( ! ( key >= 48 && key <= 57 || key === 13) )
					    	event.preventDefault();
						
						var id=$(this).attr('id');
						var productId=id.split('_')[1];
						
						var returnFromDeliveryboyListTemp=[];
						
						for(var i=0; i<returnFromDeliveryboyListOld.length; i++)
						{
							if(returnFromDeliveryboyListOld[i].product.product.productId==productId && returnFromDeliveryboyListOld[i].type==="Free")
							{
								returnFromDeliveryboy=returnFromDeliveryboyListOld[i];
							}
							else
							{
								returnFromDeliveryboyListTemp.push(returnFromDeliveryboyListOld[i]);
							}
						}
						
						
						var qty=$(this).val();
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						
						var receivingQuantity=parseInt(
								parseInt(returnFromDeliveryboy.issuedQuantity)
								-parseInt(returnFromDeliveryboy.deliveryQuantity)
								);
						if(receivingQuantity<qty)
						{
							Materialize.toast('Damage quantiy must equal or less than return quantiy', '2000', 'teal lighten-2');
							$(this).val(0);
							$('#recievingQtyFree_'+productId).text(receivingQuantity-parseInt(0));
							
							returnFromDeliveryboy.nonDamageQuantity=$('#recievingQtyFree_'+productId).text();
							returnFromDeliveryboy.damageQuantity=0;
							returnFromDeliveryboyListTemp.push(returnFromDeliveryboy);
							returnFromDeliveryboyListOld=returnFromDeliveryboyListTemp;
							calculattTotal();
							return false;
						}
						
						$('#recievingQtyFree_'+productId).text(receivingQuantity-parseInt(qty));
						
						returnFromDeliveryboy.damageQuantityReason=$('#damageQtyFreeReason_'+productId).text();
						returnFromDeliveryboy.nonDamageQuantity=$('#recievingQtyFree_'+productId).text();
						returnFromDeliveryboy.damageQuantity=qty;
						returnFromDeliveryboyListTemp.push(returnFromDeliveryboy);
						returnFromDeliveryboyListOld=returnFromDeliveryboyListTemp;
						calculattTotal();
					});
					
					$('#damageQtyFreeReason_'+returnFromDeliveryboy.product.product.productId).blur(function(){
						
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						
						var returnFromDeliveryboyListTemp=[];
						
						var returnFromDeliveryboyListTemp=[];
						for(var i=0; i<returnFromDeliveryboyListOld.length; i++)
						{
							if(returnFromDeliveryboyListOld[i].product.product.productId==productId && returnFromDeliveryboyListOld[i].type==="NonFree")
							{
								returnFromDeliveryboy=returnFromDeliveryboyListOld[i];
							}
							else
							{
								returnFromDeliveryboyListTemp.push(returnFromDeliveryboyListOld[i]);
							}
						}
						
						returnFromDeliveryboy.damageQuantityReason=$('#damageQtyFreeReason_'+productId).val();
						returnFromDeliveryboyListTemp.push(returnFromDeliveryboy);
						returnFromDeliveryboyListOld=returnFromDeliveryboyListTemp;
					});
					
					$('#damageQtyNonFree_'+returnFromDeliveryboy.product.product.productId).unbind().on("keyup keypress",function(){
						
						var key = event.which;						    
					    if( ! ( key >= 48 && key <= 57 || key === 13) )
					        event.preventDefault();
						
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						
						var returnFromDeliveryboyListTemp=[];
						
						for(var i=0; i<returnFromDeliveryboyListOld.length; i++)
						{
							if(returnFromDeliveryboyListOld[i].product.product.productId==productId && returnFromDeliveryboyListOld[i].type==="NonFree")
							{
								returnFromDeliveryboy=returnFromDeliveryboyListOld[i];
							}
							else
							{
								returnFromDeliveryboyListTemp.push(returnFromDeliveryboyListOld[i]);
							}
						}
						
						var qty=$(this).val();
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						
						var receivingQuantity=parseInt(
								parseInt(returnFromDeliveryboy.issuedQuantity)
								-parseInt(returnFromDeliveryboy.deliveryQuantity)
								);
						if(receivingQuantity<qty)
						{
							Materialize.toast('Damage quantiy must equal or less than return quantiy', '2000', 'teal lighten-2');
							$(this).val(0);
							$('#recievingQtyNonFree_'+productId).text(receivingQuantity-parseInt(0));
							
							returnFromDeliveryboy.nonDamageQuantity=$('#recievingQtyNonFree_'+productId).text();
							returnFromDeliveryboy.damageQuantity=0;
							returnFromDeliveryboyListTemp.push(returnFromDeliveryboy);
							returnFromDeliveryboyListOld=returnFromDeliveryboyListTemp;
							calculattTotal();
							return false;
						}
						
						$('#recievingQtyNonFree_'+productId).text(receivingQuantity-parseInt(qty));
						
						returnFromDeliveryboy.damageQuantityReason=$('#damageQtyNonFreeReason_'+productId).text();
						returnFromDeliveryboy.nonDamageQuantity=$('#recievingQtyNonFree_'+productId).text();
						returnFromDeliveryboy.damageQuantity=qty;
						returnFromDeliveryboyListTemp.push(returnFromDeliveryboy);
						returnFromDeliveryboyListOld=returnFromDeliveryboyListTemp;
						calculattTotal();
					});
					
					$('#damageQtyNonFreeReason_'+returnFromDeliveryboy.product.product.productId).blur(function(){
												
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						
						var returnFromDeliveryboyListTemp=[];
						
						var returnFromDeliveryboyListTemp=[];
						for(var i=0; i<returnFromDeliveryboyListOld.length; i++)
						{
							if(returnFromDeliveryboyListOld[i].product.product.productId==productId && returnFromDeliveryboyListOld[i].type==="NonFree")
							{
								returnFromDeliveryboy=returnFromDeliveryboyListOld[i];
							}
							else
							{
								returnFromDeliveryboyListTemp.push(returnFromDeliveryboyListOld[i]);
							}
						}
						
						returnFromDeliveryboy.damageQuantityReason=$('#damageQtyNonFreeReason_'+productId).val();
						returnFromDeliveryboyListTemp.push(returnFromDeliveryboy);
						returnFromDeliveryboyListOld=returnFromDeliveryboyListTemp;
					});
				}	
				
			}).fail(function(xmlHttpRequest, statusText, errorThrown) {
			  alert(
			    'Something Went Wrong...\n\n'
			      + 'XML Http Request: ' + JSON.stringify(xmlHttpRequest)
			      + ',\nStatus Text: ' + statusText
			      + ',\nError Thrown: ' + errorThrown);
			});
	
});

function calculattTotal()
{
	totalDamageQuantity=0;
	totalReceivingQuantity=0;
	for(var i=0; i<returnFromDeliveryboyListOld.length; i++)
	{
		totalDamageQuantity+=parseInt(returnFromDeliveryboyListOld[i].damageQuantity);
		totalReceivingQuantity+=parseInt(returnFromDeliveryboyListOld[i].nonDamageQuantity);
	}
	$('#totalDamagedQuantity').text(totalDamageQuantity);
	$('#totalReceivedQuantity').text(totalReceivingQuantity);
}