const productCart=[];
var freeNonFreeRdo="NonFree";
var payTypeRdo="Cash";
var paymentRdo="InstantPay";
var isPrintBill=false;
var payButtonClick;
$(document).ready(function() {

	/**
	 * only number allowed without decimal 
	 */
	$('#mobileNo').keypress(function( event ){
		var key = event.which;						    
		if( ! ( key >= 48 && key <= 57 || key === 13) )
			event.preventDefault();
	});

	//free nonFree radio button
	$('#freeId').click(function(){
		freeNonFreeRdo="Free";
	});

	//free nonFree radio button
	$('#nonFreeId').click(function(){
		freeNonFreeRdo="NonFree";
	});

	$('.paymentCls').click(function(){
		var id=$(this).attr('id');
		if(id=="cash"){
			payTypeRdo="Cash";
		}else{
			payTypeRdo="Cheque";
		}
	});

	$('.payTypesCls').click(function(){
		var id=$(this).attr('id');
		if(id=="instantPay"){
			paymentRdo="InstantPay";
		}else if(id=="credit"){
			paymentRdo="Credit";
		}else{
			paymentRdo="PartialPay";
		}
	});

	/**
	 * add product record to table
	 */
	$('#addCartButtonId').click(function(){
		
		var productId=$('#productId').val();
		if(productId=="0"){
			Materialize.toast('Select Product', '3000', 'teal lighten-2');
			return false;
		}

		var qty=$('#quantityId').val();	
		if(qty=="0" || qty==undefined || qty==''){
			Materialize.toast('Enter Quantity', '3000', 'teal lighten-2');
			return false;
		}

		for(var i=0; i<productCart.length; i++){
			var product=productCart[i][0];
			var type=productCart[i][4];
			if(product.productId==productId && freeNonFreeRdo==type){
				Materialize.toast('Product Already added', '3000', 'teal lighten-2');
				return false;
			}
		}

		var product=fetchProductByProductId(productId);			
		var mrp=Math.round(product.rate+((product.rate*product.categories.igst)/100));
		var total=parseInt(qty)*parseFloat(mrp);
		var type=freeNonFreeRdo;
		if(type=="Free"){
			total=0;
			mrp=0;
		}
		
		var availableQty=qtyAvailable(productId);

		//check product current qty exceeds with entered qty
		if(parseInt(qty)>parseInt(availableQty)){
			Materialize.toast('Qty Must be Below or equal Current Quantity. Max : '+availableQty, '3000', 'teal lighten-2');
			return false;
		}
		
		//add record to productCart[]
		productCart.push([product,qty,mrp,total,freeNonFreeRdo]);
		
		//appends table row using productCart[] 
		refreshTable();

		//find final total Amount
		findTotal();

		//reset product
		resetProduct();
	});
	
	$('#clearButtonId').click(function(){
		productCart.splice(0, productCart.length);
		refreshTable();
	});
	
	$('#amountPartial').keyup(function(){
		var amountPartial=$('#amountPartial').val();
		var balAmountPartial=$('#balAmountPartial').val();
		
		if(amountPartial=='' || amountPartial==undefined){
			amountPartial=0;
		}

		var finalTotal=0;
		for(var i=0; i<productCart.length; i++){
			finalTotal+=parseFloat(productCart[i][3]);
		}

		if(parseFloat(finalTotal)<parseFloat(amountPartial)){
			Materialize.toast('Paid amount must be less than Total Amount : '+finalTotal , '3000', 'teal lighten-2');
			$('#amountPartial').val('');
			$('#balAmountPartial').val(finalTotal);
			return false;
		}else if(parseFloat(finalTotal)==parseFloat(amountPartial)){
			Materialize.toast('Please Enter Partial Amount only <br> For Full Payment Select Instant Pay' , '3000', 'teal lighten-2');
			$('#amountPartial').val('');
			$('#balAmountPartial').val(finalTotal);
			return false;
		}

		$('#balAmountPartial').val(finalTotal-parseFloat(amountPartial));
	});

	$('#payButtonId').click(function(){

		$('.preloader-background').show();
		$('.preloader-wrapper').show();

		if(verifySubmit()){
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();
			var businessListId=$('#businessListId').val();
			if(businessListId==0){
				payButtonClick=true;
				$("#custInfo").modal('open');
				return false;
			}
			isPrintBill=false;
			var formData=$("#saveOrderWithBusines").serialize()
			$('#payButtonId').prop('disabled', true);
			$('#payAndPrintButtonId').prop('disabled', true);
			submitCounterOrder(formData);
		}	
		
		$('.preloader-background').hide();
		$('.preloader-wrapper').hide();	
	});
	$('#payAndPrintButtonId').click(function(){

		$('.preloader-background').show();
		$('.preloader-wrapper').show();

		if(verifySubmit()){
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();

			isPrintBill=true;

			var businessListId=$('#businessListId').val();
			if(businessListId==0){
				payButtonClick=false;
				$("#custInfo").modal('open');
				return false;
			}
			var formData=$("#saveOrderWithBusines").serialize();
			$('#payButtonId').prop('disabled', true);
			$('#payAndPrintButtonId').prop('disabled', true);
			submitCounterOrder(formData);
		}	
		$('.preloader-background').hide();
		$('.preloader-wrapper').hide();
	});
	
	$('#submitCustomerInfo').click(function(){
		$('.preloader-background').show();
		$('.preloader-wrapper').show();

		var custName=$('#custName').val();
		var mobileNo=$('#mobileNo').val();
		var gstNo=$('#gstNo').val();

		if(custName=='' || custName==undefined){
			Materialize.toast('Enter Customer Name' , '3000', 'teal lighten-2');
			return false;
		}

		if(mobileNo=='' || mobileNo==undefined){
			Materialize.toast('Enter Mobile Number' , '3000', 'teal lighten-2');
			return false;
		}
		if(mobileNo.length!=10){
			Materialize.toast('Enter Correct Mobile Number' , '3000', 'teal lighten-2');
			return false;
		}
		if(gstNo.length!=15 && gstNo.length>0){
				Materialize.toast('Enter Correct Gst In Number' , '3000', 'teal lighten-2');
				return false;
		}
		
		if(verifySubmit()){
			$('.preloader-background').hide();
			$('.preloader-wrapper').hide();

			$('#payButtonId').prop('disabled', true);
			$('#payAndPrintButtonId').prop('disabled', true);
			$('#submitCustomerInfo').prop('disabled', true);

			var formData=$("#saveCustomerInfoWithOrder").serialize()
			submitCounterOrder(formData);
		}
		
		$('.preloader-background').hide();
		$('.preloader-wrapper').hide();
	});

	
	//$('.selectRadioButton').click();
});

/**
 * verify when click on pay,payAndPrint,CustInfoSubmit
 */
function verifySubmit(){

		if(productCart.length==0){
			Materialize.toast('Add Product In order', '3000', 'teal lighten-2');
			return false;
		}

		var finalTotal=0;
		var productListData="";
		var nonFreeFound=false;
		
		//qty verify with current invt start
		var productListWithQty=new HashMap();
		for(var i=0; i<productCart.length; i++){

			if(productCart[i][1].purchaseQuantity==0){
				Materialize.toast(productCart[i][0].productName+' Quantity must be non zero ', '3000', 'teal lighten-2');
				return false;
			}

			/* For External use */
			finalTotal+=parseFloat(productCart[i][3]);
			productListData+=productCart[i][0].productId+","+productCart[i][1]+","+productCart[i][2]+","+productCart[i][3]+","+productCart[i][4]+"-";
			/* End */

			var product=productCart[i][0];
			var qty=productCart[i][1];	
			var mrp=productCart[i][2];
			var total=productCart[i][3];
			var type=productCart[i][4];

			if(productListWithQty.containsKey(product.productId)){				
				var entry=productListWithQty.get(product.productId);
				productListWithQty.remove(product.productId);
				productListWithQty.put(product.productId,parseInt((entry.value)[0])+parseInt(qty));
				
			}else{				
				productListWithQty.put(product.productId,parseInt(qty));
			}

			if(productCart[i][4]=="NonFree"){
				nonFreeFound=true;
			}
		}

		if(nonFreeFound==false){
			Materialize.toast('Minimum One Non-Free Product Required For Order', '3000', 'teal lighten-2');
			return false;
		}

		var entrySet=productListWithQty.getAll()
		
		for(var i=0; i<entrySet.length; i++){
			var key=entrySet[i].key;
			var val=entrySet[i].value;
			var product=fetchProductByProductId(key);

			if(product.currentQuantity<val){
				Materialize.toast(product.productName+ ' qty exceeds Current Qty. Max Available :  '+ product.currentQuantity, '3000', 'teal lighten-2');
				return false;
			}

		} 
		//qty verify with current invt end

		//remove last '-'
		productListData=productListData.substring(0,productListData.length-1);

		var amountPartial=$('#amountPartial').val();
		if(amountPartial=="" || amountPartial==undefined){
			amountPartial=0;
		}
		var balAmountPartial=$('#balAmountPartial').val();
		var dueDate=$('#dueDate').val();
		var dueDateCredit=$('#dueDateCredit').val();
		var bankName=$('#bankName').val();
		var chqNo=$('#chqNo').val();
		var chequeDate=$('#chequeDate').val();

		if(paymentRdo=="PartialPay"){

			if(amountPartial==0){
				Materialize.toast('Please Enter Partial Amount ' , '3000', 'teal lighten-2');
				return false;
			}

			if(amountPartial>=finalTotal){
				Materialize.toast('Please Enter Partial Amount only <br> For Full Payment Select Instant Pay' , '3000', 'teal lighten-2');
				return false;
			}
			
			if(dueDate=='' || dueDate==undefined){
				Materialize.toast('Select Due Date' , '3000', 'teal lighten-2');
				return false;
			}

			var dueDate2=new Date($('#dueDate').val()).setHours(0,0,0,0);
			var currDate=new Date().setHours(0,0,0,0);
			if(dueDate2<currDate){
				Materialize.toast('Please Select Correct Due Date' , '3000', 'teal lighten-2');
				return false;
			}
			$('.dueDateCls').val(dueDate);

			if(payTypeRdo=="Cheque"){
			
				if(bankName=='' || bankName==undefined){
					Materialize.toast('Enter Bank Name' , '3000', 'teal lighten-2');
					return false;
				}
	
				if(chqNo=='' || chqNo==undefined){
					Materialize.toast('Enter Cheque Number' , '3000', 'teal lighten-2');
					return false;
				}
	
				if(chequeDate=='' || chequeDate==undefined){
					Materialize.toast('Select Cheque Date' , '3000', 'teal lighten-2');
					return false;
				}
	
				var chequeDate2=new Date($('#chequeDate').val()).setHours(0,0,0,0);
				var currDate=new Date().setHours(0,0,0,0);
				if(chequeDate2<currDate){
					Materialize.toast('Please Select Correct Cheque Date' , '3000', 'teal lighten-2');
					return false;
				}
			}

		}else if(paymentRdo=="Credit"){

			
			if(dueDateCredit=='' || dueDateCredit==undefined){
				Materialize.toast('Select Due Date' , '3000', 'teal lighten-2');
				return false;
			}

			var dueDateCredit2=new Date($('#dueDateCredit').val()).setHours(0,0,0,0);
			var currDate=new Date().setHours(0,0,0,0);
			if(dueDateCredit2<currDate){
				Materialize.toast('Please Select Correct Due Date' , '3000', 'teal lighten-2');
				return false;
			}
			
			$('.dueDateCls').val(dueDateCredit);
		}else{
			if(payTypeRdo=="Cheque"){
			
				if(bankName=='' || bankName==undefined){
					Materialize.toast('Enter Bank Name' , '3000', 'teal lighten-2');
					return false;
				}
	
				if(chqNo=='' || chqNo==undefined){
					Materialize.toast('Enter Cheque Number' , '3000', 'teal lighten-2');
					return false;
				}
	
				if(chequeDate=='' || chequeDate==undefined){
					Materialize.toast('Select Cheque Date' , '3000', 'teal lighten-2');
					return false;
				}
	
				var chequeDate2=new Date($('#chequeDate').val()).setHours(0,0,0,0);
				var currDate=new Date().setHours(0,0,0,0);
				if(chequeDate2<currDate){
					Materialize.toast('Please Select Correct Cheque Date' , '3000', 'teal lighten-2');
					return false;
				}
			}
		}

		

		var businessListId=$('#businessListId').val();	

		$('.productListCls').val(productListData);
		$('.businessNameIdCls').val(businessListId);
		$('.paidAmountCls').val(amountPartial);
		$('.balAmountCls').val(balAmountPartial);
		$('.payTypeCls').val(payTypeRdo);
		$('.paymentCls').val(paymentRdo);		
		$('.bankNameCls').val(bankName);
		$('.chequeNumberCls').val(chqNo);
		$('.chequeDateCls').val(chequeDate);	
		
		return true;
}

/**
 * find available qty
 * @param {*} productId 
 */
function qtyAvailable(productId){
	var productDB=fetchProductByProductId(productId);
	var qty=0;
	for(var i=0; i<productCart.length; i++){
		var product=productCart[i][0];
		if(product.productId==productId){
			qty+=parseInt(productCart[i][1]);
		}		 
	}
	var availableQty=parseInt(productDB.currentQuantity)-parseInt(qty);
	return availableQty;
}

/**
 * appends table rows using productCart[] 
 * @returns
 */
function refreshTable(){
	$('#cartTbl').empty();
	for(var i=0; i<productCart.length; i++){
		var product=productCart[i][0];
		var qty=productCart[i][1];	
		var mrp=productCart[i][2];
		var total=productCart[i][3];
		var type=productCart[i][4];

		var productName='<td>'+product.productName+'</td>';
		var mrpCell='<td><input type="text" id="productMrp_'+type+'_'+product.productId+'" value='+mrp+' class="editable center"  style="width: 4em;" /></td>';
		if(type=="Free"){
			productName='<td>'+product.productName+'<font color="green">-(Free)</font></td>';
			mrpCell='<td>0</td>';
		}

		$('#cartTbl').append('<tr>'+
				//'<td>'+(i+1)+'</td>'+
				  productName+
				//'<td><span id="productCurrQty_'+product.productId+'"'+product.currentQuantity+'</span></td>'+
				  mrpCell+
				'<td><input type="text" id="productQty_'+type+'_'+product.productId+'" value='+qty+' class="editable center"  style="width: 4em;" /></td>'+
				'<td><span id="productTotal_'+type+'_'+product.productId+'">'+total+'</span></td>'+
				'<td><button class="deleteIcon btn-flat" onclick="deleteFromCart('+product.productId+',\''+type+'\')" ><i class="material-icons" >delete</i></button></td>'+
				'</tr>');
		
		/**
		 * only number allowed without decimal
		 */
		$('#productMrp_'+type+'_'+product.productId).keydown(function(e){            	
			-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
		 });

		/**
		 * only number allowed without decimal
		 */
		 $('#productMrp_'+type+'_'+product.productId).keypress(function(e){						
			if (e.keyCode === 46 && this.value.split('.').length === 2) {
				   return false;
			   }		
		});

		/**
		 * mrp change event in table rows
		 * then change total amount according mrp*qty
		 */
		$('#productMrp_'+type+'_'+product.productId).keyup(function(){
			var id=$(this).attr('id')
			var type=id.split('_')[1];
			var productId=id.split('_')[2];
			var productData;
			
			for(var i=0; i<productCart.length; i++){
				if(productId==productCart[i][0].productId && type==productCart[i][4]){
					productData=productCart[i];
					productCart.splice(i, 1);
					break;
				}
			}
			
			var product=productData[0];
			var qty=productData[1];	
			var mrp=$('#productMrp_'+type+'_'+productId).val();
			if(mrp==undefined || mrp==''){
				mrp=0;
			}

			var total=parseInt(qty)*parseFloat(mrp);
			if(type=="Free"){
				total=0;
			}

			$('#productTotal_'+type+'_'+productId).text(total);
			productCart.push([product,qty,mrp,total,type]);
			
			//find final total Amount
			findTotal();
		});
		/**
		 * only number allowed without decimal 
		 */
		$('#productQty_'+type+'_'+product.productId).keypress(function( event ){
		    var key = event.which;						    
		    if( ! ( key >= 48 && key <= 57 || key === 13) )
		        event.preventDefault();
		});
		/**
		 * qty change event in table rows
		 * then change total amount according mrp*qty
		 */
		$('#productQty_'+type+'_'+product.productId).keyup(function(){
			var id=$(this).attr('id')
			var type=id.split('_')[1];
			var productId=id.split('_')[2];
			var productData;
			
			for(var i=0; i<productCart.length; i++){
				if(productId==productCart[i][0].productId && type==productCart[i][4]){
					productData=productCart[i];
					productCart.splice(i, 1);
					break;
				}
			}
			
			var product=productData[0];
			var qty=$('#productQty_'+type+'_'+productId).val();
			if(qty==undefined || qty==''){
				qty=0;   
			}

			var availableQty=qtyAvailable(productId);
			if(parseInt(qty)>parseInt(availableQty)){
				Materialize.toast('Qty Must be Below or equal Current Quantity. Max : '+availableQty, '3000', 'teal lighten-2');
				$('#productQty_'+type+'_'+productId).val(parseInt(productData[1]));
				productCart.push(productData);
				return false;
			}
			var mrp=productData[2];
			var total=parseInt(qty)*parseFloat(mrp);
			if(type=="Free"){
				total=0;
			}

			$('#productTotal_'+type+'_'+productId).text(total);
			productCart.push([product,qty,mrp,total,type]);

			//find final total Amount
			findTotal();
		});
	}
	//find final total Amount
	findTotal();
}

/**
 * delete product from cart
 * @param {*} productId 
 */
function deleteFromCart(productId,type){
	for(var i=0; i<productCart.length; i++){
		var productCt=productCart[i][0];
		if(productId==productCt.productId && type==productCart[i][4]){
			//removing productData from current index				
			productCart.splice(i, 1);
			refreshTable();
			return false;
		}
	}
}

/**
 * find final totalAmount
 */
function findTotal(){
	var finalTotal=0;
	for(var i=0; i<productCart.length; i++){
		finalTotal+=parseFloat(productCart[i][3]);
	}

	$('#amountPartial').val('');
	$('#amountPartial').change();
	$('#balAmountPartial').val(parseFloat(finalTotal));
	$('#balAmountPartial').change();

	$('#totalValue').html('&#8377; <font color="blue" ><b>'+finalTotal+'</b></font>');
}

/**
 * check current qty in db and change cart current qty according it
 * @param {*} productList 
 */
/* function refreshProductCurrentQuantity(){
	
	var productList=productListData();
	//alert(productList);
	for(var i=0; i<productList.length; i++){
		var productDb=productList[i];
		for(var i=0; i<productCart.length; i++){
			var productCt=productCart[i][0];
			if(productDb.productId==productCt.productId){
				
				productData=productCart[i];
				
				//removing productData from current index				
				productCart.splice(i, 1);
				
				var product=productDb;
				var qty=productData[1];	
				var mrp=productData[2];
				var total=productData[3];
				var type=productData[4];
				
				productCart.push([product,qty,mrp,total,type]);
			}
		}
	}
	
	refreshTable();	
}  */

/**
 * fetch all product list
 */
function productListData(){

	var productList;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductListAjax",
		dataType : "json",
		async : false,
		success:function(data)
		{
			productList=data;
		},
		error: function(xhr, status, error) {
			console.log("product data list not loaded");
		}
	});
	return productList;
}

/**
 * fetch product details by productId
 * @param {*} productId 
 */
function fetchProductByProductId(productId){
	
	var product;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductByProductId?productId="+productId,
		dataType : "json",
		async : false,
		success:function(data)
		{
			product=data;
		},
		error: function(xhr, status, error) {
			console.log("product not loaded");
		}
	});
	return product;
}

function submitCounterOrder(formData){
	$.ajax({
		type:"POST",
		url : myContextPath+"/saveCounterOrder",
		dataType : "json",
		data : formData,
		async : false,
		beforeSend: function() {
			$('.preloader-background').show();
			$('.preloader-wrapper').show();
		   },
		success:function(data){
			
			if(data.status=="Success"){
				if(isPrintBill){
					//url to get pdf pring
					var win = window.open(myContextPath+"/counterOrderInvoice.pdf?counterOrderId="+data.errorMsg, '_blank');
					win.focus();
					win.print();
					Materialize.toast('Order Created SuccessFully' , '3000', 'teal lighten-2');
					setTimeout(function() {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						//url to redirect openCounter
						win.close();
						window.location.href=myContextPath+"/openCounter";
					},5000);
				}else{
					//$('.preloader-wrapper').hide();
					//$('.preloader-background').hide();

					Materialize.toast('Order Created SuccessFully' , '3000', 'teal lighten-2');
					setTimeout(function() {
				 		window.location.href=myContextPath+"/openCounter";
					},3000);
				}
			}else{
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();

				Materialize.toast('Order Creating Failed', '3000', 'red lighten-2');
			}
		},
		error: function(xhr, status, error) {
			$('.preloader-wrapper').hide();
			$('.preloader-background').hide();

			console.log("Something Went Wrong");
			Materialize.toast('Something Went Wrong' , '3000', 'teal lighten-2');
		}
	});
}

function resetProduct(){
	var source1 = $("#productId");
	source1.val(0);
	source1.change();

	$('#quantityId').val('');

	$('#nonFreeId').click();
}