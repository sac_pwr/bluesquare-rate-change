var productListQtyMng=[];
var orderProductDetails;
var orderDetails;
var radioButton="NonFree";
var orderStatus;
var orderProductDetailsForIssueCheck=[];
var quantityZeroValidation=false;
$(document).ready(function() 
{
	orderStatus=$('#orderStatusId').val();
	$('#updateId').click(function(){

		if(orderProductDetails.length==0){
			Materialize.toast('Add some products', '3000', 'teal lighten-2');
			return false;
		}

		if(quantityZeroValidation)
		{
			Materialize.toast('Quantity make as Non zero or delete this product', '3000', 'teal lighten-2');
			return false;
		}
		
		var isNonFreeFound=false;
		for(var j=0; j<orderProductDetails.length; j++)
		{			
			var orderProduct=orderProductDetails[j];
			if(orderProduct.type=='NonFree'){
				isNonFreeFound=true;
			}
		}
		if(isNonFreeFound==false){
			Materialize.toast('Minimum One Non-Free Product Required For Order', '3000', 'teal lighten-2');
			return false;
		}

	if(orderStatus==="Packed"){
		/**cheking packed qty with invt */

		var productListWithQty=new HashMap();
		for(var j=0; j<orderProductDetails.length; j++){
			var orderProduct=orderProductDetails[j];
			var oldOrderProduct=getOldOrderProduct(orderProduct.product.product.productId,orderProduct.type);

			if(productListWithQty.containsKey(orderProduct.product.product.productId)){				
				var entry=productListWithQty.get(orderProduct.product.product.productId);
				var qty;
				if(oldOrderProduct==null){
					qty=parseInt(entry.value)+parseInt(orderProduct.issuedQuantity);
				}else{
					qty=parseInt(entry.value)+parseInt(orderProduct.issuedQuantity-oldOrderProduct.issuedQuantity);
				}
				if(qty>0){
					productListWithQty.remove(orderProduct.product.product.productId);
					var value=[parseInt(qty),orderProduct.type,oldOrderProduct.issuedQuantity];
					productListWithQty.put(orderProduct.product.product.productId,value);
				}
			}else{				
				var qty;
				if(oldOrderProduct==null){
					qty=parseInt(orderProduct.issuedQuantity);
				}else{
					qty=parseInt(orderProduct.issuedQuantity-oldOrderProduct.issuedQuantity);
				}
				if(qty>0){
					var value=[parseInt(qty),orderProduct.type,oldOrderProduct.issuedQuantity];
					productListWithQty.put(orderProduct.product.product.productId,value);
				}
			}
		}

		var entrySet=productListWithQty.getAll()
		
		for(var i=0; i<entrySet.length; i++){
			var key=entrySet[i].key;
			var val=entrySet[i].value[0];
			var product=fetchProductByProductId(key);

			if(product.currentQuantity<val){
				Materialize.toast(product.productName+ '('+entrySet[i].value[1]+') qty exceeds Current Qty. Max Available :  '+ (parseInt(entrySet[i].value[2])+parseInt(product.currentQuantity)), '3000', 'teal lighten-2');
				console.log(product.productName+" qty exceeds Current Qty");
				return false;
			}

		} 
		/* cheking end */
	}
		/*[productId,purchaseQuantity,issuedQuantity,rate,sellingRate,igst,purchaseAmount,issuedAmount,type]
		 * orderProductDetailsTemp.push
							({
								"purchaseQuantity" :orderProduct.purchaseQuantity,
								"issuedQuantity":orderProduct.issuedQuantity,
								"rate":orderProduct.product.rate,
								"sellingRate":orderProduct.sellingRate,
								"purchaseAmount":orderProduct.purchaseAmount,
								"issueAmount":orderProduct.issueAmount,
								"orderDetails":orderProduct.orderDetails,
								"product":{"product":orderProduct.product.product},
								"type":orderProduct.type
							});
		 * */
		var updateOrderProductList="";
		for(var j=0; j<orderProductDetails.length; j++)
		{			
			var orderProduct=orderProductDetails[j];

			var editMode="Non Edit";
			if(orderStatus==="Packed")
			{
				for(var k=0; k<orderProductDetailsForIssueCheck.length; k++)
				{
					var orderProductOld=orderProductDetailsForIssueCheck[k];
					if(orderProductOld.product.productId==orderProduct.product.product.productId 
						&& orderProductOld.issuedQuantity!=orderProduct.issuedQuantity)
					{
						editMode="Edit";
					}
				}
				if(orderProduct.purchaseQuantity==0)
				{
					editMode="Edit";
				}
				updateOrderProductList =updateOrderProductList + 
										orderProduct.product.product.productId+","+
										orderProduct.purchaseQuantity+","+
										orderProduct.issuedQuantity+","+
										orderProduct.rate+","+
										orderProduct.sellingRate+","+
										orderProduct.purchaseAmount+","+
										orderProduct.issueAmount+","+
										orderProduct.type+","+
										orderProduct.igst+","+
										editMode+"-";
			}
			else
			{
				updateOrderProductList =updateOrderProductList + 
										orderProduct.product.product.productId+","+
										orderProduct.purchaseQuantity+","+
										orderProduct.issuedQuantity+","+
										orderProduct.rate+","+
										orderProduct.sellingRate+","+
										orderProduct.purchaseAmount+","+
										orderProduct.issueAmount+","+
										orderProduct.type+","+
										orderProduct.igst+"-";
			}
		}
		updateOrderProductList=updateOrderProductList.slice(0,-1);
		$('#updateOrderProductListId').val(updateOrderProductList);		
	});

	function getOldOrderProduct(productId,type){
		for(var k=0; k<orderProductDetailsForIssueCheck.length; k++){
			var orderProductOld=orderProductDetailsForIssueCheck[k];
			if(orderProductOld.product.product.productId==productId 
				&& orderProductOld.type==type) 
			{
				return orderProductOld;
			}
		}
		return null;
	}

	$('#brandid,#categoryid').change(function(){
		var brandId=$('#brandid').val();
		var categoryId=$('#categoryid').val();		
		filterProductList(brandId,categoryId);
	});
	if(orderStatus==="Packed"){

		$('#productid').change( function(){
			var productId=$('#productid').val();
			if(productId==0){
				return false;
			}
			var product= fetchProductByProductId(productId);
			var qtyAvail=qtyAvailable(productId);

			$('#curr_qty').val(qtyAvail);
			$('#curr_qty').change();
		});

		$('#qty').keyup(function(){

			var productId=$('#productid').val();
			var qty=$('#qty').val();
			var qtyAvail=qtyAvailable(productId);

			if(qtyAvail<qty){
				Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(qtyAvail), '3000', 'teal lighten-2');
				$('#qty').val(0);
			}

		});

		 function qtyAvailable(productId){
			var product=fetchProductByProductId(productId);
			
			var qtyAvail;
			var isFoundIn_ProductListQtyMng=false;
			for(var i=0; i<productListQtyMng.length; i++){
				if(productListQtyMng[i].productId==productId){
					productQtyMng=productListQtyMng[i];	
					qtyAvail=parseInt(productQtyMng.qtyExtraAvail)+parseInt(product.currentQuantity);
					isFoundIn_ProductListQtyMng=true;			
					break;
				}		
			}
			if(isFoundIn_ProductListQtyMng==false){
				try{
					qtyAvail=parseInt(product.currentQuantity);
				}catch(err){
					console.log(err);
					console.log("Product Id :"+productId);
				}
				
			} 
			if(qtyAvail<0){
				return 0;
			}
			return qtyAvail;
		}


		//function getAllowedQuantity(product){
			
			//return qtyAvailable(product.productId);

			/* var allowedQty=0;
			var isOld=false;
			var orderProductOld;
			var oldQty=0;
			for(var j=0; j<orderProductDetailsForIssueCheck.length; j++){
				var orderProduct=orderProductDetailsForIssueCheck[j];
				if(parseInt(orderProduct.product.product.productId)===parseInt(product.productId)){ 
					orderProductOld=orderProductDetailsForIssueCheck[j];
					isOld=true;
					oldQty+=parseInt(orderProduct.issuedQuantity);
					if(isProductDeleted(product.productId,orderProduct.type)){
						allowedQty+=parseInt(orderProduct.issuedQuantity);	
					}
				}
			}
			for(var j=0; j<orderProductDetails.length; j++){
				var orderProduct=orderProductDetails[j];
				if(orderProduct.product.product.productId==product.productId){
					if(isOld){
						if(orderProductOld.issuedQuantity!=orderProduct.issuedQuantity){
							allowedQty+=oldQty;
							if(orderProductOld.issuedQuantity>orderProduct.issuedQuantity){
								allowedQty-=parseInt(orderProductOld.issuedQuantity-orderProduct.issuedQuantity);
							}else{
								allowedQty+=parseInt(orderProduct.issuedQuantity-orderProductOld.issuedQuantity);
							}
						}
					}else{
						allowedQty-=parseInt(orderProduct.issuedQuantity);
					}
				}
			}	
			if(product.productId==36){
				var a="sdds";
			}	
			
			allowedQty+=product.currentQuantity;
			return allowedQty; */
		//}

		/* function isProductDeleted(productId,type){
			for(var j=0; j<orderProductDetails.length; j++){
				var orderProduct=orderProductDetails[j];
				if(orderProduct.product.product.productId==productId && orderProduct.type==type){
					return false;
				}
			}
			return true;
		}
		function isProductAdded(productId,type){
			for(var j=0; j<orderProductDetails.length; j++){
				var orderProduct=orderProductDetails[j];
				if(orderProduct.product.product.productId==productId && orderProduct.type==type){
					return true;
				}
			}
			return false;
		} */

		function refreshAllowedQuantityOfProducts(productId){	
			var product=fetchProductByProductId(productId);		
			var qtyAvail=qtyAvailable(product.productId);
			$('.current_qty_'+product.productId).text(qtyAvail);
			$('#productid').change();
		}  

		 function productListQtyMngFunc(id,qty,isAdd){
			var isProductQtyMngFound=false;
			for(var i=0; i<productListQtyMng.length; i++){
				if(productListQtyMng[i].productId==id){
					isProductQtyMngFound=true;
					var productListQtyMngTemp=productListQtyMng[i];
					productListQtyMng.splice(i, 1);
					break;
				}
			}

			var oldQuantity=0;
			for(var k=0; k<orderProductDetailsForIssueCheck.length; k++){
				var orderProductOld=orderProductDetailsForIssueCheck[k];
				if(orderProductOld.product.product.productId==id) 
				{
					oldQuantity+=parseInt(orderProductOld.issuedQuantity);
				}
			}

			var updatedQuantity=0;			
			for(var j=0; j<orderProductDetails.length; j++){
				var orderProductUpdated=orderProductDetails[j];	
				if(orderProductUpdated.product.product.productId==id) 
				{			
					updatedQuantity+=parseInt(orderProductUpdated.issuedQuantity);
				}
			}

			var qtyExtra=oldQuantity-updatedQuantity;

			var productListQtyMngTemp={
						productId:	id,
						qtyExtraAvail:qtyExtra
					};
			productListQtyMng.push(productListQtyMngTemp);
			
			refreshAllowedQuantityOfProducts(id);
		}
	} 
	//here privious orderProductDetails store in array and make event for delete,edit qty
	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchOrderProductDetailsEditOrder?orderId="+orderId,
		dataType : "json",
		success:function(data)
		{
			orderProductDetails=data;
			orderDetails=orderProductDetails[0].orderDetails;
			var orderProductDetailsTemp=[];
			for(var j=0; j<orderProductDetails.length; j++)
			{
				var orderProduct=orderProductDetails[j];
				
				//its function defined in calculateProperAmount.js 
				var calculateProperTaxObj=calculateProperTax(orderProduct.sellingRate,orderProduct.product.product.categories.igst);
				
				orderProductDetailsTemp.push
				({
					"purchaseQuantity" :orderProduct.purchaseQuantity,
					"issuedQuantity":orderProduct.issuedQuantity,
					"rate":calculateProperTaxObj.unitPrice,
					"sellingRate":orderProduct.sellingRate,
					"igst":orderProduct.product.product.categories.igst,
					"purchaseAmount":orderProduct.purchaseAmount,
					"issueAmount":orderProduct.issueAmount,
					//"orderDetails":orderProduct.orderDetails,
					"product":{"product":orderProduct.product.product},
					"type":orderProduct.type
				});

				orderProductDetailsForIssueCheck.push
				({
					"purchaseQuantity" :orderProduct.purchaseQuantity,
					"issuedQuantity":orderProduct.issuedQuantity,
					"rate":calculateProperTaxObj.unitPrice,
					"sellingRate":orderProduct.sellingRate,
					"igst":orderProduct.product.product.categories.igst,
					"purchaseAmount":orderProduct.purchaseAmount,
					"issueAmount":orderProduct.issueAmount,
					//"orderDetails":orderProduct.orderDetails,
					"product":{"product":orderProduct.product.product},
					"type":orderProduct.type
				});
				
				var product=orderProduct.product.product;
				if(orderProduct.type==="Free")
				{
					$('#delPrdFree_'+product.productId).click(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];
						var igst;
						var deletingQty=0;

						//delete row and rearrange sr.no.
						var row=$(this).parents("tr");				
						var siblings = row.siblings();
					    row.remove();
					    siblings.each(function(index) {
					         $(this).children().first().text(index+1);
					    });
	
						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
								srno++;
							}		
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
								srno++;
							}	
							else
							{
								deletingQty=orderProduct.issuedQuantity;
								igst=orderProduct.product.product.categories.igst;
							}			
						}
						orderProductDetails=orderProductDetailsTemp;

						if(orderStatus==="Packed"){
							//qty return add in inventory
							productListQtyMngFunc(productId);
							$('#productid').change();
						}
						
						findTotal();
					});
				}
				else
				{
					$('#delPrdNonFree_'+product.productId).click(function(){
						var id=$(this).attr('id');
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];
						var igst;
						var deletingQty=0;

						//delete row and rearrange sr.no.
						var row=$(this).parents("tr");				
						var siblings = row.siblings();
					    row.remove();
					    siblings.each(function(index) {
					         $(this).children().first().text(index+1);
					    });
					       
						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
								srno++;
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
								srno++;
							}
							else
							{
								deletingQty=orderProduct.issuedQuantity;
								igst=orderProduct.product.product.categories.igst;
							}							
						}
						orderProductDetails=orderProductDetailsTemp;	
						
						if(orderStatus==="Packed"){
							//qty return add in inventory
							productListQtyMngFunc(productId);						
							$('#productid').change();	
						}		
						findTotal();
					});
				}
				if(orderProduct.type==="Free")
				{
					$('#qtyFree_'+product.productId).keypress(function( event ){
					    var key = event.which;						    
					    if( ! ( key >= 48 && key <= 57 || key === 13) )
					        event.preventDefault();
					});
					$('#qtyFree_'+product.productId).keyup(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;
						var igst;
						var previousissuedQuantity;

						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}
							else
							{
								previousissuedQuantity=orderProduct.issuedQuantity;
								igst=orderProduct.product.product.categories.igst;
							}			
						}
						var product;
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="NonFree")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						var qty=$('#qtyFree_'+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}

						
						if(orderStatus==="Packed"){
							// /parseInt(previousissuedQuantity)+
							var allowedQty=parseInt(previousissuedQuantity)+qtyAvailable(product.productId);

							if(allowedQty<qty){

								Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(allowedQty), '3000', 'teal lighten-2');

								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$('#qtyFree_'+productId).val(orderProductEdit.issuedQuantity);

								 //edit inventory 								
								productListQtyMngFunc(product.productId);

								findTotal();

								return false;
							}
						}
						if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$('#qtyFree_'+productId).val(orderProductEdit.issuedQuantity);
								findTotal();
								return false;
							}
						}
						
						var mrp=0;
						var purchaseAmt=0;
						var issuedAmt=0;
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixed(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"Free"
							};
							$('#purchaseAmtFree_'+id.split('_')[1]).text(purchaseAmt.toFixed(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixed(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"Free"
							};
							$('#issuedAmtFree_'+id.split('_')[1]).text(issuedAmt.toFixed(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;

						if(orderStatus==="Packed"){
							//edit inventory 
							productListQtyMngFunc(product.productId);
						}
						findTotal();
					});
				}
				else
				{
					$('#qtyNonFree_'+product.productId).keypress(function( event ){
					    var key = event.which;						    
					    if( ! ( key >= 48 && key <= 57 || key === 13) )
					        event.preventDefault();
					});
					$('#qtyNonFree_'+product.productId).keyup(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;				
						var mrp,igst;
						var previousissuedQuantity;
						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}
							else {
								previousissuedQuantity=orderProduct.issuedQuantity;
								mrp=orderProduct.sellingRate;
								igst=orderProduct.product.product.categories.igst;
							}
						}
						var product;
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="Free")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						
						var qty=$('#qtyNonFree_'+product.productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						if(orderStatus==="Packed"){
							var allowedQty=parseInt(previousissuedQuantity)+qtyAvailable(product.productId); //parseInt(previousissuedQuantity)+

							if(allowedQty<qty){

								Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(allowedQty), '3000', 'teal lighten-2');

								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$('#qtyNonFree_'+productId).val(orderProductEdit.issuedQuantity);

								 //edit inventory 								
								productListQtyMngFunc(product.productId);

								findTotal();

								return false;
							}
						}
						if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$('#qtyNonFree_'+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixed(2));
								findTotal();
								return false;
							}
						}
						
						var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
						var issuedAmt=parseFloat(mrp)*parseFloat(qty);
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixed(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixed(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixed(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixed(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						
						 if(orderStatus==="Packed"){
							//edit inventory 
							productListQtyMngFunc(product.productId);
						} 
						findTotal();
					});
				}
				if(orderProduct.type==="Free")
				{
					$('#mrpFree_'+product.productId).keydown(function(e){            	
						-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
					 });
					
					 $('#mrpFree_'+product.productId).keypress(function(e){						
						if (e.keyCode === 46 && this.value.split('.').length === 2) {
							   return false;
						   }		
					});
					$('#mrpFree_'+product.productId).keyup(function(){

						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;	
						var igst;
						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}
							else
							{
								igst=orderProduct.product.product.categories.igst;
							}
						}
						var product;
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="Free")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						var qty=$("#qtyFree_"+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$("#mrpFree_"+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixed(2));
								findTotal();
								return false;
							}
						}
						
						var mrp=0;
						var purchaseAmt=0;
						var issuedAmt=0;
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":0,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixed(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixed(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":0,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixed(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixed(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						
						findTotal();

					});
				}else{
					$('#mrpNonFree_'+product.productId).keydown(function(e){            	
						-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
					 });
					
					 $('#mrpFree_'+product.productId).keypress(function(e){						
						if (e.keyCode === 46 && this.value.split('.').length === 2) {
							   return false;
						   }		
					});
					$('#mrpNonFree_'+product.productId).keyup(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;				
						var mrp=$('#mrpNonFree_'+productId).val();
						var igst;
						if(mrp=='' || mrp==undefined)
						{
							mrp="0";
						}

						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}else{
								igst=orderProduct.product.product.categories.igst;
							}
						}
						var product;
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="Free")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						var qty=$("#qtyNonFree_"+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$("#mrpNonFree_"+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixed(2));
								findTotal();
								return false;
							}
						}
						
						var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
						var issuedAmt=parseFloat(mrp)*parseFloat(qty);						
						
						//its function defined in calculateProperAmount.js 
						var calculateProperTaxObj=calculateProperTax(mrp,igst);
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":calculateProperTaxObj.unitPrice,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixed(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixed(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":product.rate,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixed(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixed(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						
						findTotal();
					});
				}
			}
			
			orderProductDetails=orderProductDetailsTemp;
			//orderProductDetailsForIssueCheck=orderProductDetailsTemp;
		},
		error: function(xhr, status, error) {
			Materialize.toast('Something Went Wrong', '3000', 'teal lighten-2');
		}
	});
	
/* 	
	//on ready productList Data fetch
	productListData();
	filterProductList("0","0");
	//fetch All ProductList and store array every 5 second
	window.setInterval(productListData, 5000); */
	
	$('#freeId').click(function(){
		radioButton="Free";
	});
	$('#nonFreeId').click(function(){
		radioButton="NonFree";
	});
	
	$('#orderProductAddId').click( function(){
		var length = document.getElementById('productcart').getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
		length++;
		
		var productId=$('#productid').val();
		var qty=$('#qty').val();
		if(qty=='' || qty==undefined)
		{
			qty=0;
		}
		if(productId==="0")
		{
			Materialize.toast('Select Product', '3000', 'teal lighten-2');
   	     	return false;
		}
		else if(qty==0)
		{
			Materialize.toast('Enter Quantity', '3000', 'teal lighten-2');
   	     	return false;
		}
		
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			if(orderProduct.product.product.productId==productId && orderProduct.type===radioButton)
			{ 
				Materialize.toast('Product Already added', '3000', 'teal lighten-2');
       	     	return false;
			}
		}
		var mrp;
		var product;
		var isOldProduct=false;
		var igst;
		for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
		{
			var orderProduct=orderProductDetailsForIssueCheck[j];
			if(orderProduct.product.product.productId===parseInt(productId) && orderProduct.type===radioButton)
			{ 
				mrp=orderProduct.sellingRate;
				product=orderProduct.product.product;				
				product.productId=orderProduct.product.product.productId;
				igst=orderProduct.product.product.categories.igst;
				isOldProduct=true;
			}
		}
		
		if(isOldProduct==false)
		{
			var product= fetchProductByProductId(productId);	

			igst=product.categories.igst;
			mrp=Math.round(product.rate+((product.rate*product.categories.igst)/100));
		}		
		
		var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
		var issuedAmt=parseFloat(mrp)*parseFloat(qty);
		
		if(orderStatus==="Booked")
		{
			if(radioButton==="NonFree")
			{
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
		        		'<td>'+product.productName+'</td>'+
		        		'<td><input style="text-align:center;" type="text" id="qtyNonFree_'+product.productId+'" value="'+qty+'"></td>'+
		        		'<td><input style="text-align:center;" type="text" id="mrpNonFree_'+product.productId+'" value="'+parseFloat(mrp)+'"></td>'+
		        		'<td><span id="purchaseAmtNonFree_'+product.productId+'">'+purchaseAmt+'</span></td>'+
		        		'<td><button id="delPrdNonFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}
			else
			{
				mrp=0;
				purchaseAmt=0;
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
		        		'<td>'+product.productName+'<font color="green">-(Free)</font></td>'+
		        		'<td><input style="text-align:center;" type="text" id="qtyFree_'+product.productId+'" value="'+qty+'"></td>'+
		        		'<td>'+parseFloat(mrp)+'</td>'+
		        		'<td><span id="purchaseAmtFree_'+product.productId+'">'+purchaseAmt+'</span></td>'+
		        		'<td><button id="delPrdFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}
			
			orderProduct=
			{
				"purchaseQuantity" :qty,
				"issuedQuantity":0,
				"rate":product.rate,
				"sellingRate":mrp,
				"igst":igst,
				"purchaseAmount":purchaseAmt.toFixed(2),
				"issueAmount":0,
				//"orderDetails":orderProductDetails[0].orderDetails,
				"product":{"product":product},
				"type":radioButton
			};

			orderProductDetails.push(orderProduct);
		}
		else
		{
			orderProduct=
			{
				"purchaseQuantity" :0,
				"issuedQuantity":qty,
				"rate":product.rate,
				"sellingRate":mrp,
				"igst":igst,
				"purchaseAmount":0,
				"issueAmount":issuedAmt.toFixed(2),
				//"orderDetails":orderProductDetails[0].orderDetails,
				"product":{"product":product},
				"type":radioButton
			};

			orderProductDetails.push(orderProduct);

			//delete qty from temp inventory
			productListQtyMngFunc(product.productId);

			if(radioButton==="NonFree")
			{
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
						'<td>'+product.productName+'</td>'+
						'<td><span class="current_qty_'+product.productId+'">'+qtyAvailable(product.productId)+'</span></td>'+
		        		'<td><input style="text-align:center;" type="text" id="qtyNonFree_'+product.productId+'" value="'+qty+'"></td>'+
						'<td><input style="text-align:center;" type="text" id="mrpNonFree_'+product.productId+'" value="'+parseFloat(mrp)+'"></td>'+
		        		'<td><span id="issuedAmtNonFree_'+product.productId+'">'+issuedAmt+'</span></td>'+
		        		'<td><button id="delPrdNonFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}
			else
			{
				mrp=0;
				issuedAmt=0;
				$('#producctListtb').append(
						'<tr>'+
		        		'<td>'+length+'</td>'+
						'<td>'+product.productName+'<font color="green">-(Free)</font></td>'+
						'<td><span class="current_qty_'+product.productId+'">'+qtyAvailable(product.productId)+'</span></td>'+
		        		'<td><input style="text-align:center;" type="text" id="qtyFree_'+product.productId+'" value="'+qty+'"></td>'+
		        		'<td>'+parseFloat(mrp)+'</td>'+
		        		'<td><span id="issuedAmtFree_'+product.productId+'">'+issuedAmt+'</span></td>'+
		        		'<td><button id="delPrdFree_'+product.productId+'" class="btn-flat"><i class="material-icons">clear</i></button></td>'+
		        		'</tr>');
			}			
		}

		findTotal();
		
		if(orderProduct.type==="Free")
		{
			$('#delPrdFree_'+product.productId).off("click");
			$('#delPrdFree_'+product.productId).click(function(){
				var id=$(this).attr('id')
				var productId=id.split('_')[1];
				var orderProductDetailsTemp=[];
				var addingQty;
				//delete row and rearrange sr.no.
				var row=$(this).parents("tr");				
				var siblings = row.siblings();
			    row.remove();
			    siblings.each(function(index) {
			         $(this).children().first().text(index+1);
			    });
				
				var srno=1;
				for(var j=0; j<orderProductDetails.length; j++)
				{
					var orderProduct=orderProductDetails[j];
					if(orderProduct.type==="NonFree")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
						srno++;
					}		
					else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
						srno++;
					}else{
						addingQty=orderProduct.issuedQuantity;
					}				
				}
				orderProductDetails=orderProductDetailsTemp;

				 if(orderStatus=="Packed"){
					//qty return add in inventory
					productListQtyMngFunc(productId);
					$('#productid').change();
				} 
				findTotal();
			});
		}
		else
		{
			$('#delPrdNonFree_'+product.productId).off("click");
			$('#delPrdNonFree_'+product.productId).click(function(){
				var id=$(this).attr('id')
				var productId=id.split('_')[1];
				var orderProductDetailsTemp=[];
				var addingQty;
				//delete row and rearrange sr.no.
				var row=$(this).parents("tr");				
				var siblings = row.siblings();
			    row.remove();
			    siblings.each(function(index) {
			         $(this).children().first().text(index+1);
			    });
				
				var srno=1;
				for(var j=0; j<orderProductDetails.length; j++)
				{
					var orderProduct=orderProductDetails[j];
					if(orderProduct.type==="Free")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
						srno++;
					}	
					else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
						srno++;
					}else{
						addingQty=orderProduct.issuedQuantity;
					}				
				}
				orderProductDetails=orderProductDetailsTemp;

				 if(orderStatus=="Packed"){
					//qty return add in inventory
					productListQtyMngFunc(productId);
					$('#productid').change();
				} 
				findTotal();
			});
		}
		if(orderProduct.type==="Free")
		{
			$('#qtyFree_'+product.productId).keypress(function( event ){
			    var key = event.which;						    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});
			$('#qtyFree_'+product.productId).off("keyup");
			$('#qtyFree_'+product.productId).keyup(function(){
				var id=$(this).attr('id')
				var productId=id.split('_')[1];
				var orderProductDetailsTemp=[];				
				var previousissuedQuantity;
				var product=fetchProductByProductId(productId);
				var igst=product.categories.igst;
				var srno=1;
				for(var j=0; j<orderProductDetails.length; j++)
				{
					var orderProduct=orderProductDetails[j];
					if(orderProduct.type==="NonFree")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
					}	
					else if(orderProduct.product.product.productId!=productId && orderProduct.type==="Free")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
					}else{
						orderProductEdit={
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						}
						product=orderProduct.product.product;
						igst=orderProduct.product.product.categories.igst;
						previousissuedQuantity=orderProduct.issuedQuantity;
					}	

				}
				
				var qty=$(this).val();
				if(qty=='' || qty==undefined)
				{
					qty="0";
				}
				if(qty==="0")
				{
					quantityZeroValidation=true;
				}
				else
				{
					quantityZeroValidation=false;
				}
				if(orderStatus==="Packed"){
					//parseInt(previousissuedQuantity)+
					var allowedQty=parseInt(previousissuedQuantity)+qtyAvailable(product.productId);

					if(allowedQty<qty){

						Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(allowedQty), '3000', 'teal lighten-2');

						orderProductDetailsTemp.push(orderProductEdit);
						orderProductDetails=orderProductDetailsTemp;
						$('#qtyFree_'+productId).val(orderProductEdit.issuedQuantity);

						 //edit inventory 								
						productListQtyMngFunc(product.productId);

						findTotal();

						return false;
					}
				}
				var mrp=0;
				var purchaseAmt=0;
				var issuedAmt=0;
				if(orderStatus==="Booked")
				{
					orderProduct=
					{
						"purchaseQuantity" :qty,
						"issuedQuantity":0,
						"rate":product.rate,
						"sellingRate":mrp,
						"igst":igst,
						"purchaseAmount":purchaseAmt.toFixed(2),
						"issueAmount":0,
						//"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"Free"
					};
					$('#purchaseAmtFree_'+id.split('_')[1]).text(purchaseAmt.toFixed(2));
				}
				else
				{
					orderProduct=
					{
						"purchaseQuantity" :0,
						"issuedQuantity":qty,
						"rate":product.rate,
						"sellingRate":mrp,
						"igst":igst,
						"purchaseAmount":0,
						"issueAmount":issuedAmt.toFixed(2),
						//"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"Free"
					};
					$('#issuedAmtFree_'+id.split('_')[1]).text(issuedAmt.toFixed(2));
				}
				
				orderProductDetailsTemp.push(orderProduct);
				
				orderProductDetails=orderProductDetailsTemp;				
				
				 if(orderStatus==="Packed"){
					//edit inventory 
					productListQtyMngFunc(product.productId);
				} 
				findTotal();
			});
		}
		else
		{
			$('#qtyNonFree_'+product.productId).keypress(function( event ){
			    var key = event.which;						    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});
			$('#qtyNonFree_'+product.productId).off("keyup");
			$('#qtyNonFree_'+product.productId).keyup(function(){
				var id=$(this).attr('id')
				var productId=id.split('_')[1];
				var orderProductDetailsTemp=[];
				var previousissuedQuantity;
				var product=fetchProductByProductId(productId);
				var igst=product.categories.igst;
				var srno=1;
				for(var j=0; j<orderProductDetails.length; j++)
				{
					var orderProduct=orderProductDetails[j];
					alert(orderProduct.product.product.productId+"!="+productId);
					if(orderProduct.type==="Free")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
					}	
					else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
					{
						orderProductDetailsTemp.push
						({
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						});
					}	
					else{
						orderProductEdit={
							"purchaseQuantity" :orderProduct.purchaseQuantity,
							"issuedQuantity":orderProduct.issuedQuantity,
							"rate":orderProduct.rate,
							"sellingRate":orderProduct.sellingRate,
							"igst":orderProduct.product.product.categories.igst,
							"purchaseAmount":orderProduct.purchaseAmount,
							"issueAmount":orderProduct.issueAmount,
							//"orderDetails":orderProduct.orderDetails,
							"product":{"product":orderProduct.product.product},
							"type":orderProduct.type
						}
						mrp=orderProduct.sellingRate;
						igst=orderProduct.product.product.categories.igst;
						product=orderProduct.product.product;
						previousissuedQuantity=orderProduct.issuedQuantity;
					}			
				}
				
				var qty=$(this).val();		
				if(qty=='' || qty==undefined)
				{
					qty="0";
				}
				if(qty==="0")
				{
					quantityZeroValidation=true;
				}
				else
				{
					quantityZeroValidation=false;
				}
				if(orderStatus==="Packed"){
					//parseInt(previousissuedQuantity)+
					var allowedQty=parseInt(previousissuedQuantity)+qtyAvailable(product.productId);

					if(allowedQty<qty){

						Materialize.toast('Entered Quantity exceeds Current Quantity, Max Allowed : '+(allowedQty), '3000', 'teal lighten-2');

						orderProductDetailsTemp.push(orderProductEdit);
						orderProductDetails=orderProductDetailsTemp;
						$('#qtyNonFree_'+productId).val(orderProductEdit.issuedQuantity);

						 //edit inventory 								
						productListQtyMngFunc(product.productId);

						findTotal();

						return false;
					}
				}
				var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
				var issuedAmt=parseFloat(mrp)*parseFloat(qty);
				
				if(orderStatus==="Booked")
				{
					orderProduct=
					{
						"purchaseQuantity" :qty,
						"issuedQuantity":0,
						"rate":product.rate,
						"sellingRate":mrp,
						"igst":igst,
						"purchaseAmount":purchaseAmt.toFixed(2),
						"issueAmount":0,
						//"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"NonFree"
					};
					$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixed(2));
				}
				else
				{
					orderProduct=
					{
						"purchaseQuantity" :0,
						"issuedQuantity":qty,
						"rate":product.rate,
						"sellingRate":mrp,
						"igst":igst,
						"purchaseAmount":0,
						"issueAmount":issuedAmt.toFixed(2),
						//"orderDetails":orderProductDetails[0].orderDetails,
						"product":{"product":product},
						"type":"NonFree"
					};
					$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixed(2));
				}
				orderProductDetailsTemp.push(orderProduct);
				
				orderProductDetails=orderProductDetailsTemp;
				
				if(orderStatus==="Packed"){
					//edit inventory 
					productListQtyMngFunc(product.productId);
				}

				findTotal();
			});
		}
		if(orderProduct.type==="Free")
				{
					$('#mrpFree_'+product.productId).keydown(function(e){            	
						-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
					 });
					
					 $('#mrpFree_'+product.productId).keypress(function(e){						
						if (e.keyCode === 46 && this.value.split('.').length === 2) {
							   return false;
						   }		
					});
					$('#mrpFree_'+product.productId).off("keyup");
					$('#mrpFree_'+product.productId).keyup(function(){

						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;
						var product=fetchProductByProductId(productId);	
						var igst=product.categories.igst;
						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}
							else
							{
								igst=orderProduct.product.product.categories.igst;
							}
						}
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="Free")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						var qty=$("#qtyFree_"+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						/* if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$("#mrpFree_"+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixed(2));
								findTotal();
								return false;
							}
						} */
						
						var mrp=0;
						var purchaseAmt=0;
						var issuedAmt=0;
						
						
						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":0,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixed(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixed(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":0,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixed(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixed(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						
						findTotal();

					});
				}else{
					$('#mrpNonFree_'+product.productId).keydown(function(e){            	
						-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
					 });
					
					 $('#mrpFree_'+product.productId).keypress(function(e){						
						if (e.keyCode === 46 && this.value.split('.').length === 2) {
							   return false;
						   }		
					});
					$('#mrpNonFree_'+product.productId).off("keyup");
					$('#mrpNonFree_'+product.productId).keyup(function(){
						var id=$(this).attr('id')
						var productId=id.split('_')[1];
						var orderProductDetailsTemp=[];					
						var orderProductEdit;				
						var mrp=$('#mrpNonFree_'+productId).val();
						var product=fetchProductByProductId(productId);	
						var igst=product.categories.igst;
						if(mrp=='' || mrp==undefined)
						{
							mrp="0";
						}

						var srno=1;
						for(var j=0; j<orderProductDetails.length; j++)
						{
							var orderProduct=orderProductDetails[j];
							alert(orderProduct.product.product.productId+"!="+productId);
							if(orderProduct.type==="Free")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
								orderProductDetailsTemp.push
								({
									"purchaseQuantity" :orderProduct.purchaseQuantity,
									"issuedQuantity":orderProduct.issuedQuantity,
									"rate":orderProduct.rate,
									"sellingRate":orderProduct.sellingRate,
									"igst":orderProduct.product.product.categories.igst,
									"purchaseAmount":orderProduct.purchaseAmount,
									"issueAmount":orderProduct.issueAmount,
									//"orderDetails":orderProduct.orderDetails,
									"product":{"product":orderProduct.product.product},
									"type":orderProduct.type
								});
							}else{
								igst=orderProduct.product.product.categories.igst;
							}
						}
						for(var j=0; j<orderProductDetailsForIssueCheck.length; j++)
						{
							var orderProduct=orderProductDetailsForIssueCheck[j];
							if(orderProduct.type==="Free")
							{
							}	
							else if(orderProduct.product.product.productId!=productId && orderProduct.type==="NonFree")
							{
							}	
							else
							{
								orderProductEdit={
										"purchaseQuantity" :orderProduct.purchaseQuantity,
										"issuedQuantity":orderProduct.issuedQuantity,
										"rate":orderProduct.rate,
										"sellingRate":orderProduct.sellingRate,
										"igst":orderProduct.product.product.categories.igst,
										"purchaseAmount":orderProduct.purchaseAmount,
										"issueAmount":orderProduct.issueAmount,
										//"orderDetails":orderProduct.orderDetails,
										"product":{"product":orderProduct.product.product},
										"type":orderProduct.type
									}
								product=orderProduct.product.product;
							}
						}
						var qty=$("#qtyNonFree_"+productId).val();	
						if(qty=='' || qty==undefined)
						{
							qty="0";
						}
						if(qty==="0")
						{
							quantityZeroValidation=true;
						}
						else
						{
							quantityZeroValidation=false;
						}
						/*if(orderStatus==="Issued")
						{
							if(orderProductEdit.issuedQuantity<qty)
							{
								Materialize.toast('Issued Quantity need to be same or below', '3000', 'teal lighten-2');
								
								orderProductDetailsTemp.push(orderProductEdit);
								orderProductDetails=orderProductDetailsTemp;
								$("#mrpNonFree_"+productId).val(orderProductEdit.issuedQuantity);
								$('#issuedAmtNonFree_'+id.split('_')[1]).text(orderProductEdit.issueAmount.toFixed(2));
								findTotal();
								return false;
							}
						}*/
						
						var purchaseAmt=parseFloat(mrp)*parseFloat(qty);
						var issuedAmt=parseFloat(mrp)*parseFloat(qty);
						
						//its function defined in calculateProperAmount.js 
						var calculateProperTaxObj=calculateProperTax(mrp,igst);

						var orderProduct;
						if(orderStatus==="Booked")
						{
							orderProduct=
							{
								"purchaseQuantity" :qty,
								"issuedQuantity":0,
								"rate":calculateProperTaxObj.unitPrice,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":purchaseAmt.toFixed(2),
								"issueAmount":0,
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#purchaseAmtNonFree_'+id.split('_')[1]).text(purchaseAmt.toFixed(2));
						}
						else
						{
							orderProduct=
							{
								"purchaseQuantity" :0,
								"issuedQuantity":qty,
								"rate":calculateProperTaxObj.unitPrice,
								"sellingRate":mrp,
								"igst":igst,
								"purchaseAmount":0,
								"issueAmount":issuedAmt.toFixed(2),
								//"orderDetails":orderProductDetails[0].orderDetails,
								"product":{"product":product},
								"type":"NonFree"
							};
							$('#issuedAmtNonFree_'+id.split('_')[1]).text(issuedAmt.toFixed(2));
						}
						orderProductDetailsTemp.push(orderProduct);
						
						orderProductDetails=orderProductDetailsTemp;
						
						findTotal();
					});
				}
		$('#qty').val('');
		$('#curr_qty').val('');
		var source=$('#productid');
		source.val(0);
		source.change();
		$('#nonFreeId').click();
	});	
});

function findTotal()
{
	var totalQuantity=0;
	var totalAmount=0;
	if(orderStatus==="Booked")
	{
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			totalQuantity+=parseInt(orderProduct.purchaseQuantity);
			totalAmount+=parseInt(orderProduct.purchaseAmount);
		}
	}
	else
	{
		for(var j=0; j<orderProductDetails.length; j++)
		{
			var orderProduct=orderProductDetails[j];
			totalQuantity+=parseInt(orderProduct.issuedQuantity);
			totalAmount+=parseInt(orderProduct.issueAmount);
		}
	}
	
	$('#totalQuantityId').text(parseInt(totalQuantity));
	$('#totalAmountId').text(totalAmount.toFixed(2));
}
//var firstTimeProductDataFetch=true
/* function productListData(){

	var productList;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductListAjax",
		dataType : "json",
		async : false,
		success:function(data)
		{
			productList=data;
			
			//$('#productid').change();
			//console.log("product data list loaded");
		},
		error: function(xhr, status, error) {
			console.log("product data list not loaded");
		}
	});
	return productList;
} */

 function fetchProductByProductId(productId){
	
	var product;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductByProductId?productId="+productId,
		dataType : "json",
		async : false,
		success:function(data)
		{
			product=data;

			/* //for manage invt qty
			if(firstTimeProductDataFetch){
				productListQtyMng=data;
				firstTimeProductDataFetch=false;
			} */
			
			//$('#productid').change();
			//console.log("product data list loaded");
		},
		error: function(xhr, status, error) {
			console.log("product data list not loaded");
		}
	});
	return product;
}

function filterProductList(brandId,categoryId)
{
	var select = document.getElementById('productid');
	select.options.length = 0;
	select.options.add(new Option("Choose Product", 0));
	var options, index, option;

	var productList;

	$.ajax({
		type:"GET",
		url : myContextPath+"/fetchProductByBrandAndCategoryIdWeb?brandId="+brandId+"&categoryId="+categoryId,
		dataType : "json",
		async : false,
		success:function(data){

			productList=data;

			for(var i=0; i<productList.length; i++){				
				product=productList[i];
				if(product.brand.brandId==brandId)
				{
					select.options.add(new Option(product.productName, product.productId));
				}			
			}
			
			$('#productid').change();
			//console.log("product data list loaded");
		},
		error: function(xhr, status, error) {
			console.log("product data list not loaded");
		}
	});
	return productList;

	/* if(brandId!=="0" && categoryId==="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.brand.brandId==brandId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else if(categoryId!=="0" && brandId==="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.categories.categoryId==categoryId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else if(brandId!=="0" && categoryId!=="0")
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			if(product.categories.categoryId==categoryId && product.brand.brandId==brandId)
			{
				select.options.add(new Option(product.productName, product.productId));
			}			
		}
	}
	else
	{
		for(var i=0; i<productList.length; i++)
		{
			product=productList[i];
			select.options.add(new Option(product.productName, product.productId));
		}
	} */
}