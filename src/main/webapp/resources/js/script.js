 $(document).ready(function() {
	 $('.preloader-background').hide();
     $(".button-collapse").sideNav();
     $('.collapsible').collapsible();
     $('select').material_select();

     // for HTML5 "required" attribute
     $("select[required]").css({
       display: "inline",
       height: 0,
       margin:0,
       padding: 0,
       width: 0
     });
     $('.modal').modal({
         dismissible: true, // Modal can be dismissed by clicking outside of the modal
         ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
           /*alert("Ready");
           console.log(modal, trigger);*/
         },
         complete: function() { Materialize.Toast.removeAll(); } // Callback for Modal close
       });
     $('#slide').click(function() {
    	 $(this).toggleClass("reveal-open");
    	
         //$(this).toggleClass("sidebutton");
         // hides all heading and shows only icons
         $('#logo-collapse').toggle();
         $('#logo-collapse-full').toggle();
         $('.element-sideNav').toggle();

         //hide logo 
         // $('.logo').toggle();
         // half closes side nav
         $('.side-nav').toggleClass("reveal-open");
         // remove left padding of body
         $('main').toggleClass("paddingBody");
        
         // starts body content after half side nav ends
         $('main').toggleClass("intro");
         /*for setting width to 100% to prevent misaligned of table header and body*/
         //$.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
         $($.fn.dataTable.tables({visible: true, api: true})).DataTable().columns.adjust();
    	 $('.dataTables_scrollHeadInner,.dataTables_scrollHeadInner table').width("100%");
     });
     //  for no of entries and global search
     $('#tblData').DataTable({
         "oLanguage": {
             "sLengthMenu": "Show:  _MENU_",
             "sSearch": " _INPUT_" //search
         },
         columnDefs: [
                      { 'width': '1%', 'targets': 0 }
                      ],
         lengthMenu: [
             [10, 25., 50, -1],
             ['10 ', '25 ', '50 ', 'All']
         ],
        autoWidth: false,
         
        // dom: 'lBfrtip',
        dom:'<lBfr<"scrollDivTable"t>ip>',
         buttons: {
             buttons: [
                 //      {
                 //      extend: 'pageLength',
                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                 //  }, 
                 {
                     extend: 'pdf',
                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                     customize: function(doc, config) {
                         var tableNode;
                         for (i = 0; i < doc.content.length; ++i) {
                           if(doc.content[i].table !== undefined){
                             tableNode = doc.content[i];
                             break;
                           }
                         }
        
                         var rowIndex = 0;
                         var tableColumnCount = tableNode.table.body[rowIndex].length;
                          
                         if(tableColumnCount > 6){
                           doc.pageOrientation = 'landscape';
                         }
                         /*for customize the pdf content*/ 
                         doc.pageMargins = [5,20,10,5];                         
                         doc.defaultStyle.fontSize = 8;
                         doc.styles.title.fontSize = 12;
                         doc.styles.tableHeader.fontSize = 11;
                         doc.styles.tableFooter.fontSize = 11;
                         doc.styles.tableHeader.alignment = 'center';
                         doc.styles.tableBodyEven.alignment = 'center';
                         doc.styles.tableBodyOdd.alignment = 'center';
                       },
                      
               
                 },
                 {
                     extend: 'excel',
                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'print',
                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                     //title of the page
                     title: function() {
                         var name = $(".heading").text();
                         return name
                     },
                     //file name 
                     filename: function() {
                         var d = new Date();
                         var date = d.getDate();
                         var month = d.getMonth();
                         var year = d.getFullYear();
                         var name = $(".heading").text();
                         return name + date + '-' + month + '-' + year;
                     },
                     //  exports only dataColumn
                     exportOptions: {
                         columns: ':visible.print-col'
                     },
                 },
                 {
                     extend: 'colvis',
                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                     collectionLayout: 'fixed two-column',
                     align: 'left'
                 },
             ]
         }

     });
     //  $('.checkAll').on('click', function() {
     //      $(this).closest('table').find('tbody :checkbox')
     //          .prop('checked', this.checked)
     //          .closest('tr').toggleClass('selected', this.checked);
     //  });

     //  $('tbody :checkbox').on('click', function() {
     //      $(this).closest('tr').toggleClass('selected', this.checked);

     //      $(this).closest('table').find('.checkAll').prop('checked', ($(this).closest('table').find('tbody :checkbox:checked').length == $(this).closest('table').find('tbody :checkbox').length)); //Tira / coloca a seleção no .checkAll
     //  });


     //for add placeholder in dataTable search 
     $('.dataTables_filter input').attr("placeholder", "  Search");
     
     /*for finding yesterday date*/
    var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
    $('.disableDate').pickadate({
   	 	container: 'body',
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        format: 'yyyy-mm-dd',
        closeOnSelect: 'true', // Close upon selecting a date,
        
        // this function closes after date selection Close upon
        // selecting a date,
        onSet: function(ele) {
            if (ele.select) {
                this.close();
            }
        },
        onStart: function() {        	 
    	    var date = new Date();
    	    this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);
    	   
    	},
        disable: [
                  { from: [0,0,0], to: yesterday }

                  
                ]
    	});
     $('.datepicker').pickadate({
    	 container: 'body',
         selectMonths: true, // Creates a dropdown to control month
         selectYears: 15, // Creates a dropdown of 15 years to control year,
         today: 'Today',
         clear: 'Clear',
         close: 'Ok',
         format: 'yyyy-mm-dd',
         closeOnSelect: 'true', // Close upon selecting a date,
         
         // this function closes after date selection Close upon
         // selecting a date,
         onSet: function(ele) {
             if (ele.select) {
                 this.close();
             }
         },
         onStart: function() {        	 
     	    var date = new Date();
     	    this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);
     	   
     	}
         
     	});
     
    /* var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);   
	   var dateDisable = $('.datepicker').pickadate({
		     container: 'body',
	         selectMonths: true, // Creates a dropdown to control month
	         selectYears: 15, // Creates a dropdown of 15 years to control year,
	         today: 'Today',
	         clear: 'Clear',
	         close: 'Ok',
	         format: 'yyyy-mm-dd',
	         closeOnSelect: 'true', // Close upon selecting a date,
	         
	         // this function closes after date selection Close upon
	         // selecting a date,
	         onSet: function(ele) {
	             if (ele.select) {
	                 this.close();
	             }
	         },
	         onStart: function() {        	 
	     	    var date = new Date();
	     	    this.set('select', [date.getFullYear(), date.getMonth(), date.getDate()]);
	     	   
	     	},
		   disable: [
	                   { from: [0,0,0], to: yesterday }

	                   
	                 ]
		   
	   });*/
     // show hide Range dates
     $(".showDates").hide();
     $(".rangeSelect").click(function() {
    
         $(".showDates").show();
     });
     $(".showDates1").hide();
     $(".rangeSelect1").click(function() {

         $(".showDates1").show();
     });
     
     $('select').material_select();
     $("select")
         .change(function() {
             var t = this;
             var content = $(this).siblings('ul').detach();
             setTimeout(function() {
                 $(t).parent().append(content);
                 $("select").material_select();
             }, 200);
         });
    
     $('#mobileNoSms').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13) )
		        event.preventDefault();
		});
     $("#mobileEditSms").change(function() {
		    if(this.checked) {
		    	$("#mobileNoSms").removeAttr('readonly');
		    }
		    else{
		    	 $("#mobileNoSms").attr('readonly','readonly');
		    	
		    }
		});
     $('.select2').material_select('destroy');
		$('.select2').select2({
			placeholder : "Choose your Product",
			allowClear : true
		});
		$(".select2").select2({
			width : 'resolve'
		});
     //  $(".status").click(function() {

     //      if (confirm("Are you sure you want to change status?")) {
     //          ($(this).text() === "Active") ? $(this).text("Inactive").css("color", "red"): $(this).text("Active").css("color", "green");
     //      } else {
     //          return false;
     //      }

     //  });
     //  $(".membership").click(function() {
     //      if (confirm("Are you sure you want to change Membership?")) {
     //          ($(this).text() === "General") ? $(this).text("Featured").css("color", "#ff6f00"): $(this).text("General").css("color", "blue");
     //      } else {
     //          return false;
     //      }
     //  });


     //     $('#text1').trigger('autoresize');

 });