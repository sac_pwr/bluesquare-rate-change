
var addUpdate=true;
$(document).ready(function(){
	
	$("#resetExpenseSubmit").click(function(){
		resetForm();
	});
	
	$('#saveExpenseTypeSubmit').click(function(){
		
		var expenseTypeName=$('#name').val();
		if(expenseTypeName=='' || expenseTypeName==undefined){
			Materialize.toast('Enter Expense Type Name', '2000', 'teal lighten-2');
			return false;
		}
		var expenseTypeId=$('#expenseTypeId').val();
				
		if(checkExpenseTypeAlreadyExistForAddUpdate(expenseTypeName,expenseTypeId)!=true){
			return false;
		}
		
		var url;
		if(addUpdate){
			url=baseUrl+"/save_expense_type";
		}else{
			url=baseUrl+"/update_expense_type";
		}
		
		$.ajax({
			type:'POST',
			url: url,
			async: false,
			data:{
				name:expenseTypeName,
				expenseTypeId:expenseTypeId
			},
			success : function(data){
					if(data=="Success"){
						if(addUpdate){
							Materialize.toast('ExpenseType Added SuccessFully', '2000', 'teal lighten-2');
						}else{
							Materialize.toast('ExpenseType Updated SuccessFully', '2000', 'teal lighten-2');
						}						
						resetForm();
						refreshExpenseTypeList();
					}else{
						if(addUpdate){
							Materialize.toast('ExpenseType Added Failed', '2000', 'red lighten-2');
						}else{
							Materialize.toast('ExpenseType Updated Failed', '2000', 'red lighten-2');
						}
					}
			},
			error: function(xhr, status, error){
				Materialize.toast('Something Went Wrong', '2000', 'red lighten-2');
			}		
		});
		
	});
	
	
	refreshExpenseTypeList();
});

function checkExpenseTypeAlreadyExistForAddUpdate(expenseTypeName,expenseTypeId){
	
	var url;
	
	if(addUpdate){
		url=baseUrl+"/expense_type_check_for_add";
	}else{
		url=baseUrl+"/expense_type_check_for_update";
	}
	
	var checkExpenseType=true;
	
	$.ajax({
		type:'POST',
		url: url,
		async: false,
		data:{
			name:expenseTypeName,
			expenseTypeId:expenseTypeId
		},
		success : function(data){
				if(data=="Success"){
					checkExpenseType=true;
				}else{
					checkExpenseType=false;
					Materialize.toast('ExpenseType Already Exist', '2000', 'teal lighten-2');
				}
		},
		error: function(xhr, status, error){
			Materialize.toast('Checking ExpenseType Failed', '2000', 'red lighten-2');
		}		
	});
	
	return checkExpenseType;
}

function resetForm(){
	$('#name').val('');
	$('#expenseTypeId').val('0');
	$("#saveExpenseTypeSubmit").html('<i class="material-icons left">add</i>Add');
	addUpdate=true;
}

function refreshExpenseTypeList(){
	$('#deleteModals').empty();
	var t = $('#tblData').DataTable();
	    t.clear().draw();
	    
	 $.ajax({
			type:'GET',
			url: baseUrl+"/fetchExpenseTypeList",
			async: false,
			success : function(data){
				if(data==''){
					Materialize.toast('Expense Type List Not Found', '2000', 'teal lighten-2');
					return false;
				}
				expenseTypeList=data;
				
				for(var i=0; i<expenseTypeList.length; i++){
					expenseType=expenseTypeList[i];
					
					var dateAddedString = moment(expenseType.addedDate).format("DD-MM-YYYY")
					var timeAddedString = moment(expenseType.addedDate).format("hh:mm:ss")
					
					var dateUpdatedString;
					var timeUpdatedString;
					if(expenseType.updatedDate!='' && expenseType.updatedDate!=undefined){
						dateUpdatedString = moment(expenseType.updatedDate).format("DD-MM-YYYY")
						timeUpdatedString = moment(expenseType.updatedDate).format("hh:mm:ss")
					}else{
						dateUpdatedString='NA';
						timeUpdatedString="NA";
					}
					
					t.row.add([
					           (i+1),
					           expenseType.name,
					           dateAddedString+"<br>"+timeAddedString,
					           dateUpdatedString+"<br>"+timeUpdatedString,
					           
					           '<button onclick="editExpenseType('+expenseType.expenseTypeId+')" class="btn-flat"><i class="material-icons tooltipped" area-hidden="true" data-position="right" data-delay="50"'+ 
						       'data-tooltip="Edit" >edit</i></button>'+ 
						       '<a href="#delete'+expenseType.expenseTypeId+'" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" '+
						       'data-delay="50" data-tooltip="Delete">delete</i></a>'
					           
					           ]).draw(false);;
					
					$('#deleteModals').append('<div id="delete'+expenseType.expenseTypeId+'" class="modal deleteModal row">'+
									                '<div class="modal-content  col s12 m12 l12">'+
									                	'<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>'+
									                    '<h5 class="center-align">'+
									                	'Do you  want to Delete'+
														'</h5>'+
									                    '<br/>'+
									                 '</div>'+
									                '<div class="modal-footer">'+
									 				    '<div class="col s6 m6 l3 offset-l3">'+
									         				'<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>'+
										    			'</div>'+
														'<div class="col s6 m6 l3">'+
										 				   '<a href="#" onclick="deleteExpenseType('+expenseType.expenseTypeId+')"'+
									                       'class="modal-action modal-close waves-effect  btn">'+
									                       'Delete'+
									                       '</a>'+
										   				'</div>'+    
									    			'</div>'+
									            '</div>');
				}
				$('.modal').modal();
				
			},
			error: function(xhr, status, error){
				Materialize.toast('Expense Type List Not Found', '2000', 'red lighten-2');
			}
			
		});
}

function editExpenseType(expenseTypeId){
	addUpdate=false;
	$.ajax({
		type:'POST',
		url: baseUrl+"/fetchExpenseType",
		async: false,
		data:{
			expenseTypeId:expenseTypeId
		},
		success : function(data){
			$('#name').val(data.name);
			$('#name').change();
			$('#expenseTypeId').val(data.expenseTypeId);
			$("#saveExpenseTypeSubmit").html('<i class="material-icons left">send</i>Update');
		},
		error: function(xhr, status, error){
			Materialize.toast('Fetching ExpenseType Failed', '2000', 'red lighten-2');
		}		
	});
}

function deleteExpenseType(expenseTypeId){
	resetForm();
	$.ajax({
		type:'POST',
		url: baseUrl+"/delete_expense_type",
		async: false,
		data:{
			expenseTypeId:expenseTypeId
		},
		success : function(data){
			if(data=="Success"){
				Materialize.toast('Expense Type Deleted Successfully', '2000', 'teal lighten-2');
				refreshExpenseTypeList();
			}else{
				Materialize.toast('ExpenseType Deleting Failed', '2000', 'red lighten-2');
			}
		},
		error: function(xhr, status, error){
			Materialize.toast('ExpenseType Deleting Failed', '2000', 'red lighten-2');
		}		
	});
	
}