    
var orderSupplierList = [];
var orderSupplierListTemp = [];
var count=1;
$(document).ready(function() 
{    
    $('#addOrderProduct').click(function(){
		
    	var productIdForOrder=$('#productIdForOrder').val();
		if(productIdForOrder==="0")
		{
			Materialize.toast('Please Select Product!', '4000', 'teal lighten-3');
			/*$('#addeditmsg').find("#modalType").addClass("warning");
			 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
			$('#addeditmsg').modal('open');
   	     	//$('#msgHead').text("Add Product in Cart Warning :");
   	     	$('#msg').text("Select Product"); */
   	     	return false;
		}
    	
		var buttonStatus=$('#addOrderProduct').text();
    	if(buttonStatus==="Update")
    	{
    		//alert("going update : "+$('#currentUpdateRowId').val());
    		updaterow($('#currentUpdateRowId').val());2
    		return false;
    	}
		
		var supplierIdForOrder=$('#supplierIdForOrder').val();
		var orderQuantity=$('#orderQuantity').val();
		var supplierMobileNumber=$('#supplierMobileNumber').val();
		//alert("'"+supplierIdForOrder+"' - '"+productIdForOrder+"'");
	
		if(supplierMobileNumber==="" || supplierMobileNumber===undefined || !(/^\d{10}$/.test(supplierMobileNumber)))
		{
			Materialize.toast('Supplier mobile number not valid!', '4000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Add Product in Cart Warning :");
   	     	$('#msg').text("Supplier mobile number not valid"); */
   	     	return false;
		}
		if(orderQuantity==="0" || orderQuantity==="" || orderQuantity===undefined)
		{
			Materialize.toast('Order Quantity not valid!', '4000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Add Product in Cart Warning :");
   	     	$('#msg').text("Order Quantity not valid"); */
   	     	return false;
		}
		
		for (var i=0; i<orderSupplierList.length; i++) {
  			var value=orderSupplierList[i];
  			if(value[0]===supplierIdForOrder && value[1] === productIdForOrder)
  			{
  				//alert("dfd");
  				Materialize.toast('This Product and Supplier is already added to Cart!', '4000', 'teal lighten-3');
  				/*$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Product Select Warning");
       	     	$('#msg').text("This Product and Supplier is already added to Cart");*/ 
       	     	return false;
  			}
  		}
		//alert(orderSupplierList.entries());
		//orderProductAndQuantityList.put(productIdForOrder,orderQuantity);
		var prodctlst = [supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber];
		orderSupplierList.push(prodctlst);
		$('#orderentriesId').val(orderSupplierList);
		//alert(orderSupplierList.entries());
		var supplierName=$('#supplierIdForOrder option:selected').text();
		var productName=$('#productIdForOrder option:selected').text();
		var supplierRate=$('#supplierRateForOrder').val();
		var supplierMRPRateForOrder=$('#supplierMRPRateForOrder').val();
		
		$("#orderCartTb").append(	"<tr id='rowdel_" + count + "' >"+
		                            "<td id='rowcount_" + count + "'>"+count+"</td>"+
		                            "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+supplierIdForOrder+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+supplierMobileNumber+"><span id='rowsuppliernametb_"+count+"'>"+supplierName+"</span></td>"+
		                            "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+productIdForOrder+"><span id='rowproductnametb_"+count+"'>"+productName+"</span></td>"+
		                            "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+supplierRate+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+supplierMRPRateForOrder+">"+supplierMRPRateForOrder+"</td>"+
		                            "<td id='roworderquantity_"+count+"'>"+orderQuantity+"</td>"+
		                            "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
		                            "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
		                        	"</tr>");
		
		count++;
		//alert(orderSupplierList.entries());
		//resetOrderSelectionData();
		$('#orderQuantity').val('');
		var source = $("#productIdForOrder");
		source.val(0);
		source.change();
		
		$('#supplierRateForOrder').val('');
		$('#supplierMRPRateForOrder').val('');
	}); 
    $('#supplierMobileNumber').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
    /*$('#orderProductsButton').click(function(){
		//alert(orderSupplierList.entries());
		
    	
    	
    	if(orderSupplierList.length==0){
    		Materialize.toast('Add Some Product For Order', '4000', 'teal lighten-3');
    		return false;
    	}
    	
    	var productIdList="";
    	for (var i=0; i<orderSupplierList.length; i++) {
    		var value=orderSupplierList[i];
    		productIdList=productIdList+value[0]+"-"+value[1]+"-"+value[2]+"-"+value[3]+",";
  		}
    	productIdList=productIdList.slice(0,-1)
    	//alert(productIdList);
    	$('#productWithSupplikerlist').val(productIdList);
    	
    	$("#orderProductsButton").attr('disabled','disabled');
    	var form = $('#editOrderBookForm');

		$.ajax({
					type : form.attr('method'),
					url : form.attr('action'),
					data : $("#editOrderBookForm").serialize(),
					success : function(data) {
						if(data=="Success")
						{
								$('#add').modal('close');
								Materialize.toast('Order Edited SuccessFully!', '4000', 'teal lighten-3');
								$('#addeditmsg').modal('open');
								$('#msgHead').text("Success : ");
								$('#msg').html("Order Edited SuccessFully");
								setTimeout(function() 
								  {
									location.reload();
								  }, 1000);
								
						}
						else
						{
							$("#orderProductsButton").removeAttr("disabled");
							Materialize.toast('Failed to edit order!', '4000', 'teal lighten-3');
								$('#addeditmsg').modal('open');
								$('#msgHead').text("Failed : ");
								$('#msg').html("<font color='red'>Order Editing Failed</font>");
						}
					}
		});
		
	});*/
    
    $('#supplierIdForOrder').change(function(){
		var supplierId=$('#supplierIdForOrder').val();
		$("#brandIdForOrder").change();
		/*$('#productIdForOrder').empty();
		$("#productIdForOrder").append('<option value="0">Choose Product</option>');
		
		$.ajax({
			url : myContextPath+"/fetchProductBySupplierId?supplierId="+supplierId,
			dataType : "json",
			success : function(data) {
				
				for(var i=0; i<data.length; i++)
				{								
					//alert(data[i].productId +"-"+ data[i].productName);
					$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
				}	
			},
			error: function(xhr, status, error) {
				  //var err = eval("(" + xhr.responseText + ")");
				  //alert("Error");
				}
		});*/
		
		
		var supplierId=$('#supplierIdForOrder').val();
		$('#supplierMobileNumber').val('');
		$('#supplierMobileNumber').change();
		if(supplierId==="0")
		{
			return false;
		}
		$.ajax({
			url : myContextPath+"/fetchSupplierBySupplierId?supplierId="+supplierId,
			dataType : "json",
			success : function(data) {
				
				$('#supplierMobileNumber').val(data.contact.mobileNumber);
				$('#supplierMobileNumber').change();
				
			},
			error: function(xhr, status, error) {
				  //var err = eval("(" + xhr.responseText + ")");
				  //alert("Error");
				}
		});
	});
    
    $('#categoryIdForOrder').change(function(){
		
		var categoryId=$('#categoryIdForOrder').val();
		var brandId=$('#brandIdForOrder').val();
		var supplierId=$('#supplierIdForOrder').val();
		
		$('#productIdForOrder').empty();
		$("#productIdForOrder").append('<option value="0">Choose Product</option>');
		$.ajax({ 
			url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
			dataType : "json",
			async :false,
			success : function(data) {
				
					for(var i=0; i<data.length; i++)
					{								
						//alert(data[i].productId +"-"+ data[i].productName);
						$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
					}	
					//alert("done");
					$("#productIdForOrder").change();
				
				},
				error: function(xhr, status, error) {
					  //var err = eval("(" + xhr.responseText + ")");
					//alert("Error ");
				}
			});
		
	});
    
    $('#brandIdForOrder').change(function(){
		
		var categoryId=$('#categoryIdForOrder').val();
		var brandId=$('#brandIdForOrder').val();
		var supplierId=$('#supplierIdForOrder').val();
		
		$('#productIdForOrder').empty();
		$("#productIdForOrder").append('<option value="0">Choose Product</option>');
		$.ajax({ 
			url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
			dataType : "json",
			async :false,
			success : function(data) {
				
				for(var i=0; i<data.length; i++)
				{								
					//alert(data[i].productId +"-"+ data[i].productName);
					$("#productIdForOrder").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
				}	
				//alert("done");
				$("#productIdForOrder").change();
				
				},
				error: function(xhr, status, error) {
					  //var err = eval("(" + xhr.responseText + ")");
					//alert("Error ");
				}
			});
		
	});
    
    $('#productIdForOrder').change(function(){
		var productId=$('#productIdForOrder').val();
		var supplierId=$('#supplierIdForOrder').val();
		$.ajax({
			url : myContextPath+"/fetchSupplierById?supplierId="+supplierId+"&productId="+productId,
			dataType : "json",
			success : function(data) {
				
				var supplierproduct=data;
				var mrp=((parseFloat(supplierproduct.supplierRate))+((parseFloat(supplierproduct.supplierRate)*parseFloat(supplierproduct.product.categories.igst))/100)).toFixed(0);
				var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierproduct.product.categories.igst));
				
				$('#supplierMRPRateForOrder').val(correctAmoutWithTaxObj.mrp);
				$('#supplierMRPRateForOrder').change();
				
				$('#supplierRateForOrder').val(parseFloat(data.supplierRate).toFixed(2));
				$('#supplierRateForOrder').change();
			},
			error: function(xhr, status, error) {
				  //var err = eval("(" + xhr.responseText + ")");
				  //alert("Error ");
				}
		});
		
	});
    
    $("#OrderMobileNumberchecker").change(function() {
	    var ischecked= $(this).is(':checked');
	    if(ischecked)
	    {
	    	$('#supplierMobileNumber').attr('readonly', false);
	    	$('#supplierMobileNumber').removeClass("grey lighten-3");
	    }
	    else
	    {
	    	$('#supplierMobileNumber').addClass("grey lighten-3");
	    	$('#supplierMobileNumber').attr('readonly', true);
	    }
	});
	$('#OrderMobileNumberchecker').prop('checked', false);
	$('#supplierMobileNumber').attr('readonly', true);
});

function resetOrderSelectionData()
{
	$('#supplierMobileNumber').val('');
	$('#supplierRateForOrder').val('');
	$('#supplierMRPRateForOrder').val('');
	$('#orderQuantity').val('');
	$('#OrderMobileNumberchecker').prop('checked', false);
	$('#supplierMobileNumber').attr('readonly', true); 
	$("#addOrderProduct").text("Add");	
	
	var source1 = $("#supplierIdForOrder");
	source1.val(0);
	source1.change();
	
	setTimeout(
			  function() 
			  {
				  var source = $("#productIdForOrder");
					source.val(0);
					source.change();
			  }, 1000);
	
	var source2 = $("#categoryIdForOrder");
	source2.val(0);
	source2.change();
	
	var source3 = $("#brandIdForOrder");
	source3.val(0);
	source3.change();
}

    function editOrder(supplierOrderIddd)
    {
    	$("#orderProductsButton").removeAttr("disabled");
    	$('#supplierOrderIdId').val(supplierOrderIddd);
    	resetOrderSelectionData();
    	orderSupplierList=[];
    	
    	$.ajax({
    		url : myContextPath+"/fetchBrandAndCategoryList",
    		dataType : "json",
    		async:false,
    		success : function(data) {
    			categoryList=data.categoryList;
    			$('#categoryIdForOrder').empty();
    			$("#categoryIdForOrder").append('<option value="0">Choose Category</option>');
    			for(var i=0; i<categoryList.length; i++)
    			{
    				$("#categoryIdForOrder").append('<option value='+categoryList[i].categoryId+'>'+categoryList[i].categoryName+'</option>');
    			}	
    			$("#categoryIdForOrder").change();
    			/*if(selectProduct==true){
    				var source=$('#categoryIdForOrder');
    				source.val(v);
    				source.change();
    			}*/
    			
    			brandList=data.brandList;
    			$('#brandIdForOrder').empty();
    			$("#brandIdForOrder").append('<option value="0">Choose Brand</option>');
    			for(var j=0; j<brandList.length; j++)
    			{
    				$("#brandIdForOrder").append('<option value='+brandList[j].brandId+'>'+brandList[j].name+'</option>');
    			}	
    			$("#brandIdForOrder").change();
    			
    			
    			supplierList=data.supplierList;
    			$('#supplierIdForOrder').empty();
    			$("#supplierIdForOrder").append('<option value="0">Choose Supplier</option>');
    			for(var k=0; k<supplierList.length; k++)
    			{
    				$("#supplierIdForOrder").append('<option value='+supplierList[k].supplierId+'>'+supplierList[k].name+'</option>');
    			}	
    			$("#supplierIdForOrder").change();
    			
    	
    		}
    	});
    	
    	
    	if(supplierOrderIddd!=null && supplierOrderIddd!=undefined )
		{
			$.ajax({
				url : myContextPath+"/editSupplierOrderAjax?supplierOrderId="+supplierOrderIddd,
				dataType : "json",
				async :false,
				success : function(data) {
					
					count=1;
					$("#orderCartTb").html('');
					for(var i=0; i<data.length; i++)
					{
						var supplierOrderDetaillist=data[i];
						var supplierIdForOrder=supplierOrderDetaillist.supplier.supplierId;
						var supplierMobileNumber=supplierOrderDetaillist.supplier.contact.mobileNumber;
						var supplierName=supplierOrderDetaillist.supplier.name;
						var productIdForOrder=supplierOrderDetaillist.product.product.productId;
						var productName=supplierOrderDetaillist.product.productName;
						var supplierRate=supplierOrderDetaillist.supplierRate;
						var orderQuantity=supplierOrderDetaillist.quantity;
						
						var mrp=((parseFloat(supplierRate))+((parseFloat(supplierRate)*parseFloat(supplierOrderDetaillist.product.categories.igst))/100)).toFixed(0);
    					var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierOrderDetaillist.product.categories.igst));
						
						var prodctlst = [supplierIdForOrder,productIdForOrder.toString(), orderQuantity.toString(),supplierMobileNumber];
						orderSupplierList.push(prodctlst);
						
						$("#orderCartTb").append("<tr id='rowdel_" + count + "' >"+
					                             "<td id='rowcount_" + count + "'>"+count+"</td>"+
					                             "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+supplierIdForOrder+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+supplierMobileNumber+"><span id='rowsuppliernametb_"+count+"'>"+supplierName+"</span></td>"+
					                             "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+productIdForOrder+"><span id='rowproductnametb_"+count+"'>"+productName+"</span></td>"+
					                             "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+supplierRate+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+correctAmoutWithTaxObj.mrp+">"+correctAmoutWithTaxObj.mrp+"</td>"+
					                             "<td id='roworderquantity_"+count+"'>"+orderQuantity+"</td>"+
					                             "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
					                             "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
					                        	 "</tr>");
						count++;
					}
					
				},
				error: function(xhr, status, error) {
					 // alert("Error");
				}
			});
		}
		//alert(orderSupplierList.entries());
		//alert(count);
		$('.modal').modal();
		$('#order').modal('open');
    }
    
    function showOrderDetails(id)
	   {
    	$('#orderentriesId').val(orderSupplierList);
		   $.ajax({
				url : myContextPath+"/fetchProductBySupplierOrderId?supplierOrderId="+id,
				dataType : "json",
				success : function(data) {
				//alert(data);
				var totalAll=0,totalAmtWithTaxAll=0;
				    $("#productOrderedDATA").empty();
					var srno=1;
					for (var i = 0, len = data.length; i < len; ++i) {
						var supplierOrderDetail = data[i];
						
						/* 
						<tr>
                            <td>1</td>
                            <td>Mouse</td>
                            <td>I.T</td>
                            <td>BlueSquare</td>
                            <td>5</td>
                            <td>350</td>
                            <th>Total Amount</th>
                            <th>Total Amount With Tax</th>
                            </tr>
						*/
						var igst=supplierOrderDetail.product.categories.igst;
						var cgst=supplierOrderDetail.product.categories.cgst;
						var sgst=supplierOrderDetail.product.categories.sgst;
						
						var mrp=((parseFloat(supplierOrderDetail.supplierRate))+((parseFloat(supplierOrderDetail.supplierRate)*parseFloat(supplierOrderDetail.product.categories.igst))/100)).toFixed(0);
						var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierOrderDetail.product.categories.igst));
						
						var total=parseFloat(supplierOrderDetail.totalAmount).toFixed(2);
						var totalAmtWithTax=parseFloat(supplierOrderDetail.totalAmountWithTax).toFixed(2);
						
						if(showSupplierNavigation){
							supplierName="<td><a href='${pageContext.request.contextPath}/fetchSupplierListBySupplierId?supplierId="+supplierOrderDetail.supplier.supplierId+"'>"+supplierOrderDetail.supplier.name+"</a></td>";
						}else{
							supplierName="<td>"+supplierOrderDetail.supplier.name+"</td>";
						}
						
						$("#productOrderedDATA").append("<tr>"+
	                           "<td>"+srno+"</td>"+
	                           supplierName+
	                           "<td>"+supplierOrderDetail.product.product.productName+"</td>"+
	                           "<td>"+supplierOrderDetail.product.categories.categoryName+"</td>"+
	                           "<td>"+supplierOrderDetail.product.brand.name+"</td>"+
	                           "<td>"+supplierOrderDetail.quantity+"</td>"+
	                           "<td>"+correctAmoutWithTaxObj.mrp+"</td>"+
	                           "<td>"+total+"</td>"+
	                           "<td>"+totalAmtWithTax+"</td>"+
	                       "</tr>"); 
						srno++;
						totalAmtWithTaxAll=parseFloat(totalAmtWithTaxAll)+parseFloat(totalAmtWithTax);
						totalAll=parseFloat(totalAll)+parseFloat(total);
					}
					
					$("#productOrderedDATA").append("<tr>"+	     
													   "<td colspan='7'><b>All Total</b></td>"+
							                           "<td  class='red-text'><b>"+totalAll.toFixed(2)+"</b></td>"+
							                           "<td  class='red-text'><b>"+totalAmtWithTaxAll.toFixed(2)+"</b></td>"+
							                       "</tr>");
					
					$('.modal').modal();
					$('#product').modal('open');
					
				},
				error: function(xhr, status, error) {
					  	//alert("error");
					}
			});
	   }
    function editrow(id) {
    	$('#orderentriesId').val(orderSupplierList);
    	
    	$('#currentUpdateRowId').val(id);
    	var supplierId=$('#rowsupplierid_'+id).val();
    	var productId=$('#rowproductid_'+id).val();
    	//alert($('#rowproductrate_'+id).val());
    			
    	var source1 = $("#supplierIdForOrder");
    	var v1=supplierId;
    	source1.val(v1);
    	source1.change();
    	
    	setTimeout(
    			  function() 
    			  {
    				  var source = $("#productIdForOrder");
    					var v=productId;
    					source.val(v);
    					source.change();
    			  }, 1000);
    	
    	
    	$('#supplierMobileNumber').val($('#rowsuppliermob_'+id).text());
    	$('#orderQuantity').val($('#roworderquantity_'+id).text());
    	$('#supplierRateForOrder').val($('#rowsupplierrate_'+id).val());
    	$('#supplierMRPRateForOrder').val($('#rowsupplierMrp_'+id).val());
    	
    	$('#supplierMobileNumber').change();
    	$('#orderQuantity').change();
    	$('#supplierRateForOrder').change();
    	$('#supplierMRPRateForOrder').change();
    	
    	$("#addOrderProduct").text("Update");	
    }

    function updaterow(id) {
    	$('#orderentriesId').val(orderSupplierList);
    	$("#addOrderProduct").text("Add");	
    	
    	var productIdForOrder=$('#productIdForOrder').val();
    	var supplierIdForOrder=$('#supplierIdForOrder').val();
    	var orderQuantity=$('#orderQuantity').val();
    	var supplierMobileNumber=$('#supplierMobileNumber').val();
    	
    	/*for (let [key, value] of orderSupplierList.entries()) {
    			//alert("supplierId : "+key+" - ProductId : "+value[0]+" - quantity : "+value[1]+" - mobile Number : "+value[2]);
    			if(key===supplierIdForOrder && value[0] === productIdForOrder)
    			{
    				$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Product Select Warning");
       	     	$('#msg').text("This Product and Supplier is already added to Cart"); 
       	     	return false;
    			}
    		}*/
    	
    	//orderProductAndQuantityList.put(productIdForOrder,orderQuantity);
    	
    	var supplierIdTemp=$('#rowsupplierid_' + id).val()
    	var productIdTemp=$('#rowproductid_' + id).val()
    	    	
    	for (var i=0; i<orderSupplierList.length; i++) 
    	{
    		var value=orderSupplierList[i];
    		if(value[0]!==supplierIdTemp || value[1]!==productIdTemp)
    		{			
    			orderSupplierListTemp.push(orderSupplierList[i]);
    		}
    	}
    	orderSupplierList=[];
    	for (var i=0; i<orderSupplierListTemp.length; i++) 
    	{			
    		orderSupplierList.push(orderSupplierListTemp[i]);
    	}
    	orderSupplierListTemp=[];
    	
    	var prodctlst = [supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber];
    	orderSupplierList.push(prodctlst);
    	
    	var supplierName=$('#supplierIdForOrder option:selected').text();
    	var productName=$('#productIdForOrder option:selected').text();
    	var supplierRate=$('#supplierRateForOrder').val();
    	var supplierMrpRate=$('#supplierMRPRateForOrder').val();
    	
    	var rowCount = $('#orderCartTb tr').length;
    	var trData="";
    	count=1;
    	for(var i=1; i<=rowCount; i++)
    	{
    		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
    		
    		if(id!=i)
    		{
    			//alert("predata");
    			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
        		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
           				"<td id='rowcount_" + count + "'>" + count + "</td>"+
           				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
           				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
           				"</tr>"; */
    			
    					
    			
           				trData=trData+"<tr id='rowdel_" + count + "' >"+
                                "<td id='rowcount_" + count + "'>"+count+"</td>"+
                                "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+$('#rowsupplierid_' + i).val()+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+$('#rowsuppliermob_' + i).val()+"><span id='rowsuppliernametb_"+count+"'>"+$('#rowsuppliernametb_' + i).text()+"</span></td>"+
                                "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+$('#rowproductid_' + i).val()+"><span id='rowproductnametb_"+count+"'>"+$('#rowproductnametb_' + i).text()+"</span></td>"+
                                "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+$('#rowsupplierrate_' + i).val()+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+$('#rowsupplierMrp_' + i).val()+">"+$('#rowsupplierMrp_' + i).val()+"</td>"+
                                "<td id='roworderquantity_"+count+"'>"+$('#roworderquantity_' + i).text()+"</td>"+
                                "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
                                "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
                            	"</tr>";
        		 count++;
    		}
    		else
    		{
    			trData=trData+"<tr id='rowdel_" + count + "' >"+
    				            "<td id='rowcount_" + count + "'>"+count+"</td>"+
    				            "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+supplierIdForOrder+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+supplierMobileNumber+"><span id='rowsuppliernametb_"+count+"'>"+supplierName+"</span></td>"+
    				            "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+productIdForOrder+"><span id='rowproductnametb_"+count+"'>"+productName+"</span></td>"+
    				            "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+supplierRate+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+supplierMrpRate+">"+supplierMrpRate+"</td>"+
    				            "<td id='roworderquantity_"+count+"'>"+orderQuantity+"</td>"+
    				            "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
    				            "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
    				        	"</tr>";
        		 count++;
    		}
    		//alert(trData);
    	} 
    	$("#orderCartTb").html('');
    	$("#orderCartTb").html(trData);
    	
    	//alert(productList.entries());
        //$('#rowdel_' + id).remove();
       // alert('productidlist '+productidlist);
    	resetOrderSelectionData();
    }

    function deleterow(id) {
    	
    	var supplierIdForOrder=$('#supplierIdForOrder').val();

    	var supplierIdTemp=$('#rowsupplierid_' + id).val()
    	var productIdTemp=$('#rowproductid_' + id).val()
    	for (var i=0; i<orderSupplierList.length; i++) 
    	{
    		var value=orderSupplierList[i];
    		if(value[0]!==supplierIdTemp || value[1]!==productIdTemp)
    		{			
    			orderSupplierListTemp.push(orderSupplierList[i]);
    		}
    	}
    	orderSupplierList=[];
    	for (var i=0; i<orderSupplierListTemp.length; i++) 
    	{			
    		orderSupplierList.push(orderSupplierListTemp[i]);
    	}
    	orderSupplierListTemp=[];
        
        var rowCount = $('#orderCartTb tr').length;
    	var trData="";
    	count=1;
    	$('#orderentriesId').val(orderSupplierList.entries());
    	for(var i=1; i<=rowCount; i++)
    	{
    		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
    		
    		if(id!==i)
    		{
    			
           				trData=trData+"<tr id='rowdel_" + count + "' >"+
    				                    "<td id='rowcount_" + count + "'>"+count+"</td>"+
    				                    "<td id='rowsuppliername_"+count+"'><input type='hidden' id='rowsupplierid_"+count+"' value="+$('#rowsupplierid_' + i).val()+"><input type='hidden' id='rowsuppliermob_"+count+"' value="+$('#rowsuppliermob_' + i).val()+"><span id='rowsuppliernametb_"+count+"'>"+$('#rowsuppliernametb_' + i).text()+"</span></td>"+
    				                    "<td id='rowproductname_"+count+"'><input type='hidden' id='rowproductid_"+count+"' value="+$('#rowproductid_' + i).val()+"><span id='rowproductnametb_"+count+"'>"+$('#rowproductnametb_' + i).text()+"</span></td>"+
    				                    "<td><input type='hidden' id='rowsupplierrate_"+count+"' value="+$('#rowsupplierrate_' + i).val()+"><input type='hidden' id='rowsupplierMrp_"+count+"' value="+$('#rowsupplierMrp_' + i).val()+">"+$('#rowsupplierMrp_' + i).val()+"</td>"+
    				                    "<td id='roworderquantity_"+count+"'>"+$('#roworderquantity_' + i).text()+"</td>"+
    				                    "<td id='roweditbutton_" + count + "'><button type='button'  onclick='editrow(" + count + ")' class='btn-flat'><i class='material-icons'>edit</i></button></td>"+
    				                    "<td id='rowdelbutton_" + count + "'><button type='button' onclick='deleterow(" + count + ")' class='btn-flat'><i class='material-icons'>cancel</i></button></td>"+
    				                	"</tr>";
        		 count++;
    		}
    		//alert(trData);
    	} 
    	$("#orderCartTb").html('');
    	$("#orderCartTb").html(trData);
    	//alert(productList.entries()); 
        //$('#rowdel_' + id).remove();
       // alert('productidlist '+productidlist);
    	resetOrderSelectionData();
    }
    
    function updateOrderProducts(){
    	//alert(orderSupplierList.entries());
    	Materialize.Toast.removeAll();
    	
    	if(orderSupplierList.length==0){
    		Materialize.toast('Add Minimum 1 product for Order', '4000', 'teal lighten-3');
    		return  false;
    	}
    	
    	if(orderSupplierList.length==0){
    		Materialize.toast('Add Some Product For Order', '4000', 'teal lighten-3');
    		return false;
    	}
    	
    	var productIdList="";
    	for (var i=0; i<orderSupplierList.length; i++) {
    		var value=orderSupplierList[i];
    		productIdList=productIdList+value[0]+"-"+value[1]+"-"+value[2]+"-"+value[3]+",";
  		}
    	productIdList=productIdList.slice(0,-1)
    	//alert(productIdList);
    	$('#productWithSupplikerlist').val(productIdList);
    	
    	$("#orderProductsButton").attr('disabled','disabled');
    	var form = $('#editOrderBookForm');

		$.ajax({
					type : form.attr('method'),
					url : form.attr('action'),
					data : $("#editOrderBookForm").serialize(),
					success : function(data) {
						if(data=="Success")
						{
								$('#add').modal('close');
								Materialize.toast('Order Edited SuccessFully!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').modal('open');
								$('#msgHead').text("Success : ");
								$('#msg').html("Order Edited SuccessFully");*/
								setTimeout(function() 
								  {
									location.reload();
								  }, 1000);
								
						}
						else
						{
							$("#orderProductsButton").removeAttr("disabled");
							Materialize.toast('Failed to edit order!', '4000', 'teal lighten-3');
								/*$('#addeditmsg').modal('open');
								$('#msgHead').text("Failed : ");
								$('#msg').html("<font color='red'>Order Editing Failed</font>");*/
						}
					}
		});
    }
    
    function orderConfirmationUpdateSupplierOrder(){
    	/* $('.modal').modal();
    	$('#delete'+id).modal('open'); */
    	Materialize.Toast.removeAll();
    	var $toastContent = $('<span>Do you want to Update Order?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="updateOrderProducts()">Yes</button>  '));
    	  Materialize.toast($toastContent, "abc");
    }