
$(document).ready(function() {
	filterChart("currentMonth","","");

	$('#lastMonthId').click(function(){
	
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var range="lastMonth";
		
		filterChart(range,startDate,endDate);
		
	});
	
	$('#last3MonthId').click(function(){
		
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var range="last3Months";
		
		filterChart(range,startDate,endDate);
		
	});
	$('#last6MonthId').click(function(){
		
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var range="last6Months";
		
		filterChart(range,startDate,endDate);
		
	});
	$('#rangeId').click(function(){
		
		var startDate=$('#startDate').val();
		var endDate=$('#endDate').val();
		var range="range";
		
		filterChart(range,startDate,endDate);
		
	});
});

function filterChart(range,startDate,endDate){
	
	$.ajax
	({
		type : "POST",
		url : myContextPath+"/fetchTopChart",
       //async:false,
		data:{
			 	range:range,
			 	startDate:startDate,
			 	endDate:endDate
			 },
		success : function(data) 
		{
			var totalSales=0,totalInvested=0,currentInventory=0,threshold=0;
			totalSales=data.totalSales.toFixed(2);
			totalInvested=parseFloat(data.totalAmountInvestInMarket).toFixed(2);
			currentInventory=parseFloat(data.totalValueOfCurrentInventory).toFixed(2);
			threshold=data.productUnderThresholdCount;
			$('#totalSalesId').html(totalSales);
			$('#totalAmountInvestInMarketId').html(totalInvested);
			$('#totalValueOfCurrentInventoryId').html(currentInventory);
			$('#productUnderThresholdCountId').html(threshold);
			$('.count').each(function () {
			    $(this).prop('Counter',0).animate({
			        Counter: $(this).text()
			    }, {
			        duration: 4000,
			        easing: 'swing',
			        step: function (now) {
			            $(this).text(Math.ceil(now));
			        }
			    });
			});
		},
		error: function(xhr, status, error) {
			//alert("Error");
		}
		
	});
	
	$.ajax
	({
		type : "POST",
		url : myContextPath+"/fetchTotalAmountChart",
      //async:false,
		data:{
		 	range:range,
		 	startDate:startDate,
		 	endDate:endDate
		 },
		success : function(data) 
		{
			setTotalAmountChart(data.recieved,data.expected);
		},
		error: function(xhr, status, error) {
			//alert("Error");
		}
	});
	$.ajax
	({
		type : "POST",
		url : myContextPath+"/fetchTopFiveProductChart",
      //async:false,
		data:{
		 	range:range,
		 	startDate:startDate,
		 	endDate:endDate
		 },
		success : function(data) 
		{	
			var productDetailsArray=[];
			productDetailsArray[0]=data.productName1;
			productDetailsArray[1]=data.productPercentage1;
			productDetailsArray[2]=data.productName2;
			productDetailsArray[3]=data.productPercentage2;
			productDetailsArray[4]=data.productName3;
			productDetailsArray[5]=data.productPercentage3;
			productDetailsArray[6]=data.productName4;
			productDetailsArray[7]=data.productPercentage4;
			productDetailsArray[8]=data.productName5;
			productDetailsArray[9]=data.productPercentage5;
			
			setTopFiveProductChart(productDetailsArray);
		},
		error: function(xhr, status, error) {
			//alert("Error");
		}
	});
	$.ajax
	({
		type : "POST",
		url : myContextPath+"/fetchTopFiveSallersChart",
      //async:false,
		data:{
		 	range:range,
		 	startDate:startDate,
		 	endDate:endDate
		 },
		success : function(data) 
		{
			var sallerDetailsArray=[];
			sallerDetailsArray[0]=data.salesManName1;
			sallerDetailsArray[1]=data.salesManPercentage1;
			sallerDetailsArray[2]=data.salesManName2;
			sallerDetailsArray[3]=data.salesManPercentage2;
			sallerDetailsArray[4]=data.salesManName3;
			sallerDetailsArray[5]=data.salesManPercentage3;
			sallerDetailsArray[6]=data.salesManName4;
			sallerDetailsArray[7]=data.salesManPercentage4;
			sallerDetailsArray[8]=data.salesManName5;
			sallerDetailsArray[9]=data.salesManPercentage5;
			setTopFiveSallersChart(sallerDetailsArray);
		},
		error: function(xhr, status, error) {
			//alert("Error");
		}
	});
	$.ajax
	({
		type : "POST",
		url : myContextPath+"/fetchTopFiveReturnReportChart",
	   //async:false,
		data:{
		 	range:range,
		 	startDate:startDate,
		 	endDate:endDate
		 },
		success : function(data) 
		{
			var returnDetailsArray=[];
			returnDetailsArray[0]=data.returnProductName1;
			returnDetailsArray[1]=data.returnProductPercentage1;
			returnDetailsArray[2]=data.returnProductName2;
			returnDetailsArray[3]=data.returnProductPercentage2;
			returnDetailsArray[4]=data.returnProductName3;
			returnDetailsArray[5]=data.returnProductPercentage3;
			returnDetailsArray[6]=data.returnProductName4;
			returnDetailsArray[7]=data.returnProductPercentage4;
			returnDetailsArray[8]=data.returnProductName5;
			returnDetailsArray[9]=data.returnProductPercentage5;
			setTopFiveReturnReportChart(returnDetailsArray);			
		},
		error: function(xhr, status, error) {
			//alert("Error");
		}
	});
	
} 

function setTotalAmountChart(rc,ep){
	
     Highcharts.chart('container1', {
    	 
         colors: ['#f45b5b', '#b0bec5 '],
         chart: {
             backgroundColor: '#eceff1',
             style: {
                 fontFamily: 'sans-serif'
             },
             spacingBottom: 5,
             spacingTop: 5,
             spacingLeft: 5,
             spacingRight: 5,
         },
         credits: ({
             enabled: false
         }),
         legend: {
             itemStyle: {
                 fontWeight: 'bold',
                 fontSize: '13px',
             }
         },
         title: {
             text: 'Total Amount'
         },
         tooltip: {
             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',

                 dataLabels: {
                     enabled: true,
                     format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                     style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                     }

                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'Total',
             data: [

                 ['Expected', ep], 
                 {
	                 name: 'Recieved', y: rc,
	                 sliced: false,
	                 selected: true
                 }
             ],
             //center: [235, 35],
             size: 105,            
             showInLegend: true,
             dataLabels: {
                 enabled: true
             }
         }],

     });
     
     //setTimeout(setDynamicChart, 2000);
     
	}
function setTopFiveProductChart(productDetailsArray){
     Highcharts.chart('container2', {


         colors: ['#ff8a80 ', '#aa00ff ', '#00e676', '#c51162', '#ff9100'],
         title: {
             text: 'Top 5 Sold Product',
         },

         chart: {
             backgroundColor: '#eceff1 ',
             style: {
                 fontFamily: 'sans-serif'
             },
             spacingBottom: 5,
             spacingTop: 5,
             spacingLeft: 5,
             spacingRight: 5,

         },
         credits: ({
             enabled: false
         }),
         tooltip: {
             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',

                 dataLabels: {
                     enabled: true,
                     format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                     style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                             'black'
                     }
                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'Sold',
   /*          data: [{
                 name: 'IE',
                 y: 56.33
             }, {
                 name: 'Chrome',
                 y: 24.03,
                 sliced: true,
                 selected: true
             }, {
                 name: 'Firefox',
                 y: 10.38
             }, {
                 name: 'Safari',
                 y: 4.77
             }, {
                 name: 'Opera',
                 y: 5.11
             }],*/
             data: 
             [
				{
				    name: productDetailsArray[0],
				    y: productDetailsArray[1]
				},
				{
				    name: productDetailsArray[2],
				    y: productDetailsArray[3]
				},
				{
				    name: productDetailsArray[4],
				    y:  productDetailsArray[5]
				},  
				{
				    name: productDetailsArray[6],
				    y:  productDetailsArray[7]
				},  
                {                	 
                     name: productDetailsArray[8],
                     y: productDetailsArray[9],
                     sliced: true,
                     selected: true
                 }
             ],
            // center: [235, 35],
             size: 105, 
             showInLegend: true,
             dataLabels: {
                 enabled: true
             }
         }],




     });
}

function setTopFiveSallersChart(sallerDetailsArray){
     Highcharts.chart('container3', {

         colors: ['#f48fb1 ', '#90caf9 ', '#26a69a ', '#ffca28', '#b0bec5'

         ],
         title: {
             text: 'Top 5 Sellers'
         },
         chart: {
             backgroundColor: '#eceff1 ',
             style: {
                 fontFamily: 'sans-serif'
             },
             spacingBottom: 5,
             spacingTop: 5,
             spacingLeft: 5,
             spacingRight: 5,

         },
         credits: ({
             enabled: false
         }),
         tooltip: {
             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',

                 dataLabels: {
                     enabled: true,
                     format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                     style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                             'black'
                     }
                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'Employees',
             /*data: [{
                 name: 'IE',
                 y: 56.33
             }, {
                 name: 'Chrome',
                 y: 24.03,
                 sliced: true,
                 selected: true
             }, {
                 name: 'Firefox',
                 y: 10.38
             }, {
                 name: 'Safari',
                 y: 4.77
             }, {
                 name: 'Opera',
                 y: 5.11
             }],*/
             data: 
             [
              {
 				    name:sallerDetailsArray[0],
 				    y:parseInt(sallerDetailsArray[1])
 				},
 				 {
 				    name:sallerDetailsArray[2],
 				    y:parseInt(sallerDetailsArray[3])
 				}, {
 				    name: sallerDetailsArray[4],
 				    y:parseInt(sallerDetailsArray[5])
 				}, {
 				    name: sallerDetailsArray[6],
 				    y:parseInt(sallerDetailsArray[7])
 				}, 
                 {                	 
                     name: sallerDetailsArray[8],
                     y: parseInt(sallerDetailsArray[9]),
                     sliced: true,
                     selected: true
                 }
             ],
            // center: [235, 35],
             size: 105, 
             showInLegend: true,

             dataLabels: {
                 enabled: true
             }
         }],


     });
}
function setTopFiveReturnReportChart(returnDetailsArray){
     Highcharts.chart('container4', {



         colors: ['#8d4654', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee'],
         title: {
             text: 'Top 5 Return Item',

         },
         chart: {
             backgroundColor: '#eceff1 ',
             style: {
                 fontFamily: 'sans-serif'
             },
             spacingBottom: 5,
             spacingTop: 5,
             spacingLeft: 5,
             spacingRight: 5,

         },
         credits: ({
             enabled: false
         }),
         tooltip: {
             pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',

                 dataLabels: {
                     enabled: true,
                     format: '<b>{point.name}%</b>: {point.percentage:.1f} %',
                     style: {
                         color: (Highcharts.theme && Highcharts.theme.contrastTextColor) ||
                             'black'
                     }
                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'Blue Square',
             /*data: [{
                 name: 'IE',
                 y: 56.33
             }, {
                 name: 'Chrome',
                 y: 24.03,
                 sliced: true,
                 selected: true
             }, {
                 name: 'Firefox',
                 y: 10.38
             }, {
                 name: 'Safari',
                 y: 4.77
             }, {
                 name: 'Opera',
                 y: 5.11
             }],*/
             data: 
             [
                 {
  				    name:returnDetailsArray[0],
  				    y:parseInt(returnDetailsArray[1])
  				},  {
  				    name:returnDetailsArray[2],
  				    y:parseInt(returnDetailsArray[3])
  				},
  				 {
  				    name:returnDetailsArray[4],
  				    y:parseInt(returnDetailsArray[5])
  				}, {
  				    name:returnDetailsArray[6],
  				    y:parseInt(returnDetailsArray[7])
  				}, 
                 {                	 
                     name:returnDetailsArray[8],
                     y: parseInt(returnDetailsArray[9]),
                     sliced: true,
                     selected: true
                 }
             ],
            // center: [235, 35],
             size: 105, 
             showInLegend: true,
             dataLabels: {
                 enabled: true
             }
         }],
     });
}
   
