$(document).ready(function() {
	
  
						$('#quantity').keypress(function( event ){
						    var key = event.which;
						    
						    if( ! ( key >= 48 && key <= 57 || key === 13) )
						        event.preventDefault();
						});
						
						$('#resetAddQuantitySubmit').click(function()
						{
							count = 1;
							productList=[];
							$('#t1').empty();
							
							/*var source=$('#supplierId');
							source.val(0);
							source.change();*/
							
							var source=$('#brandId');
							source.val(0);
							source.change();
							
							var source=$('#categoryId');
							source.val(0);
							source.change();

							/*var source=$('#productId');
							source.val(0);
							source.change();*/
							
							$('#productId').change();
							
							$('#hsncode').val('');									
							//$('#cgstPer').html(0);
							$('#igstPer').html(0);
							//$('#sgstPer').html(0);
							$('#totalAmt').val('');
							$('#amountWithTax').val('');
							//$('#cgst').val('');	
							$('#igst').val('');	
							//$('#sgst').val('');	
							
							$('#hsncode').change();
							//$('#cgstPer').change();
							$('#igstPer').change();
							//$('#sgstPer').change();
							$('#totalAmt').change();
							$('#amountWithTax').change();
							//$('#cgst').change();
							$('#igst').change();
							//$('#sgst').change();
							
							
							$('#AmtPerUnit').val(0);
							$('#AmtPerUnit').change();
							
							$('#oldquantity').val("");
							
							$('#quantity').val("");
							
							/*$('#paymentDate').val('');
							$('#paymentDate').change();*/
							
							$('#finalTotalAmount').text(0);
							$('#finalTotalAmountWithTax').text(0);
						});
						
						$('#supplierId').change(function(){
							$('#resetAddQuantitySubmit').click();
							var supplierId=$('#supplierId').val();
							
							$('#productId').empty();
							$("#productId").append('<option value="0">Choose Product</option>');
							if(supplierId==="0")
							{
								return false;
							}
							$.ajax({
								url : myContextPath+"/fetchProductBySupplierId?supplierId="+supplierId,
								dataType : "json",
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									for(var i=0; i<data.length; i++)
									{								
										//alert(data[i].productId +"-"+ data[i].productName);
										$("#productId").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
									}	
									//alert("done");
									$("#productId").change();
									$('#hsncode').val('');									
									//$('#cgstPer').html(0);
									$('#igstPer').html(0);
									//$('#sgstPer').html(0);
									$('#totalAmt').val('');
									$('#amountWithTax').val('');
									//$('#cgst').val('');	
									$('#igst').val('');	
									//$('#sgst').val('');	
									
									$('#hsncode').change();
									//$('#cgstPer').change();
									$('#igstPer').change();
									//$('#sgstPer').change();
									$('#totalAmt').change();
									$('#amountWithTax').change();
									//$('#cgst').change();
									$('#igst').change();
									//$('#sgst').change();
									
									var source=$('#categoryId');
									source.val(0);
									source.change();
									
									var source=$('#brandId');
									source.val(0);
									source.change();							
									
									$('#oldquantity').val(data.currentQuantity);
									$('#oldquantity').change();
									
									$('#AmtPerUnit').val('');
									$('#AmtPerUnit').change();
									
									$('#AmtMRP').val('');
									$('#AmtMRP').change();
									
									$('#quantity').val("");
									
									$('#paymentDate').val();
									
									count = 1;
									productList=[];
									
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Product List Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);
									}
							});
							
						});
					
						$('#productId').change(function(){
							
							var productId=$('#productId').val();
							if(productId==="0")
							{
								$('#hsncode').val('');
								$('#hsncode').change();
								$('#AmtPerUnit').val('');
								$('#AmtPerUnit').change();
								$('#oldquantity').val('');
								$('#oldquantity').change();
								$('#totalAmt').val('');
								$('#totalAmt').change();
								$('#igst').val('');
								$('#igst').change();
								$('#igstPer').text('');
								$('#igstPer').change();
								$('#amountWithTax').val('');
								$('#amountWithTax').change();
								return false;
							}
							
							//AmtPerUnit
							
							var productId=$('#productId').val();
							var supplierId=$('#supplierId').val();
							$.ajax({
								url : myContextPath+"/fetchSupplierById?supplierId="+supplierId+"&productId="+productId,
								dataType : "json",
								async:false,
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									var supplierproduct=data;
									var mrp=((parseFloat(supplierproduct.supplierRate))+((parseFloat(supplierproduct.supplierRate)*parseFloat(supplierproduct.product.categories.igst))/100)).toFixed(0);
			    					var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierproduct.product.categories.igst));
									
			    					$('#AmtMRP').val(parseFloat(correctAmoutWithTaxObj.mrp).toFixed(2));
									$('#AmtMRP').change();
									
									$('#AmtPerUnit').val(parseFloat(data.supplierRate).toFixed(2));
									$('#AmtPerUnit').change();
									
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									Materialize.toast('Product Details Not Found!', '2000', 'teal lighten-3');
									/*$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Product Details Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});
							
							
							$.ajax({
								url : myContextPath+"/fetchProductByProductId?productId="+productId,
								dataType : "json",
								async:false,
								beforeSend: function() {
									$('.preloader-background').show();
									$('.preloader-wrapper').show();
						           },
								success : function(data) {
									
									var category=data.categories;
									//$('#cgstPer').html(category.cgst);
									$('#igstPer').html(category.igst);
									//$('#sgstPer').html(category.sgst);
									var enteredQuantity=$('#quantity').val();
									if(enteredQuantity!=="" && enteredQuantity!==undefined)
									{
										//alert($('#igstPer').html());
										

										var mrp=((parseFloat($('#AmtPerUnit').val()))+((parseFloat($('#AmtPerUnit').val())*parseFloat($('#igstPer').html()))/100)).toFixed(0);
										var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat($('#igstPer').html()));
										
										$('#AmtMRP').val(correctAmoutWithTaxObj.mrp);
										
										var igstAmount=
											(((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(enteredQuantity)).toFixed(2))
											-((parseFloat(correctAmoutWithTaxObj.unitPrice)*parseInt(enteredQuantity)).toFixed(2))).toFixed(2);
										
										//((parseFloat($('#totalAmt').val())*parseFloat($('#igstPer').html()))/100).toFixed(2);
										//var cgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#cgstPer').html()))/100;;
										//var sgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#sgstPer').html()))/100;;
										$('#totalAmt').val((parseFloat(correctAmoutWithTaxObj.unitPrice)*parseInt(enteredQuantity)).toFixed(2));
										$('#amountWithTax').val((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(enteredQuantity)).toFixed(2));
										//$('#cgst').val(cgstAmount);
										$('#igst').val(igstAmount);
										//$('#sgst').val(sgstAmount);
									}
									else
									{
										//$('#cgst').val('');
										$('#igst').val('');
										//$('#sgst').val('');
										$('#amountWithTax').val('');
										$('#totalAmt').val('');
									}
									
									$('#hsncode').change();
									//$('#cgstPer').change();
									$('#igstPer').change();
									//$('#sgstPer').change();
									$('#totalAmt').change();
									$('#amountWithTax').change();
									//$('#cgst').change();
									$('#igst').change();
									//$('#sgst').change();							
									
									$('#hsncode').val(data.categories.hsnCode);
									$('#hsncode').change();
									
									$('#oldquantity').val(data.currentQuantity);
									$('#oldquantity').change();
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
								},
								error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									  //alert(error +"---"+ xhr+"---"+status);
									Materialize.toast('Product Details Not Found!', '2000', 'teal lighten-3');
									/*$('#addeditmsg').modal('open');
					       	     	$('#msgHead').text("Message : ");
					       	     	$('#msg').text("Product Details Not Found"); 
					       	     		setTimeout(function() 
										  {
						     					$('#addeditmsg').modal('close');
										  }, 1000);*/
									}
							});
							
							
							
						});
						
						$('#brandId').change(function(){
						
									var categoryId=$('#categoryId').val();
									var brandId=$('#brandId').val();
									var supplierId=$('#supplierId').val();
									
									if(supplierId==="0")
									{
										return false;
									}
									
									$('#productId').empty();
									$("#productId").append('<option value="0">Choose Product</option>');
									$.ajax({ 
										url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
										dataType : "json",
										async :false,
										beforeSend: function() {
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								           },
										success : function(data) {
											
											for(var i=0; i<data.length; i++)
											{								
												//alert(data[i].productId +"-"+ data[i].productName);
												$("#productId").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
											}	
											//alert("done");
											$("#productId").change();
											
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
										},
										error: function(xhr, status, error) {
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
											  //alert(error +"---"+ xhr+"---"+status);
											Materialize.toast('Product List Not Found!', '2000', 'teal lighten-3');
											/*$('#addeditmsg').modal('open');
							       	     	$('#msgHead').text("Message : ");
							       	     	$('#msg').text("Product List Not Found"); 
							       	     		setTimeout(function() 
												  {
								     					$('#addeditmsg').modal('close');
												  }, 1000);*/
											}
										});
							
						});
						
						$('#categoryId').change(function(){
							
							var categoryId=$('#categoryId').val();
							var brandId=$('#brandId').val();
							var supplierId=$('#supplierId').val();
							if(supplierId==="0")
							{
								return false;
							}
							/*$.ajax({
								url : myContextPath+"/fetchCategories?categoriesId="+categoryId,
								dataType : "json",
								async :false,
								success : function(data) {
									var category=data;
									
									//$('#hsncode').val(category.hsnCode);
									$('#cgstPer').html(category.cgst);
									$('#igstPer').html(category.igst);
									$('#sgstPer').html(category.sgst);
									var enteredQuantity=$('#quantity').val();
									if(enteredQuantity!=="" && enteredQuantity!==undefined)
									{
										$('#totalAmt').val(enteredQuantity*parseFloat($('#AmtPerUnit').val()));
										var igstAmount=(parseFloat($('#totalAmt').val())*parseFloat(category.igst))/100;
										var cgstAmount=(parseFloat($('#totalAmt').val())*parseFloat(category.cgst))/100;;
										var sgstAmount=(parseFloat($('#totalAmt').val())*parseFloat(category.sgst))/100;;
										$('#amountWithTax').val(igstAmount+cgstAmount+sgstAmount+parseFloat($('#totalAmt').val()));
										$('#cgst').val(cgstAmount);
										$('#igst').val(igstAmount);
										$('#sgst').val(sgstAmount);
									}
									else
									{
										$('#cgst').val('');
										$('#igst').val('');
										$('#sgst').val('');
										$('#amountWithTax').val('');
										$('#totalAmt').val('');
									}
									
									$('#hsncode').change();
									$('#cgstPer').change();
									$('#igstPer').change();
									$('#sgstPer').change();
									$('#totalAmt').change();
									$('#amountWithTax').change();
									$('#cgst').change();
									$('#igst').change();
									$('#sgst').change();
								},
								error: function(xhr, status, error) {
									  //var err = eval("(" + xhr.responseText + ")");
									//alert("Error "+xhr.responseText);
									$('#hsncode').val('');									
									$('#cgstPer').html(0);
									$('#igstPer').html(0);
									$('#sgstPer').html(0);
									$('#totalAmt').val('');
									$('#amountWithTax').val('');
									$('#cgst').val('');	
									$('#igst').val('');	
									$('#sgst').val('');	
									
									$('#hsncode').change();
									$('#cgstPer').change();
									$('#igstPer').change();
									$('#sgstPer').change();
									$('#totalAmt').change();
									$('#amountWithTax').change();
									$('#cgst').change();
									$('#igst').change();
									$('#sgst').change();
								}
							});*/
							
							
									
									$('#productId').empty();
									$("#productId").append('<option value="0">Choose Product</option>');
									$.ajax({ 
										url : myContextPath+"/fetchProductBySupplierIdAndCategoryIdAndBrandId?categoryId="+categoryId+"&brandId="+brandId+"&supplierId="+supplierId,
										dataType : "json",
										async :false,
										beforeSend: function() {
											$('.preloader-background').show();
											$('.preloader-wrapper').show();
								           },
										success : function(data) {
											
											for(var i=0; i<data.length; i++)
											{								
												//alert(data[i].productId +"-"+ data[i].productName);
												$("#productId").append('<option value='+data[i].productId+'>'+data[i].productName+'</option>');										
											}	
											//alert("done");
											$("#productId").change();
											 
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
										},
										error: function(xhr, status, error) {
											$('.preloader-wrapper').hide();
											$('.preloader-background').hide();
											  //alert(error +"---"+ xhr+"---"+status);
											Materialize.toast('Product List Not Found!', '2000', 'teal lighten-3');
											/*$('#addeditmsg').modal('open');
							       	     	$('#msgHead').text("Message : ");
							       	     	$('#msg').text("Product List Not Found"); 
							       	     		setTimeout(function() 
												  {
								     					$('#addeditmsg').modal('close');
												  }, 1000);*/
											}
										});
							
								
							
						});
						
						$('#quantity').on("keyup change blur",function(){
							
							var enteredQuantity=$('#quantity').val();
							if(enteredQuantity!=="" && enteredQuantity!==undefined && $('#igstPer').html()!=="" /*&& $('#cgstPer').html()!=="" && $('#sgstPer').html()!==""*/)
							{
								//alert($('#igstPer').html());
								
								
								var mrp=((parseFloat($('#AmtPerUnit').val()))+((parseFloat($('#AmtPerUnit').val())*parseFloat($('#igstPer').html()))/100)).toFixed(0);
								var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat($('#igstPer').html()));
								
								$('#AmtMRP').val(correctAmoutWithTaxObj.mrp);
								
								var igstAmount=
									(((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(enteredQuantity)).toFixed(2))
									-((parseFloat(correctAmoutWithTaxObj.unitPrice)*parseInt(enteredQuantity)).toFixed(2))).toFixed(2);
									//((parseFloat($('#totalAmt').val())*parseFloat($('#igstPer').html()))/100).toFixed(2);
								//var cgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#cgstPer').html()))/100;;
								//var sgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#sgstPer').html()))/100;;
								$('#totalAmt').val((parseFloat(correctAmoutWithTaxObj.unitPrice)*parseInt(enteredQuantity)).toFixed(2));
								$('#amountWithTax').val((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(enteredQuantity)).toFixed(2));
								//$('#cgst').val(cgstAmount);
								$('#igst').val(igstAmount);
								//$('#sgst').val(sgstAmount);
							}
							else
							{
								//$('#cgst').val('');
								$('#igst').val('');
								//$('#sgst').val('');
								$('#amountWithTax').val('');
								$('#totalAmt').val('');
							}
							
							$('#hsncode').change();
							//$('#cgstPer').change();
							$('#igstPer').change();
							//$('#sgstPer').change();
							$('#totalAmt').change();
							$('#amountWithTax').change();
							//$('#cgst').change();
							$('#igst').change();
							//$('#sgst').change();
						});
						
						
						$("#productAddId").click( function() {
							
							var product=$("#productId").val();
							if(product=="0")
							{
								Materialize.toast('Select Product!', '2000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#msg').text("Select Product");*/
			           	     	return false;
							}
							var category=$("#categoryId").val();
							/*if(category=="0")
							{
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#msg').text("Select category");
			           	     	return false;
							}*/
							var qua=$("#quantity").val();
							if(qua==="")
							{
								Materialize.toast('Enter Quantity!', '2000', 'teal lighten-3');
								/*$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
								$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#msg').text("Enter Quantity");*/
			           	     	return false;
							}
							
							
			            	var buttonStatus=$('#productAddButtonId').text();
			            	if(buttonStatus==="Update")
			            	{
			            		updaterow($('#currentUpdateProductId').val());
			            		return false;
			            	}
			            	$("#productAddId").html('<span id="productAddButtonId">Add</span><i class="material-icons right">add</i>');
			                var val = $("#productId").val(); 
			                var qty = $('#quantity').val();
			              	var rate = $('#AmtPerUnit').val();
			              	var rate_mrp = $('#AmtMRP').val();
			              	//alert(buttonStatus);
			              	if(val==0)
			              	{
			              		return false;
			              	}
			              	if(qty==0 || qty==="" || qty===undefined)
			              	{
			              		return false;
			              	}
			              	/* if(jQuery.inArray(val, productidlist) !== -1)
			                {	
			              		$('#addeditmsg').modal('open');
			           	     	$('#msgHead').text("Product Select Warning");
			           	     	$('#msg').text("This Product is already added");               	     	
			                } */
			              	else
			           		{
				                
			              		
			              		//alert(productList.size());
			              		/* for (let key of productList.keys()) {
			              		    alert(key+"-"+productList[key]);
			              		} */
			              		//alert(productList.entries());
			              		/* for (let entry of productList.entries()) {
			              			alert(entry[0]+"-"+ entry[1]);
			              		} */
			              		/* for (let [key, value] of productList.entries()) {
			              			alert(key+"-"+value);
			              			productList.remove(key);
			              		} */
				             	//alert(productList.key +"-"+ productList.value);
				             	 
			              		for (var i=0; i<productList.length; i++) { 
			              			var value=productList[i]; 
			              			if(value[0]===val)
			              			{  
			              				Materialize.toast('This Product is already added!', '2000', 'teal lighten-3');
			              				/*$('#addeditmsg').modal('open');
			                   	     	$('#msgHead').text("Product Select Warning");
			                   	     	$('#msg').text("This Product is already added");*/ 
			                   	     	return false;
			              			}
			              		}
			              		var prdData=[val,qty];
			              		productList.push(prdData);
				             	/* $('#productratelistinput').val(productRateidlist);
				             	$('#productlistinput').val(productidlist); */
				                var text=$("#productId option:selected").text();
				                var vl=$("#productId option:selected").val();
								//alert(value+"-"+text);
								var rowCount = $('#productcart tr').length;
								
								//var tempCgst=$('#cgstPer').html();
								var tempIgst=$('#igstPer').html();
								//var tempSgst=$('#sgstPer').html();
								
								var mrp=((parseFloat(rate))+((parseFloat(rate)*parseFloat(tempIgst))/100)).toFixed(0);
								var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(tempIgst));
								
								var ttl=( parseInt(qty) * parseFloat(rate) );
								var amtwithtax=(parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(qty)).toFixed(2);// + ( (parseFloat(ttl)*parseFloat(tempCgst))/100 ) + ( (parseFloat(ttl)*parseFloat(tempSgst))/100 ) ;
								
				                $("#t1").append("<tr id='rowdel_" + count + "' >"+
					               				"<td id='rowcount_" + count + "'>" + count + "</td>"+

					               				"<td id='rowproductname_" + count + "'>"+					               				
					               				"<input type='hidden' id='rowproduct_total_amt_" + count + "' value='"+ttl+"'>"+
					               				"<input type='hidden' id='rowproduct_total_amt_with_tax" + count + "' value='"+amtwithtax+"'>"+
					               				"<input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'>" +
					               				"<center><span id='tbproductname_" + count + "'>"+text+"</span></center>" +
					               				"</td>"+
					               				"<td id='rowproductmrp_" + count + "'>" + rate_mrp + "</td>"+					               				
					               				"<td id='rowproductrate_" + count + "'>" + rate + "</td>"+
					               				"<td id='rowproductqty_" + count + "'>" + qty + "</td>"+
					               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
					               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
					               				"</tr>");
				                count++;
			           		}
			              	//alert(productList.entries());
			              	TotalAmountAndTaxCalculate();
			              	//clearRecords();
			              	var source=$('#productId');
			            	source.val(0);
			            	source.change();
			            	$('#quantity').val("");
			            	$('#AmtMRP').val('');
			            });
						
						$('#saveAddQuantitySubmit').click(function(){
							
						 	//alert(productlistinput);
			            	if(productList.length==0)
			           	 	{
			            		Materialize.toast('Select Atlist 1 Product!', '2000', 'teal lighten-3');
			            	/*	$('#addeditmsg').find("#modalType").addClass("warning");
								 $('#addeditmsg').find(".modal-action").addClass("red lighten-2");
			           	     $('#addeditmsg').modal('open');
			           	     $('#msgHead').text("Supplier Adding Message");
			           	     $('#msg').text("Select Atlist 1 Product");*/
			           	     return false;
			           	 	}		            	
			            	
			            	
			            	//alert(productList.entries());
			            	var productIdList="";
			            	for (var i=0; i<productList.length; i++) {
			            		var value=productList[i];
			            		productIdList=productIdList+value[0]+"-"+value[1]+",";
			          		}
			            	productIdList=productIdList.slice(0,-1)
			            	//alert(productIdList);
			            	$('#productlistinput').val(productIdList);
			            	//$('#resetAddQuantitySubmit').click(); 
			            	
						    $('#chooseDate').modal('open');			   			            	
			                      	          	   
			       		});
						$("#submitModal").click(function(){
							var payDate=$('#paymentDate').val();
							if(payDate==="" || payDate==undefined)
							{
								//$('#addeditmsg').modal('open');
			           	     	//$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#errorPaymentDate').text("Select Payment date");
								return false;
							}
							var billDate=$('#selectedBillDate').val();
							if(billDate==="" || billDate==undefined)
							{
								//$('#addeditmsg').modal('open');
			           	     	//$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#errorBillDate').text("Select Bill date");
								return false;
							}
							var bill_number=$('#bill_number').val();
							if(bill_number==="" || bill_number==undefined)
							{
								//$('#addeditmsg').modal('open');
			           	     	//$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#errorBillNumber').text("Enter Bill Number");
								return false;
							}
							var paymentDate=new Date($('#paymentDate').val()).setHours(0,0,0,0);
							var today=new Date().setHours(0,0,0,0);
							if(paymentDate < today)
							{
								//$('#addeditmsg').modal('open');
			           	     	//$('#msgHead').text("Product Add Cart Warning");
			           	     	$('#errorPaymentDate').text("Payment date must be today or after todays date");
								return false;
							}
							
						});
						
						
					});
function editrow(id) {
	$('#currentUpdateProductId').val(id);
	var productId=$('#rowproductkey_'+id).val();
	//alert($('#rowproductrate_'+id).val());
		
	var source3 = $("#productId");
	var v3=productId;
	source3.val(v3);
	source3.change();
	
	$('#quantity').val($('#rowproductqty_'+id).text());
	$('#quantity').focus();
	$('#quantity').trigger('blur');
	$('#quantity').keyup();
	$("#productAddId").html('<span id="productAddButtonId">Update</span><i class="material-icons right">send</i>');
	
}

function deleterow(id) {
	
    //alert('#rowproductkey_'+id);
    var removeItem=$('#rowproductkey_' + id).val();
    //alert('removeItem '+$('#rowproductkey_' + id).val());
    //alert('productidlist '+productidlist);
  //  productList.remove(removeItem);
    
    var productListTemp=[];
    for(var i=0; i<productList.length; i++)
    {
    	var value=productList[i];
    	if(value[0]!==removeItem)
    	{
    		productListTemp.push(productList[i]);
    	}
    }
    productList=[];
    for(var i=0; i<productListTemp.length; i++)
    {
    	productList.push(productListTemp[i]);
    }
    
    var rowCount = $('#t1 tr').length;
	//alert(rowCount);
	var trData="";
	count=1;
	for(var i=1; i<=rowCount; i++)
	{
		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		
		if(id!==i)
		{
			//alert(count);
			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
    		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
       				"<td id='rowcount_" + count + "'>" + count + "</td>"+
       				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
       				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
       				"</tr>"; */
       				trData=trData+"<tr id='rowdel_" + count + "' >"+
               				"<td id='rowcount_" + count + "'>" + count + "</td>"+

               				"<td id='rowproductname_" + count + "'>" +
               				"<input type='hidden' id='rowproduct_total_amt_" + count + "' value='"+$('#rowproduct_total_amt_' + i).val()+"'>"+
               				"<input type='hidden' id='rowproduct_total_amt_with_tax" + count + "' value='"+$('#rowproduct_total_amt_with_tax' + i).val()+"'>"+
               				"<input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'>" +
               				"<center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center>" +
               				"</td>"+
               				"<td id='rowproductmrp_" + count + "'>" + $('#rowproductmrp_'+i).text() + "</td>"+	
               				"<td id='rowproductrate_" + count + "'>" + $('#rowproductrate_'+i).text() + "</td>"+
               				"<td id='rowproductqty_" + count + "'>" + $('#rowproductqty_'+i).text() + "</td>"+
               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
               				"</tr>";
    		 count++;
		}
		//alert(trData);
	} 
	$("#t1").html('');
	$("#t1").html(trData);
	TotalAmountAndTaxCalculate();
	//alert(productList.entries());
    //$('#rowdel_' + id).remove();
   // alert('productidlist '+productidlist);
	//clearRecords();
}
function updaterow(id) {
	$("#productAddId").html('<span id="productAddButtonId">Add</span><i class="material-icons right">add</i>');
    var val = $("#productId").val(); 
    var qty = $('#quantity').val();
  	var rate = $('#AmtPerUnit').val();
  	
  	var buttonStatus=$('#productAddButtonId').text();
  	var text=$("#productId option:selected").text();
    var vl=$("#productId option:selected").val();
  	
  	//alert(buttonStatus);
  	if(val==0)
  	{
  		return false;
  	}
  	if(qty==0)
  	{
  		return false;
  	}
	
  	//alert($('#rowproductkey_' + id).val());
  	var removeItem=$('#rowproductkey_' + id).val();

  	var productListTemp=[];
    for(var i=0; i<productList.length; i++)
    {
    	var value=productList[i];
    	if(value[0]!==removeItem)
    	{
    		productListTemp.push(productList[i]);
    	}
    }
    productList=[];
    for(var i=0; i<productListTemp.length; i++)
    {
    	productList.push(productListTemp[i]);
    }
  	
    var prdData=[val,qty];
  	productList.push(prdData);
  	
    //alert('#rowproductkey_'+id);
    
    //alert('removeItem '+$('#rowproductkey_' + id).val());
    //alert('productidlist '+productidlist);
    
    var rowCount = $('#t1 tr').length;
	//alert(rowCount);
	var trData="";
	count=1;
	for(var i=1; i<=rowCount; i++)
	{
		//alert($('#rowcount_'+i).html() +"---"+ $('#rowprocustname_'+i).html() +"---"+ $('#rowdelbutton_'+i).html());
		
		if(id!=i)
		{
			//alert("predata");
			//alert(i+"-(----)-"+$('#tbproductname_' + i).text());
    		 /* trData=trData+"<tr id='rowdel_" + count + "' >"+
       				"<td id='rowcount_" + count + "'>" + count + "</td>"+
       				"<td id='rowproductname_" + count + "'><input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'><center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center></td>"+
       				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
       				"</tr>"; */
       				trData=trData+"<tr id='rowdel_" + count + "' >"+
               				"<td id='rowcount_" + count + "'>" + count + "</td>"+

               				"<td id='rowproductname_" + count + "'>" +
               				"<input type='hidden' id='rowproduct_total_amt_" + count + "' value='"+$('#rowproduct_total_amt_' + i).val()+"'>"+
               				"<input type='hidden' id='rowproduct_total_amt_with_tax" + count + "' value='"+$('#rowproduct_total_amt_with_tax' + i).val()+"'>"+
               				"<input type='hidden' id='rowproductkey_" + count + "' value='"+$('#rowproductkey_' + i).val()+"'>" +
               				"<center><span id='tbproductname_" + count + "'>"+$('#tbproductname_' + i).text()+"</span></center>" +
               				"</td>"+
               				"<td id='rowproductmrp_" + count + "'>" + $('#rowproductmrp_'+i).text() + "</td>"+
               				"<td id='rowproductrate_" + count + "'>" + $('#rowproductrate_'+i).text() + "</td>"+
               				"<td id='rowproductqty_" + count + "'>" + $('#rowproductqty_'+i).text() + "</td>"+
               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
               				"</tr>";
    		 count++;
		}
		else
		{
			//alert("newdata");
			//var tempCgst=$('#cgstPer').html();
			var tempIgst=parseFloat($('#igstPer').html()).toFixed(2);
			//var tempSgst=$('#sgstPer').html();
			var ttl=( parseInt(qty) * parseFloat(rate) );
			//var amtwithtax=(parseFloat(ttl) + ( (parseFloat(ttl)*parseFloat(tempIgst))/100 )).toFixed(2);// + ( (parseFloat(ttl)*parseFloat(tempIgst))/100 ) + ( (parseFloat(ttl)*parseFloat(tempSgst))/100 ) ;
			

			var mrp=(parseFloat(rate)+((rate*parseFloat(tempIgst))/100)).toFixed(0);
			var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(tempIgst));

			var igstAmount=correctAmoutWithTaxObj.igst;//((parseFloat($('#totalAmt').val())*parseFloat($('#igstPer').html()))/100).toFixed(2);
			//var cgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#cgstPer').html()))/100;;
			//var sgstAmount=(parseFloat($('#totalAmt').val())*parseFloat($('#sgstPer').html()))/100;;
			var amtwithtax=((parseFloat(correctAmoutWithTaxObj.mrp)*parseInt(qty)).toFixed(2));
			//$('#cgst').val(cgstAmount);
			//$('#igst').val(igstAmount);
			
			
			trData=trData+"<tr id='rowdel_" + count + "' >"+
               				"<td id='rowcount_" + count + "'>" + count + "</td>"+

               				"<td id='rowproductname_" + count + "'>"+					               				
               				"<input type='hidden' id='rowproduct_total_amt_" + count + "' value='"+ttl+"'>"+
               				"<input type='hidden' id='rowproduct_total_amt_with_tax" + count + "' value='"+amtwithtax+"'>"+
               				"<input type='hidden' id='rowproductkey_" + count + "' value='"+vl+"'>" +
               				"<center><span id='tbproductname_" + count + "'>"+text+"</span></center>" +
               				"</td>"+
               				"<td id='rowproductmrp_" + count + "'>" + correctAmoutWithTaxObj.mrp + "</td>"+
               				"<td id='rowproductrate_" + count + "'>" + rate + "</td>"+
               				"<td id='rowproductqty_" + count + "'>" + qty + "</td>"+
               				"<td id='rowcountproductedit_" + count + "'><button class='btn-flat' type='button' onclick='editrow(" + count + ")'><i class='material-icons '>edit</i></button></td>"+
               				"<td id='rowdelbutton_" + count + "'><button class='btn-flat' type='button' onclick='deleterow(" + count + ")'><i class='material-icons '>clear</i></button></td>"+
               				"</tr>";
    		 count++;
		}
		//alert(trData);
	} 
	$("#t1").html('');
	$("#t1").html(trData);
	TotalAmountAndTaxCalculate();
  	//clearRecords();
  	var source=$('#productId');
	source.val(0);
	source.change();
	$('#quantity').val("");
	$('#AmtMRP').val('')
	//alert(productList.entries());
    //$('#rowdel_' + id).remove();
   // alert('productidlist '+productidlist);
	//clearRecords();
}

function TotalAmountAndTaxCalculate()
{
	totalAmount=0;
	totalAmountWithTax=0;
	
	for(var i=1; i<count; i++)
	{
		totalAmount=(parseFloat(totalAmount) + parseFloat($('#rowproduct_total_amt_'+i).val())).toFixed(2);
		totalAmountWithTax=(parseFloat(totalAmountWithTax) + parseFloat($('#rowproduct_total_amt_with_tax'+i).val())).toFixed(2);
	}
	$('#finalTotalAmount').text(totalAmount);
	$('#finalTotalAmountWithTax').text(totalAmountWithTax);
}
function clearRecords()
{
	/*var source=$('#supplierId');
	source.val(0);
	source.change();*/
	
	var source=$('#brandId');
	source.val(0);
	source.change();
	
	var source=$('#categoryId');
	source.val(0);
	source.change();

/*	var source=$('#productId');
	source.val(0);
	source.change();*/
	
	$('#hsncode').val('');									
	//$('#cgstPer').html(0);
	$('#igstPer').html(0);
	//$('#sgstPer').html(0);
	$('#totalAmt').val('');
	$('#amountWithTax').val('');
	//$('#cgst').val('');	
	$('#igst').val('');	
	//$('#sgst').val('');	
	
	$('#hsncode').change();
	//$('#cgstPer').change();
	$('#igstPer').change();
	//$('#sgstPer').change();
	$('#totalAmt').change();
	$('#amountWithTax').change();
	//$('#cgst').change();
	$('#igst').change();
	//$('#sgst').change();
	
	
	$('#AmtPerUnit').val(0);
	$('#AmtPerUnit').change();
	
	$('#oldquantity').val("");
	
	$('#quantity').val("");
	
}