var options, index, option;	
$(document).ready(function() {
						
			            $('.preloader-background').hide();
			            $('#addpincode').keypress(function(event){
			            	var key = event.which;
			        	    
			        	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
			        	        event.preventDefault();
			            });
						
						//  for no of entries and global search
						/*$('#tblDataArea').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Show: _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         "autoWidth": false,
					         "columnDefs": [
											{ "width": "2%", "targets": 0},	
											{ "width": "17%", "targets": 1},	
							                { "width": "12%", "targets": 2},
							                { "width": "17%", "targets": 3},
							                { "width": "17%", "targets": 4},
							                { "width": "17%", "targets": 5},
							                { "width": "17%", "targets": 6},
							                { "width": "4%", "targets": 7}	
							                
							              ],
					         lengthMenu: [
					             [10, 25., 50, -1],
					             ['10 ', '25 ', '50 ', 'all']
					         ],
					       
					         dom: 'lBfrtip',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                    
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                     customize: function(doc, config) {
					                    	 doc.content.forEach(function(item) {
					                    		  if (item.table) {
					                    		  item.table.widths = [50,'*',50,70,60,60,50] 
					                    		 } 
					                    		    })
					                        
					                         for customize the pdf content 
					                         doc.pageMargins = [5,20,10,5];
					                         
					                         doc.defaultStyle.fontSize = 8	;
					                         doc.styles.title.fontSize = 12;
					                         doc.styles.tableHeader.fontSize = 11;
					                         doc.styles.tableFooter.fontSize = 11;
					                         doc.styles.tableHeader.alignment = 'center';
					                         doc.styles.tableBodyEven.alignment = 'center';
					                         doc.styles.tableBodyOdd.alignment = 'center';
					                       },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title: 'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title:'Area Details',
					                     //file name 
					                     filename: 'areadetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});
						$('#tblDataRegion').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Show: _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         "autoWidth": false,
					         "columnDefs": [
											{ "width": "2%", "targets": 0},	
											{ "width": "25%", "targets": 1},	
							                { "width": "25%", "targets": 2},
							                { "width": "25%", "targets": 3},
							                { "width": "25%", "targets": 4},	
							                { "width": "2%", "targets": 5}	
							                
							              ],
					         lengthMenu: [
					             [10, 25., 50, -1],
					             ['10 ', '25 ', '50 ', 'all']
					         ],
					        
					         dom: 'lBfrtip',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'Region Deatails',
					                     //file name 
					                     filename: 'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                     customize: function(doc, config) {
					                    	 doc.content.forEach(function(item) {
					                    		  if (item.table) {
					                    		  item.table.widths = [50,'*','*','*','*'] 
					                    		 } 
					                    		    })
					                    	 
					                         for customize the pdf content 
					                         doc.pageMargins = [5,20,10,5];
					                        
					                         doc.defaultStyle.fontSize = 8	;
					                         doc.styles.title.fontSize = 12;
					                         doc.styles.tableHeader.fontSize = 11;
					                         doc.styles.tableFooter.fontSize = 11;
					                         doc.styles.tableHeader.alignment = 'center';
					                         doc.styles.tableBodyEven.alignment = 'center';
					                         doc.styles.tableBodyOdd.alignment = 'center';
					                       },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title:'Region Deatails',
					                     //file name 
					                     filename:  'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title: 'Region Deatails',
					                     //file name 
					                     filename:  'regiondetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});*/

						$('#tblDataCity').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Show: _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         "autoWidth": false,
					         "columnDefs": [
											{ "width": "2%", "targets": 0},							
							                { "width": "35%", "targets": 1},
							                { "width": "35%", "targets": 2},
							                { "width": "35%", "targets": 3},
							                { "width": "4%", "targets": 4}	
							                
							              ],
					         lengthMenu: [
					             [10, 25., 50, -1],
					             ['10 ', '25 ', '50 ', 'all']
					         ],
					         
					         dom: 'lBfrtip',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'City Details',
					                     //file name 
					                     filename:'citydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                     customize: function(doc, config) {
					                    	 doc.content.forEach(function(item) {
					                    		  if (item.table) {
					                    		  item.table.widths = ['*','*','*','*'] 
					                    		 } 
					                    		    })
					                         
					                         /*for customize the pdf content*/ 
					                         doc.pageMargins = [5,20,10,5];
					                       
					                         doc.defaultStyle.fontSize = 8	;
					                         doc.styles.title.fontSize = 12;
					                         doc.styles.tableHeader.fontSize = 11;
					                         doc.styles.tableFooter.fontSize = 11;
					                         doc.styles.tableHeader.alignment = 'center';
					                         doc.styles.tableBodyEven.alignment = 'center';
					                         doc.styles.tableBodyOdd.alignment = 'center';
					                       },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title:  'City Details',
					                     //file name 
					                     filename: 'citydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title:  'City Details',
					                     //file name 
					                     filename:'citydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});
						$('#tblDataState').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Show: _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         "autoWidth": false,
					         "columnDefs": [
											{ "width": "2%", "targets": 0},
											{ "width": "35%", "targets": 0},
							                { "width": "35%", "targets": 2},
							                { "width": "35%", "targets": 0},							               
							                { "width": "4%", "targets": 4}	
							                
							              ],
					         lengthMenu: [
					             [10, 25., 50, -1],
					             ['10 ', '25 ', '50 ', 'all']
					         ],
					       
					         dom: 'lBfrtip',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title:'State Details',
					                     //file name 
					                     filename: 'statedetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                     customize: function(doc, config) {
					                    	 doc.content.forEach(function(item) {
					                    		  if (item.table) {
					                    		  item.table.widths = ['*','*','*','*'] 
					                    		 } 
					                    		    })
					                         
					                         /*for customize the pdf content*/ 
					                         doc.pageMargins = [5,20,10,5];
					                        
					                         doc.defaultStyle.fontSize = 8	;
					                         doc.styles.title.fontSize = 12;
					                         doc.styles.tableHeader.fontSize = 11;
					                         doc.styles.tableFooter.fontSize = 11;
					                         doc.styles.tableHeader.alignment = 'center';
					                         doc.styles.tableBodyEven.alignment = 'center';
					                         doc.styles.tableBodyOdd.alignment = 'center';
					                       },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title:'State Details',
					                     //file name 
					                     filename: 'statedetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title:'State Details',
					                     //file name 
					                     filename: 'statedetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});
						$('#tblDataCountry').DataTable({
					         "oLanguage": {
					             "sLengthMenu": "Show: _MENU_",
					             "sSearch": " _INPUT_" //search
					         },
					         lengthMenu: [
					             [10, 25., 50, -1],
					             ['10 ', '25 ', '50 ', 'all']
					         ],
					         "autoWidth": false,
					         "columnDefs": [
											{ "width": "10%", "targets": 0},	
											{ "width": "40%", "targets": 1},							                
							                { "width": "10%", "targets": 2}	
							                
							              ],
					         dom: 'lBfrtip',
					         buttons: {
					             buttons: [
					                 //      {
					                 //      extend: 'pageLength',
					                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
					                 //  }, 
					                 {
					                     extend: 'pdf',
					                     className: 'pdfButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
					                     //title of the page
					                     title: 'Country Details',
					                     //file name 
					                     filename: 'countrydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                     customize: function(doc, config) {
					                    	 doc.content.forEach(function(item) {
					                    		  if (item.table) {
					                    		  item.table.widths = ['*','*'] 
					                    		 } 
					                    		    })
					                         
					                         /*for customize the pdf content*/ 
					                         doc.pageMargins = [5,20,10,5];
					                       
					                         doc.defaultStyle.fontSize = 8	;
					                         doc.styles.title.fontSize = 12;
					                         doc.styles.tableHeader.fontSize = 11;
					                         doc.styles.tableFooter.fontSize = 11;
					                         doc.styles.tableHeader.alignment = 'center';
					                         doc.styles.tableBodyEven.alignment = 'center';
					                         doc.styles.tableBodyOdd.alignment = 'center';
					                       },
					                 },
					                 {
					                     extend: 'excel',
					                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
					                     //title of the page
					                     title:  'Country Details',
					                     //file name 
					                     filename: 'countrydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'print',
					                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
					                     //title of the page
					                     title:  'Country Details',
					                     //file name 
					                     filename: 'countrydetails',
					                     //  exports only dataColumn
					                     exportOptions: {
					                         columns: ':visible.print-col'
					                     },
					                 },
					                 {
					                     extend: 'colvis',
					                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
					                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
					                     collectionLayout: 'fixed two-column',
					                     align: 'left'
					                 },
					             ]
					         }
								});
						
						 $("select")
			                .change(function() {
			                    var t = this;
			                    var content = $(this).siblings('ul').detach();
			                    setTimeout(function() {
			                        $(t).parent().append(content);
			                        $("select").material_select();
			                    }, 200);
			                });
			            $('select').material_select();
			            $('.dataTables_filter input').attr("placeholder", "  Search");
						$('#fetchCountry').click(function() {							
									
									//$("#countryList").empty();
									$('#resetCountrySubmit').click();
									var t = $('#tblDataCountry')
											.DataTable();
									t.clear().draw();
									
									$.ajax({
												type : "GET",
												url : myContextPath+"/fetchCountryList",
												/* data: "id=" + id + "&name=" + name, */
												beforeSend: function() {
													$('.preloader-background').show();
													$('.preloader-wrapper').show();
										           },

												success : function(data) {
													var srno = 1;
													//$("#tblDataCountry tbody").remove();

													//alert("data clean");

													var count = $(
															"#tblDataCountry")
															.find(
																	"tr:first th").length;
													//alert(count);

													if (data == null) {
														t.row
																.add('<tr>'
																		+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
																		+ '</tr>');
													}
													for (var i = 0, len = data.length; i < len; ++i) {
														var country = data[i];
														t.row
																.add(
																		[
																				srno,
																				country.name,
																				"<button onclick='editCountry("+country.countryId+")' id='editcountry' class='btn btn-flat'><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"

																		]).draw(false); 
														
														/*'<a href="#deleteCountry_'+country.countryId+'" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a>'
														
														
														$("#countryList")
														.append("<tr>"+
														        "<td>"+srno+"</td>"+
														        "<td>"+country.name+"</td>"+
														        "<td><button onclick='editCountry("+country.countryId+")' ><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button></td>"+	                            
														        
														    "</tr>"); */
														/*$('.modal')
																.modal();
														$("#dataAll")
																.append(
																		'<div id="deleteCountry_'+country.countryId+'" class="modal">'
																				+ ' <div class="modal-content">'
																				+ '<h4>Confirmation</h4>'
																				+ '<p>Are you sure you want to delete?</p>'
																				+ '</div>'
																				+ '<div class="modal-footer">'
																				+ '<a href=myContextPath"/fetchCountry?countryId='
																				+ country.countryId
																				+ '" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>'
																				+ '<a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>'
																				+ '</div>'
																				+ '</div>');*/

														srno = srno + 1;
													}
													$('.preloader-wrapper').hide();
													$('.preloader-background').hide();
													
												}
											});

								});
						
						$("#saveCountrySubmit")
						.click(
								function() {
									//alert("go to save");
									//alert("go to save "+form.attr('method')+"  "+form.attr('action'));
									
									if($('#addCountry').val().trim()==="")
										{
										Materialize.toast('Please Enter Country to add!', '4000', 'teal lighten-2');
										/*$('#addeditmsg').find("#modalType").addClass("warning");
										$('#addeditmsg').find(".modal-action").addClass("red lighten-2");
											$('#addeditmsg').modal('open');*/
											/*$('#msgHead').text("Error is : ");*/
											//$('#msg').text("Enter Country Name First For Save Country");
											return false;
										}
									
									var form = $('#saveCountryForm');

									$.ajax({

												type : form
														.attr('method'),

												url : form
														.attr('action'),
												beforeSend: function() {
													$('.preloader-background').show();
													$('.preloader-wrapper').show();
										           },
												/* dataType : "json", */
												data : $("#saveCountryForm")
														.serialize(),
												success : function(data) {
													
														//alert("Country Saved ");
														
														//alert(data);
														if(data=="Success")
														{
															$('#addeditmsg').find("#modalType").addClass("success");
															$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																$('#addeditmsg').modal('open');
																/*$('#msgHead').text("Success : ");*/
																
																var countryId=$('#addCountryId').val();
																if(countryId==0){
																	$('#msg').text("Country Saved SuccessFully");
																}else{
																	$('#msg').text("Country Updated SuccessFully");
																}	
																
																$('#addCountry').val('');
																
															//reset country tab
															$('#resetCountrySubmit').click();
															$('#fetchCountry').click();
														}
														else
														{
															$('#addeditmsg').find("#modalType").addClass("success");
															$('#addeditmsg').find(".modal-action").removeClass("teal lighten-2").addClass("red lighten-2 teal");
															
															$('#addeditmsg').modal('open');
															/*$('#msgHead').text("Success : ");*/
															$('#msg').text("Country "+data);
														}
															
													
														$('.preloader-wrapper').hide();
														$('.preloader-background').hide();
								    				},
													error: function(xhr, status, error) {
														$('.preloader-wrapper').hide();
														$('.preloader-background').hide();
														
														$('#addeditmsg').find("#modalType").addClass("success");
														$('#addeditmsg').find(".modal-action").removeClass("teal lighten-2").addClass("red lighten-2 teal");
														$('#addeditmsg').modal('open');
														$('#msgHead').text("Error : ");
														$('#msg').text("Something Went Wrong");
													}
											});
									return false;
								});
						$('#resetCountrySubmit').click(function() {
							$('#addCountryId').val('0');
							$('#addCountry').val('');
							$("#saveCountrySubmit").html('<i class="material-icons left">add</i> Add');
						});
						//state
						//fetchCountryList
								$('#statework').click(function() {
									$('#resetStateSubmit').click();
								// Get the raw DOM object for the select box
								var select = document.getElementById('countryListForState');

								// Clear the old options
								select.options.length = 0;
								//$('#countryListForState').html('');

								//Load the new options

								select.options.add(new Option("Choose Country", 0));
								$.ajax({
									url : myContextPath+"/fetchCountryList",
									dataType : "json",
									success : function(data) {

										/* alert(data); */
										var options, index, option;
										select = document.getElementById('countryListForState');

										for (var i = 0, len = data.length; i < len; ++i) {
											var country = data[i];
											select.options.add(new Option(country.name, country.countryId));
										}
										
										fetchallstate();
										$("#countryListForState > option").each(function() {
											 
											   if(this.text==="India")
											   {
											    //alert(this.text + ' ' + this.value);
											    var v=this.value;
											    var source=$('#countryListForState');
												source.val(v);
												source.change();
											   }
											});	
										/* for (index = 0; index < options.length; ++index) {
										  option = options[index];
										  select.options.add(new Option(option.name, option.cityId));
										} */
									}
								});

							});
						
								$("#saveStateSubmit")
								.click(
										function() {
											//alert("go to save");
											//alert("state go to save "+$("#saveStateForm").serialize());
											
											if($('#addState').val()==="")
												{
												Materialize.toast('Enter State Name First For Save State!', '4000', 'teal lighten-2');
													/*$('#addeditmsg').modal('open');
													$('#msgHead').text("Error is : ");
													$('#msg').text("Enter State Name First For Save State");*/
													return false;
												}
											if($("#countryListForState").val()==0)
											{
												Materialize.toast('Select Country First For Save State!', '4000', 'teal lighten-2');
												/*$('#addeditmsg').modal('open');
												$('#msgHead').text("Error is : ");
												$('#msg').text("Select Country First For Save State");*/
												return false;
											}
											if($('#addStateCodeId').val()==="")
											{
												Materialize.toast('Enter State Code First For Save State!', '4000', 'teal lighten-2');
												/*$('#addeditmsg').modal('open');
												$('#msgHead').text("Error is : ");
												$('#msg').text("Enter State Code First For Save State");*/
												return false;
											}
											
											var form = $('#saveStateForm');

											$.ajax({

														type : form
																.attr('method'),

														url : form
																.attr('action'),
														beforeSend: function() {
															$('.preloader-background').show();
															$('.preloader-wrapper').show();
												           },
														/* dataType : "json", */
														data : $("#saveStateForm").serialize(),
														success : function(data) {
															
																//alert("Country Saved ");
																
																//alert(data);
																if(data=="Success")
																{
																	$('#addeditmsg').find("#modalType").addClass("success");
																	$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																		$('#addeditmsg').modal('open');
																		//$('#msgHead').text("Success : ");
																		var stateId=$('#addStateId').val();
																		if(stateId==0){
																			$('#msg').text("State Saved SuccessFully");
																		}else{
																			$('#msg').text("State Updated SuccessFully");
																		}	
																		
																		var source = $("#countryListForState");
																		var v=0;
																		source.val(v);
																		source.change();
																		$('#addState').val('');
																		$('#addStateCodeId').val('');
																		
																		//reset state tab 
																		$('#saveStateSubmit').html("<i class='material-icons left'>add</i>Add");
																		$('#statework').click();
																		var source = $("#countryListForState");
																		var v=0;
																		source.val(v);
																		source.change();
																		$('#addState').val('');
																		$('#addStateCodeId').val('');
																}
																else																	
																{
																	$('#addeditmsg').find("#modalType").addClass("success");
																	$('#addeditmsg').find(".modal-action").removeClass("teal lighten-2").addClass("red lighten-2 teal");
																	$('#addeditmsg').modal('open');
																	$('#msgHead').text("Success : ");
																	$('#msg').text("State "+data);
																	
																
																}
																	
															
																$('.preloader-wrapper').hide();
																$('.preloader-background').hide();
										    				},
															error: function(xhr, status, error) {
																$('.preloader-wrapper').hide();
																$('.preloader-background').hide();
																  //alert(error +"---"+ xhr+"---"+status);
																$('#addeditmsg').find("#modalType").addClass("success");
																$('#addeditmsg').find(".modal-action").removeClass("teal lighten-2").addClass("red lighten-2 teal");
																$('#addeditmsg').modal('open');
																$('#msgHead').text("Error : ");
																$('#msg').text("Something Went Wrong");
															}
													});
											return false;
										});
								
								$('#resetStateSubmit').click(function() {
									
									
									$("#countryListForState > option").each(function() {
									 
									   if(this.text==="India")
									   {
									    //alert(this.text + ' ' + this.value);
									    var v=this.value;
									    var source=$('#countryListForState');
										source.val(v);
										source.change();
									   }
									});					
									
									
									
									$('#addStateId').val('0');
									$('#addState').val('');
									$('#addStateCodeId').val('');
									$('#addState').change();
									$('#addStateCodeId').change();
									$("#saveStateSubmit").html('<i class="material-icons left">add</i> Add');
									
									
								});
						
								//city								
								//fetchCountryList
								//fetchStateList
										$('#citywork').click(function() {
											$('#resetCitySubmit').click();
										// Get the raw DOM object for the select box
										var select = document.getElementById('countryListForCity');

										// Clear the old options
										select.options.length = 0;
										//$('#countryListForState').html('');

										//Load the new options

										select.options.add(new Option("Choose Country", 0));
										$.ajax({
											url :myContextPath+ "/fetchCountryList",
											dataType : "json",
											success : function(data) {

												/* alert(data); */
												var options, index, option;
												select = document.getElementById('countryListForCity');

												for (var i = 0, len = data.length; i < len; ++i) {
													var country = data[i];
													select.options.add(new Option(country.name, country.countryId));
												}
												
												fetchallcity();
												$("#countryListForCity > option").each(function() {
													 
													   if(this.text==="India")
													   {
													   // alert(this.text + ' ' + this.value);
													    var v=this.value;
													    var source=$('#countryListForCity');
														source.val(v);
														source.change();
													   }
													});
												
												setTimeout(
														  function() 
														  {
															  $("#stateListForCity > option").each(function() 
																{
																	 
																   if(this.text==="Maharashtra")
																   {
																    var v=this.value;
																    var source=$('#stateListForCity');
																	source.val(v);
																	source.change();
																   }
																});
														  }, 1000);
												/* for (index = 0; index < options.length; ++index) {
												  option = options[index];
												  select.options.add(new Option(option.name, option.cityId));
												} */
											}
										});										
									

									});
								
								//saveCity
										$("#saveCitySubmit")
										.click(
												function() {
													//alert("go to save");
													//alert("go to save "+form.attr('method')+"  "+form.attr('action'));
													if($("#countryListForCity").val()==0)
													{
														Materialize.toast('Select Country First For Save City!', '4000', 'teal lighten-2');
														/*$('#addeditmsg').modal('open');
														$('#msgHead').text("Error is : ");
														$('#msg').text("Select Country First For Save City");*/
														return false;
													}
													if($("#stateListForCity").val()==0)
													{
														Materialize.toast('Select State First For Save City!', '4000', 'teal lighten-2');
														/*$('#addeditmsg').modal('open');
														$('#msgHead').text("Error is : ");
														$('#msg').text("Select State First For Save City");*/
														return false;
													}
													if($('#addCity').val()==="")
														{
														Materialize.toast('Enter City Name First For Save City!', '4000', 'teal lighten-2');
															/*$('#addeditmsg').modal('open');
															$('#msgHead').text("Error is : ");
															$('#msg').text("Enter City Name First For Save City");*/
															return false;
														}
													
													
													
													
													var form = $('#saveCityForm');

													$.ajax({

																type : form
																		.attr('method'),

																url : form
																		.attr('action'),
																beforeSend: function() {
																	$('.preloader-background').show();
																	$('.preloader-wrapper').show();
														           },
																/* dataType : "json", */
																data : $("#saveCityForm")
																		.serialize(),
																success : function(data) {
																	
																		//alert("Country Saved ");
																		
																		//alert(data);
																		if(data=="Success")
																		{
																			$('#addeditmsg').find("#modalType").addClass("success");
																			$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																				$('#addeditmsg').modal('open');
																				$('#msgHead').text("Success : ");

																				var cityId=$('#addCityId').val();
																				if(cityId==0){
																					$('#msg').text("City Saved SuccessFully");
																				}else{
																					$('#msg').text("City Updated SuccessFully");
																				}	
																				
																				//$('#resetCitySubmit').click();
																				
																				//reset city tab
																				$('#citywork').click();	
																				
																		}
																		else
																		{
																			$('#addeditmsg').find("#modalType").addClass("success");
																			$('#addeditmsg').find(".modal-action").removeClass("teal lighten-2").addClass("red lighten-2 teal");
																			$('#addeditmsg').modal('open');
																			$('#msgHead').text("Success : ");
																			$('#msg').text("City "+data);
																		}
																		
																	
																		$('.preloader-wrapper').hide();
																		$('.preloader-background').hide();
												    				},
																	error: function(xhr, status, error) {
																		$('.preloader-wrapper').hide();
																		$('.preloader-background').hide();
																		  //alert(error +"---"+ xhr+"---"+status);
																		$('#addeditmsg').find("#modalType").addClass("success");
																		$('#addeditmsg').find(".modal-action").removeClass("teal lighten-2").addClass("red lighten-2 teal");
																		$('#addeditmsg').modal('open');
																		$('#msgHead').text("Error : ");
																		$('#msg').text("Something Went Wrong");
																	}
															});
													return false;
												});
										
										
										$('#countryListForCity').change(function() {
											
											
											
											// Get the raw DOM object for the select box
											var select = document.getElementById('stateListForCity');
											
											// Clear the old options
											select.options.length = 0;
											//$('#countryListForState').html('');

											//Load the new options

											select.options.add(new Option("Choose State", 0));
											$.ajax({
												url : myContextPath+"/fetchStateListByCountryId?countryId="+$('#countryListForCity').val(),
												dataType : "json",
												success : function(data) {

													/* alert(data); */
													var options, index, option;
													select = document.getElementById('stateListForCity');

													for (var i = 0, len = data.length; i < len; ++i) {
														var state = data[i];
														//alert(state.name+" "+ state.stateId);
														select.options.add(new Option(state.name, state.stateId));
													}
													
													/* for (index = 0; index < options.length; ++index) {
													  option = options[index];
													  select.options.add(new Option(option.name, option.cityId));
													} */
												}
											});		
											
										});
										
										$('#resetCitySubmit').click(function() {
											
											
											$("#countryListForCity > option").each(function() {
											 
											   if(this.text==="India")
											   {
											   // alert(this.text + ' ' + this.value);
											    var v=this.value;
											    var source=$('#countryListForCity');
												source.val(v);
												source.change();
											   }
											});
											
											
											var source = $("#stateListForCity");
											var v=0;
											source.val(v);
											source.change();
											
											$('#addCityId').val('0');
											$('#addCity').val('');
											$("#saveCitySubmit").html('<i class="material-icons left">add</i> Add');
											
											
										});
										/*$(".editcity").click(function(){
											alert("123");
											$("#saveCitySubmit").text("Update");
										});*/

										//city								
										//fetchCountryList
										//fetchStateList
												/*$('#regionwork').click(function() {
													$('#resetCitySubmit').click();
												// Get the raw DOM object for the select box
												var select = document.getElementById('countryListForRegion');
												
												// Clear the old options
												select.options.length = 0;
												//$('#countryListForState').html('');

												//Load the new options

												select.options.add(new Option("Choose Country", 0));
												$.ajax({
													url :myContextPath+ "/fetchCountryList",
													dataType : "json",
													success : function(data) {

														 alert(data); 
														var options, index, option;
														select = document.getElementById('countryListForRegion');

														for (var i = 0, len = data.length; i < len; ++i) {
															var country = data[i];
															select.options.add(new Option(country.name, country.countryId));
														}
														
														fetchallregion();
														$("#countryListForRegion > option").each(function() {
															 
															   if(this.text==="India")
															   {
															   // alert(this.text + ' ' + this.value);
															    var v=this.value;
															    var source=$('#countryListForRegion');
																source.val(v);
																source.change();
															   }
															});
														setTimeout(
																  function() 
																  {
																	  $("#stateListForRegion > option").each(function() 
																		{
																			 
																		   if(this.text==="Maharashtra")
																		   {
																		    var v=this.value;
																		    var source=$('#stateListForRegion');
																			source.val(v);
																			source.change();
																		   }
																		});
																  }, 1000);
														 for (index = 0; index < options.length; ++index) {
														  option = options[index];
														  select.options.add(new Option(option.name, option.cityId));
														} 
													}
												});										
										
											});

												$('#countryListForRegion').change(function() {
													
													// Get the raw DOM object for the select box
													var select = document.getElementById('stateListForRegion');

													// Clear the old options
													select.options.length = 0;
													//$('#countryListForState').html('');

													//Load the new options

													select.options.add(new Option("Choose State", 0));
													$.ajax({
														url : myContextPath+"/fetchStateListByCountryId?countryId="+$('#countryListForRegion').val(),
														dataType : "json",
														success : function(data) {

															 alert(data); 
															var options, index, option;
															select = document.getElementById('stateListForRegion');

															for (var i = 0, len = data.length; i < len; ++i) {
																var state = data[i];
																//alert(state.name+" "+ state.stateId);
																select.options.add(new Option(state.name, state.stateId));
															}
															
															 for (index = 0; index < options.length; ++index) {
															  option = options[index];
															  select.options.add(new Option(option.name, option.cityId));
															} 
														}
													});		
													
												});
												
													$('#stateListForRegion').change(function() {
													
													// Get the raw DOM object for the select box
													var select = document.getElementById('cityListForRegion');

													// Clear the old options
													select.options.length = 0;
													//$('#countryListForState').html('');

													//Load the new options

													select.options.add(new Option("Choose City", 0));
													$.ajax({
														url : myContextPath+"/fetchCityListByStateId?stateId="+$('#stateListForRegion').val(),
														dataType : "json",
														success : function(data) {

															 alert(data); 
															var options, index, option;
															select = document.getElementById('cityListForRegion');

															for (var i = 0, len = data.length; i < len; ++i) {
																var city = data[i];
																//alert(state.name+" "+ state.stateId);
																select.options.add(new Option(city.name, city.cityId));
															}
															
															 for (index = 0; index < options.length; ++index) {
															  option = options[index];
															  select.options.add(new Option(option.name, option.cityId));
															} 
														}
													});		
													
												});
												
												//saveCity
												$("#saveRegionSubmit")
												.click(
														function() {
															//alert("go to save");
															//alert("go to save "+form.attr('method')+"  "+form.attr('action'));
															if($("#cityListForRegion").val()==0)
															{
																Materialize.toast('Select City First For Save City!', '4000', 'teal lighten-2');
																$('#addeditmsg').modal('open');
																$('#msgHead').text("Error is : ");
																$('#msg').text("Select City First For Save City");
																return false;
															}
															if($("#countryListForRegion").val()==0)
															{
																Materialize.toast('Select Country First For Save City!', '4000', 'teal lighten-2');
																$('#addeditmsg').modal('open');
																$('#msgHead').text("Error is : ");
																$('#msg').text("Select Country First For Save City");
																return false;
															}
															if($("#stateListForRegion").val()==0)
															{
																Materialize.toast('Select State First For Save City!', '4000', 'teal lighten-2');
																$('#addeditmsg').modal('open');
																$('#msgHead').text("Error is : ");
																$('#msg').text("Select State First For Save City");
																return false;
															}
															if($('#addRegion').val()==="")
																{
																Materialize.toast('Enter Region Name First For Save City!', '4000', 'teal lighten-2');
																	$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Enter Region Name First For Save City");
																	return false;
																}
															
															
															
															
															var form = $('#saveRegionForm');

															$.ajax({

																		type : form
																				.attr('method'),

																		url : form
																				.attr('action'),
																		beforeSend: function() {
																			$('.preloader-background').show();
																			$('.preloader-wrapper').show();
																           },
																		 dataType : "json", 
																		data : $("#saveRegionForm")
																				.serialize(),
																		success : function(data) {
																			
																				//alert("Country Saved ");
																				
																				//alert(data);
																				if(data=="Success")
																				{		$('#addeditmsg').find("#modalType").addClass("success");
																				$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																						$('#addeditmsg').modal('open');
																						$('#msgHead').text("Success : ");
																						$('#msg').text("Region Saved SuccessFully");
																						
																						$('#resetRegionSubmit').click();
																						
																				}
																				else
																				{
																					$('#addeditmsg').find("#modalType").addClass("success");
																					$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																					$('#addeditmsg').modal('open');
																					$('#msgHead').text("Success : ");
																					$('#msg').text("Region "+data);
																				}
																				$('#regionwork').click();	
																			
																		},
																	error: function(xhr, status, error) {
																		$('.preloader-wrapper').hide();
																		$('.preloader-background').hide();
																		  //alert(error +"---"+ xhr+"---"+status);
																		Materialize.toast('Something went wrong!', '4000', 'teal lighten-2');
																		$('#addeditmsg').modal('open');
												               	     	$('#msgHead').text("Message : ");
												               	     	$('#msg').text("Something Went Wrong"); 
												               	     		setTimeout(function() 
																			  {
												      	     					$('#addeditmsg').modal('close');
																			  }, 1000);
																		}
																	});
															return false;
														});
												
												$('#resetRegionSubmit').click(function() {
													
													
													$("#countryListForRegion > option").each(function() {
													 
													   if(this.text==="India")
													   {
													   // alert(this.text + ' ' + this.value);
													    var v=this.value;
													    var source=$('#countryListForRegion');
														source.val(v);
														source.change();
													   }
													});
													
													
													var source = $("#stateListForRegion");
													var v=0;
													source.val(v);
													source.change();
													
													var source1 = $("#cityListForRegion");
													var v1=0;
													source1.val(v1);
													source1.change();
													
													$('#addRegionId').val('0');
													$('#addRegion').val('');
													$("#saveRegionSubmit").html('<i class="material-icons left">add</i> Add');

													
												});
												
												$('#areawork').click(function() {
													$('#resetAreaSubmit').click();
												// Get the raw DOM object for the select box
												var select = document.getElementById('countryListForArea');
												
												// Clear the old options
												select.options.length = 0;
												//$('#countryListForState').html('');

												//Load the new options

												select.options.add(new Option("Choose Country", 0));
												$.ajax({
													url :myContextPath+ "/fetchCountryList",
													dataType : "json",
													success : function(data) {

														 alert(data); 
														var options, index, option;
														select = document.getElementById('countryListForArea');

														for (var i = 0, len = data.length; i < len; ++i) {
															var country = data[i];
															select.options.add(new Option(country.name, country.countryId));
														}
														
														fetchallarea();
														$("#countryListForArea > option").each(function() {
															 
															   if(this.text==="India")
															   {
															   // alert(this.text + ' ' + this.value);
															    var v=this.value;
															    var source=$('#countryListForArea');
																source.val(v);
																source.change();
															   }
															});
														setTimeout(
																  function() 
																  {
																	  $("#stateListForArea > option").each(function() 
																		{
																			 
																		   if(this.text==="Maharashtra")
																		   {
																		    var v=this.value;
																		    var source=$('#stateListForArea');
																			source.val(v);
																			source.change();
																		   }
																		});
																  }, 1000);
														 for (index = 0; index < options.length; ++index) {
														  option = options[index];
														  select.options.add(new Option(option.name, option.cityId));
														} 
													}
												});										
										
											});
												
												
													$('#countryListForArea').change(function() {
													
													// Get the raw DOM object for the select box
													var select = document.getElementById('stateListForArea');

													// Clear the old options
													select.options.length = 0;
													//$('#countryListForState').html('');

													//Load the new options

													select.options.add(new Option("Choose State", 0));
													$.ajax({
														url : myContextPath+"/fetchStateListByCountryId?countryId="+$('#countryListForArea').val(),
														dataType : "json",
														success : function(data) {

															 alert(data); 
															var options, index, option;
															select = document.getElementById('stateListForArea');

															for (var i = 0, len = data.length; i < len; ++i) {
																var state = data[i];
																//alert(state.name+" "+ state.stateId);
																select.options.add(new Option(state.name, state.stateId));
															}
															
															if(editClicked){
																 var source1 = $("#stateListForArea");
																	var v=area.region.city.state.stateId;
																	source1.val(v);
																	source1.change();
															}
															
															 for (index = 0; index < options.length; ++index) {
															  option = options[index];
															  select.options.add(new Option(option.name, option.cityId));
															} 
														}
													});		
													
												});
												
													$('#stateListForArea').change(function() {
													
													// Get the raw DOM object for the select box
													var select = document.getElementById('cityListForArea');

													// Clear the old options
													select.options.length = 0;
													//$('#countryListForState').html('');

													//Load the new options

													select.options.add(new Option("Choose City", 0));
													$.ajax({
														url : myContextPath+"/fetchCityListByStateId?stateId="+$('#stateListForArea').val(),
														dataType : "json",
														success : function(data) {

															 alert(data); 
															var options, index, option;
															select = document.getElementById('cityListForArea');

															for (var i = 0, len = data.length; i < len; ++i) {
																var city = data[i];
																//alert(state.name+" "+ state.stateId);
																select.options.add(new Option(city.name, city.cityId));
															}
															
															 for (index = 0; index < options.length; ++index) {
															  option = options[index];
															  select.options.add(new Option(option.name, option.cityId));
															} 
														}
													});		
													
												});
													
													
													$('#cityListForArea').change(function() {
														
														// Get the raw DOM object for the select box
														var select = document.getElementById('regionListForArea');

														// Clear the old options
														select.options.length = 0;
														//$('#countryListForState').html('');

														//Load the new options

														select.options.add(new Option("Choose Region", 0));
														$.ajax({
															url : myContextPath+"/fetchRegionListByCityId?cityId="+$('#cityListForArea').val(),
															dataType : "json",
															success : function(data) {

																 alert(data); 
																var options, index, option;
																select = document.getElementById('regionListForArea');

																for (var i = 0, len = data.length; i < len; ++i) {
																	var region = data[i];
																	//alert(state.name+" "+ state.stateId);
																	select.options.add(new Option(region.name, region.regionId));
																}
																
																 for (index = 0; index < options.length; ++index) {
																  option = options[index];
																  select.options.add(new Option(option.name, option.cityId));
																} 
															}
														});		
														
													});

													//saveCity
													$("#saveAreaSubmit")
													.click(
															function() {
																//alert("go to save");
																//alert("go to save "+form.attr('method')+"  "+form.attr('action'));
																if($("#regionListForArea").val()==0)
																{
																	Materialize.toast('Select Region First For Save Area!', '4000', 'teal lighten-2');
																	$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Select Region First For Save Area");
																	return false;
																}
																if($("#cityListForArea").val()==0)
																{
																	Materialize.toast('Select City First For Save Area!', '4000', 'teal lighten-2');
																	$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Select City First For Save Area");
																	return false;
																}
																if($("#countryListForArea").val()==0)
																{
																	Materialize.toast('Select Country First For Save Area!', '4000', 'teal lighten-2');
																	$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Select Country First For Save Area");
																	return false;
																}
																if($("#stateListForArea").val()==0)
																{
																	Materialize.toast('Select State First For Save Area!', '4000', 'teal lighten-2');
																	$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Select State First For Save Area");
																	return false;
																}
																if($('#addArea').val()==="")
																{
																	Materialize.toast('Enter Area Name First For Save Area!', '4000', 'teal lighten-2');
																	$('#addeditmsg').modal('open');
																	$('#msgHead').text("Error is : ");
																	$('#msg').text("Enter Area Name First For Save Area");
																	return false;
																}
																
																
																var form = $('#saveAreaForm');

																$.ajax({

																			type : form
																					.attr('method'),

																			url : form
																					.attr('action'),
																			beforeSend: function() {
																				$('.preloader-background').show();
																				$('.preloader-wrapper').show();
																	           },
																			 dataType : "json", 
																			data : $("#saveAreaForm")
																					.serialize(),
																					
																			success : function(data) {
																				
																					//alert("Country Saved ");
																					
																					//alert(data);
																					if(data=="Success")
																					{
																						$('#addeditmsg').find("#modalType").addClass("success");
																						$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																							$('#addeditmsg').modal('open');
																							$('#msgHead').text("Success : ");
																							$('#msg').text("Area Saved SuccessFully");
																							
																							$('#resetAreaSubmit').click();
																							
																					}
																					else
																					{
																						$('#addeditmsg').find("#modalType").addClass("success");
																						$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
																						$('#addeditmsg').modal('open');
																						$('#msgHead').text("Success : ");
																						$('#msg').text("Area "+data);
																					}
																					$('#areawork').click();	
																					$('.preloader-wrapper').hide();
																					$('.preloader-background').hide();
																				
																			},
																		error: function(xhr, status, error) {
																			$('.preloader-wrapper').hide();
																			$('.preloader-background').hide();
																			  //alert(error +"---"+ xhr+"---"+status);
																			Materialize.toast('Something went wrong!', '4000', 'teal lighten-2');
																			$('#addeditmsg').find("#modalType").addClass("warning");
																			$('#addeditmsg').find(".modal-action").addClass("red lighten-2");
																			$('#addeditmsg').modal('open');
													               	     	$('#msgHead').text("Message : ");
													               	     	$('#msg').text("Something Went Wrong"); 
													               	     		setTimeout(function() 
																				  {
													      	     					$('#addeditmsg').modal('close');
																				  }, 1000);
																			
																			}
																		});
																return false;
															});
													
													$('#resetAreaSubmit').click(function() {
														
														
														$("#countryListForArea > option").each(function() {
														 
														   if(this.text==="India")
														   {
														   // alert(this.text + ' ' + this.value);
														    var v=this.value;
														    var source=$('#countryListForArea');
															source.val(v);
															source.change();
														   }
														});
														
														
														var source = $("#stateListForArea");
														var v=0;
														source.val(v);
														source.change();
														
														var source1 = $("#cityListForArea");
														var v1=0;
														source1.val(v1);
														source1.change();
														
														var source2 = $("#regionListForArea");
														var v2=0;
														source2.val(v2);
														source1.change();
														
														$('#addAreaId').val('0');
														$('#addArea').val('');
														$('#addpincode').val('');
														$("#saveAreaSubmit").html('<i class="material-icons left">add</i> Add');
												
														
													});*/
													$('#citywork').click();
													
					});

	function editCountry(id) {
		//alert(id);
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchCountry?countryId="+id,
			/* data: "id=" + id + "&name=" + name, */
			success : function(data) {
				var user = data;
				$('#addCountry').focus();
				$('#addCountryId').val(user.countryId);
				$('#addCountry').val(user.name);
				$("#saveCountrySubmit").html('<i class="material-icons left">send</i> Update');
			}

		});
	}
	
	function editState(id) {
		//alert(id);
		$("#saveStateSubmit").html('<i class="material-icons left">send</i> update');
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchState?stateId="+id,
			/* data: "id=" + id + "&name=" + name, */
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				var state = data;
				$('#addState').focus();
				$('#addStateId').val(state.stateId);
				$('#addState').val(state.name);
				$('#addStateCodeId').val(state.code);
				$('#addStateCodeId').change();
				$('#saveStateSubmit').html("<i class='material-icons left'>send</i>Update");
				
				$("#countryListForState option").each(function()
						{
						 
						    	//$('#countryListForState option[value='+state.country.countryId+']').attr("selected",true);
						    	
						    	var source = $("#countryListForState");
								var v=state.country.countryId;
								source.val(v);
								source.change();
						    	
						    	/* //remove selected one
						    	$('option:selected', 'select[name="options"]').removeAttr('selected');
						    	//Using the value
						    	$('select[name="options"]').find('option[value="3"]').attr("selected",true);
						    	 */
								$('.preloader-background').hide();
								$('.preloader-wrapper').hide();
						    	return false;
						    	
						});
			
			
			}

		});
	}
	
	function editCity(id) {
		//alert(id);
		$("#saveCitySubmit").html('<i class="material-icons left">send</i> update');
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchCity?cityId="+id,
			/* data: "id=" + id + "&name=" + name, */
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				var city = data;
				$('#addCity').focus();
				$('#addCityId').val(city.cityId);
				$('#addCity').val(city.name);		
				//alert(city.state.country.countryId);
		    	var source = $("#countryListForCity");
				var v1=city.state.country.countryId;
				source.val(v1);
				source.change();		
				//alert($("#countryListForCity").val());
			
				//alert(document.getElementById('stateListForCity').options.length);
				setTimeout(
						  function() 
						  {
						    //do something special
							  var source1 = $("#stateListForCity");
								var v=city.state.stateId;
								source1.val(v);
								source1.change();
								$('.preloader-background').hide();
								$('.preloader-wrapper').hide();
								//alert(v);
								//alert($("#stateListForCity").val());
						  }, 1000);
				
				
			}

		});
	}
	
	/*function editRegion(id) {
		//alert(id);
	
		$("#saveRegionSubmit").html('<i class="material-icons left">send</i> update');
	
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchRegion?regionId="+id,
			 data: "id=" + id + "&name=" + name, 
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				var region = data;
				$('#addRegion').focus();
				$('#addRegionId').val(region.regionId);
				$('#addRegion').val(region.name);								 
				
				//alert($('#addRegionId').val());
				
		    	var source = $("#countryListForRegion");
				var v1=region.city.state.country.countryId;
				source.val(v1);
				source.change();		
				//alert($("#countryListForCity").val());
			
				setTimeout(
						  function() 
						  {
						    //do something special
							  var source1 = $("#stateListForRegion");
								var v=region.city.state.stateId;
								source1.val(v);
								source1.change();
								//alert(v);
								//alert($("#stateListForCity").val());
						  }, 1000);
				setTimeout(
						  function() 
						  {
						    //do something special
							  //alert("city");
							  var source2 = $("#cityListForRegion");
								var v2=region.city.cityId;
								source2.val(v2);
								source2.change();
								$('.preloader-background').hide();
								$('.preloader-wrapper').hide();
								//alert(v2);
								//alert($("#stateListForCity").val());
						  }, 2000);
				
				
				
			}

		});
	}

	
	function editArea(id) {
		
		//alert(id);
		$("#saveAreaSubmit").html('<i class="material-icons left">send</i> update');
		
		$.ajax({
			type : "GET",
			url : myContextPath+"/fetchArea?areaId="+id,
			 data: "id=" + id + "&name=" + name, 
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				var area = data;
				$('#addArea').focus();
				$('#addAreaId').val(area.areaId);
				$('#addArea').val(area.name);								 
				$('#addpincode').val(area.pincode);
				$('#addpincode').focus();
				$('#addArea').focus();
				//alert($('#addRegionId').val());
				
		    	var source = $("#countryListForArea");
				var v1=area.region.city.state.country.countryId;
				source.val(v1);
				source.change();		
				//alert($("#countryListForCity").val());
			
				setTimeout(
						  function() 
						  {
						    //do something special
							  var source1 = $("#stateListForArea");
								var v=area.region.city.state.stateId;
								source1.val(v);
								source1.change();
								//alert(v);
								//alert($("#stateListForCity").val());
						  }, 1000);
				setTimeout(
						  function() 
						  {
						    //do something special
							 // alert("city");
							  var source2 = $("#cityListForArea");
								var v2=area.region.city.cityId;
								source2.val(v2);
								source2.change();
								//alert(v2);
								//alert($("#stateListForCity").val());
						  }, 2000);
				setTimeout(
						  function() 
						  {
						    //do something special
							 // alert("city");
							  var source3 = $("#regionListForArea");
								var v3=area.region.regionId;
								source3.val(v3);
								source3.change();
								//alert(v2);
								//alert($("#stateListForCity").val());
								$('.preloader-background').hide();
								$('.preloader-wrapper').hide();
								
						  }, 3000);
			
			}

		});
	}*/
	
	function fetchallstate()
	{
		var t = $('#tblDataState').DataTable();
		t.clear().draw();
		$
				.ajax({
					type : "GET",
					url : myContextPath+"/fetchStateList",
					/* data: "id=" + id + "&name=" + name, */
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var srno = 1;														

						var count = $(
								"#tblDataState")
								.find(
										"tr:first th").length;
						
						if (data == null) {
							t.row
									.add('<tr>'
											+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
											+ '</tr>');
						}
						for (var i = 0, len = data.length; i < len; ++i) {
							var state = data[i];
							t.row
									.add(	[
													srno,
													state.name,
													state.code,
													state.country.name,
													"<button onclick='editState("+state.stateId+")' id='editstate' class='btn btn-flat'><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"
	
											])
									.draw(false); 															

							srno = srno + 1;
						}
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();

					}
				});	
		
	}
	
	function fetchallcity()
	{
		var t = $('#tblDataCity').DataTable();
		t.clear().draw();
		$
				.ajax({
					type : "GET",
					url : myContextPath+"/fetchCityList",
					/* data: "id=" + id + "&name=" + name, */
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var srno = 1;														

						var count = $(
								"#tblDataCity")
								.find(
										"tr:first th").length;
						
						if (data == null) {
							t.row.add('<tr>'
											+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
											+ '</tr>');
						}
						for (var i = 0, len = data.length; i < len; ++i) {
							var city = data[i];
							t.row
									.add(
											[
													srno,
													city.name,
													city.state.name,
													city.state.country.name,
													"<button onclick='editCity("+city.cityId+")' class='editcity btn btn-flat' ><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"

											])
									.draw(
											false); 															

							srno = srno + 1;
						}
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();

					}
				});	
		
	}
	

/*	function fetchallregion()
	{
		var t = $('#tblDataRegion').DataTable();
		t.clear().draw();
		$
				.ajax({
					type : "GET",
					url : myContextPath+"/fetchRegionList",
					 data: "id=" + id + "&name=" + name, 
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var srno = 1;														

						var count = $(
								"#tblDataRegion")
								.find(
										"tr:first th").length;
						
						if (data == null) {
							t.row
									.add('<tr>'
											+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
											+ '</tr>');
						}
						for (var i = 0, len = data.length; i < len; ++i) {
							var region = data[i];
							t.row
									.add(
											[
													srno,
													region.name,
													region.city.name,
													region.city.state.name,
													region.city.state.country.name,
													"<button onclick='editRegion("+region.regionId+")' class='editregion btn btn-flat' ><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"

											])
									.draw(
											false); 															

							srno = srno + 1;
						}
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();

					}
				});	
		
	}
	
	
	function fetchallarea()
	{
		var t = $('#tblDataArea').DataTable();
		t.clear().draw();
		$.ajax({
					type : "GET",
					url : myContextPath+"/fetchAreaList",
					 data: "id=" + id + "&name=" + name, 
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var srno = 1;														

						var count = $(
								"#tblDataArea")
								.find(
										"tr:first th").length;
						
						if (data == null) {
							t.row
									.add('<tr>'
											+ '<td colspan="'+count+'"><center>No Data Found</center></td>'
											+ '</tr>');
						}
						for (var i = 0, len = data.length; i < len; ++i) {
							var area = data[i];
							t.row
									.add(
											[
													srno,
													area.name,
													area.pincode,
													area.region.name,
													area.region.city.name,
													area.region.city.state.name,
													area.region.city.state.country.name,
													"<button onclick='editArea("+area.areaId+")' class='editarea btn btn-flat'><i class='material-icons tooltipped' data-position='right' data-delay='50' data-tooltip='Edit' >edit</i></button>"

											])
									.draw(
											false); 															

							srno = srno + 1;
						}
						$('.preloader-background').hide();
						$('.preloader-wrapper').hide();

					}
				});	
		
	}*/