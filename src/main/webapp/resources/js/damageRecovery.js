$(document).ready(function() {

	
	$('#qtyGiven').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	$('#qtyGiven').keyup(function(){
		
		var qtyDamage=$('#qtyDamage').val();
		var qtyGiven=$('#qtyGiven').val();
		
		if(qtyDamage=='' || qtyDamage==undefined){
			qtyDamage=0;
		}		
		if(qtyGiven=='' || qtyGiven==undefined){
			qtyGiven=0;
		}
		if(parseInt(qtyGiven)>parseInt(qtyDamage))
		{
			Materialize.toast('Given Quantity must be same or less than Damage Quantity', '4000', 'teal lighten-3');
			$('#qtyGiven').val(0);
			$('#qtyGiven').change();
		}		
	});
	
	$('#qtyReceived').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	$('#qtyReceived').keyup(function(){
		
		var qtyReceived=$('#qtyReceived').val();
		var qtyGiven=$('#givenQuantityId').val();
		
		if(qtyDamage=='' || qtyDamage==undefined){
			qtyDamage=0;
		}		
		if(qtyGiven=='' || qtyGiven==undefined){
			qtyGiven=0;
		}
		if(parseInt(qtyReceived)>parseInt(qtyGiven))
		{
			Materialize.toast('Received Quantity must be same or less than Given Quantity is : '+qtyGiven, '4000', 'teal lighten-3');
			$('#qtyReceived').val(0);
			$('#qtyReceived').change();
		}		
	});
	
	/*$('#productId').change(function(){
		var productId=$('#productId').val();
		$('#supplierId').empty();
		$("#supplierId").append('<option value="">Choose Supplier</option>');	
		$('#qtyDamage').val(0);
		$('#qtyDamage').change();
		$.ajax({
			url : myContextPath+"/fetchSupplierListForAddQuantity?productId="+productId,
			dataType : "json",
			success : function(data) {
				supplierProductLists=data;
				
				$('#qtyDamage').val(supplierProductLists[0].product.damageQuantity);
				$('#qtyDamage').change();
				for(var i=0; i<supplierProductLists.length; i++)
				{								
					$("#supplierId").append('<option value='+supplierProductLists[i].supplier.supplierId+'>'+supplierProductLists[i].supplier.name+'</option>');
				}	
				
				$('#supplierId').change();
			},
			error: function(xhr, status, error) {
				  //var err = eval("(" + xhr.responseText + ")");
				Materialize.toast('Supplier not Available for this product <br> First Add Supplier For this product!', '4000', 'teal lighten-3');
				$('#addeditmsg').modal('open');
	   	     	$('#msgHead').text("Supplier Order : ");
	   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); 
			}
		});
		
	});*/
	

	$('#addDamageQty').click(function(){
		
		var qtyGiven=$('#qtyGiven').val();
		if(parseInt(qtyGiven)==0)
		{
			Materialize.toast('Given Quantity must be Non Zero', '4000', 'teal lighten-3');
			return false;
		}
	});
	
});
function showDamageDetails(damageRecoveryId)
{
	$("#damageProductDetailsId").empty();
	$("#damageProductDetailsFooter").empty();
	$.ajax({
		url : myContextPath+"/fetchDamageRecoveryDetailsListByDamageRecoveryId?damageRecoveryId="+damageRecoveryId,
		dataType : "json",
		success : function(data) {
			var damageRecoveryDetailsList=data;
			var srno=1;			
			var quantityGivenTotal=0,quantityReceivedTotal=0,quantityNotClaimedTotal=0;
			
			for(var i=0; i<damageRecoveryDetailsList.length; i++)
			{			
				var damageRecoveryDetails=damageRecoveryDetailsList[i];
				var givenDate=moment(damageRecoveryDetails.givenDate).format("DD-MM-YYYY");//DD MMM YYYY hh:mm a
				var receiveDate;
				var buttonProperty="";
				if(damageRecoveryDetails.receiveStatus==true)
				{
					receiveDate=moment(damageRecoveryDetails.receiveDate).format("DD-MM-YYYY");//DD MMM YYYY hh:mm a
					buttonProperty="disabled";
				}
				else
				{
					receiveDate="NA";
					buttonProperty='onclick="updateReceivedQuantity(\''+damageRecoveryDetails.damageRecoveryDetailsId+'\','+damageRecoveryDetails.quantityGiven+')"';					
				}
				
				$("#damageProductDetailsId").append(
					'<tr>'+
						'<td>'+damageRecoveryDetails.damageRecoveryDetailsId+'</td>'+
						'<td>'+damageRecoveryDetails.supplier.name+'</td>'+
						'<td>'+damageRecoveryDetails.quantityGiven+'</td>'+
						'<td>'+damageRecoveryDetails.quantityReceived+'</td>'+
						'<td>'+givenDate+'</td>'+
						'<td>'+receiveDate+'</td>'+
						'<td>'+damageRecoveryDetails.quantityNotClaimed+'</td>'+
						'<td><button '+buttonProperty+' class="btn-flat tooltipped actionBtn" data-position="right" data-delay="50" data-tooltip="Add Received Qty"><i class="material-icons">add</i></button></td>'+                   							
					'</tr>');
				
				quantityGivenTotal+=damageRecoveryDetails.quantityGiven;
				quantityReceivedTotal+=damageRecoveryDetails.quantityReceived;
				quantityNotClaimedTotal+=damageRecoveryDetails.quantityNotClaimed;
				
				srno++;
			}	
			
			$("#damageProductDetailsFooter").append(
					'<tr>'+
						'<th colspan="2">Total</th>'+
						'<th>'+quantityGivenTotal+'</th>'+
						'<th>'+quantityReceivedTotal+'</th>'+
						'<th></th>'+
						'<th></th>'+
						'<th>'+quantityNotClaimedTotal+'</th>'+
						'<th></th>'+
					'</tr>');
			
			$('#damageDetails').modal('open');
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			Materialize.toast('Something Went Wrong..with : fetchDamageRecoveryByProductId', '4000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Supplier Order : ");
   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); */
		}
	});
}

function updateReceivedQuantity(damageRecoveryDetailsId,dmgGiven){
	
	$('#damageRecoveryDetailsId').val(damageRecoveryDetailsId);
	$('#givenQuantityId').val(dmgGiven);
	
	$("#addDamageForm").hide();
	$("#updateDamageForm").show();
	$("#qtyReceived").focus();
	$("#damageDetails").modal('close');	
}

function giveProductRecovery(damageRecoveryId){

	$.ajax({
		url : myContextPath+"/fetchDamageRecoveryByDamageRecoveryId?damageRecoveryId="+damageRecoveryId,
		dataType : "json",
		success : function(data) {
			damageRecoveryDayWise=data;
			
			$('#productName').val(damageRecoveryDayWise.product.productName);
			$('#productName').change();
			
			$('#damageRecoveryId').val(damageRecoveryDayWise.damageRecoveryId);
			$('#damageRecoveryId').change();
			
			$('#qtyDamage').val(parseInt(damageRecoveryDayWise.quantityDamage)-parseInt(damageRecoveryDayWise.quantityGiven));
			$('#qtyDamage').change();
			
			$('#qtyGiven').val(0);
			$('#qtyGiven').change();
			
			getSupplierListByProductId(damageRecoveryDayWise.product.productId);
			
			$('#damageRecoveryDetailsId').val('');
			
			$("#addDamageForm").show();
			$("#updateDamageForm").hide();
			
			$('#qtyGiven').val('');
			$('#qtyGiven').focus(); 
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			Materialize.toast('Something Went Wrong', '4000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Supplier Order : ");
   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); */
		}
	});	
}

function getSupplierListByProductId(productId)
{
	$('#supplierId').empty();
	$("#supplierId").append('<option value="">Choose Supplier</option>');	
	$('#supplierId').change();
	$.ajax({
		url : myContextPath+"/fetchSupplierListForAddQuantity?productId="+productId,
		dataType : "json",
		success : function(data) {
			supplierProductLists=data;
			
			for(var i=0; i<supplierProductLists.length; i++)
			{								
				$("#supplierId").append('<option value='+supplierProductLists[i].supplier.supplierId+'>'+supplierProductLists[i].supplier.name+'</option>');
			}	
			
			$('#supplierId').change();
		},
		error: function(xhr, status, error) {
			  //var err = eval("(" + xhr.responseText + ")");
			Materialize.toast('Supplier not Available for this product <br> First Add Supplier For this product!', '4000', 'teal lighten-3');
			/*$('#addeditmsg').modal('open');
   	     	$('#msgHead').text("Supplier Order : ");
   	     	$('#msg').html("Supplier not Available for this product <br> First Add Supplier For this product"); */
		}
	});	
}