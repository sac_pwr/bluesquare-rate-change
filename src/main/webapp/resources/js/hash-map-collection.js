/* 
    Why map? :-
    A map contains values on the basis of key i.e. key and value pair. 
    Each key and value pair is known as an entry.
    Map contains only unique keys.
    Map is useful if you have to search, update or delete elements on the basis of key.
*/

/*
    const keyword :-
    Const != immutable
    It’s very important to understand const. 
    It doesn’t imply immutability.
    A variable is like a pointer to a value (it’s a pointer for objects, it’s an assigned value for primitives). 
    const prevents the variable to be assigned to another value. We could say it makes the pointer immutable,
    but it doesn’t make the value immutable too!
 */

//without generic hashmap - which set any type of key and value
const HashMap = function () {
    //internal object which store all entries(key-value pair objects)
    this.array_object = [],
    //its check key and value is undefined or not if get it will throw error 
    //its check key whether its already exist or not
    //its add new entry in map
        this.put = function (key, value) {
            if (key == undefined) {
                throw "Key can't be undefined";
            }else if(value == undefined){
                throw "Value can't be undefined";
            }

            for (var j = 0; j < this.array_object.length; j++) {
                var entry = this.array_object[j];
                if (entry.key === key) {
                    throw new Error(key + " : key is already exist");
                    return;
                }
            }
            var map_obj = {
                key: key,
                value: value,
                toString:function(){
                    return "{"+key+"-"+value+"}";
                }
            }
            this.array_object.push(map_obj);
        },
        //object instanceof with HashMap if not HashMap its throw exception 
        //its check whethe given map have exiting key or not
        //its add new map entries in own object
        this.putAll = function (other_array_object) {
            if(!(other_array_object instanceof HashMap)){
                throw new Error("Invalid Object");
            }
            for (var i = 0; i < other_array_object.array_object.length; i++) {
                var new_entry = other_array_object.array_object[i];

                for (var j = 0; j < this.array_object.length; j++) {
                    var old_entry = this.array_object[j];
                    if (new_entry.key === old_entry.key) {
                        throw new Error(new_entry.key + " : key is already exist");
                        return;
                    }
                }
                
            }
            for (var i = 0; i < other_array_object.array_object.length; i++) {
                var new_entry = other_array_object.array_object[i];
                this.array_object.push(new_entry);
            }
        },
        //its check key is undefined or not if get it will throw error 
        //its remove object from map based on given key
        //if key not found then its throw error
        this.remove = function (key) {
            if (key == undefined) {
                throw "Key can't be undefined";
            }
            for (var j = 0; j < this.array_object.length; j++) {
                var entry = this.array_object[j];
                if (entry.key === key) {
                    this.array_object.splice(j, 1);
                    return;
                }
            }
            throw new Error("key is invalid");
        },
        //its check key is undefined or not if get it will throw error 
        //its return object from map based on given key
        //if key not found then its throw error
        this.get = function (key) {
            if (key == undefined) {
                throw "Key can't be undefined";
            }
            for (var j = 0; j < this.array_object.length; j++) {
                var entry = this.array_object[j];
                if (entry.key === key) {
                    return entry;
                }
            }
            throw new Error("key is invalid");
        },
        //its check key is undefined or not if get it will throw error 
        //its find object have or not based on given key
        //its return boolean value
        this.containsKey = function (key) {
            if (key == undefined) {
                throw "Key can't be undefined";
            }
            for (var j = 0; j < this.array_object.length; j++) {
                var entry = this.array_object[j];
                if (entry.key === key) {
                    return true;
                }
            }
            return false;
        },
        //its return all key's from current map
        this.keySet = function () {
            var returnVal = "";
            for (var j = 0; j < this.array_object.length; j++) {
                var entry = this.array_object[j];
                returnVal = returnVal + entry.key + ",";
            }

            if (returnVal.length > 0) {
                returnVal = returnVal.substring(0, returnVal.length - 1);
            }

            return "[" + returnVal + "]";
        },
        //its return all values's from current map
        this.valueSet=function() {
			var values = [];
			this.forEach(function(key,value) { values.push(value); });
			return values;
		},
        //its return all entries(key-value pair) from current map
        this.entrySet = function () {
            var returnVal = "";
            for (var j = 0; j < this.array_object.length; j++) {
                var entry = this.array_object[j];
                returnVal = returnVal + entry.key + "=" + entry.value + ",";
            }

            if (returnVal.length > 0) {
                returnVal = returnVal.substring(0, returnVal.length - 1);
            }

            return "[" + returnVal + "]";
        },
        //its return all entries(key-value pair) from current map
        this.toString = function () {
            var returnVal = "";
            for (var j = 0; j < this.array_object.length; j++) {
                var entry = this.array_object[j];
                returnVal = returnVal + entry.key + "=" + entry.value + ",";
            }

            if (returnVal.length > 0) {
                returnVal = returnVal.substring(0, returnVal.length - 1);
            }

            return "{" + returnVal + "}";
        },
        //iterate map entries
        this.forEach=function(func) {
			for (var index in this.array_object) {
				var entry = this.array_object[index];
				func(entry.key, entry.value);
			}
        },
        //clear all entries from hashmap
        this.clear=function() {
			this.array_object = [];
        },
        //get count how many entries have in this map
        this.size=function(){
            return this.array_object.length;
        },
        this.getAll=function(){
            return this.array_object;
        }
};

