function calculateProperTax(mrp,igstTaxPercentage){
	mrp=parseFloat(mrp)
	
	//unit price
	var unitPrice=(
					mrp/
						(
							(igstTaxPercentage+100)/100
						)
				  );
	
	//cgst amount
	var cgst=(
				unitPrice.toFixed(2)*
				(
					(igstTaxPercentage/2)/100
				)
			  );
	//sgst amount
	var sgst=(
				unitPrice.toFixed(2)*
				(
					(igstTaxPercentage/2)/100
				)
			  );
	//Igst amount
	var igst=parseFloat(cgst.toFixed(2))+parseFloat(sgst.toFixed(2));
	//Newmrp for comparing entered mrp  and calculated mrp
	var newMrp=parseFloat(unitPrice.toFixed(2))+parseFloat(igst);
	
	
	if(parseFloat(mrp)!==parseFloat(newMrp))
	{
		igst=(
				unitPrice.toFixed(2)*
				(
					igstTaxPercentage/100
				)
			  );
		igst=parseFloat(igst).toFixed(3);
		cgst=parseFloat(parseFloat(igst).toFixed(2))/2;
		sgst=parseFloat(parseFloat(igst).toFixed(2))/2;
		newMrp=parseFloat(unitPrice.toFixed(2))+parseFloat(parseFloat(igst).toFixed(2));
		
		if(parseFloat(mrp)!==parseFloat(newMrp)){
			
			var mrpDiff=mrp-newMrp;
			unitPrice=unitPrice+mrpDiff;
			
			var data={
					mrp:mrp.toFixed(2),
					unitPrice:unitPrice.toFixed(2),
					cgst:cgst.toFixed(3),
					sgst:sgst.toFixed(3),
					igst:parseFloat(igst).toFixed(2)
				};
				return data;
			
		}else{
		
			var data={
					mrp:newMrp.toFixed(2),
					unitPrice:unitPrice.toFixed(2),
					cgst:cgst.toFixed(3),
					sgst:sgst.toFixed(3),
					igst:parseFloat(igst).toFixed(2)
				};
				return data;
		}
	}
	else
	{
		var data={
			mrp:mrp.toFixed(2),
			unitPrice:unitPrice.toFixed(2),
			cgst:cgst.toFixed(2),
			sgst:sgst.toFixed(2),
			igst:parseFloat(igst).toFixed(2)
		};
		return data;
	}
}