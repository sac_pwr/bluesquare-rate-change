<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
<script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/additional-method.js"></script>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
<script type="text/javascript">

var gstinValid=true;;
var userIdValid=true; 
var companyCheck=true;
$(document).ready(function() {
	
	 $(".eye-slash").hide();
	    $(".eye").click(function() {
	        $(".eye").hide();
	        $(".eye-slash").show();
	        $("#password").attr("type", "text");
	    });
	    $(".eye-slash").click(function() {
	        $(".eye-slash").hide();
	        $(".eye").show();
	        $("#password").attr("type", "password");
	    });
	    $(".eye-slash1").hide();
	    $(".eye1").click(function() {
	        $(".eye1").hide();
	        $(".eye-slash1").show();
	        $("#Cnfrmpassword").attr("type", "text");
	    });
	    $(".eye-slash1").click(function() {
	        $(".eye-slash1").hide();
	        $(".eye1").show();
	        $("#Cnfrmpassword").attr("type", "password");
	    });
	    $('.mobileNumber').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
		        event.preventDefault();
		}); 
		
		$.validator.setDefaults({
		       ignore: []
		}); 
		//$('select').change(function(){ $('select').valid(); });
		jQuery.validator.addMethod("companyCheck", function(value, element){
			if(companyCheck==false) {
		        return false;
		    } else {
		        return true;
		    };
		}, "company name already in use"); 
		jQuery.validator.addMethod("userIdCheck", function(value, element){
			if(userIdValid==false) {
		        return false;
		    } else {
		        return true;
		    };
		}, "User Id already in use"); 
		jQuery.validator.addMethod("gstCheck", function(value, element){
			if(gstinValid==false) {
		        return false;
		    } else {
		        return true;
		    };
		}, "gst no. Is already in use"); 
		$('#companyProfile').validate({
			
			rules: {
				password: "required",
			    confirmpass: {
			      equalTo: "#password"
			    },
			    
			    userId:{
		         	userIdCheck:userIdValid
		         },
		         gstIn:{
		        	 gstCheck:gstinValid
		          },
		          companyName:{
		        	  companyCheck:companyCheck
		          }           
			    
			  },
			
		
		   errorElement : "span",
		    errorClass : "invalid error",
		    errorPlacement : function(error, element) {
		      var placement = $(element).data('error');
		    
		      if (placement) {
		        $(placement).append(error)
		      } else {
		        error.insertAfter(element);
		      }
		      $('select').change(function(){ $('select').valid(); });
		    }
		  });
		
		$('#gstin').on('blur',function(){
			$('#gstin').valid();
			var gstin=$('#gstin').val().trim();
			if(gstin=='' || gstin==undefined)
			{
				gstinValid=true;
				return false;
			}
			
			$.ajax({
				url :"${pageContext.request.contextPath}/checkCompanyGSTNumberForUpdate?gstinNo="+gstin+"&companyId=${companyDetails.companyId}",
				async:false,
				success : function(data) {
					if(data==="Success")
					{					
						gstinValid=true;
						$('#gstin').valid();
					}
					else
					{				
						gstinValid=false;
						$('#gstin').valid();
						
					}
				}
			});
			
		});
		$('#userId').on('keypress blur',function(){
			$('#userId').valid();
			var userId=$('#userId').val().trim();
			$.ajax({
				url : "${pageContext.servletContext.contextPath}/checkCompanyUserNameForUpdate?userName="+userName+"&companyId=${companyDetails.companyId}",
				async:false,
				success : function(data) {
					if(data==="Success"){
						userIdValid=true;
						$('#userId').valid();
					}else{
						userIdValid=false;
						$('#userId').valid();
					}
				},
				error: function(xhr, status, error) {
					alert("Error");
				}
			});
			
			
		});
	$('#companyNameId').on('blur',function(){
		$('#companyNameId').valid();
			var companyName=$('#companyNameId').val().trim();
			if(companyName=='' || companyName==undefined)
			{
				companyCheck=false;
				return false;
			}
			
			$.ajax({
				url : "${pageContext.servletContext.contextPath}/checkCompanyNameForUpdate?companyName="+companyName+"&companyId=${companyDetails.companyId}",
				async:false,
				success : function(data) {
					if(data==="Success"){
						companyCheck=true;
						$('#companyNameId').valid();
					}else{
						companyCheck=false;
						$('#companyNameId').valid();
					}
				},
				error: function(xhr, status, error) {
					alert("Error");
				}
			});
			
		});
	
	/* $('#emailIdAndMobileNumberDiv').show();
	$('#otp_pass_Div').hide();
	
	$('#getOtpId').click(function(){
		
		var emailIdAndMobileNumberId=$('#emailIdAndMobileNumberId').val();
		if(emailIdAndMobileNumberId==''){
			Materialize.toast('Enter Email-Id or Mobile Number', '4000', 'teal lighten-2');
			return false;
		}	
		$('#getOtpId').prop('disabled', true);
		
		$.ajax({
			type:'POST',
			url : myContextPath+"/forgetPasswordSendOtp",
			data:{
				emailIdAndMobNo : emailIdAndMobileNumberId
			},
			success : function(data){
				if(data!=="Failed"){
					$('#emailIdAndMobileNumberDiv').hide();
					$('#otp_pass_Div').show();
					Materialize.toast(data, '4000', 'teal lighten-2');	
					
				}else{
					Materialize.toast("Check Your EmailId and Password", '4000', 'teal lighten-2');	
					$('#getOtpId').removeAttr("disabled");
				}
							
			},
			error: function(xhr, status, error) {
				Materialize.toast('Something Went Wrong', '4000', 'teal lighten-2');
			}
		});	
		
	});
	
	$('#otpNumberId').blur(function(){
		
		var otpNumber=$('#otpNumberId').val();
		
		$.ajax({
			type:'POST',
			url : myContextPath+"/checkCompanyOTP",
			data:{
				otp : otpNumber
			},
			success : function(data){
				if(data==="success"){
					otpValidation=true;
				}else{
					otpValidation=false;
					Materialize.toast("Invalid OTP", '4000', 'teal lighten-2');	
				}
							
			},
			error: function(xhr, status, error) {
				Materialize.toast('Something Went Wrong', '4000', 'teal lighten-2');
			}
		});
		
	});
	
	$('#changePasswordId').click(function(){
		
		if(otpValidation==false){
			Materialize.toast("Invalid OTP", '4000', 'teal lighten-2');				
			return false;
		}
		
		var pass=$('#passwordId').val();
		var confPass=$('#confirmPasswordId').val();
		if(pass!==confPass){
			Materialize.toast("Confirm password Must be same as Password", '4000', 'teal lighten-2');				
			return false;
		}
		
	});
	$('#confirmPasswordId').blur(function(){
		var pass=$('#passwordId').val();
		var confPass=$('#confirmPasswordId').val();
		if(pass!==confPass){
			Materialize.toast("Confirm password Must be same as Password", '4000', 'teal lighten-2');				
			return false;
		}
	}); */
});



</script>
<style>
	#upload-error{
		display:block;
	}
</style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
            	
           
            	<form action="${pageContext.servletContext.contextPath}/updateCompanyForReset" method="post" id="companyProfile">
				<input id="companyId" type="hidden" class="validate" name="companyId" value="${companyDetails.companyId}">
                <div class="row  z-depth-3">
                    <br>          
                    <div class="row">
                    <div class="row">
                    	    <div class="input-field col s12 m10 l10 push-l1 push-m1">
							 <i class="material-icons prefix">account_balance</i>
	                        <input id="companyNameId" type="text" class="validate" value="${companyDetails.companyName}"  name="companyName" required>
	                        <label for="companyNameId" class="active"><span class="red-text">*</span>Company Name</label>                       
	                    </div>
                    </div>
                     <div class="row" style="margin-bottom:0">
	                 
	                     <div class="input-field col s12 m5 l5 push-l1 push-m1" id="unitprice">
							 <i class="material-icons prefix">featured_video</i>
	                        <input id="panNo" type="text" class="validate" value="${company.panNumber}"  name="panNumber" required>
	                        <label for="panNo" class="active"><span class="red-text">*</span>Pan Number</label>                       
	                    </div>
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1" id="unitprice">
							 <i class="material-icons prefix">assistant</i>
	                        <input id="gstIn" type="text" class="validate"  name="gstinno" value="${companyDetails.gstinno}" minlength="15" maxlength="15">
	                        <label for="gstIn" class="active"><span class="red-text">*</span>GST No</label>                       
	                    </div>
	                   </div>
	                   <div class="row" style="margin-bottom:0"> 
	                    <div class="input-field col s12 m10 l10 push-l1 push-m1" id="unitprice">
							 <i class="material-icons prefix">location_on</i>
	                        <input id="address" type="text" class="validate" value="${companyDetails.address}" name="address" required>
	                        <label for="address" class="active"><span class="red-text">*</span>Address</label>                       
	                    </div>
	                    </div>
	                    <div class="row" style="margin-bottom:0">
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">phone</i>
	                        <input id="mobileNumber" type="text" class="validate mobileNumber" value="${companyDetails.contact.mobileNumber}" name="mobNo" required minlength="10" maxlength="10">
	                        <label for="mobileNumber" class="active"><span class="red-text">*</span>Mobile Number</label>
	                    </div>
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">phone</i>
	                        <input id="teleNumber" type="text" class="validate mobileNumber" value="${companyDetails.contact.telephoneNumber}" name="telephoneNo"  minlength="10" maxlength="11">
	                        <label for="teleNumber" class="active"><span class="red-text">*</span>Telephone Number</label>
	                    </div>
	                    </div>.
	                    <div class="row" style="margin-bottom:0">
	                   <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">email</i>
	                        <input id="emailId" type="email" class="validate" value="${companyDetails.contact.emailId}" name="emailId" required>
	                        <label for="emailId" class="active"><span class="red-text">*</span>Email Id</label>
	                    </div>
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">person</i>
	                        <input id="userId" type="text" class="validate" name="userId" value="${companyDetails.userId}"  required>
	                        <label for="userId" class="active"><span class="red-text">*</span>User Id</label>
	                    </div>
	                    </div>
	                    
	                     
	                     <div class="row" style="margin-bottom:0">
	                    <div class="input-field col s11 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">lock</i>
	                        <input id="password" type="password" class="validate" value="${companyDetails.password}" name="password" required>
	                        <label for="password" class="active"><span class="red-text">*</span>Password</label>
	                    </div>
	                    <div class="input-field col s1 m1 l1" style="transform:translate(0,10px)">
	                        <i class="fa fa-eye  right eye" aria-hidden="true"></i>
	                        <i class="fa  fa-eye-slash right eye-slash" aria-hidden="true"></i>
                   		 </div>
                   		 
	                     <div class="input-field col s11 m5 l5">
	                        <i class="material-icons prefix">lock</i>
	                        <input id="Cnfrmpassword" type="password" class="validate" value="${companyDetails.password}" name="confirmpass" required>
	                        <label for="Cnfrmpassword" class="active"><span class="red-text">*</span>Confirm Password</label>
	                    </div>
	                     <div class="input-field col s1 m1 l1  pull-l1 pull-m1 pull-s1" style="transform:translate(0,10px)">
                        <i class="fa fa-eye   eye1 right" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash  eye-slash1 right" aria-hidden="true"></i>
                   
                    	</div>
                    	</div>
	                    
	                     <%-- <div class="input-field col s12 m3 l3 offset-l1 offset-m1">
	                         <!-- <i class="material-icons prefix">star <span class="red-text">*</span></i> -->
	                        
	                        <select id="countryid" name="countryId">
	                                 <option value="">Choose Country</option>
	                                <c:if test="${not empty countryList}">
								<c:forEach var="listValue" items="${countryList}">							
									<option value="<c:out value="${listValue.countryId}" />"><c:out
											value="${listValue.name}" /></option>
								</c:forEach>
							</c:if>
	                        </select>
	                    </div>
	                    <div class="input-field col s12 m3 l3">
	                        <!-- <i class="material-icons prefix">filter_list <span class="red-text">*</span></i> -->
	                         
	                        <select id="stateid" name="stateId">
	                                 <option value="">Choose State</option>
	                        </select>
	                    </div>
	                    <div class="input-field col s12 m3 l3">
	                        <select id="cityid" name="cityId">
	                                 <option value="">Choose City</option>
	                        </select>
	                    </div>
	                    <div class="input-field col s12 m10 l10 push-l1 push-m1">
	                        <table class="centered tblborder" id="cityCart">
	                            <thead>
	                                <tr>
	                                    <th>Sr.No</th>
	                                    <th>CityName</th>
	                                    <th>Remove</th>
	                                </tr>
	                            </thead>
	                            <tbody id="cityTable">
	                            </tbody>
	                        </table>
	                        <br><br>
	                          <input id="citylistinput" type="hidden" class="validate" title="Add atleast one product" name="cityIdList" required>
	                    </div> --%>
                  </div>
                    <br>
					  
                    <div class="input-field col s12 m6 l6 offset-l3  offset-m3 center-align">
                        <button class="btn waves-effect waves-light blue-gradient" type="submit" id="updateCompany">Update Company<i class="material-icons right">send</i> </button>
                        <br><br>
                    </div>

                </div>


            </form>
           
            
                <%-- <div class="row  z-depth-3">
                    <br>          
                    <div class="row" id="emailIdAndMobileNumberDiv">
	                     <div class="input-field col s12 m4 l4 right-align" >
							 <i class="fa fa-inr prefix"></i>
	                        <input id="emailIdAndMobileNumberId" type="text" class="validate"  name="companyName" required>
	                        <label for="emailIdAndMobileNumberId" class="active"><span class="red-text">*</span>Email-Id Or Mobile Number</label>                       
	                    </div>
	                    <div class="input-field col s12 m6 l6 offset-l3  offset-m3 center-align">
	                        <button class="btn waves-effect waves-light blue-gradient" type="button" id="getOtpId">Get OTP<i class="material-icons right">send</i> </button>
	                        <br><br>
	                    </div>
	              	</div>
	              	<div class="row" id="otp_pass_Div">
	              		<form action="${pageContext.servletContext.contextPath}/forgetPasswordCompany" method="post">
		                    <div class="input-field col s12 m5 l4 push-l1 push-m1">
		                        <i class="material-icons prefix">shopping_cart</i>
		                        <input id="otpNumberId" type="text" class="validate" name="otpNumber" required>
		                        <label for="otpNumberId" class="active"><span class="red-text">*</span>OTP Number</label>
		                    </div>
		                   <div class="input-field col s12 5 l4 right-align">
		                        <i class="fa fa-inr prefix"></i>
		                        <input id="passwordId" type="text" class="validate" name="password" required>
		                        <label for="passwordId" class="active"><span class="red-text">*</span>Password</label>
		                    </div>
		                    <div class="input-field col s12 5 l4 right-align">
		                        <i class="fa fa-inr prefix"></i>
		                        <input id="confirmPasswordId" type="text" class="validate" name="confpassword" required>
		                        <label for="confirmPasswordId" class="active"><span class="red-text">*</span>Confirm Password</label>
		                    </div>
		                    <div class="input-field col s12 m6 l6 offset-l3  offset-m3 center-align">
		                        <button class="btn waves-effect waves-light blue-gradient" type="submit" id="changePasswordId">Change Password<i class="material-icons right">send</i> </button>
		                        <br><br>
		                    </div>
		                </form>
                  </div>
                    
                    

                </div> --%>
        </div>
       
    	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center success  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>    
    </main>
    <!--content end-->
</body>

</html>