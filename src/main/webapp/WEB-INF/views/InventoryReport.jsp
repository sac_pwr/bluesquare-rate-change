<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
 <script src="resources/js/moment.min.js"></script>
 <script type="text/javascript">
 
 var showEditButton=${sessionScope.loginType!='Admin'};
 
 var msg="${saveMsg}";
 //alert(msg);
 if(msg!='' && msg!=undefined)
 {
	 $('#addeditmsg').find("#modalType").addClass("success");
	$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
     $('#addeditmsg').modal('open');
     //$('#msgHead').text("Brand Message");
     $('#msg').text(msg);
 }
 
    function showTransactionDetails(id_and_paystatus)
	{
    	var idAndPayStatus=id_and_paystatus.split("_");
		   $.ajax({
				url : "${pageContext.servletContext.contextPath}/fetchTrasactionDetailsByInventoryId?inventoryId="+idAndPayStatus[0],
				dataType : "json",
				success : function(data) {
				//alert(data);
				var totalAll=0,totalAmtWithTaxAll=0;
				    $("#productOrderedDATA").empty();
					var srno=1;
					for (var i = 0, len = data.length; i < len; ++i) {
						var inventoryDetailList = data[i];
						
						var totalAmtWithTax=inventoryDetailList.rate.toFixed(2)*inventoryDetailList.quantity;
						
						$("#productOrderedDATA").append("<tr>"+
	                           "<td>"+srno+"</td>"+
	                           "<td>"+inventoryDetailList.product.categories.hsnCode+"</td>"+
	                           "<td>"+inventoryDetailList.product.productName+"</td>"+
	                           "<td>"+inventoryDetailList.product.categories.categoryName+"</td>"+	                           
	                           "<td>"+inventoryDetailList.product.brand.name+"</td>"+
	                           "<td>"+inventoryDetailList.quantity+"</td>"+
	                           "<td>"+inventoryDetailList.rate.toFixed(2)+"</td>"+
	                           "<td>"+totalAmtWithTax.toFixed(2)+"</td>"+
	                       "</tr>"); 
						srno++;
						totalAmtWithTaxAll=parseFloat(totalAmtWithTaxAll)+totalAmtWithTax;
					}
					
					$("#productOrderedDATA").append("<tr>"+	     
													   "<td colspan='7'><b>All Total</b></td>"+
							                           "<td  class='red-text'><b>"+totalAmtWithTaxAll.toFixed(2)+"</b></td>"+
							                       "</tr>");
					
					var dateAdded=data[0].inventory.inventoryAddedDatetime;
					$('#supplierNameId').html(data[0].inventory.supplier.name);
					$('#mobNoId').html(data[0].inventory.supplier.contact.mobileNumber);

					var today = new Date();
					
					var dateBeforeThreeDay = new Date();
					dateBeforeThreeDay.setDate(today.getDate() - 3);
					

					/* var isGreaterorEq = date1.getTime() >= date2.getTime();
					var lessThan = date2.getTime() < date1.getTime();
					console.log(isGreaterorEq ,lessThan );  */
					
					/* 
					showEditButton is true when company and gatekeeper login and false for admin login
					*/
					
					if(dateAdded<=today.getTime() 
						&& dateAdded>=dateBeforeThreeDay.getTime() 
						&& idAndPayStatus[1].trim()==="UnPaid"
						&& showEditButton==true)
					{
						$('#editButton').html('<a href="${pageContext.servletContext.contextPath}/openEditMultipleInventory?inventoryId='+data[0].inventory.inventoryTransactionId+'" class="modal-action modal-close waves-effect btn">Edit</a>');
						$('#deleteButton').html('<a onclick="deleteConfirmationForInventory(\''+data[0].inventory.inventoryTransactionId+'\')" class="waves-effect btn">Delete</a>');
					}
					else
					{
						$('#editButton').html('');
						$('#deleteButton').html('');
					}		
					
					$('.modal').modal();
					$('#product').modal('open');	
					
				},
				error: function(xhr, status, error) {
					  	alert("error");
					}
			});
	   }
    function deleteConfirmationForInventory(id){
    	/* $('.modal').modal();
    	$('#delete'+id).modal('open'); */
    	Materialize.Toast.removeAll();
    	var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><a class="btn white-text toast-action" href="${pageContext.servletContext.contextPath}/deleteInventory?inventoryId='+id+'">Delete</a>'));
    	Materialize.toast($toastContent, "abc");
    }
    
    $(document).ready(function(){
    	var table = $('#tblData').DataTable();
  		 table.destroy();
  		 $('#tblData').DataTable({
  	         "oLanguage": {
  	             "sLengthMenu": "Show _MENU_",
  	             "sSearch": "_INPUT_" //search
  	         },
  	      	autoWidth: false,
  	         columnDefs: [
  	                      { 'width': '1%', 'targets': 0 },
  	                      { 'width': '3%', 'targets': 1},
  	                      { 'width': '3%', 'targets': 2},
  	                      { 'width': '10%', 'targets': 3},
  	                      { 'width': '3%', 'targets': 4},
  	                  	  { 'width': '5%', 'targets': 5},
  	                  	  { 'width': '3%', 'targets': 6},
  	                  	  { 'width': '5%', 'targets': 7},
  	                	  { 'width': '5%', 'targets': 8},
  	            	      { 'width': '5%', 'targets': 9},
  	            		  { 'width': '3%', 'targets': 10},
  	            		  { 'width': '5%', 'targets': 11},
  	            		  { 'width': '5%', 'targets': 12},
  	            		  { 'width': '5%', 'targets': 13},
	  	            	  { 'width': '5%', 'targets': 14},
	  	            	  { 'width': '5%', 'targets': 15},
  	                      { 'width': '1%', 'targets': 16}
  	                     
  	                      ],
  	         lengthMenu: [
  	             [10, 25., 50, -1],
  	             ['10 ', '25 ', '50 ', 'All']
  	         ],
  	         
  	        
  	         //dom: 'lBfrtip',
  	         dom:'<lBfr<"scrollDivTable"t>ip>',
  	         buttons: {
  	             buttons: [
  	                 //      {
  	                 //      extend: 'pageLength',
  	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
  	                 //  }, 
  	                 {
  	                     extend: 'pdf',
  	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                     customize: function(doc, config) {
  	                    	doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [30,80,'*',40,40,40,40,40,50,50,50,50,50,50,50] 
	                    		 } 
	                    		    })
  	                         var tableNode;
  	                         for (i = 0; i < doc.content.length; ++i) {
  	                           if(doc.content[i].table !== undefined){
  	                             tableNode = doc.content[i];
  	                             break;
  	                           }
  	                         }
  	        
  	                         var rowIndex = 0;
  	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
  	                          
  	                         if(tableColumnCount > 6){
  	                           doc.pageOrientation = 'landscape';
  	                         }
  	                         /*for customize the pdf content*/ 
  	                         doc.pageMargins = [5,20,10,5];   	                         
  	                         doc.defaultStyle.fontSize = 8	;
  	                         doc.styles.title.fontSize = 12;
  	                         doc.styles.tableHeader.fontSize = 11;
  	                         doc.styles.tableFooter.fontSize = 11;
  	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
  	                       },
  	                 },
  	                 {
  	                     extend: 'excel',
  	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'print',
  	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'colvis',
  	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
  	                     collectionLayout: 'fixed two-column',
  	                     align: 'left'
  	                 },
  	             ]
  	         }

  	     });
  		 $("select")
            .change(function() {
                var t = this;
                var content = $(this).siblings('ul').detach();
                setTimeout(function() {
                    $(t).parent().append(content);
                    $("select").material_select();
                }, 200);
            });
        $('select').material_select();
        $('.dataTables_filter input').attr("placeholder", "Search");
        
        /*   $("#BankDetails").css("display", "none");
        $(".cash").change(function() {
            $("#BankDetails").css("display", "none");
        });
        $(".cheque").change(function() {
            $("#BankDetails").css("display", "block");
        }); */
        $(".showQuantity").hide();
        $(".showDates").hide();
        $("#oneDateDiv").hide();
        $(".topProduct").click(function() {
            $(".showQuantity").show();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
        });
       
        $(".rangeSelect").click(function() {
        	$("#oneDateDiv").hide();	
            $(".showDates").show();
            $(".showQuantity").hide();
        });
       
    	$(".pickdate").click(function(){
    		 $(".showQuantity").hide();
    		 $(".showDates").hide();
    		$("#oneDateDiv").show();
    	});
        
    	 //hide column depend on login
        var table = $('#tblData').DataTable(); // note the capital D to get the API instance
        var column = table.columns('.toggle');
        console.log(column);
        if(${sessionScope.loginType!='Admin'}){            
            column.visible(true);
        }else{
        	 column.visible(false);
        }
   });
    
    function fetchPaymentDetails(invtId){
		$.ajax({
			type:'GET',
			url: "${pageContext.servletContext.contextPath}/fetchPaymentSupplier?inventoryId="+invtId,
			async: false,
			success : function(data)
			{
				$('#paymentDetail').empty();
								
				var totalAmountPaid=0;
				var srno=1;
				for(var i=0; i<data.length; i++)	
				{
					var mode="";
					if(data[i].payType==="Cash")
					{
						mode="Cash";
					}else{
						mode=data[i].bankName+"-"+data[i].chequeNumber;
					}
					
					var dateFrom=new Date(parseInt(data[i].paidDate));
					month = dateFrom.getMonth() + 1;
					day = dateFrom.getDate();
					year = dateFrom.getFullYear();
					var dateFromString = day + "-" + month + "-" + year;
					
					var editDeleteButton="<a href='${pageContext.servletContext.contextPath}/editPaymentSupplier?paymentPayId="+data[i].paymentPayId+"' class='btn-flat' ><i class='material-icons'>edit</i></a>"+
					"<a href='#' class='btn-flat' onclick='deleteConfirmToast(\""+data[0].inventory.inventoryTransactionId+"\","+data[i].paymentPayId+")'><i class='material-icons'>delete</i></a>";
					//deletePaymentSupplier
					$('#paymentDetail').append('<tr>'+
												'<td>'+srno+'</td>'+
												'<td>'+data[i].paidAmount.toFixed(2)+'</td>'+
												'<td>'+mode+'</td>'+
												'<td>'+dateFromString+'</td>'+
												'<td>'+editDeleteButton+'</td></tr>');
					
					totalAmountPaid=parseFloat(totalAmountPaid)+parseFloat(data[i].paidAmount.toFixed(2));
									
					srno++;
				}
				
				$('#inventoryId').html("<b>Inventory Id : </b>"+data[0].inventory.inventoryTransactionId);
				$('#supplierDetailsId').html("<b>Supplier : </b>"+data[0].inventory.supplier.name);
				$('#totalAmt').html("<b>Total Amount : </b>"+parseFloat(data[0].inventory.totalAmountTax).toFixed(2));
				
				var totalAmountBal=(parseFloat(data[0].inventory.totalAmountTax)-parseFloat(totalAmountPaid)).toFixed(2);
				$('#balAmt').html("<b>Balance Amount : </b>"+parseFloat(totalAmountBal).toFixed(2));
				
				$('#totalAmountPaid').text(parseFloat(totalAmountPaid).toFixed(2));
				$('#viewPaymentDetail').modal('open');
			},
			error: function(xhr, status, error) 
			{
				Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
			}
			
		});
    }
    function deleteConfirmToast(invtId,paymentPayId){
    	Materialize.Toast.removeAll();
    	var $toastContent = $('<span>Do you want to Delete?</span>').add($('<button class="btn red white-text toast-action" onclick="Materialize.Toast.removeAll();">Cancel</button><button class="btn white-text toast-action" onclick="deletePaymentSupplier(\''+invtId+'\','+paymentPayId+')">Delete</button>  '));
    	  Materialize.toast($toastContent, "abc");
    }
    function deletePaymentSupplier(invtId,paymentPayId){
    	
    	$.ajax({
			type:'GET',
			url: "${pageContext.servletContext.contextPath}/deletePaymentSupplier?inventoryId="+invtId+"&paymentPayId="+paymentPayId,
			async: false,
			success : function(data)
			{				
				Materialize.Toast.removeAll();
				if(data==""){
					
					$('#viewPaymentDetail').modal('close');
					refreshInventoryReport();
					return false;
				}
				$('#paymentDetail').empty();
								
				var totalAmountPaid=0;
				var srno=1;
				for(var i=0; i<data.length; i++)	
				{
					var mode="";
					if(data[i].payType==="Cash")
					{
						mode="Cash";
					}else{
						mode=data[i].bankName+"-"+data[i].chequeNumber;
					}
					
					var dateFrom=new Date(parseInt(data[i].paidDate));
					month = dateFrom.getMonth() + 1;
					day = dateFrom.getDate();
					year = dateFrom.getFullYear();
					var dateFromString = day + "-" + month + "-" + year;
					
					var editDeleteButton="<a href='${pageContext.servletContext.contextPath}/editPaymentSupplier?paymentPayId="+data[i].paymentPayId+"' class='btn-flat' ><i class='material-icons'>edit</i></a>"+
					"<a href='#' class='btn-flat' onclick='deletePaymentSupplier(\""+data[0].inventory.inventoryTransactionId+"\")'><i class='material-icons'>delete</i></a>";
					
					$('#paymentDetail').append('<tr>'+
												'<td>'+srno+'</td>'+
												'<td>'+data[i].paidAmount.toFixed(2)+'</td>'+
												'<td>'+mode+'</td>'+
												'<td>'+dateFromString+'</td>'+
												'<td>'+editDeleteButton+'</td></tr>');
					
					totalAmountPaid=parseFloat(totalAmountPaid)+parseFloat(data[i].paidAmount.toFixed(2));
									
					srno++;
				}
				
				$('#inventoryId').html("<b>Inventory Id : </b>"+data[0].inventory.inventoryTransactionId);
				$('#supplierDetailsId').html("<b>Supplier : </b>"+data[0].inventory.supplier.name);
				$('#totalAmt').html("<b>Total Amount : </b>"+data[0].inventory.totalAmountTax);
				
				var totalAmountBal=parseFloat(data[0].inventory.totalAmountTax)-parseFloat(totalAmountPaid);
				$('#balAmt').html("<b>Balance Amount : </b>"+totalAmountBal);
				
				$('#totalAmountPaid').text(totalAmountPaid);
				//$('#viewPaymentDetail').modal('open');
				
				refreshInventoryReport();
			},
			error: function(xhr, status, error) 
			{
				Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
			}
			
		});
    	
    }
    
    function refreshInventoryReport(){
    	var tblData = $('#tblData').DataTable();
    		tblData.clear().draw(); 
	    
   		var range="${range}";
   		if(range=="" || range==undefined){
   			range="-";
   		}
   		
   		var startDate="${startDate}";
   		if(startDate=="" || startDate==undefined){
   			startDate="-";
   		}
   		
   		var endDate="${endDate}";
   		if(endDate=="" || endDate==undefined){
   			endDate="-";
   		}
   		var supplierId="${supplierId}";
   		var data={
				range : range,
				startDate : startDate, 
				endDate : endDate,
				supplierId : supplierId
			 };
   		
   		
   		if(supplierId=="" || supplierId==undefined){
   			data={
   					range : range,
   					startDate : startDate, 
   					endDate : endDate
   				 };
   		}
   		
   		
    		
    	$.ajax({
			type:'GET',
			url: "${pageContext.servletContext.contextPath}/fetchInventoryReportViewAjax",
			data:data,
			beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
			async: false,
			success : function(data)
			{
				for(var i=0; i<data.length; i++){
					listValue=data[i]; 
					
					var supplierAnchorVisibility;
                	if("${sessionScope.loginType!='GateKeeper'}"){
                		
                		supplierAnchorVisibility='<a class="tooltipped" data-position="right" data-delay="50" data-tooltip="View Supplier Details"'+ 
                		'href="${pageContext.servletContext.contextPath}/fetchSupplierListBySupplierId?supplierId='+listValue.supplier.supplierId+'">'+
                		listValue.supplier.name+'</a>';
                		
                	}else{                		
                		supplierAnchorVisibility=listValue.supplier.name;                		
                	}
                	
                	var amountPaid;
               		if(listValue.payStatus!='UnPaid'){
               			amountPaid ='<button class="btn-flat" onclick="fetchPaymentDetails('+listValue.transactionId+')">'+
			                    	'<b>'+
			                    	listValue.amountPaid.toFixed(2)+
			                    	'</b>'+
			                    	'</button>';
               		}else{
               			amountPaid = listValue.amountPaid.toFixed(2);
               		}
    				
               		var addedDate=moment(listValue.addedDate).format("DD-MM-YYYY");
					var addedTime=moment(listValue.addedDate).format("HH:mm:ss");
					var paymentDate=moment(listValue.paymentDate).format("DD-MM-YYYY");
					
					var billDate=moment(listValue.billDate).format("DD-MM-YYYY");
					var billTime=moment(listValue.billDate).format("HH:mm:ss");
					
					var paidStatus;					
					if(listValue.payStatus=='Paid'){
						paidStatus="<b><font color='green'>"+listValue.payStatus+"</font></b>";
					}else if(listValue.payStatus=='UnPaid'){
						paidStatus="<b><font color='Red'>"+listValue.payStatus+"</font></b>";
					}else if(listValue.payStatus=='Partially Paid'){
						paidStatus="<b><font color='Blue'>"+listValue.payStatus+"</font></b>";
					}					
					
					tblData.row.add([	
								listValue.srno,
								listValue.transactionId,
								listValue.billNumber,
								
								'<button class="btn blue-gradient"'+ 
								'onclick="showTransactionDetails(\''+listValue.transactionId+'_'+listValue.payStatus+'\')">'+
								'View</button>',
								
								supplierAnchorVisibility,								
								listValue.totalQuantity,
                				listValue.totalAmount,
                				(parseFloat(listValue.totalAmountTax)-parseFloat(listValue.totalAmount)).toFixed(2),
                				listValue.totalAmountTax.toFixed(2),                				
                				amountPaid,                				
                				listValue.amountUnPaid.toFixed(2),                				
                				addedDate+"<br>"+addedTime,
                				billDate+"<br>"+billTime,
                				paymentDate,                				
                				listValue.byUserName,
                				paidStatus,
                				
                				'<a class="btn-flat tooltipped" data-position="right" data-delay="50" data-tooltip="Make Payment"'+ 
                				'href="${pageContext.servletContext.contextPath}/paymentSupplier?inventoryTransactionId='+listValue.transactionId+'">'+
                				'Pay</a>'
								
					          ]).draw(false);					
				}
				
				$('.preloader-background').hide();
				$('.preloader-wrapper').hide();
			},
			error: function(xhr, status, error) 
			{
				$('.preloader-background').hide();
				$('.preloader-wrapper').hide();
				Materialize.toast('Something Went Wrong', '2000', 'teal lighten-2');
			}			
		});
    	
    }
    </script>
    <style>
        tr [colspan="2"] {
            text-align: center;
        }
        
        tr,
        td,
        th {
            text-align: center;
        }
    </style>


</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
         <div class="col s12 m4 l4 right right-align" style="margin-top:1%;">
                <!-- <div class="col s6 m4 l4 right"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=today&suppplierId=${supplierId}">Today</a></li>
                    	<li><a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=yesterday&suppplierId=${supplierId}">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=last7days&suppplierId=${supplierId}">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=currentMonth&suppplierId=${supplierId}">Current Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=lastMonth&suppplierId=${supplierId}">Last Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=last3Months&suppplierId=${supplierId}">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/fetchInventoryReportView?range=viewAll&supplierId=${supplierId}">View All</a></li>
                    </ul>
                </div>
                <div class="col s12 l3 m3 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchInventoryReportView" method="post">
	                         <input type="hidden" name="range" value="pickDate">
                   			 <input type="hidden" name="supplierId" value="${supplierId}">
	                    <div class="input-field col s12 m7 l7">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                       
                        </div>
                    </form>
               </div>
               <div class="col s12 l4 m4 right"> 
                 <form action="${pageContext.request.contextPath}/fetchInventoryReportView" method="post">
                    <input type="hidden" name="range" value="range">
                    <input type="hidden" name="supplierId" value="${supplierId}">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                                <div class="input-field col s6 m5 l5 right">
	                                <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required> 
	                                <label for="endDate">To</label>
                                </div>
                              <div class="input-field col s6 m5 l5 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
                                     <label for="startDate">From</label>
                               </div>                             
                          </span>
                </form>
                 </div>
				
               
               
                
           
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No</th>
                            <th class="print-col">Transaction Id</th>
                            <th class="print-col">Bill Number</th>
                            <th>Product</th>
                            <th class="print-col">Supplier Name</th>
                            <th class="print-col">Total Qty</th>
                            <th class="print-col">Taxable Amount</th>
                            <th class="print-col">Tax</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Amount Paid</th>
                            <th class="print-col">Balance Amount</th>
                            <th class="print-col">Added Date</th>
                            <th class="print-col">Bill Date</th>
                            <th class="print-col">Payment Date</th>
                            <th class="print-col ">By User</th>
                            <th class="print-col toggle">Payment Status</th>
                            <th class="toggle">Pay</th>
                        </tr>
                    </thead>

                    <tbody >
                    <c:if test="${not empty inventoryReportViews}">
					<c:forEach var="listValue" items="${inventoryReportViews}">
                        <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                            <td><c:out value="${listValue.transactionId}" /></td>
                            <td><c:out value="${listValue.billNumber}" /></td>
                            <td><button class="btn blue-gradient" onclick="showTransactionDetails('${listValue.transactionId}_${listValue.payStatus}')">View</button></td>
                            <td class="wrapok">
                            <c:choose>
                            	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
                            		<a class="tooltipped" data-position="right" data-delay="50" data-tooltip="View Supplier Details" href="${pageContext.servletContext.contextPath}/fetchSupplierListBySupplierId?supplierId=${listValue.supplier.supplierId}"><c:out value="${listValue.supplier.name}" /></a>
                            	</c:when>
                            	<c:otherwise>
 	                            	<c:out value="${listValue.supplier.name}" />
                            	</c:otherwise>
                            </c:choose>
                            </td>
                            <td><c:out value="${listValue.totalQuantity}" /></td>
                            <td><c:out value="${listValue.totalAmount}" /></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountTax-listValue.totalAmount}" /></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountTax}" /></td>
                            <td> 
                                <c:choose>
                            	<c:when test="${sessionScope.loginType!='Admin'}">                       	
	                            	<c:choose>
	                    			<c:when test="${listValue.payStatus!='UnPaid'}">
	                    				<button class="btn-flat" onclick="fetchPaymentDetails('${listValue.transactionId}')">
		                            	<b>
		                            	<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" />
		                            	</b>
		                            	</button>
	                    			</c:when>
	                    			<c:otherwise>
	                    				<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" />
	                    			</c:otherwise>
	                    			</c:choose>
	                    		</c:when>
	                    		<c:otherwise>
	                    			<fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountPaid}" />
	                    		</c:otherwise>
	                    		</c:choose>
                            </td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountUnPaid}" /></td>
                             <td class="wrapok">
                             <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.addedDate}"  /><c:out value="${date}" />
                             <br>
                             <fmt:formatDate pattern = "HH:mm:ss" var="time"   value = "${listValue.addedDate}"  /><c:out value="${time}" />
                            </td>
                            <td>
                            	<fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.billDate}"  /><c:out value="${date}" />
	                             <br>
	                             <fmt:formatDate pattern = "HH:mm:ss" var="time"   value = "${listValue.billDate}"  /><c:out value="${time}" />
	                            </td>
                            <td class="wrapok"><fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.paymentDate}"  /><c:out value="${date}" />
                            </td>
                            <td><c:out value="${listValue.byUserName}" /></td>
                            <td>
                            	<font color="${listValue.payStatus=='Paid'?'Green':''}
                            				   ${listValue.payStatus=='UnPaid'?'Red':''}
                            				   ${listValue.payStatus=='Partially Paid'?'Blue':''}">
	                            	<b>
	                            		<c:out value="${listValue.payStatus}" />
	                            	</b>
                            	</font>
                            </td>
                            <td><a class="btn-flat tooltipped" data-position="right" data-delay="50" data-tooltip="Make Payment" href="${pageContext.servletContext.contextPath}/paymentSupplier?inventoryTransactionId=${listValue.transactionId}">Pay</a></td>
                        </tr>
					</c:forEach>
					</c:if>
                    </tbody>
                </table>
            </div>
        </div>
        <br>
		<!-- Modal Structure for View Product Details -->
        <div id="product" class="modal modal-fixed-footer" style="width:70%;">
            <div class="modal-content">
                <h5 class="center"><u>Product Details</u></h5>              
             
               
                 <h6 class="left">Supplier Name : <span id="supplierNameId"></span></h6>
                <h6 class="right">Mobile No. : <span id="mobNoId"></span></h6>
               <br><br>
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>HSN Code</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>Rate</th>
                            <th>Total Amount With Tax</th>
                        </tr>
                    </thead>
                    <tbody id="productOrderedDATA">
                        <!-- <tr>
                            <td>1</td>
                            <td>Mouse</td>
                            <td>I.T</td>
                            <td>BlueSquare</td>
                            <td>5</td>
                            <td>350</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">
                <div class="col s12 m3 l2 offset-l3" id="editButton">
                    <!-- <a href="#!" class="modal-action modal-close waves-effect btn">Edit</a> -->
                </div>
                <div class="col s12 m3 l2">
                    <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
                </div>
                 <div class="col s12 m3 l2" id="deleteButton">
                    <!-- <a href="#!" class="modal-action modal-close waves-effect btn">Edit</a> -->
                </div>
				
            </div>
        </div>
        <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Payment list view model -->
		<div id="viewPaymentDetail" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h5 class="center"><u>Payment Details</u></h5>
                <hr>

                <div class="row">
                    <div class="col s12 l6 m6">
                        <h6 id="inventoryId" class="black-text">Inventory No:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="totalAmt" class="black-text">Total Amt :</h6>
                    </div>
                
                
                    <div class="col s12 l6 m6">
                        <h6 id="balAmt" class="black-text">Balance Amount:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="supplierDetailsId" class="black-text">Supplier : </h6>
                    </div> 
                </div>
               
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount</th>
                            <th>Mode</th>
                            <th> Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="paymentDetail">
                       
                    </tbody>
                    <tfoot>
                    <tr>
                    <th>Total</th>
                     <th><span id="totalAmountPaid"></span></th>
                     <th></th>
                     <th></th>
                     <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer row">
             <div class="col s6 m6 l3 offset-l4">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                 </div>	
            </div>
        </div>
		<!-- payment list view model end -->
		
    </main>
    <!--content end-->
</body>

</html>