<%@page import="java.io.File"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
     <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/additional-method.js"></script>
<script type="text/javascript">
//function for calculating unitprice and cgst,igst amount

var categoryIgst;
var categorySgst;
var categoryCgst;

function gstAmountCalc(){
	var rate=parseFloat($("#productRate").val());
	if(rate>0)
	{
		/*var unitprice;
		var divisor;
		var multiplier;
		var igstAmt;
		var cgstAmt;
		var sgstAmt;
	
		//for calculating divisor means
		divisor=(categoryIgst/100)+1;
		//console.log(divisor);
		unitprice=(rate/divisor);
		//console.log(unitprice);
		cgstAmt=(unitprice*categoryCgst)/100;
		//console.log(igstAmt);
		//for calculating cgst percent from igst percent
		//var cgstPer=(categoryIgst/2);					
		igstAmt=parseFloat(cgstAmt.toFixed(2))+parseFloat(cgstAmt.toFixed(2));
		//console.log(cgstAmt);*/
		
		var correctAmoutWithTaxObj=calculateProperTax(rate,categoryIgst);
		
		$("#unitRate").val(correctAmoutWithTaxObj.unitPrice);
		$("#unitRate").focus();
		$("#unitRate").blur();
		$("#cgst").val(correctAmoutWithTaxObj.cgst);
		$("#cgst").focus();
		$("#cgst").blur();
		$("#sgst").val(correctAmoutWithTaxObj.sgst);
		$("#sgst").focus();
		$("#sgst").blur();
		$("#igst").val(correctAmoutWithTaxObj.igst);
		$("#igst").focus();	
		$("#igst").blur();			
	
	}
};

$(document).ready(function() {
	$.validator.setDefaults({
	       ignore: []
	}); 
	$.validator.addMethod('filesize', function (value, element, param) {
	    return this.optional(element) || (element.files[0].size <= param)
	}, 'File size must not exceed 10kb');
	//$('select').change(function(){ $('select').valid(); });
	$('#updateProductForm').validate({
		
	messages:{
		file:{
			extension:'Upload only png,jpg or jpeg format'
		}
	},	
	
	    errorElement : "span",
	    errorClass : "invalid error",
	    errorPlacement : function(error, element) {
	      var placement = $(element).data('error');
	    
	      if (placement) {
	        $(placement).append(error)
	      } else {
	        error.insertAfter(element);
	      }
	      $('select').change(function(){ $('select').valid(); });
	    }
	  });
	$(".upload").rules("add", { 
		extension: "jpg,jpeg,png",
		  filesize: 10000,
		});
	 /* $('#productRate').keydown(function(e){            	
			-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
		 });
		document.getElementById('productRate').onkeypress=function(e)
		{		if (e.keyCode === 46 && this.value.split('.').length === 2) {
		  		 return false;
				 }
		} */
		
		$('#productRate').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13) )
		        event.preventDefault();
		});
			  
			  $('#threshold').keypress(function( event ){
				    var key = event.which;
				    
				    if( ! ( key >= 48 && key <= 57 ) )
				        event.preventDefault();
				});
			  /* for taking igst value of category selected */
				
				$("#categoryid").change(function(){
					var categoryid=$("#categoryid").val();
					if(categoryid=='' || categoryid==undefined)
					{
						return false;
					}
						$.ajax({
							type : "GET",
							url :"${pageContext.request.contextPath}/fetchCategories?categoriesId="+categoryid,
							async:false,
							beforeSend: function() {
								$('.preloader-background').show();
								$('.preloader-wrapper').show();
					           },
					           success : function(data) {
					        	    categoryIgst = data.igst;
					        	    categorySgst = data.sgst;
					        	    categoryCgst = data.cgst;
					        	   // gstAmountCalc();
					        	   //console.log(CategoryIgst);
					        	   $('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									$("#productRate").keyup();
					           },
					           error: function(xhr, status, error) {
									$('.preloader-wrapper').hide();
									$('.preloader-background').hide();
									Materialize.toast('Something Went Wrong!', '2000', 'red lighten-2');
									/* $('#addeditmsg').modal('open');
				           	     	$('#head').text("Warning: ");
				           	     	$('#msg').text("Something Went Wrong"); 
				           	     		setTimeout(function() 
										  {
				  	     					$('#addeditmsg').modal('close');
										  }, 1000); */
									}						
					           
						});			
				});
				/* for calculating unit price of product */
				$("#productRate").keyup(function(){
					var categoryid=$("#categoryid").val();
					if(categoryid=='' || categoryid==undefined)
					{
						return false;
					}
					gstAmountCalc();
					$("#productRate").focus();
				});
			
			var msg="${saveMsg}";
			 //alert(msg);
			 if(msg!='' && msg!=undefined)
			 {
				 $('#addeditmsg').find("#modalType").addClass("success");
				 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
			     $('#addeditmsg').modal('open');
			    /*  $('#msgHead').text("Product Update Message"); */
			     $('#msg').text(msg);
			 }
			 
			/*  $("#updateProductSubmit").click(function() {

				 var brandid = $("#brandid").val();
				//alert(brandid);
				if (brandid === "0") {
					$('#addeditmsg').modal('open');
				     $('#msgHead').text("Product Update Message");
				     $('#msg').text("Select Brand");
					return false;
				}
			 
				var categoryid = $("#categoryid").val();
				//alert(categoryid);
				if (categoryid === "0") {
					$('#addeditmsg').modal('open');
				     $('#msgHead').text("Product Update Message");
				     $('#msg').text("Select Category");
					return false;
				}
				
				
			}); */
			 $("#addImage").click(function() {
				    $("#upload").click();
			});
			 $('#upload').change(function (evt) {

				   
				   var filename = $("#upload").val();

			        // Use a regular expression to trim everything before final dot
			        var extension = filename.replace(/^.*\./, '');

			        // Iff there is no dot anywhere in filename, we would have extension == filename,
			        // so we account for this possibility now
			        if (extension == filename) {
			            extension = '';
			        } else {
			            // if there is an extension, we convert to lower case
			            // (N.B. this conversion will not effect the value of the extension
			            // on the file upload.)
			            extension = extension.toLowerCase();
			        }

			        switch (extension) {
			            case 'jpg':
			            case 'jpeg':
			            case 'png':
			            	{
			                //alert("it's got an extension which suggests it's a PNG or JPG image (but N.B. that's only its name, so let's be sure that we, say, check the mime-type server-side!)");
			            		var tgt = evt.target || window.event.srcElement,
								        files = tgt.files;
			            		
			            		var fileSize = files[0].size;
			            		var kb=fileSize/1024;
			            		if(kb>10)
			            		{
			            			Materialize.toast('Please upload less than 10kb sizes image!', '2000', 'red lighten-2');
			            			$('#upload').val('');
			            			/* $('#addeditmsg').modal('open');
								    $('#head').text("Image Upload Error : ");
			            			$('#msg').text("Please upload less than 10kb sizes image"); */
			            		  	
			            			return false;
			            		}
			            		
									$('#imgContentType').val(files[0].type);
								    // FileReader support
								    if (FileReader && files && files.length) {
								        var fr = new FileReader();
								        fr.onload = function () {
								            document.getElementById('addImage').src = fr.result;
								        }
								        fr.readAsDataURL(files[0]);
								    }
								
								    // Not supported
								    else {
								        // fallback -- perhaps submit the input to an iframe and temporarily store
								        // them on the server until the user's session ends.
								    }
								    return false;
			            	}
			            // uncomment the next line to allow the form to submitted in this case:
//			          break;
								
			            default:
			            	Materialize.toast('Only png and jpeg image is allowed!', '2000', 'red lighten-2');
			                $('#upload').val('');
			            	/* $('#addeditmsg').modal('open');
						     $('#head').text("Image Upload Error : ");
						     $('#msg').text("Only png and jpeg image allowed"); */
			                // Cancel the form submission
			                return false;
			        }
				   
			    
			}); 
	

			 $("#categoryid").change();
			 
			 var uprice="${product.rate}";
			 var divisor=(categoryIgst/100)+1;
			 var mrp=(uprice*divisor);
			 $("#productRate").val(mrp.toFixed(0));			 
			
			 gstAmountCalc();
		});



</script>
<style>
	/* .paddingBody .input-field .material-icons{
		line-height: 2.3 !important;
	} */
	/* .paddingBody button i{
		    line-height: inherit !important;
	} */
	/* .paddingBody .fa{
			line-height: 2.3 !important;
			white-space: nowrap;
   			 word-wrap: normal;
	} */ 
	#upload-error{
		display:block;
	}
	.select-wrapper{
		height:3.2rem !important;
	}
	 
</style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">

        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/updateProduct" method="post" id="updateProductForm"  enctype="multipart/form-data">
					
                <div class="row  z-depth-3 ">
                    <br>
                    <div class="row"  style="margin-bottom:10px">
                    
                    	
                    	 <c:choose>
                    	 <c:when test="${isProductImg==false}">
                    	 <div class="input-field col s12 m6 l4 center-align" style="margin-bottom:10px;margin-top:20px;height:180px">
						        <img id="addImage" style="border:1px solid;height:120px;width:120px;" src="resources/img/defaultphoto.png">
                         	    <input type="file" id="upload" class="upload" style="display: none;" name="file"/><br/>
                         	    <input type="hidden" id="imgContentType" style="display: none;" name="imageType"/>
                         	    </div>
				         </c:when>
				         <c:otherwise>
				          <div class="input-field col s12 m6 l4 center-align" style="margin-bottom:10px;margin-top:20px;height:180px">
						        <img id="addImage" style="border:1px solid;height:120px;width:120px;" src="${pageContext.request.contextPath}/downloadProductImage/${product.productId}">
                          		<input type="file" id="upload" class="upload" style="display: none;" name="file"/><br/>
                          		<input type="hidden" id="imgContentType" style="display: none;" name="imageType"/>
                          		</div>
						</c:otherwise>
						</c:choose>
                    	
                       <%--  <img id="addImage" style="border:1px solid;height:200px;width:200px; margin-left:10%;" src="${pageContext.request.contextPath}/downloadProductImage/${product.productId}">
                          <input type="file" id="upload" style="display: none;" name="file"/>   --%>
                    <%-- <div class="input-field col s12 m5 l5 push-l1 push-m1">
               			 <i class="material-icons prefix">location_on<span class="red-text">*</span></i>
               			<select name="areaId" id="areaId" class="validate" required="" aria-required="true" title="Please select Area">
                                 <option value="" selected>Select Area</option>
                                <c:if test="${not empty areaList}">
							<c:forEach var="listValue" items="${areaList}">
								<option value="<c:out value="${listValue.areaId}" />" ${product.area.areaId==listValue.areaId?'selected':''}><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
							</c:if>
                        </select>
              		 </div> --%>

                   <div class="input-field col s12 m5 l5 push-l1 pull-m1">
                        <i class="material-icons prefix">star<span class="red-text">*</span></i>  
                         <!--   <label for="brandid" class="active"></label> -->                        
                        <select id="brandid" name="brandId" required="" aria-required="true">
                                 <option value="">Choose Brand</option>
                                <c:if test="${not empty brandlist}">
							<c:forEach var="listValue" items="${brandlist}">							
								<option value="<c:out value="${listValue.brandId}" />" <c:if test="${listValue.brandId == product.brand.brandId}">selected</c:if>><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 pull-m1">                   
                        <i class="material-icons prefix">filter_list<span class="red-text">*</span></i>
                       <!--   <label for="categoryid" class="active"><span class="red-text">*</span></label> -->
                        <select id="categoryid" name="categoryId" required="" aria-required="true">
                                 <option value="">Choose Category</option>
                                <c:if test="${not empty categorylist}">
							<c:forEach var="listValue" items="${categorylist}">
								
								<option value="<c:out value="${listValue.categoryId}" />"  <c:if test="${listValue.categoryId == product.categories.categoryId}">selected</c:if>><c:out
										value="${listValue.categoryName}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    </div>
                    <div class="row">
                    <input id="productId" type="hidden" class="validate" name="productId" value="<c:out value="${product.productId}" />">
                    <div class="input-field col s12 m4 l4 right-align" id="productrate">
                        <i class="fa fa-inr prefix"></i>
                        <input id="unitRate" type="text" class="grey lighten-2" value="" name="productRate" readonly>
                        <label for="unitRate" class="active">Product Unit Price</label>     
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 pull-m1 ">
                        <i class="material-icons prefix">shopping_cart</i>
                        
                        <input id="product" type="text" class="validate" name="productname" value="<c:out value="${product.productName}" />" required>
                        <label for="product" class="active"><span class="red-text">*</span>Enter Product Name</label>
                    </div>
                   <!--  <div class="input-field file-field  col s12 m4 l4 left-align ">

                        <div class="file-path-wrapper">
                            <i class="material-icons prefix" style="margin-left:-3%;">image</i>
                            <input id="upload" type="file" name="file">
                            <input class="file-path validate" type="text" placeholder="Upload one or more files">
                        </div>

                    </div> -->
                     <div class="input-field   col s12 m4 l4 right-align">
						<i class="fa fa-inr prefix"></i>
                      <input id="cgst" type="text" class="grey lighten-2" readonly>
                        <label for="cgst" class="active">CGST Amount</label>

                    </div>
                    
                    
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                         <i class="fa fa-inr prefix"></i>
                        <input id="productRate" type="text" class="validate" name="mrp" value=""  required>
                        <label for="productRate" class="active"><span class="red-text">*</span>Enter MRP</label>
                    </div>
                     <div class="input-field file-field  col s12 m4 l4 right-align">
                     <i class="fa fa-inr prefix"></i>
                         <input id="sgst" type="text" class="grey lighten-2" readonly>
                        <label for="sgst" class="active">SGST Amount</label>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix"> rate_review</i>
                        <input id="code" type="text" class="validate" name="productcode" value="<c:out value="${product.productCode}" />">
                        <label for="code" class="active">Enter Product Code</label>
                    </div>  
                      <div class="input-field col s12 m4 l4 right-align" id="igstDiv">
                    <i class="fa fa-inr prefix"></i>
                        <input id="igst" type="text" class="grey lighten-2"  readonly>
                        <label for="igst" class="active">IGST Amount</label>
                    </div>  
                               
                   
                     <div class="input-field col s12 m5 l5   offset-l1  offset-m1">
                        <i class="material-icons prefix">view_comfy</i>
                        <input id="threshold" type="text" class="validate" name="thresholvalue" value="<c:out value="${product.threshold}" />" required>
                        <label for="threshold" class="active"><span class="red-text">*</span>Enter Threshold Value</label>
                    </div>
                  <%--   <div class="input-field col s12 m5 l5 push-l5 push-m5  " id="productrate">
                        <input id="productTax" type="text" name="productTax" value="<c:out value="${product.tax}" />"  readonly>
                        <label for="productTax" class="active">Product Tax</label>
                    </div>
                    <br>
                    <div class="input-field col s12 m5 l5 push-l5 push-m5  " id="productrate">
                        <input id="productRateWithTax" type="text" name="productRateWithTax"  value="<c:out value="${product.rateWithTax}" />" readonly>
                        <label for="productRateWithTax" class="active">Product Rate With Tax</label>
                    </div> --%>  
                    
                   
                    <div class="input-field col s12 m4 l4 right-align">

                        <i class="material-icons prefix">date_range</i>
                        <fmt:formatDate pattern="yyyy-MM-dd" var="dt" value="${product.productAddedDatetime}" />                        
                        <input id="threshold" type="text" name="brand" value="<c:out value="${dt}" />" readonly>
                        <label for="threshold" class="active"><span class="red-text">*</span>Product Added Date</label>

                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 pull-m1">
                        <i class="material-icons prefix">event</i>
                         <fmt:formatDate pattern="yyyy-MM-dd" var="dt2" value="${product.productQuantityUpdatedDatetime}" />
                        
                        <input id="threshold" type="text" name="brand" value="<c:out value="${dt2}" />" readonly>
                        <label for="threshold" class="active"><span class="red-text">*</span>Last Updated Date</label>

                    </div>
                    <div class="col s12 m4 l4 offset-l4  offset-m4 center-align" style="margin-left:32%">
                        <button class="btn waves-effect waves-light" id="updateProductSubmit" type="submit">Update<i class="material-icons right">send</i> </button>
                        <br><br>
                    </div>
					</div>
                </div>


            </form>
        </div>
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
       	<!-- <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
    </main>
    <!--content end-->
</body>

</html>