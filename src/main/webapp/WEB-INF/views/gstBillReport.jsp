<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<%@include file="components/header_imports.jsp" %>
<script>
	  $(document).ready(function () {

		   $("#btnprint").click(function(){				
			window.print();
		});
		   $("#btnExcel").click(function(e) {
	      	    e.preventDefault();
	      	    //getting data from our table
	      	    var data_type = 'data:application/vnd.ms-excel';
	      	    var table_div = document.getElementsByClassName('tblExport');
	      	    //var table_div = document.getElementById('tblSales');
	      	    var table_html = table_div[0].outerHTML;
	      	    var tablefinal='<h4><b>Distribution System</b></h4><h4>${sessionScope.companyDetails.address}</h4>'+table_html+table_div[1].outerHTML;
	      	    var a = document.createElement('a');
	      	    a.href = data_type + ', ' + escape(tablefinal);
	      	    a.download = 'GST Report'+'.xls';
	      	    a.click();
	      	  });
		    
		   var type="${type}";
		   var startDate="${startDate}";
		   var endDate="${endDate}";
		 		   
		   var $fromInput = $('.fromPicker').pickadate();
		   var $toInput = $('.toPicker').pickadate(); 

		   // Use the picker object directly.
		   var Frompicker = $fromInput.pickadate('picker');
		   var topicker = $toInput.pickadate('picker');
		   
		   Frompicker.set('select', startDate, { format: 'yyyy-mm-dd' });
		   topicker.set('select', endDate, { format: 'yyyy-mm-dd' }); 
		   		   
		    var source=$('#selectType');
			source.val(type);
			source.change(); 
			/*
			$('#fromDateId').val(startDate);
			$('#toDateId').val(endDate); */
	  });
		
</script>
<style>
@media print {
  body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>
</head>
<body>
 <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end--> 
    <main class="paddingBody">
    <div class="row">
    	<div class="col s12 m12 l12">
    		<br>
    		<div class="col s12 m12 l12">
    		<form method="post" action="${pageContext.request.contextPath}/fetchGstBillReport">
				<div class="col s12 l3 m3">
					<select id="selectType" name="type" required>
						<option value="" >Choose Option</option>
						<option value="split">Split</option>
						<option value="merge">Merge</option>						
					</select>
				</div>

				<div class="col s12 l2 m2" style="width:16%">
					<input type="text" class="datepicker fromPicker" placeholder="From Date" id="fromDateId" name="startDate" required>

				</div>
				<div class="col s12 l2 m2" style="width:16%">
					<input type="text" class="datepicker toPicker"  placeholder="To Date" id="toDateId" name="endDate" required>

				</div>
				<div class="col s12 l1 m1" style="margin-top:1%">
					<button type="submit" id="show" class="btn waves-effect waves-light">Show</button>
				</div>
			</form>
           <div class="col s4 l2 m2 right right-align" style="margin-top:1%">
    	     		<button class="btn waves-effect waves-light blue-gradient"  id="btnprint">Print<i class="material-icons left">local_printshop</i> </button>
  				 </div>
    		<div class="col s4 l2 m2 right right-align" style="margin-top:1%">
    				 <button class="btn waves-effect waves-light blue-gradient center"   id="btnExcel">Excel<i class="material-icons left">insert_drive_file</i> </button>
   			 </div>
    		</div>
    		<div id="section-to-print">
    		<div class="col s12 m12 l12">
    		<br>
    		<h5 class="center-align"><b>Distribution System</b></h5>
    		<p class="center-align"><c:out value="${sessionScope.companyDetails.address}"/>	</p>
    		</div>
    		 <table id="tblSales" border="1" class="tblborder centered striped highlight tblExport">
    		 	<thead>
    		 		<tr>
    		 			<c:if test="${type=='split'}"><th colspan="10"><h5>Sales</h5></th></c:if>
    		 			<c:if test="${type!='split'}"><th colspan="9"><h5>Sales</h5></th></c:if>
    		 		</tr>
    		 		<tr>
    		 			<th>Date</th>
    		 			<th>Invoice No</th>
    		 			<th>Name</th>
    		 			<th>GST No</th>
    		 			<c:if test="${type=='split'}"><th>Product Name</th></c:if>
    		 			<th>Taxable Amt</th>
    		 			<th>CGST</th>
    		 			<th>SGST</th>
    		 			<th>IGST</th>
    		 			<th>Total Amt</th>
    		 		</tr>
    		 	</thead>
    		 	<tbody>
    		 	<c:choose>
    		 	<c:when test="${type=='split'}">
    		 		<c:if test="${not empty gstBillReport.saleTableList}">
						<c:forEach var="listValue" items="${gstBillReport.saleTableList}">
		    		 		<tr>
		    		 			<td><c:out value="${listValue.date}" /></td>
		    		 			<c:choose>
		    		 				<c:when test="${listValue.orderProductDetails!=null}">
		    		 					<td><c:out value="${listValue.orderProductDetails.orderDetails.invoiceNumber}" /></td>
				    		 			<td><c:out value="${listValue.orderProductDetails.orderDetails.businessName.shopName}" /></td>
				    		 			<td><c:out value="${listValue.orderProductDetails.orderDetails.businessName.gstinNumber}" /></td>
				    		 			<td><c:out value="${listValue.orderProductDetails.product.productName}" />
				    		 					<font color="green">
				    		 						<c:out value="${listValue.orderProductDetails.type=='Free'?'-Free':''}" />
				    		 					</font>
				    		 			</td>
		    		 				</c:when>
		    		 				<c:otherwise>
		    		 					<c:choose> 
		    		 						<c:when test="${listValue.counterOrderProductDetails.counterOrder.businessName!=null}">
		    		 							<td><c:out value="${listValue.counterOrderProductDetails.counterOrder.invoiceNumber}" /></td>
						    		 			<td><c:out value="${listValue.counterOrderProductDetails.counterOrder.businessName.shopName}" /></td>
						    		 			<td><c:out value="${listValue.counterOrderProductDetails.counterOrder.businessName.gstinNumber}" /></td>
						    		 			<td><c:out value="${listValue.counterOrderProductDetails.product.productName}" />
						    		 					<font color="green">
						    		 						<c:out value="${listValue.counterOrderProductDetails.type=='Free'?'-Free':''}" />
						    		 					</font>
						    		 			</td>
		    		 						</c:when>
		    		 						<c:otherwise>
		    		 							<td><c:out value="${listValue.counterOrderProductDetails.counterOrder.invoiceNumber}" /></td>
						    		 			<td><c:out value="${listValue.counterOrderProductDetails.counterOrder.customerName}" /></td>
						    		 			<td><c:out value="${listValue.counterOrderProductDetails.counterOrder.customerGstNumber}" /></td>
						    		 			<td><c:out value="${listValue.counterOrderProductDetails.product.productName}" />
						    		 					<font color="green">
						    		 						<c:out value="${listValue.counterOrderProductDetails.type=='Free'?'-Free':''}" />
						    		 					</font>
						    		 			</td>
		    		 						</c:otherwise>
		    		 					</c:choose>
		    		 				</c:otherwise>
		    		 			</c:choose>
		    		 			
		    		 			<td><c:out value="${listValue.taxableAmount}" /></td>
		    		 			<td><c:out value="${listValue.cgstAmt}" /></td>
		    		 			<td><c:out value="${listValue.sgstAmt}" /></td>
		    		 			<td><c:out value="${listValue.igstAmt}" /></td>
		    		 			<td><c:out value="${listValue.totalAmount}" /></td>
		    		 		</tr>
	    		 		</c:forEach>
    		 		</c:if>
    		 	</c:when>
    		 	<c:otherwise>
    		 		<c:if test="${not empty gstBillReport.saleMergeTableList}">
						<c:forEach var="listValue" items="${gstBillReport.saleMergeTableList}">
		    		 		<tr>
		    		 			<td><c:out value="${listValue.date}" /></td>
		    		 			<td><c:out value="${listValue.invoiceNo}" /></td>
		    		 			<td><c:out value="${listValue.name}" /></td>
		    		 			<td><c:out value="${listValue.gstNo}" /></td>
		    		 			<td><c:out value="${listValue.taxableAmount}" /></td>
		    		 			<td><c:out value="${listValue.cgstAmt}" /></td>
		    		 			<td><c:out value="${listValue.sgstAmt}" /></td>
		    		 			<td><c:out value="${listValue.igstAmt}" /></td>
		    		 			<td><c:out value="${listValue.totalAmount}" /></td>
		    		 		</tr>
	    		 		</c:forEach>
    		 		</c:if>
    		 	</c:otherwise>
    		 	</c:choose>
    		 	</tbody>
    		 	<tfoot>
    		 		<tr>
    		 			<th></th>
    		 			<th>Total</th>
    		 			<c:if test="${type=='split'}"><th></th></c:if>
    		 			<th></th>
    		 			<th></th>
    		 			<th><c:out value="${gstBillReport.saleTotalTaxableAmt}" /></th>
    		 			<th><c:out value="${gstBillReport.saleCgstAmt}" /></th>
    		 			<th><c:out value="${gstBillReport.saleSgstAmt}" /></th>
    		 			<th><c:out value="${gstBillReport.saleIgstAmt}" /></th>
    		 			<th><c:out value="${gstBillReport.saleFinalAmt}" /></th>    		 			
    		 		</tr>
    		 	</tfoot>
    		 </table>
    		 <br>    		 
    		 <table id="tblPurchase" border="1" class="tblborder centered striped highlight tblExport">
    		 	<thead>
    		 		<tr>
    		 			<c:if test="${type=='split'}"><th colspan="10"><h5>Purchase</h5></th></c:if>
    		 			<c:if test="${type!='split'}"><th colspan="9"><h5>Purchase</h5></th></c:if>
    		 		</tr>
    		 		<tr>
    		 			<th>Date</th>
    		 			<th>Invoice No</th>
    		 			<th>Name</th>
    		 			<th>GST No</th>
    		 			<c:if test="${type=='split'}"><th>Product Name</th></c:if>
    		 			<th>Taxable Amt</th>
    		 			<th>CGST</th>
    		 			<th>SGST</th>
    		 			<th>IGST</th>
    		 			<th>Total Amt</th>
    		 		</tr>
    		 	</thead>
    		 	<tbody>
    		 	<c:choose>
    		 	<c:when test="${type=='split'}">
    		 		<c:if test="${not empty gstBillReport.purchaseTableList}">
						<c:forEach var="listValue" items="${gstBillReport.purchaseTableList}">
		    		 		<tr>
		    		 			<td><c:out value="${listValue.date}" /></td>
		    		 			<td><c:out value="${listValue.inventoryDetails.inventory.inventoryTransactionId}" /></td>
		    		 			<td><c:out value="${listValue.inventoryDetails.inventory.supplier.name}" /></td>
		    		 			<td><c:out value="${listValue.inventoryDetails.inventory.supplier.gstinNo}" /></td>
		    		 			<td><c:out value="${listValue.inventoryDetails.product.productName}" /></td>
		    		 			<td><c:out value="${listValue.taxableAmount}" /></td>
		    		 			<td><c:out value="${listValue.cgstAmt}" /></td>
		    		 			<td><c:out value="${listValue.sgstAmt}" /></td>
		    		 			<td><c:out value="${listValue.igstAmt}" /></td>
		    		 			<td><c:out value="${listValue.totalAmount}" /></td>
		    		 		</tr>
	    		 		</c:forEach>
    		 		</c:if>
    		 	</c:when>
    		 	<c:otherwise>
    		 		<c:if test="${not empty gstBillReport.purchaseMergeTableList}">
						<c:forEach var="listValue" items="${gstBillReport.purchaseMergeTableList}">
		    		 		<tr>
		    		 			<td><c:out value="${listValue.date}" /></td>
		    		 			<td><c:out value="${listValue.invoiceNo}" /></td>
		    		 			<td><c:out value="${listValue.name}" /></td>
		    		 			<td><c:out value="${listValue.gstNo}" /></td>
		    		 			<td><c:out value="${listValue.taxableAmount}" /></td>
		    		 			<td><c:out value="${listValue.cgstAmt}" /></td>
		    		 			<td><c:out value="${listValue.sgstAmt}" /></td>
		    		 			<td><c:out value="${listValue.igstAmt}" /></td>
		    		 			<td><c:out value="${listValue.totalAmount}" /></td>
		    		 		</tr>
	    		 		</c:forEach>
    		 		</c:if>
    		 	</c:otherwise>
    		 	</c:choose>
    		 	</tbody>
    		 	<tfoot>
    		 		<tr>
    		 			<th></th>
    		 			<th>Total</th>
    		 			<th></th>
    		 			<c:if test="${type=='split'}"><th></th></c:if>
    		 			<th></th>
    		 			<th><c:out value="${gstBillReport.purchaseTotalTaxableAmt}" /></th>
    		 			<th><c:out value="${gstBillReport.purchaseCgstAmt}" /></th>
    		 			<th><c:out value="${gstBillReport.purchaseSgstAmt}" /></th>
    		 			<th><c:out value="${gstBillReport.purchaseIgstAmt}" /></th>
    		 			<th><c:out value="${gstBillReport.purchaseFinalAmt}" /></th>    		 	    		 			
    		 		</tr>
    		 	</tfoot>
    		 </table>
    		 </div>
    	</div>
    </div>
   </main>
   <!-- <script src=resources/js/jsExcel.js type="text/javascript"></script> -->
</body>
</html>