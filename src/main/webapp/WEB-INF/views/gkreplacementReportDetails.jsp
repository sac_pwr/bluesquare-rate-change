<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
     <style>
     	.card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
    tfoot th,
    tfoot td    {
    	text-align:center;
    }
    .leftHeader{
    	width:105px !important;
    	display:inline-block;
    }
     </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
    <!-- <script>alert("${orderProductIssueListForIssueReportResponse.deliveryPersonName}");</script> -->
        <br>
        <div class="row">
        
        	<div class="col s12 l10 m10 card-panel hoverable blue-grey lighten-4">
      	
       			
                <div class="col s12 l6 m6 ">
                <p>
                 <span class="leftHeader">Order Id:</span>
                 <b><c:out value="${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.returnOrderProduct.orderDetails.orderId}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                </div>
                 
                <div class="col s12 l6 m6 ">
                   <p id="department"><span class="leftHeader">Shop Name: </span><b><c:out value="${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.shopName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <p><span class="leftHeader">Mobile No: </span><b><c:out value="${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.contact.mobileNumber}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                      <p><span class="leftHeader">Delivery Boy:</span><b><c:out value="${replacementReportOrderProductDetailsResponse.deliveryPersonName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6">
                       <p id="Add"><span class="leftHeader">Date of Reissue:</span> 
                       <b>
                       <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.reIssueDate}"  />
				       	<c:out value="${date}" />
                       </b>
                       </p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6">
                       <p id="Add"><span class="leftHeader">Date of Return:</span> 
                       <b>
                       <fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.returnOrderProduct.returnOrderProductDatetime}"  />
				       	<c:out value="${date}" />
                       </b>
                       </p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                
                
               
                
           
               <%--  <div class="col s12 l6 m6 ">
                       <p id="Add" class=" blue-text">Date of Order: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderDetailsAddedDatetime}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div> --%>
                
           
           </div> 
        
        
         <%--    <div class="col s12 l6 m6 card-panel hoverable">
                		<div class="col s12 m6 l6">
                         <p id="name" class="center-align blue-text">Order ID: <b><c:out value="${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.returnOrderProduct.orderDetails.orderId}" /></b> </p>
                        </div>
                        <div class="col s12 m6 l6">
                         <p id="department" class="center-align blue-text">Shop Name: <b><c:out value="${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.shopName}" /></b></p>
                        </div>
                        <div class="col s12 m6 l6">
                        <p id="MobileNo" class="center-align blue-text">Mobile No: <b><c:out value="${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.contact.mobileNumber}" /></b></p>
                        </div>
                       	 <div class="col s12 m6 l6">
                        <p id="MobileNo" class="center-align blue-text">Delivery Boy Name: <b><c:out value="${replacementReportOrderProductDetailsResponse.deliveryPersonName}" /></b></p>
                        </div>
                          <div class="col s12 m6 l6">
                        <p id="Add" class="center-align blue-text">Date of Reissue: <b>
                        <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.reIssueDate}"  />
				       	<c:out value="${date}" />
                        </b></p>
                        </div>
                        
						 <div class="col s12 m6 l6">
                         <p id="Add" class="center-align blue-text">Date of Return: <b>
                         <fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${replacementReportOrderProductDetailsResponse.reIssueOrderDetails.returnOrderProduct.returnOrderProductDatetime}"  />
				       	<c:out value="${date}" />
                        </b></p>
                        </div>
                                     
            </div> --%>
       
           
       
            <div class="col s12 l12 m12 ">
                <table class="striped highlight centered mdl-data-table display" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        	<th class="print-col">Sr. No.</th>
                            <th class="print-col">Product Name</th>
                            <th class="print-col">Return Quantity</th>
                            <th class="print-col">ReIssue Quantity</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<% int rowincrement=0; %>
                    	<c:if test="${not empty replacementReportOrderProductDetailsResponse.reIssueOrderProductDetailsList}">
						<c:forEach var="listValue" items="${replacementReportOrderProductDetailsResponse.reIssueOrderProductDetailsList}">
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><c:out value="${listValue.product.productName}" /></td>
                            <td><c:out value="${listValue.returnQuantity}" /></td>
                            <td><c:out value="${listValue.reIssueQuantity}" /></td>
                        </tr>
                        </c:forEach>
                        </c:if>
                    </tbody>
                </table>
            </div>
        </div>
    </main>
    <!--content end-->
</body>

</html>