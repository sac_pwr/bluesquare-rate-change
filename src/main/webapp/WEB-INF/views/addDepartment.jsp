<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
       <%@include file="components/header_imports.jsp" %>

<script type="text/javascript">

 $(document).ready(function(){
	 
	 var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
		 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	    /*  $('#msgHead').text("Department Message"); */
	     $('#msg').text(msg);
	 }
	 
	$("#resetDepartmentSubmit").click(function(){
		
		$('#departmentname').val('');
		$('#departmentshortname').val('');	
		$('#departmentId').val('0');
		$("#saveDepartmentSubmit").html('<i class="left material-icons">add</i>Add');
	});
});
 
 function editDepartment(id) {
		
		$.ajax({
					type : "GET",
					url : "${pageContext.request.contextPath}/fetchDepartment?departmentId="+id,
					/* data: "id=" + id + "&name=" + name, */
					beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
					success : function(data) {
						var department = data;
						//alert(brand.name +" "+ brand.brandId);
						$('#departmentname').val(department.name);	
						$('#departmentId').val(department.departmentId);
						$('#departmentname').focus();
						//departmentshortname
						$('#departmentshortname').val(department.shortNameForEmployeeId);
						$('#departmentshortname').focus();
						$("#saveDepartmentSubmit").html('<i class="left material-icons">send</i>Update');
						
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
					},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						  Materialize.toast('Something Went Wrong!', '2000', 'teal lighten-3');
						/* $('#addeditmsg').modal('open');
	           	     	$('#msgHead').text("Message : ");
	           	     	$('#msg').text("Something Went Wrong"); 
	           	     		setTimeout(function() 
							  {
	  	     					$('#addeditmsg').modal('close');
							  }, 1000); */
						}							
				
				});
	}

 </script>

</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">

        <br>
        <form action="${pageContext.servletContext.contextPath}/saveDepartment" method="post" id="saveDepartmentForm">
            <div class="row">
                <div class=" input-field col s12 l3 m4 left">
                	<input id="departmentId" type="hidden" class="validate" value="0" name="departmentId">
                    <input id="departmentname" type="text" class="validate" name="departmentName" required minlength="1" maxlength="20"  pattern=".{1,20}" >
                    
                    <label for="departmentname" class="active"><span class="red-text">*</span>Enter Department Name</label>
                </div>
                <div class=" input-field col s12 l3 m4 left">
                    <input id="departmentshortname" type="text" class="validate" name="departmentshortName" required minlength="2" maxlength="2"  pattern=".{2,2}"  title="2 character required only">
                    <label for="departmentname" class="active"><span class="red-text">*</span>Enter Department Short Name</label>
                </div>
                <div class="input-field col  s12 l3 m4 left " style="padding-top:1%;">
                    <button class="btn waves-effect waves-light blue-gradient" id="saveDepartmentSubmit" type="submit" name="action"><i class="left material-icons">add</i>Add</button>
                </div>
                  <div class="col s10 l3 m6  offset-s2">
                    <button id="resetDepartmentSubmit" class="btn waves-effect waves-light blue-gradient" type="button" name="action" style="margin-top:10%"><i class="left material-icons">refresh</i>Reset</button>
                </div>
            </div>
        </form>

       
            <div class="row">
                <div class="col s12 l12 m12 ">
                    <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th class="print-col">Sr.No</th>
                                <th class="print-col">Department</th>
                                <th class="print-col">Short Name For Employee Id</th>
                                <th class="print-col">Date</th>
                                <th>Edit</th>
                                <!-- <th>Delete</th> -->
                            </tr>
                        </thead>

                        <tbody>
                            <% int rowincrement=0; %>
                     <c:if test="${not empty departmentList}">
							<c:forEach var="listValue" items="${departmentList}">
						
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                            <tr>
                                 <td><c:out value="${rowincrement}" /></td>
                                <td><c:out value="${listValue.name}" /></td>
                                <td><c:out value="${listValue.shortNameForEmployeeId}" /></td>
                                <td>
	                                <fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.departmentAddedDatetime}"  />
			                        <c:out value="${date}" />
                                </td>
                                <td><button class=" btn-flat "  onclick='editDepartment(${listValue.departmentId})'><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></button></td>
                                <!-- Modal Trigger -->
                                <!-- <td><a href="#delete"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                            </tr>
                           </c:forEach>
						</c:if>


                        </tbody>
                    </table>
                </div>

                <!-- Modal Structure -->
                
                        <div id="delete" class="modal deleteModal row">
                             <div class="modal-content  col s12 m12 l12">
                                <h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
                                <h5 class="center-align">Do you want to delete?</h5>
                                <br>
                            </div>
                            
                            <div class="modal-footer">
                            	 <div class="col s6 m6 l3 offset-l3">
              					   <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
              				  </div>
           					 <div class="col s6 m6 l3">
                                <a href="#!" class="modal-action modal-close waves-effect btn">Delete</a>
                               
                            </div>
                        </div>
                    </div>
                

           

        <!--Add Edit Confirm Modal Structure -->
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
       <!--  <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div>
		 -->


    </main>
    <!--content end-->
</body>

</html>
