<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
<script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>

<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}"
$(document).ready(function() {
	$('#gstNo').keypress(function( event ) {  
		if(!/[0-9a-zA-Z-]/.test(String.fromCharCode(event.which)))
			return false;
	});
	$.validator.setDefaults({
	       ignore: []
	}); 
	//$('select').change(function(){ $('select').valid(); });
	jQuery.validator.addMethod("gstCheck", function(value, element){
   		if(value=='' || value==undefined)
   		{
   			return true;
   		}		
   	    return checkBusinessDuplication(value,"gstNo");
   	}, "gst no. Is already in use"); 
	jQuery.validator.addMethod("telephoneNumberCheck", function(value, element){
		if(value=='' || value==undefined)
   		{
   			return true;
   		}		
   	    return checkBusinessDuplication(value,"telephoneNumber");
   	}, "Telephone Number Is already in use"); 
	jQuery.validator.addMethod("emailIdCheck", function(value, element){
		if(value=='' || value==undefined)
   		{
   			return true;
   		}		
   	    return checkBusinessDuplication(value,"emailId");
   	}, "Email Id Is already in use"); 
   	jQuery.validator.addMethod("mobileNumberCheck", function(value, element){
   		if(value=='' || value==undefined)
   		{
   			return true;
   		}		
   	    return checkBusinessDuplication(value,"mobileNumber");
   	}, "Mobile Number Is already in use");
	
	
	$('#updateBusinessForm').validate({
	    
		rules: {    		    
			telephoneNumber:{
				telephoneNumberCheck:true
            },
            emailId:{
            	emailIdCheck:true
            },
            mobileNumber:{
            	mobileNumberCheck:true,
            	required:true
            },
            gstinNumber:{
            	gstCheck:true
            }
		  },
		
		errorElement : "span",
	    errorClass : "invalid error",
	    errorPlacement : function(error, element) {
	      var placement = $(element).data('error');
	    
	      if (placement) {
	        $(placement).append(error)
	      } else {
	        error.insertAfter(element);
	      }
	      $('select').change(function(){ $('select').valid(); });
	    }
	  });
	setTimeout(
			  function() 
			  {
			    //do something special
				  var source1 = $("#cityList");
					var v="${businessName.area.region.city.cityId}";
					source1.val(v);
					source1.change();
					//alert(v);
					//alert($("#stateListForCity").val());
			  }, 2000);
	setTimeout(
			  function() 
			  {
			    //do something special
				 // alert("city");
				  var source2 = $("#regionList");
					var v2="${businessName.area.region.regionId}";
					source2.val(v2);
					source2.change();
					//alert(v2);
					//alert($("#stateListForCity").val());
			  }, 3000);
	setTimeout(
			  function() 
			  {
			    //do something special
				 // alert("city");
				  var source3 = $("#areaList");
					var v3="${businessName.area.areaId}";
					source3.val(v3);
					source3.change();
					//alert(v2);
					//alert($("#stateListForCity").val());
			  }, 4000);
	
	
	$('#creditLimit').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	});
	 $('#mobileNo').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	}); 
	 $('#telephoneNo').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
		        event.preventDefault();
		}); 
	
	
	
	
	var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
			$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	    /*  $('#msgHead').text("Business Message"); */
	     $('#msg').text(msg);
	 }
	
	
	
	
	$('#cityList').change(function() {
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('regionList');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options

		select.options.add(new Option("Choose Region", ""));
		$.ajax({
			url : myContextPath+"/fetchRegionListByCityId?cityId="+$('#cityList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {

				/* alert(data); */
				var options, index, option;
				select = document.getElementById('regionList');

				for (var i = 0, len = data.length; i < len; ++i) {
					var region = data[i];
					//alert(state.name+" "+ state.stateId);
					select.options.add(new Option(region.name, region.regionId));
				}
				
				/* for (index = 0; index < options.length; ++index) {
				  option = options[index];
				  select.options.add(new Option(option.name, option.cityId));
				} */
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('Region List Not Found!', '2000', 'teal lighten-2');
				  //alert(error +"---"+ xhr+"---"+status);
				/* $('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Region List Not Found"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000); */
				}
		});		
		
	});
	
	$('#regionList').change(function() {
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('areaList');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options

		select.options.add(new Option("Choose Area", ""));
		$.ajax({
			url : myContextPath+"/fetchAreaListByRegionId?regionId="+$('#regionList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {

				/* alert(data); */
				var options, index, option;
				select = document.getElementById('areaList');

				for (var i = 0, len = data.length; i < len; ++i) {
					var area = data[i];
					//alert(state.name+" "+ state.stateId);
					select.options.add(new Option(area.name, area.areaId));
				}
				
				/* for (index = 0; index < options.length; ++index) {
				  option = options[index];
				  select.options.add(new Option(option.name, option.cityId));
				} */
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				  Materialize.toast('Area List Not Found!', '2000', 'teal lighten-2');
			/* 	$('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Area List Not Found"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000); */
				}
		});		
		
	});
	
$('#areaList').change(function() {
		
		$.ajax({
			url : myContextPath+"/fetchArea?areaId="+$('#areaList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) 
			{
				$('#pincode').val(data.pincode);
				$('#pincode').focus();
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				 Materialize.toast('Area Details Not Found!', '2000', 'teal lighten-2');
				  //alert(error +"---"+ xhr+"---"+status);
				/* $('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Area Details Not Found"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000); */
				}
		});		
		
	});
});

function checkBusinessDuplication(checkText,type){
	var status=false;
	$.ajax({
		url : "${pageContext.servletContext.contextPath}/checkBusinessDuplicationForUpdate?checkText="+checkText+"&type="+type+"&businessNameId=${businessName.businessNameId}",
		async:false,
		success : function(data) {
			if(data==="Success"){
				status=true;
			}else{
				status=false;
			}
		},
		error: function(xhr, status, error) {
			alert("Error");
		}
	});
	
	return status;
}

</script>

<style type="text/css">
h6{
	font-size:0.9rem !important;
}
</style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/updateBusiness" method="post" id="updateBusinessForm">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Business Details </h4>
                    </div>
                     <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">store</i>
                        <input id="businessNameId" type="hidden" class="validate" name="businessNameId" required value="<c:out value="${businessName.businessNameId}" />">
                        <input id="name" type="text" class="validate" name="shopName" required value="<c:out value="${businessName.shopName}" />">
                        <label for="name" class="active"><span class="red-text">*</span>Shop Name</label>

                    </div>
                     <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">person</i>
                        <input id="name" type="text" class="validate" name="ownerName" required value="<c:out value="${businessName.ownerName}" />">
                        <label for="name" class="active"><span class="red-text">*</span>Owner Name</label>

                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix	">work</i>
                         <label for="businessType" class="active"><span class="red-text">*</span></label>
                        <select name="businessTypeId" id="businessType" required>
                                 <option value="" selected>Business Type</option>
                                <c:if test="${not empty businessTypeList}">  
									<c:forEach var="listValue" items="${businessTypeList}">
										<option value="<c:out value="${listValue.businessTypeId}" />" <c:if test="${businessName.businessType.businessTypeId == listValue.businessTypeId}">selected</c:if>><c:out
												value="${listValue.name}" /></option>
									</c:forEach>
								</c:if>
                        </select>
                    </div>
                </div>
                <div class="row  z-depth-3">
                
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Contact Details </h4>
                    </div>
                    
                   <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="fa fa-volume-control-phone prefix" aria-hidden="true"></i>
                        <input id="telephoneNo" type="tel" name="telephoneNumber" minlength="10" maxlength="15" value="<c:out value="${businessName.contact.telephoneNumber}" />">
                        <label for="telephoneNo" class="active">Telephone No.</label>
                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">stay_current_portrait<span class="red-text">*</span></i>
                        <input id="mobileNo" type="tel" name="mobileNumber" minlength="10" maxlength="10" value="<c:out value="${businessName.contact.mobileNumber}" />">
                        <label for="mobileNo" class="active">Mobile No.</label>
                    </div>

                   <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">mail</i>
                        <input id="emailId" type="email" name="emailId" value="<c:out value="${businessName.contact.emailId}" />">
                        <label for="emailId" class="active">Email Id</label>
                    </div>
                    <%-- <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">store</i>
                        <textarea id="line1" class="materialize-textarea" name="address" required><c:out value="${businessName.address}" /></textarea>
                       <!--  <textarea id="line1" class="validate" name="address" class="materialize-textarea" required></textarea> -->
                        <label for="line1" class="active"><span class="red-text">*</span>Address</label>

                    </div> --%>
                </div>
                <div class="row z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Address </h4>
                    </div>
                  <%--   <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">store</i>
                        <input id="line1" type="text" class="validate" name="address" required value="<c:out value="${businessName.address}" />">
                        <label for="line1" class="active">Line 1</label>

                    </div>
		
 --%>
 					<div class="input-field col s12 m10 l10 push-l1 push-m1">
                        <i class="material-icons prefix">store</i>
                        <input id="line1" type="text" name="address" value="<c:out value="${businessName.address}" />" required>
                       <!--  <textarea id="line1" class="materialize-textarea" name="address" required></textarea> -->
                       <!--  <textarea id="line1" class="validate" name="address" class="materialize-textarea" required></textarea> -->
                        <label for="line1" class="active"><span class="red-text">*</span>Address</label>

                    </div>
 						<%-- <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">store</i>
                        <textarea id="line1" class="materialize-textarea" name="address" required><c:out value="${businessName.address}" /></textarea>
                       <!--  <textarea id="line1" class="validate" name="address" class="materialize-textarea" required></textarea> -->
                        <label for="line1" class="active"><span class="red-text">*</span>Address</label>

                    </div> --%>
 	                     

                   <%--  <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on</i>
                        <select name="countryid">
                                 <option value="0" selected>Country</option>
                                <c:if test="${not empty countrylists}">
							<c:forEach var="listValue" items="${countrylists}">
								<option value="<c:out value="${listValue.countryId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div> --%>
                    <%--  <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">location_on</i>
                         <label for="stateList" class="active"><span class="red-text">*</span></label>
                         <select name="stateId" id="stateList">
                                 <option value="0" selected>State</option>
                                 <c:if test="${not empty stateList}">
								<c:forEach var="listValue" items="${stateList}">
									<option value="<c:out value="${listValue.stateId}" />"><c:out	value="${listValue.name}" /></option>
								</c:forEach>
							</c:if> 
                        </select>
                        </div> --%>
                        <%-- <div style="display:none;">
                        
                        <select name="countryid" id="countryList" >
                                 <option value="0" selected>Country</option>
                                <c:if test="${not empty coutryList}">
								<c:forEach var="listValue" items="${coutryList}">
								<c:set var="indiaset" scope="session" value="India" />
									<option value="<c:out value="${listValue.countryId}" />" <c:if test="${listValue.name == indiaset}">selected="selected"</c:if>><c:out
											value="${listValue.name}" /></option>
								</c:forEach>
							</c:if>
                        </select>
                        </div> --%>
                       
                   <div class="row">
                   	<div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">location_on</i>
                         <label for="cityList" class="active"><span class="red-text">*</span></label>
                        <select name="cityid" id="cityList" required>    
                                 <option  value="" selected>City</option>
                                 <c:if test="${not empty cityList}">
									<c:forEach var="listValue" items="${cityList}">
										<option value="<c:out value="${listValue.cityId}" />"><c:out
												value="${listValue.name}" /></option>
									</c:forEach>
								</c:if> 
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">track_changes</i>
                         <label for="regionList" class="active"><span class="red-text">*</span></label>
                        <select name="regionId" id="regionList" required>
                                 <option value="" selected>Region</option>
                        </select>
                    </div>
                   </div>
                     <div class="row">
                     <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">business</i>
                         <label for="areaList" class="active"><span class="red-text">*</span></label>
                        <select name="areaId"  id="areaList" class="active" required>
                                <option value="" selected>Area</option>
                        </select>
                    </div>
					<div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">gps_fixed</i>
                        <input id="pincode" type="text" class="grey lighten-3" name="pincode" readonly value="<c:out value="${businessName.area.pincode}" />">
                        <label for="pincode" class="active">pincode</label>
                    </div>
                    </div>
                </div>


                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Other Details </h4>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">rate_review</i>
                        <input id="gstNo" type="text" name="gstinNumber" minlength="15" maxlength="15"  value="<c:out value="${businessName.gstinNumber}" />">
                        <label for="gstNo" class="active">Enter GST No.</label>

                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1">

                        <i class="material-icons prefix">credit_card</i>
                        <input id="creditLimit" type="text" name="creditLimit" required value="<c:out value="${businessName.creditLimit}" />">
                        <label for="creditLimit" class="active">Enter Credit Limit</label>

                    </div>
                    
                </div>
                <div class="row z-depth-3">
                   <div class="col s12 l12 m12">
                       <div class="col s12 l5 m5 push-l1 push-m1">
                       <br>
                           <h6>Added Date : 
							<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${businessName.businessAddedDatetime}" /><c:out value="${dt}" />
							&
							<fmt:formatDate pattern="HH:mm:ss" var="time" value="${businessName.businessAddedDatetime}" /><c:out value="${time}" />
                           </h6>.
                            <br>
                       </div>
                       <div class="col s12 l5 m5 push-l1 push-m1">
                        <br>
                           <h6>Last Updated Date : 
							<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${businessName.businessUpdatedDatetime}" /><c:out value="${dt}" />
							&
							<fmt:formatDate pattern="HH:mm:ss" var="time" value="${businessName.businessUpdatedDatetime}" /><c:out value="${time}" />                           
                           </h6>
                            <br>
                       </div>
                   </div>
               </div>
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue darken-8" type="submit" id="updateBusinessSubmit">Update Business<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>

        </div>

	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>

	<!-- <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->


    </main>
    <!--content end-->
</body>

</html>