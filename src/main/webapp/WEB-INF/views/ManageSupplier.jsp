<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
<script type="text/javascript">

var checkedId=[];
   $(document).ready(
			function() {
				$('.mobileDiv').hide();
				var table = $('#tblData').DataTable();
				table.destroy();
				$('#tblData').DataTable({
			         "oLanguage": {
			             "sLengthMenu": "Show:  _MENU_",
			             "sSearch": "_INPUT_" //search
			         },
			         "autoWidth": false,
			         "columnDefs": [
									{ "width": "2%", "targets": 0},
									{ "width": "5%", "targets": 2},
									{ "width": "3%", "targets": 3},
									{ "width": "3%", "targets": 5},
					                { "width": "9%", "targets": 6},
					                { "width": "9%", "targets": 7},
					                { "width": "2%", "targets": 8},	
					                { "width": "2%", "targets": 9}	
					                
					              ],
			         lengthMenu: [
			             [10, 25., 50, -1],
			             ['10 ', '25 ', '50 ', 'All']
			         ],
			         
			         dom: 'lBfrtip',
			         buttons: {
			             buttons: [
			                 //      {
			                 //      extend: 'pageLength',
			                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
			                 //  }, 
			                 {
			                     extend: 'pdf',
			                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
			                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
			                     //title of the page
			                     title: function() {
			                         var name = $(".heading").text();
			                         return name
			                     },
			                     //file name 
			                     filename: function() {
			                         var d = new Date();
			                         var date = d.getDate();
			                         var month = d.getMonth();
			                         var year = d.getFullYear();
			                         var name = $(".heading").text();
			                         return name + date + '-' + month + '-' + year;
			                     },
			                     //  exports only dataColumn
			                     exportOptions: {
			                         columns: ':visible.print-col'
			                     },
			                     customize: function(doc, config) {
			                         var tableNode;
			                         for (i = 0; i < doc.content.length; ++i) {
			                           if(doc.content[i].table !== undefined){
			                             tableNode = doc.content[i];
			                             break;
			                           }
			                         }
			                         
			                         var rowIndex = 0;
			                         /* var tableColumnCount = tableNode.table.body[rowIndex].length;
			                          
			                         if(tableColumnCount > 6){
			                           doc.pageOrientation = 'landscape';
			                         } */
			                         /*for customize the pdf content*/ 
			                         doc.pageMargins = [5,20,10,5];
			                         doc.styles['table']={width:'100%'};
			                         doc.defaultStyle.fontSize = 8;
			                         doc.styles.title.fontSize = 12;
			                         doc.styles.tableHeader.fontSize = 11;
			                         doc.styles.tableFooter.fontSize = 11;			                        
			                         doc.styles.tableHeader.alignment = 'center';
			                         doc.styles.tableBodyEven.alignment = 'center';
			                         doc.styles.tableBodyOdd.alignment = 'center';
			                       },
			                      
			               
			                 },
			                 {
			                     extend: 'excel',
			                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
			                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
			                     //title of the page
			                     title: function() {
			                         var name = $(".heading").text();
			                         return name
			                     },
			                     //file name 
			                     filename: function() {
			                         var d = new Date();
			                         var date = d.getDate();
			                         var month = d.getMonth();
			                         var year = d.getFullYear();
			                         var name = $(".heading").text();
			                         return name + date + '-' + month + '-' + year;
			                     },
			                     //  exports only dataColumn
			                     exportOptions: {
			                         columns: ':visible.print-col'
			                     },
			                 },
			                 {
			                     extend: 'print',
			                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
			                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
			                     //title of the page
			                     title: function() {
			                         var name = $(".heading").text();
			                         return name
			                     },
			                     //file name 
			                     filename: function() {
			                         var d = new Date();
			                         var date = d.getDate();
			                         var month = d.getMonth();
			                         var year = d.getFullYear();
			                         var name = $(".heading").text();
			                         return name + date + '-' + month + '-' + year;
			                     },
			                     //  exports only dataColumn
			                     exportOptions: {
			                         columns: ':visible.print-col'
			                     },
			                 },
			                 {
			                     extend: 'colvis',
			                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
			                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
			                     collectionLayout: 'fixed two-column',
			                     align: 'left'
			                 },
			             ]
			         }

			     });
				$('select').material_select();
			     $("select")
			         .change(function() {
			             var t = this;
			             var content = $(this).siblings('ul').detach();
			             setTimeout(function() {
			                 $(t).parent().append(content);
			                 $("select").material_select();
			             }, 200);
			         });
			     $('.dataTables_filter input').attr("placeholder", "Search");
			     
				var msg="${saveMsg}";
				// alert(msg);
				 if(msg!='' && msg!=undefined)
				 {
					 $('#addeditmsg').find("#modalType").addClass("success");
					 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
				     $('#addeditmsg').modal('open');
				    // $('#msgHead').text("Product Message");
				     $('#msg').text(msg);
				 }
				 
				
				 $("#checkAll").click(function () {
						var colcount=$('#tblData').DataTable().columns().header().length;
					    var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
					        state = this.checked;
							//alert(state);
					    for (var i = 0; i < cells.length; i += 1) {
					        cells[i].querySelector("input[type='checkbox']").checked = state;
					    }		                
					    
					});
					
					 $("input:checkbox").change(function(a){
						 
						 var colcount=$('#tblData').DataTable().columns().header().length;
					     var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
					         
					      state = false; 				
					     var ch=false;
					     
					     for (var i = 0; i < cells.length; i += 1) {
					    	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
					    	
					         if(cells[i].querySelector("input[type='checkbox']").checked == state)
					         { 
					        	 $("#checkAll").prop('checked', false);
					        	 ch=true;
					         }                      
					     }
					     if(ch==false)
					     {
					    	 $("#checkAll").prop('checked', true);
					     }
					    	
					    //alert($('#tblData').DataTable().rows().count());
					});
					
				 $('#sendsmsid').click(function(){
					 
					 $('#mobileEditSms').prop('checked', false);
					 	$("#mobileEditSms").change();
					 	mobileNumberCollection='';
					 
						var count=parseInt('${count}')
						checkedId=[];
						//chb
						 var idarray = $("#employeeTblData")
			          .find("input[type=checkbox]") 
			          .map(function() { return this.id; }) 
			          .get();
						 var j=0;
						 var mobileNumberCollection="";
						 for(var i=0; i<idarray.length; i++)
						 {								  
							 idarray[i]=idarray[i].replace('chb','');
							 if($('#chb'+idarray[i]).is(':checked'))
							 {
								 checkedId[j]=idarray[i].split("_")[0];
								 mobileNumberCollection=mobileNumberCollection+idarray[i].split("_")[1]+",";
								 j++;
							 }
						 }
						
						 mobileNumberCollection=mobileNumberCollection.substring(0,mobileNumberCollection.length-1);
						 $('#mobileNoSms').val(mobileNumberCollection);
						 $('#mobileNoSms').change();
						 
						 if(checkedId.length==0)
						 {
							 $('#addeditmsg').find("#modalType").addClass("warning");
							 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
							 $('#addeditmsg').modal('open');
						     //$('#msgHead').text("Message : ");
						     $('#msg').text("Select Supplier For Send Message");
						 }
						 
						 else
						 {
							 if(checkedId.length==1)
							 {
								 $('#smsText').val('');
								 $('#smsSendMesssage').html('');
								 $('.mobileDiv').show();
								 $('#sendMsg').modal('open');
							 }
							 else
							 {
								 $('#smsText').val('');
								 $('#smsSendMesssage').html('');
								 $('.mobileDiv').hide();
								 $('#sendMsg').modal('open');
							 }
						 }
					});
					
					$('#sendMessageId').click(function(){
						
						
						 $('#smsSendMesssage').html('');
						
						 var smsText=$("#smsText").val();
							if(checkedId.length==0)
							{
								$('#smsSendMesssage').html("<font color='red'>Select Employee For Send Message</font>");
								return false;
							}
							if(smsText==="")
							{
								$('#smsSendMesssage').html("<font color='red'>Enter Message Text</font>");
								return false;
							}
						
						var form = $('#sendSMSForm');
						//alert(form.serialize()+"&employeeDetailsId="+checkedId);
						
						$.ajax({
								type : form.attr('method'),
								url : form.attr('action'),
								data : form.serialize()+"&supplierIds="+checkedId+"&mobileNumber="+$('#mobileNoSms').val(),
								//async: false,
								success : function(data) {
									if(data==="Success")
									{
										$('#smsSendMesssage').html("<font color='green'>Message send SuccessFully</font>");
										$('#smsText').val('');
									}
									else
									{
										$('#smsSendMesssage').html("<font color='red'>Message sending Failed</font>");
									}
								}
						});
					});
					/* $("input#mobileEditSms:checkbox").change(function(a){
					if($('input#mobileEditSms[type=checkbox]').prop('checked')){
						alert();
					}
					}); */
			});
   
			
    function viewProducts(url)
    {
    	var table = $('#modalTable').DataTable();
		table.destroy();
    	   $.ajax({
    			url : url,
    			dataType : "json",
    			success : function(data) {
    			//alert(data);
    				
    				var supplier = data[0].supplier;
    				$("#suplierName").text(supplier.name);
    				$("#mobNo").text(supplier.contact.mobileNumber);
    				$("#address").text(supplier.address);
    				$("#gstno").text(supplier.gstinNo);
    			
    			    $("#tbproductlist").empty();
    				var srno=1;
    				for (var i = 0, len = data.length; i < len; ++i) {
    					var supplierproduct = data[i];
    					
    					var mrp=((parseFloat(supplierproduct.supplierRate))+((parseFloat(supplierproduct.supplierRate)*parseFloat(supplierproduct.product.categories.igst))/100)).toFixed(0);
    					var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierproduct.product.categories.igst));
    					
    					$("#tbproductlist").append("<tr>"+
                            "<td>"+srno+"</td>"+
                            "<td>"+supplierproduct.product.productName+"</td>"+
                            "<td>"+supplierproduct.product.categories.categoryName+"</td>"+
                            "<td>"+supplierproduct.product.brand.name+"</td>"+
                            "<td>"+correctAmoutWithTaxObj.mrp+"</td>"+
                            "<td>"+supplierproduct.supplierRate+"</td>"+
                        "</tr>"); 
    					srno++;
    				}
    					    
    				$('#modalTable').DataTable({
    					 "bSort" : false,
    					 "bPaginate": false,
    					 "autoWidth":false,
    					 "columnDefs": [
    									{ "width": "2%", "targets": 0},
    									{ "width": "20%", "targets": 1},
    									{ "width": "8%", "targets": 2},
    									{ "width": "8%", "targets": 3},
    									{ "width": "3%", "targets": 4},
    									{ "width": "3%", "targets": 5}
    					                
    					              ],
    				      dom: 'B',
    				      buttons: {
    				             buttons: [
    				              
    				                 {
    				                     extend: 'pdf',
    				                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
    				                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
    				                     //title of the page
    				                     title: 'Product Details',
    				                     //file name 
    				                     filename: function() {
    				                         var d = new Date();
    				                         var date = d.getDate();
    				                         var month = d.getMonth();
    				                         var year = d.getFullYear();
    				                         var name = 'Product Details(Supplier)';
    				                         return name + date + '-' + month + '-' + year;
    				                     },
    				                    
    				                     //  exports only dataColumn
    				                     exportOptions: {
    				                         columns: '.print-col',
    				                         
    				                     },
    				                     customize: function(doc, config) {
    				                    	 doc.content.forEach(function(item) {
    				                    		  if (item.table) {
    				                    		  item.table.widths = ['*','*','*','*','*','*'] 
    				                    		 } 
    				                    		    });
    				                    		    doc.styles.tableHeader.alignment = 'center';
    				                         doc.styles.tableBodyEven.alignment = 'center';
    				                         doc.styles.tableBodyOdd.alignment = 'center';
    				                     	doc.content.splice(0,1);
    				                        	/* doc['header']=(function() {
    				 							return {
    				 								columns:[ 
    				 								          [
    				 									
    				 									{
    				 										alignment: 'left',
    				 										italics: true,
    				 										text: 'dataTablesTable',
    				 										fontSize: 18,
    				 										margin: [10,0]
    				 									},
    				 									{
    				 										alignment: 'right',
    				 										fontSize: 14,
    				 										text: 'Custom PDF export with dataTables'
    				 									}
    				 								],
    				 								[
 				 									
 				 									{
 				 										alignment: 'left',
 				 										italics: true,
 				 										text: 'dataTablesTable',
 				 										fontSize: 18,
 				 										margin: [10,0]
 				 									},
 				 									{
 				 										alignment: 'right',
 				 										fontSize: 14,
 				 										text: 'Custom PDF export with dataTables'
 				 									}
 				 								],
    				 								],
    				 								margin: 20
    				 							}
    				 						}); */
    				                     },
    				                     messageTop: function () {                 
    				                    	 return 'Name:'+supplier.name+'      '+'Mobile No:'+supplier.contact.mobileNumber+'       '+'Gst No:'+supplier.gstinNo+'      '+'Address:'+supplier.address;
				                         },
				                          /* customize: function ( doc,config ) {
				                        	  doc.content.forEach(function(item) {
					                    		  if (item.table) {
					                    		  item.table.widths = [40,'*','*','*',40] 
					                    		 } 
					                    		    })
					                    		    doc.styles.tableHeader.alignment = 'center';
						                         doc.styles.tableBodyEven.alignment = 'center';
						                         doc.styles.tableBodyOdd.alignment = 'center';
				                         } */
    				                
    				                 },
    				                 {
    				                     extend: 'excel',
    				                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
    				                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
    				                     //title of the page
    				                     title: 'Product Details',
    				                     //file name 
    				                     filename: function() {
    				                         var d = new Date();
    				                         var date = d.getDate();
    				                         var month = d.getMonth();
    				                         var year = d.getFullYear();
    				                         var name = 'Product Details(Supplier)';
    				                         return name + date + '-' + month + '-' + year;
    				                     },
    				                      messageTop: function () {                 
				                             return 'Name:'+supplier.name+'      '+'Mobile No:'+supplier.contact.mobileNumber+'       '+'Gst No:'+supplier.gstinNo+'      '+'Address:'+supplier.address;
				                         }, 
				                         /* customize: function ( win ) {
    				                         $(win.document.body)
    				                             .css( 'font-size', '10pt' )
    				                             .prepend(
    				                                 '<h6 style="position:absolute; top:0; left:0;">Name:'+supplier.name+'</h6>'+
    				                                 
    				                                 '<h6 style="position:absolute; top:0; right:10%;">Name:'+supplier.name+'</h6>'+
    				                                 '<br/>'+
													 '<h6 style="position:absolute; top:5%; left:0;bottom:5%">Name:'+supplier.name+'</h6>'+    				                                 
    				                                 '<h6 style="position:absolute; top:5%; right:10%;bottom:5%;">Name:'+supplier.name+'</h6>'
    				                             );
				                         }, */
    				                    
    				                     //  exports only dataColumn
    				                     exportOptions: {
    				                         columns: ':visible.print-col'
    				                     },
    				                 },
    				                 {
    				                     extend: 'print',
    				                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
    				                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
    				                     //title of the page
    				                    title: '',
    				                     //file name 
    				                     filename: function() {
    				                         var d = new Date();
    				                         var date = d.getDate();
    				                         var month = d.getMonth();
    				                         var year = d.getFullYear();
    				                         var name = 'Product Details(Supplier)';
    				                         return name + date + '-' + month + '-' + year;
    				                     },
    				                    
    				                     customize: function (win) {
    				                         $(win.document.body)
    				                             .css( 'font-size', '10pt' )
    				                             .prepend(
    				                                 '<h6 style="position:absolute; top:0; left:0;">Name:'+supplier.name+'</h6>'+
    				                                 
    				                                 '<h6 style="position:absolute; top:0; right:10%;">Mobile No:'+supplier.contact.mobileNumber+'</h6>'+
    				                                 '<br/>'+
													 '<h6 style="position:absolute; top:5%; left:0;">Gst No:'+supplier.gstinNo+'</h6>'+    				                                 
    				                                 '<h6 style="position:absolute; top:5%; right:10%;">Address:'+supplier.address+'</h6>'+'<br/>'
    				                             );
    				     				                     } ,
				                       
    				                     
    				                     //  exports only dataColumn
    				                     exportOptions: {
    				                         columns: ':visible.print-col'
    				                     },
    				                 }
    				                 ]
    				         }
    				  });
    				 
    				$('.modal').modal();
    				$('#viewDetails').modal('open');
    				//alert("data came");
    			/* 	var table = $('#modalTable').DataTable();
    				table.destroy(); */
    				return false;
    				/* for (index = 0; index < options.length; ++index) {
    				  option = options[index];
    				  select.options.add(new Option(option.name, option.cityId));
    				} */
    				
    			},
    			error: function(xhr, status, error) {
    				  //var err = eval("(" + xhr.responseText + ")");
    				  //alert(error +"---"+ xhr+"---"+status);
    				  	$('#addeditmsg').find("#modalType").addClass("warning");
    					 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
    				  	 //$('#addeditmsg').modal('open');
    				     $('#msgHead').text("Manage Supplier Message");
    				     $('#msg').text("Not Have Product List");
    				}
    		});
    }
   </script>

<style>
.noborder>td{
	border:none !important;
	border-bottom:none !important;
	border-left:none !important;
	/* empty-cells: hide; */
}

tr.noborder:first-child th:first-child, tr.noborder td:first-child{
border-left:none !important;
}
#modalTable_wrapper div.dt-buttons{
	margin-left: 33% !important;
}

 .sidenavDarker{
 	color:#282d36 !important;
 }
</style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <!--Add New Supplier-->
        <div class="row">
       <div class="col s10 l3 m6 left offset-s2">
            <a class="btn waves-effect waves-light blue-gradient left" href="${pageContext.request.contextPath}/addSupplier"><i class="material-icons left" >add</i>Add Supplier</a>
        </div>
 

        <div class="col s12 l12 m12">
          <table class="striped highlight centered mdl-data-table display  select" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr. No.</th>
                        <th class="print-col">Name</th>
                        <th>Products</th>
                        <th class="print-col">Contact</th>
                        <!-- <th class="print-col">Email</th> -->
                        <th class="print-col">Address</th>
                        <th class="print-col">GST IN</th>
                        <th class="print-col">Added</th>
                        <th class="print-col">Updated</th>
                        <th>Edit</th>
                        <!-- <th>Delete</th> -->
                        <th> <a href="#" class="modal-trigger tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Send SMS" id="sendsmsid"><i class="material-icons">mail</i></a><br>
                            <input type="checkbox" class="filled-in checkAll" id="checkAll"/>
                            <label for="checkAll" style="padding-left:20px;"></label>
                        </th>
                    </tr>
                </thead>

                <tbody id="employeeTblData">
                	<c:if test="${not empty fetchSupplierList}">
							<c:forEach var="listValue" items="${fetchSupplierList}">
                    	<tr>
                        <td><c:out value="${listValue.srNo}" /></td>
                        <td class="wrapok"><c:out value="${listValue.name}" /></td>
                        <td>
                            <button id="getProductList" class=" btn blue-gradient" onclick="viewProducts('${pageContext.servletContext.contextPath}/fetchSupplierProductList?supplierId=${listValue.supplierId}')" class="modal-trigger">View</button>
                           <%--  <input id="supplierId_${listValue.srNo}" type="hidden" value="<c:out value="${listValue.supplierId}" />"> --%>
                        </td>

                        <td><c:out value="${listValue.mobileNumber}" /></td>
                       <%--  <td class="wrapok"><c:out value="${listValue.emailId}" /></td> --%>
                        <td><c:out value="${listValue.address}" /></td>
                        <td class="wrapok"><c:out value="${listValue.gstinNo}" /></td>
                        <td><fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.addedDate}" /><c:out value="${dt}" /><br><fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.addedDate}" /><c:out value="${time}" />		                         
                        </td>
                        <td><fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.updatedDate}" /><c:out value="${dt}" /><br><fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.updatedDate}" /><c:out value="${time}" />
                        </td>
                        <td><a class=" btn-flat " href="${pageContext.servletContext.contextPath}/fechSupplier?supplierId=${listValue.supplierId}"><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a></td>
                        <!-- Modal Trigger -->
                        <!-- <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                        <td>

                            <input type="checkbox" class="filled-in" id="chb${listValue.supplierId}_${listValue.mobileNumber}" />
                            <label for="chb${listValue.supplierId}_${listValue.mobileNumber}" style="padding-left: 20px;"></label>

                        </td>
                    </tr>
                   </c:forEach>
					</c:if>
				</tbody>
            </table>


                    <!-- Modal Structure for delete -->

                    <!-- <div id="delete" class="modal">
                        <div class="modal-content">
                            <h4>Confirmation</h4>
                            <p>Are you sure you want to delete?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
                            <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
                        </div>
                    </div> -->
                    <!-- Modal Structure for sendMsg -->
			<form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMSTOSupplier" method="post">
               <div id="sendMsg" class="modal">
                    
                        <div class="modal-content">                                   	
                        
                         
                           <div class="fixedDiv">
                              <div class="center-align" style="padding:0">
                         		<!-- <i class="material-icons  medium" style="padding:50% 40% 0 40%">mail</i> -->
                         	  </div>
                         	  </div>
                         	  
                         	  <div class="scrollDiv">
                         	  
                         	 <div class="row mobileDiv" style="margin-bottom:0">
                         	  	<div class="col s12 l9 m9 input-field" style="padding-left:0">
                                    <label for="mobileNoSms" class="black-text" style="left:0">Mobile No</label>
                                    <input type="text" id="mobileNoSms" class="grey lighten-3" minlength="10" maxlength="10" readonly>
                                    	
                               </div>  
                                 <div class="col s12 l2 m2 right-align" style="margin-top:10%">
                                    <input type="checkbox" id="mobileEditSms"/>
                                    <label for="mobileEditSms" class="black-text">Edit</label>
                                    
                                </div>
                               </div>
                                <div class="input-field">
                                    <label for="smsText" class="black-text">Type Message</label>
                                    <textarea id="smsText" class="materialize-textarea" data-length="180" name="smsText"></textarea>
                                </div> 
                                <div class="input-field center">                                
                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
            					</div>
            					<div class="fixedFooter">
            					 <div class="col s6 m4 l3 right-align" style="margin-left:3%">
                            		 <a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
                                 </div>
		                          	<div class="col s6 m6 l2">	
		                           		<button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
		                           			 
		                           			
		                             </div>
		                         </div>
		                           </div>  	
		                          <br><br><br>
                         	  
                         	                             
                            </div>
                            
                               				
                        </div>
                      <!--   <div class="modal-footer center-align row" style="margin-bottom:0">
                         <div class="col s12 m12 l12 divider">  </div>
                         	
                       
                            
                            
                          
                        </div>
                        -->
                 
                     </form>
                
        </div>
        </div>
        <!-- Modal Structure for View Product Details -->

        <div id="viewDetails" class="row modal modal-fixed-footer">
            <div class="modal-content">
                <h5 class="center"><u>Product Details</u></h5>
              
                   <table  class="centered tblborder" id="modalTable">
                    <thead>
                    <tr class="noborder">
                    	<td colspan="3" class="print-col"><h6><b>Name:</b> <span id="suplierName"></span></h6></td>
                    	<td colspan="2" class="print-col"><h6><b>Mobile No:</b><span id="mobNo"></span></h6></td>
                    	</tr>
                    	<tr class="noborder">
                    	<td colspan="3" class="print-col nowrap"><div class="col s12 m2 l2" style="padding:0"><h6><b>Address:</b></h6></div> 
                    									  <div class="col s12 m6 l6" style="padding:0"><b><h6 id="address"></h6></b></div>
                    	</td>
                    	<td colspan="2" class="print-col"> <h6><b>GST No:</b><span id="gstno"></span></h6></td>
                    </tr>
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col ">Product Name</th>
                            <th class="print-col">Category</th>
                            <th class="print-col">Brand</th>
                            <th class="print-col wrapok">MRP</th>
                            <th class="print-col wrapok">Unit Price</th>
                        </tr>
                    </thead>
                    <tbody id="tbproductlist">
                       <!--  <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
          
            <div class="modal-footer">
			
				<div class="col s12 m6 l5 offset-l2">
						 <a href="#!" id="productDetailsClose" class="modal-action modal-close waves-effect center btn red">Close</a>
				</div>
				
				
			</div>			
               
            </div>
       <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
					</div>
				</div>
			</div>
		</div>
        
	<!-- <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
    </main>
    <!--content end-->
</body>

</html>