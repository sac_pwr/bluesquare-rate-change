<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>

<head>

    <%@include file="components/header_imports.jsp" %>
	<script>var myContextPath = "${pageContext.request.contextPath}"</script>
	<script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	
	<!-- for find date different -->
	<script src="resources/js/moment.min.js"></script>
	 
	<script type="text/javascript">
$(document).ready(function() {	
	$("#checkAll").click(function () {
    	var colcount=$('#tblData').DataTable().columns().header().length;
        var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
            state = this.checked;
			//alert(state);
        for (var i = 0; i < cells.length; i += 1) {
            cells[i].querySelector("input[type='checkbox']").checked = state;
        }		                
        
    });
    
     $("input:checkbox").change(function(a){
    	 
    	 var colcount=$('#tblData').DataTable().columns().header().length;
         var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
             
          state = false; 				
         var ch=false;
         
         for (var i = 0; i < cells.length; i += 1) {
        	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
        	
             if(cells[i].querySelector("input[type='checkbox']").checked == state)
             { 
            	 $("#checkAll").prop('checked', false);
            	 ch=true;
             }                      
         }
         if(ch==false)
         {
        	 $("#checkAll").prop('checked', true);
         }
        	
        //alert($('#tblData').DataTable().rows().count());
    });
	
	$('#sendsmsid').click(function(){
		
		$('#mobileEditSms').prop('checked', false);
	 	$("#mobileEditSms").change();
	 	mobileNumberCollection='';
		
		var count=parseInt('${count}')
		checkedId=[];
		//chb
		 var idarray = $("#employeeTblData")
         .find("input[type=checkbox]") 
         .map(function() { return this.id; }) 
         .get();
		 var j=0;
		 var mobileNumberCollection="";
		 for(var i=0; i<idarray.length; i++)
		 {								  
			 idarray[i]=idarray[i].replace('chb','');
			 if($('#chb'+idarray[i]).is(':checked'))
			 {
				 checkedId[j]=idarray[i].split("_")[0];
				 mobileNumberCollection=mobileNumberCollection+idarray[i].split("_")[1]+",";
				 j++;
			 }
		 }
		 mobileNumberCollection=mobileNumberCollection.substring(0,mobileNumberCollection.length-1);
		 $('#mobileNoSms').val(mobileNumberCollection);
		 $('#mobileNoSms').change();
		 $('#smsText').val('');
		 $('#smsSendMesssage').html('');
		 
		 if(checkedId.length==0)
		 {
			 $('#addeditmsg').find("#modalType").addClass("warning");
			 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
			 $('#addeditmsg').modal('open');							    
		     $('#msg').text("Select Employee For Send SMS");
		 }
		 else
		 {
			 if(checkedId.length==1)
			 {
				 $('#smsText').val('');
				 $('#smsSendMesssage').html('');
				 $('.mobileDiv').show();
				 $('#sendMsg').modal('open');
			 }
			 else{
				 $('#smsText').val('');
				 $('#smsSendMesssage').html('');
				 $('.mobileDiv').hide();
				 $('#sendMsg').modal('open');
			 }
		 }
	});
	
	$('#sendMessageId').click(function(){
		
		
		 $('#smsSendMesssage').html('');
		
		 var smsText=$("#smsText").val();
			if(checkedId.length==0)
			{
				$('#smsSendMesssage').html("<font color='red'>Select Employee For Send Message</font>");
				return false;
			}
			if(smsText==="")
			{
				$('#smsSendMesssage').html("<font color='red'>Enter Message Text</font>");
				return false;
			}
		
		var form = $('#sendSMSForm');
		//alert(form.serialize()+"&employeeDetailsId="+checkedId);
		
		$.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize()+"&employeeDetailsId="+checkedId+"&mobileNumber="+$('#mobileNoSms').val(),
				//async: false,
				success : function(data) {
					if(data==="Success")
					{
						$('#smsSendMesssage').html("<font color='green'>Message send SuccessFully</font>");
						$('#smsText').val('');
					}
					else
					{
						$('#smsSendMesssage').html("<font color='red'>Message sending Failed</font>");
					}
				}
		});
	});
});
	</script>
	 	
	<style>
	.noborder>td{
	border:none !important;
	border-bottom:none !important;
	border-left:none !important;
	/* empty-cells: hide; */
}

tr.noborder:first-child th:first-child, tr.noborder td:first-child{
border-left:none !important;
}
#modalTable_wrapper div.dt-buttons{
	margin-left: 20% !important;
}
	.btn-flat:focus,
	.btn-flat:hover {
    background-color: transparent !important;
    box-shadow: none;
}
	/* table.dataTable tbody td {
    padding: 3px 2px !important;
} */
	
#incentive .input-field{
	margin-top:10px !important;
}
	#incentive{
		width:35% !important;
		height:55%;
		border-radius:5px;
	}
#area{
	width:37% !important;
	height:400px !important;
}
	textarea.materialize-textarea {
    overflow-y: hidden;
    padding: 0 0 1.9rem 0 !important;
    resize: none;
    height:2.5rem;
}


span.error {
    color: red;
    display: "inline";
    margin-top: 0 !important;
    margin-left: 0;
    padding: 0;
    font-size: 0.9em;
}

	/* .datepicker table,
	.datepicker td,
	.datepicker th{
	border: none !important; 
     font-size: 13px; 
	}  */
	</style>

    <script>
        $(document).ready(function() {
       	 
            var table = $('#tblData').DataTable();
			 table.destroy();
			 $('#tblData').DataTable({
		         "oLanguage": {
		             "sLengthMenu": "Show _MENU_",
		             "sSearch": " _INPUT_" //search
		         },		    
		         autoWidth: false,
		          columnDefs: [
		                      { 'width': '1%', 'targets': 0},
		                      { 'width': '2%', 'targets': 1},
		                      { 'width': '3%', 'targets': 2},
		                      { 'width': '2%', 'targets': 3},
		                      { 'width': '3%', 'targets': 4},
		                      { 'width': '1%', 'targets': 5},
		                      { 'width': '1%', 'targets': 6 },
		                      { 'width': '1%', 'targets': 7}
		                      ], 
		     
		         lengthMenu: [
		             [10, 25., 50, -1],
		             ['10 ', '25 ', '50 ', 'All']
		         ],
		         
		       
		         //dom: 'lBfrtip',
		         dom:'<lBfr<"scrollDivTable"t>ip>',
		         buttons: {
		             buttons: [
		                 //      {
		                 //      extend: 'pageLength',
		                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
		                 //  }, 
		                 {
		                     extend: 'pdf',
		                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: '.print-col'
		                     },
		                     customize: function(doc, config) {
		                    	 doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [50,120,100,80,100,80] 
		                    		 } 
		                    		    })
		                         var tableNode;
		                         for (i = 0; i < doc.content.length; ++i) {
		                           if(doc.content[i].table !== undefined){
		                             tableNode = doc.content[i];
		                             break;
		                           }
		                         }
		        
		                         var rowIndex = 0;
		                         var tableColumnCount = tableNode.table.body[rowIndex].length;
		                          
		                         if(tableColumnCount > 6){
		                           doc.pageOrientation = 'landscape';
		                         }
		                         console.log(doc);
		                         /*for customize the pdf content*/ 
		                         doc.pageMargins = [5,20,10,5];
		                        
		                         doc.defaultStyle.fontSize = 8	;
		                         doc.styles.title.fontSize = 12;
		                         doc.styles.tableHeader.fontSize = 11;
		                         doc.styles.tableFooter.fontSize = 11;
		                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
		                       },
		                 },
		                 {
		                     extend: 'excel',
		                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                 },
		                 {
		                     extend: 'print',
		                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                 },
		                 {
		                     extend: 'colvis',
		                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
		                     collectionLayout: 'fixed two-column',
		                     align: 'left'
		                 },
		             ]
		         }

		     });
			 $("select")
               .change(function() {
                   var t = this;
                   var content = $(this).siblings('ul').detach();
                   setTimeout(function() {
                       $(t).parent().append(content);
                       $("select").material_select();
                   }, 200);
               });
           $('select').material_select();
           $('.dataTables_filter input').attr("placeholder", "Search");
           var table = $('#tblData').DataTable(); // note the capital D to get the API instance
           var column = table.columns('.toggle');
           column.visible(false);
           $('#showColumn').on('click', function () {
            
           	 //console.log(column);
         
           	 column.visible( ! column.visible()[0] );
         
           });
         /* // Use "DataTable" with upper "D"
            oTableStaticFlow = $('#tblData').DataTable({
                "aoColumnDefs": [{
                    'bSortable': true,
                    'aTargets': [0]
                }],
            });

            $("#checkAll").click(function () {
            	var colcount=oTableStaticFlow.columns().header().length;
                var cells = oTableStaticFlow.column(colcount-1).nodes(), // Cells from 1st column
                    state = this.checked;
				
                for (var i = 0; i < cells.length; i += 1) {
                    cells[i].querySelector("input[type='checkbox']").checked = state;
                }
            }); */
        });
        
        function showAreaDetail(id)
    	{
    		var table = $('#modalTable').DataTable();
    		table.destroy();
    		$.ajax({
    			url : myContextPath+"/fetchEmployeeAreas?employeeDetailsId="+id,
    			dataType : "json",
    			success : function(data) {
    				
    				employeeAreaDetails=data;
    				
    				$('#employeeareaname').html("<b>Name: </b>"+employeeAreaDetails.name);
    				$('#employeeareadeptname').html("<b>Department: </b>"+employeeAreaDetails.departmentName);
    				//alert(employeeAreaDetails.MobileNumber);
    				$('#employeeareamobilenumber').html("<b>Mobile No: </b>"+employeeAreaDetails.mobileNumber);
    				
    				employeeAreaList=employeeAreaDetails.areaList;
    				var srno=1;
    				var empGenId;
    				$("#areaList").empty();
    				for(var i=0; i<employeeAreaList.length; i++)
    				{					
    					arealist=employeeAreaList[i];
    					empGenId=arealist.employeeDetails.employeeDetailsGenId;
    					//alert(arealist);
    					$('#areaList').append("<tr>"+
    					        "<td>"+srno+"</td>"+
    					        "<td>"+arealist.area.name+"</td>"+
    					        "</tr>"); 
    					srno++;
    				}
    				$("#employeeareaId").html("<b>Employee Id: </b>"+empGenId);
    				$('#modalTable').DataTable({
    					 "bSort" : false,
    					 "bPaginate": false,
    					 "bDestroy": true,
    					 "autoWidth":false,					 
    				      dom: 'B',
    				      buttons: {
    				             buttons: [
    				              
    				                 {
    				                     extend: 'pdf',
    				                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
    				                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
    				                     //title of the page
    				                     title: 'Area Details',
    				                     //file name 
    				                     filename: function() {
    				                         var d = new Date();
    				                         var date = d.getDate();
    				                         var month = d.getMonth();
    				                         var year = d.getFullYear();
    				                         var name = 'Area Details(HRM)';
    				                         return name + date + '-' + month + '-' + year;
    				                     },
    				                    
    				                     //  exports only dataColumn
    				                     exportOptions: {
    				                         columns: ':visible.print-col',
    				                         
    				                     },
    				                     customize: function(doc, config) {
    				                    	 doc.content.forEach(function(item) {
    				                    		  if (item.table) {
    				                    		  item.table.widths = ['*','*'] 
    				                    		 } 
    				                    		    });
    				                    	 doc.styles.tableHeader.alignment = 'center';
    				                         doc.styles.tableBodyEven.alignment = 'center';
    				                         doc.styles.tableBodyOdd.alignment = 'center';
    				                     },
    				                     messageTop: function () {                 
    				                    	 return 'Employee Id :'+empGenId+'     '+'Name :'+employeeAreaDetails.name+'      '+'Department :'+employeeAreaDetails.departmentName+'      '+'Mobile No :'+employeeAreaDetails.mobileNumber;
    			                         },
    			                         
    				                 },
    				                 {
    				                     extend: 'excel',
    				                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
    				                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
    				                     //title of the page
    				                     title: 'Area Details',
    				                     //file name 
    				                     filename: function() {
    				                         var d = new Date();
    				                         var date = d.getDate();
    				                         var month = d.getMonth();
    				                         var year = d.getFullYear();
    				                         var name = 'Area Details(HRM)';
    				                         return name + date + '-' + month + '-' + year;
    				                     },
    				                      messageTop: function () {                 
    			                             return 'Employee Id :'+empGenId+'     '+'Name :'+employeeAreaDetails.name+'      '+'Department :'+employeeAreaDetails.departmentName+'      '+'Mobile No :'+employeeAreaDetails.mobileNumber;
    			                         }, 
    			                         
    				                    
    				                     //  exports only dataColumn
    				                     exportOptions: {
    				                         columns: ':visible.print-col'
    				                     },
    				                 },
    				                 {
    				                     extend: 'print',
    				                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
    				                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
    				                     //title of the page
    				                    title: '',
    				                     //file name 
    				                     filename: function() {
    				                         var d = new Date();
    				                         var date = d.getDate();
    				                         var month = d.getMonth();
    				                         var year = d.getFullYear();
    				                         var name = 'Area Details(HRM)';
    				                         return name + date + '-' + month + '-' + year;
    				                     },
    				                    
    				                     customize: function (win) {
    				                    	 console.log(win);
    				                    	
    				                         $(win.document.body)
    				                             .css( 'font-size', '10pt' )
    				                             .prepend(
    				                                 '<h6 style="position:absolute; top:0; left:0;">Employee Id :'+empGenId+'</h6>'+
    				                                 '<h6 style="position:absolute; top:0; right:10%;">Name: '+employeeAreaDetails.name+'</h6>'+
    				                                 '<br/>'+
    				                                 '<h6 style="position:absolute; top:5%; left:0;">Department: '+employeeAreaDetails.departmentName+'</h6>'+
    				                                 '<h6 style="position:absolute; top:5%; right:10%;">Mobile No: '+employeeAreaDetails.mobileNumber+'</h6>'+
    												 '<br/>'
    				                             );
    				     				                     } ,
    			                       
    				                     
    				                     //  exports only dataColumn
    				                     exportOptions: {
    				                         columns: ':visible.print-col'
    				                     },
    				                 }
    				                 ]
    				         }
    				  });
    				$('.modal').modal();
    				$('#area').modal('open');
    			}
    		});
    	}
    </script>	
	
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
       
        <div class="row">
          <br>
       


        <div class="col s12 l12 m12" style="padding:0;">
        
        
      
            <table class="striped highlight centered" id="tblData"  width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr. No.</th>
                        <th class="print-col">Employee Id</th>
                        <th class="print-col" style="min-width:200px;">Name</th>
                        <th class="print-col">Mobile No.</th>
                        <th class="print-col">EmailId</th>
                        <th class="print-col">Department</th>                  
                        <th>Area</th>
                        <th> <a href="#" class="modal-trigger tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Send SMS" id="sendsmsid" style="margin-left:-10%;"><i class="material-icons">mail</i></a>
                            <br>
                            <input type="checkbox" class="filled-in checkAll" id="checkAll"/>
                            <label for="checkAll"></label>
                        </th>
                    </tr>
                </thead>

                <tbody id="employeeTblData">
                 <c:if test="${not empty employeeViewModelList}">
							<c:forEach var="listValue" items="${employeeViewModelList}">													
                  	  <tr ${listValue.status == true ? 'class="red lighten-4"' : ''}>
                        <td><c:out value="${listValue.srno}"/></td>
                        <td><c:out value="${listValue.employeeId}"/></td>
                        <td class="wrapok"><c:out value="${listValue.name}"/></td>
                        <td><c:out value="${listValue.mobileNumber}"/></td>
                        <td><c:out value="${listValue.email}"/></td> 
                        <td><c:out value="${listValue.departmentName}"/></td>
                        <td>
                            <button type="button" class="btn-flat tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Area details" onclick="showAreaDetail('${listValue.employeePkId}')"  style="font-size:10px;font-weight:800;" id="areaDetails${listValue.srno}">
                            	Show
                            </button>
                        </td>
                        <td>
                            <input type="checkbox" class="filled-in chbclass" id="chb${listValue.employeePkId}_${listValue.mobileNumber}" />
                             <label for="chb${listValue.employeePkId}_${listValue.mobileNumber}"></label> 
                        </td>
                    </tr>
                 </c:forEach>
                 </c:if> 
                 </tbody>
                 </table>
                
        
                   <!-- Modal Structure for Area Details -->
        	<div class="row">
			<div class="col s12 m12 l2">
        <div id="area" class="modal row modal-fixed-footer">
            <div class="modal-content">
                <h5 class="center"><u>Area Details</u></h5>
            
                	 <!-- <div class="col s12 l6 m6">
                        <h6 id="employeeareaId" class="black-text">Employee Id:GK1000000001</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="employeeareaname" class="black-text">Name:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="employeeareadeptname" class="black-text">Department:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="employeeareamobilenumber" class="black-text">Mobile No:</h6>
                    </div> -->
                   
                
                <table id="modalTable" border="2" class="centered tblborder">
                	
                    <thead>
                    	<tr class="noborder">
                    		<td width="230px"><h6 id="employeeareaId" class="black-text">Employee Id:GK1000000001</h6></td>
                    		<td><h6 id="employeeareaname" class="black-text">Name:</h6></td>
                    	</tr>
                    	<tr class="noborder">
                    		<td><h6 id="employeeareadeptname" class="black-text">Department:</h6></td>
                    		<td><h6 id="employeeareamobilenumber" class="black-text">Mobile No:</h6></td>
                    	</tr>
                    	
                        <tr>
                            <th class="print-col">Sr.No</th>
                            <th class="print-col">Area Name</th>

                        </tr>
                    </thead>
                    <tbody id="areaList">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
            
				<div class="col s6 m6 l5 offset-l2">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                </div>
            </div>
        </div>
	</div>
        </div>
                    
                    
                    <!-- Modal Structure for sendMsg -->
                   
                    <form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMS" method="post">
                         <div id="sendMsg" class="modal">
                    
                        <div class="modal-content">                                   	
                        
                         
                           <div class="fixedDiv">
                              <div class="center-align" style="padding:0">
                         		<!-- <i class="material-icons  medium" style="padding:50% 40% 0 40%">mail</i> -->
                         	  </div>
                         	  </div>
                         	  
                         	  <div class="scrollDiv">
                         	  
                         	 <div class="row mobileDiv" style="margin-bottom:0">
                         	  	<div class="col s12 l9 m9 input-field" style="padding-left:0">
                                    <label for="mobileNoSms" class="black-text" style="left:0">Mobile No</label>
                                    <input type="text" id="mobileNoSms" class="grey lighten-3" minlength="10" maxlength="10" readonly>
                                    	
                               </div>  
                                 <div class="col s12 l2 m2 right-align" style="margin-top:10%">
                                    <input type="checkbox" id="mobileEditSms"/>
                                    <label for="mobileEditSms" class="black-text">Edit</label>
                                    
                                </div>
                               </div>
                                <div class="input-field">
                                    <label for="smsText" class="black-text">Type Message</label>
                                    <textarea id="smsText" class="materialize-textarea" data-length="180" name="smsText"></textarea>
                                </div> 
                                <div class="input-field center">                                
                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
            					</div>
            					<div class="fixedFooter">
            					 <div class="col s6 m4 l3 right-align" style="margin-left:3%">
                            		 <a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
                                 </div>
		                          	<div class="col s6 m6 l2">	
		                           		<button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
		                           			 
		                           			
		                             </div>
		                         </div>
		                           </div>  	
		                          <br><br><br>
                         	  
                         	                             
                            </div>
                            
                               				
                        </div>
                        </form>
           
        </div>
        
       </div>
        
        
        
        
       

        
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
</body>

</html>