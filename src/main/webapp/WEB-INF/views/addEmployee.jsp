<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>

    <%@include file="components/header_imports.jsp" %>
    <script>var myContextPath = "${pageContext.request.contextPath}"</script>
     <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="resources/js/addEmployee.js"></script>
	
    <script>
        $(document).ready(function() {
            $('select').material_select();
            $(".eye-slash").hide();
            $(".eye").click(function() {
                $(".eye").hide();
                $(".eye-slash").show();
                $("#password").attr("type", "text");
            });
            $(".eye-slash").click(function() {
                $(".eye-slash").hide();
                $(".eye").show();
                $("#password").attr("type", "password");
            });
            $(".eye-slash1").hide();
            $(".eye1").click(function() {
                $(".eye1").hide();
                $(".eye-slash1").show();
                $("#Cnfrmpassword").attr("type", "text");
            });
            $(".eye-slash1").click(function() {
                $(".eye-slash1").hide();
                $(".eye1").show();
                $("#Cnfrmpassword").attr("type", "password");
            });
            
            $('#basicSalary').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('basicSalary').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }
           
        });
        
       
    </script>
    <style>
        .eye .eye-slash .eye1 .eye-slash1 {
            transform: translateY(-16%);
        }
      
      
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
		
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/saveEmployee" method="post" id="saveEmployeeForm">
            <input id="areaListIds" type="hidden" class="validate" name="areaIdLists">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Personal Details</h4>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">person</i>
                        <input id="name" type="text" class="validate" name="employeeName" required>
                        <label for="name" class="active"><span class="red-text">*</span>Name</label>

                    </div>

                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">stay_current_portrait</i>
                        <input id="mobileNo" type="tel" id="mobileNo" class="validate" name="mobileNumber" required minlength="10" maxlength="10">
                        <label for="mobileNo" class="active"><span class="red-text">*</span>Mobile No.</label>
                        
                    </div>

                   <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">mail</i>
                        <input id="emailId" type="email" class="validate" name="emailId">
                        <label for="emailId" data-error="wrong" data-success="right"  class="active">Email Id</label>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on</i>
                        <textarea id="textarea1" class="materialize-textarea" name="address" required></textarea>
                        <label for="textarea1"><span class="red-text">*</span>Address</label>
                    </div>
                </div>
               
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Work Details </h4>
                    </div>
                    <div class="row" style="margin-bottom:5px;">
                 <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="fa fa-money prefix" aria-hidden="true"></i>
                        <input id="basicSalary" type="text" class="validate" name="basicSalary" required>
                        <label for="basicSalary" class="active"><span class="red-text">*</span>Basic Salary</label>
                    </div>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">view_stream <span class="red-text">*</span></i>                        
                        <select id="departmentid" name="departmentId" required="" aria-required="true" >
                                 <option value="" selected>Department</option>
                                <c:if test="${not empty departmentList}">
							<c:forEach var="listValue" items="${departmentList}">
								<option value="<c:out value="${listValue.departmentId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    </div>
                     <%-- <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on <span class="red-text">*</span></i>
                     
                      
                        <select name="stateid" id="stateList" required="" aria-required="true">
                                 <option value="" selected>State</option>
                                 <c:if test="${not empty stateList}">
									<c:forEach var="listValue" items="${stateList}">
										<option value="<c:out value="${listValue.stateId}" />"><c:out
												value="${listValue.name}" /></option>
									</c:forEach>
								</c:if> 
                        </select>
                    </div> --%>
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on <span class="red-text">*</span></i>
                       
                        <select name="cityid" id="cityList" required="" aria-required="true">    
                                 <option  value="" selected>City</option>
                                 <c:if test="${not empty cityList}">
									<c:forEach var="listValue" items="${cityList}">
										<option value="<c:out value="${listValue.cityId}" />"><c:out
												value="${listValue.name}" /></option>
									</c:forEach>
								</c:if> 
                        </select>
                    </div>
                     <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">track_changes <span class="red-text">*</span></i>
                       
                        <select name="regionId" id="regionList" required="" aria-required="true">
                                 <option value="" selected>Region</option>
                        </select>
                    </div>
                     <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">business <span class="red-text">*</span></i>
                       
                        <select name="AreaId"  id="areaList" class="active" required="" aria-required="true">
                                <option value="" selected>Area</option>
                        </select>
                    </div>
                    <!--<div class="input-field col s12 m6 l1 push-l1 ">
                        <button class="btn waves-effect waves-light blue darken-8" type="button">Ok</button>
                    </div>-->
                   <div class="input-field col s12 m6 l6 offset-l3 offset-m3 center-align">
                        <table class="centered tblborder" id="areatable">
                            <thead>
                                <tr>
                                    <th>Sr.No</th>
                                    <th>Area </th>
                                    <th>Cancel</th>
                                </tr>
                            </thead>
                            <tbody id="t1">
                            </tbody>
                        </table>
                        <br><br>
                    </div>

                </div>

                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Login Credentials </h4>
                    </div>
                    <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1 ">
                        <i class="material-icons prefix">person</i>
                        <input id="userId" type="text" class="validate" name="userId" required>
                        <label for="userId" id="useridvalid" class="active" data-success=""><span class="red-text">*</span>User Id</label>
					 </div>	
					               
					             

                    <div class="input-field col col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1 ">

                        <i class="material-icons prefix">vpn_key</i>
                        <input id="password"  type="password" name="password" required>
                        <label for="password" class="active"><span class="red-text">*</span>Password</label>

                    </div>
                   <div class="input-field col s12 m1 l1 push-l1 push-m1 " style="transform:translate(0,10px)">
                        <i class="fa fa-eye  right eye" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash right eye-slash" aria-hidden="true"></i>
                    </div>

                   <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                        <i class="material-icons prefix">vpn_key</i>

                        <input id="Cnfrmpassword" type="password" name="confirmpass" required style="margin-bottom:5px;">
                        <label for="Cnfrmpassword" class="active"><span class="red-text">*</span>Confirm Password</label>
                        <span id="CnfrmpasswordLabel" class="red-text"></span>
                         
                    </div>
                     <div class="input-field col s12 m1 l1 push-l1 push-m1" style="transform:translate(0,10px)">
                        <i class="fa fa-eye   eye1 right" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash  eye-slash1 right" aria-hidden="true"></i>
                   
                    </div>
                  
                </div>
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue darken-8" type="submit" id="saveEmployeeSubmit">Add Employee<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>
 
        </div>


 <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
			<!-- <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->

    </main>
    <!--content end-->
</body>

</html>