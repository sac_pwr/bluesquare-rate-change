<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    
	    $(document).ready(function() {
	    	$('#showPaymentCounterStatus').hide();
	    	var msg="${saveMsg}";
	       	 if(msg!='' && msg!=undefined)
	       	 {
	       		 $('#addeditmsg').find("#modalType").addClass("success");
	 			$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	       	     $('#addeditmsg').modal('open');
	       	     /* $('#msgHead').text("HRM Message"); */
	       	     $('#msg').text(msg);
	       	 }
	    	
	    	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   	         "oLanguage": {
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0 },
	   	                      { 'width': '2%', 'targets': 1},
	   	                  	  { 'width': '5%', 'targets': 2},
	   	                	  { 'width': '5%', 'targets': 3},
	   	             		  { 'width': '3%', 'targets': 4},
	   	             		  { 'width': '3%', 'targets': 5},
	   	                      { 'width': '1%', 'targets': 6},
	   	                  	  { 'width': '3%', 'targets': 7},
	   	                	  { 'width': '5%', 'targets': 8},
	   	             		  { 'width': '8%', 'targets': 9},
	   	             		{ 'width': '1%', 'targets': 10},
	   	             		{ 'width': '3%', 'targets': 11},
	   	             		{ 'width': '3%', 'targets': 12}
	   	                	  
	   	                     
	   	                      ],
	   	         lengthMenu: [
	   	             [50, 75., 100, -1],
	   	             ['50 ', '75 ', '100 ', 'All']
	   	         ],
	   	         
	   	        
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [30,40,50,70,40,40,30,40,50,60,50] 
		                    		 } 
		                    		    })
	   	       
	   	                         doc.pageMargins = [5,20,10,5];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select")
	             .change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
           //$(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
           /*  $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            }); */
           
            $(".rangeSelect").click(function() {
           
                $(".showDates").show();
              //  $(".showQuantity").hide();
                $("#oneDateDiv").hide();
            });
            $(".pickdate").click(function(){
            	//$(".showQuantity").hide();
   	   		 $(".showDates").hide();
   	   		$("#oneDateDiv").show();
   	   	});
            $('#topProductId').change(function(){
            
            	window.location.href = '${pageContext.servletContext.contextPath}/counterOrderReport?range=TopProducts&topProductNo='+$('#topProductId').val();
            	
            });
           
            
          //hide column depend on login
            var table = $('#tblData').DataTable(); // note the capital D to get the API instance
            var column = table.columns('.toggle');
            console.log(column);
            if(${sessionScope.loginType=='GateKeeper'}){            
                column.visible(true);
            }else{
            	 column.visible(false);
            }
        });
	    
	    function fetchPaymentDetails(orderId){
					$.ajax({
						type:'GET',
						url: "${pageContext.servletContext.contextPath}/fetchPaymentCounterOrder?counterOrderId="+orderId,
						async: false,
						success : function(data)
						{
							$('#paymentDetail').empty();
							
							var totalAmountPaid=0;
							for(var i=0; i<data.length; i++)	
							{
								totalAmountPaid+=data[i].currentAmountPaid.toFixed(2)-data[i].currentAmountRefund.toFixed(2)
							}
							
							if(data[0].counterOrder.businessName=="" || data[0].counterOrder.businessName==undefined){
								$('#boothNo').html("<b>Customer Name : </b>"+data[0].counterOrder.customerName);
							}else{
								$('#boothNo').html("<b>Booth No. : </b>"+data[0].counterOrder.businessName.businessNameId);
							}
							
							$('#orderId').html("<b>Order Id. : </b>"+data[0].counterOrder.counterOrderId);
							$('#totalAmt').html("<b>Total Amount : </b>"+data[0].counterOrder.totalAmountWithTax);
							$('#balanceAmt').html("<b>Balance Amount : </b>"+(parseFloat(data[0].counterOrder.totalAmountWithTax)-parseFloat(totalAmountPaid)));
							
							var totalAmountPaid=0;
							var srno=1;
							$('#showPaymentCounterStatus').show();
							for(var i=0; i<data.length; i++)	
							{
								var mode="";
								if(data[i].payType==="Cash")
								{
									mode="Cash";
								}
								else
								{
									mode=data[i].bankName+"-"+data[i].chequeNumber;
								}
								
								var dateFrom=new Date(parseInt(data[i].paidDate));
								month = dateFrom.getMonth() + 1;
								day = dateFrom.getDate();
								year = dateFrom.getFullYear();
								var dateFromString = day + "-" + month + "-" + year;
								
								if(data[i].currentAmountRefund=='' || data[i].currentAmountRefund==undefined){
									$('#paymentDetail').append('<tr>'+
																'<td>'+srno+'</td>'+
																'<td>'+data[i].currentAmountPaid.toFixed(2)+'</td>'+
																'<td><font color="blue"><b>Paid</b></font></td>'+
																'<td>'+mode+'</td>'+
																'<td>'+data[i].employeeName+'</td>'+
																'<td>'+dateFromString+'</td></tr>');
									totalAmountPaid=parseFloat(totalAmountPaid)+parseFloat(data[i].currentAmountPaid.toFixed(2));
								}else{
									$('#paymentDetail').append('<tr>'+
											'<td>'+srno+'</td>'+
											'<td>'+data[i].currentAmountRefund.toFixed(2)+'</td>'+
											'<td><font color="green"><b>Refund</b></font></td>'+
											'<td>'+mode+'</td>'+
											'<td>'+data[i].employeeName+'</td>'+
											'<td>'+dateFromString+'</td></tr>');
									totalAmountPaid=parseFloat(totalAmountPaid)-parseFloat(data[i].currentAmountRefund.toFixed(2));
								}
								
								srno++;
							}
							$('#totalAmountPaid').text(totalAmountPaid);
							$('#viewPaymentDetail').modal('open');
						},
						error: function(xhr, status, error) 
						{
							Materialize.toast('Payment List Not Found!', '2000', 'teal lighten-2');
							/* $('#addeditmsg').modal('open');
			       	     	$('#msgHead').text("Message : ");
			       	     	$('#msg').text("Product List Not Found"); 
			       	     		setTimeout(function() 
								  {
				     					$('#addeditmsg').modal('close');
								  }, 1000); */
						}
						
					});
		}

    </script>
     <style>
     .card{
     height: 2.5rem;
     line-height:2.5rem;
    /*  text-align:center !important; */
     }
    .card-image{
        	width:35% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
#oneDateDiv{
	margin-top: 0.5rem;
	margin-left:4%
}
td .btn-flat{
	color:unset !important;
	font-size:0.8rem !important;
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main  class="paddingBody">
        <br>
        <div class="row">
           <div class="col s12 m3 l3 right right-align" style="margin-top:0.6%;width:15%;">
           
            <!-- <div class="col s6 m7 l7 right right-align" style="margin-top:3%;"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>                    
                     <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today">Today</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=viewAll">View All</a></li>
                    </ul>
              
               </div>
                 
               <div class="input-field col s12 l6 m6 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/counterOrderReport" method="post">
	                    <div class="input-field col s12 m2 l4">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
                <div class="input-field col s12 l6 m6 right" style="margin-top:0.5rem;">      
           		 <form action="${pageContext.servletContext.contextPath}/counterOrderReport" method="post">
                    <input type="hidden" name="range" value="range"> 
                    	  <span class="showDates">
                              <div class="input-field col s6 m2 l3">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s6 m2 l3">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s6 m2 l2">
                            <button type="submit">View</button>
                            </div>
                          </span>
                </form>                
         </div>
                 
        
        
          <%-- <div class="col s12 m5 l5 right">
       			 <h5 class="center red-text">Total Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTax}" /></h5>
		</div> --%>
<!-- </div>

        <div class="row"> -->
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>
                            <th class="print-col">Counter Order Id</th>
                            <th class="print-col">Gatekeeper name</th>
                            <th class="print-col">Shop Name/Customer Name</th>
                            <th class="print-col">Total Amt</th>
                            <th class="print-col">Total Amt (With Tax)</th>
                            <th class="print-col">Total Qty</th>
                            <th class="print-col">Order Status</th>
                            <th class="print-col">Estimated payment Date</th>
                            <th class="print-col">Payment Status</th>
                            <th class="print-col">Order Taken Date/Time</th>
                            <th >Get Bill</th>
                            <th class="toggle">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                    <c:if test="${not empty counterOrderReportList}">
						<c:forEach var="listValue" items="${counterOrderReportList}">
		                    <tr class="${listValue.orderStatus == 'Cancelled' ? 'red lighten-4' : ''}">
		                    	<td><c:out value="${listValue.srno}" /></td>
		                    	<td>
		                    		<a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/counterOrderProductDetailsListForWebApp?counterOrderId=${listValue.counterOrderId}"><c:out value="${listValue.counterOrderId}" /></a>
		                    	</td>
		                    	<td><c:out value="${listValue.gkName}" /></td>
		                    	<td><c:out value="${listValue.customerName}" /></td>
		                    	<td><c:out value="${listValue.totalAmount}" /></td>
		                    	<td><c:out value="${listValue.totalAmountWithTax}" /></td>
		                    	<td><c:out value="${listValue.totalQuantity}" /></td>
		                        <td><c:out value="${listValue.orderStatus}" /></td>
		                    	<td><c:out value="${listValue.paymentDate}" /></td>
		                    	<td>
		                    		<font color="${listValue.paymentStatus=='Paid'?'Green':''}
                            				   ${listValue.paymentStatus=='UnPaid'?'Red':''}
                            				   ${listValue.paymentStatus=='Partially Paid'?'Blue':''}">
	                            	<b style="cursor:${listValue.paymentStatus=='UnPaid'?'not-allowed':''}">
		                    			<c:choose>
		                    			<c:when test="${listValue.paymentStatus!='UnPaid'}">
		                    				<button class="btn-flat" onclick="fetchPaymentDetails('${listValue.counterOrderId}')"><c:out value="${listValue.paymentStatus}" /></button>
		                    			</c:when>
		                    			<c:otherwise>
		                    				<c:out value="${listValue.paymentStatus}" />
		                    			</c:otherwise>
		                    			</c:choose>
		                    		</b>
		                    		</font>
		                    	</td>
		                    	<td>
		                    	<c:out value="${listValue.orderTakenDate}" /><br>  <c:out value="${listValue.orderTakenTime}" />
		                    	</td>
		                    	<td><a class="btn-flat waves-effect waves-light tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Get Bill" href="${pageContext.request.contextPath}/counterOrderInvoice.pdf?counterOrderId=${listValue.counterOrderId}"><i class="material-icons">receipt</i></a></td>
		                    	<td>
		                    	<c:if test="${listValue.paymentStatus!='Paid' and listValue.orderStatus=='Delivered'}">
		                    		<a href="${pageContext.request.contextPath}/paymentCounterOrder?counterOrderId=${listValue.counterOrderId}" class="btn-flat"><i class="fa fa-inr tooltipped " data-position="right" data-delay="50" data-tooltip="Take Payment"></i></a>
		                    	</c:if>
		                    	<c:if test="${listValue.orderStatus=='Delivered'}">
		                    		<a href="${pageContext.request.contextPath}/openCounterForEdit?counterOrderId=${listValue.counterOrderId}" class="btn-flat"><i class="material-icons tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit order" >edit</i></a> 
		                    		<a href="#delete_${listValue.counterOrderId}" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a>
		                    	</c:if>
		                    	</td>
		                    	
		                    </tr>
		                    <c:if test="${listValue.orderStatus=='Delivered'}">
			                    <!-- Modal Structure for delete start-->
			                    <div id="delete_${listValue.counterOrderId}" class="modal deleteModal row">
			                        <div class="modal-content  col s12 m12 l12">
			                        	<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
			                            <h5 class="center-align">Do You Want to Delete?</h5>
			                            <br/>
			                        </div>
			                        <div class="modal-footer">            
			         				   <div class="col s6 m6 l3 offset-l3">
			                 				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
			                			</div>
			            				<div class="col s6 m6 l3">
			             				   <a href="${pageContext.request.contextPath}/deleteCounterOrder?counterOrderId=${listValue.counterOrderId}" class="modal-action modal-close waves-effect  btn">Delete</a>
			               				 </div>                
			            			</div>
			                    </div>
			                    <!-- Modal Structure for delete end-->
		                    </c:if>
		                 </c:forEach>
		             </c:if>
                     	

                    
                    </tbody>
                </table>
            </div>
        </div>
		
		<!-- Payment list view model -->
		<div id="viewPaymentDetail" class="modal modal-fixed-footer">
            <div class="modal-content">
                <h5 class="center"><u>Payment Detail</u></h5>
                <hr>

                <div class="row">
                    <div class="col s12 l6 m6">
                        <h6 id="boothNo" class="black-text">Booth No:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="orderId" class="black-text">Order Id:</h6>
                    </div>
                
                
                    <div class="col s12 l6 m6">
                        <h6 id="totalAmt" class="black-text">Total Amount:</h6>
                    </div>
                    <div class="col s12 l6 m6">
                        <h6 id="balanceAmt" class="black-text">Balance Amount : </h6>
                    </div> 
                </div>
               
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Amount</th>
                            <th class="showPaymentCounterStatus">Status</th>
                            <th>Mode</th>
                            <th>Collecting Person</th>
                            <th> Date</th>
                        </tr>
                    </thead>
                    <tbody id="paymentDetail">
                       
                    </tbody>
                    <tfoot>
                    <tr>
                    <th>Total</th>
                     <th><span id="totalAmountPaid"></span></th>
                     <th class="showPaymentCounterStatus"></th>
                     <th></th>
                     <th></th>
                     <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="modal-footer row">
             <div class="col s6 m6 l3 offset-l4">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                 </div>	
            </div>
        </div>
		<!-- payment list view model end -->
		
    </main>
    <!--content end-->
</body>

</html>