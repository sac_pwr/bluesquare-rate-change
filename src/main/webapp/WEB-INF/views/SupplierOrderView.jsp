<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <!-- <style>
        nav {
            /*height: 100px;*/
            background-color: red;
            background-image: linear-gradient( to right, #3498ce, #311b92);
        }
    </style> -->
    <script type="text/javascript">
    var showSupplierNavigation=${sessionScope.loginType=='CompanyAdmin'};
    $(document).ready(function(){
        $(".showDates").hide();
        $("#oneDateDiv").hide();
       
        $(".rangeSelect").click(function() {
        	$("#oneDateDiv").hide();	
            $(".showQuantity").hide();
        });
       
    	$(".pickdate").click(function(){
    		 $(".showDates").hide();
    		$("#oneDateDiv").show();
    	});
    });
    function showProductDetails(supplierOrderId)
    {
    	
    	$.ajax({
			url : "${pageContext.request.contextPath}/fetchSupplierOrderDetails?supplierOrderId="+supplierOrderId,
			dataType : "json",
			success : function(data) {
				supplierOrderDetailsLists=data;
				var srno=1;
				$("#supplierOrderDetailsData").empty();
				var srno=0;
				var totalAll=0,totalAmtWithTaxAll=0;
				for(var i=0; i<supplierOrderDetailsLists.length; i++)
				{					
					supplierOrderDetails=supplierOrderDetailsLists[i];
					srno++;
					
					if(showSupplierNavigation){
						supplierName="<td><a href='${pageContext.request.contextPath}/fetchSupplierListBySupplierId?supplierId="+supplierOrderDetails.supplier.supplierId+"'>"+supplierOrderDetails.supplier.name+"</a></td>";
					}else{
						supplierName="<td>"+supplierOrderDetails.supplier.name+"</td>";
					}
					
					var mrp=((parseFloat(supplierOrderDetails.supplierRate))+((parseFloat(supplierOrderDetails.supplierRate)*parseFloat(supplierOrderDetails.product.categories.igst))/100)).toFixed(0);
					var correctAmoutWithTaxObj=calculateProperTax(mrp,parseFloat(supplierOrderDetails.product.categories.igst));
					
					var total=parseFloat(supplierOrderDetails.totalAmount).toFixed(2);
					var totalAmtWithTax=parseFloat(supplierOrderDetails.totalAmountWithTax).toFixed(2);
					
					$('#supplierOrderDetailsData').append("<tr><td>"+srno+"</td>"+
							                            supplierName+
							                            "<td>"+supplierOrderDetails.product.productName+"</td>"+
							                            "<td>"+supplierOrderDetails.product.brand.name+"</td>"+
							                            "<td>"+supplierOrderDetails.product.categories.categoryName+"</td>"+
							                            "<td>"+supplierOrderDetails.quantity+"</td>"+
							                            "<td>"+parseFloat(correctAmoutWithTaxObj.mrp).toFixed(2)+"</td>"+
							                            "<td>"+parseFloat(supplierOrderDetails.totalAmount).toFixed(2)+"</td>"+
							                            "<td>"+parseFloat(supplierOrderDetails.totalAmountWithTax).toFixed(2)+"</td><tr>");
					totalAmtWithTaxAll=parseFloat(totalAmtWithTaxAll)+parseFloat(totalAmtWithTax);
					totalAll=parseFloat(totalAll)+parseFloat(total);
				}
				$("#supplierOrderDetailsData").append("<tr>"+	     
													   "<td colspan='7'><b>All Total</b></td>"+
								                        "<td  class='red-text'><b>"+totalAll.toFixed(2)+"</b></td>"+
								                        "<td  class='red-text'><b>"+totalAmtWithTaxAll.toFixed(2)+"</b></td>"+
								                    "</tr>");
				$('.modal').modal();
				$('#product').modal('open');
			}
		});
    	
    }
    </script>
    
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
        	
        	<div class="col s12 m4 l4 right right-align" style="margin-top:1%;">
                <!-- <div class="col s6 m4 l4 right"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=today&suppplierId=${supplierId}">Today</a></li>
                    	<li><a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=yesterday&suppplierId=${supplierId}">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=last7days&suppplierId=${supplierId}">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=currentMonth&suppplierId=${supplierId}">Current Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=lastMonth&suppplierId=${supplierId}">Last Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=last3Months&suppplierId=${supplierId}">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/showSupplierOrderReport?range=viewAll&supplierId=${supplierId}">View All</a></li>
                    </ul>
                </div>
                <div class="col s12 l3 m3 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/showSupplierOrderReport" method="post">
	                         <input type="hidden" name="range" value="pickDate">
                   			 <input type="hidden" name="supplierId" value="${supplierId}">
	                    <div class="input-field col s12 m7 l7">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                       
                        </div>
                    </form>
               </div>
               <div class="col s12 l4 m4 right"> 
                 <form action="${pageContext.request.contextPath}/showSupplierOrderReport" method="post">
                    <input type="hidden" name="range" value="range">
                    <input type="hidden" name="supplierId" value="${supplierId}">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                                <div class="input-field col s6 m5 l5 right">
	                                <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required> 
	                                <label for="endDate">To</label>
                                </div>
                              <div class="input-field col s6 m5 l5 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
                                     <label for="startDate">From</label>
                               </div>                             
                          </span>
                </form>
                 </div>
        
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        	<th>Sr.No.</th>
                            <th class="print-col">Supplier Order Id</th>
                            <th class="print-col">Product Details</th>                            
                            <th class="print-col">Total Quantity</th>
                            <th class="print-col">Taxable Amount</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Order Date</th>
                            <th class="print-col">Order Updated Date</th>
                            
                        </tr>
                    </thead>

                    <tbody>
                    	<% int rowincrement=0; %>
 						<c:if test="${not empty supplierOrderLists}">
						<c:forEach var="listValue" items="${supplierOrderLists}"> 
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>                  
                        <tr ${listValue.status == false ? 'class="red lighten-4"' : ''}>
                        	<td><c:out value="${rowincrement}" /></td>
                            <td><c:out value="${listValue.supplierOrderId}" /></td>
                            <td>
                                <button class="btn" onclick="showProductDetails('${listValue.supplierOrderId}')">View</button>
                            </td>
                            <td><c:out value="${listValue.totalQuantity}" /></td>
                            <td><c:out value="${listValue.totalAmount}" /></td>
                            <td><c:out value="${listValue.totalAmountWithTax}" /></td>
                            <td>
                            	<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.supplierOrderDatetime}" /><c:out value="${dt}" />
			                        	<br/>
			                    <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.supplierOrderDatetime}" /><c:out value="${time}" />
                            </td>
                            <td>
                            	<c:choose>
	                        	<c:when test="${empty  listValue.supplierOrderUpdateDatetime}">
	                        		NA
		                        </c:when>
		                        <c:otherwise>
			                        <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.supplierOrderUpdateDatetime}" /><c:out value="${dt}" />
			                        	<br/>
			                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.supplierOrderUpdateDatetime}" /><c:out value="${time}" />
		                        </c:otherwise>
		                        </c:choose>  
                            </td>
                        </tr>
						</c:forEach>
						</c:if>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal Structure for View Product Details -->
        <div id="product" class="modal ">
            <div class="modal-content">
                <h5 class="center"><u>Product Details</u></h5>
              
                <br>
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No.</th>
                            <th>Supplier Name</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>MRP</th>
                            <th>Taxable Amount</th>
                            <th>Total Amount</th>
                        </tr>
                    </thead>
                    <tbody id="supplierOrderDetailsData"></tbody>
                </table>
            </div>
            <div class="modal-footer row">

                <div class="col s12 m6 l6 offset-l1">
                    <a href="#!" class="modal-action modal-close waves-effect btn">Close</a>
                </div>

            </div>
        </div>




    </main>
    <!--content end-->
</body>

</html>