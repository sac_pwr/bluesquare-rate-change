<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
     <style>
      .card-panel p{
        	font-size:15px !important;
        }
     </style>

</head>

<body>
     <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%;">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>
                            <th class="print-col">Product</th>
                            <th class="print-col">Category</th>
                            <th class="print-col">Brand</th>
                            <th class="print-col">Issued Quantity</th>
                            <th class="print-col">Return Quantity</th>
                            <th class="print-col">ReIssued Quantity</th>
                            <th class="print-col">Selling Rate</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Total Tax</th>
                            <th class="print-col">Total Amount With Tax</th>
                        </tr>
                    </thead>

                    <tbody>
                    
                        <% int rowincrement=0; %>
                        <c:if test="${not empty reIssueOrderProductDetailsList}">
						<c:forEach var="listValue" items="${reIssueOrderProductDetailsList}">
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
						<tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><c:out value="${listValue.product.productName}" /></td>
                            <td><c:out value="${listValue.product.categories.categoryName}" /></td>
                            <td><c:out value="${listValue.product.brand.name}" /></td>
                            <td><c:out value="${listValue.issuedQuantity}" /></td>
                            <td><c:out value="${listValue.returnQuantity}" /></td>
                            <td><c:out value="${listValue.reIssueQuantity}" /></td>
                            <td><c:out value="${listValue.sellingRate}" /></td>
                            <td><c:out value="${listValue.sellingRate*listValue.reIssueQuantity}" /></td>
                            <td><c:out value="${listValue.reIssueAmountWithTax-(listValue.sellingRate*listValue.reIssueQuantity)}" /></td>
                            <td><c:out value="${listValue.reIssueAmountWithTax}" /></td>
                        </tr>	                       
						</c:forEach>
						</c:if>
                       
                    </tbody>
                </table>
            </div>
        </div>

    <!--content end-->
</body>

</html>