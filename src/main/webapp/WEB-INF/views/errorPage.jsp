<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>

<head>
    <title>Blue Square</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="shortcut icon" href="resources/img/bs_icon2-01.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Compiled and minified JavaScript -->
     <link rel="stylesheet"  href="resources/css/materialize.min.css">
    <link rel="stylesheet" href="resources/css/font-awesome.min.css">
     <script	src="resources/js/jquery.min.js"></script>
<script src="resources/js/materialize.min.js"></script>
     <link href="resources/css/style.css" rel="stylesheet">
    <script type="text/javascript" src="resources/js/script.js"></script>
       <link href="resources/css/materialicon.css"	rel="stylesheet">
    
    
    
    
    
    
    <script type="text/javascript">
      
    </script>
    <style>
        button,
        body {
            /*background-color: red;*/
            /* background-image: linear-gradient( to right, #311b92, #3498ce); */
          
            background-image: linear-gradient(to right, #303641, grey);
           /*  background-image: linear-gradient(45deg, #8baaaa 0%, #ae8b9c 100%); */
        }
        .loading {
         
            position: fixed;
            top: 0%;
            left: 0;
            width: 100%;
            height: 100%;
            background:url('resources/img/error.jpg') #303641 no-repeat;
            background-size:cover;
            z-index: 9999;
            
        }
        .loading p{
            position: relative;
             top: 0;
             font-size:400% !important;
        }
        .loading-text {
            position: relative;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            text-align: center;
            width: 100%;
            height: 100px;
            line-height: 30px;
        }
        
        .loading-text span {
            display: inline-block;
            margin: 0 5px;
            color: #fff;
            font-family: 'Quattrocento Sans', sans-serif;
            filter: blur(0px);
            animation: blur-text 2s infinite alternate;
             will-change: transform;
        }
        /* .loading-text span:nth-child(1){
        	animation-delay:0.2s;
        	-moz-animation-delay: 0.2s;
    -webkit-animation-delay: 0.2s;
        }
        .loading-text span:nth-child(2){
        	animation-delay:0.4s;
        	-moz-animation-delay: 0.4s;
    -webkit-animation-delay: 0.4s;
        }
        .loading-text span:nth-child(3){
        	animation-delay:0.6s;
        	-moz-animation-delay: 0.6s;
    -webkit-animation-delay: 0.6s;
        }
        .loading-text span:nth-child(4){
        	animation-delay:0.8s;
        	-moz-animation-delay: 0.9s;
    -webkit-animation-delay: 0.9s;
        }
        .loading-text span:nth-child(5){
        	animation-delay:1.0s;
        	-moz-animation-delay: 1.0s;
    -webkit-animation-delay: 1.0s;
        }
        .loading-text span:nth-child(6){
        	animation-delay:1.2s;
        	-moz-animation-delay: 1.2s;
    -webkit-animation-delay: 1.2s;
        }
        .loading-text span:nth-child(7){
        	animation-delay:1.4s;
        	-moz-animation-delay: 1.4s;
    -webkit-animation-delay: 1.4s;
        }
        .loading-text span:nth-child(8){
        	animation-delay:1.6s;
        	-moz-animation-delay: 1.6s;
    -webkit-animation-delay: 1.6s;
        }
        .loading-text span:nth-child(9){
        	animation-delay:1.8s;
        	-moz-animation-delay: 1.8s;
    -webkit-animation-delay: 1.8s;
        }
        .loading-text span:nth-child(11){
        	animation-delay:2.0s;
        	-moz-animation-delay: 2.0s;
    -webkit-animation-delay: 2.0s;
        	
        }
        .loading-text span:nth-child(12){
        	animation-delay:2.2s;
        	-moz-animation-delay: 2.2s;
    -webkit-animation-delay: 2.2s;
        }
        .loading-text span:nth-child(13){
        	animation-delay:2.4s;
        	-moz-animation-delay: 2.4s;
    -webkit-animation-delay: 2.4s;
        }
        .loading-text span:nth-child(14){
        	animation-delay:2.6s;
        	-moz-animation-delay: 2.6s;
    -webkit-animation-delay: 2.6s;
        }
        .loading-text span:nth-child(16){
        	animation-delay:2.8s;
        	-moz-animation-delay: 2.8s;
    -webkit-animation-delay: 2.8s;
        }
         .loading-text span:nth-child(17){
        	animation-delay:3.0s;
        	-moz-animation-delay: 3.0s;
    -webkit-animation-delay: 3.0s;
        }
         .loading-text span:nth-child(18){
        	animation-delay:3.2s;
        	-moz-animation-delay: 3.2s;
    -webkit-animation-delay: 3.2s;
        }
         .loading-text span:nth-child(19){
        	animation-delay:3.4s;
        	-moz-animation-delay: 3.4s;
    -webkit-animation-delay: 3.4s;
        }
         .loading-text span:nth-child(20){
        	animation-delay:3.6s;
        	-moz-animation-delay: 3.6s;
    -webkit-animation-delay: 3.6s;
        } */
        
        a{
        	 position: fixed;
        	 bottom:10%;
        	 right:30%;
        	 
        }
        @keyframes blur-text {
            0% {
                filter: blur(0px);
            }
            100% {
                filter: blur(4px);
            }
        }  
    </style>

    <body>

        <div class="container">
  	
          <div class="loading">
         <p class="white-text center">${errorCode}</p>
	 	<span class="justify white-text" style="display:none">${error}</span>
	<div class="loading-text">
	      
		<span class="loading-text-words">S</span>
		<span class="loading-text-words">O</span>
		<span class="loading-text-words">M</span>
		<span class="loading-text-words">E</span>
		<span class="loading-text-words">T</span>
		<span class="loading-text-words">H</span>
		<span class="loading-text-words">I</span>
    <span class="loading-text-words">N</span>
    <span class="loading-text-words">G</span>
     <span class="loading-text-words">&nbsp;</span>
    <span class="loading-text-words">W</span>
    <span class="loading-text-words">E</span>
    <span class="loading-text-words">N</span>
    <span class="loading-text-words">T</span>
     <span class="loading-text-words">&nbsp;</span>
    <span class="loading-text-words">W</span>
    <span class="loading-text-words">R</span>
    <span class="loading-text-words">O</span>
    <span class="loading-text-words">N</span>
    <span class="loading-text-words">G</span>
   
	</div>
	<a class="btn-flat white-text" href="${pageContext.request.contextPath}/">Go to Home Page<i class="right material-icons">trending_flat</i></a>
</div>  

</div>
                <%-- <div class="row">
                    <div class="col s12 m12 l8 offset-l2 z-depth-5 card-panel" style="border-radius:20px;opacity:0.8;">
                        <div class="black-text">
                          <!--   <div class="col s12 m12 l12">
                            <h2 class="card-title center teal-text" style="font-family: Serif;">Error:404</h2>
                        </div>--> 
                            <div class="col l6 m6">
                                <div class="col s6 m7 l12">
                                    <img src="resources/img/oops3.jpg" class="responsive-img" style="margin-top:9%;">
                                   
                                </div>
                            </div>
                            <div class="col l6 m6">
                                <br>
                                 <!--<h2 class="center">404</h2>-->
                                 <div class="col s12 m12 l12">
                                    <h6>${url}</h6>
									<h2 class="card-title center teal-text" style="font-family: Serif;">Error : </h2>
									<span class="justify">${error}</span>
<br><br><!--${error}  --></div>
                                <a class="btn waves-effect waves-light blue darken-8 " href="${pageContext.request.contextPath}/">
								Go to home page&nbsp; <i class="material-icons right">send</i>
							</a>
<br><br>
                                </div>
                                
                            </div>
                            
                        </div>
                    </div> --%>
              
           
       
    </body>

</html>