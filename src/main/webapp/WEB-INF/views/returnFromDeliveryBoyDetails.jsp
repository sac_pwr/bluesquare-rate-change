<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
        <%@include file="components/header_imports.jsp" %>

	 
     <script type="text/javascript">
	     var myContextPath = "${pageContext.request.contextPath}";
	     var orderId="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.returnFromDeliveryBoyMainId}";     
     </script>
    <!-- check it for its return from delivery boy accept or not 
    	if not received then its show content of if block
     --> 
    <c:if test="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.receivedStatus==false}">	
		<script type="text/javascript" src="resources/js/returnfromDBMng.js"></script>
	</c:if>
	
    <script type="text/javascript">
    
    $(document).ready(function(){
    	
  		 $("select")
            .change(function() {
                var t = this;
                var content = $(this).siblings('ul').detach();
                setTimeout(function() {
                    $(t).parent().append(content);
                    $("select").material_select();
                }, 200);
            });
        $('select').material_select();
        $('.dataTables_filter input').attr("placeholder", "Search");
       /*  $('#damageQty').keypress(function( event ){
    	    var key = event.which;
    	    
    	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
    	        event.preventDefault();
    	}); 
        $('#recievingQty').keypress(function( event ){
    	    var key = event.which;
    	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
    	        event.preventDefault();
    	}); 
        $("#damageQty").on('keyup blur',function(){
        	
        	$("#damageQtySpan").text(this.val());
        });
        $("#recievingQty").on('keyup blur',function(){
        	$("#recievingQtySpan").text(this.val());
        }); */
        });
    	
    </script>
    
    <style>
        tfoot th,
        tfoot td,
        input[type='text']{
        	text-align:center;
        }
        .leftHeader{
    	width:105px !important;
    	display:inline-block;
    }
        .card-panel p {
    margin: 2px	!important;
    margin-top:3% !important;
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
       <div class="row">
       <br/>
       	<div class="col s12 l12 m12 card-panel formBg hoverable">
      	
       			
                <div class="col s12 l4 m4">
                <p>
                 <span class="leftHeader">Order Id:</span>
                 <b><c:out value="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.orderDetails.orderId}" /></b>
               		
                </p>
                </div>
                     <div class="col s12 l4 m4">
                       <p><span class="leftHeader">Date of Order:</span> 
                       <b>
                       <c:out value="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.orderDetails.orderDetailsAddedDatetime}" />
                       </b>
                       </p>
               		 
                </div>
                <div class="col s12 l4 m4">
                   <p><span class="leftHeader">Shop Name: </span>
                   <b>
                   <c:out value="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.orderDetails.businessName.shopName}" />
                   </b>
                   </p>
               		
                </div>
                 <div class="col s12 l4 m4">
                   <p><span class="leftHeader">Salesman: </span>
                   <b>
                   <c:out value="${smName}" />
                   </b></p>
               		  
                </div>
                <div class="col s12 l4 m4">
                    <p><span class="leftHeader">Mobile No: </span>
                    <b>
                    <c:out value="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.orderDetails.businessName.contact.mobileNumber}" />
                    </b></p>
               		
                </div>
                
                <div class="col s12 l4 m4">
                   <p id="area"><span class="leftHeader">Area:</span>
                   <b>
                   <c:out value="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.orderDetails.businessName.area.name}" />
                   </b></p>
               		
                </div>
           </div> 
       </div>
       
        <div class="row">
         
            <div class="col s12 l12 m12" style="padding:0">
                <table class="striped highlight tblborder centered" id="tblData1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>                         
                            <th class="print-col">Product Name</th>                            
                            <th class="print-col">Issued Qty</th>
                            <th class="print-col">Delivered Qty</th>
                            <th class="print-col">Damage Qty</th>
                            <th class="print-col">Damage Qty Reason</th>
                            <th class="print-col">Recieving Qty</th>
                        </tr>
                    </thead>

                    <tbody>
                     <% int rowincrement=0; %>
                     <c:if test="${not empty returnFromDeliveryBoyList}">
					 <c:forEach var="listValue" items="${returnFromDeliveryBoyList}">
					 <c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>                            
                            <td><c:out value="${listValue.product.productName}" /><font color="green">${listValue.type=='Free'?'-(Free)':''}</font></td>
                            <td>
                            	<c:out value="${listValue.issuedQuantity}" />
                            	<c:set var="totalIssuedQuantity" value="${totalIssuedQuantity + listValue.issuedQuantity}" scope="page"/>
							</td>
                            <td>
                            	<c:out value="${listValue.deliveryQuantity}" />
                            	<c:set var="totalDeliveredQuantity" value="${totalDeliveredQuantity + listValue.deliveryQuantity}" scope="page"/>
                            </td>
                            
                            <!-- check it for its return from delivery boy accept or not 
						    	if not received then its show content of if block
						     -->  
                            <c:choose>                           
                            <c:when test="${listValue.returnFromDeliveryBoyMain.receivedStatus==false}">
	                       		<c:choose>
	                       			<c:when test="${listValue.type=='Free'}">
	                       				<td><input id="damageQtyFree_${listValue.product.product.productId}"  type="text" value="0"></td>
	                       				<td>
	                       					<input type="text" id="damageQtyFreeReason_${listValue.product.product.productId}"/>
	                       						
	                       				</td>
	                       				<td>
			                            	<span id="recievingQtyFree_${listValue.product.product.productId}"><c:out value="${listValue.issuedQuantity-listValue.deliveryQuantity}" /></span>
			                            	<c:set var="totalReceivedQuantity" value="${totalReceivedQuantity + (listValue.issuedQuantity-listValue.deliveryQuantity)}" scope="page"/>
			                            </td>
	                       			</c:when>
	                       			<c:otherwise>
	                       				<td><input id="damageQtyNonFree_${listValue.product.product.productId}"  type="text" value="0"></td>
	                       				<td>
	                       				  <input type="text" id="damageQtyNonFreeReason_${listValue.product.product.productId}"/>
	                       				 
	                       				</td>
	                       				<td>
			                            	<span id="recievingQtyNonFree_${listValue.product.product.productId}"><c:out value="${listValue.issuedQuantity-listValue.deliveryQuantity}" /></span>
			                            	<c:set var="totalReceivedQuantity" value="${totalReceivedQuantity + (listValue.issuedQuantity-listValue.deliveryQuantity)}" scope="page"/>
			                            </td>
	                       			</c:otherwise>
	                       		</c:choose>
	                    	</c:when>   
	                    	<c:otherwise>
	                    		<td>
	                            	<c:out value="${listValue.damageQuantity}" />
	                            	<c:set var="totalDamageQuantity" value="${totalDamageQuantity + listValue.damageQuantity}" scope="page"/>
								</td>
								<td><c:out value="${listValue.damageQuantityReason}" /></td>
	                            <td>
	                            	<c:out value="${listValue.nonDamageQuantity}" />
	                            	<c:set var="totalNonDamageQuantity" value="${totalNonDamageQuantity + listValue.nonDamageQuantity}" scope="page"/>
	                            </td>
	                    	</c:otherwise>
	                    	</c:choose>    
                            
                        </tr>
					</c:forEach>
					</c:if>
                    </tbody>
                     <tfoot class="centered">
	            		<tr>
	             			  <th colspan="2">Total</th>
	                		  <th><c:out value="${totalIssuedQuantity}" /></th>
	                		  <th><c:out value="${totalDeliveredQuantity}" /></th>
	                		  <!-- check it for its return from delivery boy accept or not 
						    	if not received then its show content of if block
						     -->
	                		  <c:choose>         
                              <c:when test="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.receivedStatus==false}">
		                		  <th><span id="totalDamagedQuantity">0</span></th>
		                		  <th></th>
		                		  <th><span id="totalReceivedQuantity"><c:out value="${totalReceivedQuantity}" /></span></th>
	                		  </c:when>
	                		  <c:otherwise>
	                		  	  <th><c:out value="${totalDamageQuantity}" /></th>
	                		  	  <th></th>
		                		  <th><c:out value="${totalNonDamageQuantity}" /></th>
	                		  </c:otherwise>
	                		  </c:choose>
	           			</tr>
	           			<tr> 
                            <!-- check it for its return from delivery boy accept or not 
						    	if not received then its show content of if block
						     -->
	           				 <c:if test="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.receivedStatus==false}">
	             			  <th colspan="7">Received above mentioned products? <a href="#confirmation" class="modal-trigger btn white-text">Yes</a></th>
	             			 </c:if>
	           			</tr>
        			</tfoot>
                </table> 
            </div>
        </div>
<!-- check it for its return from delivery boy accept or not 
if not received then its show content of if block
-->
<c:if test="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.receivedStatus==false}">
        <!-- Modal Structure for View Product Details -->
        <div id="confirmation" class="modal deleteModal row">
            <div class="modal-content">
             <h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
      		  <h5>You have checked and received the retuned products?</h5>            
            </div>
	            <div class="modal-footer">
	              <div class="col s6 m6 l3 offset-l3">
	        				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
	       		  </div>
	  				<div class="col s6 m6 l2">
	   				   <form action="${pageContext.request.contextPath}/updateReturnFromDeliveryBoy" method="post">
							<input id="updateOrderProductListId" type="hidden" name="returnFromDeliveryboyListUpdated"/>
							<input id="orderId" type="hidden" name="returnFromDeliveryBoyMainId" value="${returnFromDeliveryBoyList.get(0).returnFromDeliveryBoyMain.returnFromDeliveryBoyMainId}"/>
							<button id="receivingDone" class="modal-action modal-close waves-effect btn">Yes</button>
					   </form>
	     			</div>       
	   			</div>
            
        </div>
</c:if>
    </main>
    <!--content end-->
</body>

</html>