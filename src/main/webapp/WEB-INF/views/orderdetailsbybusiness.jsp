<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
    var myContextPath = "${pageContext.request.contextPath}";
    var employeeDetailsId="${employeeDetailsId}";
    $(document).ready(function() {
    	 var table = $('#tblData').DataTable();
 		table.destroy();
 		$('#tblData').DataTable({
 	         "oLanguage": {
 	             "sLengthMenu": "Show:  _MENU_",
 	             "sSearch": "_INPUT_" //search
 	         },
 	         "autoWidth": false,
 	        
 	         lengthMenu: [
 	             [10, 25., 50, -1],
 	             ['10 ', '25 ', '50 ', 'All']
 	         ],
 	         
 	         dom: 'lBfrtip',
 	         buttons: {
 	             buttons: [
 	                 //      {
 	                 //      extend: 'pageLength',
 	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
 	                 //  }, 
 	                 {
 	                     extend: 'pdf',
 	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                     customize: function(doc, config) {
 	                    	  doc.content.forEach(function(item) {
 	                    		  if (item.table) {
 	                    		 /*  item.table.widths = [40,'*','*','*']  */
 	                    		 } 
 	                    		    })
 	                         var tableNode;
 	                         for (i = 0; i < doc.content.length; ++i) {
 	                           if(doc.content[i].table !== undefined){
 	                             tableNode = doc.content[i];
 	                             break;
 	                           }
 	                         }
 	        
 	                         var rowIndex = 0;
 	                        /*  var tableColumnCount = tableNode.table.body[rowIndex].length;
 	                          
 	                         if(tableColumnCount > 6){
 	                           doc.pageOrientation = 'landscape';
 	                         } */
 	                         /*for customize the pdf content*/
 	                          doc.content[1].table.alignment = 'center';
 	                         doc.pageMargins = [5,20,10,5];
 	                         
 	                         doc.defaultStyle.fontSize = 8;
 	                         doc.styles.title.fontSize = 12;
 	                         doc.styles.tableHeader.fontSize = 11;
 	                         doc.styles.tableFooter.fontSize = 11;
 	                         doc.styles.tableHeader.alignment = 'center';
 	                         doc.styles.tableBodyEven.alignment = 'center';
 	                         doc.styles.tableBodyOdd.alignment = 'center';
 	                        
 	                       },
 	                      
 	               
 	                 },
 	                 {
 	                     extend: 'excel',
 	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                 },
 	                 {
 	                     extend: 'print',
 	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
 	                     //title of the page
 	                     title: function() {
 	                         var name = $(".heading").text();
 	                         return name
 	                     },
 	                     //file name 
 	                     filename: function() {
 	                         var d = new Date();
 	                         var date = d.getDate();
 	                         var month = d.getMonth();
 	                         var year = d.getFullYear();
 	                         var name = $(".heading").text();
 	                         return name + date + '-' + month + '-' + year;
 	                     },
 	                     //  exports only dataColumn
 	                     exportOptions: {
 	                         columns: ':visible.print-col'
 	                     },
 	                 },
 	                 {
 	                     extend: 'colvis',
 	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
 	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
 	                     collectionLayout: 'fixed two-column',
 	                     align: 'left'
 	                 },
 	             ]
 	         }

 	     });
 		$('select').material_select();
 	     $("select")
 	         .change(function() {
 	             var t = this;
 	             var content = $(this).siblings('ul').detach();
 	             setTimeout(function() {
 	                 $(t).parent().append(content);
 	                 $("select").material_select();
 	             }, 200);
 	         });
 	     $('.dataTables_filter input').attr("placeholder", "  Search");
    	$(".showDates").hide();
        $(".rangeSelect").click(function() {
       	 $("#oneDateDiv").hide();
            $(".showDates").show();
        });
   	$("#oneDateDiv").hide();
   	$(".pickdate").click(function(){
   		 $(".showDates").hide();
   		$("#oneDateDiv").show();
   	});
     });	
    	
    </script>
    
    <style>
       .card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
    tfoot th,
    tfoot td    {
    	text-align:center;
    }
    .leftHeader{
    	width:110px !important;
    	display:inline-block;
    }
    .dataTables_wrapper th {
    padding: 5px 10px !important;
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
        <div class="col s12 l12 m12 card-panel hoverable blue-grey lighten-4">
        <div class="col s12 l7 m7">
       
       			<div class="col s12 l6 m6">
                <p>
                 <span class="leftHeader">Shop Name :</span>
                 <b id="shopName"><c:out value="${orderDetailsListOfBusiness.businessName.shopName}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                
                </div>
                <div class="col s12 l6 m6">
                <fmt:formatNumber var="totalBusinessAmount" value="${orderDetailsListOfBusiness.totalBusinessAmount}" maxFractionDigits="2" minFractionDigits="2" />
                <p>
                 <span class="leftHeader">Total Business:</span>
                 <b id="totalBusiness"><c:out value="${totalBusinessAmount}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                
                </div>
                <div class="col s12 l6 m6">
                
                <p>
                 <span class="leftHeader">Account No :</span>
                 <b id="accNumber"><c:out value="${orderDetailsListOfBusiness.businessName.businessNameId}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                
                </div>
                <div class="col s12 l6 m6">
                <fmt:formatNumber var="amountPaid" value="${orderDetailsListOfBusiness.amountPaid}" maxFractionDigits="2" minFractionDigits="2" />
                <p>
                 <span class="leftHeader">Amount Paid :</span>
                 <b id="amtPaid"><c:out value="${amountPaid}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                
                </div>
               <div class="col s12 l6 m6">
                
                <p>
                 <span class="leftHeader">Mobile No :</span>
                 <b id="empdetailmobile"><c:out value="${orderDetailsListOfBusiness.businessName.contact.mobileNumber}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                
                </div>
                
                <div class="col s12 l6 m6">
                <fmt:formatNumber var="amountUnPaid" value="${orderDetailsListOfBusiness.amountUnPaid}" maxFractionDigits="2" minFractionDigits="2" />
                <p>
                 <span class="leftHeader">Balance Amt:</span>
                 <b id="empdetailmobile"><c:out value="${amountUnPaid}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                
                </div>
                <div class="col s12 l12 m12">
                
                <p>
                 <span class="leftHeader">Area :</span>
                 <b id="empdetailareas"><c:out value="${orderDetailsListOfBusiness.businessName.area.name}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                
                </div>
                
                
                
                
            </div>
           
        	<div class="col s12 m2 l2 right right-align" style="margin-top:0.5%;">               
                    <!-- Dropdown Trigger &-->
                    <a class='dropdown-button btn waves-effect' href='#' data-activates='filter'>Action<i
       				 class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                         <li><a href="${pageContext.servletContext.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId?businessNameId=${orderDetailsListOfBusiness.businessName.businessNameId}&range=today">Today</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId?businessNameId=${orderDetailsListOfBusiness.businessName.businessNameId}&range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId?businessNameId=${orderDetailsListOfBusiness.businessName.businessNameId}&range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId?businessNameId=${orderDetailsListOfBusiness.businessName.businessNameId}&range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId?businessNameId=${orderDetailsListOfBusiness.businessName.businessNameId}&range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId?businessNameId=${orderDetailsListOfBusiness.businessName.businessNameId}&range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect" style="font-size:13px;">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId?businessNameId=${orderDetailsListOfBusiness.businessName.businessNameId}&range=viewAll">View All</a></li>
                    </ul>
                </div>
             
	            <div class="col l3 m3 s3 right">
                <form action="${pageContext.request.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId" method="post">
                    <input type="hidden" name="range" value="range"> 
                    <input type="hidden" name="businessNameId" id="businessNameId" value="${orderDetailsListOfBusiness.businessName.businessNameId}">
                    <span class="showDates">
                      <div class="input-field col s6 m5 l5">
                        <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate1" required>
                        <label for="startDate1">From</label>
                      </div>

                      <div class="input-field col s6 m5 l5">
                            <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate1">
                             <label for="endDate1">To</label>
                       </div>
                       <div class="input-field col s6 m2 l2">
                    <button type="submit" >View</button>
                    </div>
                  </span>
                </form>
            </div>
        	<div class="input-field col s12 l3 m3 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId" method="post">
	                    <div class="input-field col s12 m5 l5">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                     <input type="hidden" name="businessNameId" id="businessNameId" value="${orderDetailsListOfBusiness.businessName.businessNameId}">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
            
        </div>
        </div>
 		<div class="col s12 l12 m12 ">
        <table class="striped highlight centered display " id="tblData" cellspacing="0" width="100%">
            <thead>
                <tr>
                     <th class="print-col">Sr.No</th>
                     <th class="print-col">Order Id</th>
                     <th class="print-col">Total Amount</th>
                     <th class="print-col">Paid Amount</th>
                     <th class="print-col">Balance Amount</th>
                     <th class="print-col">Date of Payment</th>
                     <th class="print-col">Bank Detail</th>
                     <th class="print-col">Mode Of Payment</th>
                     <th class="print-col">Status</th>
                 </tr>
            </thead>
            <tbody>
             <c:if test="${not empty orderDetailsListOfBusiness.orderDetailsListOfBusinessSubList}">
			 <c:forEach var="listValue" items="${orderDetailsListOfBusiness.orderDetailsListOfBusinessSubList}">
			 <fmt:formatNumber var="totalAmount" value="${listValue.totalAmount}" maxFractionDigits="2" minFractionDigits="2" />
			 <fmt:formatNumber var="paidAmount" value="${listValue.paidAmount}" maxFractionDigits="2" minFractionDigits="2" />
			 <fmt:formatNumber var="balanceAmount" value="${listValue.balanceAmount}" maxFractionDigits="2" minFractionDigits="2" />
            	<tr>
            	<td><c:out value="${listValue.srno}" /></td>
            	<td><a title="View Order details" href="${pageContext.request.contextPath}/orderProductDetailsListForWebApp?orderDetailsId=${listValue.orderId}"><c:out value="${listValue.orderId}" /></a></td>
            	<td><c:out value="${totalAmount}" /></td>
            	<td><c:out value="${paidAmount}" /></td>
            	<td><c:out value="${balanceAmount}" /></td>
            	<td><c:out value="${listValue.dateOfPayment}" /></td>
            	<td><c:out value="${listValue.bankDetail}" /></td>
            	<td><c:out value="${listValue.modeOfPayment}" /></td>
            	<td>
            	<font color="${listValue.statusOfPaid=='Paid'?'Green':''}
             				   ${listValue.statusOfPaid=='UnPaid'?'Red':''}
             				   ${listValue.statusOfPaid=='Partial Paid'?'Blue':''}">
	              	<b>
	              		<c:out value="${listValue.statusOfPaid}" />
	              	</b>
             	</font>
            	</td>
            	</tr>            
            </c:forEach>
            </c:if>	
            </tbody>
            <tfoot>
            	 <tr>          	
				 <th colspan="2" style="text-align:center">Total</th>           	
            	<th style="text-align:center"><c:out value="${totalBusinessAmount}" /></th>
            	<th style="text-align:center"><c:out value="${amountPaid}" /></th>
            	<th style="text-align:center"><c:out value="${amountUnPaid}" /></th>
            	<th colspan="4"></th>
            	
            	</tr>
            	
            </tfoot>
        </table>
        <br>
  </div>      
       
    </main>
    <!--content end-->
</body>

</html>