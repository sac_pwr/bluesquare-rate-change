<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>


<script type="text/javascript">
var checkedId=[];
var mobileNumberCollection="";
$(document).ready(function() {
	
	var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
		$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	     //$('#msgHead').text("Brand Message");
	     $('#msg').text(msg);
	 }
	
	$("#checkAll").click(function () {
		var colcount=$('#tblData').DataTable().columns().header().length;
	    var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
	        state = this.checked;
			//alert(state);
	    for (var i = 0; i < cells.length; i += 1) {
	        cells[i].querySelector("input[type='checkbox']").checked = state;
	    }		                
	    
	});
	
	 $("input:checkbox").change(function(a){
		 
		 var colcount=$('#tblData').DataTable().columns().header().length;
	     var cells = $('#tblData').DataTable().column(colcount-1).nodes(), // Cells from 1st column
	         
	      state = false; 				
	     var ch=false;
	     
	     for (var i = 0; i < cells.length; i += 1) {
	    	 //alert(cells[i].querySelector("input[type='checkbox']").checked);
	    	
	         if(cells[i].querySelector("input[type='checkbox']").checked == state)
	         { 
	        	 $("#checkAll").prop('checked', false);
	        	 ch=true;
	         }                      
	     }
	     if(ch==false)
	     {
	    	 $("#checkAll").prop('checked', true);
	     }
	    	
	    //alert($('#tblData').DataTable().rows().count());
	});
	 
	 $('#sendsmsid').click(function(){
		 
		 $('#mobileEditSms').prop('checked', false);
		 	$("#mobileEditSms").change();
		 	mobileNumberCollection='';
		 	
			var count=parseInt('${count}')
			checkedId=[];
			//chb
			 var idarray = $("#employeeTblData")
          .find("input[type=checkbox]") 
          .map(function() { return this.id; }) 
          .get();
			 var j=0;
			 
			 for(var i=0; i<idarray.length; i++)
			 {								  
				 idarray[i]=idarray[i].replace('chb','');
				 if($('#chb'+idarray[i]).is(':checked'))
				 {					 
					 checkedId[j]=idarray[i].split("_")[0];
					 mobileNumberCollection=mobileNumberCollection+idarray[i].split("_")[1]+",";
					 j++;
				 }
			 }
			 mobileNumberCollection=mobileNumberCollection.substring(0,mobileNumberCollection.length-1);
			 $('#mobileNoSms').val(mobileNumberCollection);
			 $('#mobileNoSms').change();
			 $('#smsText').html('');
			 $('#smsSendMesssage').html('');
			 
			 if(checkedId.length==0)
			 {
				 $('#addeditmsg').find("#modalType").addClass("warning");
				$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
				 $('#addeditmsg').modal('open');
			    // $('#msgHead').text("Message : ");
			     $('#msg').text("Select Business For Send Message");
			 }
			 else
			 {
				 if(checkedId.length==1)
				 {
					 $('#smsText').val('');
					 $('#smsSendMesssage').html('');
					 $('.mobileDiv').show();
					 $('#sendMsg').modal('open');
				 }
				 else{
				 $('#smsText').val('');
				 $('#smsSendMesssage').html('');
				 $('.mobileDiv').hide();
				 $('#sendMsg').modal('open');
				 }
			 }
		});
		
		$('#sendMessageId').click(function(){
			
			
			 $('#smsSendMesssage').html('');
			var smsText=$("#smsText").val();
			if(checkedId.length==0)
			{
				$('#smsSendMesssage').html("<font color='red'>Select Employee For Send Message</font>");
				return false;
			}
			if(smsText==="")
			{
				$('#smsSendMesssage').html("<font color='red'>Enter Message Text</font>");
				return false;
			}
			var form = $('#sendSMSForm');
			//alert(form.serialize()+"&employeeDetailsId="+checkedId);
			
			$.ajax({
					type : form.attr('method'),
					url : form.attr('action'),
					data : form.serialize()+"&shopsIds="+checkedId+"&mobileNumber="+$('#mobileNoSms').val(),
					//async: false,
					success : function(data) {
						if(data==="Success")
						{
							$('#smsSendMesssage').html("<font color='green'>Message send SuccessFully</font>");
							$('#smsText').val('');
						}
						else
						{
							$('#smsSendMesssage').html("<font color='red'>Message sending Failed</font>");
						}
					}
			});
		});
		var table = $('#tblData').DataTable();
		 table.destroy();
		 $('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	         autoWidth: false,
	         columnDefs: [
	                      { 'width': '1%', 'targets': 0 },
	                      { 'width': '1%', 'targets': 1 },
	                      { 'width': '12%', 'targets': 2},
	                      { 'width': '15%', 'targets': 3},
	                      { 'width': '3%', 'targets': 4},
	                      { 'width': '3%', 'targets': 5},
	                      { 'width': '3%', 'targets': 6},
	                      { 'width': '3%', 'targets': 7},
	                      { 'width': '8%', 'targets': 8},
	                      { 'width': '3%', 'targets': 9,},
	                      { 'width': '3%', 'targets': 10},
	                      { 'width': '3%', 'targets': 11},
	                      /* { 'width': '1%', 'targets': 12,"visible": false}, */
	                      { 'width': '1%', 'targets': 12 },
	                      { 'width': '3%', 'targets': 13,"orderable": false},
	                      { 'width': '1%', 'targets': 14}
	                      ],
	         lengthMenu: [
	             [50, 75, 100, -1],
	             ['50 ', '75 ', '100', 'All']
	         ],
	         
	        
	         //dom: 'lBfrtip',
	         dom:'<lBfr<"scrollDivTable"t>ip>',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: '.print-col'
	                     },
	                     customize: function(doc, config) {
	                    	 doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [20,30,100,70,50,60,80,70,60,40,40,40,70] 
	                    		 } 
	                    		    })
	                         var tableNode;
	                         for (i = 0; i < doc.content.length; ++i) {
	                           if(doc.content[i].table !== undefined){
	                             tableNode = doc.content[i];
	                             break;
	                           }
	                         }
	        
	                         var rowIndex = 0;
	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	                          
	                         if(tableColumnCount > 6){
	                           doc.pageOrientation = 'landscape';
	                         }
	                         /*for customize the pdf content*/ 
	                         doc.pageMargins = [5,20,10,5];
	                         
	                         doc.defaultStyle.fontSize = 8	;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
	                       },
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: '.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: '.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		 $("select")
          .change(function() {
              var t = this;
              var content = $(this).siblings('ul').detach();
              setTimeout(function() {
                  $(t).parent().append(content);
                  $("select").material_select();
              }, 200);
          });
      $('select').material_select();
      $('.dataTables_filter input').attr("placeholder", "Search");
      var table = $('#tblData').DataTable(); // note the capital D to get the API instance
      var column = table.columns('.toggle');
      column.visible(false);
      $('#showColumn').on('click', function () {
       
      	 //console.log(column);
    
      	 column.visible( ! column.visible()[0] );
    
      });
      
});

</script>
<style>
table.dataTable tbody td {
    padding: 0 2px !important;
}
	.dataTables_wrapper {
 
    margin-left: 2px !important;
}

/* #sendMsg{
	width:35%;
	border-radius: 20px;
} */
table.dataTable thead th, table.dataTable thead td {
    padding: 10px 10px;
    border-bottom: 1px solid #111;
}
</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
         
        <!--Add New Supplier-->
        <div class="row">
        <br/> 
         <div class="col s10 l3 m6 left">        
            <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.servletContext.contextPath}/fetchBusinessTypeList"><i class="material-icons left" >add</i>Add Business Type</a>
        </div>
        <div class="col s10 l4 m6 left left-align">
            <a class="btn waves-effect waves-light blue-gradient" href="${pageContext.servletContext.contextPath}/addBusiness"><i class="material-icons left" >add</i>Add Business</a>
        </div>
   
        <div class="col s12 l12 m12 left">
      <br/>
            <table class="striped highlight centered  display " id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr. No.</th>
                        <th class="print-col">Account No.</th>
                        <th class="print-col" style="min-width:200px">Shop Name</th>
                         <th class="print-col" style="min-width:300px">Address</th>
                        <th class="print-col">Owner Name</th>
                        <th class="print-col">Mobile No./<br>Tele.No.</th>
                        <th class="print-col toggle">Email-Id</th>
                       
                        <th class="print-col toggle">GST No</th>
                        <th class="print-col toggle">Business Type</th>
                        <th class="print-col toggle">Area <br/></th>
                        <th class="print-col toggle">Region</th>
                        <th class="print-col toggle">City</th>
                       <!--  <th class="print-col">State</th>
                        <th class="print-col">Country</th> -->
                        <!-- <th class="print-col">Credit Limit</th> -->
                        <th class="print-col">Balance Amount</th>
                        <th>Action<br/><a href="#" class="tooltipped" id="showColumn" data-position='right' data-delay='50' data-tooltip='Show more columns'><i class="material-icons black-text">swap_horiz</i></a></th>
                        <!-- <th></th> -->
                        <th> <a href="#" class="modal-trigger" id="sendsmsid"><i class="material-icons tooltipped" data-position='right' data-delay='50' data-tooltip='Send Message' style="margin-left:-22%;">mail</i></a>
                            <input type="checkbox" class="filled-in checkAll" id="checkAll" />
                            <label for="checkAll"></label>
                        </th>
                    </tr>
                </thead>

                <tbody id="employeeTblData">
              <% int rowincrement=0; %>
                   <c:if test="${not empty businessNameList}">
					<c:forEach var = "i" begin = "0" end = "${businessNameList.size()-1}" step="1">						
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                    <tr ${businessNameList[i].businessName.status == true ? 'class="red lighten-4"' : ''}>
                        <td><c:out value="${rowincrement}" /></td>
                        <td><a class="tooltipped" data-position='right' data-delay='50' data-tooltip='View orders of business' href="${pageContext.request.contextPath}/fetchOrderDetailsListOfBusinessByBusinessNameId?businessNameId=${businessNameList[i].businessName.businessNameId}&range=currentMonth"><c:out value="${businessNameList[i].businessName.businessNameId}" /></a></td>
                        
                        <td><c:out value="${businessNameList[i].businessName.shopName}" /></td>
                         <td><c:out value="${businessNameList[i].businessName.address}" /></td>
                        <td class="wrapok"><c:out value="${businessNameList[i].businessName.ownerName}" /></td>
                        
                        <c:choose>
	                        <c:when test="${businessNameList[i].businessName.contact.mobileNumber != '' and businessNameList[i].businessName.contact.telephoneNumber != ''}">
	                        	<td class="wrapok"><c:out value="${businessNameList[i].businessName.contact.mobileNumber}" />/<br><c:out value="${businessNameList[i].businessName.contact.telephoneNumber}" /></td>
	                        </c:when>
	                        <c:when test="${businessNameList[i].businessName.contact.mobileNumber == '' and businessNameList[i].businessName.contact.telephoneNumber != ''}">
	                        	<td class="wrapok"><c:out value="${businessNameList[i].businessName.contact.telephoneNumber}" /></td>
	                        </c:when>
	                        <c:when test="${businessNameList[i].businessName.contact.mobileNumber != '' and businessNameList[i].businessName.contact.telephoneNumber == ''}">
	                        	<td class="wrapok"><c:out value="${businessNameList[i].businessName.contact.mobileNumber}" /></td>
	                        </c:when>
	                        <c:otherwise>
	                        	<td>NA</td>
	                        </c:otherwise>
                        </c:choose>
                        
                        
                        <td><c:out value="${(empty businessNameList[i].businessName.contact.emailId)?'NA':businessNameList[i].businessName.contact.emailId}" /></td>
                       
                        
                        <c:choose>  
						    <c:when test="${businessNameList[i].businessName.gstinNumber == ''}">  
						        <td>NA</td>
						    </c:when>  
						    <c:otherwise>  
						       <td class="wrapok"><c:out value="${businessNameList[i].businessName.gstinNumber}" /></td> 
						    </c:otherwise>  
						</c:choose>  
                        
                        <td><c:out value="${businessNameList[i].businessName.businessType.name}" /></td>
                        <td class="wrapok"><c:out value="${businessNameList[i].businessName.area.name}" /></td>
                        <td class="wrapok"><c:out value="${businessNameList[i].businessName.area.region.name}" /></td>
                        <td class="wrapok"><c:out value="${businessNameList[i].businessName.area.region.city.name}" /></td>
                        <%-- <td><c:out value="${businessNameList[i].businessName.area.region.city.state.name}" /></td>
                        <td><c:out value="${businessNameList[i].businessName.area.region.city.state.country.name}" /></td> --%>
                        <%-- <td>
                        <fmt:formatNumber var="amount" value="${businessNameList[i].businessName.creditLimit}" maxFractionDigits="2" minFractionDigits="2" />
                        <c:out value="${amount}" />
                        </td> --%>
                        <td class="wrapok">
                        <fmt:formatNumber var="amount" value="${businessNameList[i].balanceAmount}" maxFractionDigits="2" minFractionDigits="2" />
                        <c:out value="${amount}" />
                        </td>
                        <td><a class="btn-flat" ${businessNameList[i].businessName.status == true ? 'disabled="disabled"' : ''} href="${pageContext.servletContext.contextPath}/fetchBusiness?businessNameId=${businessNameList[i].businessName.businessNameId}"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></a> <a href="#delete${rowincrement}" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td>
                        <!-- Modal Trigger -->
                        <%-- <td><a href="#delete${rowincrement}" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> --%>
                        <td>

                            <input type="checkbox" class="filled-in" id="chb${businessNameList[i].businessName.businessNameId}_${businessNameList[i].businessName.contact.mobileNumber}" />
                            <label for="chb${businessNameList[i].businessName.businessNameId}_${businessNameList[i].businessName.contact.mobileNumber}"></label>

                        </td>
                    </tr>
                    	 <!-- Modal Structure for delete -->

                    <div id="delete${rowincrement}" class="modal deleteModal row">
                        <div class="modal-content col s12 m12 l12">
                            <h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
                            <h5 class="center-align">${businessNameList[i].businessName.status == true ? 'Are you sure you want to Enable?' : 'Are you sure you want to Disable?'}</h5>
                        </div>
                        <div class="modal-footer">
            
         				   <div class="col s6 m6 l3 offset-l3">
                 				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
                			</div>
            				<div class="col s6 m6 l3">
             				   <a href="${pageContext.request.contextPath}/disableBusiness?businessNameId=${businessNameList[i].businessName.businessNameId}" class="modal-action modal-close waves-effect  btn">${businessNameList[i].businessName.status == true ? 'Enable' : 'Disable'}</a>
               				 </div>
                
            			</div>
                
                    </div>
				</c:forEach>
				</c:if>
				</tbody>
            </table>

                    <!-- Modal Structure for sendMsg -->
       
                    <form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShops" method="post">
                         <div id="sendMsg" class="modal">                    
	                        <div class="modal-content">                                   	
	                        	 <div class="fixedDiv">
		                              <div class="center-align" style="padding:0">
		                         		<!-- <i class="material-icons  medium" style="padding:50% 40% 0 40%">mail</i> -->
		                         	  </div>
	                         	 </div>
	                         	 <div class="scrollDiv">
	                         	  
		                         	 <div class="row mobileDiv" style="margin-bottom:0">
			                         	  	<div class="col s12 l9 m9 input-field" style="padding-left:0">
			                                    <label for="mobileNoSms" class="black-text" style="left:0">Mobile No</label>
			                                    <input type="text" id="mobileNoSms" class="grey lighten-3" minlength="10" maxlength="10" readonly>
			                                    	
			                                </div>  
			                                 <div class="col s12 l2 m2 right-align" style="margin-top:10%">
			                                    <input type="checkbox" id="mobileEditSms"/>
			                                    <label for="mobileEditSms" class="black-text">Edit</label>
			                                    
			                                </div>
		                             </div>
		                                <div class="input-field">
		                                    <label for="smsText" class="black-text">Type Message</label>
		                                    <textarea id="smsText" class="materialize-textarea" data-length="180" name="smsText"></textarea>
		                                </div> 
		                                <div class="input-field center">                                
		                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
		            					</div>
		            					<div class="fixedFooter">
		            					 	<div class="col s6 m4 l3 right-align" style="margin-left:3%">
		                            		 <a href="#!" class="modal-action modal-close waves-effect  btn red">Cancel</a>
			                                </div>
				                          	<div class="col s6 m6 l2">	
				                           		<button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
				                            </div>
				                         </div>
			                     </div>  	
			                          <br><br><br>
	                         	</div>	
                        </div>
                        </form>
                   
                  <%--   <div id="sendMsg" class="modal">
                    <form id="sendSMSForm" action="${pageContext.request.contextPath}/sendSMSTOShops" method="post">
                        <div class="modal-content">
                            <h5 class="center">Message</h5>
                            <hr>	
                            <div class="row">
                                <div class="col s12 l7 m7 push-l3">
                                    <label for="smsText" class="black-text">Type Message</label>
                                    <textarea id="smsText" class="materialize-textarea" name="smsText"></textarea>
                                </div> 
                                <div class="input-field col s12 l7 push-l4 m5 push-m4 ">                                
                   			 		 <font color='red'><span id="smsSendMesssage"></span></font>
            					</div>                            
                            </div>
                               				
                        </div>
                        <div class="modal-footer center-align row">
                            <div class="col s6 m6 l6">
                            		 <button type="button" id="sendMessageId" class="modal-action waves-effect btn btn-waves  ">Send</button>
                             </div>
                          	<div class="col s6 m6 l4">	
                           			 <a href="#!" class="modal-action modal-close waves-effect  btn red ">Cancel</a>
                             </div>                           
                          
                        </div>
                        </form>
                    </div> --%>
                
        </div>
   </div>
        <!-- Modal Structure for View Product Details -->

        <div id="viewDetails" class="modal">
            <div class="modal-content">
                <h5><u>Product Details</u></h5>
                <table border="2" class="tblborder centered">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td>abc</td>
                            <td>abc</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">

                <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
            </div>
        </div>
         <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
					</div>
				</div>
			</div>
		</div>
		
		<!-- <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
    </main>
    <!--content end-->
</body>

</html>