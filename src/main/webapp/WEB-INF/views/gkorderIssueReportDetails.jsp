<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
     <script>
     	$(document).ready(function(){
     		var table = $('#tblData').DataTable();
   		 table.destroy();
   		 $('#tblData').DataTable({
   	         "oLanguage": {
   	             "sLengthMenu": "Show _MENU_",
   	             "sSearch": "_INPUT_" //search
   	         },
   	         autoWidth: false,
   	         columnDefs: [
   	                      { 'width': '5%', 'targets': 0 },
   	                      { 'width': '20%', 'targets': 1 },
   	                      { 'width': '10%', 'targets': 2},
   	                      { 'width': '10%', 'targets': 3},
   	                      { 'width': '10%', 'targets': 4}
   	                      ],
   	         lengthMenu: [
   	             [10, 25., 50, -1],
   	             ['10 ', '25 ', '50 ', 'All']
   	         ],
   	         
   	         
   	         //dom: 'lBfrtip',
   	         dom:'<lBfr<"scrollDivTable"t>ip>',
   	         buttons: {
   	             buttons: [
   	                 //      {
   	                 //      extend: 'pageLength',
   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
   	                 //  }, 
   	                 {
   	                     extend: 'pdf',
   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
   	                     //title of the page
   	                     title: function() {
   	                         var name = $(".heading").text();
   	                         return name
   	                     },
   	                     //file name 
   	                     filename: function() {
   	                         var d = new Date();
   	                         var date = d.getDate();
   	                         var month = d.getMonth();
   	                         var year = d.getFullYear();
   	                         var name = $(".heading").text();
   	                         return name + date + '-' + month + '-' + year;
   	                     },
   	                     //  exports only dataColumn
   	                     exportOptions: {
   	                         columns: '.print-col'
   	                     },
   	                     customize: function(doc, config) {
   	                    	 doc.content.forEach(function(item) {
   	                    		  if (item.table) {
   	                    		  item.table.widths = [40,'*',90,80,60] 
   	                    		 } 
   	                    		    })
   	                      
   	                         /*for customize the pdf content*/ 
   	                         doc.pageMargins = [5,20,10,5];
   	                         
   	                         doc.defaultStyle.fontSize = 8	;
   	                         doc.styles.title.fontSize = 12;
   	                         doc.styles.tableHeader.fontSize = 11;
   	                         doc.styles.tableFooter.fontSize = 11;
   	                         doc.styles.tableHeader.alignment = 'center';
   	                         doc.styles.tableBodyEven.alignment = 'center';
   	                         doc.styles.tableBodyOdd.alignment = 'center';
   	                       },
   	                 },
   	                 {
   	                     extend: 'excel',
   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
   	                     //title of the page
   	                     title: function() {
   	                         var name = $(".heading").text();
   	                         return name
   	                     },
   	                     //file name 
   	                     filename: function() {
   	                         var d = new Date();
   	                         var date = d.getDate();
   	                         var month = d.getMonth();
   	                         var year = d.getFullYear();
   	                         var name = $(".heading").text();
   	                         return name + date + '-' + month + '-' + year;
   	                     },
   	                     //  exports only dataColumn
   	                     exportOptions: {
   	                         columns: '.print-col'
   	                     },
   	                 },
   	                 {
   	                     extend: 'print',
   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
   	                     //title of the page
   	                     title: function() {
   	                         var name = $(".heading").text();
   	                         return name
   	                     },
   	                     //file name 
   	                     filename: function() {
   	                         var d = new Date();
   	                         var date = d.getDate();
   	                         var month = d.getMonth();
   	                         var year = d.getFullYear();
   	                         var name = $(".heading").text();
   	                         return name + date + '-' + month + '-' + year;
   	                     },
   	                     //  exports only dataColumn
   	                     exportOptions: {
   	                         columns: '.print-col'
   	                     },
   	                 },
   	                 {
   	                     extend: 'colvis',
   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
   	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
   	                     collectionLayout: 'fixed two-column',
   	                     align: 'left'
   	                 },
   	             ]
   	         }

   	     });
   		 $("select")
             .change(function() {
                 var t = this;
                 var content = $(this).siblings('ul').detach();
                 setTimeout(function() {
                     $(t).parent().append(content);
                     $("select").material_select();
                 }, 200);
             });
         $('select').material_select();
         $('.dataTables_filter input').attr("placeholder", "Search");
     	});
     </script>
     <!-- #0073b7 -->
     <style>
     .card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
    tfoot th,
    tfoot td    {
    	text-align:center;
    }
    .leftHeader{
    	width:105px !important;
    	display:inline-block;
    }
     </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
    <!-- <script>alert("${orderProductIssueListForIssueReportResponse}");</script> -->
    	
        <br>
        <div class="row">
        	<div class="col s12 l10 m10 card-panel hoverable blue-grey lighten-4">
      	
       			
                <div class="col s12 l6 m6 ">
                <p>
                 <span class="leftHeader">Order Id:</span>
                 <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderId}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                </div>
                     <div class="col s12 l6 m6">
                       <p id="Add"><span class="leftHeader">Date of Order:</span> 
                       <b>
                       <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderDetailsAddedDatetime}"  />
				       <c:out value="${date}" />
                       </b>
                       </p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                   <p id="department"><span class="leftHeader">Shop Name: </span><b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.businessName.shopName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                 <div class="col s12 l6 m6 ">
                   <p><span class="leftHeader">Salesman: </span><b><c:out value="${orderProductIssueListForIssueReportResponse.salesman}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                    <p><span class="leftHeader">Mobile No: </span><b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.businessName.contact.mobileNumber}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6 ">
                      <p><span class="leftHeader">Delivery Boy:</span><b><c:out value="${orderProductIssueListForIssueReportResponse.deliveryBoy}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l6 m6">
                 <div class="col s12 m2 l2 leftHeader" style="padding:0;"><p id="area"><span>Area:</span></p></div>
                 <div class="col s12 m8 l8" style="word-wrap:break-word;padding:0"><p><b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.businessName.area.name}" /></b></p></div>
                   <%-- <p id="area"><span class="leftHeader">Area:</span> <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.businessName.area.name}" /></b></p> --%>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                
               
                
           
               <%--  <div class="col s12 l6 m6 ">
                       <p id="Add" class=" blue-text">Date of Order: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderDetailsAddedDatetime}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div> --%>
                
           
           </div> 
          <div class="col s12 m2 l2 right right-align" style="padding:0"><%-- ${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderStatus.status == 'Delivered' or orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderStatus.status == 'Delivered Pending' ? '' : 'style="display: none;"'}> --%>
          	
        		<a class="btn waves-effect waves-light" style="margin-top:7%;margin-left:25%" href="${pageContext.request.contextPath}/openIssuedBill?orderId=${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderId}">Get Bill</a>
          </div>
          </div>   
        
        
           <%--  <div class="col s12 l6 m6">
                <div class="card grey lighten-3 hoverable z-depth-3">
                    <div class="card-content blue-text">
                        <p id="name" class="center-align blue-text">Order ID: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderId}" /></b> </p>
                        <hr style="border:1px dashed teal; "> 
                        <p id="department" class="center-align blue-text">Shop Name: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.businessName.shopName}" /></b></p>
                        <hr style="border:1px dashed teal;"> 
                        <p id="area" class="center-align blue-text">Area: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.businessName.area.name}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="MobileNo" class="center-align blue-text">Mobile No: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.businessName.contact.mobileNumber}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="MobileNo" class="center-align blue-text">Salesman Name: <b><c:out value="${orderProductIssueListForIssueReportResponse.salesman}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="MobileNo" class="center-align blue-text">Delivery Boy Name: <b><c:out value="${orderProductIssueListForIssueReportResponse.deliveryBoy}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="Add" class="center-align blue-text">Date of Order: <b><c:out value="${orderProductIssueListForIssueReportResponse.orderProductIssueDetails.orderDetails.orderDetailsAddedDatetime}" /></b></p>
                    </div>
                </div>
            </div> --%>
     
       
       
            <div class="col s12 l12 m12 ">
                <table class="striped highlight centered mdl-data-table display" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        	<th class="print-col">Sr. No.</th>
                            <th class="print-col">Product Name</th>
                            <!-- <th class="print-col">Current Quantity</th> -->
                            <th class="print-col">Ordered Quantity</th>
                            <th class="print-col">Issue Quantity</th>
                            <th class="print-col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<% int rowincrement=0; %>
                    	<c:if test="${not empty orderProductIssueListForIssueReportResponse.orderProductDetailsList}">
							<c:forEach var="listValue" items="${orderProductIssueListForIssueReportResponse.orderProductDetailsList}">
							<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
		                        <tr>
		                            <td><c:out value="${rowincrement}" /></td>
		                            <td><c:out value="${listValue.product.productName}" /><font color="green"><c:out value="${listValue.type=='Free'?'-(Free)':''}" /></font></td>
		                            <%--  <td><c:out value="${listValue.product.currentQuantity}" /></td> --%>
		                            <td><c:out value="${listValue.purchaseQuantity}" /></td>
		                            <td><c:out value="${listValue.issuedQuantity}" /></td>
		                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.issueAmount}" /></td>
		                        </tr>
	                        </c:forEach>
                        </c:if>
                    </tbody>
                    <tfoot class="centered">
            		<tr>
             			  <td colspan="2"><b>Total</b></td>
                		  <td><center><c:out value="${totalOrderQuantity}" /></center></td>
                		  <td><center><c:out value="${totalIssuedQuantity}" /></center></td>
                		  <td><center><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${allTotalAmount}" /></center></td>
           			</tr>
        			</tfoot>
                </table>
            </div>
        </div>
    </main>
    <!--content end-->
</body>

</html>