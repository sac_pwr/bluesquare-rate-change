<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Blue Square</title>

<link rel="shortcut icon" href="resources/img/bs_icon2-01.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- Compiled and minified JavaScript -->
<link rel="stylesheet"  href="resources/css/materialize.min.css">
 <!-- <link rel="stylesheet"	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css"> --> 
<!-- <script src="https://use.fontawesome.com/cf75b05ed3.js"></script> -->
<link rel="stylesheet" href="resources/css/font-awesome.min.css">
<script	src="resources/js/jquery.min.js"></script>
<script src="resources/js/materialize.min.js"></script>
<!-- <script	src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script> -->
<!--   <script src="https://code.highcharts.com/highcharts.js"></script> -->
<link href="resources/css/style.css" rel="stylesheet">

 <link href="https://fonts.googleapis.com/css?family=Nunito+Sans|Open+Sans|Ubuntu" rel="stylesheet">

<script type="text/javascript" src="resources/js/script.js"></script>
<script type="text/javascript" src="resources/js/select2.min.js"></script>
<script type="text/javascript" src="resources/js/calculateProperAmount.js"></script>

<link href="resources/css/materialicon.css"	rel="stylesheet">
<link href="resources/css/select2.min.css"	rel="stylesheet">
<!-- https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css
https://cdn.datatables.net/1.10.16/css/dataTables.material.min.css -->
<link rel="stylesheet"	href="resources/css/jquery.dataTables.min.css">
<link rel="stylesheet"	href="resources/css/buttons.dataTables.min.css">
<!--<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<!--<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>-->
<script type="text/javascript"	src="resources/js/jquery.dataTables.min.js"></script>
<!-- <script type="text/javascript"	src="https://cdn.datatables.net/1.10.16/js/dataTables.material.min.js"></script> -->

<script type="text/javascript"	src="resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"	src="resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"	src="resources/js/jszip.min.js"></script>
<script type="text/javascript"	src="resources/js/pdfmake.min.js"></script>
<script type="text/javascript" src="resources/js/vfs_fonts.js"></script>
<script type="text/javascript"	src="resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"	src="resources/js/buttons.print.min.js"></script>
<script type="text/javascript"	src="resources/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="resources/js/hashtable.js"></script>
<link href="resources/css/animation.min.css" rel="stylesheet">
<script type="text/javascript" src="resources/js/moment.js"></script>
<style>
        #myBtn {
            display: none;
            /* Hidden by default */
            position: fixed;
            /* Fixed/sticky position */
            bottom: 20px;
            /* Place the button at the bottom of the page */
            right: 30px;
            /* Place the button 30px from the right */
            z-index: 99;
            /* Make sure it does not overlap */
            border: none;
            /* Remove borders */
            color: white;
            /* Text color */
            cursor: pointer;
            /* Add a mouse pointer on hover */
            padding: 8px 0px 8px 15px;
            /* Some padding */
            width: 40px;
            background-color: #353535;
        }
        
        #myBtn:hover {
            background-color: #c7081b;
            /* Add a dark-grey background on hover */
        }
      
		<!-- code for loader -->

        </style>
	        <script>
        window.onscroll = function() {
            scrollFunction()
        };
        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Chrome, Safari and Opera 
            document.documentElement.scrollTop = 0; // For IE and Firefox
        }
        
        $(document).ready(function(){
        	var from_$input = $('#startDate').pickadate(), 
    		from_picker = from_$input.pickadate('picker')
    		console.log(from_picker);
    		var to_$input = $('#endDate').pickadate(),
    		to_picker = to_$input.pickadate('picker')
    		console.log(to_picker);
    		// Check if ther's a "from" or "to" date to start with.
    		if (from_picker.get('value')) {
    			to_picker.set('min', from_picker.get('select'))
    		}
    		if (to_picker.get('value')) {
    			from_picker.set('max', to_picker.get('select'))
    		}
    		// When something is selected, update the "from" and "to"
    		// limits.
    		from_picker.on('set', function(event) {
    			if (event.select) {
    				to_picker.set('min', from_picker.get('select'))
    			} else if ('clear' in event) {
    				to_picker.set('min', false)
    			}
    		})
    		to_picker.on('set', function(event) {
    			if (event.select) {
    				from_picker.set('max', to_picker.get('select'))
    			} else if ('clear' in event) {
    				from_picker.set('max', false)
    			}
    		})
        });
    </script>