
<%@page import="java.util.Calendar"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">

var run = function(){
	var online = navigator.onLine;
	if(!online)
	{
	document.getElementById('errorInternet').style.display="block";
		//alert("No internet connection.");
	}
	if(online)
	{ 
		document.getElementById('errorInternet').style.display="none";
		//alert("No internet connection.");
	}	
}
 
setInterval(run,1000);

</script>
<style>

	 .collapsible li.active .collapsible-header span>i {
  -ms-transform: rotate(180deg); /* IE 9 */
  -webkit-transform: rotate(180deg); /* Chrome, Safari, Opera */
  transform: rotate(180deg);
}
	
        main{
        padding-right:10px !important;
        }
       /*css for side nav hide/show*/
        
        .reveal-open {
            max-width: 60px !important;
            /*max-height: 100px !important;*/
            /*margin-top: 62px;*/
        }
        
        .side-nav {
            /* overflow given to give effect of half hiding side nav */
            /*overflow: hidden;*/
            max-width: 225px;
            /* to give effect of sliding of side navbar */
            -webkit-transition: max-width .01s linear;
            -moz-transition: max-width .01s linear;
            transition: max-width .01s linear;
        }
        /* to give padding to body after side nav is half closed */
        
        .intro {
            padding-left: 70px;
        }
        /*for display button over sidebar*/
        
       /*  .side-nav.fixed {
            left: 0;
            top: 68px;
            -webkit-transform: translateX(0);
            transform: translateX(0);
            position: fixed;
            z-index: 100;
        } */
        
       /*  .sidebutton {
            transform: translateX(-190px) rotate(180deg);
            transform: rotate(180deg);
            transform-origin: right left;
        } */
        
           
     
        
      
     
        .greycolor {
            background-color: #eceff1;
        }
        
        #logo-collapse {
            display: none;
        }
        
        #logo-collapse-full {
            display: visible;
        }
        
        nav ul li a.btn-flat{
        	padding: 0 5px 4px 5px;
        	line-height:45px;
        	height:45px; 
        	margin-right: 0px;
        }
        .slidemargin{
        	
        }
     /*    nav a{
        	color:#303641 !important;
        } */
       
       	nav .btn-flat{
       		font-size:0.8rem !important;
       	}
       	/* url('resources/img/net.png') */
       	#errorInternet{
       	display:none;
       	 position:fixed; 
       	 background: grey  center no-repeat;
       	 background-size:cover;         	
       	 top:0;
       		right:0;
       		bottom:0;
       		left:0;
			height:100vh;
       	    width:100vw;
       	     opacity:0.8;  		
       		background-size:cover;
       		z-index:1000;
       		/* -webkit-filter: blur(5px);
  -moz-filter: blur(5px);
  -o-filter: blur(5px);
  -ms-filter: blur(5px);
  filter: blur(5px);	 */ 	
       	
       	}
   
		.spinner-blue, .spinner-blue-only {
    		border-color: black;
		}
</style>


<!--top to scroll button-->
    <div onclick="topFunction()" id="myBtn"> <img src="resources/img/arrow-up.png" alt="scroll up" /> </div>
  
  <div id="errorInternet" >    	
    	<!-- <img src="resources/img/molecule.png" class="pulse center-align" width="100%" height="100%"/> -->
    	<!-- <h2 class="pulse center-align" style="position:relative;top:30%">Oops</h2> -->
    	<h2 class="pulse center-align" style="position:relative;top:35%">No internet Connection....</h2>
    </div>
  <!-- style="display:none;" -->
    <!--navbar start-->
    <div class="navbar-fixed z-depth-3">
        <!--class=" blue darken-8 nav"-->
        <nav class="sidenav-color">

            <div class="nav-wrapper">
                <!--style="padding-left:50px; height:60px; "-->

                <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"><img class="responsive-img" src="resources/img/bslogo_66.png" style="margin:5% 2% 0 15%;height:50px;"></a> --%>
                
                <a href="#" data-activates="mobile-demo" class="button-collapse left" style="padding-left: 10px; margin-left: 0px;"><i class="material-icons">menu</i></a>
            
                <!--<ul class=" hide-on-med-and-down center" style="margin-left:50%">-->
                <!--<li><a id="slide"><i class="medium material-icons ">swap_horiz</i></a></li>-->
                <!--<li><a id="slide"><i class="medium material-icons ">add</i></a></li>-->
                <!--</ul>-->
                <!--<ul class="center hide-on-med-and-down">
                    <li>
                        <a class="white-text  center" style="font-size:200%;">List of Supplier</a>
                    </li>
                </ul>-->
              <!--   <ul class="hide-on-med-and-down" style="margin-left:15%;">
                 <li><a id="slide" class=""><i class="medium material-icons">menu</i></a></li>
                </ul> -->
                
                <ul class="hide-on-large-only center-align" style="margin-left:40%;">
                	 
                    <li>
                        <a class="center heading1" style="font-size:120%;">${pageName}</a>
                    </li>
                  
                    <!--<li><a id="slide"><i class="medium material-icons ">add</i></a></li>-->
                </ul>
                <ul class="hide-on-med-and-down" style="margin-left:48%;">
                	 
                    <li>
                        <a class="center heading" style="font-size:120%;">${pageName}</a>
                    </li>
                  
                    <!--<li><a id="slide"><i class="medium material-icons ">add</i></a></li>-->
                </ul>
                <ul class="right">
                    <!--<li><a href="search" class="waves-effect waves-light btn-flat white-text" style="padding: 0px 5px 4px 5px; height: 64px; margin-right: 0px;"><i
						class="fa fa-search white-text"></i> Search</a></li>-->
					<c:choose>
                    	<c:when test="${sessionScope.loginType=='CompanyAdmin'}">
                    		<li><a href="${pageContext.servletContext.contextPath}/openForgetPasswordCompany" class="waves-effect waves-light btn-flat"><i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>
                    	</c:when>
                    	<c:when test="${sessionScope.loginType=='Admin'}">
	                    	<li><a href="${pageContext.servletContext.contextPath}/companySetting" class="waves-effect waves-light btn-flat"><i class="fa fa-user left"></i>${sessionScope.sessionCopamnyName}</a></li>
                    		<li><a href="${pageContext.servletContext.contextPath}/#" class="waves-effect waves-light btn-flat"><i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>
                    	</c:when>
                    	<c:otherwise>
                    		<li><a href="${pageContext.servletContext.contextPath}/openForgetPasswordGateKeeper" class="waves-effect waves-light btn-flat"><i class="fa fa-user left"></i>${sessionScope.loginName}</a></li>                    	
                    	</c:otherwise>
                    </c:choose>
					
                    <li><a href="${pageContext.servletContext.contextPath}/logoutEmployee" class="waves-effect waves-light btn-flat"><i
					class="fa fa-sign-out left"></i> Logout</a></li>
                </ul>
                
            </div>
        </nav>
    </div>
    
    <!-- admin navbar for medium and small device start -->
<c:if test="${sessionScope.loginType=='Admin'}">
    <div class="col s4 l4 m4">
         <ul id="mobile-demo" class="side-nav hide-on-large-only sidenav-color z-depth-3">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="center element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <!-- <a id="slide" href="#" class=""><i class="material-icons small " data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a> -->
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                    <li><a href="${pageContext.servletContext.contextPath}/fetchAllCompany" class="collapsible-header">Manage Companies<i class="material-icons" >settings</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/location_admin" class="collapsible-header">Location<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header">Inventory Details<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Inventory" aria-hidden="true">build</i></a></li>
                	<li>
                        <a class="collapsible-header ">Reports<i class="material-icons right chevron">expand_more</i><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Counter Report">insert_drive_file</i>Counter Report</a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Payment Report">account_balance_wallet</i>Payment Report</a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Cheque Report">subtitles</i>Cheque Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down " data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i>Product Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Business Report">people</i>Business Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i>Salesman Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Inventory Report">view_module</i>Inventory Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today">Supplier orders<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Supplier Orders" aria-hidden="true">attach_money</i>Supplier Orders</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Order Report">assignment</i>Order Report</a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i>Return Item Report</a></li>
								<li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Report">broken_image</i>Damage Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today">Collection Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true">collections_bookmark</i>Collection Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today">Sales Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true">attach_money</i>Sales Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today">Cancel Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true">clear</i>Cancel Report</a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport">GST Report<i class="fa fa-inr " data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="padding-left:3%"></i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/profit-loss">Profit & Loss<i class="material-icons" data-position="right" data-delay="50" data-tooltip="Profit & Loss" aria-hidden="true" style="padding-left:3%">show_chart</i></a></li>
                            </ul>
                        </div>
                    </li>                	
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header">Monitoring<i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Current Location" aria-hidden="true">location_on</i></a></li>            
                </ul>
            </li>
        </ul>
    </div>
</c:if>
<!-- admin navbar for medium and small device stop -->
	<!-- Companyadmin navbar for medium and small device start -->
<c:if test="${sessionScope.loginType=='CompanyAdmin'}">
   <!-- fixed side nav on all page -->
    <div class="col s4 l4 m4">
         <ul id="mobile-demo" class="side-nav hide-on-large-only sidenav-color z-depth-3">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="center element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <!-- <a id="slide" href="#" class=""><i class="material-icons small " data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a> -->
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                	<%-- <li><a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left">Vaisansar<i class="material-icons black-text" aria-hidden="true">menu</i>
                        </a>
                	</li>  --%>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchSupplierList" class="collapsible-header">Manage Supplier<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Supplier" aria-hidden="true">settings</i>
                        </a>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/location_company" class="collapsible-header">Location<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li>
                    <li>
                        <a class="collapsible-header">Product<i class="material-icons right chevron">expand_more</i><i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Manage Product">local_grocery_store</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBrandList"><i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Manage Brand">star</i>Manage Brand</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchCategoriesList"><i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Manage Category">filter_list</i>Manage Category</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductList"><i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Manage Product">add_shopping_cart</i>Manage Product</a></li>
                            </ul>
                        </div>
                    </li>
	                <li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeList" class="collapsible-header">HRM<i class="material-icons " data-position="right" data-delay="50" data-tooltip="HRM" aria-hidden="true">work</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameList" class="collapsible-header">Business List<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Business list" aria-hidden="true">business_center</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header">Manage Inventory<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Inventory" aria-hidden="true">build</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header">Manage Expense<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Expense" aria-hidden="true">settings</i>
                        </a>
                    </li>
                	<li>
                        <a class="collapsible-header ">Reports<i class="material-icons right chevron">expand_more</i><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Counter Report">insert_drive_file</i>Counter Report</a></li>
                               	<li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Payment Report">account_balance_wallet</i>Payment Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Cheque Report">subtitles</i>Cheque Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down " data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i>Product Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Business Report">people</i>Business Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i>Salesman Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Inventory Report">view_module</i>Inventory Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today">Supplier orders<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Supplier Orders" aria-hidden="true">attach_money</i>Supplier Orders</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Order Report">assignment</i>Order Report</a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i>Return Item Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Report">broken_image</i>Damage Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today">Collection Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true">collections_bookmark</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today">Sales Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true">attach_money</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today">Cancel Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true">clear</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport">GST Report<i class="fa fa-inr " data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="padding-left:3%"></i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/profit-loss">Profit & Loss<i class="material-icons" data-position="right" data-delay="50" data-tooltip="Profit & Loss" aria-hidden="true" style="padding-left:3%">show_chart</i></a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchPaymentPendingList" class="collapsible-header ">Payment<i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Manage Payment" aria-hidden="true">payment</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=currentMonth" class="collapsible-header ">Ledger<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Ledger" aria-hidden="true">assessment</i></a></li>
                    <%-- <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header">Complains<i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Complains" aria-hidden="true">feedback</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header">Get Invoice<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Get Invoice" aria-hidden="true">receipt</i></a></li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header">Monitoring<i class="material-icons  " >location_on</i></a></li>            
                </ul>
            </li>
        </ul>
    </div>    
    <!--navbar end-->
</c:if>
	<!-- Companyadmin navbar for medium and small device stop -->
	<!-- gatekeeper navbar for medium and small device start -->   
<c:if test="${sessionScope.loginType=='GateKeeper'}">
<!-- fixed side nav on all page -->

    <div class="col s4 l4 m4">
        <ul id="mobile-demo" class="side-nav hide-on-large-only sidenav-color z-depth-3">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="center element-sideNav" style="font-size:30px;">Vaisansar</a>
                <!--  <a id="slide" href="#" class=""><i class="material-icons small " data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a> -->
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                <li><a href="${pageContext.servletContext.contextPath}/openCounter" class="collapsible-header">Counter Page<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Counter" aria-hidden="true">dvr</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header">Manage Inventory<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Inventory" aria-hidden="true">build</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeListForGkView" class="collapsible-header">Employee Details<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Employee Details" aria-hidden="true">work</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/GKOrderDetailsTodayList?areaId=0">Booked Order List<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Booked Order List" aria-hidden="true">format_list_bulleted</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header">Get Invoice<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Get Invoice" aria-hidden="true">receipt</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header">Manage Expense<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Manage Expense" aria-hidden="true">settings</i>
                        </a>
                    </li>
                	<%-- <li>
                        <a class="collapsible-header ">Reports<i class="material-icons right chevron">expand_more</i><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down " data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i>Product Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Business Report">people</i>Business Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i>Salesman Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Inventory Report">view_module</i>Inventory Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Order Report">assignment</i>Order Report</a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i>Return Item Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today">Collection Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true">collections_bookmark</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today">Sales Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true">attach_money</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today">Cancel Report<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true">clear</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today">DeliveryBoy Return<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Return from Delivery " aria-hidden="true">file_download</i></a></li>
		                		<% Calendar cal=Calendar.getInstance();%>	
		                		<li><a href="${pageContext.servletContext.contextPath}/damageRecoveryList?startMonth=<%=cal.get(Calendar.MONTH)+1%>&startYear=<%=cal.get(Calendar.YEAR)%>&endMonth=<%=cal.get(Calendar.MONTH)+1%>&endYear=<%=cal.get(Calendar.YEAR)%>">Damage Recovery<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Recovery" aria-hidden="true">restore_page</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport">GST Report<i class="fa fa-inr " data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="padding-left:3%"></i></a></li>
                            </ul>
                        </div>
                    </li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today">DeliveryBoy Return<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Return from Delivery " aria-hidden="true">file_download</i></a></li>
              		<% Calendar cal=Calendar.getInstance();%>	
              		<li><a href="${pageContext.servletContext.contextPath}/damageRecoveryList?startMonth=<%=cal.get(Calendar.MONTH)+1%>&startYear=<%=cal.get(Calendar.YEAR)%>&endMonth=<%=cal.get(Calendar.MONTH)+1%>&endYear=<%=cal.get(Calendar.YEAR)%>">Damage Recovery<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Recovery" aria-hidden="true">restore_page</i></a></li>
                	<%-- <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header">Complains<i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Complains" aria-hidden="true">feedback</i></a></li> --%>
                    <li>
                        <a class="collapsible-header">Reports <i class="material-icons right chevron">expand_more</i><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Reports">equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Counter Report">insert_drive_file</i>Counter Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Inventory Report">view_module</i>Inventory Report</a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today">Supplier orders<i class="material-icons " data-position="right" data-delay="50" data-tooltip="Supplier Orders" aria-hidden="true">attach_money</i>Supplier Orders</a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Order Report">assignment</i>Order Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Sales Report" >attach_money</i>Sales Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Cancel Report" >clear</i>Cancel Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderReportForWeb"><i class="material-icons  " data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i>Return Item Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchReplacementOrderReportForWeb"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Replacement Report">autorenew</i>Replacement Report</a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Report">broken_image</i>Damage Report</a></li>                                
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header">Monitoring<i class="material-icons  " >location_on</i></a></li>
                </ul>
            </li>
        </ul>
    </div>
    <!--navbar end-->
</c:if>
<!-- gatekeeper navbar for medium and small device stop -->   
	
	
	
	
    <%--  <ul id="mobile-demo" class="side-nav hide-on-large-only sidenav-color z-depth-3">
       
        <li>
            <div class="user-view">
                <div class="background">
                <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left">Vaisansar</a>
                    <!--<a href="index.html"><img class="circle" src="resources/img/logo_small.png"></a>-->
                    <a href="#"><span class="white-text name">${employeeDetails.name}</span></a>
                </div>
            </div>
        </li>

		
        <li class="no-padding">
             <ul class="collapsible" data-collapsible="accordion">
             		<li style="margin-top:3%;margin-bottom:5%;"><a href="${pageContext.servletContext.contextPath}/" class="brand-logo  center" style="font-size:30px;">Vaisansar</a></li>    
                    <li><a href="${pageContext.servletContext.contextPath}/fetchSupplierList" class="collapsible-header">Manage Supplier<i class="material-icons black-text" aria-hidden="true">settings</i>
                        </a>
                    </li>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <li>
                        <a class="collapsible-header"><span class="element-sideNav">Product</span><i class="material-icons black-text" >local_grocery_store</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBrandList"><i class="material-icons black-text" >star</i>Manage Brand</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchCategoriesList"><i class="material-icons black-text">filter_list</i>Manage Category</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductList"><i class="material-icons black-text">add_shopping_cart</i>Manage Product</a></li>
                            </ul>
                        </div>
                    </li>
                     <li><a href="${pageContext.servletContext.contextPath}/location" class="collapsible-header ">Location<i class="material-icons black-text"  aria-hidden="true">edit_location</i>
                	</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeList" class="collapsible-header">HRM<i class="material-icons black-text"  aria-hidden="true">work</i>
                	</a></li>
                	 <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameList" class="collapsible-header">Business List<i class="material-icons black-text " aria-hidden="true">business_center</i>
                	</a></li>
                	 <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header">Manage Inventory<i class="material-icons black-text " aria-hidden="true">build</i>
                	</a></li>
                	
                	<li>
                        <a class="collapsible-header ">Reports<i class="material-icons black-text ">equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down black-text "  aria-hidden="true"></i>Product Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="material-icons black-text " >people</i>Business Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons black-text ">person</i>Salesman Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons black-text " >view_module</i>Inventory Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons black-text " >assignment</i>Order Report</a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="material-icons black-text " >assignment_returned</i>Return Item Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today">Collection Report<i class="material-icons black-text "  aria-hidden="true">collections_bookmark</i>
                	</a></li>	
                				<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport">GST Report<i class="material-icons black-text "  aria-hidden="true">collections_bookmark</i>
                				</a></li>
                            </ul>
                        </div>
                    </li>
                	<li>
                        <a class="collapsible-header ">Gatekeeper<i class="material-icons black-text">assignment_ind</i></a>
                        <div class="collapsible-body">
                            <ul>
                                 <li><a href="${pageContext.servletContext.contextPath}/GKOrderDetailsTodayList?areaId=0">Gatekeeper Orderlist<i class="material-icons black-text " aria-hidden="true">format_list_bulleted</i>
                		</a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb"><i class="fa fa-cart-arrow-down black-text " aria-hidden="true"></i>Issue Reports</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderReportForWeb"><i class="material-icons black-text " >assignment_returned</i>Return Item Report</a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchReplacementOrderReportForWeb"><i class="material-icons black-text " >autorenew</i>Replacement Report</a></li>
                                
                            </ul>
                        </div>
                    </li>
                	
                	
                    <li><a href="${pageContext.servletContext.contextPath}/fetchPaymentPendingList" class="collapsible-header ">Payment<i class="material-icons black-text"  aria-hidden="true">payment</i>
                	</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=currentMonth" class="collapsible-header ">Ledger<i class="material-icons black-text"  aria-hidden="true">assessment</i>
                	</a></li>
                   
                    
                   
                    <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header">Complains<i class="material-icons black-text " aria-hidden="true">feedback</i>
                	</a></li>
                         <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header">Current Location<i class="material-icons black-text" aria-hidden="true">location_on</i>
                	</a></li>
                    
					 <li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header">Get Invoice<i class="material-icons black-text"  aria-hidden="true">receipt</i>
                	</a></li>
            
                </ul>
        </li>
    </ul> --%>

<!-- navbar for large screen start -->
<!-- navbar for admin start -->
<c:if test="${sessionScope.loginType=='Admin'}">
    <div class="col s4 l4 m4">
        <ul id="slide-out" class="side-nav fixed hide-on-med-and-down sidenav-color z-depth-3" style="margin-top:0;">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="left element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <a id="slide" href="#" class=""><i class="material-icons small tooltipped" data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a>
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                    <li><a href="${pageContext.servletContext.contextPath}/fetchAllCompany" class="collapsible-header"><span class="element-sideNav">Manage Companies</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Companies" aria-hidden="true">settings</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/location_admin" class="collapsible-header"><span class="element-sideNav">Location</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header"><span class="element-sideNav">Inventory Details</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Inventory Details" aria-hidden="true">build</i></a></li>
                	<li>
                        <a class="collapsible-header "><span class="element-sideNav">Reports<i class="material-icons right chevron">expand_more</i></span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Counter Report">insert_drive_file</i><span class="element-sideNav">Counter Report</span></a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Payment Report">account_balance_wallet</i><span class="element-sideNav">Payment Report</span></a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Cheque Report">subtitles</i><span class="element-sideNav">Cheque Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down tooltipped" data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i><span class="element-sideNav">Product Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Business Report">people</i><span class="element-sideNav">Business Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i><span class="element-sideNav">Salesman Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Inventory Report">view_module</i><span class="element-sideNav">Inventory Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Supplier Orders">assignment</i><span class="element-sideNav">Supplier Orders</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Order Report">assignment</i><span class="element-sideNav">Order Report</span></a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i><span class="element-sideNav">Return Item Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Damage Report">broken_image</i><span class="element-sideNav">Damage Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><span class="element-sideNav">Collection Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true">collections_bookmark</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><span class="element-sideNav">Sales Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true">attach_money</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><span class="element-sideNav">Cancel Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true">clear</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport"><span class="element-sideNav">GST Report</span><i class="fa fa-inr tooltipped" data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="padding-left:3%"></i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/profit-loss"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Profit & Loss">show_chart</i><span class="element-sideNav">Profit & Loss</span></a></li>
                            </ul>
                        </div>
                    </li>                	
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header"><span class="element-sideNav">Monitoring</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Current Location" aria-hidden="true">location_on</i></a></li>            
                </ul>
            </li>
        </ul>
    </div>
</c:if>
<!-- navbar for admin end -->

<!-- navbar for company admin start -->
<c:if test="${sessionScope.loginType=='CompanyAdmin'}">
   <!-- fixed side nav on all page -->
    <div class="col s4 l4 m4">
        <ul id="slide-out" class="side-nav fixed hide-on-med-and-down sidenav-color z-depth-3" style="margin-top:0;">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="left element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <a id="slide" href="#" class=""><i class="material-icons small tooltipped" data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a>
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                	<%-- <li><a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left">Vaisansar<i class="material-icons black-text" aria-hidden="true">menu</i>
                        </a>
                	</li>  --%>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchSupplierList" class="collapsible-header"><span class="element-sideNav">Manage Supplier</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Supplier" aria-hidden="true">settings</i>
                        </a>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/location_company" class="collapsible-header"><span class="element-sideNav">Location</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Location" aria-hidden="true">edit_location</i></a></li>
                    <li>
                        <a class="collapsible-header"><span class="element-sideNav">Product<i class="material-icons right chevron">expand_more</i></span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Product">local_grocery_store</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBrandList"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Brand">star</i><span class="element-sideNav">Manage Brand</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchCategoriesList"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Category">filter_list</i><span class="element-sideNav">Manage Category</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductList"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Product">add_shopping_cart</i><span class="element-sideNav">Manage Product</span></a></li>
                            </ul>
                        </div>
                    </li>
	                <li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeList" class="collapsible-header"><span class="element-sideNav">HRM</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="HRM" aria-hidden="true">work</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameList" class="collapsible-header"><span class="element-sideNav">Business List</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Business list" aria-hidden="true">business_center</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header"><span class="element-sideNav">Manage Inventory</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Inventory" aria-hidden="true">build</i></a></li>
	              	<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header"><span class="element-sideNav">Manage Expense</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Expense" aria-hidden="true">settings</i>
                        </a>
                    </li>
                	<li>
                        <a class="collapsible-header "><span class="element-sideNav">Reports<i class="material-icons right chevron">expand_more</i></span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Counter Report">insert_drive_file</i><span class="element-sideNav">Counter Report</span></a></li>
                            	<li><a href="${pageContext.servletContext.contextPath}/paymentReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Payment Report">account_balance_wallet</i><span class="element-sideNav">Payment Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/chequeReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Cheque Report">subtitles</i><span class="element-sideNav">Cheque Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down tooltipped" data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i><span class="element-sideNav">Product Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Business Report">people</i><span class="element-sideNav">Business Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i><span class="element-sideNav">Salesman Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Inventory Report">view_module</i><span class="element-sideNav">Inventory Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Supplier Orders">assignment</i><span class="element-sideNav">Supplier Orders</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Order Report">assignment</i><span class="element-sideNav">Order Report</span></a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i><span class="element-sideNav">Return Item Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Report">broken_image</i>Damage Report</a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><span class="element-sideNav">Collection Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true">collections_bookmark</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><span class="element-sideNav">Sales Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true">attach_money</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><span class="element-sideNav">Cancel Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true">clear</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport"><span class="element-sideNav">GST Report</span><i class="fa fa-inr tooltipped" data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="padding-left:3%"></i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/profit-loss"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Profit & Loss">show_chart</i><span class="element-sideNav">Profit & Loss</span></a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchPaymentPendingList" class="collapsible-header "><span class="element-sideNav">Payment</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Payment" aria-hidden="true">payment</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchLedgerPaymentView?range=currentMonth" class="collapsible-header "><span class="element-sideNav">Ledger</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Ledger" aria-hidden="true">assessment</i></a></li>
                    <%-- <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header"><span class="element-sideNav">Complains</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Complains" aria-hidden="true">feedback</i></a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header"><span class="element-sideNav">Get Invoice</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Get Invoice" aria-hidden="true">receipt</i></a></li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header"><span class="element-sideNav">Monitoring</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Current Location" aria-hidden="true">location_on</i></a></li>            
                </ul>
            </li>
        </ul>
    </div>    
    <!--navbar end-->
</c:if>
<!-- navbar for company admin end -->    	

<!-- navbar for gatekeeper start -->    
<c:if test="${sessionScope.loginType=='GateKeeper'}">
<!-- fixed side nav on all page -->

    <div class="col s4 l4 m4">
        <ul id="slide-out" class="side-nav fixed hide-on-med-and-down sidenav-color z-depth-3" style="margin-top:0;">
            <!--height:65px; width:300px;-->
            <li style="margin-top:1%;margin-bottom:5%;">
            
                 <a href="${pageContext.servletContext.contextPath}/" class="left element-sideNav" style="font-size:30px;">Vaisansar</a>
                 <a id="slide" href="#" class=""><i class="material-icons small tooltipped" data-position="right" data-delay="50" data-tooltip="Menu">menu</i></a>
                   <%-- <a href="${pageContext.servletContext.contextPath}/" class="brand-logo  left"></a><a id="slide" class=""><i class="material-icons small">menu</i></a> --%>
            
            </li>
            <li class="no-padding">
                <ul class="collapsible" data-collapsible="accordion">
                	<li><a href="${pageContext.servletContext.contextPath}/openCounter" class="collapsible-header"><span class="element-sideNav">Open Counter</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Open Counter" aria-hidden="true">dvr</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchProductListForInventory" class="collapsible-header"><span class="element-sideNav">Manage Inventory</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Inventory" aria-hidden="true">build</i></a></li>
                	
                	<li><a href="${pageContext.servletContext.contextPath}/fetchEmployeeListForGkView" class="collapsible-header"><span class="element-sideNav">Employee Details</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Employee Details" aria-hidden="true">work</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/GKOrderDetailsTodayList?areaId=0"><span class="element-sideNav">Booked Order List</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Booked Order List" aria-hidden="true">format_list_bulleted</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchOrderIssueReportForWeb" class="collapsible-header"><span class="element-sideNav">Get Invoice</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Get Invoice" aria-hidden="true">receipt</i></a></li>
                	<li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth" class="collapsible-header"><span class="element-sideNav">Manage Expense</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Manage Expense" aria-hidden="true">settings</i>
                        </a>
                    </li>
                	<%-- <li>
                        <a class="collapsible-header "><span class="element-sideNav">Reports<i class="material-icons right chevron">expand_more</i></span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Reports" >equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth"><i class="fa fa-cart-arrow-down tooltipped" data-position="right" data-delay="50" data-tooltip="Product Report" aria-hidden="true"></i><span class="element-sideNav">Product Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchBusinessNameForReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Business Report">people</i><span class="element-sideNav">Business Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Salesman Report">person</i><span class="element-sideNav">Salesman Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Inventory Report">view_module</i><span class="element-sideNav">Inventory Report</span></a></li>
                                <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Order Report">assignment</i><span class="element-sideNav">Order Report</span></a></li>   
								<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i><span class="element-sideNav">Return Item Report</span></a></li>
                    			<li><a href="${pageContext.servletContext.contextPath}/getCollectionReportDetails?range=today"><span class="element-sideNav">Collection Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Collection Report" aria-hidden="true">collections_bookmark</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><span class="element-sideNav">Sales Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Sales Report" aria-hidden="true">attach_money</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><span class="element-sideNav">Cancel Report</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Cancel Report" aria-hidden="true">clear</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today"><span class="element-sideNav">DeliveryBoy Return</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Return from Delivery " aria-hidden="true">file_download</i></a></li>
		                		<% Calendar cal=Calendar.getInstance();%>	
		                		<li><a href="${pageContext.servletContext.contextPath}/damageRecoveryList?startMonth=<%=cal.get(Calendar.MONTH)+1%>&startYear=<%=cal.get(Calendar.YEAR)%>&endMonth=<%=cal.get(Calendar.MONTH)+1%>&endYear=<%=cal.get(Calendar.YEAR)%>"><span class="element-sideNav">Damage Recovery</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Damage Recovery" aria-hidden="true">restore_page</i></a></li>
		                		<li><a href="${pageContext.servletContext.contextPath}/fetchGstBillReport"><span class="element-sideNav">GST Report</span><i class="fa fa-inr tooltipped" data-position="right" data-delay="50" data-tooltip="GST Report" aria-hidden="true" style="padding-left:3%"></i></a></li>
                            </ul>
                        </div>
                    </li> --%>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderFromDeliveryBoyReport?range=today"><span class="element-sideNav">DeliveryBoy Return</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Return from Delivery " aria-hidden="true">file_download</i></a></li>
              		<% Calendar cal=Calendar.getInstance();%>	
              		<li><a href="${pageContext.servletContext.contextPath}/damageRecoveryList?startMonth=<%=cal.get(Calendar.MONTH)+1%>&startYear=<%=cal.get(Calendar.YEAR)%>&endMonth=<%=cal.get(Calendar.MONTH)+1%>&endYear=<%=cal.get(Calendar.YEAR)%>"><span class="element-sideNav">Damage Recovery</span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Damage Recovery" aria-hidden="true">restore_page</i></a></li>
                	<%-- <li><a href="${pageContext.servletContext.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" class="collapsible-header"><span class="element-sideNav">Complains</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Complains" aria-hidden="true">feedback</i></a></li> --%>
                    <li>
                        <a class="collapsible-header"><span class="element-sideNav">Reports <i class="material-icons right chevron">expand_more</i></span><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Reports">equalizer</i></a>
                        <div class="collapsible-body">
                            <ul>
                            
                            	<li><a href="${pageContext.servletContext.contextPath}/counterOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Counter Report">insert_drive_file</i><span class="element-sideNav">Counter Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchInventoryReportView?range=currentMonth&supplierId="><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Inventory Report">view_module</i><span class="element-sideNav">Inventory Report</span></a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/showSupplierOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Supplier Orders">assignment</i><span class="element-sideNav">Supplier Orders</span></a></li>
                				 <li><a href="${pageContext.servletContext.contextPath}/showOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Order Report">assignment</i><span class="element-sideNav">Order Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Sales Report">attach_money</i><span class="element-sideNav">Sales Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/cancelOrderReport?range=today"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Cancel Report" >clear</i><span class="element-sideNav">Cancel Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchReturnOrderReportForWeb"><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Return Item Report">assignment_returned</i><span class="element-sideNav">Return Item Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/fetchReplacementOrderReportForWeb"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Replacement Report">autorenew</i><span class="element-sideNav">Replacement Report</span></a></li>
                                 <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth"><i class="material-icons " data-position="right" data-delay="50" data-tooltip="Damage Report">broken_image</i>Damage Report</a></li>                                
                            </ul>
                        </div>
                    </li>
                    <li><a href="${pageContext.servletContext.contextPath}/findLocation" class="collapsible-header"><span class="element-sideNav">Monitoring</span><i class="material-icons  tooltipped" data-position="right" data-delay="50" data-tooltip="Current Location" aria-hidden="true">location_on</i></a></li>
                </ul>
            </li>
        </ul>
    </div>
    
    <!--navbar end-->
</c:if>
<!-- navbar for gatekeeper stop -->
<!-- navbar for large screen stop -->
    <!-- code for loader -->
   


    <div class="preloader-background">
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
         
        </div>
      </div>
      
    </div>
    <!--  <div class="red-text"><br><h6>Wait.....</h6></div> -->
     </div>