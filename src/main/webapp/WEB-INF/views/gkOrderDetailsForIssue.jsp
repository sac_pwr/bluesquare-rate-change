<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
	<script type="text/javascript">
	
	/* function productListQtyMngFunc(id,qty,isAdd){
		for(var i=0; i<productListQtyMng.length; i++){
			if(productListQtyMng[i].productId==id){
				var productListQtyMngTemp=productListQtyMng[i];
				productListQtyMng.splice(i, 1);
				if(isAdd){
					productListQtyMngTemp.currentQuantity+=parseInt(qty);
				}else{
					productListQtyMngTemp.currentQuantity-=parseInt(qty);
				}
				productListQtyMng.push(productListQtyMngTemp);
				break;
			}
		}
		refreshAllowedQuantityOfProducts();
	}
	function refreshAllowedQuantityOfProducts(){			
		for(var i=0; i<productListQtyMng.length; i++){
			$('.current_qty_'+productListQtyMng[i].productId).text(productListQtyMng[i].currentQuantity);
		}
	} 
	function productListData(){
		$.ajax({
			type:"GET",
			url : myContextPath+"/fetchProductListAjax",
			dataType : "json",
			async : false,
			success:function(data)
			{
				productListQtyMng=data;
				//console.log("product data list loaded");
			},
			error: function(xhr, status, error) {
				//console.log("product data list not loaded");
			}
		});
	} */

    $(document).ready(function() {
    	
    	<c:forEach var="listValue" items="${orderDetailResponse.orderProductDetailList}">
    	
    	$('#issuedQuantity${listValue.orderProductDetailsid}').keypress(function( event ){
    	    var key = event.which;
    	    
    	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
    	        event.preventDefault();
    	});
    	
    	$('#issuedQuantity${listValue.orderProductDetailsid}').keyup(function(){
    		
    		var issuedQuantity=$('#issuedQuantity${listValue.orderProductDetailsid}').val();
    		//alert(issuedQuantity);
    		if(issuedQuantity==="" || issuedQuantity===undefined){
    			issuedQuantity=0;
    		}
    		
    		if(parseInt(issuedQuantity) > parseInt($('#orderdQuantity${listValue.orderProductDetailsid}').text()))
    		{
    			Materialize.toast('${listValue.product.product.productName} Product Issued Quantity exceeds Order Quantity', 2000, 'teal lighten-2');
    			/*  $('#addeditmsg').modal('open');
	   		     $('#msgHead').text("Warning Message");
	   		     $('#msg').html("<font color='red'><b>${listValue.product.productName}</b> Product <b>Issued Quantity</b> exceed <b>Order Quantity</b></font>");
    		    setTimeout(function() {
    		    	$('#addeditmsg').modal('close');
    		    }, 2000); */
    		    $('#issuedQuantity${listValue.orderProductDetailsid}').val("0");
    		    //return false;
    		}
    		
    		if(parseInt(issuedQuantity)>parseInt($("#currrentQuantity_${listValue.type=='Free'?'Free' : 'NonFree'}_${listValue.orderProductDetailsid}").text()))
    		{
    			Materialize.toast('${listValue.product.product.productName} Product Issued Quantity exceeds Current Quantity', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		     $('#msgHead').text("Warning Message");
	   		     $('#msg').html("<font color='red'><b>${listValue.product.productName}</b> Product <b>Issued Quantity</b> exceed <b>Current Quantity</b></font>");
	   		    setTimeout(function() {
	   		    	$('#addeditmsg').modal('close');
	   		    }, 2000); */
	   		    $('#issuedQuantity${listValue.orderProductDetailsid}').val("0");
	   		    //return false;
    		}
			
			var issuedQuantity=$('#issuedQuantity${listValue.orderProductDetailsid}').val();
   		//alert(issuedQuantity);
			if(issuedQuantity==="" || issuedQuantity===undefined)
			{
    			issuedQuantity=0;
    		}
			
    	    var data = {
    	    		'issuedQuantity': issuedQuantity,
    	    		'orderProductDetailsid': "${listValue.orderProductDetailsid}"
    	    	};
    	    
    		$.ajax({
        		type:"POST",
    			url : "${pageContext.request.contextPath}/issueProductDetailsCalculate",
        		dataType : "json",
        		data : data,
        		success : function(data) {
        			orderProductDetailListNew=data;
        			var totalOrderQuantity=0;
        			var totalIssuedQuantity=0;
        			var totalAmount=0;
        			
        			for(var i=0; i<orderProductDetailListNew.length; i++)
        			{
        				if(orderProductDetailListNew[i].orderProductDetailsid=="${listValue.orderProductDetailsid}")
        				{
        					$('#amountWithTax${listValue.orderProductDetailsid}').text(parseFloat(orderProductDetailListNew[i].issueAmount).toFixed(2));
        				}
        				totalOrderQuantity=totalOrderQuantity+orderProductDetailListNew[i].purchaseQuantity;
        				totalIssuedQuantity=totalIssuedQuantity+orderProductDetailListNew[i].issuedQuantity;
        				totalAmount=totalAmount+orderProductDetailListNew[i].issueAmount;
        			}
        			
        			//$('#totalOrderQuantity').text(totalOrderQuantity);
        			$('#totalIssuedQuantity').text(parseInt(totalIssuedQuantity));
        			$('#totalOrderAmountWithTax').text(parseFloat(totalAmount).toFixed(2));
        			
        		}
			});
			productListData();
    	});
    	</c:forEach>
    	
    	$('#issueConfirmButtonId').click(function(){
    		
    		var deliveryBoyId=$('#deliveryBoyId').val();
    		var deliveryDateId=$('#deliveryDateId').val();		
    		
    		if(deliveryBoyId==="0" && deliveryDateId==="")
    		{
    			Materialize.toast('Select Delivery Boy And Delivery Date', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Boy And Delivery Date</font>"); */
	   		    return false;
    		}
    		else if(deliveryBoyId==="0")
    		{
    			Materialize.toast('Select Delivery Boy', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Boy</font>"); */
	   		    return false;
    		}
    		else if(deliveryDateId==="")
    		{
    			Materialize.toast('Select Delivery Date', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Date</font>"); */
	   		    return false;
    		}
    		
    		var deliveryDate=new Date(deliveryDateId).setHours(0,0,0,0);
    		var today=new Date().setHours(0,0,0,0);    
    		if(deliveryDate < today)
			{
    			Materialize.toast('Select Delivery Date in Current Date or After that date', 2000, 'teal lighten-2');
    			/* $('#addeditmsg').modal('open');
	   		    $('#msgHead').text("Warning Message");
	   		    $('#msg').html("<font color='red'>Select Delivery Date in Current Date or After that date</font>"); */
				return false;
			}    	
    		
    		 var data = {
     	    		'deliveryBoyId': deliveryBoyId,
     	    		'deliveryDate': deliveryDateId
     	    	};
     	    $("#issueConfirmButtonId").attr('disabled','disabled')
     		$.ajax({
         		type:"POST",
     			url : "${pageContext.request.contextPath}/packedOrderToDeliverBoyForWeb",
         		dataType : "json",
         		data : data,
         		beforeSend: function() {
					$('.preloader-background').show();
					$('.preloader-wrapper').show();
		           },
         		success : function(data) {
         			if(data.status==="Success")
         			{
         				Materialize.toast('Order SuccessFully Issued', 2000, 'teal lighten-2');
         				/* $('#addeditmsg').modal('open');
        	   		    $('#msgHead').text("Message");
        	   		    $('#msg').html("<font color='green'>Order SuccessFully Issued</font>");
        	   		 	*/
	        	   		setTimeout(function() 
	        	   		{
	     	   		    	$('#addeditmsg').modal('close');
	     	   		        window.location.href="${pageContext.request.contextPath}/openIssuedBill?orderId="+data.errorMsg;
	     	   		    }, 2000); 
         			}
         			else
         			{
         				 $('#addeditmsg').find("#modalType").addClass("warning");
         				$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("red lighten-2");
         				$('#addeditmsg').modal('open');
        	   		    /* $('#msgHead').text("Error Message"); */
        	   		    $('#msg').html("<font color='red'>"+data.status+"</font>");
        	   		 	$("#issueConfirmButtonId").removeAttr("disabled");
         			}
         			$('.preloader-background').hide();
					$('.preloader-wrapper').hide();
         		},error: function(xhr, status, error) {
					$('.preloader-wrapper').hide();
					$('.preloader-background').hide();
							
					Materialize.toast('Order Issuing Failed', 2000, 'teal lighten-2');
					
				}		
     		});
    	});
    	
    });
    </script>
    <style>
     .card-panel p
     {
     margin:5px	 !important;
     font-size:16px !important;
     color:black;
     /* border:1px solid #9e9e9e; */
     }
    .card-panel{
    	padding:8px !important;
    	border-radius:8px;
    }
    
    .leftHeader{
    	width:105px !important;
    	display:inline-block;
    }
    </style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
        	
        
        
        
        
            <%-- <div class="col s12 l6 m6">
                <div class="card grey lighten-3 hoverable z-depth-3">
                    <div class="card-content blue-text">
                        <p id="name" class="center-align blue-text">Order ID: <b><c:out value="${orderDetailResponse.orderDetailList.orderId}" /></b> </p>
                        <hr style="border:1px dashed teal; ">
                        <p id="department" class="center-align blue-text">Shop Name: <b><c:out value="${orderDetailResponse.orderDetailList.businessName.shopName}" /></b></p>
                        <hr style="border:1px dashed teal;"> 
                        <p id="area" class="center-align blue-text">Area: <b><c:out value="${orderDetailResponse.orderDetailList.businessName.area.name}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="MobileNo" class="center-align blue-text">Mobile No: <b><c:out value="${orderDetailResponse.orderDetailList.businessName.contact.mobileNumber}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="MobileNo" class="center-align blue-text">Salesman Name: <b><c:out value="${orderDetailResponse.salesPersonName}" /></b></p>
                        <hr style="border:1px dashed teal;">
                        <p id="Add" class="center-align blue-text">Date of Order: <b><c:out value="${orderDetailResponse.orderDetailList.orderDetailsAddedDatetime}" /></b></p>
                    </div>
                </div>
            </div> --%>
            
            
            
             <div class="col s12 l12 m12 card-panel hoverable blue-grey lighten-4">
      	
       			<div class="col s12 l2 m2 right right-align" style="padding:0">
                <a href="${pageContext.request.contextPath}/editOrder?orderId=${orderDetailResponse.orderDetailList.orderId}" id="editOrderId" class="btn waves-effect waves-light blue-gradient">Edit Order</a>
            </div>
                <div class="col s12 l5 m5">
                <p>
                 <span class="leftHeader">Order Id:</span>
                 <b><c:out value="${orderDetailResponse.orderDetailList.orderId}" /></b>
               		 <!--  <hr style="border:1px dashed teal;"> -->
                </p>
                </div>
                     <div class="col s12 l5 m5">
                       <p id="Add"><span class="leftHeader">Date of Order:</span> 
                       <b>
                       <c:out value="${orderDetailResponse.orderDetailList.orderDetailsAddedDatetime}" />
                       </b>
                       </p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l5 m5">
                   <p id="department"><span class="leftHeader">Shop Name: </span><b><c:out value="${orderDetailResponse.orderDetailList.businessName.shopName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
               		  
                </div>
                 <div class="col s12 l5 m5">
                   <p><span class="leftHeader">Salesman: </span><b><c:out value="${orderDetailResponse.salesPersonName}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                <div class="col s12 l5 m5">
                    <p><span class="leftHeader">Mobile No: </span><b><c:out value="${orderDetailResponse.orderDetailList.businessName.contact.mobileNumber}" /></b></p>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
                
                <div class="col s12 l5 m5">
                   <div class="col s12 m2 l2 leftHeader" style="padding:0"><p id="area"><span>Area:</span></p></div>
                 <div class="col s12 m8 l8" style="word-wrap:break-word;padding:0;"><p><b><c:out value="${orderDetailResponse.orderDetailList.businessName.area.name}" /></b></p></div>
                
                
                  <%--  <p id="area"><span class="leftHeader">Area:</span> <b><c:out value="${orderDetailResponse.orderDetailList.businessName.area.name}" /></b></p> --%>
               		  <!-- <hr style="border:1px dashed teal;"> -->
                </div>
           		
           </div> 
           <div class="col s12 l3 m3 offset-l2">
                <select id="deliveryBoyId">
			      <option value="0"  selected>Delivery Person</option>
			      	  <c:if test="${not empty orderDetailResponse.employeeNameAndIdSMAndDBList}">
							<c:forEach var="listValue" items="${orderDetailResponse.employeeNameAndIdSMAndDBList}">
								<option value="<c:out value="${listValue.employeeId}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
					  </c:if>
			    </select>
            </div>

            <div class="col s12 l2 m2">
                <input type="text" class="datepicker disableDate" placeholder="Choose Date" id="deliveryDateId" id="Date">
            </div>
            <div class="col s12 l2 m2">
                <button type="button" id="issueConfirmButtonId" class="btn waves-effect waves-light blue-gradient" style="margin-top:7%">Confirm</button>
            </div>
            
        </div>
        <div class="row">
            <div class="col s12 l12 m12" style="padding:0">
                <table class="striped highlight tblborder centered" id="tblData1" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                        	<th class="print-col">Sr. No.</th>
                            <th class="print-col">Product Name</th>
                            <th class="print-col">Current Qty</th>
                            <th class="print-col">Ordered Qty</th>
                            <th class="print-col">Issue Qty</th>
                            <th class="print-col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<% int rowincrement=0; %>
                    	<c:if test="${not empty orderDetailResponse.orderProductDetailList}">
						<c:forEach var="listValue" items="${orderDetailResponse.orderProductDetailList}">
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td class="wrapok"><c:out value="${listValue.product.product.productName}" />
                            	<font color="green"><c:out value="${listValue.type=='Free'?'-(Free)' : ''}" /></font>
                            </td>
                            <td>
                            <span id="currrentQuantity_${listValue.type=='Free'?'Free' : 'NonFree'}_${listValue.orderProductDetailsid}"><c:out value="${listValue.product.product.currentQuantity}" /></span>
                            </td>
                            <td>
                            <span id="orderdQuantity${listValue.orderProductDetailsid}"><c:out value="${listValue.purchaseQuantity}" /></span>
                            <c:set var="totalOrderQuantity" value="${totalOrderQuantity + listValue.purchaseQuantity}" scope="page"/>
                            </td>
                            <td>
	                            <input style="text-align:center;" type="text" value="<c:out value="${listValue.purchaseQuantity}" />" id="issuedQuantity${listValue.orderProductDetailsid}">
	                         
	                            <c:set var="totalIssuedQuantity" value="${totalIssuedQuantity + listValue.purchaseQuantity}" scope="page"/>
                            </td>
                           <td>
	                            <span id="amountWithTax${listValue.orderProductDetailsid}"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.purchaseQuantity * listValue.sellingRate}" /></span>
	                            <c:set var="totalOrderAmountWithTax" value="${totalOrderAmountWithTax + (listValue.purchaseQuantity * listValue.sellingRate)}" scope="page"/>
                           </td>
                        </tr>
                        </c:forEach>
                        </c:if>                        
                    </tbody>
                    <tbody>
                   		<tr>
                            <td colspan="3"><b>Total</b></td>
                            <td><span id="totalOrderQuantity"><c:out value="${totalOrderQuantity}" /></span></td>
                            <td><span id="totalIssuedQuantity"><c:out value="${totalIssuedQuantity}" /></span></td>
                            <td><span id="totalOrderAmountWithTax"><c:out value="${totalOrderAmountWithTax}" /></span></td>
                        </tr>
                    </tbody>
                   </table>
            </div>
        </div>
        
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
        
      
    </main>
    <!--content end-->
</body>

</html>