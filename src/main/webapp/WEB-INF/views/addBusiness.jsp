<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	

<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}"
$(document).ready(function() {
	$('#gstNo').keypress(function( event ) {  
		if(!/[0-9a-zA-Z-]/.test(String.fromCharCode(event.which)))
			return false;
	});
	
	$.validator.setDefaults({
	       ignore: []
	}); 
	//$('select').change(function(){ $('select').valid(); });
	jQuery.validator.addMethod("gstCheck", function(value, element){
   		if(value=='' || value==undefined)
   		{
   			return true;
   		}		
   	    return checkBusinessDuplication(value,"gstNo");
   	}, "gst no. Is already in use"); 
	jQuery.validator.addMethod("telephoneNumberCheck", function(value, element){
		if(value=='' || value==undefined)
   		{
   			return true;
   		}		
   	    return checkBusinessDuplication(value,"telephoneNumber");
   	}, "Telephone Number Is already in use"); 
	jQuery.validator.addMethod("emailIdCheck", function(value, element){
		if(value=='' || value==undefined)
   		{
   			return true;
   		}		
   	    return checkBusinessDuplication(value,"emailId");
   	}, "Email Id Is already in use"); 
   	jQuery.validator.addMethod("mobileNumberCheck", function(value, element){
   		if(value=='' || value==undefined)
   		{
   			return true;
   		}		
   	    return checkBusinessDuplication(value,"mobileNumber");
   	}, "Mobile Number Is already in use");
	
	$('#saveBusinessForm').validate({
	    
		rules: {    		    
			telephoneNumber:{
				telephoneNumberCheck:true
            },
            emailId:{
            	emailIdCheck:true
            },
            mobileNumber:{
            	mobileNumberCheck:true,
            	required:true
            },
            gstinNumber:{
            	gstCheck:true
            }
		  },
		
		errorElement : "span",
	    errorClass : "invalid error",
	    errorPlacement : function(error, element) {
	      var placement = $(element).data('error');
	    
	      if (placement) {
	        $(placement).append(error)
	      } else {
	        error.insertAfter(element);
	      }
	      $('select').change(function(){ $('select').valid(); });
	    }
	  });
	 

	
	$('#creditLimit').keydown(function(e){            	
		-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
	 });
    
	document.getElementById('creditLimit').onkeypress=function(e){
    	
    	if (e.keyCode === 46 && this.value.split('.').length === 2) {
      		 return false;
  		 }

    }
	
	$('#mobileNo').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	}); 
	 $('#telephoneNo').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
		        event.preventDefault();
		}); 
	
	
	
	
	
	var msg="${saveMsg}";
	 //alert(msg);
	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
			$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	     /* $('#msgHead').text("Business Message"); */
	     $('#msg').text(msg);
	 }
	

	
	
	$('#cityList').change(function() {
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('regionList');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options

		select.options.add(new Option("Choose Region", ''));
		$.ajax({
			url : myContextPath+"/fetchRegionListByCityId?cityId="+$('#cityList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {

				/* alert(data); */
				var options, index, option;
				select = document.getElementById('regionList');

				for (var i = 0, len = data.length; i < len; ++i) {
					var region = data[i];
					//alert(state.name+" "+ state.stateId);
					select.options.add(new Option(region.name, region.regionId));
				}
				
				/* for (index = 0; index < options.length; ++index) {
				  option = options[index];
				  select.options.add(new Option(option.name, option.cityId));
				} */
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('Region List Not Found!', '2000', 'teal lighten-2');
				  //alert(error +"---"+ xhr+"---"+status);
				/* $('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Region List Not Found"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000); */
				}
		});		
		
	});
	
	$('#regionList').change(function() {
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('areaList');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options

		select.options.add(new Option("Choose Area", ''));
		$.ajax({
			url : myContextPath+"/fetchAreaListByRegionId?regionId="+$('#regionList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {

				/* alert(data); */
				var options, index, option;
				select = document.getElementById('areaList');

				for (var i = 0, len = data.length; i < len; ++i) {
					var area = data[i];
					//alert(state.name+" "+ state.stateId);
					select.options.add(new Option(area.name, area.areaId));
				}
				
				/* for (index = 0; index < options.length; ++index) {
				  option = options[index];
				  select.options.add(new Option(option.name, option.cityId));
				} */
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('Area List Not Found!', '2000', 'teal lighten-2');
				  //alert(error +"---"+ xhr+"---"+status);
				/* $('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Area List Not Found"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000); */
				}
		});		
		
	});
	
$('#areaList').change(function() {
		
		$.ajax({
			url : myContextPath+"/fetchArea?areaId="+$('#areaList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				$('#pincode').val(data.pincode);
				$('#pincode').focus();
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				  //alert(error +"---"+ xhr+"---"+status);
				/* $('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Area Details Not Found"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000); */
				}
		});		
		
	});

	/* $("#test").click(function(){
		  $('#addeditmsg').modal('open');
		     $('#msgHead').text("Business Message");
		     $('#msg').text("Please Select Business Type to Save Business");
		     return false;
		
	})
	 */
});

function checkBusinessDuplication(checkText,type){
	var status=false;
	$.ajax({
		url : "${pageContext.servletContext.contextPath}/checkBusinessDuplicationForSave?checkText="+checkText+"&type="+type,
		async:false,
		success : function(data) {
			if(data==="Success"){
				status=true;
			}else{
				status=false;
			}
		},
		error: function(xhr, status, error) {
			alert("Error");
		}
	});
	
	return status;
}
</script>

<style> 
	#addeditmsg{
		border-radius:10px;
		 width:30% !important; 
	}
	
	#toast-container {
    top: 35%;
    right: 35%;
    max-width: 86%;
}
i span{
	font-size:15px !important;
	padding-left:4%;
}
.select-wrapper span.error{
	margin-left:0 !important;
}		
	
	/* .modal{
		width:30% !important;
	} */
	
	
</style>
</head>

<body >
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
        	 <!-- <button class="btn waves-effect waves-light blue-gradient" id="test">Text</button> -->
            <form action="saveBusiness" method="post" id="saveBusinessForm">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Business Details </h4>
                    </div>
                    <div class="row">
         
                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">store</i>
                        <input id="name" type="text" class="validate" name="shopName" required>
                        <label for="name" class="active"><span class="red-text">*</span>Shop Name</label>

                    </div>
                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">person</i>
                        <input id="ownername" type="text" class="validate" name="ownerName" required>
                        <label for="ownername" class="active"><span class="red-text">*</span>Owner Name</label>
                    </div>
                    </div>
                     <div class="row">
                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
                      
                        <i class="material-icons prefix">work<span class="red-text">*</span></i>
                        <!-- <label for="businessType" class="active"><span class="red-text">*</span></label> -->
                        <select name="businessTypeId" id="businessType" class="validate" required="" aria-required="true" title="Please select Business Type">
                                 <option value="" selected disabled>Business Type</option>
                                <c:if test="${not empty businessTypeList}">
							<c:forEach var="listValue" items="${businessTypeList}">
								<option value="<c:out value="${listValue.businessTypeId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div>
                    </div>
                </div>
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Contact Details </h4>
                    </div>
                     <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="fa fa-volume-control-phone prefix" aria-hidden="true"></i>
                        <input id="telephoneNo" type="tel" name="telephoneNumber" minlength="10" maxlength="11">
                        <label for="telephoneNo" class="active">Telephone No.</label>
                    </div>

                      <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">stay_current_portrait<span class="red-text">*</span></i>
                        <input id="mobileNo" type="tel" name="mobileNumber" minlength="10" maxlength="10">
                        <label for="mobileNo" class="active">Mobile No.</label>
                    </div>

                  <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">mail</i>
                        <input id="emailId" type="email" name="emailId">
                        <label for="emailId" class="active">Email Id</label>
                    </div>
                     
                    <!--<div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                        <i class="material-icons prefix">location_on</i>
                        <textarea id="textarea1" class="materialize-textarea"></textarea>
                        <label for="textarea1">Textarea</label>
                    </div>-->
                </div>
                <div class="row z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Address </h4>
                    </div>
                    
                     <!-- <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">store</i>
                        <textarea id="line1" class="materialize-textarea" name="address" required></textarea>
                        <textarea id="line1" class="validate" name="address" class="materialize-textarea" required></textarea>
                        <label for="line1" class="active">Address</label>

                    </div> -->
                    <!--  <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">account_balance</i>
                        <input id="line2" type="text" class="validate" name="line2" required>
                        <label for="line2" class="active">Line 2</label>

                    </div>
                     <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">filter_hdr</i>
                        <input id="landmark" type="text" class="validate" name="landmark" required>
                        <label for="landmark" class="active">Landmark</label>

                    </div> -->
                     

                   <%--  <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on</i>
                        <select name="countryid">
                                 <option value="0" selected>Country</option>
                                <c:if test="${not empty countrylists}">
							<c:forEach var="listValue" items="${countrylists}">
								<option value="<c:out value="${listValue.countryId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                        </select>
                    </div> --%>
                    <div class="row">
                    <div class="input-field col s12 m10 l10 push-l1 push-m1">
                        <i class="material-icons prefix">store</i>
                        <input id="line1" type="text" name="address" required>
                       <!--  <textarea id="line1" class="materialize-textarea" name="address" required></textarea> -->
                       <!--  <textarea id="line1" class="validate" name="address" class="materialize-textarea" required></textarea> -->
                        <label for="line1" class="active"><span class="red-text">*</span>Address</label>

                    </div>
                   <%--  <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on<span class="red-text">*</span></i>
                        <div style="display:none;">
                        <select name="countryid" id="countryList" >
                                 <option value="0" selected>Country</option>
                                <c:if test="${not empty coutryList}">
								<c:forEach var="listValue" items="${coutryList}">
								<c:set var="indiaset" scope="session" value="India" />
									<option value="<c:out value="${listValue.countryId}" />" <c:if test="${listValue.name == indiaset}">selected="selected"</c:if>><c:out
											value="${listValue.name}" /></option>
								</c:forEach>
							</c:if>
                        </select>
                        </div>
                         <!-- <label for="stateList" class="active"><span class="red-text">*</span></label> -->
                        <select name="stateId" id="stateList" class="validate" required="" aria-required="true">
                                 <option value=""  selected disabled>State</option>
                                 <c:if test="${not empty stateList}">
									<c:forEach var="listValue" items="${stateList}">
										<option value="<c:out value="${listValue.stateId}" />"><c:out
												value="${listValue.name}" /></option>
									</c:forEach>
								</c:if> 
                        </select>
                    </div> --%>
                  
                    </div>
                    <div class="row">
                      <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">location_on<span class="red-text">*</span></i>
                       <!--  <label for="cityList" class="active"><span class="red-text">*</span></label> -->
                        <select name="cityid" id="cityList" class="validate" required aria-required="true">    
                                 <option  value="" selected disabled>City</option>
                                 <c:if test="${not empty cityList}">
									<c:forEach var="listValue" items="${cityList}">
										<option value="<c:out value="${listValue.cityId}" />"><c:out
												value="${listValue.name}" /></option>
									</c:forEach>
								</c:if> 
                        </select>
                    </div>
                    
                    <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">track_changes<span class="red-text">*</span></i>
                       <!--  <label for="regionList" class="active"><span class="red-text">*</span></label> -->
                        <select name="regionId" id="regionList" class="validate" required aria-required="true">
                                 <option value="" selected disabled>Region</option>
                        </select>
                    </div>
                    
                    
                      </div>
                        <div class="row">
                        <div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">business<span class="red-text">*</span></i>
                        <!-- <label for="areaList" class="active"><span class="red-text">*</span></label> -->
                        <select name="areaId"  id="areaList" class="active" class="validate" required="" aria-required="true">
                                <option value="" selected disabled>Area</option>
                        </select>
                    </div>
					<div class="input-field col s12 m5 l5 push-l1 push-m1 ">
                        <i class="material-icons prefix">gps_fixed</i>
                        <input id="pincode" type="text" class="grey lighten-3" name="pincode" readonly>
                        <label for="pincode" class="active">pincode</label>

                    </div>
                    </div>
                </div>


                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center">Other Details </h4>
                    </div>
                   <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">rate_review</i>
                        <input id="gstNo" type="text" name="gstinNumber" minlength="15" maxlength="15" >
                        <label for="gstNo" class="active">Enter GST No.</label>

                    </div>

                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
                        <i class="material-icons prefix">credit_card</i>

                        <input id="creditLimit" type="text" name="creditLimit">
                        <label for="creditLimit" class="active">Enter Credit Limit</label>

                    </div>
                </div>
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue-gradient" type="submit" id="saveBusinessSubmit">Add Business<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>

        </div>

		
			 <!-- <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
		<!-- <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="red lighten-2 center" style="padding:3% 0 3% 0">
						
						<i class="material-icons medium white-text">info_outline</i>
												
					</div>
					<h5 id="msgHead" class="red-text"></h5>
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div> -->

	<!-- 	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center success  white-text" style="padding:3% 0 3% 0">
						
						<i class="success medium white-text"></i>
												
					</div>
					<h5 id="msgHead" class="red-text"></h5>
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div> -->

		<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
		

    </main>
    <!--content end-->
</body>

</html>