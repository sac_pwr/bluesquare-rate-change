<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>

    <script>
        $(document).ready(function() {
        	/*  var dateDisable = $('.datepicker').pickadate();
        	var picker = dateDisable.pickadate('picker');
        	   var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
        	   picker.set( 'disable', { from: [0,0,0], to: yesterday } ); */
        	  
        	   
            	 
        
            $('.chequeNo').hide();
        	
            $('#cash').click(function() {
                $('.chequeNo').hide();
                $('.amount').addClass('offset-l4 offset-m4');
            });
            $('#cheque').click(function() {
            	 $('.amount').removeClass('offset-l4 offset-m4');
                $('.chequeNo').show();
               
            });
            $('#cheqNo').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});

           
            
            $('#amount').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('amount').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }	
            $('#paySubmit').click(function(){
            	var payCash=$('#cash').is(':checked');
    			var payCheque=$('#cheque').is(':checked');
    			
    			var amount1=$('#amount').val();
    			var amount=amount1.trim();
				var bankName1=$('#bankName').val().trim();
				var bankName=bankName1.trim();
				var cheqNo1=$('#cheqNo').val();
				var cheqNo=cheqNo1.trim();
				var cheqDate=$('#cheqDate').val();
				var paymentcomment=$('#comment').val();
				
				var type=$('#typeId').val();
				
				if(payCheque==true)
				{
					if(bankName===""){
						
						Materialize.toast('Please Enter Bank Name!', '4000', 'teal lighten-3');
						return false;
					}
					if(cheqNo==="")
					{
						Materialize.toast('Please Enter cheque number!', '4000', 'teal lighten-3');
						/* $('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Enter bank name"); */
						return false;
				     }
					
					if(cheqDate==="")
					{
						Materialize.toast('Please Enter cheque date!', '4000', 'teal lighten-3');
						/* $('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Enter cheque date"); */
						return false;
					}
					
					var today=new Date().setHours(0,0,0,0);
					var chDate=new Date(cheqDate).setHours(0,0,0,0);
					
					if(chDate < today)
					{
						Materialize.toast('Select Cheque Date After Current Date!', '4000', 'teal lighten-3');
						 /* $('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message"); 
					     $('#msg').text("Select Cheque Date After Current Date"); */
						return false;
					}
					
				}
				else
				{
					$('#bankName').val('');
					$('#cheqDate').val('');
				}
				 <c:if test="${paymentDoInfo.type =='supplier' || paymentDoInfo.type =='counter'}">  
				    var unPaid="${paymentDoInfo.amountUnPaid}";
				    
				    <c:if test="${forEdit=='forSupplierPaymentEdit'}">
				    	var maxPay=parseFloat("${paymentDoInfo.paidAmount}")+parseFloat("${paymentDoInfo.amountUnPaid}");
				    	unPaid=maxPay;
				    </c:if>
				    				    
				    if(parseFloat(amount)>parseFloat(unPaid))
				    {
				    	Materialize.toast('Entered amount must be same or less than Balance Amount!', '4000', 'red lighten-3');
				    	/* $('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Entered amount must be same or less than Balance Amount"); */
						return false;
				    }	            	
		            
				 </c:if> 
				
				if(amount==="" || !(amount>0))
				{
					Materialize.toast('Please Enter amount!', '4000', 'teal lighten-3');
					/* $('#addeditmsg').modal('open');
				     $('#msgHead').text("Payment Message");
				     $('#msg').text("Enter amount"); */
					return false;
				}
				
				/* if(paymentcomment==="" && type==="employee")
				{
					 $('#addeditmsg').modal('open');
				     $('#msgHead').text("Payment Message");
				     $('#msg').text("Enter Comment");
					return false;
				} */
			
            });
            
            <c:if test="${forEdit=='forSupplierPaymentEdit'}">
            		
            		var payType="${paymentDoInfo.payType}"; 
            		var paidAmount="${paymentDoInfo.paidAmount}";
            		var bankName="${paymentDoInfo.bankName}";
            		var checkNumber="${paymentDoInfo.checkNumber}";
            		var checkDate="${paymentDoInfo.checkDate}";
            		
            		$('#bankName').val(bankName);
					            		
           		 	/* var checkDateInput = $('#cheqDate').pickadate();         		     
				 	var checkDatePicker = checkDateInput.pickadate('picker');          		   
				 	checkDatePicker.set('select', checkDate, { format: 'yyyy-mm-dd' }); */
					
					$('#cheqNo').val(checkNumber);
					$('#amount').val(paidAmount);
					
            		if(payType=="Cash"){
            			$('#cash').click();
            		}else{
            			$('#cheque').click();
            		}
            		
            		$('#amount').keyup(function(){
            			
            			var maxPay=parseFloat(paidAmount)+parseFloat("${paymentDoInfo.amountUnPaid}");
            			
            			var payingAmt=$('#amount').val();
            			if(payingAmt=="" || payingAmt==undefined){
            				payingAmt=0;
            			}
            			
            			if(maxPay<payingAmt){
            				Materialize.toast('Entered amount must be same or less than Balance Amount!', '2000', 'teal lighten-2');
            				return false;
            			}
            			            			
            		});
            		           	
            </c:if>
            
            <c:if test="${forEdit=='forEmployeeSalaryEdit'}">
    		
	    		var payType="${paymentDoInfo.payType}"; 
	    		var paidAmount="${paymentDoInfo.paidAmount}";
	    		var bankName="${paymentDoInfo.bankName}";
	    		var checkNumber="${paymentDoInfo.checkNumber}";
	    		var checkDate="${paymentDoInfo.checkDate}";
	    		var comment="${paymentDoInfo.comment}";
	    		
	    		$('#bankName').val(bankName);
				            		
	   		 	/* var checkDateInput = $('#cheqDate').pickadate();         		     
			 	var checkDatePicker = checkDateInput.pickadate('picker');          		   
			 	checkDatePicker.set('select', checkDate, { format: 'yyyy-mm-dd' }); */
				
				$('#cheqNo').val(checkNumber);
				$('#amount').val(paidAmount);
				$('#comment').val(comment);
				
	    		if(payType=="Cash"){
	    			$('#cash').click();
	    		}else{
	    			$('#cheque').click();
	    		}
	    		           	
	    	</c:if>

        });
    </script>
    <style>
    
    .card{
     height: 2.5rem;
     line-height:2.5rem;
     }
    .card-image{
        	width:40% !important;
        	background-color:#0073b7 !important;
        }
	.card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}
    </style>
</head>

<body>
     <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br> 
        <div class="row">
          <!--   <div class="col l12 m12 s12">
                <h3 class="center"> Payment Details </h3>
            </div> -->
            <div class="row">
            <div class="col s12 m12 l12">
            	<c:choose>
            	 <c:when test="${paymentDoInfo.type=='supplier'}">  
            		<div class="col s6 m4 l4">
            			
            			<div class="card horizontal">
					      <div class="card-image">
					         <h6 class="white-text center">Supplier Id</h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p><h6 class="center-align"><c:out value="${paymentDoInfo.id}" /></h6>
								</p>	        
					        </div>
            	          </div>	
            		 </div>	
            			  
            		</div>
            		<div class="col s6 m4 l4">
            			
            			<div class="card horizontal">
					      <div class="card-image">
					           <h6 class="white-text center">Transaction Id </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					            <h6 class="center-align"><c:out value="${paymentDoInfo.inventoryId}" /></h6>
								</p>	        
					        </div>
            	          </div>	
            		 </div>	
            			  
            		</div>
            	</c:when>  
            	
            <c:otherwise>  
            	<c:choose>
            		<c:when test="${paymentDoInfo.type=='counter'}">  
            		<c:if test="${paymentDoInfo.id!=null}">
	            		<div class="col s6 m4 l4">	
	            			<div class="card horizontal">
						      <div class="card-image">
						         <h6 class="white-text center">Shop Id</h6>
						      </div>
						      <div class="card-stacked grey lighten-3">
						        <div class="card-content">
						          <p><h6 class="center-align"><c:out value="${paymentDoInfo.id}" /></h6>
									</p>	        
						        </div>
	            	          </div>	
	            		 </div>	            			  
	            		</div>
            		</c:if>
            		<div class="col s6 m4 l4">
            			
            			<div class="card horizontal">
					      <div class="card-image">
					           <h6 class="white-text center">Counter Order Id</h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					            <h6 class="center-align"><c:out value="${paymentDoInfo.inventoryId}" /></h6>
								</p>	        
					        </div>
            	          </div>	
            		 </div>	
            			  
            		</div>
            	</c:when>
            	<c:otherwise>
            		<div class="col s6 m4 l4">
            			
            			<div class="card horizontal">
					      <div class="card-image">
					       <h6 class="white-text center">Employee Id </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content" style="padding:0">
					          <p>
								 <h6 class="center-align"><c:out value="${paymentDoInfo.id}" /></h6>														
					 		</p>	        
					        </div>
            	          </div>	
            		 </div>	
            			  
            		</div>
            	</c:otherwise>
            </c:choose>
            		
            </c:otherwise> 
           </c:choose> 
            			<%-- <div class="col s6 m4 l4">
            			<div class="card horizontal">
					      <div class="card-image">
					        <img src="resources/img/user64.png">
					      </div>
					      <div class="card-stacked">
					        <div class="card-content" style="padding:0">
					          <p>
					          	<c:choose>  
								    <c:when test="${paymentDoInfo.type=='supplier'}">  
								       <h5 class="blue-text text-darken-8 center-align">Supplier Id <br><c:out value="${paymentDoInfo.id}" /></h5>
								       
								       <h5 class="blue-text text-darken-8 center-align">Inventory Id <br><c:out value="${paymentDoInfo.inventoryId}" /></h5>
								    </c:when>  
								    <c:otherwise>  
								       <h5 class="blue-text text-darken-8 center-align">Employee Id<br> <c:out value="${paymentDoInfo.id}" /></h5>
								    </c:otherwise>  
							  </c:choose>  
					 		</p>	        
					        </div>
            	          </div>	
            		 </div>	
            			
            		</div> --%>
            		<div class="col s6 m4 l4">
            		   <div class="card horizontal">
					      <div class="card-image">
					         <h6 class="white-text center">Name </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p><h6 class="center-align"><c:out value="${paymentDoInfo.name}" /> </h6></p>
					        </div>
            	          </div>	
            		 </div>	
            			<%-- <h5 class="blue-text text-darken-8  center-align">Name <br> <c:out value="${paymentDoInfo.name}" /> </h5> --%>
            		</div>
            		</div>
            		<div class="col s12 m12 l12">
            		<div class="col s6 m4 l4">
            			<div class="card horizontal">
					      <div class="card-image">
					         <h6 class="white-text center">Total Amount </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p><h6 class="center-align"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.totalAmount}" /></h6></p>
					        </div>
            	          </div>	
            		 </div>	
            			
            		</div>
            		
            		<div class="col s6 m4 l4">
            			<div class="card horizontal">
					      <div class="card-image">
					          <h6 class="white-text center">Amount Paid </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					         	<h6 class="center-align"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.amountPaid}" /></h6>
					          </p>
					        </div>
            	          </div>	
            		 </div>	
            			
            		</div>
            		<div class="col s6 m4 l4">
            			<div class="card horizontal">
					      <div class="card-image">
					           <h6 class="white-text center">Balance Amount </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					          <h6 class="center-align"><b class="red-text"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.amountUnPaid}" />  </b></h6>
					          </p>
					        </div>
            	          </div>	
            		 </div>
            			
            		</div>
            	</div>
            </div>
            
            
            
            
            <%-- <div class="col s12 m6 l4 offset-l1 z-depth-3 grey lighten-4" style="padding: 20px;margin-left:15%; height:340px;">                
                
                <c:choose>  
				    <c:when test="${paymentDoInfo.type=='supplier'}">  
				       <h5 class="blue-text text-darken-8 center-align">Supplier Id : <c:out value="${paymentDoInfo.id}" /></h5>
				       <hr style="border:1px dashed teal;">
				       <h5 class="blue-text text-darken-8 center-align">Inventory Id : <c:out value="${paymentDoInfo.inventoryId}" /></h5>
				    </c:when>  
				    <c:otherwise>  
				       <h5 class="blue-text text-darken-8 center-align">Employee Id : <c:out value="${paymentDoInfo.id}" /></h5>
				    </c:otherwise>  
				</c:choose>  
                
                <hr style="border:1px dashed teal;">
                <h5 class="blue-text text-darken-8  center-align">Name : <c:out value="${paymentDoInfo.name}" /> </h5>
                <hr style="border:1px dashed teal;">
                <h5 class="blue-text text-darken-8  center-align">Total Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.totalAmount}" /></h5>
                <hr style="border:1px dashed teal;">
                <h5 class="blue-text text-darken-8  center-align">Amount Paid : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.amountPaid}" /></h5>
                <hr style="border:1px dashed teal;">
                <h5 class="blue-text text-darken-8  center-align"><b class="red-text">Balance Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${paymentDoInfo.amountUnPaid}" />  </b></h5>
                
                
            </div> --%>
            <div class="col s12 m12 l12 center-align">
            	<div class="col s12 m12 l12 z-depth-3 grey lighten-4">
                <form action="${pageContext.servletContext.contextPath}/${paymentDoInfo.url}" method="post">
                	<input value="${paymentDoInfo.type}" type="hidden" id="typeId">
                	<input value="${paymentDoInfo.pkId}" type="hidden" name="employeeDetailsId">
                	<input value="${paymentDoInfo.inventoryId}" type="hidden" name="inventoryId">
                	<input value="${paymentDoInfo.paymentId}" type="hidden" name="paymentId">
                	
                	
                    <div class="col l11 m12 s12 chqcash" style="margin-top:10px;">
                        <b><h5> Payment Mode</h5><br/>
                        <input type="radio" id="cash" name="group1" checked /> 
                        <label for="cash">Cash</label>
                        <input type="radio" id="cheque" name="group1"/> 
                        <label for="cheque">Cheque</label></b>
                    </div>
                    <br>
                    <div class="col s12 m12 l12 center-center-align">                    
                    <div class="input-field col l4 m4 s6 left chequeNo">
                        <input type="text" name="bankName" class="validate" id="bankName" title="Enter Bank Name"> <label for="customerBankName">Bank Name</label>
                    </div>
                      <div class="input-field col s6 m2 l2 offset-l4 offset-m4 center amount">
                        <input type="text" name="amount" id="amount" title="Enter Amount" required>
                        <label for="amount"><span class="red-text">*</span>Amount</label>
                    </div>
                    <div class="input-field col s6 m2 l2 center">
                        <input type="text" name="dueDate" class="datepicker disableDate" id="dueDate" title="Enter dueDate" required>
                        <label for="dueDate"><span class="red-text">*</span>Due Date</label>
                    </div>
                    <div class="input-field col l2 m4 s6   chequeNo">
                        <input type="text" name="cheqNo" class="validate num" id="cheqNo" maxlength="6" minlength="6" title="Enter Cheque Number">
                         <label for="customerChequeNumber">Cheque Number</label>
                    </div>
                    <c:if test="${paymentDoInfo.type=='counter'}">  
	                    <div class="input-field col l2 m4 s6 right chequeNo">
		                     <input type="text" id="cheqDate" class="datepicker disableDate" name="cheqDate">
		                     <label for="cheqDate" class="black-text">Cheque Date</label>
	                    </div> 
                    </c:if>
                  </div>
                    
                    <br>
                   
                    <br>
                    <div class="col s12 m12 l12 center center-align">
                    <c:choose>  
					    <c:when test="${paymentDoInfo.type=='employee'}"> 
		                    <div class="input-field col s12 m4 l4 offset-l4">
		                        <textarea id="comment" type="text" name="comment" class="materialize-textarea" title="Enter Comment" required></textarea>
		                        <label for="comment">Comment</label>
		                    </div>
	                    </c:when>
                    </c:choose>
                    </div>
                    <br><br>
                    <div class="input-field col s6 m12 l12 center center-align">
                        <button id="paySubmit" class="btn waves-effect waves-light" type="submit">Pay<i class="material-icons right">send</i>
                                </button>
                                <br><br>
                    </div>
                    <br><br>
                </form>
            </div>
				</div>
        </div>









        <br>
        <!--<div class="container">
            <form action="" method="post" name="aboutus">
                <div class="row  z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Payment Details </h4>
                    </div>
                    <div class="row">

                        <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                            <h5 class="left"> Name: </h5>
                            <h5 class="right" id="name">ABC </h5>
                        </div>

                        <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                            <h5 class="left">Total Amount: </h5>
                            <h5 class="right" id="totalAmt">123 </h5>
                        </div>

                        <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                            <h5 class="left">Amount Paid: </h5>
                            <h5 class="right" id="amtPaid">123 </h5>

                        </div>
                        <div class="input-field col s12 m6 l6 push-l2 push-m2 offset-m1 offset-l1">
                            <h5 class="left">Amount Remaining: </h5>
                            <h5 class="right" id="amtRemaining">123 </h5>
                        </div>
                        <br>
                        <div class="input-field col s12 m6 l12 push-l2 push-m2 offset-m1 offset-l1">
                            <div class="col s12 l6">
                                <p>

                                    <input name="group1" type="radio" id="test1" class="cash" checked/>
                                    <label class="left" for="test1">Cash</label>

                                    <input name="group1" type="radio" id="test2" class="cheque" />
                                    <label class="right" for="test2">Cheque</label>
                                </p>
                            </div>
                        </div>
                        <div class="input-field col s12 m12 l3 push-m2 offset-m1 push-l2 offset-l1">
                            <input id="amount" type="text">
                            <label for="amount" class="active">Enter Amount:</label>
                        </div>
                        <div id="BankDetails">

                            <div class="input-field col s12 m6 l3 push-l2 ">
                                <input id="bankName" type="text">
                                <label for="bankName" class="active">Enter Bank Name:</label>
                            </div>
                            <br>
                            <div class="col s12 m6 l12 push-l2">
                                <div class="input-field col s12 m6 l3 push-l1 ">
                                    <input id="cheqNo" type="text">
                                    <label for="cheqNo" class="active">Enter Cheque No.:</label>
                                </div>
                                <div class="input-field col s12 m6 l3 push-l1 ">
                                    <input id="cheqDate" type="text" class="datepicker">
                                    <label for="cheqDate">Enter Cheque Date:</label>
                                </div>
                            </div>
                        </div>
                        <div class="input-field col s12 m5 l5 pull-l1 pull-l1 offset-m1 offset-l5">
                            <!--<i class="material-icons prefix">mode_edit</i>-->
        <!--<textarea id="comment" class="materialize-textarea"></textarea>
        <label for="comment">Comment</label>
        </div>

        </div>

        </div>

        <div class="input-field col s12 m6 l4 offset-l5 center-align">
            <button class="btn waves-effect waves-light blue darken-8" type="submit">Pay<i class="material-icons right">send</i> </button>
        </div>
        <br>
        </form>-->
        </div>
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
       	<!-- <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
    </main>





    <!--content end-->
</body>

</html>