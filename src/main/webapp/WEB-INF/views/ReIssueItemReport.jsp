<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Return Order Id</th>
                            <th class="print-col">Sales Person</th>
                            <th class="print-col">Delivery Person</th>
                            <th class="print-col">Shop Name</th>
                            <th class="print-col">Area</th>
                            <th class="print-col">Region</th>
                            <th class="print-col">City</th>
                            <th class="print-col">Total ReIssue Quantity</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Total Tax</th>
                            <th class="print-col">Total Amount With Tax</th>
                            <th class="print-col">Date Of ReIssue</th>
                            <th class="print-col">Date Of Delivery</th>
                            <th class="print-col">ReIssue Status</th>
							<th>Product Details</th>
                        </tr>
                    </thead>

                    <tbody>
                        
                        <c:if test="${not empty reIssueOrderReportList}">
						<c:forEach var="listValue" items="${reIssueOrderReportList}">
						<tr>
                            <td><c:out value="${listValue.reIssueOrderDetails.returnOrderProduct.returnOrderProductId}" /></td>
                            <td><a href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsSM.employeeDetailsId}"><c:out value="${listValue.employeeDetailsSM.name}" /></a></td>
                            <td><a href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsDB.employeeDetailsId}"><c:out value="${listValue.employeeDetailsDB.name}" /></a></td>
                            <td><c:out value="${listValue.reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.shopName}" /></td>
                            <td><c:out value="${listValue.reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.area.name}" /></td>
                            <td><c:out value="${listValue.reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.area.region.name}" /></td>
                            <td><c:out value="${listValue.reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.area.region.city.name}" /></td>
                            <td><c:out value="${listValue.reIssueOrderDetails.totalQuantity}" /></td>
			    <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.reIssueOrderDetails.totalAmount}" /></td>            
                            <td><c:out value="${listValue.reIssueOrderDetails.totalAmountWithTax-listValue.reIssueOrderDetails.totalAmount}" /></td>
                            <td><c:out value="${listValue.reIssueOrderDetails.totalAmountWithTax}" /></td>
                            <td>
                            	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.reIssueOrderDetails.reIssueDate}"  />
	                            <c:out value="${date}" />
                            </td>
                            <td>
                            	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.reIssueOrderDetails.reIssueDeliveryDate}"  />
	                            <c:out value="${date}" />
                            </td>
                            <td><c:out value="${listValue.reIssueOrderDetails == null ? 'Pending' : listValue.reIssueOrderDetails.status}" /></td>
                            <td><a href="${pageContext.servletContext.contextPath}/reIssueOrderDetailsByReturnOrderId?reIssueOrderId=${listValue.reIssueOrderDetails.reIssueOrderId}">View</a></td>
                        </tr>
						</c:forEach>
						</c:if>
                       
                    </tbody>
                </table>
            </div>
        </div>


    </main>
    <!--content end-->
</body>

</html>