<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
    
    <script type="text/javascript">
    
    $(document).ready(function(){
    	 $(".showDates").hide();
         $(".rangeSelect").click(function() {
        	 $("#oneDateDiv").hide();
             $(".showDates").show();
         });
    	$("#oneDateDiv").hide();
    	$(".pickdate").click(function(){
    		 $(".showDates").hide();
    		$("#oneDateDiv").show();
    	});
    	var table = $('#tblData').DataTable();
		table.destroy();
    	 $('#tblData').DataTable({
             "oLanguage": {
                 "sLengthMenu": "Show:  _MENU_",
                 "sSearch": " _INPUT_" //search
             },
             columnDefs: [
                          { 'width': '1%', 'targets': 0 },
                          { 'width': '1%', 'targets': 1 },
                          { 'width': '1%', 'targets': 2 },
                          { 'width': '10%', 'targets': 3 },
                          { 'width': '5%', 'targets': 4 },
                          { 'width': '8%', 'targets': 5 },
                          { 'width': '2%', 'targets': 6 },
                          { 'width': '2%', 'targets': 7 },
                          { 'width': '1%', 'targets': 8 },
                          { 'width': '1%', 'targets': 9 },
                          { 'width': '1%', 'targets': 10 }, 
                          { 'width': '1%', 'targets': 11 }
                          ],
             lengthMenu: [
                 [10, 25., 50, -1],
                 ['10 ', '25 ', '50 ', 'All']
             ],
             autoWidth: false,
             
             //dom: 'lBfrtip',
             dom:'<lBfr<"scrollDivTable"t>ip>',
             buttons: {
                 buttons: [
                     //      {
                     //      extend: 'pageLength',
                     //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
                     //  }, 
                     {
                         extend: 'pdf',
                         className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                         text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
                         //title of the page
                         title: function() {
                             var name = $(".heading").text();
                             return name
                         },
                         //file name 
                         filename: function() {
                             var d = new Date();
                             var date = d.getDate();
                             var month = d.getMonth();
                             var year = d.getFullYear();
                             var name = $(".heading").text();
                             return name + date + '-' + month + '-' + year;
                         },
                         //  exports only dataColumn
                         exportOptions: {
                             columns: ':visible.print-col'
                         },
                         customize: function(doc, config) {
                        	  doc.content.forEach(function(item) {
                          		  if (item.table) {
                          		  item.table.widths = [20,65,43,50,35,50,40,30,40,40,40,45] 
                          		 } 
                          		    })
                             /* var tableNode;
                             for (i = 0; i < doc.content.length; ++i) {
                               if(doc.content[i].table !== undefined){
                                 tableNode = doc.content[i];
                                 break;
                               }
                             }
            
                             var rowIndex = 0;
                             var tableColumnCount = tableNode.table.body[rowIndex].length;
                              
                             if(tableColumnCount > 6){
                               doc.pageOrientation = 'landscape';
                             } */
                             /*for customize the pdf content*/ 
                             doc.pageMargins = [5,20,10,5];
                             
                             doc.defaultStyle.fontSize = 8;
                             doc.styles.title.fontSize = 12;
                             doc.styles.tableHeader.fontSize = 11;
                             doc.styles.tableFooter.fontSize = 11;
                             doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
                           },
                          
                   
                     },
                     {
                         extend: 'excel',
                         className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                         text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
                         //title of the page
                         title: function() {
                             var name = $(".heading").text();
                             return name
                         },
                         //file name 
                         filename: function() {
                             var d = new Date();
                             var date = d.getDate();
                             var month = d.getMonth();
                             var year = d.getFullYear();
                             var name = $(".heading").text();
                             return name + date + '-' + month + '-' + year;
                         },
                         //  exports only dataColumn
                         exportOptions: {
                             columns: ':visible.print-col'
                         },
                     },
                     {
                         extend: 'print',
                         className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                         text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
                         //title of the page
                         title: function() {
                             var name = $(".heading").text();
                             return name
                         },
                         //file name 
                         filename: function() {
                             var d = new Date();
                             var date = d.getDate();
                             var month = d.getMonth();
                             var year = d.getFullYear();
                             var name = $(".heading").text();
                             return name + date + '-' + month + '-' + year;
                         },
                         //  exports only dataColumn
                         exportOptions: {
                             columns: ':visible.print-col'
                         },
                     },
                     {
                         extend: 'colvis',
                         className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
                         text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
                         collectionLayout: 'fixed two-column',
                         align: 'left'
                     },
                 ]
             }

         });
    	 $('select').material_select();
	     $("select")
	         .change(function() {
	             var t = this;
	             var content = $(this).siblings('ul').detach();
	             setTimeout(function() {
	                 $(t).parent().append(content);
	                 $("select").material_select();
	             }, 200);
	         });
	     $('.dataTables_filter input').attr("placeholder", "Search");
	     //$('.dataTables_filter input').attr("placeholder", "&#61447; Search");
	     
    	
    });
    
    
    </script>
     <style>
    /* div.dataTables_filter input::-webkit-input-placeholder, 
div.dataTables_filter input::-moz-placeholder,
div.dataTables_filter input:-ms-input-placeholder,
div.dataTables_filter input:-moz-placeholder{
		font-family: 'FontAwesome' !important;
          content: "Search" !important;        
        padding-top: 0;
        display: block;
        text-align:center;
	} */
div.dataTables_filter{
	font-family: 'FontAwesome' !important;
	content:'';
	/* color:red; */
}	
     .card{
     height: 2.5rem;
     line-height:2.5rem;
    /*  text-align:center !important; */
     }
    .card-image{
        	width:45% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}
     
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
     <br>
        <div class="row">
          
            <div class="col s12 l3 m3">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Total Amount</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="">&#8377;<span id="totalAmountId"><c:out value="${totalAmount}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
             <div class="col s12 l3 m3">
             	 <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">No of Order</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class=""><span id="totalAmountId"><c:out value="${noOfOrders}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
                
            </div>
            <div class="col s12 m2 l2 right-align right" style="margin-top:1%;">
             <!--    <div class="col s6 m4 l4 right"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light light-blue darken-4' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    <li><a href="${pageContext.servletContext.contextPath}/salesReport?range=today">Today</a></li>
                    	<li><a href="${pageContext.request.contextPath}/salesReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.request.contextPath}/salesReport?range=last7days">Last 7 Days</a></li>
                        <li><a href="${pageContext.request.contextPath}/salesReport?range=last1month">Last 1 Month</a></li>
                        <li><a href="${pageContext.request.contextPath}/salesReport?range=last3months">Last 3 Month</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li>                        
                        <li><a href="${pageContext.request.contextPath}/salesReport?range=viewAll">View All</a></li>
                    </ul>
                </div>
                	<div class="col s12 l4	m4 right"> 
                 <form action="${pageContext.request.contextPath}/salesReport" method="post">
                    <input type="hidden" name="range" value="range">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                             
                              <div class="input-field col s6 m5 l5 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                                <div class="input-field col s6 m5 l5 right">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                                 </div>
                          </span>
                </form>
                 </div>
				<div class="col s12 l3	m3 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/salesReport" method="post">
                    <input type="hidden" name="range" value="pickdate">
	                    <div class="input-field col s12 m6 l6">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
                       
                        </div>
                    </form>
               </div>
               
               
                
            </div>
        </div>

        <div class="row">
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col" rowspan="2">Sr. No.</th>
                            <th class="print-col" rowspan="2">Order Id</th>
                            <th class="print-col" rowspan="2">Business Id</th>
                            <th class="print-col" rowspan="2">Shop Name</th>
                            <th class="print-col" rowspan="2">Area</th>
                            <th class="print-col" rowspan="2">Salesman Name</th>
                            <th class="print-col" rowspan="2">Taxable Amt</th>
                            <th class="print-col" rowspan="2">Total Amt</th>
                            <th class="print-col" colspan="3">Status</th>
                            <th class="print-col" rowspan="2">Payment date</th>
                        </tr>
                        <tr>
                        	<th class="print-col">SalesMan</th>
                        	<th class="print-col">GateKeeper</th>
                        	<th class="print-col">DeliveryBoy</th>
                        </tr>
                    </thead>

                    <tbody>
	                    <c:if test="${not empty salesManReportModelList}">
						<c:forEach var="listValue" items="${salesManReportModelList}">
	                        <tr>
	                            <td><c:out value="${listValue.srno}"/></td>
	                            <td><a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.request.contextPath}/orderProductDetailsListForWebApp?orderDetailsId=${listValue.orderId}"><c:out value="${listValue.orderId}"/></a></td>
	                            <td><c:out value="${listValue.businessNameId}"/></td>
	                            <td><c:out value="${listValue.shopName}"/></td>
	                            
	                            <td><c:out value="${listValue.areaName}"/></td>
	                            <td>
		                           <a title="View Salesman Details" class="tooltipped" data-position="right" data-delay="50" data-tooltip="View Salesman Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsId}"><c:out value="${listValue.smName}"/></a>
	                            </td>
	                            <td><c:out value="${listValue.orderAmount}"/></td>
	                            <td><c:out value="${listValue.orderAmountWithTax}"/></td>
								
								<td class="wrapok"><c:out value="${listValue.orderStatusSM}" /> <br> <c:out value="${listValue.orderStatusSMDate}" /> <br> <c:out value="${listValue.orderStatusSMTime}" /></td>
	                            <td class="wrapok"><c:out value="${listValue.orderStatusGK}" /> <br> <c:out value="${listValue.orderStatusGKDate}" /> <br> <c:out value="${listValue.orderStatusGKTime}" /></td>
	                            <td class="wrapok"><c:out value="${listValue.orderStatusDB}" /> <br> <c:out value="${listValue.orderStatusDBDate}" /> <br> <c:out value="${listValue.orderStatusDBTime}" /></td>
	                            
	                            <td class="wrapok"><c:out value="${listValue.dateOfPayment}"/></td>
	                        </tr>
	                     </c:forEach>
	                     </c:if>
                    </tbody>
                </table>
            </div>
        </div>

		<!-- Modal Structure for Salesman Details -->
        <div id="salesmanDetails" class="modal">
            <div class="modal-content">
                <h5 class="center"><u>Salesman Details</u></h5>
                
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Route No</th>
                            <th>Area Name</th>
                            <th>Cluster</th>
                            <th>Salesman Name</th>
                            <th>Mobile No</th>
                            <th>Area Sales Manager</th>
                            <th>ASM-Mobile No.</th>

                        </tr>
                    </thead>
                    <tbody id="areaList">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">
				<div class="col s6 m6 l7">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                </div>
            </div>
        </div>

    </main>
    <!--content end-->
</body>

</html>