<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
        <%@include file="components/header_imports.jsp" %>
        <script>var myContextPath = "${pageContext.request.contextPath}"</script>
      <script type="text/javascript" src="resources/js/hashtable.js"></script>
      <script>
      var showSupplierNavigation=${sessionScope.loginType=='CompanyAdmin'};
      </script>
   <script type="text/javascript" src="resources/js/editOrder.js"></script>

    <script>
    $(document).ready(function(){
    	var table = $('#tblData').DataTable();
  		 table.destroy();
  		 $('#tblData').DataTable({
  	         "oLanguage": {
  	             "sLengthMenu": "Show _MENU_",
  	             "sSearch": "_INPUT_" //search
  	         },
  	      	autoWidth: false,
  	         columnDefs: [
  	                      { 'width': '1%', 'targets': 0 },
  	  	                   { 'width': '5%', 'targets': 6}
  	                 	                     
  	                      ],
  	         lengthMenu: [
  	             [10, 25., 50, -1],
  	             ['10 ', '25 ', '50 ', 'All']
  	         ],
  	         
  	         
  	         //dom: 'lBfrtip',
  	         dom:'<lBfr<"scrollDivTable"t>ip>',
  	         buttons: {
  	             buttons: [
  	                 //      {
  	                 //      extend: 'pageLength',
  	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
  	                 //  }, 
  	                 {
  	                     extend: 'pdf',
  	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                     customize: function(doc, config) {
  	                    	doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = ['*','*','*','*','*']
	                    		 }
	                    		    });
  	                         var tableNode;
  	                         for (i = 0; i < doc.content.length; ++i) {
  	                           if(doc.content[i].table !== undefined){
  	                             tableNode = doc.content[i];
  	                             break;
  	                           }
  	                         }
  	        
  	                         var rowIndex = 0;
  	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
  	                          
  	                         if(tableColumnCount > 6){
  	                           doc.pageOrientation = 'landscape';
  	                         }
  	                         /*for customize the pdf content*/ 
  	                         doc.pageMargins = [5,20,10,5];   	                         
  	                         doc.defaultStyle.fontSize = 8	;
  	                         doc.styles.title.fontSize = 12;
  	                         doc.styles.tableHeader.fontSize = 11;
  	                         doc.styles.tableFooter.fontSize = 11;
  	                       doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
  	                       },
  	                 },
  	                 {
  	                     extend: 'excel',
  	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'print',
  	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'colvis',
  	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
  	                     collectionLayout: 'fixed two-column',
  	                     align: 'left'
  	                 },
  	             ]
  	         }

  	     });
  		 $("select")
            .change(function() {
                var t = this;
                var content = $(this).siblings('ul').detach();
                setTimeout(function() {
                    $(t).parent().append(content);
                    $("select").material_select();
                }, 200);
            });
        $('select').material_select();
        $('.dataTables_filter input').attr("placeholder", "Search");
    });
    </script>
    <style>
      
        #order{
	width:80% !important;
	max-height:80% !important;
	}
	.dropdown-content{
	max-height:250px !important;
	}
		.select2 {
	width: 100% !important;
	float:right;
	height:3rem !important;
	
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
       
        <br>
        
        <div class="row">
           
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>
                            <th class="print-col">Order Id</th>
                            <th>Order Details</th>
                            <th class="print-col">Taxable Amount</th>
                            <th class="print-col">Total Amount</th>
                            <th class="print-col">Order Date</th>
                            <th>Edit</th>
                        </tr>
                    </thead>

                    <tbody>
                    <% int rowincrement=0; %>
                   <c:if test="${not empty last24Order}">
					<c:forEach var="listValue" items="${last24Order}">
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
                        <tr>
                            <td><c:out value="${rowincrement}" /></td>
                            <td><c:out value="${listValue.supplierOrderId}" /></td>
                            <td><button onclick="showOrderDetails('${listValue.supplierOrderId}')" class="btn blue-gradient">View</button></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountWithTax}" /></td>
                            <td>
                            	<fmt:formatDate pattern = "yyyy-MM-dd" var="date"   value = "${listValue.supplierOrderDatetime}"  />
		                        <c:out value="${date}" />
                            </td>
                            <td>
                            <a href="#delete_${listValue.supplierOrderId}" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a>
                            <a onclick="editOrder('${listValue.supplierOrderId}')" class="btn-flat">
                            	<i class="material-icons tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit order" >edit</i>
                            </a>
                            </td>
                        </tr>
                        
                        		<!-- Modal Structure for delete start-->
			                    <div id="delete_${listValue.supplierOrderId}" class="modal deleteModal row">
			                        <div class="modal-content  col s12 m12 l12">
			                        	<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
			                            <h5 class="center-align">Do You Want to Delete?</h5>
			                            <br/>
			                        </div>
			                        <div class="modal-footer">            
			         				   <div class="col s6 m6 l3 offset-l3">
			                 				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
			                			</div>
			            				<div class="col s6 m6 l3">
			             				   <a href="${pageContext.request.contextPath}/deleteSupplierOrder?supplierOrderId=${listValue.supplierOrderId}" class="modal-action modal-close waves-effect  btn">Delete</a>
			               				 </div>                
			            			</div>
			                    </div>
			                    <!-- Modal Structure for delete end-->
					</c:forEach>
					</c:if>
                    </tbody>
                </table>
            </div>
        </div>

        <!-- Modal Structure for View Product Details -->
        <div id="product" class="modal">
            <div class="modal-content">
                <h5 class="center"><u>Product Details</u></h5>
               
                <br>
                <table border="2" class="centered tblborder" >
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Supplier Name</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>MRP</th>
                            <th>Taxable Amount</th>
                            <th>Total Amount</th>
                        </tr>
                    </thead>
                    <tbody id="productOrderedDATA">
                        <!-- <tr>
                            <td>1</td>
                            <td>Mouse</td>
                            <td>I.T</td>
                            <td>BlueSquare</td>
                            <td>5</td>
                            <td>350</td>
                        </tr> -->
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">

                <div class="col s12 m6 l6 offset-l1">
                    <a href="#!" class="modal-action modal-close waves-effect btn">Close</a>
                </div>

            </div>
        </div>


        <!-- Modal Structure for order -->
        <div id="order" class="modal">
            <div class="modal-content">
                <h5 class="center-align"><u>Order Information</u></h5>
                
               
               
                    <div class="row">
                        <form action="${pageContext.servletContext.contextPath}/editOrderBook" method="post" id="editOrderBookForm">
							<input id="productWithSupplikerlist" type="hidden" class="validate" name="productIdList">
							<input id="supplierOrderIdId" type="hidden" class="validate" name="supplierOrderId">
                        	<input type='hidden' id='currentUpdateRowId'>
                            <div class="col s12 l6 m6">
                            	<div class="input-field col s12 m6 l6">
                                    <select id="supplierIdForOrder" class="select2" name="supplierId">
                                            <option value="0"  selected>Choose Supplier Name</option>
                                            <option value="1">Option 1</option>
                                            <option value="2">Option 2</option>
                                            <option value="3">Option 3</option>
                                     </select>
                                    <!--<label>Supplier Name</label>-->
                                </div>
                                
                                <div class="input-field col s12 m6 l6">
                                     <select id="brandIdForOrder" class="select2" name="brandId">
                                            <option value="0"  selected>Choose Brand</option>
                                           
                                     </select>
                                </div>
                                <div class="input-field col s12 m6 l6">
                                     <select id="categoryIdForOrder" class="select2" name="categoryId">
                                            <option value="0"  selected>Choose Category</option>
                                            
                                     </select>
                                </div>
                                <div class="input-field col s12 m6 l6">
                                
                                     <select id="productIdForOrder" class="select2" name="productId">
                                            <option value="0"  selected>Choose Product Name</option>
                                            
                                     </select>
                                </div>                                
                                 <div class="input-field col s12 m6 l6">
                                    <input id="supplierMRPRateForOrder" type="text"  readonly class="grey lighten-3">
                                    <input id="supplierRateForOrder" type="hidden" class="grey lighten-3">
                                    <label for="supplierRateForOrder" class="black-text">MRP</label>
                                </div>
                                <!-- </div>
                                  <div class="row"> -->
                                <div class="input-field col s12 m4 l4">
                                <label for="supplierMobileNumber" class="black-text">Mobile Number </label>
                                    <input id="supplierMobileNumber" type="text" class="grey lighten-3" minlength="10" maxlength="10">
                                    <!-- <label for="mobile"></label> -->
                                </div>
                                <div class="col s12 m2 l2" style="margin-top:4%">
                                    <input type="checkbox" id="OrderMobileNumberchecker" required/>
                                    <label for="OrderMobileNumberchecker" class="black-text">Edit</label>
                                    <!-- <a href="#!" class="modal-action waves-effect waves-green btn">Edit</a> -->
                                </div>
                                <div class="input-field col s12 m6 l6">
                                    <input id="orderQuantity" type="text" class="validate" required>
                                    <label for="orderQuantity" class="black-text">Quantity</label>
                                </div>
                               
                                
                                <div class="input-field col s12 m6 l4" style="margin-top:4%">
                                    <button id="addOrderProduct" type="button" name="addOrderCart" class="modal-action waves-effect waves-green btn" style="margin-top:3%">Add Cart</button>
                                </div>
                                
                            </div>


                            <div class="col s12 l6 m6">
                                <table class="tblborder centered">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</th>
                                            <th>Supplier Name</th>
                                            <th>Product</th>
                                            <th>MRP</th>
                                            <th>Quantity</th>
                                            <th>Edit</th>
                                            <th>Cancel</th>
                                        </tr>
                                    </thead>
                                    <tbody id="orderCartTb">
                                        <!-- <tr>
                                            <td>1</td>
                                            <td>Ankit</td>
                                            <td>John</td>
                                            <td>300</td>
                                            <td>5</td>
                                            <td><button type="button" class="btn-flat"><i class="material-icons ">edit</i></button></td>
                                            <td><button type="button" class="btn-flat"><i class="material-icons ">cancel</i></button></td>
                                        </tr> -->
                                    </tbody>
                                </table>
                            </div>

							<br/>
							<br/>
							
							<span id="orderentriesId"></span>
							
                        </form>
                    </div>
              
            </div>
            <div class="divider"></div>
            <div class="modal-footer">
                <div class="row" style="margin-bottom:0;">
                	 <div class="col s12 m6 l3 offset-l2 offset-m3 ">
                                    <a href="${pageContext.servletContext.contextPath}/addSupplier" class="modal-action waves-effect  btn teal lighten-2" style="margin-top:3%">Add New Supplier</a>
                                </div>
                    <div class="col s6 m3 l3 center-align">
                        <button type="button" id="orderProductsButton" onclick="orderConfirmationUpdateSupplierOrder()" class="modal-action waves-effect  btn">Update Order</button>
                    </div>
                    <div class="col s6 m3 l3 pull-l1 pull-m1 center-align">
                        <a href="#!" class="modal-action waves-effect modal-close btn red">close</a>
                    </div>

                </div>
            </div>
        </div>


	<!-- msg pop up -->
	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
    </main>
    <!--content end-->
</body>

</html>