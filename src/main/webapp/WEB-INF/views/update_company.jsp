<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
<script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/additional-method.js"></script>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>

<script type="text/javascript">
var cityIdCart=[];
var cityIdNameCart=[];
var gstinValid=true;;
var userIdValid=true; 
var companyCheck=true;
$(document).ready(function() {
	
    $(".eye-slash").hide();
    $(".eye").click(function() {
        $(".eye").hide();
        $(".eye-slash").show();
        $("#password").attr("type", "text");
    });
    $(".eye-slash").click(function() {
        $(".eye-slash").hide();
        $(".eye").show();
        $("#password").attr("type", "password");
    });
    $(".eye-slash1").hide();
    $(".eye1").click(function() {
        $(".eye1").hide();
        $(".eye-slash1").show();
        $("#Cnfrmpassword").attr("type", "text");
    });
    $(".eye-slash1").click(function() {
        $(".eye-slash1").hide();
        $(".eye1").show();
        $("#Cnfrmpassword").attr("type", "password");
    });
	
	<c:forEach var="listValue" items="${companyCities}">							
		cityIdCart.push(parseInt("${listValue.city.cityId}"));
		cityIdNameCart.push([parseInt("${listValue.city.cityId}"),"${listValue.city.name}"]);
	</c:forEach>
	refreshCart();
	$('.mobileNumber').keypress(function( event ){
	    var key = event.which;
	    
	    if( ! ( key >= 48 && key <= 57 || key === 13 ) )
	        event.preventDefault();
	}); 
	$('#gstIn,#panNo').keypress(function( event ) {  
		/* var k = event ? event.which : window.event.keyCode;
	    if (k == 32) */
	    if(!/[0-9a-zA-Z-]/.test(String.fromCharCode(event.which)))
	    return false;
	});
	
	$.validator.setDefaults({
	       ignore: []
	}); 
	//$('select').change(function(){ $('select').valid(); });
	jQuery.validator.addMethod("companyNameCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return false;
		}		
	    return checkDuplication(value,"companyName");
	}, "Company name already in used"); 
	
	jQuery.validator.addMethod("userIdCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return false;
		}		
	    return checkDuplication(value,"userId");
	}, "User Id already in used"); 
	
	jQuery.validator.addMethod("gstCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return true;
		}		
	    return checkDuplication(value,"gstNo");
	}, "Gst no. Is already in used"); 
	
	jQuery.validator.addMethod("panNumberCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return false;
		}		
	    return checkDuplication(value,"panNumber");
	}, "PAN Number Is already in used"); 
	
	jQuery.validator.addMethod("mobileNumberCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return false;
		}		
	    return checkDuplication(value,"mobileNumber");
	}, "Mobile Number Is already in used"); 
	
	jQuery.validator.addMethod("telephoneNumberCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return true;
		}		
	    return checkDuplication(value,"telephoneNumber");
	}, "Telephone Number Is already in used"); 
	
	jQuery.validator.addMethod("emailIdCheck", function(value, element){
		if(value=='' || value==undefined)
		{
			return true;
		}		
	    return checkDuplication(value,"emailId");
	}, "EmailId Is already in used"); 
	
	$('#saveCompanyForm').validate({
		
		rules: 
		{
		   password: {
	         	required: true
	         },
		   confirmpass: {
		      equalTo: "#password"
		    },		    
	       userId:{
	         	userIdCheck:true,
	         	required: true
         	},
           companyName:{
	    	   companyNameCheck:true,
           	  required: true
          	},
          	gstinno:{
	        	 gstCheck:true
	          },
	       panNumber :{
	    	   panNumberCheck:true
	       },
	       mobNo:{
	    	   mobileNumberCheck:true,
	    	   required:true
	       },
	       telephoneNo:{
	    	   telephoneNumberCheck:true
	       },
	       emailId:{
	    	   emailIdCheck:true
	       } 
		},
	   errorElement : "span",
	    errorClass : "invalid error",
	    errorPlacement : function(error, element) {
	      var placement = $(element).data('error');
	    
	      if (placement) {
	        $(placement).append(error)
	      } else {
	        error.insertAfter(element);
	      }
	      $('select').change(function(){ $('select').valid(); });
	    }
	  });
	
/* 	$('#gstin').on('blur',function(){
		$('#gstin').valid();
		var gstin=$('#gstin').val().trim();
		if(gstin=='' || gstin==undefined)
		{
			gstinValid=true;
			return false;
		}
		
		$.ajax({
			url :"${pageContext.request.contextPath}/checkCompanyGSTNumberForUpdate?gstinNo="+gstin+"&companyId=${company.companyId}",
			async:false,
			success : function(data) {
				if(data==="Success")
				{					
					gstinValid=true;
					$('#gstin').valid();
				}
				else
				{				
					gstinValid=false;
					$('#gstin').valid();
					
				}
			},
			error: function(xhr, status, error) {
				alert("Error");
			}
		});
		
	});
	$('#userId').on('keypress blur',function(){
		$('#userId').valid();
		var userId=$('#userId').val().trim();
		if(userId=='' || userId==undefined)
		{
			userIdValid=false;
			return false;
		}
		$.ajax({
			url : "${pageContext.servletContext.contextPath}/checkCompanyUserNameForUpdate?userName="+userId+"&companyId=${company.companyId}",
			async:false,
			success : function(data) {
				if(data==="Success"){
					userIdValid=true;
					$('#userId').valid();
				}else{
					userIdValid=false;
					$('#userId').valid();
				}
			},
			error: function(xhr, status, error) {
				alert("Error");
			}
		});
		
		
	});
$('#companyNameId').on('blur',function(){
	$('#companyNameId').valid();
		var companyName=$('#companyNameId').val().trim();
		if(companyName=='' || companyName==undefined)
		{
			companyCheck=false;
			return false;
		}
		
		$.ajax({
			url : "${pageContext.servletContext.contextPath}/checkCompanyNameForUpdate?companyName="+companyName+"&companyId=${company.companyId}",
			async:false,
			success : function(data) {
				if(data==="Success"){
					companyCheck=true;
					$('#companyNameId').valid();
				}else{
					companyCheck=false;
					$('#companyNameId').valid();
				}
			},
			error: function(xhr, status, error) {
				alert("Error");
			}
		});
		
	}); */
	
	$('#updateCompany').click(function(){
		
		/* var companyName=$('#companyNameId').val();
		var status=checkCheckCompanyNAme(companyName);
		if(status==false){
			alert("company name already exist");
			return false;
		}
		
		var userName=$('#userName').val();
		var status=checkCheckCompanyUserName(userName);
		if(status==false){
			alert("Company user name already exist");
			return false;
		}
		
		if(cityIdCart.length==0){
			Materialize.toast('Add atleast one city', '4000', 'teal lighten-2');
			return false;
		}		
		 */
		$('#citylistinput').val(cityIdCart);
		
	});
	
$('#countryid').change(function(){
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('stateid');
		
		// Clear the old options
		select.options.length = 0;

		//Load the new options
		select.options.add(new Option("Choose State", 0));
		
		$.ajax({
			url : myContextPath+"/fetchStateListByCountryId?countryId="+$('#countryid').val(),
			dataType : "json",
			success : function(data) {
				var options, index, option;
				select = document.getElementById('stateid');

				for (var i = 0, len = data.length; i < len; ++i) {
					var state = data[i];
					select.options.add(new Option(state.name, state.stateId));
				}
			},
			error: function(xhr, status, error) {
				Materialize.toast('State Not Found', '4000', 'teal lighten-2');
			}
		});		
		
	});
	
	$('#stateid').change(function(){
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('cityid');

		// Clear the old options
		select.options.length = 0;

		//Load the new options
		select.options.add(new Option("Choose City", 0));
		$.ajax({
			url : myContextPath+"/fetchCityListByStateId?stateId="+$('#stateid').val(),
			dataType : "json",
			success : function(data) {
				var options, index, option;
				select = document.getElementById('cityid');

				for (var i = 0, len = data.length; i < len; ++i) {
					var city = data[i];
					select.options.add(new Option(city.name, city.cityId));
				}
			},
			error: function(xhr, status, error) {
				Materialize.toast('City Not Found', '4000', 'teal lighten-2');
			}
		});	
		
	});
	
	$('#cityid').change(function(){
		
		var countryid=$('#countryid').val();		
		if(countryid=="" || countryid==undefined){
			Materialize.toast('Select Country', '4000', 'teal lighten-2');
			return false;
		}
		
		var stateid=$('#stateid').val();
		if(stateid=="" || stateid==undefined){
			Materialize.toast('Select State', '4000', 'teal lighten-2');
			return false;
		}
		
		var cityid=$('#cityid').val();
		if(cityid=="" || cityid==undefined || cityid==0){
			Materialize.toast('Select City', '4000', 'teal lighten-2');
			return false;
		}		
		
		if(checkContains(cityIdNameCart,parseInt(cityid))){
			Materialize.toast('City Already added', '4000', 'teal lighten-2');
			return false;
		}
		
		var cityName=$('#cityid option:selected').text();
		cityIdCart.push(parseInt(cityid));
		cityIdNameCart.push([parseInt(cityid),cityName]);
		refreshCart();
	});
});
function refreshCart(){
	
	$('#cityTable').empty();
	
	for(var i=0; i<cityIdNameCart.length; i++){
		$('#cityTable').append(	'<tr>'+
							    '<td>'+(i+1)+'</td>'+
							    '<td>'+cityIdNameCart[i][1]+'</td>'+
							    '<td><button type="button" class="btn-flat" onclick="deleteCity('+i+')"><i class="material-icons">delete</i></button></td>'+
								'</tr>');
	}	
}
function deleteCity(index){
	cityIdNameCart.splice(index, 1);
	cityIdCart.splice(index, 1);
	refreshCart();
}
function checkContains(array,value){
	for(var i=0; i<array.length; i++){
		if(array[i][0]===value){
			return true;
		}
	}
	return false;
}
function checkDuplication(checkText,type){
	var status=false;
	$.ajax({
		url : "${pageContext.servletContext.contextPath}/checkDuplicationForUpdate?checkText="+checkText+"&type="+type+"&companyId=${company.companyId}",
		async:false,
		success : function(data) {
			if(data==="Success"){
				status=true;
			}else{
				status=false;
			}
		},
		error: function(xhr, status, error) {
			alert("Error");
		}
	});
	
	return status;
}


</script>
<style>
	#upload-error{
		display:block;
	}
</style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
            <form action="${pageContext.servletContext.contextPath}/updateCompany" method="post" id="saveCompanyForm">
				<input id="companyId" type="hidden" class="validate" name="companyId" value="${company.companyId}">
                <div class="row  z-depth-3">
                    <br>          
                    <div class="row">
                    <div class="row">
                    	<div class="input-field col s12 m10 l10 push-l1 push-m1">
							<i class="material-icons prefix">account_balance</i>
	                        <input id="companyNameId" type="text" class="validate" value="${company.companyName}"  name="companyName" required>
	                        <label for="companyNameId" class="active"><span class="red-text">*</span>Company Name</label>                       
	                    </div>
	                    
                    </div>
                     <div class="row" style="margin-bottom:0">
                     <div class="input-field col s12 m5 l5 push-l1 push-m1" id="unitprice">
							 <i class="material-icons prefix">featured_video</i>
	                        <input id="panNo" type="text" class="validate" value="${company.panNumber}"  minlength="10" maxlength="10" name="panNumber" required>
	                        <label for="panNo" class="active"><span class="red-text">*</span>Pan Number</label>                       
	                    </div>
	                     <div class="input-field col s12 m5 l5 push-l1 push-m1" id="unitprice">
							<i class="material-icons prefix">assistant</i>
	                        <input id="gstIn" type="text" class="validate"  name="gstinno" minlength="15" maxlength="15" value="${company.gstinno}">
	                        <label for="gstIn" class="active"><!-- <span class="red-text">*</span> -->GST No</label>                       
	                    </div>
	                     </div>
	                      <div class="row" style="margin-bottom:0">
	                    <div class="input-field col s12 m10 l10 push-l1 push-m1" id="unitprice">
							 <i class="material-icons prefix">location_on</i>
	                        <input id="address" type="text" class="validate"   value="${company.address}" name="address" required>
	                        <label for="address" class="active"><span class="red-text">*</span>Address</label>                       
	                    </div>
	                    </div>
	                    <div class="row" style="margin-bottom:5px">
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">phone</i>
	                        <input id="mobileNumber" type="text" class="validate mobileNumber" value="${company.contact.mobileNumber}" name="mobNo" required minlength="10" maxlength="10">
	                        <label for="mobileNumber" class="active"><span class="red-text">*</span>Mobile Number</label>
	                    </div>
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">phone</i>
	                        <input id="teleNumber" type="text" class="validate mobileNumber" name="telephoneNo"  value="${company.contact.telephoneNumber}"   minlength="10" maxlength="15">
	                        <label for="teleNumber" class="active"><!-- <span class="red-text">*</span> -->Telephone Number</label>
	                    </div>
	                    </div>.
	                    <div class="row" style="margin-bottom:0">
	                   <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">email</i>
	                        <input id="emailId" type="email" class="validate" value="${company.contact.emailId}" name="emailId" >
	                        <label for="emailId" class="active"><!-- <span class="red-text">*</span> -->Email Id</label>
	                    </div>
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">person</i>
	                        <input id="userId" type="text" class="validate" name="userId" value="${company.userId}"  required>
	                        <label for="userId" class="active"><span class="red-text">*</span>User Id</label>
	                    </div>
	                    </div>
	                    
	                     
	                     <div class="row" style="margin-bottom:0">
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">lock</i>
	                        <input id="password" type="password" class="validate" value="${company.password}" name="password" required>
	                        <label for="password" class="active"><span class="red-text">*</span>Password</label>
	                    </div>
	                    <div class="input-field col s12 m1 l1" style="transform:translate(0,10px)">
	                        <i class="fa fa-eye  right eye" aria-hidden="true"></i>
	                        <i class="fa  fa-eye-slash right eye-slash" aria-hidden="true"></i>
                   		 </div>
                   		 
	                     <div class="input-field col s11 m5 l5">
	                        <i class="material-icons prefix">lock</i>
	                        <input id="Cnfrmpassword" type="password" class="validate" value="${company.password}"  name="confirmpass" required>
	                        <label for="Cnfrmpassword" class="active"><span class="red-text">*</span>Confirm Password</label>
	                    </div>
	                     <div class="input-field col s12 m1 l1  pull-l1 pull-m1 pull-s1" style="transform:translate(0,10px)">
                        <i class="fa fa-eye   eye1 right" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash  eye-slash1 right" aria-hidden="true"></i>
                   
                    	</div>
                    	</div>
	                    <div class="row" style="margin-bottom:0">
	                     <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                         <i class="material-icons prefix">location_on<!-- <span class="red-text">*</span> --></i>
	                        
	                        <select id="countryid" name="countryId">
	                                 <option value="">Choose Country</option>
	                                <c:if test="${not empty countryList}">
								<c:forEach var="listValue" items="${countryList}">							
									<option value="<c:out value="${listValue.countryId}" />"><c:out
											value="${listValue.name}" /></option>
								</c:forEach>
							</c:if>
	                        </select>
	                    </div>
	                   <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                        <i class="material-icons prefix">location_on<!-- <span class="red-text">*</span> --></i>
	                         
	                        <select id="stateid" name="stateId">
	                                 <option value="">Choose State</option>
	                        </select>
	                    </div>
	                    </div>
	                    <div class="row" style="margin-bottom:0">
	                    <div class="input-field col s12 m5 l5 push-l1 push-m1">
	                    <i class="material-icons prefix">location_on<!-- <span class="red-text">*</span> --></i>
	                        <select id="cityid" name="cityId">
	                                 <option value="">Choose City</option>
	                        </select>
	                    </div>
	                    </div>
	                    <div class="input-field col s12 m10 l10 push-l1 push-m1">
	                        <table class="centered tblborder" id="cityCart">
	                            <thead>
	                                <tr>
	                                    <th>Sr.No</th>
	                                    <th>CityName</th>
	                                    <th>Remove</th>
	                                </tr>
	                            </thead>
	                            <tbody id="cityTable">
	                            </tbody>
	                        </table>
	                        <br><br>
	                          <input id="citylistinput" type="hidden" class="validate" title="Add atleast one product" name="cityIdList" required>
	                    </div>
                  </div>
                    <br>
					  
                    <div class="input-field col s12 m6 l6 offset-l3  offset-m3 center-align">
                        <button class="btn waves-effect waves-light blue-gradient" type="submit" id="updateCompany">Update Company<i class="material-icons right">send</i> </button>
                        <br><br>
                    </div>

                </div>


            </form>
        </div>
       
    	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center success  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>    
    </main>
    <!--content end-->
</body>

</html>