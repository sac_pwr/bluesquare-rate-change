<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>

    <script>
    var IsFull=true;
    var IsCash=true;
    
        $(document).ready(function() {
        	/*  var dateDisable = $('.datepicker').pickadate();
        	var picker = dateDisable.pickadate('picker');
        	   var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
        	   picker.set( 'disable', { from: [0,0,0], to: yesterday } ); */
        	  
        	   
            	 
        	   $('.partialAmount').hide();
            $('.chequeNo').hide();
        	   $('#full').click(function() {
               	IsFull=true;               	
                   if (IsCash==true) { 
                	   $('#cash').click();
                   }
                   else{
                	   $('#cheque').click();
                   }
                  
               });
               $('#partial').click(function() {
               	IsFull=false;
               	if (IsCash==true) { 
             	   $('#cash').click();
                }
                else{
             	   $('#cheque').click();
                }
                  
               });
            $('#cash').click(function() {
            	IsCash=true;
            	 $('.chequeNo').hide();
                if ( $("#full").is(':checked') ) {
                	$('.partialAmount').hide();
                	$('.amount').show();
                	 $('.dueDate').addClass('offset-l3 offset-m3');
                }else{
                	$('.partialAmount').show();
                	$('.amount').hide();
                	
                	$('.dueDate').addClass('offset-l3 offset-m3');
                }
               
               
            });
            $('#cheque').click(function() {
            	IsCash=false;
            	$('.chequeNo').show();
            	if ( $("#full").is(':checked') ) {
                	$('.partialAmount').hide();
                	$('.amount').show();
                	 $('.dueDate').removeClass('offset-l3 offset-m3');
                }else{
                	$('.partialAmount').show();
                	$('.amount').hide();
                	
                	$('.dueDate').removeClass('offset-l3 offset-m3');
                }               
            });
            
            $('#cheqNo').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});

           
            
            $('#amount').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('amount').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }	
			
		  $('#partialAmounttxt').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('partialAmounttxt').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }	
			
            $('#paySubmit').click(function(){
            	var amountFullPaid1=$('#amount').val();
    			var amountFullPaid=amountFullPaid1.trim();
    			var partialAmounttxt=$('#partialAmounttxt').val();
				var bankName1=$('#bankName').val().trim();
				var bankName=bankName1.trim();
				var cheqNo1=$('#cheqNo').val()
				var cheqNo=cheqNo1.trim();
				var cheqDate=$('#cheqDate').val();
				var dueDate=$('#dueDate').val();
				var today=new Date().setHours(0,0,0,0);
				var maxAmt="${payment.amountBalance+payment.amountPaid}";
				
			 /* <input type="hidden" name="cashCheckStatus">
            	<input type="hidden" name="fullPartialPayment">
            	<input type="hidden" name="dueDate">
            	<input type="hidden" name="bankName">
            	<input type="hidden" name="checkNo">
            	<input type="hidden" name="checkDate">
            	<input type="hidden" name="paidAmount"> */
            	
				if(IsFull){
					
					$('#fullPartialPayment').val('Full Payment');
					$('#paidAmount').val(amountFullPaid);
					
					if(amountFullPaid=="" || !(amountFullPaid>0)){
						Materialize.toast('Please Enter amount!', '4000', 'teal lighten-3');
						return false;
					}
					
					if(parseFloat(amountFullPaid)!=parseFloat(maxAmt)){
						   Materialize.toast('Please Enter Full Amount : '+maxAmt, '2000', 'teal lighten-2');
						   return false;
					}					   
			   }else{
				   
				   $('#fullPartialPayment').val('Partial Payment');
				   $('#paidAmount').val(partialAmounttxt);
				   
				   if(partialAmounttxt=="" || !(partialAmounttxt>0)){
						Materialize.toast('Please Enter amount!', '4000', 'teal lighten-3');
						return false;
					}
				   
				   if(parseFloat(partialAmounttxt)>=parseFloat(maxAmt)){
					   Materialize.toast('Please Enter Partial Amount', '2000', 'teal lighten-2');
					   return false;
					}				   
			   }
            	
            	$('#dueDate1').val(dueDate);
            	
				if(IsCash==false)
				{
					$('#cashCheckStatus').val("Cheque");					
					$('#bankName1').val(bankName);
					$('#checkNo').val(cheqNo);
					$('#checkDate').val(cheqDate);
					
					if(bankName===""){
						
						Materialize.toast('Please Enter Bank Name!', '4000', 'teal lighten-3');
						return false;
					}
					if(cheqNo==="")
					{
						Materialize.toast('Please Enter cheque number!', '4000', 'teal lighten-3');
						/* $('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Enter bank name"); */
						return false;
				     }
					
					if(cheqDate==="")
					{
						Materialize.toast('Please Enter cheque date!', '4000', 'teal lighten-3');
						/* $('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message");
					     $('#msg').text("Enter cheque date"); */
						return false;
					}
					
					
					var chDate=new Date(cheqDate).setHours(0,0,0,0);
					
					if(chDate < today)
					{
						Materialize.toast('Select Proper Cheque Date', '4000', 'teal lighten-3');
						 /* $('#addeditmsg').modal('open');
					     $('#msgHead').text("Payment Message"); 
					     $('#msg').text("Select Cheque Date After Current Date"); */
						return false;
					}
					
				}
				else
				{
					$('#cashCheckStatus').val("Cash");					
					//$('#bankName').val('');
					//$('#cheqDate').val('');
				}
			
				var duDate=new Date(dueDate).setHours(0,0,0,0);
				
				if(duDate < today)
				{
					Materialize.toast('Select Proper Due Date', '4000', 'teal lighten-3');
					 /* $('#addeditmsg').modal('open');
				     $('#msgHead').text("Payment Message"); 
				     $('#msg').text("Select Cheque Date After Current Date"); */
					return false;
				}				
            });
            
			var fullPartialStatus="${payment.fullPartialStatus}";
			if(fullPartialStatus=="Full Payment"){
				$('#full').click();
			}else{
				$('#partial').click();
			}			
			
			var cashChequeStatus="${payment.cashChequeStatus}";
			if(cashChequeStatus=="Cash"){
				 $('#cash').click();
			}else{
				 $('#cheque').click();
			}
			
			var chequeDate="${payment.chequeDate}";
			var dueDate="${payment.dueDate}";

		   var $chequeDate = $('#cheqDate').pickadate();
		   var $dueDate = $('#dueDate').pickadate(); 
			
		   // Use the picker object directly.
		   var chequeDatepicker = $chequeDate.pickadate('picker');
		   var dueDatePicker = $dueDate.pickadate('picker');
		   
		   chequeDatepicker.set('select', chequeDate, { format: 'yyyy-mm-dd' });
		   dueDatePicker.set('select', dueDate, { format: 'yyyy-mm-dd' });

		   /* $('.amount1').keyup(function(){
			   var id = $(this).attr('id');
			   var maxAmt="${payment.amountBalance+payment.amountPaid}";
			   var enteredAmt=$('#'+id).val();
			   
			   $('.amount1').val(enteredAmt);
			   
			   if(IsFull){
				   
				   if(IsCash){
					   if(parseFloat(enteredAmt)!=parseFloat(maxAmt)){
						   Materialize.toast('Entered Amount Must Be Equal to '+maxAmt, '2000', 'teal lighten-2');
						   return false;
					   }
				   }else{
					   
				   }
				   
			   }else{
				   
				   if(IsCash){
					   
				   }else{
					   
				   }
				   
			   }
			   
			   if(parseFloat(enteredAmt)>parseFloat(maxAmt)){
				   Materialize.toast('Entered Amount Exceeds Total. Max Pay : '+maxAmt, '2000', 'teal lighten-2');
				   return false;
			   } 
		   }); */
		   
		   
		   $('#partialAmounttxt').keyup(function(){
			   
			   var parttialAmtEntered= $('#partialAmounttxt').val();
			   var maxAmt="${payment.amountBalance+payment.amountPaid}";
			   
			   $('#balAmt').val(parseFloat(maxAmt)-parseFloat(parttialAmtEntered));
			   
		   });
		   
        });
    </script>
    <style>
    
    .card{
     height: 2.5rem;
     line-height:2.5rem;
     }
    .card-image{
        	width:40% ;
        	background-color:#0073b7 !important;
        }
	.card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}
    </style>
</head>

<body>
     <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br> 
        <div class="row">
          <!--   <div class="col l12 m12 s12">
                <h3 class="center"> Payment Details </h3>
            </div> -->
            <div class="row">
            <div class="col s12 m12 l12">
            	
            		<div class="col s6 m4 l4">
            			
            			<div class="card horizontal">
					      <div class="card-image">
					         <h6 class="white-text center">Shop Name</h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p><h6 class="center-align"><c:out value="${payment.customerShopName}" /></h6>
								</p>	        
					        </div>
            	          </div>	
            		 </div>	
            			  
            		</div>
            		<div class="col s6 m4 l4">
            			
            			<div class="card horizontal">
					      <div class="card-image">
					           <h6 class="white-text center">Order Id</h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					            <h6 class="center-align"><c:out value="${payment.orderId}" /></h6>
								</p>	        
					        </div>
            	          </div>	
            		 </div>	
            			  
            		</div>
            	
            		<div class="col s6 m4 l4">
            			
            			<div class="card horizontal">
					      <div class="card-image">
					           <h6 class="white-text center">Mobile No</h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					            <h6 class="center-align"><c:out value="${payment.mobileNumber}" /></h6>
								</p>	        
					        </div>
            	          </div>	
            		 </div>	
            			  
            		</div>
            		
            	</div>
            		 <%-- <div class="col s12 m12 l12">	
            		<div class="col s12 m12 l12">
            		   <div class="card horizontal">
					      <div class="card-image" style="width:13%;">
					         <h6 class="white-text center">Address </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p><h6 class="center-align"><c:out value="${payment.name}" /> </h6></p>
					        </div>
            	          </div>	
            		 </div>	
            			<h5 class="blue-text text-darken-8  center-align">Name <br> <c:out value="${payment.name}" /> </h5>
            		</div>
            		</div> --%>
            		<div class="col s12 m12 l12">
            		<div class="col s6 m4 l4">
            			<div class="card horizontal">
					      <div class="card-image">
					         <h6 class="white-text center">Total Amount </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					          <h6 class="center-align">
					          <c:out value="${payment.totalAmount}" />
					          <%-- <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${payment.totalAmount}" /> --%>
					          </h6>
					          </p>
					        </div>
            	          </div>	
            		 </div>	
            			
            		</div>
            		
            		<div class="col s6 m4 l4">
            			<div class="card horizontal">
					      <div class="card-image">
					          <h6 class="white-text center">Amount Paid </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					         	<h6 class="center-align">
					         	<c:out value="${payment.totalAmountPaid}" />
					         	<%-- <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${payment.amountPaid}" /> --%>
					         	</h6>
					          </p>
					        </div>
            	          </div>	
            		 </div>	
            			
            		</div>
            		<div class="col s6 m4 l4">
            			<div class="card horizontal">
					      <div class="card-image">
					           <h6 class="white-text center">Balance Amount </h6>
					      </div>
					      <div class="card-stacked grey lighten-3">
					        <div class="card-content">
					          <p>
					          <h6 class="center-align">
					          <b class="red-text">
					          	<%-- <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${payment.amountUnPaid}" /> --%>  
					          	<c:out value="${payment.balanceAmount}" />
					          </b></h6>
					          </p>
					        </div>
            	          </div>	
            		 </div>
            			
            		</div>
            	</div>
            </div>
            
            
            
            
          
            <div class="col s12 m12 l12 center-align">
            	<div class="col s12 m12 l12 z-depth-3 grey lighten-4">
                	
                    <div class="col l12 m12 s12 chqcash" style="margin-top:10px;">
                    <h5> Payment Mode</h5><br/>
                    <div class="col s12 m12 l12">
                    	    <div class="col s6 m1 l1 offset-l5 offset-m5">
                    	    <input type="radio" id="full" name="group2" checked /> 
                       		 <label for="full">Full</label>
                    	    </div>	             
                        
                        <div class="col s6 m1 l1">
                        <input type="radio" id="partial" name="group2"/> 
                        <label for="partial">Partial</label>
                        </div>	
                       
                    </div>
                    <div class="col s12 m12 l12">
                    	<div class="col s6 m1 l1 offset-l5 offset-m5">                  
                        
                        <input type="radio" id="cash" name="group1" checked /> 
                        <label for="cash">Cash</label>
                        </div>
                         <div class="col s6 m1 l1">
                        <input type="radio" id="cheque" name="group1"/> 
                        <label for="cheque">Cheque</label>
                        </div>
                    </div>
                        
                        
                    </div>
                    <br>
                    <div class="col s12 m12 l12 center-center-align fullDiv">                    
                    <div class="input-field col l2 m2 s6 left chequeNo">
                        <input type="text" name="bankName" class="validate" id="bankName" title="Enter Bank Name" value="${payment.bankName}"> <label for="customerBankName">Bank Name</label>
                    </div>
					 <div class="input-field col l2 m2 s6 offset-l3 offset-m3 left dueDate">
                    <input type="text" id="dueDate" class="datepicker disableDate" name="dueDate">
		                     <label for="dueDate" class="black-text">Due Date</label>
                    </div>
                      <div class="input-field col s6 m4 l4 center amount">
                        <input type="text" name="amount" id="amount" class="amount1" title="Enter Amount" value="${payment.amountPaid}" required>
                        <label for="amount"><span class="red-text">*</span>Amount</label>
                    </div>
                    <div class="col s6 m4 l4 center partialAmount" style="padding:0">	
                      <div class="input-field col s6 m6 l6">
                        <input type="text" name="amount" class="amount1" id="partialAmounttxt" title="Enter Amount" value="${payment.amountPaid}" required>
                        <label for="partialAmounttxt"><span class="red-text">*</span>Partial Amount</label>
                    </div>
                    <div class="input-field col s6 m6 l6 center">
                        <input type="text" name="balamount" id="balAmt" title="Enter Amount" value="${payment.amountBalance}" readonly="readonly">
                        <label for="balAmt"><span class="red-text">*</span>Balance Amount</label>
                    </div>
                    </div>
                    <%-- <div class="col s6 m4 l4 center partialAmount" style="padding:0">	
                      <div class="input-field col s6 m6 l6">
                        <input type="text" name="partialAmt" class="amount" id="partialAmt" title="Enter Amount" value="${payment.amountPaid}" required>
                        <label for="partialAmt"><span class="red-text">*</span>Partial Amount</label>
                    </div>
                    <div class="input-field col s6 m6 l6 center">
                        <input type="text" name="balamount" id="balamount" title="Enter Amount" value="${payment.amountBalance}" required>
                        <label for="balamount"><span class="red-text">*</span>Balance Amount</label>
                    </div>
                    </div> --%>
                    <div class="input-field col l2 m4 s6   chequeNo">
                        <input type="text" name="cheqNo" class="validate num" value="${payment.chequeNumber}" id="cheqNo" maxlength="6" minlength="6" title="Enter Cheque Number">
                         <label for="customerChequeNumber">Cheque Number</label>
                    </div>
                    
                    <div class="input-field col l2 m4 s6 right chequeNo">
	                     <input type="text" id="cheqDate" class="datepicker disableDate" name="cheqDate">
	                     <label for="cheqDate" class="black-text">Cheque Date</label>
                    </div> 
                  
                  </div>
                  
                    
                    <br>
                   
                    <br>
                    <div class="col s12 m12 l12 center center-align">
                   
                    </div>
                    <br><br>
                    <div class="input-field col s6 m12 l12 center center-align">
                   	<form action="${pageContext.servletContext.contextPath}/updateOrderDetailsPayment" method="post">
	                	<input value="${payment.orderId}" type="hidden" name="orderId">
	                	<input value="${payment.paymentId}" type="hidden" name="paymentId">
	                	
	                	<input type="hidden" id="cashCheckStatus" name="cashCheckStatus">
	                	<input type="hidden" id="fullPartialPayment" name="fullPartialPayment">
	                	<input type="hidden" id="dueDate1" name="dueDate">
	                	<input type="hidden" id="bankName1" name="bankName">
	                	<input type="hidden" id="checkNo" name="checkNo">
	                	<input type="hidden" id="checkDate" name="checkDate">
	                	<input type="hidden" id="paidAmount" name="paidAmount">
                    
                        <button id="paySubmit" class="btn waves-effect waves-light" type="submit">Pay<i class="material-icons right">send</i></button>
                        
                        <br><br>
                    </form>
                    </div>
                    <br><br>
                
            </div>
				</div>
        </div>









        <br>
        
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
       	<!-- <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>	
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
    </main>





    <!--content end-->
</body>

</html>