<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html lang="en">
<head>
<%@include file="components/header_imports.jsp" %>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
<script type="text/javascript" src="resources/js/locationscript_admin.js"></script>
<script>
/* document.addEventListener("DOMContentLoaded", function(){
	
	alert();
	$('.preloader-background').delay(1700).fadeOut('slow');
	
	$('.preloader-wrapper')
		.delay(1700)
		.fadeOut();
}); */
</script>
<style>
/* div.dataTables_filter {
     margin: 0 0 !important; 
} */
label{
	color:black;
}
.row .col.l12 {
    width: 100%;
    margin-left: auto;
    left: auto;
    right: auto;
    padding-bottom:50px;
}
#addArea,
#addRegion,
#addCity,
#addState,
#addCountry
{
	 text-transform: uppercase;
}
</style>

</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    
     <!-- <div class="preloader-background">
    <div class="preloader-wrapper big active">
      <div class="spinner-layer spinner-blue">
        <div class="circle-clipper left">
          <div class="circle"></div>
        </div><div class="gap-patch">
          <div class="circle"></div>
        </div><div class="circle-clipper right">
          <div class="circle"></div>
         
        </div>
      </div>
     <div class="red-text"><br><h6>Wait.....</h6></div> 
    </div>
     
      </div>  -->
	<!-- <div class="preloader-wrapper big active">
		<div class="spinner-layer spinner-blue-only">
			<div class="circle-clipper left">
				<div class="circle"></div>
			</div>
			<div class="gap-patch">
				<div class="circle"></div>
			</div>
			<div class="circle-clipper right">
				<div class="circle"></div>
			</div>
		</div>
	</div> -->

    
    <main class="paddingBody">
        <br>
	<div class="row">
	<!-- 	<div class="col s12 l12 m12"> -->
			<ul class="tabs tabs-fixed-width">
				<!-- <li class="tab col s3 left" style="margin-left:0;left:0"><a class="active left" href="#area" id="areawork">Area</a></li>
				<li class="tab col s3"><a href="#region" id="regionwork">Region</a></li> -->
				<li class="tab col s3"><a href="#city" id="citywork">City</a></li>
				<li class="tab col s3"><a href="#state" id="statework">State</a></li>
				<li class="tab col s3" style="padding-right:0"><a href="#country" id="fetchCountry">Country</a></li>
			</ul>
		<!-- </div> -->
	</div>
	<%-- <!--AREA START-->
	<div id="area">
		<form  action="${pageContext.servletContext.contextPath}/saveArea" method="post" id="saveAreaForm">
			<!--Add New Area-->
			<div class="row">
			<div class="col s12 m12 l12 formBg">
			<div class="input-field col s10 l2 m2" >
					<select id="countryListForArea" name="countryId">
						<option value="0" selected>Choose Country</option>
					</select>

			</div>
			<div class="input-field col s10 l2 m2 ">
					<select id="stateListForArea" name="stateId">
						<option value="0" selected>Choose State</option>
					</select>

				</div>
				<div class="input-field col s10 l2 m2">
					<select id="cityListForArea" name="cityId">
						<option value="0" selected>Choose City</option>
						
					</select>

				</div>
				<div class="input-field col s10 l2 m2">
					<select id="regionListForArea" name="regionId">
						<option value="0" selected>Choose Area</option>
						
					</select>

				</div>
				<div class="input-field col s10 l2 m2">
				<input value="0" id="addAreaId" type="hidden" class="validate" name="areaId">
					<input value="" id="addArea" type="text" class="validate" name="name" required>
					<label for="addArea">Enter Area Name</label>
				</div>
				<div class="input-field col s10 l2 m2">
				
					<input value="" id="addpincode" type="text" class="validate" name="pincode" maxlength="6" minlength="6">
					<label for="addpincode" required>Enter Pincode</label>
				</div>
				<div class="col s10 l2 m3 offset-l4" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="saveAreaSubmit">
						<i class="material-icons left">add</i>Add
					</button>
				</div>
				<div class="col s10 l3 m3" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="resetAreaSubmit"><i class="material-icons left">refresh</i>
						Reset
					</button>					
				</div>		
			</div>
			</div>
		</form>
		<div class="col s12 l12 m12">
			<table class="striped highlight centered " id="tblDataArea" width="100%">
				<thead>
					<tr>
						<th class="print-col">Sr.  No.</th>
						<th class="print-col">Area Name</th>
						<th class="print-col">Pincode</th>
						<th class="print-col">Region</th>
						<th class="print-col">City</th>
						<th class="print-col">State</th>
						<th class="print-col">Country</th>
						<th>Edit</th>
						<!-- <th>Delete</th> -->

					</tr>
				</thead>

				<tbody>
					<tr>
						<td>1</td>
						<td>abc</td>
						<td>abc</td>
						<td>abc</td>
						<td>abc</td>
						<td>abc</td>
						<td>abc</td>
						<td><a href="#" class="btn-flat"><i class="material-icons tooltipped"
								data-position="right" data-delay="50" data-tooltip="Edit">edit</i></a></td>
						<!-- Modal Trigger -->
						<!-- <td><a href="#delete" class="modal-trigger"><i
								class="material-icons tooltipped" data-position="right"
								data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
					</tr>
					<!-- Modal Structure for delete -->


				</tbody>
			</table>

			
		</div>
	</div>
	<!--AREA END-->
	
	 <!--REGION START-->
	<div id="region">
		<form   action="${pageContext.servletContext.contextPath}/saveRegion" method="post" id="saveRegionForm">
			<!--Add New Region-->
			<div class="row"><!-- style="display: none;" -->
			<div class="col s12 m12 l12 formBg">
			<div class="input-field col s10 l2 m2  offset-s2" >
					<select id="countryListForRegion" name="countryId">
						<option value="0" selected>Choose Country</option>
					</select>

				</div>
			<div class="input-field col s10 l2 m2  offset-s2">
					<select id="stateListForRegion" name="stateId">
						<option value="0" selected>Choose State</option>
					</select>

				</div>
				<div class="input-field col s10 l2 m2  offset-s2">
					<select id="cityListForRegion" name="cityId">
						<option value="0" selected>Choose City</option>
						
					</select>

				</div>
				<div class="input-field col s10 l2 m2  offset-s2">
				<input value="0" id="addRegionId" type="hidden" class="validate" name="regionId">
					<input value="" id="addRegion" type="text" class="validate" name="name">
					<label for="addArea">Enter Region Name</label>
				</div>
				<div class="col s10 l2 m2" style="margin-top:2%;width:14%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="saveRegionSubmit">
						<i class="material-icons left">add</i>Add
					</button>
				</div>
				<div class="col s10 l2 m2" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="resetRegionSubmit"><i class="material-icons left">refresh</i>
						Reset
					</button>					
				</div>		
			</div>
			</div>
		</form>
		<div class="col s12 l12 m12">
			<table class="striped highlight centered " id="tblDataRegion" width="100%">
				<thead>
					<tr>
						<th class="print-col">Sr.  No.</th>
						<th class="print-col">Region Name</th>
						<th class="print-col">City Name</th>
						<th class="print-col">State Name</th>
						<th class="print-col">Country Name</th>
						<th>Edit</th>
						<!-- <th>Delete</th> -->

					</tr>
				</thead>

				<tbody>
					<tr>
						<td>1</td>
						<td>abc</td>
						<td>abc</td>
						<td>abc</td>

						<td>abc</td>
						<td><a href="#"><i class="material-icons tooltipped"
								data-position="right" data-delay="50" data-tooltip="Edit">edit</i></a></td>
						<!-- Modal Trigger -->
						<!-- <td><a href="#delete" class="modal-trigger"><i
								class="material-icons tooltipped" data-position="right"
								data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
					</tr>
					<!-- Modal Structure for delete -->

					
				</tbody>
			</table>
		</div>
	</div>
	<!--REGION END-->  --%>
	<!--CITY START-->
	<div id="city">
		<form  action="${pageContext.servletContext.contextPath}/saveCity" method="post" id="saveCityForm">
			<!--Add New City-->
			<div class="row">
			<div class="col s12 m12 l12 formBg">
				<div class="input-field col s10 l2 m2  offset-s2">
					<select id="countryListForCity" name="coutryId">
						<option value="0" selected>Choose Country</option>
					</select>
				</div>
				<div class="input-field col s10 l2 m2  offset-s2">
					<select id="stateListForCity" name="stateId">
						<option value="0" selected>Choose State</option>
					</select>

				</div>
				<div class="input-field col s10 l2 m2  ">
					<input value="0" id="addCityId" type="hidden" class="validate" name="cityId">
					<input value="" id="addCity" type="text" class="validate" name="name">
					<label for="addArea">Enter City Name</label>
				</div>
				<div class="col s10 l2 m2" style="margin-top:2%;width:14%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="saveCitySubmit">
						<i class="material-icons left">add</i>Add
					</button>					
				</div>	
				<div class="col s10 l2 m2" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="resetCitySubmit"><i class="material-icons left">refresh</i>
						Reset
					</button>					
				</div>					
			</div>
			</div>
		</form>
		<div class="col s12 l12 m12">
			<table class="striped highlight centered " id="tblDataCity" width="100%">
				<thead>
					<tr>
						<th class="print-col">Sr. No.</th>
						<th class="print-col">City Name</th>
						<th class="print-col">State Name</th>
						<th class="print-col">Country Name</th>
						<th>Edit</th>
						<!-- <th>Delete</th> -->
					</tr>
				</thead>

				<tbody>
					<!-- <tr>
						<td>1</td>
						<td>abc</td>
						<td>abc</td>
						<td>abc</td>
						<td><a href="#"><i class="material-icons tooltipped"
								data-position="right" data-delay="50" data-tooltip="Edit">edit</i></a></td>
						Modal Trigger
						<td><a href="#delete" class="modal-trigger"><i
								class="material-icons tooltipped" data-position="right"
								data-delay="50" data-tooltip="Delete">delete</i></a></td>
					</tr> -->
					<!-- Modal Structure for delete -->

					
				</tbody>
			</table>
		</div>
	</div>
	<!--CITY END--> 
	<!--STATE START-->
	<div id="state">

		<form action="${pageContext.servletContext.contextPath}/saveState" method="post" id="saveStateForm">
			<!--Add New State-->
			<div class="row">
			<div class="col s12 m12 l12 formBg">
				<div class="input-field col s10 l2 m2  offset-s2">
					<select id="countryListForState" name="countryId">
						<option value="0" selected>Choose Country</option>
					</select>

				</div>


				<div class="input-field col s10 l2 m2  offset-s2">
					<input value="0" id="addStateId" type="hidden" class="validate" name="stateId">
					<input value="" id="addState" type="text" class="validate" name="name">
					<label for="addArea">Enter State Name</label>
				</div>
				<div class="input-field col s10 l2 m2  offset-s2">
					<input value="" id="addStateCodeId" type="text" class="validate" name="code">
					<label for="addArea">Enter State Code</label>
				</div>
				<div class="col s10 l2 m2" style="margin-top:2%;width:14%;">
					<button class="btn waves-effect waves-light blue-gradient"
						id="saveStateSubmit" type="button" name="action" >
						<i class="material-icons left">add</i>Add
					</button>
				</div>
				<div class="col s10 l2 m2" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="resetStateSubmit"><i class="material-icons left">refresh</i>
						Reset
					</button>					
				</div>		
			</div>
			</div>
		</form>
		<div class="col s12 l12 m12">
			<table class="striped highlight centered " id="tblDataState" width="100%">
				<thead>
					<tr>
						<th class="print-col">Sr. No.</th>
						<th class="print-col">State Name</th>
						<th class="print-col">State Code</th>
						<th class="print-col">Country Name</th>
						<th>Edit</th>
						<!-- <th>Delete</th> -->

					</tr>
				</thead>

				<tbody>
		<!-- 			<tr>
						<td>1</td>
						<td>abc</td>
						<td>abc</td>
						<td><a href="#"><i class="material-icons tooltipped"
								data-position="right" data-delay="50" data-tooltip="Edit">edit</i></a></td>
						Modal Trigger
						<td><a href="#delete" class="modal-trigger"><i
								class="material-icons tooltipped" data-position="right"
								data-delay="50" data-tooltip="Delete">delete</i></a></td>
					</tr> -->
					<!-- Modal Structure for delete -->

					
				</tbody>
			</table>
		</div>
	</div>
	<!--STATE END--> <!--COUNTRY START-->
	<div id="country">
		<form action="${pageContext.servletContext.contextPath}/saveCountry" method="post" id="saveCountryForm"> 
			<!--Add New Contry-->
			<div class="row">
			<div class="col s12 m12 l12 formBg">
				<div class="input-field col s10 l3 m6  offset-s2 offset-l3">
				<input value="0" id="addCountryId" type="hidden" class="validate" name="countryId">
					<input value="" id="addCountry" type="text"  name="name" required>
					<label for="addArea" class="active">Enter Country Name</label>
				</div>
				<div class="col s10 l2 m2" style="margin-top:2%;width:14%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button"  id="saveCountrySubmit" name="action">
						<i class="material-icons left">add</i>Add
					</button>
				</div>	
				<div class="col s10 l2 m2" style="margin-top:2%;">
					<button class="btn waves-effect waves-light blue-gradient"
						type="button" name="action" id="resetCountrySubmit"><i class="material-icons left">refresh</i>
						Reset
					</button>					
				</div>
			</div>
			</div>
		</form>
		<div class="col s12 l12 m12 " id="dataAll">
			<table class="striped highlight centered " id="tblDataCountry" width="100%">
				<thead>
					<tr>
						<th class="print-col">Sr. No.</th>
						<th class="print-col">Country Name</th>
						<th>Edit</th>
						<!-- <th>Delete</th> -->

					</tr>
				</thead>

				<tbody id="countryList">
					<!-- <tr>
                            <td>1</td>
                            <td>abc</td>
                            <td><button onclick=""><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></button></td>
                            
                            <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td>
                        </tr> -->
					<!-- Modal Structure for delete -->


				</tbody>
			</table>

		</div>
	</div>
	<!--COUNTRY END--> </main>
	<!--content end-->
	 <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center   white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!--  <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect btn">OK</a>
					</div>
				</div>
			</div>
		</div>
	<!-- <div class="row">
			<div class="col s8 m2 l2">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead">Error</h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect  blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
</body>

</html>