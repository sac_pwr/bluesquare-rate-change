<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    
	    $(document).ready(function() {
	    	
	    	var msg="${saveMsg}";
	       	 if(msg!='' && msg!=undefined)
	       	 {
	       		 $('#addeditmsg').find("#modalType").addClass("success");
	 			$('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	       	     $('#addeditmsg').modal('open');
	       	     /* $('#msgHead').text("HRM Message"); */
	       	     $('#msg').text(msg);
	       	 }
	    	
	    	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   	         "oLanguage": {
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0 },
	   	                      { 'width': '2%', 'targets': 1},
	   	                  	  { 'width': '5%', 'targets': 2},
	   	                	  { 'width': '5%', 'targets': 3},
	   	             		  { 'width': '3%', 'targets': 4},
	   	                      { 'width': '1%', 'targets': 5},
	   	                  	  { 'width': '3%', 'targets': 6},
	   	                	  { 'width': '5%', 'targets': 7},
	   	             		  { 'width': '8%', 'targets': 8},
	   	             		  { 'width': '5%', 'targets': 9}
	   	                     ],
	   	         lengthMenu: [
	   	             [50, 75., 100, -1],
	   	             ['50 ', '75 ', '100 ', 'All']
	   	         ],
	   	         
	   	        
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [30,40,50,70,40,40,30,40,50,60,50] 
		                    		 } 
		                    		    })
	   	       
	   	                         doc.pageMargins = [5,20,10,5];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select")
	             .change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
           //$(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
           /*  $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            }); */
           
            $(".rangeSelect").click(function() {
           
                $(".showDates").show();
              //  $(".showQuantity").hide();
                $("#oneDateDiv").hide();
            });
            $(".pickdate").click(function(){
            	//$(".showQuantity").hide();
   	   		 $(".showDates").hide();
   	   		$("#oneDateDiv").show();
   	   	});
            
            
            //hide column depend on login
            var table = $('#tblData').DataTable(); // note the capital D to get the API instance
            var column = table.columns('.toggle');
            console.log(column);
            if(${sessionScope.loginType=='CompanyAdmin'}){            
                column.visible(true);
            }else{
            	 column.visible(false);
            }
           
        });
	    
	   

    </script>
     <style>
     .card{
     height: 2.5rem;
     line-height:2.5rem;
    /*  text-align:center !important; */
     }
    .card-image{
        	width:35% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
#oneDateDiv{
	margin-top: 0.5rem;
	margin-left:4%
}
td .btn-flat{
	color:unset !important;
	font-size:0.8rem !important;
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main  class="paddingBody">
        <br>
         <!--Add Employee-->
         
        <div class="row">
        <div class="col s10 l2 m6 left" style="padding-left: 2px;">
             <a class="btn waves-effect waves-light blue-gradient left" href="${pageContext.request.contextPath}/addExpense"><i class="material-icons left" >add</i>Add Expense</a>
         </div>
         <c:if test="${sessionScope.loginType=='CompanyAdmin'}">
	         <div class="col s10 l3 m6">
	             <a class="btn waves-effect waves-light blue-gradient left" href="${pageContext.request.contextPath}/expense_type_page"><i class="material-icons left" >add</i>Add Expense Type</a>
	         </div>
         </c:if>
           <div class="col s12 m3 l3 right right-align" style="margin-top:0.6%;width:15%;">
           
            <!-- <div class="col s6 m7 l7 right right-align" style="margin-top:3%;"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>                    
                     <li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=today">Today</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/fetchExpenseList?range=viewAll">View All</a></li>
                    </ul>
              
               </div>
                 
               <div class="input-field col s12 l6 m6 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchExpenseList" method="post">
	                    <div class="input-field col s12 m2 l4">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
                <div class="input-field col s12 l6 m6 right" style="margin-top:0.5rem;">      
           		 <form action="${pageContext.servletContext.contextPath}/fetchExpenseList" method="post">
                    <input type="hidden" name="range" value="range"> 
                    	  <span class="showDates">
                              <div class="input-field col s6 m2 l3">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s6 m2 l3">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s6 m2 l2">
                            <button type="submit">View</button>
                            </div>
                          </span>
                </form>                
         </div>
                 
        
        
         
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>
                            <th class="print-col">Expense Id</th>
                            <th class="print-col">Description</th>
                            <th class="print-col">Reference</th>
                            <th class="print-col">User</th>
                            <th class="print-col">Amount</th>
                            <th class="print-col">Payment Mode</th>
                            <th class="print-col">Added Date</th>
                          	<th class="print-col">Updated Date</th>
                          	<th class="toggle">Action</th>
                        </tr>
                    </thead>

                    <tbody>
                    <% int rowincrement=0; %>
                    <c:if test="${not empty expenseList}">
					<c:forEach var="listValue" items="${expenseList}">
					<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
	                    <tr>
	                    	<td><c:out value="${rowincrement}" /></td>
	                    	<td><c:out value="${listValue.expenseId}" /></td>
	                    	<td><c:out value="${listValue.description}" /></td>
	                    	<td><c:out value="${listValue.reference}" /></td>
	                    	<td><c:out value="${listValue.userName}" /></td>
	                    	<td><c:out value="${listValue.amount}" /></td>
	                    	<td><c:out value="${listValue.type}" /></td>
	                    	<td>
	                    		<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.addDate}" />
	                    		<c:out value="${dt}" /><br/>
	                    		<fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.addDate}" />
	                    		<c:out value="${time}" />
	                    	</td>
	                    	<td>
	                    		<c:choose>
	                    		<c:when test="${listValue.updateDate!=null}">
									<fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.updateDate}" />
									<c:out value="${dt}" /><br/>
									<fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.updateDate}" />
									<c:out value="${time}" />
								</c:when>
								<c:otherwise>
									NA
								</c:otherwise>
								</c:choose>
							</td>
							<td>
		                    	<a href="${pageContext.request.contextPath}/fetchExpense?expenseId=${listValue.expenseId}" class="btn-flat"><i class="material-icons tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Edit order" >edit</i></a> 
	                    		<a href="#delete_${listValue.expenseId}" class="modal-trigger btn-flat"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a>
		                    </td>
	                    </tr>
	                    <!-- Modal Structure for delete start-->
	                    <div id="delete_${listValue.expenseId}" class="modal deleteModal row">
	                        <div class="modal-content  col s12 m12 l12">
	                        	<h5 class="center-align" style="margin-bottom:30px"><u>Confirmation</u></h5>
	                            <h5 class="center-align">Do You Want to Delete?</h5>
	                            <br/>
	                        </div>
	                        <div class="modal-footer">            
	         				   <div class="col s6 m6 l3 offset-l3">
	                 				<a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
	                			</div>
	            				<div class="col s6 m6 l3">
	             				   <a href="${pageContext.request.contextPath}/deleteExpense?expenseId=${listValue.expenseId}" class="modal-action modal-close waves-effect  btn">Delete</a>
	               				 </div>                
	            			</div>
	                    </div>
	                    <!-- Modal Structure for delete end-->
                    </c:forEach>
                    </c:if>
                    </tbody>
                </table>
            </div>
        </div>
		
		
		<!-- payment list view model end -->
		
    </main>
    <!--content end-->
</body>

</html>