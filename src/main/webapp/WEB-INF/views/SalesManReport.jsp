<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
    
    <script type="text/javascript">
    
    $(document).ready(function(){
    	var table = $('#tblData').DataTable();
  		 table.destroy();
  		 $('#tblData').DataTable({
  	         "oLanguage": {
  	             "sLengthMenu": "Show _MENU_",
  	             "sSearch": "_INPUT_" //search
  	         },
  	      	autoWidth: false,
  	         columnDefs: [
  	                      { 'width': '5%', 'targets': 0 },  	                      
  	                      { 'width': '10%', 'targets': 1},
  	                      { 'width': '25%', 'targets': 2},
  	                      { 'width': '15%', 'targets': 3},
  	                      { 'width': '15%', 'targets': 4},
  	                  	  { 'width': '5%', 'targets': 5},
  	                  	  { 'width': '5%', 'targets': 6}
  	                     
  	                      ],
  	         lengthMenu: [
  	             [10, 25., 50, -1],
  	             ['10 ', '25 ', '50 ', 'All']
  	         ],
  	         
  	        
  	         //dom: 'lBfrtip',
  	         dom:'<lBfr<"scrollDivTable"t>ip>',
  	         buttons: {
  	             buttons: [
  	                 //      {
  	                 //      extend: 'pageLength',
  	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
  	                 //  }, 
  	                 {
  	                     extend: 'pdf',
  	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                     customize: function(doc, config) {
  	                    	doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [30,'*','*','*','*',50] 
	                    		 } 
	                    		    })
  	                         /* var tableNode;
  	                         for (i = 0; i < doc.content.length; ++i) {
  	                           if(doc.content[i].table !== undefined){
  	                             tableNode = doc.content[i];
  	                             break;
  	                           }
  	                         }
  	        
  	                         var rowIndex = 0;
  	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
  	                          
  	                         if(tableColumnCount > 6){
  	                           doc.pageOrientation = 'landscape';
  	                         } */
  	                         /*for customize the pdf content*/ 
  	                         doc.pageMargins = [5,20,10,5];   	                         
  	                         doc.defaultStyle.fontSize = 8	;
  	                         doc.styles.title.fontSize = 12;
  	                         doc.styles.tableHeader.fontSize = 11;
  	                         doc.styles.tableFooter.fontSize = 11;
  	                       doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
  	                       },
  	                 },
  	                 {
  	                     extend: 'excel',
  	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'print',
  	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'colvis',
  	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
  	                     collectionLayout: 'fixed two-column',
  	                     align: 'left'
  	                 },
  	             ]
  	         }

  	     });
  		 $("select")
            .change(function() {
                var t = this;
                var content = $(this).siblings('ul').detach();
                setTimeout(function() {
                    $(t).parent().append(content);
                    $("select").material_select();
                }, 200);
            });
        $('select').material_select();
        $('.dataTables_filter input').attr("placeholder", "Search");
    	$(".showDates").hide();
        $(".rangeSelect").click(function() {
       	 $("#oneDateDiv").hide();
            $(".showDates").show();
        });
   	$("#oneDateDiv").hide();
   	$(".pickdate").click(function(){
   		 $(".showDates").hide();
   		$("#oneDateDiv").show();
   	});
    	
    	var totalAmount=0;
    	
    	var table = $('#tblData').DataTable();	
    	$.fn.dataTable.ext.search.push(function( settings, data, dataIndex ) {
    		       	
    		var searchText=$('.dataTables_filter input').val().toLowerCase();
    		        var salesManId=data[1].trim();
    				var salesManId2=$('#salesManId').val();
    				//alert(salesManId+"-"+salesManId2);
    				if(salesManId2==="SalesMan")
    				{
    					var len=table.columns().nodes().length;
    					for(var i=0; i<len; i++)
    					{
    						var val=data[i].trim().toLowerCase();
    						if(val.includes(searchText))
    						{
    							totalAmount=parseFloat(totalAmount)+parseFloat(data[3].trim());
    							return true;	
    						}
    					}  
    							
    				}
    				
    		        if ( salesManId2 === salesManId )
    		        {
    		        	var len=table.columns().nodes().length;
    					for(var i=0; i<len; i++)
    					{
    						var val=data[i].trim().toLowerCase();
    						if(val.includes(searchText))
    						{
    							totalAmount=parseFloat(totalAmount)+parseFloat(data[3].trim());
    							return true;	
    						}
    					}  
    						
    		        }
    		        return false;
    		        
    	});
    	
	$('#salesManId').change(function(){
			totalAmount=0;
    		table.draw();
    		$('#totalAmountId').text(totalAmount);
    	});
    	
    });
    
    function showAreaDetail(id)
	{
		$.ajax({
			url : "${pageContext.request.contextPath}/fetchEmployeeAreas?employeeDetailsId="+id,
			dataType : "json",
			success : function(data) {
				employeeAreaList=data.areaList;
				var srno=1;
				$("#areaList").empty();
				for(var i=0; i<employeeAreaList.length; i++)
				{					
					arealist=employeeAreaList[i];
					//alert(arealist);
					$('#areaList').append("<tr>"+
					        "<td>"+srno+"</td>"+
					        "<td>"+arealist.area.name+"</td>"+
					        "</tr>"); 
					srno++;
				}
				$('.modal').modal();
				$('#area').modal('open');
			}
		});
	}
    </script>
      <style>
     .card{
     height: 2.5rem;
     line-height:2.5rem;
     }
    .card-image{
        	width:35% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
#oneDateDiv{
	margin-top: 0.5rem;
	margin-left:4%
}
	#area{
		width:35% !important;
	}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>        
        <div class="row">
        	<div class="col s12 l4 m4 left">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Total Amount </h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="">&#8377;<span id="totalAmountId"><c:out value="${salesManReport.salesManCollectionAmount}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
           <%--  <div class="col s12 l9  m9 right">
                <h5 class="right red-text">Total Amount:  <span id="totalAmountId"><c:out value="${salesManReport.salesManCollectionAmount}" /></span></h5>
            </div> --%>
           
            
            <div class="col s12 m3 l3 right right-align" style="margin-top:0.6%;width:15%;">
                <!-- <div class="col s6 m4 l4 right"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=today">Today</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/fetchSalesManReport?range=viewAll">View All</a></li>
                    </ul>
                </div>
                 <div class="col s12 m3 l2 right-align right">
                <select name="salesManId" id="salesManId" class="btnSelect">
                   <option value="SalesMan" selected>SalesMan Filter</option>
                        <c:if test="${not empty salesManReport.employeeDetailsList}">
							<c:forEach var="listValue" items="${salesManReport.employeeDetailsList}">
								<option value="<c:out value="${listValue.employeeDetailsGenId}" />"><c:out value="${listValue.name}" /></option>
							</c:forEach>
						</c:if>
                </select>
            </div>
				<div class="input-field col s12 l3 m3" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchSalesManReport" method="post">
	                    <div class="input-field col s12 m7 l7">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2" style="margin-top:5%;">
	                     <button type="submit">View</button>
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
               <div class="input-field col s12 l4 m4">
                <form action="${pageContext.request.contextPath}/fetchSalesManReport" method="post">
                    <input type="hidden" name="range" value="range">
                     <span class="showDates">
                     	     
                              <div class="input-field col s6 m5 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                                <div class="input-field col s6 m5 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                                 </div>
                         		<div class="input-field col s6 m1 l1">
                            <button type="submit">View</button>
                            </div>
                               
                          </span>
                </form>
            </div>
            
           
       <!--  </div>

        <div class="row"> -->
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>
                             <th class="print-col" >Salesman Id</th>
                            <th class="print-col">Salesman Name</th>
                            <th class="print-col">Amount Without Tax</th>
                            <th class="print-col">Amount With Tax</th>
                            <th>Area</th>
                            <th class="print-col">Orders</th>
                        </tr>
                    </thead>

                    <tbody>
	                    <c:if test="${not empty salesManReport.salesManReportSubList}">
						<c:forEach var="listValue" items="${salesManReport.salesManReportSubList}">
	                        <tr>
	                            <td><c:out value="${listValue.srno}"/></td>
	                            <td><c:out value="${listValue.employeeDetailsSMGenId}"/></td>
	                            <td>
		                           <%-- <input id="employeeDetailsId" value="<c:out value="${listValue.employeeDetailsSMId}"/>" > --%>
		                            <a class="tooltipped" data-position="right" data-delay="50" data-tooltip="View Salesman Details" title="View Salesman Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetailsSMId}"><c:out value="${listValue.employeeSMName}"/></a>
	                            </td>
	                            <td>
	                            	<c:out value="${listValue.amountOfBusinessDid}" />
	                            </td>
	                            <td>
	                            	<c:out value="${listValue.amountOfBusinessWithTaxDid}" />
	                            </td>
	                            <td>
									<button type="button" class="btn-flat tooltipped" data-position="right" data-delay="50" data-tooltip="Show Area" onclick="showAreaDetail('${listValue.employeeDetailsSMId}')"  id="areaDetails${listValue.srno}">
		                            Show
		                            </button>			
								</td>
	                            <td>
		                             <a class="btn-floating  waves-effect waves-light tootlipped" data-position="right" data-delay="50" data-tooltip="View Order" href="${pageContext.servletContext.contextPath}/showOrderReportByEmployeeSMId?employeeSMId=${listValue.employeeSMId}&${partUrl}">
		                             <c:out value="${listValue.orderCount}"/>
		                             </a>	
	                            </td>
	                        </tr>
	                     </c:forEach>
	                     </c:if>
                    </tbody>
                </table>
            </div>
        </div>

<!-- Modal Structure for Area Details -->
        <div id="area" class="modal">
            <div class="modal-content">
                <h5 class="center"><u>Area Details</u></h5>
                
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th width="15%">Sr.No</th>
                            <th>Area Name</th>

                        </tr>
                    </thead>
                    <tbody id="areaList">
                        
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">
				<div class="col s6 m6 l7">
                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
                </div>
            </div>
        </div>

    </main>
    <!--content end-->
</body>

</html>