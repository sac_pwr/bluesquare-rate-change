<!DOCTYPE html>
<html lang="en">
<head>
<%-- <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 --%><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="components/header_imports.jsp"%>
<style>
label {
	color: black;
	font-weight: bold;
}


</style>
<script type="text/javascript">
var baseUrl="${pageContext.servletContext.contextPath}";
</script>
<script type="text/javascript" src="resources/js/expense_type.js"></script>
<script type="text/javascript">

 $(document).ready(function(){
	 
	 var table = $('#tblData').DataTable();
		table.destroy();
		$('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show:  _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	         "autoWidth": false,
	          "columnDefs": [
							{ "width": "2%", "targets": 0},	
							{ "width": "12%", "targets": 1},	
			                { "width": "5%", "targets": 2},
			                { "width": "5%", "targets": 3},	
			                { "width": "4%", "targets": 4}	
			                
			              ],
	         lengthMenu: [
	             [10, 25., 50, -1],
	             ['10 ', '25 ', '50 ', 'All']
	         ],
	         
	         dom: 'lBfrtip',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [40,"*","*","*"] 
		                    		 } 
		                    		    })
	   	       
	   	                         doc.pageMargins = [5,20,10,5];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		$('select').material_select();
	     $("select")
	         .change(function() {
	             var t = this;
	             var content = $(this).siblings('ul').detach();
	             setTimeout(function() {
	                 $(t).parent().append(content);
	                 $("select").material_select();
	             }, 200);
	         });
	     $('.dataTables_filter input').attr("placeholder", "Search");
	 
	/*  $("#expenseTypeForm").submit(function(){
			$("#saveExpenseTypeSubmit").attr("disabled",true);
	 }); */
	
	
});
 

 </script>
</head>
<body>
		<%@include file="components/navbar.jsp"%>
	<main class="paddingBody">
	 <br>
	<!-- <div class="row">
		<div class="col s12 m12 l12">
			<h3 class="center-align" style="font-family: Serif;">Expense Type</h3>
		</div>
	</div> -->
	<%-- <h4 class="red-text center-align">${msg}</h4> --%>

	<div class="row">
		<!-- <form name="table"
			action=${pageContext.servletContext.contextPath}/adding-expense-type
			method="post" id="expenseTypeForm"> -->
			<div class="input-field col l4 m6 s6 offset-l3">

				<input id="expenseTypeId"
					type="hidden" class="validate" value="0" name="expenseTypeId"> <input
					id="name" name="name" type="text" class="validate" required>
				<label for="to">Expense Type Name*</label>
			</div>
			<div class="input-field col l2 m6 s6 left-align" style="margin-top:1.5%">
				<button type="submit" id="saveExpenseTypeSubmit"
					class="btn waves-effect waves-light right ">
					<i class="material-icons left">add</i>Add
				</button>
			</div>
			<div class="input-field col s10 l2 m6" style="margin-top:1.5%">
				<button id="resetExpenseSubmit"
					class="btn waves-effect waves-light" type="button"
					name="action">Reset<i class="material-icons left">refresh</i></button>
			</div>
		<!-- </form> -->
	</div>
	<div class="row">
		<div class="col s12 m12 l12">
			<table id="tblData" class="centered striped highlight bordered">
				<thead>
					<tr>
						<th class="print-col">Sr No.</th>
						<th class="print-col">Expense Type Name</th>
						<th class="print-col">Added DateTime</th>
						<th class="print-col">Updated DateTime</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody ><%-- 
					<c:if test="${not empty expenseTypeList}">
					<c:set var="index" value="1" scope="page"/>
					<c:forEach var="expenseType" items="${expenseTypeList}" varStatus="i">
						<c:set var="index" value="${index+1}" scope="page"/>
						<tr>
							<td><c:out value="${index}" /></td>
							<td><c:out value="${expenseType.name}" /></td>
							<td><button class=" btn-flat "
									onclick='editExpenseType(${expenseType.expenseTypeId})'>
									<i class="material-icons tooltipped blue-text "
										data-position="right" data-delay="50" data-tooltip="Edit">edit</i>
								</button></td>
						</tr>
					</c:forEach>
					</c:if> --%>
				</tbody>
			</table>
		</div>
	</div> 
	<!-- deleteModals -->
	<div id="deleteModals"></div>
	<!-- deleteModals end -->
	</main>
	<%@include file="components/footer.jsp"%>
</body>

</html>