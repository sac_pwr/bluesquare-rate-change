<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
     <%@include file="components/header_imports.jsp" %>
		
		<script>
    $(document).ready(function() {
    	var table = $('#tblData').DataTable();
		 table.destroy();
		 $('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	      	autoWidth: false,
	         columnDefs: [
	                       { 'width': '1%', 'targets': 0 },
	                      { 'width': '1%', 'targets': 1},
	                      { 'width': '15%', 'targets': 2},
	                      { 'width': '10%', 'targets': 3},
	                      { 'width': '5%', 'targets': 4},
	                  	  { 'width': '1%', 'targets': 5},
	                  	  { 'width': '3%', 'targets': 6},
	                  	  { 'width': '3%', 'targets': 7},
	                	  { 'width': '1%', 'targets': 8},
	                	  { 'width': '1%', 'targets': 9},
	                	  { 'width': '1%', 'targets': 10},
	                	  { 'width': '1%', 'targets': 11},
	            	      { 'width': '1%', 'targets': 12,"orderable": false}
	            		  /* { 'width': '5%', 'targets': 10} */ 
	                     
	                      ],
	         lengthMenu: [
	             [5,10, 25, 50, -1],
	             ['5','10', '25 ', '50 ', 'All']
	         ],
	         
	        
	         //dom: 'lBfrtip',
	         dom:'<lBfr<"scrollDivTable"t>ip>',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                     customize: function(doc, config) {
	                    	 doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [60,60,'*','*',50,50,40,40,40,50,50,50,50] 
	                    		 } 
	                    		    })
	                          var tableNode;
	                         for (i = 0; i < doc.content.length; ++i) {
	                           if(doc.content[i].table !== undefined){
	                             tableNode = doc.content[i];
	                             break;
	                           }
	                         }
	        
	                         var rowIndex = 0;
	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	                          
	                         if(tableColumnCount > 6){
	                           doc.pageOrientation = 'landscape';
	                         } 
	                         /*for customize the pdf content*/ 
	                         doc.pageMargins = [5,20,10,5];   	                         
	                         doc.defaultStyle.fontSize = 8	;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
	                       },
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		 $("select")
      .change(function() {
          var t = this;
          var content = $(this).siblings('ul').detach();
          setTimeout(function() {
              $(t).parent().append(content);
              $("select").material_select();
          }, 200);
      });
  $('select').material_select();
  $('.dataTables_filter input').attr("placeholder", "Search");
  var table = $('#tblData').DataTable(); // note the capital D to get the API instance
  var column = table.columns('.toggle');
  column.visible(false);
  $('#showColumn').on('click', function () {
   
  	 //console.log(column);

  	 column.visible( ! column.visible()[0] );

  });
        /*   $("#BankDetails").css("display", "none");
          $(".cash").change(function() {
              $("#BankDetails").css("display", "none");
          });
          $(".cheque").change(function() {
              $("#BankDetails").css("display", "block");
          }); */
          $(".showQuantity").hide();
          $(".showDates").hide();
          $("#oneDateDiv").hide();
          $(".topProduct").click(function() {
              $(".showQuantity").show();
              $(".showDates").hide();
              $("#oneDateDiv").hide();
          });
         
          $(".rangeSelect").click(function() {
          	$("#oneDateDiv").hide();	
              $(".showDates").show();
              $(".showQuantity").hide();
          });
         
      	$(".pickdate").click(function(){
      		 $(".showQuantity").hide();
      		 $(".showDates").hide();
      		$("#oneDateDiv").show();
      	});
     	
		

      });
    </script>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        
        <div class="row"> 
       	<br>
        	    <div class="col s12 m4 l4 right right-align" style="margin-top:1%">
                <!-- <div class="col s6 m3 l3 right"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                    	<li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=today">Today</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/returnOrderReport?range=viewAll">View All</a></li>
                    </ul>
                </div>
                <div class="col s12 l4	m4 right"  id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/returnOrderReport" method="post">
	                         <input type="hidden" name="range" value="pickDate">
                   			  <div class="input-field col s12 m2 l2 right">
	                     <button type="submit">View</button>
                       
                        </div>
	                    <div class="input-field col s12 m5 l5 right">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                   
                    </form>
               </div>
                	<div class="col s12 l7	m7 right right-align"> 
                 <form action="${pageContext.request.contextPath}/returnOrderReport" method="post">
                    <input type="hidden" name="range" value="range">
                     <span class="showDates">
                     			 <div class="input-field col s6 m2 l2 right">
                            <button type="submit">View</button>
                            </div>
                                <div class="input-field col s6 m2 l3 right">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate" required> 
                                <label for="endDate">To</label>
                                 </div>
                              <div class="input-field col s6 m2 l3 right">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate">
                                     <label for="startDate">From</label>
                               </div>
                             
                          </span>
                </form>
                 </div>
				
               
               
                
           
            <div class="col s12 l12 m12 ">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Return Order Id</th>
                            <th class="print-col">Order Id</th>
                            
                            <th class="print-col">Shop Name</th>
                            <th class="print-col">Sales Person</th>
                            <th class="print-col">Area</th>
                            <th class="print-col toggle">Region</th>
                            <th class="print-col toggle">City</th>
                            <th class="print-col">Total Return Qty</th>
                            <th class="print-col">Taxable Amt</th>
                            <th class="print-col">Total Tax</th>
                            <th class="print-col">Total Amt</th>
                            <th class="print-col">Date Of Returned</th>
                            <th class="print-col">ReIssue Status<br/><a href="#" class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="Show more columns" id="showColumn"><i class="material-icons black-text">swap_horiz</i></a> </th>
							<!-- <th>Product Details</th> -->
                        </tr>
                    </thead>

                    <tbody>
                        
                        <c:if test="${not empty returnItemReportMainList}">
						<c:forEach var="listValue" items="${returnItemReportMainList}">
						<tr>
                            <td><a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Return Order"href="${pageContext.servletContext.contextPath}/returnOrderDetailsByReturnOrderId?returnOrderId=${listValue.returnOrderProductlist.returnOrderProductId}"><c:out value="${listValue.returnOrderProductlist.returnOrderProductId}" /></a></td>
                            <td><a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Order Details" href="${pageContext.servletContext.contextPath}/fetchIssueProductDetailsForIssueReportByOrderIdForWeb?orderId=${listValue.returnOrderProductlist.orderDetails.orderId}"><c:out value="${listValue.returnOrderProductlist.orderDetails.orderId}" /></a></td>
                            
                            <td><c:out value="${listValue.returnOrderProductlist.orderDetails.businessName.shopName}" /></td>
                            <td><a class="tooltipped" area-hidden="true" data-position="right" data-delay="50" data-tooltip="View Details" href="${pageContext.servletContext.contextPath}/getEmployeeView?employeeDetailsId=${listValue.employeeDetails.employeeDetailsId}"><c:out value="${listValue.employeeDetails.name}" /></a></td>
                            <td><c:out value="${listValue.returnOrderProductlist.orderDetails.businessName.area.name}" /></td>
                            <td><c:out value="${listValue.returnOrderProductlist.orderDetails.businessName.area.region.name}" /></td>
                            <td><c:out value="${listValue.returnOrderProductlist.orderDetails.businessName.area.region.city.name}" /></td>
                            <td><c:out value="${listValue.returnOrderProductlist.totalQuantity}" /></td>
                           <td class="wrapok"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.returnOrderProductlist.totalAmount}" /></td>
                            <td class="wrapok"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.returnOrderProductlist.totalAmountWithTax-listValue.returnOrderProductlist.totalAmount}" /></td>
                           <td class="wrapok"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.returnOrderProductlist.totalAmountWithTax}" /></td>                          
                            <td class="wrapok"><fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.returnOrderProductlist.returnOrderProductDatetime}"  /><c:out value="${date}" />
                            </td>
                            <td><c:out value="${listValue.reIssueOrderDetails == null ? 'Pending' : listValue.reIssueOrderDetails.status}" /></td>
                            <%-- <td><a href="${pageContext.servletContext.contextPath}/returnOrderDetailsByReturnOrderId?returnOrderId=${listValue.returnOrderProductlist.returnOrderProductId}" class="btn-flat">View</a></td> --%>
                        </tr>
						</c:forEach>
						</c:if>
                       
                    </tbody>
                </table>
            </div>
        </div>


    </main>
    <!--content end-->
</body>

</html>