<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<html>

<head>
    <%@include file="components/header_imports.jsp" %>

<script type="text/javascript">

 $(document).ready(function(){
	 var table = $('#tblData').DataTable();
		table.destroy();
		$('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show:  _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	         "autoWidth": false,
	          "columnDefs": [
							{ "width": "2%", "targets": 0},
							{ "width": "10%", "targets": 1},
							{ "width": "5%", "targets": 2},
							{ "width": "3%", "targets": 3},
							{ "width": "3%", "targets": 4},
			                { "width": "3%", "targets": 5},
			                { "width": "2%", "targets": 6},
			                { "width": "2%", "targets": 7},	
			                { "width": "1%", "targets": 8}	
			                
			              ],
	         lengthMenu: [
	             [50, 75, 100, -1],
	             ['50', '75', '100', 'All']
	         ],
	         
	         dom: 'lBfrtip',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                     customize: function(doc, config) {
	                    	  doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [40,'*',50,40,40,35,80,80] 
	                    		 } 
	                    		    })
	                         var tableNode;
	                         for (i = 0; i < doc.content.length; ++i) {
	                           if(doc.content[i].table !== undefined){
	                             tableNode = doc.content[i];
	                             break;
	                           }
	                         }
	        
	                         var rowIndex = 0;
	                        /*  var tableColumnCount = tableNode.table.body[rowIndex].length;
	                          
	                         if(tableColumnCount > 6){
	                           doc.pageOrientation = 'landscape';
	                         } */
	                         /*for customize the pdf content*/ 
	                         doc.pageMargins = [5,20,10,5];
	                         
	                         doc.defaultStyle.fontSize = 8;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
	                       },
	                      
	               
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY</span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		$('select').material_select();
	     $("select")
	         .change(function() {
	             var t = this;
	             var content = $(this).siblings('ul').detach();
	             setTimeout(function() {
	                 $(t).parent().append(content);
	                 $("select").material_select();
	             }, 200);
	         });
	     $('.dataTables_filter input').attr("placeholder", "Search");
	 var msg="${saveMsg}";
	 //alert(msg);
	 $('#addeditmsg').modal({
	 complete:function(){
		 msg="";
		 <c:set var="saveMsg" value=""/>
		 var m = "${saveMsg}";
		// console.log("m = "+m);
	 }
});
	 if(msg!='' && msg!=undefined)
	 {
		 $('#addeditmsg').find("#modalType").addClass("success");
		 $('#addeditmsg').find(".modal-action").removeClass("red lighten-2 teal").addClass("teal lighten-2");
	     $('#addeditmsg').modal('open');
	    /*  $('#msgHead').text("Category Message"); */
	     $('#msg').text(msg);
	 }
	 
	$("#resetCategorySubmit").click(function(){
		
		$('#categoryName').val('');	
		$('#categoryId').val('0');
		$('#hsnCode').val('');
		$('#cgst').val(0);
		$('#igst').val(0);
		$('#sgst').val(0);
		$('#per0').click();
		$("#saveCategorySubmit").html('<i class="material-icons left">add</i> Add');
	});
	
$('#per0').click(function(){
		
		$('#cgst').val(0);
		$('#igst').val(0);
		$('#sgst').val(0);
		
		$('#cgst').change();
		$('#igst').change();
		$('#sgst').change();
	});
	$('#per5').click(function(){
			
			$('#cgst').val(2.5);
			$('#igst').val(5);
			$('#sgst').val(2.5);
			
			$('#cgst').change();
			$('#igst').change();
			$('#sgst').change();
			
		});
	$('#per12').click(function(){
		
		$('#cgst').val(6);
		$('#igst').val(12);
		$('#sgst').val(6);
		
		$('#cgst').change();
		$('#igst').change();
		$('#sgst').change();
		
	});
	$('#per18').click(function(){
		
		$('#cgst').val(9);
		$('#igst').val(18);
		$('#sgst').val(9);
		
		$('#cgst').change();
		$('#igst').change();
		$('#sgst').change();
		
	});
	$('#per28').click(function(){
		
		$('#cgst').val(14);
		$('#igst').val(28);
		$('#sgst').val(14);
		
		$('#cgst').change();
		$('#igst').change();
		$('#sgst').change();
		
	});
	$('#per0').click();
});
 
function editcategory(id) {
	//alert("${pageContext.request.contextPath}/fetchCategories?categoriesId="+id);
	$.ajax({
				type : "GET",
				url : "${pageContext.request.contextPath}/fetchCategories?categoriesId="+id,
				/* data: "id=" + id + "&name=" + name, */
				beforeSend: function() {
						$('.preloader-background').show();
						$('.preloader-wrapper').show();
			           },
				success : function(data) {
					var category = data;
					//alert(brand.name +" "+ brand.brandId);
					$('#categoryName').val(category.categoryName);	
					$('#categoryId').val(category.categoryId);
					
					
					$('#igst').val(category.igst);
					$('#igst').focus();
					$('#sgst').val(category.sgst);
					$('#sgst').focus();
					$('#cgst').val(category.cgst);
					$('#cgst').focus();
					
					if(parseFloat(category.igst)==0)
					{
						$('#per0').click();
					}
					else if(parseFloat(category.igst)==5)
					{
						$('#per5').click();
					}
					else if(parseFloat(category.igst)==12)
					{
						$('#per12').click();
					}
					else if(parseFloat(category.igst)==18)
					{
						$('#per18').click();
					}
					else if(parseFloat(category.igst)==28)
					{
						$('#per28').click(); 
					}
					$('#hsnCode').val(category.hsnCode);
					$('#hsnCode').focus();
					$('#categoryName').focus();
						$("#saveCategorySubmit").html('<i class="material-icons left">send</i> Update');
												
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
					},
					error: function(xhr, status, error) {
						$('.preloader-wrapper').hide();
						$('.preloader-background').hide();
						  //alert(error +"---"+ xhr+"---"+status);
						$('#addeditmsg').modal('open');
	           	     	$('#msgHead').text("Message : ");
	           	     	$('#msg').text("Something Went Wrong"); 
	           	     		setTimeout(function() 
							  {
	  	     					$('#addeditmsg').modal('close');
							  }, 1000);
						}						
			
			});
	
	
}


</script>
<style>
label{
	color:black !important;
}
.btnmarginInform{
	margin-top:2.5%;
}
.btnmargin{
margin-top:2%;
}
[type="radio"]:not(:checked)+label, [type="radio"]:checked+label{
	/* padding-left:26px; */
}
.btn {
    font-size: 0.8rem;
    border: none;
    border-radius: 2px;
    display: inline-block;
    height: 32px;
    line-height: 32px;
    padding: 0 1rem;
    text-transform: uppercase;
    vertical-align: middle;
    -webkit-tap-highlight-color: transparent;
}
i.left {
    float: left;
    margin-right: 5px;
}
.dataTables_wrapper th {
    padding: 5px 10px !important;
}
</style>
</head>

<body>
<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">       
        <div class="col s4 m4 l2 right right-align" style="width:13%;padding-right:0">
         <a  class="btn waves-effect waves-light blue-gradient right" href="${pageContext.request.contextPath}/fetchProductList"><i class="left material-icons">settings</i>Product</a>
        </div>
        <div class="col s4 m4 l2 right right-align" style="width:12%">
         <a  class="btn waves-effect waves-light blue-gradient right" href="${pageContext.request.contextPath}/fetchBrandList"><i class="left material-icons">settings</i>Brand</a>
        </div>
       <!--  <div class="col s12 m12 l12">  -->
       <div class="col s12 m12 l9 formBg">
        <form  action="${pageContext.servletContext.contextPath}/saveCategories" method="post" id="saveCategoryForm">
            <input id="categoryId" type="hidden" name="categoriesId" value="0">
            <!--Add New Area-->
            <div class="row" style="margin-bottom:5px;">
                <div class="input-field col s10 l3 m6">
                 <label for="categoryName" class="active"><span class="red-text">*</span>Category Name</label>
                    <input id="categoryName" type="text" name="categoryName" required style="margin-bottom:0;">
                 
                </div>
                <div class="input-field col s12 m4 l2">
                <label for="hsnCode" class="active"><span class="red-text">*</span>HSN Code</label>
                    <input id="hsnCode" type="text" name="hsnCode" required style="margin-bottom:0;">
                   
                </div>
                <%-- <div class="input-field col s12 l3 m4">
               
               	<select name="areaId" id="areaId" class="validate" required="" aria-required="true" title="Please select Area">
                                 <option value="" selected><span class="red-text">*</span>Select Area</option>
                                <c:if test="${not empty areaList}">
							<c:forEach var="listValue" items="${areaList}">
								<option value="<c:out value="${listValue.areaId}" />"><c:out
										value="${listValue.name}" /></option>
							</c:forEach>
							</c:if>
                        </select>
               </div> --%>
                
                <div class="input-field col s10 l2 m6 btnmarginInform" style="width:16%">
                    <button class="btn waves-effect waves-light blue-gradient" type="submit" id="saveCategorySubmit"name="action"><i class="left material-icons">add</i>Add</button>
                </div>
                <div class="input-field col s10 l2 m6 btnmarginInform" style="width:15%">
                    <button class="btn waves-effect waves-light blue-gradient" type="button" id="resetCategorySubmit" name="action"><i class="left material-icons">refresh</i>Reset</button>                    
                </div>
                <div class="col s12 m4 l8" style="margin-top:2%;">
              	  <input name="group1" type="radio" id="per0" required/>
     				 <label for="per0">0%</label>
     				 <input name="group1" type="radio" id="per5" required/>
     				 <label for="per5">5%</label>
     				 <input name="group1" type="radio" id="per12" required/>
     				 <label for="per12">12%</label>
     				 
     				 <input name="group1" type="radio" id="per18" required/>
     				 <label for="per18">18 %</label>
     				 <input name="group1" type="radio" id="per28" required/>
     				 <label for="per28">28%</label>
     				
                </div>
           </div>
          
                    <input id="cgst" type="hidden" name="cgst" readonly>
                   
                    <input id="sgst" type="hidden" name="sgst" readonly>
                   
                    <input id="igst" type="hidden" name="igst" readonly>
            	

    </form>    
    </div>
    </div>
    
		<div class="col s12 m12 l8">
            <table class="striped highlight centered" id="tblData" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th class="print-col">Sr. No.</th>
                        <th class="print-col">Category</th>
                        <th class="print-col">HSN</th>
                        <th class="print-col">% CGST</th>
                        <th class="print-col">% SGST</th>
                        <th class="print-col" >% IGST</th>
                        <th class="print-col">Added</th>
                        <th class="print-col">Updated</th>
                        <th>Edit</th>
                        <!-- <th>Delete</th> -->

                    </tr>
                </thead>

                <tbody>
                     <% int rowincrement=0; %>
                     <c:if test="${not empty categorieslist}">
							<c:forEach var="listValue" items="${categorieslist}">
						
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
						<tr>
                        <td><c:out value="${rowincrement}" /></td>
                        <td><c:out value="${listValue.categoryName}" /></td>
                       <td class="wrapok"><c:out value="${listValue.hsnCode}" /></td>
                       <td><c:out value="${listValue.cgst}" />%</td>
                       <td><c:out value="${listValue.sgst}" />%</td>
                       <td><c:out value="${listValue.igst}" />%</td>
                       <td class="wrapok"><fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.categoryDate}" /><c:out value="${dt}" /><br/><fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.categoryDate}" /><c:out value="${time}" />
                        </td>
                        <td class="wrapok">
                        	<c:choose>
                        	<c:when test="${empty  listValue.categoryUpdateDate}">
                        		NA
	                        </c:when>
	                        <c:otherwise>
		                        <fmt:formatDate pattern="dd-MM-yyyy" var="dt" value="${listValue.categoryUpdateDate}" /><c:out value="${dt}" />
		                        	<br/>
		                        <fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.categoryUpdateDate}" /><c:out value="${time}" />
	                        </c:otherwise>
	                        </c:choose>
                        </td>
                        <td><button class=" btn-flat " type="button" class="brandedit" onclick='editcategory(${listValue.categoryId})'><i class="material-icons tooltipped blue-text " data-position="right" data-delay="50" data-tooltip="Edit" >edit</i></button></td>
                        <!-- Modal Trigger -->
                        <!-- <td><a href="#delete" class="modal-trigger"><i class="material-icons tooltipped" data-position="right" data-delay="50" data-tooltip="Delete">delete</i></a></td> -->
                    </tr>
                    </c:forEach>
                    </c:if>
                    
                    <!-- Modal Structure for delete -->

                   <!--  <div id="delete" class="modal">
                        <div class="modal-content">
                            <h4>Confirmation</h4>
                            <p>Are you sure you want to delete?</p>
                        </div>
                        <div class="modal-footer">
                            <a href="#!" class="modal-action modal-close waves-effect  btn-flat ">Delete</a>
                            <a href="#!" class="modal-action modal-close waves-effect btn-flat ">Close</a>
                        </div>
                    </div> -->
                </tbody>
            </table>
</div>

       	  <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
       	 <!-- <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="head"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
    </main>
    <!--content end-->
</body>

</html>