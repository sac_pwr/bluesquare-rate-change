<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	

<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}"
$(document).ready(function() {
	
	
$('#countryList').change(function() {
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('stateList');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options

		select.options.add(new Option("All State", '-1'));
		
		var selectcity = document.getElementById('cityList');
		selectcity.options.length = 0;
		selectcity.options.add(new Option("All City", '-1'));
		
		var selectregion = document.getElementById('regionList');
		selectregion.options.length = 0;
		selectregion.options.add(new Option("All Region", '-1'));
		
		var selectarea = document.getElementById('areaList');
		selectarea.options.length = 0;
		selectarea.options.add(new Option("All Area", '-1'));
		$.ajax({
			url : myContextPath+"/fetchStateListByCountryIdBranchConfig?countryId="+$('#countryList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {
				
				var options, index, option;
				select = document.getElementById('stateList');

				for (var i = 0, len = data.length; i < len; ++i) {
					var state = data[i];
					
					select.options.add(new Option(state.name, state.stateId));
				}
				
				
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('State List Not Found!', '2000', 'teal lighten-2');
				  
				}
		});		
		
	});

	
	$('#stateList').change(function() {
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('cityList');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options

		select.options.add(new Option("All City", '-1'));
		
		var selectregion = document.getElementById('regionList');
		selectregion.options.length = 0;
		selectregion.options.add(new Option("All Region", '-1'));
		
		var selectarea = document.getElementById('areaList');
		selectarea.options.length = 0;
		selectarea.options.add(new Option("All Area", '-1'));
		$.ajax({
			url : myContextPath+"/fetchCityListByStateIdBranchConfig?stateId="+$('#stateList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {

				console.log(data);
				var options, index, option;
				select = document.getElementById('cityList');

				for (var i = 0, len = data.length; i < len; ++i) {
					var city = data[i];
					//alert(state.name+" "+ state.stateId);
					select.options.add(new Option(city.name, city.cityId));
				}
				
				/* for (index = 0; index < options.length; ++index) {
				  option = options[index];
				  select.options.add(new Option(option.name, option.cityId));
				} */
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('City List Not Found!', '2000', 'teal lighten-2');
				 
				}
		});		
		
	});
	
	
	$('#cityList').change(function() {
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('regionList');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options
		
		select.options.add(new Option("All Region", '-1'));
		
		var selectarea = document.getElementById('areaList');
		selectarea.options.length = 0;
		selectarea.options.add(new Option("All Area", '-1'));
		$.ajax({
			url : myContextPath+"/fetchRegionListByCityIdBranchConfig?cityId="+$('#cityList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {

				/* alert(data); */
				var options, index, option;
				select = document.getElementById('regionList');

				for (var i = 0, len = data.length; i < len; ++i) {
					var region = data[i];
					//alert(state.name+" "+ state.stateId);
					select.options.add(new Option(region.name, region.regionId));
				}
				
				/* for (index = 0; index < options.length; ++index) {
				  option = options[index];
				  select.options.add(new Option(option.name, option.cityId));
				} */
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('Region List Not Found!', '2000', 'teal lighten-2');
				  //alert(error +"---"+ xhr+"---"+status);
				/* $('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Region List Not Found"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000); */
				}
		});		
		
	});
	
	$('#regionList').change(function() {
		
		// Get the raw DOM object for the select box
		var select = document.getElementById('areaList');

		// Clear the old options
		select.options.length = 0;
		//$('#countryListForState').html('');

		//Load the new options

		select.options.add(new Option("All Area", '-1'));
		$.ajax({
			url : myContextPath+"/fetchAreaListByRegionIdBranchConfig?regionId="+$('#regionList').val(),
			dataType : "json",
			beforeSend: function() {
				$('.preloader-background').show();
				$('.preloader-wrapper').show();
	           },
			success : function(data) {

				/* alert(data); */
				var options, index, option;
				select = document.getElementById('areaList');

				for (var i = 0, len = data.length; i < len; ++i) {
					var area = data[i];
					//alert(state.name+" "+ state.stateId);
					select.options.add(new Option(area.name, area.areaId));
				}
				
				/* for (index = 0; index < options.length; ++index) {
				  option = options[index];
				  select.options.add(new Option(option.name, option.cityId));
				} */
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
			},
			error: function(xhr, status, error) {
				$('.preloader-wrapper').hide();
				$('.preloader-background').hide();
				Materialize.toast('Area List Not Found!', '2000', 'teal lighten-2');
				  //alert(error +"---"+ xhr+"---"+status);
				/* $('#addeditmsg').modal('open');
       	     	$('#msgHead').text("Message : ");
       	     	$('#msg').text("Area List Not Found"); 
       	     		setTimeout(function() 
					  {
	     					$('#addeditmsg').modal('close');
					  }, 1000); */
				}
		});		
		
	});


	/* $("#test").click(function(){
		  $('#addeditmsg').modal('open');
		     $('#msgHead').text("Business Message");
		     $('#msg').text("Please Select Business Type to Save Business");
		     return false;
		
	})
	 */
});

</script>

<style> 
	#addeditmsg{
		border-radius:10px;
		 width:30% !important; 
	}
	
	#toast-container {
    top: 35%;
    right: 35%;
    max-width: 86%;
}
i span{
	font-size:15px !important;
	padding-left:4%;
}
.select-wrapper span.error{
	margin-left:0 !important;
}		
	
	/* .modal{
		width:30% !important;
	} */
	
	
</style>
</head>

<body >
   <!--navbar start-->
   	<%-- <%@include file="components/navbar.jsp" %> --%>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
        	 <!-- <button class="btn waves-effect waves-light blue-gradient" id="test">Text</button> -->
            <form action="${pageContext.request.contextPath}/setBranch" method="post" id="setBranchForm">
                
               
                <div class="row z-depth-3">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Set Branch </h4>
                    </div>
                    
                    <div class="row">
                       <div class="input-field col s12 m5 l5 offset-l3 offset-m3">
                        <i class="material-icons prefix">location_on</i>
                   
                        <select name="countryId" id="countryList" >
                                 <option value="-1" selected>All Country</option>
                                <c:if test="${not empty countryList}">
								<c:forEach var="listValue" items="${countryList}">
								
									<option value="<c:out value="${listValue.countryId}" />"><c:out
											value="${listValue.name}" /></option>
								</c:forEach>
							</c:if>
                        </select>
                        </div>
                        
                        <div class="input-field col s12 m5 l5 offset-l3 offset-m3">
                        	<i class="material-icons prefix">location_on</i>                      
                        	<select name="stateId" id="stateList">
                                <option value="-1"  selected>All State</option>
                      
                     	   </select>
                   		</div>
                    <div class="input-field col s12 m5 l5 offset-l3 offset-m3">
                        <i class="material-icons prefix">location_on</i>
                   
                        <select name="cityId" id="cityList" class="validate">    
                                 <option  value="-1" selected>All City</option>
                        </select>
                    </div>
                    
                    <div class="input-field col s12 m5 l5 offset-l3 offset-m3">
                        <i class="material-icons prefix">track_changes</i>
                       <!--  <label for="regionList" class="active"><span class="red-text">*</span></label> -->
                        <select name="regionId" id="regionList" class="validate">
                                 <option value="-1" selected>All Region</option>
                        </select>
                    </div>
                    <div class="input-field col s12 m5 l5 offset-l3 offset-m3">
                        <i class="material-icons prefix">business</i>
                        
                        <select name="areaId"  id="areaList" class="active" class="validate">
                                <option value="-1" selected>All Area</option>
                        </select>
                    </div>
                      </div>
                       
                </div>

               
                <div class="input-field col s12 m6 l4 offset-l5 center-align">
                    <button class="btn waves-effect waves-light blue-gradient" type="submit" id="setBranch">Set Branch<i class="material-icons right">send</i> </button>
                </div>
                <br>
            </form>

        </div>

		

		

    </main>
    <!--content end-->
</body>

</html>