<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
      <%@include file="components/header_imports.jsp" %>

    <script>
        $(document).ready(function() {
        	/*  var dateDisable = $('.datepicker').pickadate();
        	var picker = dateDisable.pickadate('picker');
        	   var yesterday = new Date((new Date()).valueOf()-1000*60*60*24);
        	   picker.set( 'disable', { from: [0,0,0], to: yesterday } ); */
        	  
        	   
            	 
        
            $('.chequeNo').hide();
        	$('#paymentType').val('Cash');    
        	   
            $('#cash').click(function() {
            	$("#cheqNo").removeAttr("required","required");
            	$("#bankName").removeAttr("required","required");
                $('.chequeNo').hide();
                $('.amount').addClass('offset-l4 offset-m4');
                $('#paymentType').val('Cash');                
            });
            $('#cheque').click(function() {
            	$("#cheqNo").attr("required","required");
            	$("#bankName").attr("required","required");
            	$('.amount').removeClass('offset-l4 offset-m4');
                $('.chequeNo').show();
                $('#paymentType').val('Cheque');
            });
            $('#cheqNo').keypress(function( event ){
			    var key = event.which;
			    
			    if( ! ( key >= 48 && key <= 57 || key === 13) )
			        event.preventDefault();
			});
            
            $('#amount').keydown(function(e){            	
				-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()
			 });
            
			document.getElementById('amount').onkeypress=function(e){
            	
            	if (e.keyCode === 46 && this.value.split('.').length === 2) {
              		 return false;
          		 }

            }	
            
            

        });
    </script>
    <style>
    
    .card{
     height: 2.5rem;
     line-height:2.5rem;
     }
    .card-image{
        	width:40% !important;
        	background-color:#0073b7 !important;
        }
	.card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}
    </style>
</head>

<body>
     <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br> 
       	 <div class="container">
            <form action="${pageContext.request.contextPath}/saveExpense" method="post" id="saveExpenseForm">
                <div class="row  z-depth-3">
                <div class="col s12 m11 l11 push-l1 push-m1 center">
	                    	 <h5> Add Expense</h5>
	                    </div>
	                    
                	<div class="row" style="margin-bottom:0">
	                     <div class="input-field col s6 m5 l5 push-l1 push-m1" id="unitprice">
							 <!--<i class="material-icons prefix">edit</i>
	                         <input id="description" type="text" class="validate"  name="description" required>
	                        <label for="description" class="active"><span class="red-text">*</span>Description</label>    -->
	                        <i class="material-icons prefix">view_stream <span class="red-text">*</span></i>                        
	                        <select id="expenseTypeId" name="expenseTypeId" required="" aria-required="true" >
	                                 <option value="" selected> Expense Type</option>
	                                <c:if test="${not empty expenseTypeList}">
										<c:forEach var="listValue" items="${expenseTypeList}">
											<option value="<c:out value="${listValue.expenseTypeId}" />"><c:out
													value="${listValue.name}" /></option>
										</c:forEach>
									</c:if>
	                        </select>                    
	                    </div>
	                    
	                <!--  </div>
	                 <div class="row" style="margin-bottom:0"> -->
	                     
	                <!--      <div class="input-field col s12 m5 l5 push-l1 push-m1">
							 <i class="material-icons prefix">event</i>
	                        <input id="expenseDate" type="text" class="datepicker"  name="expenseDate" required>
	                        <label for="expenseDate" class="active"><span class="red-text">*</span>Expense Date</label>                       
	                    </div> -->
	                    <div class="input-field col s6 m5 l5 push-l1 push-m1" id="unitprice">
							 <i class="material-icons prefix">chrome_reader_mode</i>
	                        <input id="reference" type="text" class="validate"  name="reference" required>
	                        <label for="reference" class="active"><span class="red-text">*</span>Reference</label>                       
	                    </div>
	                    </div>
	                    <div class="row">
	                    <div class="col s12 m11 l11 push-l1 push-m1">
	                    <br>
	                    	
	                    </div>
	                    
	                    <div class="col s12 m5 l5 push-l1 push-m1 chqcash" style="margin-top:10px;">
	                    
                         <h6> Payment Mode</h6>
                        <input type="hidden" name="type" id="paymentType" value="">
                        <input type="radio" id="cash" name="group1" checked /> 
                        <label for="cash">Cash</label>
                        <input type="radio" id="cheque" name="group1"/> 
                        <label for="cheque">Cheque</label></b>
                   		 </div>
                   		 <div class="input-field col s12 m5 l5 push-l1 push-m1  amount1">
                   		 <i class="fa fa-inr prefix"></i>
                        <input type="text" name="amount" id="amount" title="Enter Amount" required>
                        <label for="amount"><span class="red-text">*</span>Amount</label>
                    	</div>
                   		  <div class="input-field col s12 m5 l5 push-l1 push-m1 chequeNo">
                   		  <i class="material-icons prefix">account_balance</i>
                        	<input type="text" name="bankName" class="validate" id="bankName" title="Enter Bank Name"> <label for="customerBankName">Bank Name</label>
                    	</div>
                      	
                    	<div class="input-field col s12 m5 l5 push-l1 push-m1   chequeNo">
                        <i class="material-icons prefix">featured_play_list</i>
                        <input type="text" name="chequeNumber" class="validate num" id="cheqNo" maxlength="6" minlength="6" title="Enter Cheque Number">
                         <label for="customerChequeNumber">Cheque Number</label>
                   	 </div>
	                    </div>
	                   <div class="input-field col s6 m12 l12 center center-align">
                        <button id="submit" class="btn waves-effect waves-light" type="submit">Submit
                                </button>
                                <br><br>
                    </div> 
                </div>
            </form>
         </div>
        <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>     
    </main>





    <!--content end-->
</body>

</html>