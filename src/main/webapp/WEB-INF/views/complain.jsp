<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
       <%@include file="components/header_imports.jsp" %>
        <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	
<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}";
var employeeId="${employeeDetails.employee.employeeId}";
//alert(employeeId);

$(document).ready(function() {
	$.validator.setDefaults({
	       ignore: []
	}); 

	$('#saveComplainForm').validate({
		
		 ignore: [],
	    errorElement : "span",
	    errorClass : "invalid error",
	    errorPlacement : function(error, element) {
	      var placement = $(element).data('error');
	    
	      if (placement) {
	        $(placement).append(error)
	      } else {
	        error.insertAfter(element);
	      }
	      //$('select').change(function(){ $('select').valid(); });
	    }	    
   });
	
//$('select').change(function(){ $('select').valid(); });
		$('.modal').modal();
	$('#feedback').modal();
	$('#complain').modal({complete: function() {
		
		$('#saveComplainForm').trigger('reset');
		// location.reload();
	}});
	 var table = $('#tblData').DataTable();
		table.destroy();
		$('#tblData').DataTable({
	         "oLanguage": {
	             "sLengthMenu": "Show:  _MENU_",
	             "sSearch": "_INPUT_" //search
	         },
	         "autoWidth": false,
	         "columnDefs":   [
							{ "width": "1%", "targets": 0},
							{ "width": "5%", "targets": 1},
							{ "width": "2%", "targets": 2},
							{ "width": "5%", "targets": 3},
			                { "width": "5%", "targets": 4},
			                { "width": "2%", "targets": 5},	
			                { "width": "2%", "targets": 6},
			                { "width": "1%", "targets": 7},
			                { "width": "5%", "targets": 8},
			                { "width": "5%", "targets": 9},
			                { "width": "1%", "targets": 10}
			             
			                
			              ],
	         lengthMenu: [
	             [10, 25., 50, -1],
	             ['10 ', '25 ', '50 ', 'All']
	         ],
	         
	         dom: 'lBfrtip',
	         buttons: {
	             buttons: [
	                 //      {
	                 //      extend: 'pageLength',
	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	                 //  }, 
	                 {
	                     extend: 'pdf',
	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                     customize: function(doc, config) {
	                         var tableNode;
	                         for (i = 0; i < doc.content.length; ++i) {
	                           if(doc.content[i].table !== undefined){
	                             tableNode = doc.content[i];
	                             break;
	                           }
	                         }
	        
	                         var rowIndex = 0;
	                        /*  var tableColumnCount = tableNode.table.body[rowIndex].length;
	                          
	                         if(tableColumnCount > 6){
	                           doc.pageOrientation = 'landscape';
	                         } */
	                         /*for customize the pdf content*/ 
	                         doc.pageMargins = [5,20,10,5];
	                         
	                         doc.defaultStyle.fontSize = 8;
	                         doc.styles.title.fontSize = 12;
	                         doc.styles.tableHeader.fontSize = 11;
	                         doc.styles.tableFooter.fontSize = 11;
	                         doc.styles.tableHeader.alignment = 'center';
		                        doc.styles.tableBodyEven.alignment = 'center';
		                        doc.styles.tableBodyOdd.alignment = 'center';
	                       },
	                      
	               
	                 },
	                 {
	                     extend: 'excel',
	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'print',
	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	                     //title of the page
	                     title: function() {
	                         var name = $(".heading").text();
	                         return name
	                     },
	                     //file name 
	                     filename: function() {
	                         var d = new Date();
	                         var date = d.getDate();
	                         var month = d.getMonth();
	                         var year = d.getFullYear();
	                         var name = $(".heading").text();
	                         return name + date + '-' + month + '-' + year;
	                     },
	                     //  exports only dataColumn
	                     exportOptions: {
	                         columns: ':visible.print-col'
	                     },
	                 },
	                 {
	                     extend: 'colvis',
	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	                     collectionLayout: 'fixed two-column',
	                     align: 'left'
	                 },
	             ]
	         }

	     });
		$('select').material_select();
	     $("select")
	         .change(function() {
	             var t = this;
	             var content = $(this).siblings('ul').detach();
	             setTimeout(function() {
	                 $(t).parent().append(content);
	                 $("select").material_select();
	             }, 200);
	         });
	     $('.dataTables_filter input').attr("placeholder", "Search");
	
	var msg="${complainResponse.errorMsg}";
	if(msg!=='' && msg != undefined)
	{
		Materialize.toast(msg, 2000, 'teal lighten-2');
		/* $('#addeditmsg').modal('open');
	     $('#msgHead').text("Message");
	     $('#msg').text(msg); */
	}
	$('#areaIds').change(function(){
		
		var areaId=$('#areaIds').val();
		var departmentId=$('#departmentIds').val();
		$('#errorMsg').html('');
		if(areaId==="")
		{
			//$('#errorMsg').html("<font color='red'>Select Area</font>");
			return false;
		}
		if(departmentId==="")
		{
			//$('#errorMsg').html("<font color='red'>Select DepartMent</font>");
			return false;
		}
		if(departmentId==="" && areaId==="")
		{
			//$('#errorMsg').html("<font color='red'>Select Area and DepartMent</font>");
			return false;
		}
		
		$.ajax({
			url : myContextPath+"/fetchEmployeeDetailListByAreaIdAndDepartmentIdForWeb/"+areaId+"/"+departmentId,
			dataType : "json",
			success : function(data) {
				
				employeeNameAndIdList=data.employeeNameAndIdList;
				$('#employeeId').empty();
				$("#employeeId").append('<option value="">Choose Employee</option>');
				for(var i=0; i<employeeNameAndIdList.length; i++)
				{	
					$("#employeeId").append('<option value='+employeeNameAndIdList[i].employeeId+'>'+employeeNameAndIdList[i].name+'</option>');
				}
				$("#employeeId").change();
			}
		});
		
	});
	
	$("#departmentIds").change(function(){
		
		var areaId=$('#areaIds').val();
		var departmentId=$('#departmentIds').val();
		$('#errorMsg').html('');
		if(areaId==="")
		{
			//$('#errorMsg').html("<font color='red'>Select Area</font>");
			return;
		}
		if(departmentId==="")
		{
			//$('#errorMsg').html("<font color='red'>Select DepartMent</font>");
			return;
		}
		if(departmentId==="" && areaId==="")
		{
			//$('#errorMsg').html("<font color='red'>Select Area and DepartMent</font>");
			return;
		}
		
		$.ajax({
			url : myContextPath+"/fetchEmployeeDetailListByAreaIdAndDepartmentIdForWeb/"+areaId+"/"+departmentId,
			dataType : "json",
			success : function(data) {
				
				employeeNameAndIdList=data.employeeNameAndIdList;
				$('#employeeId').empty();
				$("#employeeId").append('<option value="">Choose Employee</option>');
				for(var i=0; i<employeeNameAndIdList.length; i++)
				{	
					$("#employeeId").append('<option value='+employeeNameAndIdList[i].employeeId+'>'+employeeNameAndIdList[i].name+'</option>');
				}
			
			}
		});
		
	});
	
	
	$('#sendReply').click(function(){
		
		if($('#reply').val().trim()==="")
		{
			$('#replysendmsg').html("<font color='red'>Enter reply msg</font>");
			$('#reply').val('');
			return false;
		}
		
		
		var form = $('#saveFeedBackReply');
		//alert(form.serialize());
		$.ajax({
				type : form.attr('method'),
				url : form.attr('action'),
				data : form.serialize(),
				//async: false,
				success : function(data) {
					if(data.status==="Success")
					{
						$('#replysendmsg').html("<font color='green'>Reply send SuccessFully</font>");
						
						$('#reply').attr("readonly", "true");
						$('#sendReply').attr("disabled", "true");
						
						setTimeout(function() 
						  {
							  location.reload();
						  }, 1000);

					}
					else
					{
						$('#replysendmsg').html("<font color='red'>Reply sending Failed</font>");
					}
				}
		});
	});
	
	
	
	$('#sendComaplainButtonId').click(function(){
		
		/* if($('#textarea1').val().trim()==="")
		{
			$('#errorMsg').html("<font color='red'>Fill Complain Message</font>");
			return false;
		}
		var employeeId=$('#employeeId').val();
		if(employeeId==="0")
		{
			$('#errorMsg').html("<font color='red'>Select Employee</font>");
			return false;
		} */
		
		if ($("#saveComplainForm").valid()) {
		var form = $('#saveComplainForm');
		//alert(form.attr('action'));
		$.ajax({
			type : form.attr('method'),
			url : form.attr('action'),
			data : form.serialize(),
			dataType : "json",
			success : function(data) {
				if(data.status=="Success")
				{
						$('#errorMsg').html("<font color='green'>Send Complain Successfully</font>");
						
						  setTimeout(function() 
						  {
							  location.reload();
						  }, 1000);
				}
				else
				{
					$('#errorMsg').html("<font color='red'>Sending Complain Failed</font>");
				}
			}
		});
			}
	});
});

function openComplainModel()
{
	$.ajax({
		url : myContextPath+"/fetchAreaListAndDepartmentListForRaiseComplainForWeb",
		dataType : "json",
		success : function(data) {
			
			areaList=data.areaList;
			$('#areaIds').empty();
			$("#areaIds").append('<option value="">Choose Area</option>');
			for(var i=0; i<areaList.length; i++)
			{	
				$("#areaIds").append('<option value='+areaList[i].areaId+'>'+areaList[i].name+'</option>');
			}
			$("#areaIds").change();
			
			departmentList=data.departmentList;
			$('#departmentIds').empty();
			$("#departmentIds").append('<option value="">Choose Department</option>');
			for(var i=0; i<departmentList.length; i++)
			{	
				$("#departmentIds").append('<option value='+departmentList[i].departmentId+'>'+departmentList[i].name+'</option>');
			}
			$("#departmentIds").change();
			
			$('#complain').modal('open');
		}
	});
	
}

function showFeedBack(id)
{
	//alert(id)
	$('#complainReplyIdForReply').val(id);
	$('#replysendmsg').html("");
	$.ajax({
		url : myContextPath+"/fetchComplainReplyDetailsByComplainReplyIdForWeb/"+id,
		dataType : "json",
		success : function(data) {
			
			complainReplyList=data.complainReplyDetails;
			$('#message').val(complainReplyList.message);
			$('#message').attr("readonly", "true");
			
			//alert(complainReplyList.employeeReplyId.employeeId+" - "+employeeId);
			if( (complainReplyList.reply==="" || complainReplyList.reply==null) && complainReplyList.employeeReplyId.employeeId==employeeId)
			{			
				$('#reply').val('');
				$('#sendReply').removeAttr("disabled");
				$('#reply').val(complainReplyList.reply);
				$('#reply').removeAttr("readonly");
			}
			else
			{
				$('#reply').attr("readonly", "true");
				$('#reply').val(complainReplyList.reply);
				$('#sendReply').attr("disabled", "true");
			}
			
			$('#feedback').modal('open');
		}
	});
}




</script>
<style>
	  ::-webkit-input-placeholder { /* WebKit browsers */
    color:grey;
     opacity: 1 !important;
}
	#feedback{
		width:35% !important; 
	}
	#complain{
		width:40% !important; 
	}
</style>
</head>

<body>
     <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
  	<br>
    <div class="row">
    	<div class="col s12 m12 l12"> 
    	<div class="col s12 m12 l12 formBg">
    	<div class="col s12 m6 l6 center offset-l4">
    	<form action="${pageContext.request.contextPath}/fetchComplainDetailByEmpIdAndDateRangeForWeb" method="post">
              <input type="hidden" name="range" value="dateRange"> 
              <span class="showDates5">
                        <div class="input-field col s6 m3 l3">
                          <input type="date" class="datepicker" placeholder="Choose Date" name="fromDate" id="startDate" required> 
                          <label for="startDate">From</label>
                        </div>

                        <div class="input-field col s6 m3 l3">
                              <input type="date" class="datepicker" placeholder="Choose Date" name="toDate" id="endDate">
                               <label for="endDate">To</label>
                        </div>
                         <div class="col s12 m2 l2" style="margin-top:3%;">
                      		<button type="submit" class="btn blue-gradient">View</button>
                      	</div>
          	       </span>
        </form>
                
                
       </div>
              <div class="col s12 l2 m2 right-align" style="margin-top:1%;">
    			<button type="button" class="btn blue-gradient" onclick="openComplainModel()">Complain</button>
    		 </div>
     </div>
     </div>
     </div>  
        <div class="row">
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th rowspan="2" class="print-col">Sr. No</th>
                            <th colspan="3" class="print-col">From</th>
                            <th colspan="3" class="print-col">To</th>
                            <th rowspan="2" class="print-col">Message</th>
                            <th rowspan="2" class="print-col">Complaint date</th>
                            <th rowspan="2" class="print-col">Reply Date</th>
                            <th rowspan="2" class="print-col">Reply Status</th>
                        </tr>
                        <tr>
                            <th class="print-col">Name</th>
                            <th class="print-col">Dept</th>
                            <th class="print-col">Area</th>
                            <th class="print-col">Name</th>
                            <th class="print-col">Dept</th>
                            <th class="print-col">Area</th>
                        </tr>

                    </thead>

                    <tbody>
                    	<% int rowincrement=0; %>
                        <c:if test="${not empty complainResponse.complainReplyModelForWebList}">
						<c:forEach var="listValue" items="${complainResponse.complainReplyModelForWebList}">							
						<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
						<%-- <c:set var="employeeComplainAreaList" value="${fn:split(listValue.employeeComplainAreaList,',')}" />  
		                <c:set var="employeeReplyAreaList" value="${fn:split(listValue.employeeReplyAreaList,',')}" /> --%>
		                        <tr>
		                            <td><c:out value="${rowincrement}"/></td>
		                            <td><c:out value="${listValue.employeeDetailsComplain.name}"/></td>		                            
		                            <td class="wrapok"><c:out value="${listValue.employeeDetailsComplain.employee.department.name}"/></td>
		                            <td><a href="#area_complain_${rowincrement}" class="modal-trigger">View</a></td>
		                            <td><c:out value="${listValue.employeeDetailsReply.name}"/></td>		                            
		                            <td class="wrapok"><c:out value="${listValue.employeeDetailsReply.employee.department.name}"/></td>
		                            <td><a href="#area_reply_${rowincrement}" class="modal-trigger">View</a></td>
		                            <td><button type="button" id="showFeedBack" class="btn blue-gradient" onclick="showFeedBack('${listValue.complainReplyId}')">View</button></td>
		                            <td class="wrapok">
			                            <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.msgDateTime}"  />
					       		       	<c:out value="${date}" />
		                            </td>
		                        	<td class="wrapok">
			                        	<fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.replyDateTime}"  />
					       		       	<c:out value="${date}" />
		                        	</td>
		                        	<c:if test="${listValue.reply==null}">
		                        		<td>No</td> 
		                        	</c:if> 
		                        	<c:if test="${listValue.reply!=null}">
		                        		<td>Yes</td> 
		                        	</c:if>    
		                        	
		                        	
		                        	    
		                       </tr>
		                       
		                   
		                                
		                       
		                        
						
							</c:forEach>
                        </c:if>
                    </tbody>
                </table>
                <c:set var="rowincrementModel" value="0" scope="page"/>
                <c:if test="${not empty complainResponse.complainReplyModelForWebList}">
						<c:forEach var="listValue" items="${complainResponse.complainReplyModelForWebList}">							
						<c:set var="rowincrementModel" value="${rowincrementModel + 1}" scope="page"/>
						<c:set var="employeeComplainAreaList" value="${fn:split(listValue.employeeComplainAreaList,',')}" />  
		                <c:set var="employeeReplyAreaList" value="${fn:split(listValue.employeeReplyAreaList,',')}" />
		                	<div id="area_complain_${rowincrementModel}" class="modal row modal-fixed-footer">
		                        	<div class="modal-content">
		                        		  <h5 class="center"><u>Area Details</u></h5>		
		                        		<table class="centered tblborder">
		                        			<thead>
		                        				<tr>
		                        				<th>Sr. No</th>
		                        				<th>Area Name</th>
		                        				</tr>
		                        			</thead>
		                        			<tbody>
		                        				<c:set var="rowincrement" value="0" scope="page"/>
								                <c:forEach var="listValue" items="${employeeComplainAreaList}">
								                        <c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
		                        				<tr>
		                        					<td><c:out value="${rowincrement}"/></td>
		                        					<td><c:out value="${listValue}"/></td>
		                        				</tr>
		                        				</c:forEach>
		                        			</tbody>
		                        		</table>
		                        	</div>
		                        	<div class="modal-footer">								            
												<div class="col s6 m6 l5 offset-l2">
								                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
								                </div>
								     </div>
		                        </div>
							    
		                        <!-- Modal Structure for Area Details -->
					        	
								        <div id="area_reply_${rowincrementModel}" class="modal row modal-fixed-footer">
								            <div class="modal-content">
								                <h5 class="center"><u>Area Details</u></h5>
								                   
								                
								                <table id="modalComplainTable1" border="2" class="centered tblborder">
								                	
								                    <thead>
								                    	<tr>
								                            <th class="print-col">Sr.No</th>
								                            <th class="print-col">Area Name</th>								
								                        </tr>
								                    </thead>
								                    <tbody id="areaList1">
								                    	<c:set var="rowincrement" value="0" scope="page"/>
								                        <c:forEach var="listValue" items="${employeeReplyAreaList}">
								                        <c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
								                        		<tr>
										                            <td class="print-col"><c:out value="${rowincrement}"/></td>
										                            <td class="print-col"><c:out value="${listValue}"/></td>				
										                        </tr>
								                        </c:forEach>
								                    </tbody>
								                </table>
								            </div>
								            <div class="modal-footer">								            
												<div class="col s6 m6 l5 offset-l2">
								                <a href="#!" class="modal-action modal-close waves-effect btn">Ok</a>
								                </div>
								            </div>
								        </div>
						</c:forEach>
						</c:if>
            </div>
        </div>

        <!-- Modal Structure for sendMsg -->

        <div id="feedback" class="modal row">
        <form action="${pageContext.servletContext.contextPath}/updateComplainAndSendNotificationForWeb" method="post" id="saveFeedBackReply" >
            <div class="modal-content">
                <h4 class="center"><u>Reply</u></h4>
                <hr>
               
                
						<input name="complainReplyId" id="complainReplyIdForReply" value="" type="hidden">
                     
                            <div class="col s12 l8 m8 offset-l2 offset-m2">
                                <label for="message" class="black-text">Message</label>
                                <textarea id="message" class="materialize-textarea"></textarea>
                           </div>
                     
                       
                            <div class="col s12 l8 m8 offset-l2 offset-m2">
                                <label for="reply" class="black-text">Reply</label>
                                <textarea id="reply" class="materialize-textarea" name="reply"></textarea>
                            </div>
                         
                        <div>
                        	<span id="replysendmsg"></span>
                        </div>     
                </div>
            
            <div class="modal-footer col s12 l12 m12">
            <div class="divider red-text"></div>
            	<div class="col s12 l3 m3 offset-l3">
            	<button type="button" class="modal-action modal-close waves-effect btn red ">Cancel</button>
                
                </div>
                <div class="col s12 l3 m3 left-align">
                <button type="button" id="sendReply" class="modal-action waves-effect btn">Send</button>
                </div>
            </div>
            </form>
        </div>


<!-- Modal Structure for Customer details -->
<form action="${pageContext.servletContext.contextPath}/saveComplainAndSendNotificationForWeb" method="post" id="saveComplainForm">
<div id="complain" class="modal">


        <div class="modal-content">

            <h4 class="center"><u>Complain</u></h4>
            <hr>
            <div class="row" style="margin-bottom:0">

                <div class="input-field col s12 l4 m4">
                    <select id="areaIds" name="areas" required>
				      <option value="" selected>Area</option>
				      
				    </select>

                </div>


                <div class="input-field col s12 l4 m4">
                    <select id="departmentIds" name="departmentIds" required>
				      <option value=""  selected>Department</option>
				     
				    </select>

                </div>
                	<div class="input-field col s12 l4 m4">
                    <select id="employeeId" name="empId" required>
                    <option value="">Choose Employee</option>
				    </select>

                </div>
</div>
<div class="row">
                <div class="input-field col s12 l12 m12">
                    <textarea id="textarea1" name="message" class="materialize-textarea" required></textarea>
                    <label for="textarea1" class="black-text">Message</label>
                </div>
                <div class="input-field col s12 l12 m12 center">
                    <span id="errorMsg"></span>
                </div>
            </div>
        </div>
        <div class="modal-footer row">
          <hr>
          <div class="col s6 m3 l2 offset-l4">
          	<button type="button" class="modal-action modal-close waves-effect red btn">Cancel</button>
           
          </div>
          <div class="col s6 m3 l3">
             <button type="button" class="modal-action  waves-effect  btn" id="sendComaplainButtonId">Send</button>
          </div>
        </div>
        
</div>
</form>
			<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>

			

       
    </main>
    <!--content end-->
</body>

</html>