<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
 
<head>
     <%@include file="components/header_imports.jsp" %>
    
     <script type="text/javascript">
     	var myContextPath = "${pageContext.request.contextPath}";
     </script>
	 <script type="text/javascript" src="resources/js/moment.js"></script>
	<script type="text/javascript" src="resources/js/damageRecovery.js"></script>
	<script>
		$(document).ready(function(){
			var d=new Date();
		   	var currentYear=d.getFullYear();
		   	$(".yearSelect").val(currentYear);	 
		   	$(".yearSelect").change();
			var table = $('#tblData').DataTable();
			 table.destroy();
			 $('#tblData').DataTable({
		         "oLanguage": {
		             "sLengthMenu": "Show _MENU_",
		             "sSearch": "_INPUT_" //search
		         },
		  
		      	autoWidth: false,
		         columnDefs: [
		                      { 'width': '1%', 'targets': 0},
		                      { 'width': '3%', 'targets': 1},
		                      { 'width': '8%', 'targets': 2},
		                  	  { 'width': '2%', 'targets': 3},
		                  	  { 'width': '5%', 'targets': 4},
		                	  { 'width': '3%', 'targets': 5},
		                	  { 'width': '3%', 'targets': 6}		                	  
		                     ],
		         lengthMenu: [
		             [10, 25., 50, -1],
		             ['10 ', '25 ', '50 ', 'All']
		         ],
		         
		        
		        // dom: 'lBfrtip',
		        dom:'<lBfr<"scrollDivTable"t>ip>',
		         buttons: {
		             buttons: [
		                 //      {
		                 //      extend: 'pageLength',
		                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
		                 //  }, 
		                 {
		                     extend: 'pdf',
		                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                     customize: function(doc, config) {
		                    	 doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [40,90,100,40,90,80,80] 
		                    		 } 
		                    		    })
		                         /*for customize the pdf content*/ 
		                         doc.pageMargins = [5,20,10,5];   	                         
		                         doc.defaultStyle.fontSize = 8	;
		                         doc.styles.title.fontSize = 12;
		                         doc.styles.tableHeader.fontSize = 11;
		                         doc.styles.tableFooter.fontSize = 11;
		                         doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
		                       },
		                 },
		                 {
		                     extend: 'excel',
		                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                 },
		                 {
		                     extend: 'print',
		                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
		                     //title of the page
		                     title: function() {
		                         var name = $(".heading").text();
		                         return name
		                     },
		                     //file name 
		                     filename: function() {
		                         var d = new Date();
		                         var date = d.getDate();
		                         var month = d.getMonth();
		                         var year = d.getFullYear();
		                         var name = $(".heading").text();
		                         return name + date + '-' + month + '-' + year;
		                     },
		                     //  exports only dataColumn
		                     exportOptions: {
		                         columns: ':visible.print-col'
		                     },
		                 },
		                 {
		                     extend: 'colvis',
		                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
		                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
		                     collectionLayout: 'fixed two-column',
		                     align: 'left'
		                 },
		             ]
		         }

		     });
			 $("select").change(function() {
	           var t = this;
	           var content = $(this).siblings('ul').detach();
	           setTimeout(function() {
	               $(t).parent().append(content);
	               $("select").material_select();
	           }, 200);
	       });
	   $('select').material_select();
	   $('.dataTables_filter input').attr("placeholder", "Search");
	   //$(".showQuantity").hide();
       $(".showDates").hide();
       $("#oneDateDiv").hide();
      
       $(".rangeSelect").click(function() {
      
           $(".showDates").show();
         //  $(".showQuantity").hide();
           $("#oneDateDiv").hide();
       });
       $(".pickdate").click(function(){
       	//$(".showQuantity").hide();
	   		 $(".showDates").hide();
	   		$("#oneDateDiv").show();
	   	}); 	
	});
	</script>
<style>
    .card{
     height: 2.5rem;
     line-height:2.5rem;
     }
    .card-image{
        	width:40%;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
	padding:5px;
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}

tfoot td{
	border:1px solid #9e9e9e;
	text-align:center !important;
}
.commentSection{
	height:200px;
	overflow-y:auto;
	border:1px solid #e0e0e0;
}
.commentSection p{
	padding:10px;
}
/* #startYear_table,
#endYear_table{
	display:none !important;
}
 */
</style>


</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="row">
       		   <%-- <div class="col s12 m12 l12" style="padding-bottom:15px;">
       		   <form method="post" action="${pageContext.request.contextPath}/damageRecoveryList">
       		   <div class="input-field col s6 m2 l1 right right-align" style="margin-top:3%">
                    		<button type="submit" class="btn">View</button>
                      </div>
       		   <div class="col s12 m4 l4 right">	
       		   
            		<fieldset>
 					 <legend>Filter</legend>
                      <div class="input-field col s12 m6 l6 right-align">
                      	<!-- <input type="text" id="startYear" class="datepicker"> -->
                        <select id="startMonthId" name="startMonth" required>
                                 <option value="">Month</option>
                                 <option value="1">Jan</option>
                                 <option value="2">Feb</option>
                                 <option value="3">Mar</option>
                                 <option value="4">Apr</option>
                                 <option value="5">May</option>
                                 <option value="6">June</option>
                                 <option value="7">July</option>
                                 <option value="8">Aug</option>
                                 <option value="9">Sept</option>
                                 <option value="10">Oct</option>
                                 <option value="11">Nov</option>
                                 <option value="12">Dec</option>                                 
                        </select>
                      </div>
                      <div class="input-field col s12 m6 l6 right-align">
                      		<!-- <input type="text" id="endYear" class="datepicker"> -->
                            <select id="startYearId" class="yearSelect" name="startYear" required>
                                 <option value="">Start Year</option>
                                 <%int year=2017;
                                 for(year=2017; year<2051; year++){%>
                                 	<option value="<%=year%>"><%=year%></option>
                                 <%} %>
                            </select>   
                       </div>
                       </fieldset>
                       </div>
                 		
                  
                  </form>
                
                </div> --%>
                
                <div class="col s12 m3 l3 right right-align" style="margin-top:0.6%;width:15%;">
           
            <!-- <div class="col s6 m7 l7 right right-align" style="margin-top:3%;"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>                    
                     <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=today">Today</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/damageReport?range=viewAll">View All</a></li>
                    </ul>
              
               </div>
                 
               <div class="input-field col s12 l6 m6 right" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/damageReport" method="post">
	                    <div class="input-field col s12 m2 l4">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
                <div class="input-field col s12 l6 m6 right" style="margin-top:0.5rem;">      
           		 <form action="${pageContext.servletContext.contextPath}/damageReport" method="post">
                    <input type="hidden" name="range" value="range"> 
                    	  <span class="showDates">
                              <div class="input-field col s6 m2 l3">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s6 m2 l3">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s6 m2 l2">
                            <button type="submit">View</button>
                            </div>
                          </span>
                </form>                
         </div>
                
          
                    
                    <div class="col s12 m12 l12" style="padding:0">
                    
                        <table class="striped highlight centered" id="tblData">
                            <thead>
                                <tr>
                                    <th class="print-col">Sr.No</th>
                                    <th class="print-col">OrderId/GateKeeper Name</th>
                                    <th class="print-col">Product Name</th>
                                    <th class="print-col">Qty</th>
                                    <th class="print-col">Collecting Person</th>
                                    <th class="print-col">Department</th>                                    
                                    <th class="print-col">Date & Time</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            <% int rowincrement=0; %>
                            <c:if test="${not empty damageDefineList}">
									<c:forEach var="listValue" items="${damageDefineList}">
									<c:set var="rowincrement" value="${rowincrement + 1}" scope="page"/>
		                            	<tr>
		                            		<td><c:out value="${rowincrement}" /></td>
		                            		<td><c:out value="${listValue.orderId}" /></td>
		                            		<td><a class="modal-trigger" href="#damageDetails_${rowincrement}"><c:out value="${listValue.product.productName}" /></a></td>		                            		
		                            		<td><c:out value="${listValue.qty}" /></td>
		                            		<td><c:out value="${listValue.collectingPerson}" /></td>
		                            		<td><c:out value="${listValue.departmentName}" /></td>
		                            		<td>
		                            		   <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.date}"  /><c:out value="${date}" />
		                            		   <br/><fmt:formatDate pattern="HH:mm:ss" var="time" value="${listValue.date}" /><c:out value="${time}" />
		                            		</td>
		                            		
		                            	</tr>
		                            	
		                            	<div id="damageDetails_${rowincrement}" class="modal row">
				                    		<div class="modal-content">
				                    			<h5 class="center"><u>Damage Details</u></h5>
				                    			
				                    			<div class="col s6 m6 l6">
				                    				<div class="col s2 m5 l5">
				                    					<h6><b>Damaged Qty</b></h6>	
				                    				</div>
				                    				<div class="col s6 m6 l6">
				                    					<h6 id="damageQty"><c:out value="${listValue.qty}" /></h6>	
				                    				</div>
				                    			</div>
				                    			<div class="col s6 m6 l6">
				                    				<div class="col s6 m5 l5">
				                    					<h6><b>Date & Time</b></h6>	
				                    				</div>
				                    				<div class="col s6 m6 l6">
				                    					<h6 id="dateTime"><fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.date}"  /><c:out value="${date}" /></h6>	
				                    				</div>
				                    			</div>
				                    			<div class="col s12 m12 l12">
				                    				<div class="col s2 m3 l3" style="width:20%">
				                    					<h6><b>Product Name</b></h6>	
				                    				</div>
				                    				<div class="col s6 m6 l6 left-align">
				                    					<h6 id="prodName"><c:out value="${listValue.product.productName}" /></h6>	
				                    				</div>
				                    			</div>
				                    			<div class="col s12 m12 l12">
				                    			<div class="col s12 m12 l12">
				                    			<br>
				                    				<h6><b>Comment</b></h6>
				                    			</div>
				                    			<div class="col s12 m12 l12">
				                    				<div class="commentSection">
				                    					<p><c:out value="${listValue.reason}" /></p>
				                    				</div>
				                    			</div>
				                    			</div>
				                    		</div>
				                    		<div class="modal-footer">
				                    			<div class="col s12 m2 l2 offset-l5 center center-align">
				                    				<a class="btn modal-action modal-close waves-effect">Ok</a>
				                    			</div>				                    			
				                    		</div>
				                    	</div>
		                            	
                            		</c:forEach>
                            </c:if>
                            </tbody>
                           
                        </table>
                        <br><br>
                    </div>
              </div>
                    	
                    	
      	 <div class="row">
			<div class="col s12 m12 l8">
				<div id="addeditmsg" class="modal">
					<div class="modal-content" style="padding:0">
					<div class="center white-text" id="modalType" style="padding:3% 0 3% 0"></div>
						<!-- <h5 id="msgHead"></h5> -->
						
						<h6 id="msg" class="center"></h6> 
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect btn" >OK</a>
					</div>
				</div>
			</div>
		</div>
       
    </main>
    <!--content end-->
</body>

</html>