<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
    <%@include file="components/header_imports.jsp" %>
    <script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
	

<script type="text/javascript">
var myContextPath = "${pageContext.request.contextPath}"
$(document).ready(function() {
});

</script>

<style> 
	#addeditmsg{
		border-radius:10px;
		 width:30% !important; 
	}
	
	#toast-container {
    top: 35%;
    right: 35%;
    max-width: 86%;
}
i span{
	font-size:15px !important;
	padding-left:4%;
}
.select-wrapper span.error{
	margin-left:0 !important;
}		
	
	/* .modal{
		width:30% !important;
	} */
	
	.background {
            background: grey;
            position: relative;
            height: 100vh;
            width: 100vw;
        }
        
        .img {
            display: block;
            position: absolute;
            height: 100%;
            width: 100%;
            background: url('resources/img/loginBack.jpg') no-repeat;
            background-size: cover;
            -webkit-filter: blur(2px);
            -moz-filter: blur(2px);
            -ms-filter: blur(2px);
            filter: blur(2px);
        }
        
        .img:before {
            content: '';
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background-image: linear-gradient(to bottom right, #002f4b, #dc4225);
            opacity: .6;
        }
        
        .overlay {
            display: block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            background-image: -webkit-gradient( linear, left top, left bottom, color-stop(0, rgba(0, 0, 0, 0.5)), color-stop(1, rgba(0, 0, 0, 0.2)));
            /* Opera 11.1 - 12 */
            background-image: -o-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Firefox 3.6 - 15 */
            background-image: -moz-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Safari 5.1, iOS 5.0-6.1, Chrome 10-25, Android 4.0-4.3 */
            background-image: -webkit-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* IE */
            background-image: -ms-linear-gradient(bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
            /* Opera 15+, Chrome 25+, IE 10+, Firefox 16+, Safari 6.1+, iOS 7+, Android 4.4+ */
            background-image: linear-gradient(to bottom, #303641 0%, rgba(0, 0, 0, 0.5) 100%);
        }
        .paddingBody1 input.select-dropdown{
        	color:#bdbdbd;
        }
        .paddingBody1 .select-wrapper span.caret{
        	color:#bdbdbd !important;
        } 
</style>
</head>

<body>
   <!--navbar start-->
   	<c:if test="${sessionScope.showNavbar==true}">
   	 <%@include file="components/navbar.jsp" %>
   	 <main class="paddingBody">
      
  	<div class="container">
        	 <br><br><br><br><br><br>
            <form action="${pageContext.request.contextPath}/setCompany" method="post" id="setComapanyForm">
                
               	
              
                <div class="col l8 m8 s12 z-depth-3">
                    
                    
                    <div class="row">
                    <div class="col l12 m12 s12">
                        <h4 class="center"> Set Company </h4>
                    </div>
                       <div class="input-field col s12 m5 l5 offset-l3 offset-m3">
                        <i class="material-icons prefix">location_on</i>
                   
                        <select name="companyId" id="companyList">
                                 <option value="-1" selected>Choose Company</option>
                                <c:if test="${not empty companyList}">
								<c:forEach var="listValue" items="${companyList}">
								
									<option value="<c:out value="${listValue.companyId}" />"><c:out
											value="${listValue.companyName}" /></option>
								</c:forEach>
							</c:if>
                        </select>
                        </div>
                      
                       <div class="input-field col s12 m12 l12  center-align">
                         <br>
                    		<button class="btn waves-effect waves-light blue-gradient" type="submit" id="setBranch">Set Company<i class="material-icons right">send</i> </button>
                    		<br><br>                     		
                	  </div>
                 </div>
                </div>
                 <br>
              
               
              
            </form>

        </div>
		

    </main>
   	 </c:if> 
    <!--navbar end-->
    <!--content start-->
    <c:if test="${sessionScope.showNavbar==false}">
    <main class="paddingBody1">
      
        <div class="background">
        <div class="img"></div>
        <div class="overlay"></div>
        <div class="container" style="transform: translate(20px, 50px);">
            <form action="${pageContext.request.contextPath}/setCompany" method="post" id="setComapanyForm">
                <div class="row">
                    <div class="col s12 m12 l8 offset-l2">
                                <br><br><br><br><br><br>                    
                            <div class="col l8 m8 offset-l2 z-depth-5">
                                
                               	<div class="col l12 m12 s12">
                       				 <h4 class="center grey-text text-lighten-1"> Set Company </h4>
                  			   </div>
                               
                                <div class="row" style="margin-bottom:4%">
                                    <div class="input-field col s12 m10 l10 offset-l1 offset-m1">
                        				<i class="material-icons prefix grey-text text-lighten-1">account_balance</i>
                   
				                        <select name="companyId" id="companyList">
				                                 <option value="-1" selected>Choose Company</option>
				                                <c:if test="${not empty companyList}">
												<c:forEach var="listValue" items="${companyList}">
												
													<option value="<c:out value="${listValue.companyId}" />"><c:out
															value="${listValue.companyName}" /></option>
												</c:forEach>
											</c:if>
				                        </select>
                       				</div>
                       				
                                </div>
                                
                                <div class="col s12 l12 m12   center center-align">
                             	 		<button class="btn waves-effect waves-light blue-gradient" type="submit" id="setBranch">Set Company<i class="material-icons right">send</i> </button>                     		
										 <br><br>
                                </div>
                               
                            </div>
                         
                        </div>
                    <!-- </div> -->
                </div>
            </form>
            </div>
             		 
       		
		</div>
 </c:if> 
		

    </main>
    <!--content end-->
</body>

</html>