<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
      <%@include file="components/header_imports.jsp" %>
<script  type="text/javascript" src="resources/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="resources/js/additional-method.js"></script>
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
<script type="text/javascript">

var otpValidation=false;
$(document).ready(function() {
	$(".eye-slash").hide();
    $(".eye").click(function() {
        $(".eye").hide();
        $(".eye-slash").show();
        $("#passwordId").attr("type", "text");
        $("#currentPass").attr("type", "text");
        
    });
    $(".eye-slash").click(function() {
        $(".eye-slash").hide();
        $(".eye").show();
        $("#passwordId").attr("type", "password");
        $("#currentPass").attr("type", "password");
    });
    $(".eye-slash1").hide();
    $(".eye1").click(function() {
        $(".eye1").hide();
        $(".eye-slash1").show();
        $("#confirmPasswordId").attr("type", "text");
    });
    $(".eye-slash1").click(function() {
        $(".eye-slash1").hide();
        $(".eye1").show();
        $("#confirmPasswordId").attr("type", "password");
    });
	$("#changePassDiv").hide();
	$("#currentPassError").text("");
	
	$("#cuurentPassBtn").click(function(){
		var currentPass=$("#currentPass").val().trim();
		if(currentPass == "" || currentPass== undefined){
			$("#currentPassError").text("This field is required");
			return false;
		}
		
		$.ajax({
			type:'POST',
			url : myContextPath+"/checkGateKeeperPassword?password="+currentPass,
			success : function(data){
				if(data!=="Failed"){
					$('#currentPassDiv').hide();
					$('#changePassDiv').show();
					Materialize.toast(data, '4000', 'teal lighten-2');	
					
				}else{
					$("#currentPassError").text("Please enter a valid password");
					//Materialize.toast("Check Your EmailId and Password", '4000', 'teal lighten-2');	
					
				}
							
			},
			error: function(xhr, status, error) {
				Materialize.toast('Something Went Wrong', '4000', 'teal lighten-2');
			}
		});	
		
	});
	$("#changePassForm").validate({
		rules: {
			password: "required",
			confpassword: {
		      equalTo: "#passwordId"
		    }
		  },
		
	
	   errorElement : "span",
	    errorClass : "invalid error",
	    errorPlacement : function(error, element) {
	      var placement = $(element).data('error');
	    
	      if (placement) {
	        $(placement).append(error)
	      } else {
	        error.insertAfter(element);
	      }
	      $('select').change(function(){ $('select').valid(); });
	    }
	});
});
	/* $('#emailIdAndMobileNumberDiv').show();
	$('#otp_pass_Div').hide();
	
	$('#getOtpId').click(function(){
		
		var emailIdAndMobileNumberId=$('#emailIdAndMobileNumberId').val();
		if(emailIdAndMobileNumberId==''){
			Materialize.toast('Enter Email-Id or Mobile Number', '4000', 'teal lighten-2');
			return false;
		}	
		$('#getOtpId').prop('disabled', true);
		
		$.ajax({
			type:'POST',
			url : myContextPath+"/forgetPasswordSendOtp_gk",
			data:{
				emailIdAndMobNo : emailIdAndMobileNumberId
			},
			success : function(data){
				if(data!=="Failed"){
					$('#emailIdAndMobileNumberDiv').hide();
					$('#otp_pass_Div').show();
					Materialize.toast(data, '4000', 'teal lighten-2');	
					
				}else{
					Materialize.toast("Check Your EmailId and Password", '4000', 'teal lighten-2');	
					$('#getOtpId').removeAttr("disabled");
				}
							
			},
			error: function(xhr, status, error) {
				Materialize.toast('Something Went Wrong', '4000', 'teal lighten-2');
			}
		});	
		
	});
	
	$('#otpNumberId').blur(function(){
		
		var otpNumber=$('#otpNumberId').val();
		
		$.ajax({
			type:'POST',
			url : myContextPath+"/checkGatekeeperOTP",
			data:{
				otp : otpNumber
			},
			success : function(data){
				if(data==="success"){
					otpValidation=true;
				}else{
					otpValidation=false;
					Materialize.toast("Invalid OTP", '4000', 'teal lighten-2');	
				}
							
			},
			error: function(xhr, status, error) {
				Materialize.toast('Something Went Wrong', '4000', 'teal lighten-2');
			}
		});
		
	});
	
	$('#changePasswordId').click(function(){
		
		if(otpValidation==false){
			Materialize.toast("Invalid OTP", '4000', 'teal lighten-2');				
			return false;
		}
		
		var pass=$('#passwordId').val();
		var confPass=$('#confirmPasswordId').val();
		if(pass!==confPass){
			Materialize.toast("Confirm password Must be same as Password", '4000', 'teal lighten-2');				
			return false;
		}
		
	});
	
	$('#confirmPasswordId').blur(function(){
		var pass=$('#passwordId').val();
		var confPass=$('#confirmPasswordId').val();
		if(pass!==confPass){
			Materialize.toast("Confirm password Must be same as Password", '4000', 'teal lighten-2');				
			return false;
		}
	}); */
	




</script>
<style>
	#upload-error{
		display:block;
	}
</style>
</head>

<body>
   <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br>
        <div class="container">
             <br> <br> 
                <div class="row  z-depth-3">
                    <br>  
                    	<div class="col s12 m12 l12">
                    		<h4 class="center center-align">Reset Password</h4>	
                    	</div>
                    <div class="row" id="currentPassDiv">
                    	<div class="input-field col s11 m4 l4 offset-l4 offset-m4">
							 <i class="material-icons prefix">lock</i>
	                        <input id="currentPass" type="text" class="validate"  name="currentPass">
	                        <label for="currentPass" class="active"><span class="red-text">*</span>Current Password</label> 
	                         <span class="invalid error" id="currentPassError"></span>                  
	                    </div>
	                    <div class="input-field col s1 m1 l1 pull-l1 pull-m1 pull-s1" style="transform:translate(0,10px)">
	                  
	                        <i class="fa fa-eye  right eye" aria-hidden="true"></i>
	                        <i class="fa  fa-eye-slash right eye-slash" aria-hidden="true"></i>
                   		 </div>
	                    <div class="input-field col s12 m12 l12 center-align">
	                        <button class="btn waves-effect waves-light blue-gradient" type="button" id="cuurentPassBtn">submit<i class="material-icons right">send</i> </button>
	                        <br><br>
	                    </div>
                    </div>
                    <div class="row" id="changePassDiv">
                    <form id="changePassForm" action="${pageContext.request.contextPath}/forgetPasswordGatekeeper" method="post" name="changePassForm">
                    	<div class="input-field col s11 m4 l4 offset-l4 offset-m4">
		                        <i class="material-icons prefix">lock</i>
		                        <input id="passwordId" type="text" class="validate" name="password" required>
		                        <label for="passwordId" class="active"><span class="red-text">*</span>New Password</label>
		                    </div>
		                    <div class="input-field col s1 m1 l1 pull-l1 pull-m1 pull-s1" style="transform:translate(0,10px)">
	                  
	                        <i class="fa fa-eye  right eye" aria-hidden="true"></i>
	                        <i class="fa  fa-eye-slash right eye-slash" aria-hidden="true"></i>
                   		 </div>
		                    <div class="input-field col s11 m4 l4 offset-l4 offset-m4">
		                        <i class="material-icons prefix">lock</i>
		                        <input id="confirmPasswordId" type="text" class="validate" name="confpassword" required>
		                        <label for="confirmPasswordId" class="active"><span class="red-text">*</span>Confirm Password</label>
		                    </div>
		                    <div class="input-field col s11 m1 l1  pull-l1 pull-m1 pull-s1" style="transform:translate(0,10px)">
                        <i class="fa fa-eye   eye1 right" aria-hidden="true"></i>
                        <i class="fa  fa-eye-slash  eye-slash1 right" aria-hidden="true"></i>
                   
                    	</div>
		                    <div class="input-field col s12 m12 l12 center-align">
		                        <button class="btn waves-effect waves-light blue-gradient" type="submit" id="changePasswordId">Change Password<i class="material-icons right">send</i> </button>
		                        <br><br>
		                    </div>
		                    </form>
                    </div>
                            
                   <%--  <div class="row" id="emailIdAndMobileNumberDiv">
	                     <div class="input-field col s12 m4 l4 right-align" >
							 <i class="fa fa-inr prefix"></i>
	                        <input id="emailIdAndMobileNumberId" type="text" class="validate"  name="companyName" required>
	                        <label for="emailIdAndMobileNumberId" class="active"><span class="red-text">*</span>Email-Id Or Mobile Number</label>                       
	                    </div>
	                    <div class="input-field col s12 m6 l6 offset-l3  offset-m3 center-align">
	                        <button class="btn waves-effect waves-light blue-gradient" type="button" id="getOtpId">Get OTP<i class="material-icons right">send</i> </button>
	                        <br><br>
	                    </div>
	              	</div>
	              	<div class="row" id="otp_pass_Div">
	              		<form action="${pageContext.servletContext.contextPath}/forgetPasswordGatekeeper" method="post">
		                    <div class="input-field col s12 m5 l4 push-l1 push-m1">
		                        <i class="material-icons prefix">shopping_cart</i>
		                        <input id="otpNumberId" type="text" class="validate" name="otpNumber" required>
		                        <label for="otpNumberId" class="active"><span class="red-text">*</span>OTP Number</label>
		                    </div>
		                   <div class="input-field col s12 5 l4 right-align">
		                        <i class="fa fa-inr prefix"></i>
		                        <input id="passwordId" type="text" class="validate" name="password" required>
		                        <label for="passwordId" class="active"><span class="red-text">*</span>Password</label>
		                    </div>
		                    <div class="input-field col s12 5 l4 right-align">
		                        <i class="fa fa-inr prefix"></i>
		                        <input id="confirmPasswordId" type="text" class="validate" name="confpassword" required>
		                        <label for="confirmPasswordId" class="active"><span class="red-text">*</span>Confirm Password</label>
		                    </div>
		                    <div class="input-field col s12 m6 l6 offset-l3  offset-m3 center-align">
		                        <button class="btn waves-effect waves-light blue-gradient" type="submit" id="changePasswordId">Change Password<i class="material-icons right">send</i> </button>
		                        <br><br>
		                    </div>
		                </form>
                  </div> --%>
                    
                    

                </div>
        </div>
       
    	<div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center success  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>    
    </main>
    <!--content end-->
</body>

</html>