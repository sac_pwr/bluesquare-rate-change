<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
  <%@include file="components/header_imports.jsp" %>
  <script>var myContextPath = "${pageContext.request.contextPath}"</script>
  <script type="text/javascript" src="resources/js/hash-map-collection.js"></script>
  
  <c:if test="${isEdit==false}">
  	<script type="text/javascript" src="resources/js/counter.js"></script>
  </c:if>
  
  <c:if test="${isEdit==true}">
  	<script type="text/javascript">
  		const counterOrderId="${counterOrder.counterOrderId}";
  		$(".paymentSection").hide();
  	</script>
  	<script type="text/javascript" src="resources/js/counterEdit.js"></script>
  </c:if>
   
  <script>
  	$(document).ready(function(){
  		$('.num').keypress(function( event ){
		    var key = event.which;
		    
		    if( ! ( key >= 48 && key <= 57 || key === 13) )
		        event.preventDefault();
		});

  		
  		$(".chequeForm").hide();
	    $(".partialForm").hide(); 

  		$('.creditDueDate').hide();
		$(".chequeForm").hide();
	   	$(".partialForm").hide(); 
	   	$(".dueDateExtraPayDiv").hide();
	   
	    	 
	    	 $('#cash').click(function() {
	    		 $(".chequeForm").hide();
	               
	            });
	            $('#cheque').click(function() {
	            	
	                $('.chequeForm').show();
	               
	            });
	            $('#partialPay').click(function() {
	            	 $(".partialForm").show(); 
	            	 $('.paymentTypeSection').show();
	            	 $('.creditDueDate').hide();
	            });
	            $('#instantPay').click(function() {
	            	
	                $('.partialForm').hide();
	                $('.creditDueDate').hide();
	                $('.paymentTypeSection').show();
	            });
 				 $('#credit').click(function() { 
		            	
		                $('.partialForm').hide();
		                $('.creditDueDate').show();
		                $('.paymentTypeSection').hide();
		            });
 				$('.select2').material_select('destroy');
				$('.select2').select2({
					placeholder : "Choose your Product",
					allowClear : true
				});
				$(".select2").select2({
					width : 'resolve'
				});
				/* $(".btnPay").click(function(){
					var businessList=$("#businessList option:selected").val();
					
					if(businessList=="" ||businessList==undefined){
						$("#custInfo").modal('open');
					}
				}); */
				
				 $('#logo-collapse').toggle();
		         $('#logo-collapse-full').toggle();
		         $('.element-sideNav').toggle();
					$("#slide").toggleClass("reveal-open");
					$('.side-nav').toggleClass("reveal-open");
					$('main').toggleClass("paddingBody");
					$('main').toggleClass("intro");
				
  	});
  </script>
  <style>
  	#custInfo{
  	width:35% !important;
  	}
  	#tblProduct input{
	margin-bottom:0px;
	height:1rem;
	}
	.select2 {
	width: 100% !important;
	float:right;
	height:3rem !important;
	
}

.select-wrapper span.caret {
	color: initial;
	position: absolute;
	right: 0;
	top: 0;
	bottom: 0;
	height: 10px;
	margin: 10px 0;
	font-size: 10px;
	line-height: 10px;
}


  </style>
</head>
<body>
  <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
    	<input type="hidden" id="memberIdentifier" value="nonMember">
    	
    	<div class="row">
		<div class="col l6 m6 s12">
			<div class="card z-depth-4">
				<div class="card-content">
					<span class="card-title sidenav-color white-text center grey-text text-lighten-2"
						style="padding: 5px; font-size: 30px; font-family: Serif;">Product</span>
					<div class="row bottomPadding">
						<!-- <form action="sales_buying_product" method="post"> -->
						<div class="input-field col s12 m12 l7" style="margin-top:3%">
							<select id="productId" class="select2" required>
								<option value="0"  selected>Choose your Product <span class="red-text">*</span> </option>
								<c:if test="${not empty productList}">
									<c:forEach var="product" items="${productList}" varStatus="status">
										<option value="${product.productId}" id="">${product.productName}</option>
									</c:forEach>
								</c:if>
							</select>
							<!--  <label>Select Product</label>  -->
						</div>
						<!-- pattern="^[\d.]{0,3}$" -->
						<div class="input-field col l2 m4 s6">
							<input id="quantityId" type="text"
								class="num"
								title="only number allowed" required> 
								<label for="quantityId">Quantity <span class="red-text">*</span></label><br>
						</div>
						
						
						
		     		    <div class="input-field col l2 m2 s4" style="margin-top:4%">
							<button class="waves-effect waves-light btn button-submit btnEffect"
								name="CalcHF" id="addCartButtonId" value="submit"
								style="width: 115px;">Submit <i class="material-icons tiny right btnHover">navigate_next</i></button>
						</div>
						<div class="col s12 m4 l4" style="margin-top:2%;">
		              	  	 <input name="group1" type="radio" id="freeId"/>
		     				 <label for="freeId" style="padding-left:25px;">Free</label>
		     				 <input name="group1" type="radio" id="nonFreeId" checked />
		     				 <label for="nonFreeId" style="padding-left:25px;">Non-Free</label>
		     		    </div>
						
						<!-- </form> -->
					</div>
				<!-- </div> -->
			<!-- </div>
			<div class="card z-depth-4">
				<div class="card-content">
					<span class="card-title teal white-text center"
						style="padding: 10px; font-size: 40px; font-family: Serif;">Product
						Barcode</span> -->
					<div class="row bottomPadding">
						<!-- <form action="sales_buying_barcode" method="post"> -->
							<div class="input-field col l11 m12 s12">
								<input id="barcode" name="barcode" type="text" maxlength="10" placeholder="Enter Barcode Number here" autofocus="autofocus" required> <label for="rfidno">Bar
									Code <span class="red-text">*</span> </label>
							</div>
					
							<div class="input-field col l1 m1 s1">
								<button id="barcodeSubmit" class="waves-effect waves-light btn button-submit btnEffect"
									name="CalcHF" value="submit" type="button" style="width: 115px;visibility:hidden;">Submit <i class="material-icons tiny right btnHover">navigate_next</i></button>
							</div>
						<!-- </form> -->
					</div>
				</div>
			</div>
			<div class="row z-depth-4" style="padding: 10px;">
				<div class="col l12">
					<form action="prepaid" method="post">
						<h4 class="center-align sidenav-color white-text center grey-text text-lighten-2"
							style="margin: 5px; font-size:30px; font-family: serif; padding: 5px;">Purchase
							Section</h4>
						<div class="input-field col l12 m12 s12 center">
							
								<div class="input-field col l12 m12 s12">
							
								<select class="select2" id="businessListId"  name="businessList" required style="padding-top:20px;">
									<option value="0" selected>Business List<span class="red-text">*</span> </option>
									<c:if test="${not empty businessNameList}">
										<c:forEach var="businessName" items="${businessNameList}" varStatus="status">
											<option value="${businessName.businessNameId}">${businessName.shopName}</option>
										</c:forEach>
									</c:if>
								</select>
							<button type="submit" class="btn"
								style="float: right; visibility: hidden;">Check</button>
							</div>
						</div>
					</form>
				</div>
			
			</div>
			
		</div>
		<!--div 2 for bill and main products-->
		<div class="col l6 m6 s12">
			<div class="card z-depth-4">
				<div class="card-content" style="height: 54vh;overflow-y:auto;">
					<span class="card-title grey-text text-lighten-2 sidenav-color white-text center"
						style="padding: 5px; font-size: 30px; font-family: Serif;">Bill</span>
					<table id="tblProduct" class="highlight centered">
						<thead>
							<tr>
							<!-- <th>Sr No.</th> -->
								<th width="50%">Product</th>
								<!-- <th>Curr Qty</th> -->
								<th>MRP</th>
								<th>Quantity</th>
								<th>Total</th>
								<!-- <th>Edit</th> -->
								<th><i class="material-icons">delete</i></th>
							</tr>
						</thead>
						<tbody id="cartTbl">
						<!-- <tr>
							<td>1</td>
							<td>mouse</td>
							<td>24</td>
							<td><input type="text" value='1'
										class="editable center"  style="width: 4em;" required /></td>
							<td><input type="text" value='2'
										class="editable center"  style="width: 4em;" required /></td>
							<td>2</td>
							<td><a class="deleteIcon"
										href="#"><i
											class="material-icons">delete</i></a></td>
						</tr> -->
							<%-- <c:forEach var="tsales" items="${tsalesList}" varStatus="status">
								<tr>
									<td>1</td>	
									<td>${tsales.pname}</td>
									<fmt:formatNumber var="unitCost" value='${tsales.unitCost}'
										minFractionDigits="2" maxFractionDigits="2" />
									<td>${unitCost}</td>
									<fmt:formatNumber var="pquantity" value='${tsales.pquantity}'
										minFractionDigits="2" maxFractionDigits="2" />
									<td><input type="text" value='${tsales.pquantity}'
										class="editable" readonly style="width: 4em;" required /></td>
									<td>${pquantity}</td>
									<fmt:formatNumber var="pamt" value='${tsales.pamt}'
										minFractionDigits="2" maxFractionDigits="2" />
									<td><input type="text" value='${tsales.pamt}' readonly
										class="editable" style="width: 4em;" required /></td>
									<td><a href="#" class="editFields"><i
											class="material-icons editIcon">edit</i> <i
											class="material-icons saveIcon">save</i></a></td>
									<td><a class="deleteIcon"
										href="<c:url value="/deletets/${tsales.tsalesid} "/>"><i
											class="material-icons">delete</i></a></td>
								</tr>
							</c:forEach> --%>
						</tbody>
					</table>
				</div>
				
				<div class="card-action" style="padding-bottom: 0.1%;">
					<div class="row">
						<div class="col l4 m6 s6 left">
							<strong>Total : <span id="totalValue">&#8377; <font color="blue"><b>0</b></font></span></strong>
						</div>
						<div class="col l4 m6 s6 center">
						<c:if test="${isEdit==true}">
							
								<strong><span id="balanceTotalValue">&#8377; <font color="blue"><b>0</b></font></span></strong>
							
						</c:if>
						</div>
						<%-- <c:if test="${isEdit==true}">
							<div class="dueDateExtraPayDiv"> 
									<div class="row">								
										<div class="input-field col l4 m4 s12 dueDateExtraPayDiv">
											<input type="date" class="datepicker" placeholder="Due Date" name="chequeDate" id="dueDateExtraPay"> 
												<label for="dueDateExtraPay">Due Date <span class="red-text">*</span> </label>
										</div>
									</div>
							 </div>
						</c:if>	 --%>
							
							<div class="col l4 m6 s6 right-align">
								<button type="button" id="clearButtonId" class="waves-effect waves-light btn red btnEffect" style="width: 115px;">
										Clear<i class="material-icons tiny right btnHover">clear_all</i>
									</button>
							</div>
					</div>
					
					<%-- <c:if test="${isEdit==false}"> --%>
					<div class="paymentSection">
						<div class="row">
								<div class="col s12 m12 l12 divider"></div>
								<br>
								<div class="col s12 m4 l4">
									<input name="group2" type="radio" id="instantPay" class="payTypesCls" checked />
	      							<label for="instantPay">Instant Pay</label>
								</div>
								<div class="col s12 m4 l4">
									<input name="group2" type="radio" id="credit" class="payTypesCls" />
	      							<label for="credit">Credit</label>
								</div>
								
								<div class="col s12 m4 l4">
									<input name="group2" type="radio" id="partialPay" class="payTypesCls" />
	      							<label for="partialPay">Partial Credit</label>
								</div>
								<div class="partialForm">
									<div class="input-field col l4 m4 s12">
										<input id="amountPartial" name="amountPartial" type="text" class="num" title="only number allowed" required> 
										<label	for="amountPartial">Amount Paid<span class="red-text">*</span></label><br>
									</div>
									<div class="input-field col l4 m4 s12">
										<input id="balAmountPartial" name="balAmountPartial" type="text" class="grey lighten-3 num" readonly title="only number allowed" required> 
										<label	for="balAmountPartial">Balance Amount<span class="red-text">*</span></label><br>
									</div>
									<div class="input-field col l4 m4 s12">
										<input type="date" class="datepicker chequeDate disableDate" placeholder="Due Date" name="chequeDate" id="dueDate"> 
											<label for="dueDate">Due Date <span class="red-text">*</span> </label>
									</div>
								</div>
								<div class="creditDueDate">
									<div class="input-field col l4 m4 s12">
										<input type="date" class="datepicker chequeDate disableDate" placeholder="Due Date" name="chequeDate" id="dueDateCredit"> 
											<label for="dueDateCredit">Due Date <span class="red-text">*</span> </label>
									</div>
								</div>
						</div>	
						
						<div class="paymentTypeSection">
						<div class="row">
								<div class="col s12 m12 l12 divider"></div>
								<br>	
								<div class="col s12 m6 l6">
									<input name="payment" type="radio" id="cash" class="paymentCls" checked />
	      							<label for="cash">Cash</label>
								</div>
								<div class="col s12 m6 l6">
									<input name="payment" type="radio" id="cheque" class="paymentCls" />
	      							<label for="cheque">Cheque</label>
								</div>
								
								<div class="chequeForm">
								<div class="input-field col l4 m4 s12">
									<input id="bankName" name="bankName" type="text" class="" title="only number allowed" required> 
									<label	for="bankName">Bank Name<span class="red-text">*</span></label><br>
								</div>
								<div class="input-field col l4 m4 s12">
									<input id="chqNo" name="chqNo" type="text" class="num" title="only number allowed" maxlength="6" minlength="6" required> 
									<label	for="chqNo">Cheque No.<span class="red-text">*</span></label><br>
								</div>
								<div class="input-field col l4 m4 s12">
									<input type="date" class="datepicker chequeDate disableDate"
										placeholder="Cheque Date" name="chequeDate"
										id="chequeDate"> 
										<label for="chequeDate">Cheque Date <span class="red-text">*</span> </label>
								</div>
								</div>
						</div>	
						</div>
						</div>
					<%-- </c:if> --%>
					<%-- <c:if test="${isEdit==true}">
						<div class="dueDateExtraPayDiv">
								<div class="row">								
									<div class="input-field col l4 m4 s12">
										<input type="date" class="datepicker" placeholder="Due Date" name="chequeDate" id="dueDateExtraPay"> 
											<label for="dueDateExtraPay">Due Date <span class="red-text">*</span> </label>
									</div>
								</div>
						</div>
					</c:if>	 --%>
						<form method="post" action="${pageContext.request.contextPath}/saveCounterOrder" id="saveOrderWithBusines">
						
							<!-- send data -->
							
							<input type="hidden" class="counterOrderId" name="counterOrderId" value="${counterOrder.counterOrderId}">
							<input type="hidden" class="productListCls" name="productList">
							<input type="hidden" class="businessNameIdCls" name="businessNameId">
							<input type="hidden" class="paidAmountCls" name="paidAmount">
							<input type="hidden" class="balAmountCls" name="balAmount">
							<input type="hidden" class="refAmountCls" name="refAmount">
							<input type="hidden" class="paymentSituationCls" name="paymentSituation">
							<input type="hidden" class="dueDateCls" name="dueDate">
							<input type="hidden" class="payTypeCls" name="payType">
							<input type="hidden" class="paymentCls" name="paymentType">
							<input type="hidden" class="bankNameCls" name="bankName">
							<input type="hidden" class="chequeNumberCls" name="chequeNumber">
							<input type="hidden" class="chequeDateCls" name="chequeDate">	
								
							<!-- send Date End -->
							
							
							<div class="row">
							<div class="col l6 m6 s6 center">
									<button type="button" value="PAY" id="payButtonId"
										class="waves-effect waves-light btn btnPay btnEffect" style="width: 115px;">
										Pay <i class="material-icons tiny right btnHover">navigate_next</i>
									</button>
								</div>
								<div class="col l6 m6 s6 center">							 
									<button type="button" value="PAY"  id="payAndPrintButtonId"
										class="waves-effect waves-light btn btnPay btnEffect" style="width: 120px;padding:0px !important;">
										&nbsp; Pay & Print<i class="material-icons tiny right btnHover">navigate_next</i>
									</button>
								</div>
							</div>
						</form>
						<!-- <a class="modal-trigger" href="#custInfo">Modal</a> -->
					</div>
						
						</div>
						</div>
						</div>
				 <form  method="post" action="${pageContext.request.contextPath}/saveCounterOrder" class="col s12 l12 m12" id="saveCustomerInfoWithOrder">		
				<div id="custInfo" class="modal row">
         	       <div class="modal-content">  
         	             
                    <h5 class="center"><u>Customer Info</u></h5>
                     
							<!-- send data -->
							
							<input type="hidden" class="counterOrderId" name="counterOrderId" value="${counterOrder.counterOrderId}">
							<input type="hidden" class="productListCls" name="productList">
							<input type="hidden" class="businessNameIdCls" name="businessNameId">
							<input type="hidden" class="paidAmountCls" name="paidAmount">
							<input type="hidden" class="balAmountCls" name="balAmount">
							<input type="hidden" class="refAmountCls" name="refAmount">
							<input type="hidden" class="paymentSituationCls" name="paymentSituation">
							<input type="hidden" class="dueDateCls" name="dueDate">
							<input type="hidden" class="payTypeCls" name="payType">
							<input type="hidden" class="paymentCls" name="paymentType">
							<input type="hidden" class="bankNameCls" name="bankName">
							<input type="hidden" class="chequeNumberCls" name="chequeNumber">
							<input type="hidden" class="chequeDateCls" name="chequeDate">	
								
							<!-- send Date End -->
							
							
                        <div class="row">
                            <div class="input-field col s12 l8 m8 offset-l2">
                              <input id="custName" type="text" name="custName" value="${counterOrder.customerName}">
                                <label for="custName" class="active black-text"><span class="red-text">*</span>Enter Name</label>
                                
                            </div>
                            <div class="input-field col s12 l8 m8 offset-l2">
                             <input id="mobileNo" type="text" value="${counterOrder.customerMobileNumber}" name="mobileNo" maxlength="10" minlength="10">
                                <label for="mobileNo" class="active black-text"><span class="red-text">*</span>Mobile No</label>
                            </div>
                             <div class="input-field col s12 l8 m8 offset-l2">
                                <input id="gstNo" type="text" name="gstNo" value="${counterOrder.customerGstNumber}"  maxlength="15" minlength="15">
                                <label for="gstNo" class="black-text">GST No.</label>
                            </div>                             
                        </div>
          	  </div>
            <div class="modal-footer">
            <div class="col s12 m12 l12 divider"></div>
            <div class="col s6 m6 l3 offset-l3">
                <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
                </div>
            	<div class="col s6 m6 l3">
                <button id="submitCustomerInfo" type="button" class="modal-action waves-effect  btn">Submit</button>
                </div>
                
            </div>
			
        </div>
		</form>
		<!--end of div 2 for bill and main products-->
	
    </main>
</body>
</html>