<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="resources/js/prettify.js"></script>
<script type="text/javascript" src="resources/js/html2canvas.js"></script>

<script type="text/javascript">
	$(document).ready(function() {

		  $("#printBill").click(function() {  

			//alert(document.getElementById("printdetailbill").value);

			var mywindow = window.open('', 'PRINT', 'height=400,width=600');
			
			mywindow.document.write('<html><head><title> Print </title>');
			mywindow.document.write('</head><body >');
			/* mywindow.document.write('<h1> Print  </h1>'); */
			mywindow.document.write($("#tableData1").html());
			mywindow.document.write('</body></html>');

			mywindow.document.close(); // necessary for IE >= 10
			mywindow.focus(); // necessary for IE >= 10

			mywindow.print();
			mywindow.close();
			return true; 
			//window.location.replace("${pageContext.request.contextPath}/home");
			 /* var modelAttributeValue = '${table}';
			$('#printdetailbill').html(modelAttributeValue);  */

		});  
		  window.prettyPrint();
		  $('#makePdf').click(function(){
             event.preventDefault();
             html2canvas($('#tableData1'), {
                 allowTaint: true,
                 taintTest: false,
                 onrendered: function(canvas) {
                     //document.body.appendChild(canvas);
                     
                     
                     var imgData = canvas.toDataURL("image/PNG",0.3);
                     var pdf = new jsPDF("p", "mm", "a4");                       
                     
                     pdf.setProperties({
                         title: 'Order Invoice',
                         subject: 'Issued Product Invoice',
                         author: 'BlueSquare',
                         keywords: 'generated, javascript, web 2.0, ajax',
                         creator: 'BlueSquare'
                     });
                     
                     var width = pdf.internal.pageSize.width;    
                     var height = pdf.internal.pageSize.height;
                     
                     //pdf.addImage(imgData, 'PNG', 6,1,(width*2)-(width/1),height);
                     pdf.addImage(imgData, 'PNG', 6,1,width,height);
                     var currentDate = new Date();
                     var day = currentDate.getDate();
                     var month = currentDate.getMonth() + 1;
                     var year = currentDate.getFullYear();

                     var d = day + "-" + month + "-" + year;
                     
                     pdf.save('${billPrintDataModel.invoiceNumber}_'+d+'.pdf');  
                     
                       /* var options = {
                             pagesplit: true
                        };
                     var margins = {
                              top: 10,
                              bottom: 10,
                              left: 10,
                              width: 600
                            };
                     /*
                    pdf.addHTML($("#tableData1"),15,15, options, function()
                    {
                        pdf.save("test.pdf");
                    },margins);  
                    
                    var imgWidth = 210; 
                    var pageHeight = 295;  
                    var imgHeight = canvas.height * imgWidth / canvas.width;
                    var heightLeft = imgHeight;

                    var position = 0;

                    pdf.addImage(imgData, 'PNG',5,5, position, imgWidth, imgHeight);
                    heightLeft -= pageHeight;

                    while (heightLeft >= 0) {
                      position = heightLeft - imgHeight-2;
                      pdf.addPage();
                      pdf.addImage(imgData, 'PNG', 5,5, position, imgWidth, imgHeight);
                      heightLeft -= pageHeight;
                    }
                    pdf.save('file.pdf'); */   
                    
                    
				
				 /* var blob = pdf.output('blob');
				var data = new FormData();

				data.append("businessNameId", '${billPrintDataModel.businessName.businessNameId}');
			    data.append("fileName", '${billPrintDataModel.invoiceNumber}_'+d+'.pdf');
			    data.append("files",blob);
			    
                     $.ajax({
                         type: "POST",
                         enctype: 'multipart/form-data',
                         url: "${pageContext.request.contextPath}/sendEmail",
                         data: data,
                         //http://api.jquery.com/jQuery.ajax/
                         //https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
                         processData: false, //prevent jQuery from automatically transforming the data into a query string
                         contentType: false,
                         cache: false,
                         timeout: 600000,
                         success: function (data) {

                             $("#result").text(data);
                             console.log("SUCCESS : ", data);
                             $("#btnSubmit").prop("disabled", false);

                         },
                         error: function (e) {

                             $("#result").text(e.responseText);
                             console.log("ERROR : ", e);
                             $("#btnSubmit").prop("disabled", false);

                         }
                     }); */ 
				
                 }
             }); 	            
		  });
		  
		  $('#sendEmail').click(function(){
			  
			  
			  event.preventDefault();
	             html2canvas($('#tableData1'), {
	                 allowTaint: true,
	                 taintTest: false,
	                 onrendered: function(canvas) {
	                     //document.body.appendChild(canvas);
	                     
	                     
	                     var imgData = canvas.toDataURL("image/PNG",0.3);
	                     var pdf = new jsPDF("p", "mm", "a4");                       
	                     
	                     pdf.setProperties({
	                         title: 'Order Invoice',
	                         subject: 'Issued Product Invoice',
	                         author: 'BlueSquare',
	                         keywords: 'generated, javascript, web 2.0, ajax',
	                         creator: 'BlueSquare'
	                     });
	                     
	                     var width = pdf.internal.pageSize.width;    
	                     var height = pdf.internal.pageSize.height;
	                     
	                     //pdf.addImage(imgData, 'PNG', 6,1,(width*2)-(width/1),height);
	                     pdf.addImage(imgData, 'PNG', 6,1,width,height);
	                     var currentDate = new Date();
	                     var day = currentDate.getDate();
	                     var month = currentDate.getMonth() + 1;
	                     var year = currentDate.getFullYear();

	                     var d = day + "-" + month + "-" + year;
	                     
	                     //pdf.save('${billPrintDataModel.invoiceNumber}_'+d+'.pdf');  
	                     
					 var blob = pdf.output('blob');
					var data = new FormData();

					data.append("businessNameId", '${billPrintDataModel.businessName.businessNameId}');
				    data.append("fileName", '${billPrintDataModel.invoiceNumber}_'+d+'.pdf');
				    data.append("files",blob);
				    
	                     $.ajax({
	                         type: "POST",
	                         enctype: 'multipart/form-data',
	                         url: "${pageContext.request.contextPath}/sendEmail",
	                         data: data,	                         
	                         processData: false, 
	                         contentType: false,
	                         cache: false,
	                         timeout: 600000,
	                         success: function (data) {

	                        	 alert(data);
	                         },
	                         error: function (e) {

	                        	 alert("Something Went Wrong");

	                         }
	                     }); 
					
	                 }
	             });
			  
		  });
	});
	
	 
</script>

</head>
<!-- style="outline: thin solid" -->
<body id="printdetailbill">
<div id="tableData1"  style="background-color:white;">

    <table  id="tableData" border ="1" style="border-collapse: collapse;width:750px;height:800px;">
        <tr><td colspan="8"><center><font size="5"><b>Tax Invoice</b></font></center></td></tr>
        <tr>
            <td colspan="2" rowspan="1">
		            <b>Ramdev Enterprises</b>
		            <br>
		            Shreenath Bhavan,Shop-3<br>
					37/43 Bora Bazar Street,Fort,Mumbai<br>
					Pin-400001<br>
					022-66558189/66105235<br>
					<b>GSTIN/UIN</b> : 27AGUPA0945J1Z6<br>
					<b>E-Mail</b> : ramdevent2009@gmail.com. <br>
		    </td>            
            <td colspan="6">
            	<table border ="1" frame="void" style="width: 100%;height: 100%;border-collapse: collapse;">
	            	<tr>
		                <td style="width: 50%;" colspan="2"><b>Invoice Number</b><br><c:out value="${billPrintDataModel.invoiceNumber}" /></td>
		                <td style="width: 50%;"  colspan="2"><b>Order Book Dated</b><br><c:out value="${billPrintDataModel.orderDate}" /></td>
		            </tr>
	            	<tr>
		                <td style="width: 50%;"  colspan="2"><b>Delivery Note</b><br><br></td>
		                <td style="width: 50%;"  colspan="2"></td>
		            </tr>
		            <tr>
		                <td style="width: 50%;"  colspan="2"><b>Supplier's Ref.</b><br><br></td>
		                <td style="width: 50%;"  colspan="2"><b>Other Reference(s)</b><br><br></td>
		            </tr>
	            </table>	
            </td>
        </tr>


        <tr>
            <td colspan="2" rowspan="1">
            
            <b><u>Buyer</u></b><br>
            <b><c:out value="${billPrintDataModel.businessName.shopName}" /></b><br>
           
            <c:if test="${not empty billPrintDataModel.addressLineList}">
			<c:forEach var="listValue" items="${billPrintDataModel.addressLineList}">
           		 <c:out value="${listValue}" />
            </c:forEach>
            </c:if> 
            <br>
            <c:out value="${billPrintDataModel.businessName.area.region.city.state.name}" />, <b>Code </b> : <c:out value="${billPrintDataModel.businessName.area.region.city.state.code}" /><br>
            <b>Mobile Number</b> : <c:out value="${billPrintDataModel.businessName.contact.mobileNumber}" /><br>
            <b>Tax Type</b> : <c:out value="${billPrintDataModel.businessName.taxType}" /><br>
            <b>GSTIN/UIN</b> : <c:out value="${billPrintDataModel.businessName.gstinNumber}" /><br>
            
            </td>
            <td  colspan="6">
            	<table  border ="1" frame="void" style="width: 100%;height: 100%;border-collapse: collapse;">
            		<tr>
		                <td style="width: 50%;"  colspan="2"><b>Buyer's Order No.</b><br><c:out value="${billPrintDataModel.orderNumber}" /></td>
		                <td style="width: 50%;"  colspan="2"><b>Delivery Date</b><br><c:out value="${billPrintDataModel.deliveryDate}" /></td>
		            </tr>
	            	<tr>
		                <td style="width: 50%;"  colspan="2"><b>Despatch Document No.</b><br><br></td>
		                <td style="width: 50%;"  colspan="2"><b>Delivery Note Date</b><br><br></td>
		            </tr>
		            <tr>
		                <td style="width: 50%;"  colspan="2"><b>Despatched through</b><br><br></td>
		                <td style="width: 50%;"  colspan="2"><b>Destination</b><br><br></td>
		            </tr>
	            </table>	
            </td>
        </tr>

        <tr >
            <th>Sr.No</th>
            <th>Description of Goods</th>
            <th>HSN/SAC</th>
            <th>Quantity</th>
            <th>Rate</th>
            <th>per</th>
            <th>Disc. %</th>
            <th>Amount</th>
        </tr>
        <c:if test="${not empty billPrintDataModel.productListForBill}">
		<c:forEach var="listValue" items="${billPrintDataModel.productListForBill}">
        <tr style='border-bottom-color:white;'>
            <td><c:out value="${listValue.srno}" /></td>
            <td><c:out value="${listValue.productName}" /></td>
            <td><c:out value="${listValue.hsnCode}" /></td>
            <td><b><c:out value="${listValue.quantityIssued}" /></b><b> nos.</b></td>
            <td><c:out value="${listValue.ratePerProduct}" /></td>
            <td>nos.</td>
            <td>0</td>
            <td><c:out value="${listValue.amountWithoutTax}" /></td>
        </tr>
		</c:forEach>
		</c:if>
		
		<tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td> 
            	<hr>
	            <b><c:out value="${billPrintDataModel.totalAmountWithoutTax}" /></b>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
	            <b> Less:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp CGST</b><br>
	            <b> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp SGST</b><br>
	            <b> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp IGST</b><br>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td><br>
	            <b><c:out value="${billPrintDataModel.cGSTAmount}" /></b><br> 
	            <b><c:out value="${billPrintDataModel.sGSTAmount}" /></b><br> 
	            <b><c:out value="${billPrintDataModel.iGSTAmount}" /></b><br>
	            <br>
            </td>
        </tr><!-- 
        <tr>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
            <td> <br><br><br><br> </td>
        </tr> -->
        <tr>
            <td></td>
            <td>Total</td>
            <td></td>
            <td><c:out value="${billPrintDataModel.totalQuantity}" /> nos</td>
            <td></td>
            <td></td>
            <td></td>
            <td> <b>&#8377;</b> <b> <c:out value="${billPrintDataModel.totalAmountWithTax}" /></b></td>
        </tr>
        <tr>
            <td colspan="8">Amount Chargeable(in words) &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                E.&O.E <br><b>INR <c:out value="${billPrintDataModel.totalAmountWithTaxInWord}" /></b> </td>
        </tr>
        <tr>
            <th colspan="1" rowspan="2">HSN/SAC</th>
            <th rowspan="2">Taxable Value</th>
            <th colspan="2">CGST</th>
            <th colspan="2">SGST</th>
            <th colspan="2">IGST</th>
            <tr>
                <td>Rate</td>
                <td>Amount</td>
                <td>Rate</td>
                <td>Amount</td>
                <td>Rate</td>
                <td>Amount</td>
            </tr>
        </tr>

        <c:if test="${not empty billPrintDataModel.categoryWiseAmountForBills}">
		<c:forEach var="listValue" items="${billPrintDataModel.categoryWiseAmountForBills}">
        <tr>
            <td><c:out value="${listValue.hsnCode}" /></td>
            <td><c:out value="${listValue.taxableValue}" /></td>
            <td><c:out value="${listValue.cgstPercentage}" />%</td>
            <td><c:out value="${listValue.cgstRate}" /></td>
            <td><c:out value="${listValue.sgstPercentage}" />%</td>
            <td><c:out value="${listValue.sgstRate}" /></td>
            <td><c:out value="${listValue.igstPercentage}" />%</td>
            <td><c:out value="${listValue.igstRate}" /></td>
        </tr>
        </c:forEach>
        </c:if>
        
        <tr>
            <td colspan="1"> <b> Total</b></td>
            <td><b> <c:out value="${billPrintDataModel.totalAmount}" /></b></td>
            <td> </td>
            <td><b> <c:out value="${billPrintDataModel.totalCGSTAmount}" /> </b></td>
            <td> </td>
            <td><b><c:out value="${billPrintDataModel.totalSGSTAmount}" /></b></td>
            <td> </td>
            <td><b><c:out value="${billPrintDataModel.totalIGSTAmount}" /></b></td>
        </tr>
        <tr>
            <td colspan="8">Tax Amount(in words):<b>INR <c:out value="${billPrintDataModel.taxAmountInWord}" /></b> </td>
        </tr>
        <tr>
            <td colspan="4">Company's PAN : <b>AGUO010152</b> <br>
                <u> Declaration</u> <br> We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct.</td>
            <td colspan="4" align="right"><b>for Ramdev Enterprises</b><br><br><br>Authorised Signatory</td>
        </tr>
        <tr>
            <td colspan="8" align="center">This is Computer Generated Invoice</td>
        </tr>
        <tr>
            <td colspan="8" align="center"><br></td>
        </tr>
    </table>
    </div>
<br>
<button id="printBill">Print</button>
<br>
<div  id="invoiceSend"> </div>
	<button id="makePdf">Pdf</button>
<br>
<button id="sendEmail">Send Mail</button>



</body>

</html>