<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
   <%@include file="components/header_imports.jsp" %>
   <script type="text/javascript" src="resources/js/moment.js"></script>
   <style>
.mapContainer{
    width:50%;
    position: relative;
}
.mapContainer a.direction-link {
    position: absolute;
    top: 15px;
    right: 15px;
    z-index: 100010;
    color: #FFF;
    text-decoration: none;
    font-size: 15px;
    font-weight: bold;
    line-height: 25px;
    padding: 8px 20px 8px 50px;
    background: #0094de;
    background-image: url('direction-icon.png');
    background-position: left center;
    background-repeat: no-repeat;
}
.mapContainer a.direction-link:hover {
    text-decoration: none;
    background: #0072ab;
    color: #FFF;
    background-image: url('resources/img/direction.png');
    background-position: left center;
    background-repeat: no-repeat;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCufJBXdnhqxranPnUypI4Pp-IqYNZyNT0"></script>
  
<script>var myContextPath = "${pageContext.request.contextPath}"</script>
<script type="text/javascript" src="resources/js/currentRoute.js"></script>
    <style>  

	 th,td{
	 	border:1px solid #9e9e9e;
	 	padding:2px;
	 	background-color:white;
	 }
	 .sticky{
position: sticky;
 top: -1px;
 
 
 }
 .wrapper{
 	height:300px;
 	overflow-y:auto;
 }
 .wrapper table{
 	position: relative;
 	width:100%;
 }
 

.tabs .tab a {
    color: #303641;
    display: block;
    width: 100%;
    height: 30px;
    padding: 0 24px;
    font-size: 14px;
    background-color: white !important;
    /* line-height: 0.2em; */
    text-overflow: ellipsis;
    overflow: hidden;
    transition: color .28s ease;
    /* border-radius: 10px; */
    border: 2px solid transparent;
    border-image-slice: 1;
}
	label{
		color:black;
	}
   </style>
   <!-- <style>
   th,td{
   	padding:2px;
   	
   }
     table.outertable th,
     table.outertable td{
    	border:1px solid !important;
    }
    
    table.innerTable th,
    table.innerTable td
    {
    		border:none !important;
    		border-bottom:1px solid !important;
    }
  	table.innerTable tr:last-child td{
  		border-bottom:none !important;
  	}
 	
   </style> -->
</head>
<body>
 <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
        <br> <br>
        <div class="row ">
                 <div class="col s12 m12 l12 hoverable z-depth-3" style="padding:0"> 
            <div id="map-canvas"   style="border:0;width: 100%;height:300px;">
            
            </div>
            
                 <!-- <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3767.1647819147424!2d72.85943331447078!3d19.231648986998827!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7b0d1bc8cd01f%3A0x5fde5207e9aa0924!2sVaisansar+Technologies+PVT.+LTD.!5e0!3m2!1sen!2sin!4v1511759567678" width="100%" height="530" frameborder="0" style="border:0"></iframe> -->
                           
            </div>
         
          <div class="col s12 m12 l12" style="padding:0">
             <br/>
           <div class="col s12 m12 l12 formBg">
             <div class="col s8 l8 m8 offset-l2" style="margin-bottom:20px;">
			      <ul class="tabs">
			        <li class="tab col s6 m6 l6 left-align"><a href="#tblCurrentLocation" class="active">Current Location</a></li>
			        <li class="tab col s6 m6 l6 right-align" style="padding-right:0"><a  href="#tblRoute">Route</a></li>			        
			      </ul>
			 </div>
			  <div class="col l4 m4 s12 offset-l2">
            <label class="active" for="departmentId">Select Department</label>
                 <select id="departmentId">                 
                 <option value="0" selected>Choose Department</option>
                 <c:if test="${not empty departmentList}">
					<c:forEach var="listValue" items="${departmentList}">
                         <option value="${listValue.departmentId}"><c:out value="${listValue.name}" /></option>
                     </c:forEach>
                     </c:if>
                </select>
            </div>
            <div class="col l4 m4 s12">
            <label class="active" for="employeeId">Select Employee</label>
                 <select id="employeeId">
                    	 <option value="0" selected>All</option>
                </select>
            </div>
            </div>
			  <div id="tblCurrentLocation" class="col l12 m12 s12" style="padding:0">
			 
			 <br> 
			 <div class="wrapper">
			  <table class="centered" width="100%">
                 <thead>
                <tr>
                	<th width="60px" class="sticky">Sr.No</th>
                	<th width="300px" class="sticky">Name</th>
                	<th class="sticky">Current Location</th>
                	<th class="sticky">Mobile No.</th>
                	<th width="60px" class="sticky">View</th>
                </tr>
                </thead>
                <tbody id="empCurrentLocation">
               <!--  <tr>
                <td>1</td>
                <td>SAchin</td>
                <td>Western Exp. Highwey</td>
                </tr>     -->    
                
                </tbody>
               </table>
			 </div>
		</div>	
		<div id="tblRoute" class="col l12 m12 s12" style="padding:0">
	 <br>  
		<div class="wrapper">
		
       	  <table class="centered">
         		<thead>
			        <tr>
			            		<th class="sticky" width="3%">Sr.No</th>
			                	<th class="sticky" width="8%">Name</th>
			                	<th width="15%" class="sticky">Mobile No.</th>
			                	<th width="20%" class="sticky">Start</th>
			                	<th width="20%" class="sticky">End</th>
			                	<th width="10%" class="sticky">Task Perform</th>
			                	<th width="10%" class="sticky"><i class="material-icons">location_on</i></th>
			        </tr>
   	 			</thead>

   	 		
   	 		
   	 		
   	 		 	<tbody id="routeListWithTime">
   	 		 	<!-- <tr>
   	 		 		<td>1</td>
					<td>sachin</td>
					<td>
				 		<ol>
							  <li class="liborder">
								  <ul>
									  <li>Malad</li>
									  <li>Malad West</li>
								  </ul>
							  </li>
							  <li class="liborder">
								  <ul>
									  <li>Tea 1</li>
									  <li>Tea 2</li>
								  </ul>
							  </li>
						</ol>     		
                	</td>
                	<td>
                		<ul>
							  <li class="liborder">
								  <ul>
									  <li>9:30</li>
									  <li>9:53</li>
								  </ul>
							  </li>
							  <li class="liborder">
								  <ul>
									  <li>Tea 1</li>
									  <li>Tea 2</li>
								  </ul>
							  </li>
						</ul>
							      		
   					</td>
         			<td>
		               <ul>		               	
			               	<li>
			               		<ul>
			               			<li class="liborder"> <button class="btn btn-flat"><i class="material-icons">visibility</i></button></li>
			               			<li class="liborder"> <button class="btn btn-flat"><i class="material-icons">visibility</i></button></li>
			               		</ul>
			               	</li>
		               </ul>
        		    </td>
                					
   	 		 		</tr> -->
   	 		 	</tbody>
   	 		 	</table>
   	 		 </div>
   	
     
 </div> 
			<!--  <div id="tblCurrentLocation" class="col l12 m12 s12">
			 <br> 
			  <table class="outertable centered highlight striped" width="100%">
                 <thead>
                <tr>
                	<th width="3%">Sr.No</th>
                	<th>Name</th>
                	<th>Current Location</th>
                </tr>
                </thead>
                <tbody id="empCurrentLocation">
                <tr>
                	<td>1</td>
                	<td>Sachin</td>
                	<td>western Exp.highway</td>
                </tr>
                </tbody>
               </table>
			 </div> -->
  			 <!--  <div id="tblRoute" class="col s12">
  			   <br> 
               	<table class="outertable centered highlight striped" width="100%">
                 <thead>
                <tr>
                	<th width="3%">Sr.No</th>
                	<th>Name</th>
                	<th>Current Route</th>
                	<th>Time</th>
                	<th width="3%">View</th>
                </tr>
                </thead>
                <tbody>
                <td>1</td>
                <td>sachin</td>
                <td>
	                <table class="innerTable">
	                
		                <tr>
			                <td>1</td>
			                <td>Start:<span>Malad</span><br>
			                	End:<span>Borivali</span>
			                </td>               
		                </tr>
		                <tr>
			                <td>2</td>
			                 <td>Start:<span>Malad</span><br>
			                	End:<span>Borivali</span>
			                </td>		                
		                </tr>
	                </table>
                </td>
               
                <td>                
		             <table class="innerTable">
		             
			                <tr>			              
				                 <td>9.30<br>
				                 4.30				                	
				                </td>			                
			                </tr>
			                <tr>		              
				                 <td>9.30<br>
				                 4.30				                	
				                </td> 			                
			                </tr>
		              </table>
                </td>
                <td>
                <table class="innerTable">
			                <tr>			              
				                 <td> <button class="btn btn-flat"><i class="material-icons">visibility</i></button>			                	
				                </td>			                
			                </tr>
			                <tr>		              
				                 <td> <button class="btn btn-flat"><i class="material-icons">visibility</i></button>			                	
				                </td> 			                
			                </tr>
		              </table>
               </td>
                </tbody>
                </table>
                
            </div>-->
            
         </div> 
            
         

        </div> <br>
     <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:20%;">
					<div class="modal-content" style="padding:0">
					<div class="center  white-text" id="modalType" style="padding:3% 0 3% 0"></div>
					<!-- <h5 id="msgHead" class="red-text"></h5> -->
						<h6 id="msg" class="center"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!" class="modal-action modal-close waves-effect  btn">OK</a>
					</div>
				</div>
			</div>
		</div>
     <!-- <div class="row">
			<div class="col s12 m12 l12">
				<div id="addeditmsg" class="modal" style="width:40%;">
					<div class="modal-content">
						<h5 id="msgHead"></h5>
						<hr>
						<h6 id="msg"></h6>
					</div>
					<div class="col s8 m2 l2 offset-l5 modal-footer">
						<a href="#!"
							class="modal-action modal-close waves-effect waves-green btn blue-gradient">OK</a>
					</div>
				</div>
			</div>
		</div> -->
   
    </main>

</body>
</html>