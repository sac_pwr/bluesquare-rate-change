<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
  <%@include file="components/header_imports.jsp" %>
    <script type="text/javascript">
	    function showTaxCalculation(productId)
	    {
	
	    	$.ajax({
	    		url :"${pageContext.request.contextPath}/fetchProductTaxcalculation?productId="+productId,
	    		dataType : "json",
	    		success : function(data) {
	    			
	    			$('#cgstPerId').text(data.cgstPercentage);
	    			$('#cgstAmtId').text(data.cgstRate);
	    			$('#sgstPerId').text(data.sgstPercentage);
	    			$('#sgstAmtId').text(data.sgstRate);
	    			$('#igstPerId').text(data.igstPercentage);
	    			$('#igstAmtId').text(data.igstRate);
	    			$('#taxAmountId').text(data.totalTaxAmount.toFixed(2));
	    			$('.modal').modal();
	    			$('#tax').modal('open');
	    		},
	    		error: function(xhr, status, error) {
	    			  //var err = eval("(" + xhr.responseText + ")");
	    			  alert("Error");
	    			}
	    	});
	    	
	    }
	    $(document).ready(function() {
	    	var table = $('#tblData').DataTable();
	   		 table.destroy();
	   		 $('#tblData').DataTable({
	   	         "oLanguage": {
	   	             "sLengthMenu": "Show _MENU_",
	   	             "sSearch": "_INPUT_" //search
	   	         },
	   	      	autoWidth: false,
	   	         columnDefs: [
	   	                      { 'width': '1%', 'targets': 0 },
	   	                      { 'width': '8%', 'targets': 4},
	   	                   { 'width': '1%', 'targets': 5},
	   	                { 'width': '1%', 'targets': 6},
	   	             { 'width': '8%', 'targets': 7}
	   	                     
	   	                      ],
	   	         lengthMenu: [
	   	             [50, 75., 100, -1],
	   	             ['50 ', '75 ', '100 ', 'All']
	   	         ],
	   	         
	   	        
	   	         //dom: 'lBfrtip',
	   	         dom:'<lBfr<"scrollDivTable"t>ip>',
	   	         buttons: {
	   	             buttons: [
	   	                 //      {
	   	                 //      extend: 'pageLength',
	   	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
	   	                 //  }, 
	   	                 {
	   	                     extend: 'pdf',
	   	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                     customize: function(doc, config) {
	   	                    	doc.content.forEach(function(item) {
		                    		  if (item.table) {
		                    		  item.table.widths = [30,'*','*','*',40,30,30,50] 
		                    		 } 
		                    		    })
	   	        /*                  var tableNode;
	   	                         for (i = 0; i < doc.content.length; ++i) {
	   	                           if(doc.content[i].table !== undefined){
	   	                             tableNode = doc.content[i];
	   	                             break;
	   	                           }
	   	                         }
	   	        
	   	                         var rowIndex = 0;
	   	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
	   	                          
	   	                         if(tableColumnCount > 6){
	   	                           doc.pageOrientation = 'landscape';
	   	                         } */
	   	                         /*for customize the pdf content*/ 
	   	                         doc.pageMargins = [5,20,10,5];   	                         
	   	                         doc.defaultStyle.fontSize = 8	;
	   	                         doc.styles.title.fontSize = 12;
	   	                         doc.styles.tableHeader.fontSize = 11;
	   	                         doc.styles.tableFooter.fontSize = 11;
	   	                      doc.styles.tableHeader.alignment = 'center';
		                         doc.styles.tableBodyEven.alignment = 'center';
		                         doc.styles.tableBodyOdd.alignment = 'center';
	   	                       },
	   	                 },
	   	                 {
	   	                     extend: 'excel',
	   	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'print',
	   	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
	   	                     //title of the page
	   	                     title: function() {
	   	                         var name = $(".heading").text();
	   	                         return name
	   	                     },
	   	                     //file name 
	   	                     filename: function() {
	   	                         var d = new Date();
	   	                         var date = d.getDate();
	   	                         var month = d.getMonth();
	   	                         var year = d.getFullYear();
	   	                         var name = $(".heading").text();
	   	                         return name + date + '-' + month + '-' + year;
	   	                     },
	   	                     //  exports only dataColumn
	   	                     exportOptions: {
	   	                         columns: ':visible.print-col'
	   	                     },
	   	                 },
	   	                 {
	   	                     extend: 'colvis',
	   	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
	   	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
	   	                     collectionLayout: 'fixed two-column',
	   	                     align: 'left'
	   	                 },
	   	             ]
	   	         }

	   	     });
	   		 $("select")
	             .change(function() {
	                 var t = this;
	                 var content = $(this).siblings('ul').detach();
	                 setTimeout(function() {
	                     $(t).parent().append(content);
	                     $("select").material_select();
	                 }, 200);
	             });
	         $('select').material_select();
	         $('.dataTables_filter input').attr("placeholder", "Search");
           //$(".showQuantity").hide();
            $(".showDates").hide();
            $("#oneDateDiv").hide();
           /*  $(".topProduct").click(function() {
                $(".showQuantity").show();
                $(".showDates").hide();
                $("#oneDateDiv").hide();
            }); */
           
            $(".rangeSelect").click(function() {
           
                $(".showDates").show();
              //  $(".showQuantity").hide();
                $("#oneDateDiv").hide();
            });
            $(".pickdate").click(function(){
            	//$(".showQuantity").hide();
   	   		 $(".showDates").hide();
   	   		$("#oneDateDiv").show();
   	   	});
            $('#topProductId').change(function(){
            
            	window.location.href = '${pageContext.servletContext.contextPath}/fetchProductListForReport?range=TopProducts&topProductNo='+$('#topProductId').val();
            	
            });
           
        });

    </script>
     <style>
     .card{
     height: 2.5rem;
     line-height:2.5rem;
    /*  text-align:center !important; */
     }
    .card-image{
        	width:35% !important;
        	background-color:#0073b7 !important;
        }
        .card-image h6{
		padding:5px;		
	}
	.card-stacked .card-content{
	 padding:5px;
	}

   .input-field {
    position: relative;
    margin-top: 0.5rem;
}
#oneDateDiv{
	margin-top: 0.5rem;
	margin-left:4%
}
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main  class="paddingBody">
        <br>
        <div class="row">
                 
         <div class="col s12 l4 m4 left">
      		  <div class="card horizontal">
      		  	<div class="card-image">
       				  <h6 class="white-text center">Total Amount</h6>
      			</div>
      			<div class="card-stacked grey lighten-3">
      			  <div class="card-content">
       			<h6 class="">&#8377;<span id="totalAmountId"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTax}" /></span></h6>
       			  </div>
        	    </div>
                
          	  </div>
           </div>  
           <div class="col s12 m3 l3 right right-align" style="margin-top:0.6%;width:15%;">
           
           
           
           
            <!-- <div class="col s6 m7 l7 right right-align" style="margin-top:3%;"> -->
                    <!-- Dropdown Trigger -->
                    <a class='dropdown-button btn waves-effect waves-light' href='#' data-activates='filter'>Action<i
                class="material-icons right">arrow_drop_down</i></a>
                    <!-- Dropdown Structure -->
                    <ul id='filter' class='dropdown-content'>
                     <!-- <li><button class=" btn-flat topProduct teal-text">Top Sold Product</button></li> -->
                     <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=today">Today</a></li>
                    <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=yesterday">Yesterday</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=last7days">Last 7 days</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=currentMonth">Current Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=lastMonth">Last Month</a></li>
                        <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=last3Months">Last 3 Months</a></li>
                        <li><a class="rangeSelect">Range</a></li>
                        <li><a class="pickdate">Pick date</a></li> 
                        <li><a href="${pageContext.servletContext.contextPath}/fetchProductListForReport?range=viewAll">View All</a></li>
                    </ul>
              
               </div>
                 <div class="col s6 m2 l2 right right-align">                   
                         <select id="topProductId" class="btnSelect">
                         <option value="0"  selected>Top Sold Product</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                           <option value="15">15</option>
                        </select>                        
           </div>   
               <div class="input-field col s12 l3 m3" id="oneDateDiv">                            	
                    <form action="${pageContext.request.contextPath}/fetchProductListForReport" method="post">
	                    <div class="input-field col s12 m2 l7">
		                    <input type="text" id="oneDate" class="datepicker" placeholder="Choose date" name="startDate">
		                    <label for="oneDate" class="black-text">Pick Date</label>
	                    </div>
	                    <div class="input-field col s12 m2 l2">
	                     <button type="submit">View</button>
	                    <input type="hidden" value="0" name="areaId">
                        <input type="hidden" value=pickDate name="range">
                       
                        </div>
                    </form>
               </div>
                <div class="input-field col s12 l4 m4 right" style="margin-top:0.5rem;">      
           		 <form action="${pageContext.servletContext.contextPath}/fetchProductListForReport" method="post">
                    <input type="hidden" name="range" value="range"> 
                    	  <span class="showDates">
                              <div class="input-field col s6 m2 l5">
                                <input type="date" class="datepicker" placeholder="Choose Date" name="startDate" id="startDate" required> 
                                <label for="startDate">From</label>
                              </div>
                              <div class="input-field col s6 m2 l5">
                                    <input type="date" class="datepicker" placeholder="Choose Date" name="endDate" id="endDate">
                                     <label for="endDate">To</label>
                               </div>
                               <div class="input-field col s6 m2 l2">
                            <button type="submit">View</button>
                            </div>
                          </span>
                </form>                
         </div>
                 
        
        
          <%-- <div class="col s12 m5 l5 right">
       			 <h5 class="center red-text">Total Amount : <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTax}" /></h5>
		</div> --%>
<!-- </div>

        <div class="row"> -->
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered " id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>
                            <th class="print-col">Product</th>
                            <th class="print-col">Category</th>
                            <th class="print-col">Brand</th>
                            <th class="print-col">Qty  Sold</th>
                            <th class="print-col">Qty Damage</th>
                            <th class="print-col">Qty Free</th>
                            <th class="print-col">Amount</th>
                        </tr>
                    </thead>

                    <tbody>
                    
                    <c:if test="${not empty productReportViews}">
					<c:forEach var="listValue" items="${productReportViews}">
                        <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                            <td><c:out value="${listValue.product.productName}" /></td>
                            <td><c:out value="${listValue.product.categories.categoryName}" /></td>
                            <td><c:out value="${listValue.product.brand.name}" /></td>
                            <td><c:out value="${listValue.issuedQuantity}" /></td>
                            <td><c:out value="${listValue.damageQuantity}" /></td>
                            <td><c:out value="${listValue.freeQuantity}" /></td>
                           <%--  <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.rateWithTax}" /></td> --%>
                            <td class="wrapok"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.amountWithTax}" /></td>
                            <%-- <button class="btn-flat blue-text" onclick="showTaxCalculation(${listValue.product.productId})"> --%>
                            <!-- </button> -->
                           
                        </tr>
                     </c:forEach>
                     </c:if>
                     
                    </tbody>
                </table>
            </div>
        </div>

 <!-- Modal Structure for Tax Details -->
        <div id="tax" class="modal">
            <div class="modal-content">
                <h5 class="center"><u>Tax Details</u></h5>
                
                <br>
                <table class="centered tblborder" border="2">
					<thead>
                    <tr>
                        <th colspan="2">%CGST</th>                        
                        <th colspan="2">%SGST</th>
                        <th colspan="2">%IGST</th>
                        <th rowspan="2">Total tax Amount</th>
                    </tr>
                    <tr>
                        <th>%</th>
                        <th>Amount</th>
                        <th>%</th>
                        <th>Amount</th>
                        <th>%</th>
                        <th>Amount</th>

                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td id="cgstPerId"></td>
                            <td id="cgstAmtId"></td>
                            <td id="sgstPerId"></td>
                            <td id="sgstAmtId"></td>
                            <td id="igstPerId"></td>
                            <td id="igstAmtId"></td>
                            <td id="taxAmountId"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">

                <a href="#!" class="modal-action modal-close waves-effect btn red">Close</a>
            </div>
        </div>

    </main>
    <!--content end-->
</body>

</html>