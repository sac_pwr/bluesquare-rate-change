<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
        <%@include file="components/header_imports.jsp" %>

    <script type="text/javascript">
    
    function viewProducts(url)
	   {
		   $.ajax({
				url : url,
				dataType : "json",
				success : function(data) {
				//alert(data);
				    $("#tbproductlist").empty();
					var srno=1;
					for (var i = 0, len = data.length; i < len; ++i) {
						var supplierproduct = data[i];
						$("#tbproductlist").append("<tr>"+
	                           "<td>"+srno+"</td>"+
	                           "<td>"+supplierproduct.product.productName+"</td>"+
	                           "<td>"+supplierproduct.product.categories.categoryName+"</td>"+
	                           "<td>"+supplierproduct.product.brand.name+"</td>"+
	                           "<td>"+supplierproduct.supplierRate+"</td>"+
	                       "</tr>"); 
						srno++;
					}
						    	
					
					$('.modal').modal();
					$('#viewDetails').modal('open');
					//alert("data came");
					return false;
					/* for (index = 0; index < options.length; ++index) {
					  option = options[index];
					  select.options.add(new Option(option.name, option.cityId));
					} */
				},
				error: function(xhr, status, error) {
					  //var err = eval("(" + xhr.responseText + ")");
					  //alert(error +"---"+ xhr+"---"+status);
					  	 $('#addeditmsg').modal('open');
					     $('#msgHead').text("Manage Supplier Message");
					     $('#msg').text("Not Have Product List");
					}
			});
	   }
    $(document).ready(function(){
    	var table = $('#tblData').DataTable();
  		 table.destroy();
  		 $('#tblData').DataTable({
  	         "oLanguage": {
  	             "sLengthMenu": "Show _MENU_",
  	             "sSearch": "_INPUT_" //search
  	         },
  	      	autoWidth: false,
  	         columnDefs: [
  	                      { 'width': '1%', 'targets': 0 },
  	                      { 'width': '10%', 'targets': 1},
  	                      { 'width': '10%', 'targets': 2},
  	                      { 'width': '10%', 'targets': 3},
  	                    { 'width': '3%', 'targets': 4},
  	                  	  { 'width': '3%', 'targets': 5},
  	                  	  { 'width': '3%', 'targets': 6},
  	                  	  { 'width': '3%', 'targets': 7}
  	                     
  	                      ],
  	         lengthMenu: [
  	             [10, 25., 50, -1],
  	             ['10 ', '25 ', '50 ', 'All']
  	         ],
  	         
  	       dom:'<lBfr<"scrollDivTable"t>ip>',
  	         //dom: 'lBfrtip',
  	         buttons: {
  	             buttons: [
  	                 //      {
  	                 //      extend: 'pageLength',
  	                 //      className: 'pageLengthButton waves-effect waves-light   white-text blue-grey lighten-1'
  	                 //  }, 
  	                 {
  	                     extend: 'pdf',
  	                     className: 'pdfButton waves-effect waves-light  grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-pdf-o"></i> &nbsp;<span style="font-size:15px;">PDF<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                     customize: function(doc, config) {
  	                    	doc.content.forEach(function(item) {
	                    		  if (item.table) {
	                    		  item.table.widths = [20,'*','*','*',40,40,60,60] 
	                    		 } 
	                    		    }) 
  	                        /*  var tableNode;
  	                         for (i = 0; i < doc.content.length; ++i) {
  	                           if(doc.content[i].table !== undefined){
  	                             tableNode = doc.content[i];
  	                             break;
  	                           }
  	                         }
  	        
  	                         var rowIndex = 0;
  	                         var tableColumnCount = tableNode.table.body[rowIndex].length;
  	                          
  	                         if(tableColumnCount > 6){
  	                           doc.pageOrientation = 'landscape';
  	                         } */
  	                         /*for customize the pdf content*/ 
  	                         doc.pageMargins = [5,20,10,5];   	                         
  	                         doc.defaultStyle.fontSize = 8	;
  	                         doc.styles.title.fontSize = 12;
  	                         doc.styles.tableHeader.fontSize = 11;
  	                         doc.styles.tableFooter.fontSize = 11;
  	                       doc.styles.tableHeader.alignment = 'center';
	                         doc.styles.tableBodyEven.alignment = 'center';
	                         doc.styles.tableBodyOdd.alignment = 'center';
  	                       },
  	                 },
  	                 {
  	                     extend: 'excel',
  	                     className: 'excelButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-file-excel-o  fa-fw"></i> &nbsp;<span style="font-size:15px;">EXCEL<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'print',
  	                     className: 'printButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<i class="fa fa-print fa-fw"></i> &nbsp;<span style="font-size:15px;">PRINT<span>',
  	                     //title of the page
  	                     title: function() {
  	                         var name = $(".heading").text();
  	                         return name
  	                     },
  	                     //file name 
  	                     filename: function() {
  	                         var d = new Date();
  	                         var date = d.getDate();
  	                         var month = d.getMonth();
  	                         var year = d.getFullYear();
  	                         var name = $(".heading").text();
  	                         return name + date + '-' + month + '-' + year;
  	                     },
  	                     //  exports only dataColumn
  	                     exportOptions: {
  	                         columns: ':visible.print-col'
  	                     },
  	                 },
  	                 {
  	                     extend: 'colvis',
  	                     className: 'colvisButton waves-effect waves-light grey lighten-3 light-blue-text text-darken-4 z-depth-2',
  	                     text: '<span style="font-size:15px;">COLUMN VISISBILITY<span>&nbsp;&nbsp;<i class="fa fa-caret-down fa-fw"></i> ',
  	                     collectionLayout: 'fixed two-column',
  	                     align: 'left'
  	                 },
  	             ]
  	         }

  	     });
  		 $("select")
            .change(function() {
                var t = this;
                var content = $(this).siblings('ul').detach();
                setTimeout(function() {
                    $(t).parent().append(content);
                    $("select").material_select();
                }, 200);
            });
        $('select').material_select();
        $('.dataTables_filter input').attr("placeholder", "Search");
        });
    </script>
    
    <style>
        
         .card-panel p{
        	font-size:15px !important;
        }
    </style>
</head>

<body>
    <!--navbar start-->
   	<%@include file="components/navbar.jsp" %>
    <!--navbar end-->
    <!--content start-->
    <main class="paddingBody">
       
       
        <div class="row">
         <br> <br>
            <div class="col s12 l12 m12">
                <table class="striped highlight bordered centered" id="tblData" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th class="print-col">Sr. No.</th>
                          <!--   <th class="print-col">Sales Person Name</th> -->
                            <th class="print-col">Category Name</th>
                            <th class="print-col">Brand Name</th>
                            <th class="print-col">Product Name</th>
                            <th class="print-col">MRP</th>
                            <th class="print-col">Qty</th>
                            <th class="print-col">Taxable Amount</th>
                            <th class="print-col">Amount With Tax</th>
                            <!-- <th class="print-col">Order Date</th> -->
                        </tr>
                    </thead>

                    <tbody>
                     <c:if test="${not empty counterOrderProductDetailListForWebApps}">
							<c:forEach var="listValue" items="${counterOrderProductDetailListForWebApps}">
                         <tr>
                            <td><c:out value="${listValue.srno}" /></td>
                             <%-- <td><a title="View Sales person Details" href="getEmployeeView?employeeDetailsId=${listValue.employeeId}"><c:out value="${listValue.salesPersonName}" /></a></td><!-- ManageSupplier.html --> --%>
                            <td class="wrapok"><c:out value="${listValue.product.categories.categoryName}" /></td>
                            <td class="wrapok"><c:out value="${listValue.product.brand.name}" /></td>
                            <td class="wrapok"><c:out value="${listValue.product.productName}" /><font color="green"><c:out value="${listValue.type=='Free'? '-(Free)':''}" /></font></td>
                            <td><c:out value="${listValue.rateWithTax}" /></td>
                            <td><c:out value="${listValue.quantity}" /></td>     
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmount}" /></td>
                            <td><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${listValue.totalAmountWithTax}" /></td>
                              <%--<td>
                           <fmt:formatDate pattern = "dd-MM-yyyy" var="date"   value = "${listValue.orderDate}"  /> 
		                    <c:out value="${date}" />
                            </td>--%>
                        </tr>
					</c:forEach>
					</c:if>
                    </tbody>
                     <tfoot class="centered">
            		<tr>
             			  <th class="center" colspan="5">Total</th>
                		  <th><center><c:out value="${totalQuantity}" /></center></th>
                		  <th><center><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmount}" /></center></th>
                		  <th><center><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAmountWithTax}" /></center></th>
                		  <!-- <th></th> -->
           			</tr>
        			</tfoot>
                </table> 
            </div>
        </div>

        <!-- Modal Structure for View Product Details -->
        <div id="product" class="modal ">
            <div class="modal-content">
                <h5 class="center"><u>Product Details</u></h5>
                
                <br>
                <table border="2" class="centered tblborder">
                    <thead>
                        <tr>
                            <th>Sr.No</th>
                            <th>Product Name</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Quantity</th>
                            <th>Rate</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Mouse</td>
                            <td>I.T</td>
                            <td>BlueSquare</td>
                            <td>5</td>
                            <td>350</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer row">

                <div class="col s12 m6 l6 offset-l1">
                    <a href="#!" class="modal-action modal-close waves-effect btn">Close</a>
                </div>

            </div>
        </div>

    </main>
    <!--content end-->
</body>

</html>