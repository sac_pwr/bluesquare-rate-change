package com.bluesquare.rc.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.FetchSupplierList;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.Constants;

@Controller
public class ManageSupplierController {

	@Autowired
	SupplierService supplierService;
	
	@Autowired
	CategoriesService categoriesService;

	@Autowired
	BrandService brandService;
	
	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	Product product;
	
	@Autowired
	Brand brand;
	
	@Autowired
	Categories categories;
	
	@Autowired
	Contact contact;
	
	@Autowired
	PaymentPaySupplier paymentPaySupplier;
	
	@Autowired
	Inventory inventory;
	
	@Autowired
	Supplier supplier;
	
	@Autowired
	AreaService areaService;
	
	
	@Transactional 	@RequestMapping("/addSupplier")
	public ModelAndView manageSupplier(Model model,HttpSession session) {
		System.out.println("in addSupplier controller");
		
		model.addAttribute("pageName", "Add Supplier");
		
				

		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		List<Product> productList=productService.fetchProductListForWebApp();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		model.addAttribute("productlist", productList);
		
		return new ModelAndView("addSupplier");
	}
	
	@Transactional 	@RequestMapping("/checkSupplierDuplicationForSave")
	public @ResponseBody String checkSupplierDuplicationForSave(Model model,HttpServletRequest request,HttpSession session)
	{
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return supplierService.checkSupplierDuplication(checkText, type, "0");
	}
	
	@Transactional 	@RequestMapping("/checkSupplierDuplicationForUpdate")
	public @ResponseBody String checkSupplierDuplicationForUpdate(Model model,HttpServletRequest request,HttpSession session)
	{
		String supplierId=request.getParameter("supplierId");
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return supplierService.checkSupplierDuplication(checkText, type, supplierId);
	}
	
	@Transactional 	@RequestMapping("/saveSupplier")
	public ModelAndView saveSupplier(@ModelAttribute Supplier supplier ,Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in saveSupplier controller");
		
		
		
		String productIdList=request.getParameter("productIdList");
		
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobileNumber"));
		
		supplier.setContact(contact);
		
		supplier.setSupplierAddedDatetime(new Date());
		supplier.setSupplierUpdatedDatetime(new Date());
		supplierService.saveSupplier(supplier);
		
		supplierService.saveSupplierProductList(productIdList, supplier.getSupplierId());
		
		session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
		return new ModelAndView("redirect:/fetchSupplierList");
	}
	
	@Transactional 	@RequestMapping("/fetchProductListByBrandIdAndCategoryId")
	public @ResponseBody List<Product> fetchProductListByBrandIdAndCategoryId(HttpServletRequest request)
	{
		long categoryId=Long.parseLong(request.getParameter("categoryId"));
		long brandId=Long.parseLong(request.getParameter("brandId"));
		
		List<Product> productList=productService.fetchProductListByBrandIdAndCategoryIdForWebApp(categoryId,brandId);
		/*List<Product> productList2=null;
		if(productList != null)
		{
			productList2=productService.makeProductImageNull(productList);
		}	*/	
		return productList;
	}
	
	
	@Transactional 	@RequestMapping("/fetchSupplierList")
	public ModelAndView fetchSupplierList(Model model,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		model.addAttribute("pageName", "Supplier List");
		
		List<FetchSupplierList> fetchSupplierList=supplierService.fetchSupplierForWebApp();
		
		model.addAttribute("fetchSupplierList", fetchSupplierList);
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("ManageSupplier");
	}
	
	@Transactional 	@RequestMapping("/fetchSupplierListBySupplierId")
	public ModelAndView fetchSupplierListBySupplierId(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		model.addAttribute("pageName", "Supplier List");
		
		List<FetchSupplierList> fetchSupplierList=new ArrayList<>();
		supplier=supplierService.fetchSupplier(request.getParameter("supplierId"));
		fetchSupplierList.add(new FetchSupplierList(supplier.getSupplierId(), 1, supplier.getName(), supplier.getContact().getEmailId(), supplier.getContact().getMobileNumber(), supplier.getAddress(), supplier.getGstinNo(), supplier.getSupplierAddedDatetime(),supplier.getSupplierUpdatedDatetime()));
		
		model.addAttribute("fetchSupplierList", fetchSupplierList);
		
		return new ModelAndView("ManageSupplier");
	}
	
	@Transactional 	@RequestMapping("/fetchSupplierProductList")
	public @ResponseBody List<SupplierProductList> fetchSupplierProductList(Model model,HttpServletRequest request) {
		System.out.println("in fetchSupplierProductList controller");
		
		String supplierId=request.getParameter("supplierId");
		List<SupplierProductList> fetchSupplierProductList=supplierService.fetchSupplierProductListBySupplierIdForWebApp(supplierId);
		/*if(fetchSupplierProductList==null)
		{
			return null;
		}
		List<SupplierProductList> SupplierProductList=supplierService.makeSupplierProductListNullForWebApp(fetchSupplierProductList);*/
		
		return fetchSupplierProductList;
	}
	
	//fechSupplier
	@Transactional 	@RequestMapping("/fechSupplier")
	public ModelAndView fechSupplier(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in fetchSupplierList controller");
		
		
		  model.addAttribute("pageName", "Update Supplier");
		
		String supplierId=request.getParameter("supplierId");
		Supplier supplier=supplierService.fetchSupplier(supplierId);		
		
		model.addAttribute("supplier", supplier);
		List<SupplierProductList> fetchSupplierProductList=supplierService.fetchSupplierProductListBySupplierIdForWebApp(supplierId);

		String productidlist="";
		model.addAttribute("count","");
		
		if(fetchSupplierProductList!=null)
		{
			model.addAttribute("count",fetchSupplierProductList.size()+1);
		}
		
		model.addAttribute("supplierProductList", fetchSupplierProductList);	
		
		productidlist=supplierService.getProductIdList(fetchSupplierProductList);
		model.addAttribute("productidlist", productidlist);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);		
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		List<Product> productList=productService.fetchProductListForWebApp();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		model.addAttribute("productlist", productList);
		
		return new ModelAndView("updateSupplier");
	}
	
	@Transactional 	@RequestMapping("/updateSupplier")
	public ModelAndView updateSupplier(@ModelAttribute Supplier supplier ,Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("in updateSupplier controller");
		
		
		
		String supplierId=request.getParameter("supplierId");
		Supplier supler=supplierService.fetchSupplier(supplierId);
		supplier.setSupplierPKId(supler.getSupplierPKId());
		String productIdList=request.getParameter("productIdList"); 
		
		contact.setContactId(supler.getContact().getContactId());
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobileNumber"));
		supplier.setContact(contact);
		
		supplier.setSupplierAddedDatetime(supler.getSupplierAddedDatetime());
		supplier.setSupplierUpdatedDatetime(new Date());
		supplierService.updateSupplier(supplier);
		
		supplierService.updateSupplierProductList(productIdList, supplier.getSupplierId());
		
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchSupplierList");
	}
	
	@Transactional 	@RequestMapping("/sendSMSTOSupplier")
	public @ResponseBody String sendSMSTOSupplier(HttpServletRequest request) {
		
		try {
			String supplierIds=request.getParameter("supplierIds");		
			String smsText=request.getParameter("smsText");	
			String mobileNumber=request.getParameter("mobileNumber");	
			supplierService.sendSMSTOSupplier(supplierIds, smsText, mobileNumber);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
	}
	
	@Transactional 	@RequestMapping("/paymentSupplier")
	public  ModelAndView paymentEmployee(Model model,HttpServletRequest request,HttpSession session) {
		
		model.addAttribute("pageName", "Make Supplier Payment");
		
		
		String inventoryTransactionId=request.getParameter("inventoryTransactionId");
				
		PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentStatus(inventoryTransactionId);
		
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		
		return new ModelAndView("makePayment");
	}

	@Transactional 	@RequestMapping("/giveSupplierOrderPayment")
	public ModelAndView giveSupplierOrderPayment(HttpServletRequest request,HttpSession session,Model model) {
		
		
		
					
			//String employeeDetailsId=request.getParameter("employeeDetailsId");
			String inventoryId=request.getParameter("inventoryId");
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			//String comment=request.getParameter("comment");
			
			inventory=inventoryService.fetchInventory(inventoryId);			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentStatus(inventoryId);
			
			
			
			
			if(bankName.equals(""))
			{
				paymentPaySupplier.setChequeDate(null);
				bankName=null;
				cheqNo=null;
				paymentPaySupplier.setPayType(Constants.CASH_PAY_STATUS);
			}
			else
			{
				try {
					//paymentPaySupplier.setChequeDate(dateFormat.parse(cheqDate));
					paymentPaySupplier.setChequeDate(new Date());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				paymentPaySupplier.setPayType(Constants.CHEQUE_PAY_STATUS);
			}
			
			paymentPaySupplier.setBankName(bankName);
			paymentPaySupplier.setChequeNumber(cheqNo);
			paymentPaySupplier.setDueAmount(paymentDoInfo.getAmountUnPaid()-amount);
			paymentPaySupplier.setDueDate(new Date());
			paymentPaySupplier.setPaidAmount(amount);
			paymentPaySupplier.setPaidDate(new Date());			
			
			if(paymentPaySupplier.getDueAmount()==0)
			{
				inventory.setPayStatus(true);
			}
			else
			{
				inventory.setPayStatus(false);
			}
			
			paymentPaySupplier.setInventory(inventory);
			inventoryService.givePayment(paymentPaySupplier);			
		
			session.setAttribute("saveMsg", "Payment Done SuccessFully");
		return new ModelAndView("redirect:/"+session.getAttribute("lastUrl"));
	}
	
	@Transactional 	@RequestMapping("/updatePaymentSupplier")
	public ModelAndView updatePaymentSupplier(HttpServletRequest request,HttpSession session,Model model) {
		
			//String employeeDetailsId=request.getParameter("employeeDetailsId");
			String inventoryId=request.getParameter("inventoryId");
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			//String comment=request.getParameter("comment");
			
			inventory=inventoryService.fetchInventory(inventoryId);			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			//PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentStatus(inventoryId);
			
			paymentPaySupplier=inventoryService.fetchPaymentPaySupplierListByPaymentPaySupplierId(request.getParameter("paymentId"));
			if(bankName.equals(""))
			{
				paymentPaySupplier.setChequeDate(null);
				bankName=null;
				cheqNo=null;
				paymentPaySupplier.setPayType(Constants.CASH_PAY_STATUS);
			}
			else
			{
				try {
					//paymentPaySupplier.setChequeDate(dateFormat.parse(cheqDate));
					paymentPaySupplier.setChequeDate(new Date());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				paymentPaySupplier.setPayType(Constants.CHEQUE_PAY_STATUS);
			}
			
			paymentPaySupplier.setBankName(bankName);
			paymentPaySupplier.setChequeNumber(cheqNo);
			//paymentPaySupplier.setDueAmount(paymentDoInfo.getAmountUnPaid()-amount);
			paymentPaySupplier.setDueDate(new Date());
			paymentPaySupplier.setPaidAmount(amount);	
			
			if(paymentPaySupplier.getDueAmount()==0)
			{
				inventory.setPayStatus(true);
			}
			else
			{
				inventory.setPayStatus(false);
			}
			
			paymentPaySupplier.setInventory(inventory);
			inventoryService.updateSupplierPayment(paymentPaySupplier);			
		
			session.setAttribute("saveMsg", "Payment Updated SuccessFully");
		return new ModelAndView("redirect:/"+session.getAttribute("lastUrl"));
	}
	
	@Transactional 	@RequestMapping("/fetchPaymentSupplier")
	public @ResponseBody List<PaymentPaySupplier> fetchPaymentSupplier(HttpServletRequest request) {
		
		String inventoryId=request.getParameter("inventoryId");		
		List<PaymentPaySupplier>  paymentPaySupplierList=inventoryService.fetchPaymentPaySupplierListByInventoryId(inventoryId);
		
		return paymentPaySupplierList;
	}
	
	@Transactional 	@RequestMapping("/deletePaymentSupplier")
	public @ResponseBody List<PaymentPaySupplier> deletePaymentSupplier(HttpServletRequest request) {
		
		String inventoryId=request.getParameter("inventoryId");
		String paymentPayId=request.getParameter("paymentPayId");
		
		inventoryService.deletePaymentPaySupplierListByInventoryId(paymentPayId);
		List<PaymentPaySupplier>  paymentPaySupplierList=inventoryService.fetchPaymentPaySupplierListByInventoryId(inventoryId);
		
		return paymentPaySupplierList;
	}
	
	@Transactional 	@RequestMapping("/editPaymentSupplier")
	public  ModelAndView editPaymentSupplier(Model model,HttpServletRequest request,HttpSession session) {
		
		model.addAttribute("pageName", "Make Supplier Payment");
		
		String paymentPayId=request.getParameter("paymentPayId");
				
		PaymentDoInfo paymentDoInfo= inventoryService.fetchPaymentPaySupplierForEdit(paymentPayId);
		
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		
		model.addAttribute("forEdit", "forSupplierPaymentEdit");
		
		return new ModelAndView("makePayment");
	}
}
