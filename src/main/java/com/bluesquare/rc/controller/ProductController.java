package com.bluesquare.rc.controller;

import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.DamageRecoveryDetails;
import com.bluesquare.rc.entities.DamageRecoveryMonthWise;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.models.ProductReportView;
import com.bluesquare.rc.models.ProductViewList;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.Constants;

@Controller
public class ProductController {

	@Autowired
	CategoriesService categoriesService;

	@Autowired
	BrandService brandService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	Product product;
	
	@Autowired
	Brand brand;
	
	@Autowired
	Categories categories;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	SupplierService supplierService;
	
	
	@Transactional 	@RequestMapping("/fetchProductList")
	public ModelAndView fetchProductList(Model model,HttpSession session) {

		System.out.println("in fechProductList controller");
		model.addAttribute("pageName", "Manage Product");
				
		  
		List<ProductViewList> productList=productService.fetchProductViewListForWebApp();		
		model.addAttribute("productlist", productList);		
		
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));		
		session.setAttribute("saveMsg", null);
		return new ModelAndView("ManageProduct");
	}
	
	@Transactional 	@RequestMapping("/fetchProduct")
	public ModelAndView fechProduct(Model model,HttpServletRequest request,HttpSession session) {

		System.out.println("in fechProductList controller");
		
		model.addAttribute("pageName", "Update Product");
		
		
		long productId=Long.parseLong(request.getParameter("productId"));
		product=productService.fetchProductForWebApp(productId);
		
		model.addAttribute("product", product);	
		
		model.addAttribute("isProductImg", (product.getProductImage()==null)?false:true);
		
	    List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		
		return new ModelAndView("updateProducts");
	}
	
	@Transactional 	@RequestMapping("/addProduct")
	public ModelAndView addProduct(Model model,HttpSession session) {

		System.out.println("in addProduct controller");
		
		model.addAttribute("pageName", "Add Product");
		
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);		  
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoriesService.fetchCategoriesListForWebApp();
		
		model.addAttribute("brandlist", brandList);
		model.addAttribute("categorylist", categoryList);
		
		
		return new ModelAndView("addProducts");
	}
	
	
	@Transactional 	@RequestMapping(value = "/saveProduct", method = RequestMethod.POST)
	public ModelAndView saveCountryForWeb(@ModelAttribute("file") MultipartFile file,HttpSession session ,HttpServletRequest request,Model model)  {
		System.out.println("in Save Product");		
		
		this.product=new Product();
		this.product.setProductName(request.getParameter("productname"));
		this.product.setProductContentType(request.getParameter("imageType"));
		this.product.setProductCode(request.getParameter("productcode"));
		//this.product.setProductImage(productImage);
		this.brand.setBrandId(Long.parseLong(request.getParameter("brandId")));
		this.product.setBrand(brand);
		this.categories.setCategoryId(Long.parseLong(request.getParameter("categoryId")));
		this.product.setCategories(categories);
		
		this.product.setRate(Float.parseFloat(request.getParameter("productRate")));

		this.product.setThreshold(Long.parseLong(request.getParameter("thresholvalue")));
		this.product.setProductAddedDatetime(new Date());
		this.product.setCurrentQuantity(0);
		
		this.product.setProductName((Character.toString(this.product.getProductName().charAt(0)).toUpperCase() + (this.product.getProductName().substring(1)).toLowerCase()));
		boolean flag = false;
		List<Product> productList = productService.fetchProductListForWebApp();
		if(productList!=null){
			for (Product productFromDb : productList) {
				if (productFromDb.getProductName().trim().toUpperCase().equals(product.getProductName().trim().toUpperCase())) {
					flag = true;
					break;
				}
			}
		}
			
		if(flag==true)
		{ 
			session.setAttribute("saveMsg", "Product Name "+Constants.ALREADY_EXIST);
			return new ModelAndView("redirect:/fetchProductList");		
		}
		
		productService.saveProductForWebApp(file, this.product);
		session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
		return new ModelAndView("redirect:/fetchProductList");
	}
	
	@Transactional 	@RequestMapping(value = "/updateProduct", method = RequestMethod.POST)
	public ModelAndView updateCountryForWeb(@ModelAttribute("file") MultipartFile file,HttpSession session,HttpServletRequest request,Model model) {

			System.out.println("in Update Country");
			this.product=new Product();
			this.product.setProductId(Long.parseLong(request.getParameter("productId")));
			
			if (product.getProductId() == 0) {
				System.out.println("con not update because 0 city not available");
				
				session.setAttribute("saveMsg", "Re-Click to Edit Button");
				return new ModelAndView("redirect:/fetchProductList");
			}
			
			Product prdct=productService.fetchProductForWebApp(Long.parseLong(request.getParameter("productId")));
			this.product.setCompany(prdct.getCompany());
			
			this.product.setProductName(request.getParameter("productname"));
			this.product.setProductContentType(request.getParameter("imageType"));
			this.product.setProductCode(request.getParameter("productcode"));
			//this.product.setProductImage(productImage);
			this.brand.setBrandId(Long.parseLong(request.getParameter("brandId")));
			this.product.setBrand(brand);
			this.categories.setCategoryId(Long.parseLong(request.getParameter("categoryId")));
			this.product.setCategories(categories);
			
			this.product.setRate(Float.parseFloat(request.getParameter("productRate")));
			
			this.product.setThreshold(Long.parseLong(request.getParameter("thresholvalue")));
			this.product.setProductAddedDatetime(prdct.getProductAddedDatetime());
			this.product.setCurrentQuantity(prdct.getCurrentQuantity());
			this.product.setProductQuantityUpdatedDatetime(new Date());
			
			this.product.setProductName((Character.toString(this.product.getProductName().charAt(0)).toUpperCase() + (this.product.getProductName().substring(1)).toLowerCase()));
			boolean flag = false;
			List<Product> productList = productService.fetchProductListForWebApp();
			if(productList!=null){
				for (Product productFromDb : productList) {
					if (productFromDb.getProductName().trim().toUpperCase().equals(product.getProductName().trim().toUpperCase()) && prdct.getProductId()!=this.product.getProductId()) {
						flag = true;
						break;
					}
				}
			}
				
			if(flag==true)
			{ 
				session.setAttribute("saveMsg", "Product Name "+Constants.ALREADY_EXIST);
				return new ModelAndView("redirect:/fetchProductList");		
			}			
			
			product.setProductName((Character.toString(product.getProductName().charAt(0)).toUpperCase() + product.getProductName().substring(1)));
			
			if(file.isEmpty())
			{
				this.product.setProductImage(prdct.getProductImage());
			}
			
			productService.updateProductForWebApp(file, this.product);

			session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
			return new ModelAndView("redirect:/fetchProductList");

	}
	
	@Transactional 	@RequestMapping("/downloadProductImage/{productId}/{companyId}")
	public void downloadAdvertImage(@PathVariable("productId") long productId,@PathVariable("companyId") long companyId, HttpServletResponse response,HttpSession session) {
		  
		try {
			Product product = productService.fetchProductForImage(productId,companyId);
				
			if(this.product.getProductImage()==null){
				product=productService.setNoImageToProductForImage(product);
			}
			
			response.setHeader("Content-Disposition", "inline;filename=\"" + product.getProductName() + "\"");
			OutputStream out = response.getOutputStream();
			response.setContentType(product.getProductContentType());
			IOUtils.copy(product.getProductImage().getBinaryStream(), out);
			out.flush();
			out.close();
			
			System.out.println("get image" + product.getProductName()+"."+product.getProductContentType());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		return;
	}
	
	@Transactional 	@RequestMapping("/downloadProductImage/{productId}")
	public void downloadAdvertImage(@PathVariable("productId") long productId, HttpServletResponse response,HttpSession session) {
		  
		try {
			this.product = productService.fetchProductForWebApp(productId);
			
			if(this.product.getProductImage()==null){
				this.product=productService.setNoImageToProductForImage(this.product);
			}
			
			response.setHeader("Content-Disposition", "inline;filename=\"" + this.product.getProductName() + "\"");
			OutputStream out = response.getOutputStream();
			response.setContentType(this.product.getProductContentType());
			IOUtils.copy(this.product.getProductImage().getBinaryStream(), out);
			out.flush();
			out.close();
			
			System.out.println("get image" + this.product.getProductName()+"."+this.product.getProductContentType());
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		return;
	}
	
	@Transactional 	@RequestMapping("/fetchProductByProductId")
	public @ResponseBody Product fetchProductByProductId(Model model,HttpServletRequest request) {

		System.out.println("in fechProductList controller");
		
		long productId=Long.parseLong(request.getParameter("productId"));
		product=productService.fetchProductForWebApp(productId);
		return product;
	}
	
	@Transactional 	@RequestMapping("/fetchProductListForReport")
	public ModelAndView fetchProductListForReport(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Product Report");
		
		List<ProductReportView> productReportViews=productService.fetchProductListForReport(request.getParameter("range"), request.getParameter("startDate"), request.getParameter("endDate"),request.getParameter("topProductNo"));
		model.addAttribute("productReportViews", productReportViews);
		double totalAmountWithTax=0;
		for(ProductReportView productReportView:productReportViews)
		{
			totalAmountWithTax+=productReportView.getAmountWithTax();
		}
		model.addAttribute("totalAmountWithTax", totalAmountWithTax);
		return new ModelAndView("ProductReport");
	}
	
	@Transactional 	@RequestMapping("/fetchProductListAjax")
	public @ResponseBody List<Product> fetchProductListAjax(Model model,HttpSession session) {

		//List<Product> productListPhotoNull=new ArrayList<>();
		List<Product> productList=productService.fetchProductListForWebApp();
		
			/*if(productList!=null)
			{
				Iterator<Product> itr=productList.iterator();
				while(itr.hasNext())
				{
					Product product=itr.next();
					product.setProductImage(null);	
					productListPhotoNull.add(product);
				}
			}*/
		
		return productList;
	}
	
	@Transactional 	@RequestMapping("/damageRecoveryList")
	public ModelAndView damageRecoveryDayWiseList(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Damage Recovery Report");
		
		
		List<DamageRecoveryMonthWise> damageRecoveryDayWiseList=productService.fetchDamageRecoveryDayWise(request.getParameter("startMonth"), 
																										request.getParameter("startYear"),
																										request.getParameter("endMonth"), 
																										request.getParameter("endYear"));
		model.addAttribute("damageRecoveryDayWiseList", damageRecoveryDayWiseList);
		
		List<Product> productList=productService.fetchProductListForWebApp();		
		model.addAttribute("productList", productList);
		
		return new ModelAndView("damageProductReport");
	}
	
	@Transactional 	@RequestMapping("/fetchDamageRecoveryByDamageRecoveryId")
	public @ResponseBody DamageRecoveryMonthWise fetchDamageRecoveryByDamageRecoveryId(Model model,HttpServletRequest request,HttpSession session) {
		
		DamageRecoveryMonthWise damageRecoveryDayWise=productService.fetchDamageRecoveryByDamageRecoveryId(Long.parseLong(request.getParameter("damageRecoveryId")));
		//damageRecoveryDayWise.getProduct().setProductImage(null);
		
		return damageRecoveryDayWise;
	}
	
	@Transactional 	@RequestMapping("/fetchDamageRecoveryDetailsListByDamageRecoveryId")
	public @ResponseBody List<DamageRecoveryDetails> fetchDamageRecoveryByProductId(Model model,HttpServletRequest request,HttpSession session) {

		//List<DamageRecoveryDetails> damageRecoveryDetailsListProductImageNull=new ArrayList<>();
		List<DamageRecoveryDetails> damageRecoveryDetailsList=productService.fetchDamageRecoveryDetailsListByDamageRecoveryId(Long.parseLong(request.getParameter("damageRecoveryId")));
		
		/*if(damageRecoveryDetailsList!=null)
		{
			Iterator<DamageRecoveryDetails> itr=damageRecoveryDetailsList.iterator();
			while(itr.hasNext())
			{
				DamageRecoveryDetails damageRecoveryDetails=itr.next();
				damageRecoveryDetails.getDamageRecoveryMonthWise().getProduct().setProductImage(null);
				damageRecoveryDetailsListProductImageNull.add(damageRecoveryDetails);
			}
		}*/
		
		return damageRecoveryDetailsList;
	}
	
	@Transactional 	@RequestMapping("/saveDamageRecoveryDetails")
	public ModelAndView saveDamageRecoveryDetails(Model model,HttpServletRequest request,HttpSession session) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		DamageRecoveryDetails damageRecoveryDetails=new DamageRecoveryDetails();
		try {
			damageRecoveryDetails.setGivenDate(dateFormat.parse(request.getParameter("givenDate")));
		} catch (ParseException e) {}
		damageRecoveryDetails.setQuantityGiven(Long.parseLong(request.getParameter("quantityGiven")));
		damageRecoveryDetails.setReceiveStatus(false);
		Supplier supplier=new Supplier();
		supplier=supplierService.fetchSupplier(request.getParameter("supplierId"));		
		damageRecoveryDetails.setSupplier(supplier);
		
		productService.saveDamageRecoveryDetails(damageRecoveryDetails, Long.parseLong(request.getParameter("damageRecoveryId")));
		
		SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		
		int year=Integer.parseInt(yearFormat.format(new Date()));
		int month=Integer.parseInt(monthFormat.format(new Date()));
		
		return new ModelAndView("redirect:/damageRecoveryList?startMonth="+month+"&startYear="+year+"&endMonth="+month+"&endYear="+year);
	}
	
	@Transactional 	@RequestMapping("/updateDamageRecoveryDetails")
	public ModelAndView updateDamageRecoveryDetails(Model model,HttpServletRequest request,HttpSession session) {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		DamageRecoveryDetails damageRecoveryDetails=productService.fetchDamageRecoveryDetailsByDamageRecoveryDetailsId(request.getParameter("damageRecoveryDetailsId"));
		try {
			damageRecoveryDetails.setReceiveDate(dateFormat.parse(request.getParameter("receivedDate")));
		} catch (ParseException e) {}
		damageRecoveryDetails.setQuantityReceived(Long.parseLong(request.getParameter("quantityReceived")));
		damageRecoveryDetails.setQuantityNotClaimed(damageRecoveryDetails.getQuantityGiven()-damageRecoveryDetails.getQuantityReceived());
		damageRecoveryDetails.setReceiveStatus(true);
		
		productService.updateDamageRecoveryDetails(damageRecoveryDetails);
		
		SimpleDateFormat monthFormat = new SimpleDateFormat("MM");
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		
		int year=Integer.parseInt(yearFormat.format(new Date()));
		int month=Integer.parseInt(monthFormat.format(new Date()));
		
		return new ModelAndView("redirect:/damageRecoveryList?startMonth="+month+"&startYear="+year+"&endMonth="+month+"&endYear="+year);
	}
	
	
	@Transactional 	@RequestMapping("/damageReport")
	public ModelAndView damageReport(Model model,HttpServletRequest request,HttpSession session) {
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");	
		
		List<DamageDefine> damageDefineList=productService.fetchDamageDefineList(range, startDate, endDate);
		model.addAttribute("damageDefineList", damageDefineList);
		
		return new ModelAndView("damageReport");
	}
}
