package com.bluesquare.rc.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.utils.Constants;

@Controller
public class ManageBrandController {

	@Autowired
	BrandService brandService;
	
	@Autowired
	Brand brand;
	
	@Autowired
	AreaService areaService;

	
	@Transactional 	@RequestMapping("/fetchBrandList")
	public ModelAndView fetchBrandList(Model model,HttpSession session) {
		System.out.println("in fetchBrandList controller");
		model.addAttribute("pageName", "Brand List");
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		model.addAttribute("brandlist", brandList);
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("ManageBrand");
	}
	
	@Transactional 	@RequestMapping("/fetchBrand")
	public @ResponseBody Brand fetchBrand(Model model,HttpServletRequest request) {
		System.out.println("in fetchBrand controller");
		long brandId=Long.parseLong(request.getParameter("brandId"));
		Brand brand=brandService.fetchBrandForWebApp(brandId);
		
		return brand;
	}
	
		//saveCountry
		@Transactional 	@RequestMapping("/saveBrand")
		public ModelAndView saveBrandForWeb(HttpServletRequest request,Model model,HttpSession session)  {
			System.out.println("in Save Brand");		
		
			
			this.brand.setBrandId(Long.parseLong(request.getParameter("brandId")));
			this.brand.setName(request.getParameter("name"));
			
			session.setAttribute("saveMsg", "");
						
			brand.setName((Character.toString(brand.getName().charAt(0)).toUpperCase() + brand.getName().substring(1)));
			
			boolean flag = false;
			List<Brand> brandList = brandService.fetchBrandListForWebApp();
			if(brandList!=null){
				for (Brand brandFromDb : brandList) {
					if (brandFromDb.getName().trim().toUpperCase().equals(brand.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			if (brand.getBrandId() == 0 ) 
			{
				if(flag == false)
				{
					brand.setBrandAddedDatetime(new Date());
					brandService.saveBrandForWebApp(brand);
					session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
					return new ModelAndView("redirect:/fetchBrandList");
				}
			}
			
			if (brand.getBrandId() != 0 ) {
				
				flag = false;
				if(brandList!=null){
					for (Brand brandFromDb : brandList) {
						if (brandFromDb.getName().trim().toUpperCase().equals(brand.getName().trim().toUpperCase()) && 
								brandFromDb.getBrandId()!=brand.getBrandId()) {
							flag = true;
							break;
						}
					}
				}
				if(flag == false)
				{
					System.out.println("Moving request for Update");
					session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
					return updateBrandForWeb(brand,model,session);
				}
			}
			
			/*if (!flag) 
			{
				
				if (brand.getBrandId() != 0) 
				{
					System.out.println("Moving request for Update");
					model.addAttribute("saveMsg", Constants.UPDATE_SUCCESS);
					return updateBrandForWeb(brand);
				}
			}	*/	
			
			session.setAttribute("saveMsg", Constants.ALREADY_EXIST);
			return new ModelAndView("redirect:/fetchBrandList");

		}
		
		@Transactional 	@RequestMapping("/updateBrand")
		public ModelAndView updateBrandForWeb(@ModelAttribute Brand brand,Model model,HttpSession session) {

			System.out.println("in Update Country");
			

			if (brand.getBrandId() == 0 || brand.getName() == null || brand.getName().equals("")) {
				System.out.println("con not update because 0 city not available");
				return new ModelAndView("redirect:/fetchBrandList");
			}
			
			this.brand=brandService.fetchBrandForWebApp(brand.getBrandId());
			brand.setBrandAddedDatetime(this.brand.getBrandAddedDatetime());
			brand.setCompany(this.brand.getCompany());
			brand.setBrandUpdateDatetime(new Date());
			brandService.updateBrandForWebApp(brand);

			return new ModelAndView("redirect:/fetchBrandList");

		}

}
