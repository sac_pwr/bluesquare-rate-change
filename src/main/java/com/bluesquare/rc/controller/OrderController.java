package com.bluesquare.rc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierOrder;
import com.bluesquare.rc.entities.SupplierOrderDetails;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.GstBillReport;
import com.bluesquare.rc.models.OrderDetailsListOfBusiness;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.SalesReportModel;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.CounterOrderService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.SupplierOrderService;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.InvoiceGenerator;
import com.itextpdf.text.DocumentException;

@Controller
public class OrderController {

	@Autowired
	SupplierOrderService supplierOrderService; 
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	SupplierOrder supplierOrder;
	
	@Autowired
	SupplierOrderDetails supplierOrderDetails;
	
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	CounterOrderService counterOrderService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Transactional 	@RequestMapping("/invoiceDownload")
	public ModelAndView invoiceDownload(Model model,HttpSession session,HttpServletResponse response,HttpServletRequest request) throws DocumentException, IOException {
		
		String filePath="/resources/trial.pdf";
    	
    	ServletContext context = request.getServletContext();
        String appPath = context.getRealPath("");
        System.out.println("appPath = " + appPath);
 
        // construct the complete absolute path of the file
        String fullPath = appPath + filePath;      
        File downloadFile = new File(fullPath);
		File dFile;
		long companyId;
		try {
			companyId = Long.parseLong(request.getParameter("companyId"));
		} catch (Exception e) {
			companyId=Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds());
		}
		BillPrintDataModel billPrintDataModel=orderDetailsService.fetchBillPrintData(request.getParameter("orderId"),companyId);
			dFile = InvoiceGenerator.generateInvoicePdf(billPrintDataModel,fullPath);
		
		 // get your file as InputStream
	      InputStream is = new FileInputStream(dFile);
	      // copy it to response's OutputStream
	      org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
	      response.flushBuffer();
		
		return null;
	}
	
	
	
	
	@Transactional 	@RequestMapping("/orderBook")
	public @ResponseBody String orderBook(HttpServletRequest request,HttpSession session) {
		System.out.println("inside order book");
		try {
		/*	Map<Long,String[]> orderCartMap=(Map<Long,String[]>)session.getAttribute("supplierOrderCartMap");
			String productIdList="";
			for(Map.Entry<Long,String[]> entry : orderCartMap.entrySet())
			{
				productIdList=productIdList+entry.getValue()[0]+"-"+entry.getValue()[1]+"-"+entry.getValue()[2]+",";
      		}
			productIdList=productIdList.substring(0, productIdList.length()-1);*/
			supplierOrderService.saveSupplierOrder(request.getParameter("productIdList"));
		} catch (Exception e) {
			return "Failed";
		}
		
		return "Success"; 
	}
	@Transactional 	@RequestMapping("/editOrderBook")
	public @ResponseBody String editOrderBook(HttpServletRequest request) {
		System.out.println("inside editOrderBook");
		try {
			supplierOrder=supplierOrderService.fetchSupplierOrder(request.getParameter("supplierOrderId"));
			supplierOrderService.editSupplierOrder(request.getParameter("productIdList"),supplierOrder);
		} catch (Exception e) {
			return "Failed";
		}
		
		return "Success"; 
	}
	
	@Transactional 	@RequestMapping("/deleteSupplierOrder")
	public ModelAndView deleteSupplierOrder(HttpServletRequest request,Model model,HttpSession session) {
				
		supplierOrderService.deleteSupplierOrder(request.getParameter("supplierOrderId"));
		
		List<SupplierOrder> supplierOrdersList=orderDetailsService.fetchSupplierOrders24hour("CurrentDay","","");
		model.addAttribute("last24Order", supplierOrdersList);
		
		return new ModelAndView("last24OrderDetailsForEdit");
	}
	
	//supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber
	
	/*@RequestMapping("/showOrderReport")
	public ModelAndView showOrderReport(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Order Report");
		
		
		List<OrderReportList> orderReportLists= orderDetailsService.showOrderReport();
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		return new ModelAndView("orderReport");
	}*/
	
	@Transactional 	@RequestMapping("/showSupplierOrderReport")
	public ModelAndView showSupplierOrderReport(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Supplier Orders");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");	
				
		List<SupplierOrder> supplierOrderLists= supplierOrderService.fetchSupplierOrderList(range, startDate, endDate);
		model.addAttribute("supplierOrderLists", supplierOrderLists);
		
		return new ModelAndView("SupplierOrderView");
	}
	
	@Transactional 	@RequestMapping("/fetchSupplierOrderDetails")
	public @ResponseBody List<SupplierOrderDetails> showSupplierOrderDetails(HttpServletRequest request,Model model) {
		
		List<SupplierOrderDetails> supplierOrderDetailsLists= supplierOrderService.fetchSupplierOrderDetailsList(request.getParameter("supplierOrderId"));
				
		model.addAttribute("supplierOrderDetailsLists", supplierOrderDetailsLists);
		
		return supplierOrderDetailsLists;
	}
	
	@Transactional 	@RequestMapping("/showOrderReportByBusinessNameId")
	public ModelAndView showOrderReportByBusinessNameId(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Order Report By Business");
		
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");	
		  
		List<OrderReportList> orderReportLists= orderDetailsService.showOrderReportByBusinessNameId(request.getParameter("businessNameId"), range, startDate, endDate);
		List<OrderReportList> counterOrderReportList=counterOrderService.showCounterOrderReportByBusinessNameId(request.getParameter("businessNameId"), range, startDate, endDate);
		
		//counterOrders add in salesman orders
		if(orderReportLists!=null && counterOrderReportList!=null){
			orderReportLists.addAll(counterOrderReportList);
		}else if(orderReportLists==null && counterOrderReportList!=null){
			orderReportLists=new ArrayList<>();
			orderReportLists.addAll(counterOrderReportList);
		}
		if(orderReportLists!=null){			
			Collections.sort(orderReportLists,new OrderDateComparator());
			List<OrderReportList> orderReportListsTemp=new ArrayList<>();
			int i=1;
			for(OrderReportList orderReportList: orderReportLists){
				orderReportList.setSrno(i);
				i++;
				orderReportListsTemp.add(orderReportList);
			}
		}
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		model.addAttribute("businessNameId", request.getParameter("businessNameId"));
		model.addAttribute("url", "showOrderReportByBusinessNameId");

		session.setAttribute("lastUrl","showOrderReportByBusinessNameId?range="+range+"&startDate="+startDate+"&endDate="+endDate);
		
		return new ModelAndView("orderReport");
	}
	
	public class OrderDateComparator implements Comparator<OrderReportList> {
	    public int compare(OrderReportList p, OrderReportList q) {
	        if (p.getOrderDate().before(q.getOrderDate())) {
	            return -1;
	        } else if (p.getOrderDate().after(q.getOrderDate())) {
	            return 1;
	        } else {
	            return 0;
	        }        
	    }
	}
	
	@Transactional 	@RequestMapping("/showOrderReportByEmployeeSMId")
	public ModelAndView showOrderReportByemployeeSMId(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Order Report By Employee");
	
		  
		  String range=request.getParameter("range");
			String startDate=request.getParameter("startDate");
			String endDate=request.getParameter("endDate");		
		
		List<OrderReportList> orderReportLists= orderDetailsService.showOrderReportByEmployeeSMId(request.getParameter("employeeSMId"), range, startDate, endDate);
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		model.addAttribute("employeeSMId", request.getParameter("employeeSMId"));
		model.addAttribute("url", "showOrderReportByEmployeeSMId");
		
		session.setAttribute("lastUrl","showOrderReportByEmployeeSMId?range="+range+"&startDate="+startDate+"&endDate="+endDate);
		
		return new ModelAndView("orderReport");
	}
	
	@Transactional 	@RequestMapping("/showOrderReport")
	public ModelAndView showOrderReport(Model model,HttpServletRequest request,HttpSession session){		
		model.addAttribute("pageName", "Order Report");
		
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");		
		
		List<OrderReportList> orderReportLists= orderDetailsService.showOrderReport( range, startDate, endDate);
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		model.addAttribute("url", "showOrderReport");
		
		session.setAttribute("lastUrl","showOrderReport?range="+range+"&startDate="+startDate+"&endDate="+endDate);
		
		return new ModelAndView("orderReport");
	}
	
	@Transactional 	@RequestMapping("/cancelOrderReport")
	public ModelAndView cancelOrderReport(Model model,HttpServletRequest request,HttpSession session){		
		model.addAttribute("pageName", "Cancel Order Report");
	
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");		
		
		List<OrderReportList> orderReportLists= orderDetailsService.cancelOrderReport( range, startDate, endDate);
		model.addAttribute("orderReportLists", orderReportLists);
		
		List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		model.addAttribute("url", "cancelOrderReport");
		
		return new ModelAndView("orderReport");
	}

	@Transactional 	@GetMapping("/orderProductDetailsListForWebApp")
	public ModelAndView orderProductDetailsListForWebApp(Model model,HttpServletRequest request,HttpSession session){
		model.addAttribute("pageName", "Order Product Details");
		
		List<OrderProductDetailListForWebApp> orderProductDetailListForWebApps=orderDetailsService.orderProductDetailsListForWebApp(request.getParameter("orderDetailsId"));
		model.addAttribute("orderProductDetailListForWebApps", orderProductDetailListForWebApps);
		
		double totalAmount=0;
		double totalAmountWithTax=0;
		long totalQuantity=0;
		
		for(OrderProductDetailListForWebApp orderProductDetailListForWebApp: orderProductDetailListForWebApps)
		{
			totalAmount+=orderProductDetailListForWebApp.getTotalAmount();
			totalAmountWithTax+=orderProductDetailListForWebApp.getTotalAmountWithTax();
			totalQuantity+=orderProductDetailListForWebApp.getQuantity();
		}
		
		model.addAttribute("totalQuantity", totalQuantity);
		model.addAttribute("totalAmount", totalAmount);
		model.addAttribute("totalAmountWithTax", totalAmountWithTax);
		
		return new ModelAndView("OrderDetails");
	}
	
	/*@Transactional 	@RequestMapping("/fetchLast24HoursOrders")
	public ModelAndView fetchLast24HoursOrders(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Order Details");
		
		
		System.out.println("in fetchLast24HoursOrders controller");
		
		String supplierId=request.getParameter("supplierId");
		//String fetchBy=request.getParameter("fetchBy");
		String filter=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		
		session.setAttribute("last_range",filter);
		session.setAttribute("last_startDate",startDate);
		session.setAttribute("last_endDate",endDate);
		
		if(fetchBy==null)
		{
			List<SupplierOrder> supplierOrdersList=orderDetailsService.fetchSupplierOrders24hour(filter,startDate,endDate);
			model.addAttribute("last24Order", supplierOrdersList);
		}
		else
		{
			List<SupplierOrder> supplierOrdersList=orderDetailsService.fetchSupplierOrders24hourBySupplierId(supplierId);
			model.addAttribute("last24Order", supplierOrdersList);
		}
		model.addAttribute("fetchRecordType", filter);
		
		List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();		
		model.addAttribute("supplierList", supplierList);
		
		return new ModelAndView("last24OrderDetails");
	}*/
	
	@Transactional 	@RequestMapping("/fetchLast24HoursOrdersForEdit")
	public ModelAndView fetchLast24HoursOrdersForEdit(Model model,HttpServletRequest request,HttpSession session) {

		System.out.println("in fetchLast24HoursOrdersForEdit controller");
		
		model.addAttribute("pageName", "Editable Supplier Order List");
		
		List<SupplierOrder> supplierOrdersList=orderDetailsService.fetchSupplierOrders24hour("CurrentDay","","");
		model.addAttribute("last24Order", supplierOrdersList);
		
		return new ModelAndView("last24OrderDetailsForEdit");
	}
	
	@Transactional 	@RequestMapping("/fetchProductBySupplierOrderId")
	public @ResponseBody List<SupplierOrderDetails> fetchSupplierOrderDetailsList(Model model,HttpServletRequest request) {

		System.out.println("in fetchProductBySupplierOrderId controller");
		
		List<SupplierOrderDetails> supplierOrderDetailList=orderDetailsService.fetchSupplierOrderDetailsListBySupplierOrderId(request.getParameter("supplierOrderId"));
		System.out.println(supplierOrderDetailList);
		return supplierOrderDetailList;
	}
	
	
	
	@Transactional 	@RequestMapping("/editSupplierOrderAjax")
	public @ResponseBody List<SupplierOrderDetails> editSupplierOrderAjax(Model model,HttpServletRequest request) {

		System.out.println("in editSupplierOrder controller");
		
		String supplierOrderId=request.getParameter("supplierOrderId");
		
		List<SupplierOrderDetails> supplierOrderDetaillist=supplierOrderService.fetchSupplierOrderDetailsList(supplierOrderId);
		
		return supplierOrderDetaillist;
	}
	
	@Transactional 	@RequestMapping("/fetchOrderDetailsListOfBusinessByBusinessNameId")
	public ModelAndView fetchOrderDetailsListOfBusinessByBusinessNameId(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Order Details By Business");
		
		
		String businessNameId=request.getParameter("businessNameId");
		String range=request.getParameter("range");
		String fromDate=request.getParameter("startDate");
		String toDate=request.getParameter("endDate");
		
		OrderDetailsListOfBusiness orderDetailsListOfBusiness=orderDetailsService.fetchOrderDetailsListOfBusinessByBusinessNameId(businessNameId, fromDate, toDate, range);
		model.addAttribute("orderDetailsListOfBusiness", orderDetailsListOfBusiness);
		
	return new ModelAndView("orderdetailsbybusiness");
	}
	@Transactional 	@RequestMapping("/sendSMSTOShopsUsingOrderId")
	public @ResponseBody String sendSMSTOShopsUsingOrderId(HttpServletRequest request) {
		
		try {
			String shopsIds=request.getParameter("shopsIds");		
			String smsText=request.getParameter("smsText");	
			String mobileNumber=request.getParameter("mobileNumber");	
			orderDetailsService.sendSMSTOShopsUsingOrderId(shopsIds, smsText, mobileNumber);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
	}
	
	@Transactional 	@RequestMapping("/salesReport")
	public ModelAndView salesReport(Model model,HttpServletRequest request,HttpSession session)
	{
		model.addAttribute("pageName", "Sales Report");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");		
		List<SalesReportModel> salesManReportModelList=orderDetailsService.fetchSalesReportModel(startDate, endDate, range);
		model.addAttribute("salesManReportModelList", salesManReportModelList);
		
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		double totalAmount=0;
		long noOfOrders=0;
		if(salesManReportModelList!=null)
		{
			for(SalesReportModel salesManReportModel:salesManReportModelList)
			{
				totalAmount+=salesManReportModel.getOrderAmountWithTax();
				noOfOrders+=1;
			}
		}
		model.addAttribute("noOfOrders", noOfOrders);
		model.addAttribute("totalAmount", Double.parseDouble(decimalFormat.format(totalAmount)));
		return new ModelAndView("SalesReport");
	}
	@Transactional 	@RequestMapping("/fetchGstBillReport")
	public ModelAndView fetchGstBillReport(HttpServletRequest request,Model model) {
		
			model.addAttribute("pageName", "GST Report");
		
			String startDate=request.getParameter("startDate");		
			String endDate=request.getParameter("endDate");	
			String type=request.getParameter("type");	
			
			GstBillReport gstBillReport=null;
			if(type!=null)
			{
				gstBillReport=orderDetailsService.fetchGstBillReport(startDate, endDate,type);
			}
			model.addAttribute("gstBillReport", gstBillReport);
			model.addAttribute("type", type);
			model.addAttribute("startDate", startDate);
			model.addAttribute("endDate", endDate);
			
			return new ModelAndView("gstBillReport");
	}
}
