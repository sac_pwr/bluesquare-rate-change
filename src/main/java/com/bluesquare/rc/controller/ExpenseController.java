package com.bluesquare.rc.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.dom4j.Branch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Expense;
import com.bluesquare.rc.entities.ExpenseType;
import com.bluesquare.rc.models.ExpenseListModel;
import com.bluesquare.rc.service.ExpenseService;
import com.bluesquare.rc.utils.Constants;

@Controller
public class ExpenseController {

	@Autowired
	ExpenseService expenseService;
	
	@Transactional 	@RequestMapping("/addExpense")
	public ModelAndView addExpense(Model model,HttpSession session) {
		
		System.out.println("in addExpense controller");
		model.addAttribute("pageName", "Add Expense");
		
		List<ExpenseType> expenseTypeList=expenseService.fetchExpenseTypeList();
		model.addAttribute("expenseTypeList", expenseTypeList);
		
		return new ModelAndView("addExpense");
	}
	
	@Transactional 	@RequestMapping("/saveExpense")
	public ModelAndView saveExpense(Model model,HttpSession session,HttpServletRequest request) {
		
		System.out.println("in saveExpense controller");
		
		String amount=request.getParameter("amount");
		String bankName=request.getParameter("bankName");
		String chequeNumber=request.getParameter("chequeNumber");
		String expenseTypeId=request.getParameter("expenseTypeId");
		String refernce=request.getParameter("reference");
		String type=request.getParameter("type");
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat dateTimeFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Expense expense=new Expense();	
				
		expense.setAddDate(new Date());		
		expense.setAmount(Double.parseDouble(amount));
		
		if(type.equals(Constants.CASH_PAY_STATUS)){
			expense.setBankName(null);
			expense.setChequeNumber(null);
		}else{
			expense.setBankName(bankName);
			expense.setChequeNumber(chequeNumber);
		}
		
		ExpenseType expenseType=expenseService.fetchExpenseTypeByExpenseTypeId(Long.parseLong(expenseTypeId));
		expense.setExpenseType(expenseType);
		
		expense.setReference(refernce);
		expense.setType(type);		
		expense.setStatus(true);
		
		expenseService.saveExpense(expense);
		
		return new ModelAndView("redirect:/fetchExpenseList?range=currentMonth");
	}
	
	@Transactional 	@RequestMapping("/fetchExpense")
	public ModelAndView fetchExpense(Model model,HttpSession session,HttpServletRequest request){
		
		System.out.println("in fetchExpense controller");
		model.addAttribute("pageName", "Update Expense");
		
		Expense expense=expenseService.fetchExpense(request.getParameter("expenseId"));
		model.addAttribute("expense", expense);
		
		List<ExpenseType> expenseTypeList=expenseService.fetchExpenseTypeList();
		model.addAttribute("expenseTypeList", expenseTypeList);
		
		return new ModelAndView("updateExpense");
	}
	
	@Transactional 	@RequestMapping("/updateExpense")
	public ModelAndView updateExpense(Model model,HttpSession session,HttpServletRequest request) {
		
		System.out.println("in updateExpense controller");
		
		String expenseId=request.getParameter("expenseId");
		String amount=request.getParameter("amount");
		String bankName=request.getParameter("bankName");
		String chequeNumber=request.getParameter("chequeNumber");
		String expenseTypeId=request.getParameter("expenseTypeId");
		String refernce=request.getParameter("reference");
		String type=request.getParameter("type");
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat=new SimpleDateFormat("HH:mm:ss");
		SimpleDateFormat dateTimeFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Expense expense=expenseService.fetchExpense(expenseId);
				
		expense.setUpdateDate(new Date());		
		expense.setAmount(Double.parseDouble(amount));

		if(type.equals(Constants.CASH_PAY_STATUS)){
			expense.setBankName(null);
			expense.setChequeNumber(null);
		}else{
			expense.setBankName(bankName);
			expense.setChequeNumber(chequeNumber);
		}

		ExpenseType expenseType=expenseService.fetchExpenseTypeByExpenseTypeId(Long.parseLong(expenseTypeId));
		expense.setExpenseType(expenseType);
		
		expense.setReference(refernce);
		expense.setType(type);		
		
		expenseService.updateExpense(expense);
		
		return new ModelAndView("redirect:/fetchExpenseList?range=currentMonth");
	}
	
	@Transactional 	@RequestMapping("/deleteExpense")
	public ModelAndView deleteExpense(Model model,HttpSession session,HttpServletRequest request){
		
		System.out.println("in deleteExpense controller");
		Expense expense=expenseService.fetchExpense(request.getParameter("expenseId"));
		expense.setStatus(false);
		expenseService.deleteExpense(expense);
		
		return new ModelAndView("redirect:/fetchExpenseList?range=currentMonth");
	}
	
	@Transactional 	@RequestMapping("/fetchExpenseList")
	public ModelAndView fetchExpenseList(Model model,HttpSession session,HttpServletRequest request){
		
		System.out.println("in fetchExpenseList controller");
		model.addAttribute("pageName", "Expense List");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		
		List<ExpenseListModel> expenseListModelList=expenseService.fetchExpenseListByRange(range, startDate, endDate);
		model.addAttribute("expenseList", expenseListModelList);
		
		float total=0;
		if(expenseListModelList!=null){
			for(ExpenseListModel expenseListModel: expenseListModelList){
				total+=expenseListModel.getAmount();
			}
		}
		model.addAttribute("total", total);
		return new ModelAndView("ManageExpense");
	}
	
	
	// New Code For Expense Module and Related to Profit Loss
	
		@RequestMapping("expense_type_page")
		public ModelAndView displayExpenseType(HttpSession session, HttpServletRequest request,Model model) {
			return new ModelAndView("expense_type");
		}
		
		@RequestMapping("expense_type_check_for_add")
		public @ResponseBody String expenseTypeCheckForAdd(HttpSession session, HttpServletRequest request,Model model) {
			String expenseTypeName=request.getParameter("name");			
			return expenseService.checkExistByExpenseTypeIdForAdd(expenseTypeName);
		}
		
		@RequestMapping("expense_type_check_for_update")
		public @ResponseBody String expenseTypeCheckForUpdate(HttpSession session, HttpServletRequest request,Model model) {
			String expenseTypeName=request.getParameter("name");
			long expenseTypeId=Long.parseLong(request.getParameter("expenseTypeId"));
			return expenseService.checkExistByExpenseTypeIdForUpdate(expenseTypeName,expenseTypeId);
		}

		// Add Expense Type
		@RequestMapping("save_expense_type")
		public @ResponseBody String saveExpenseType(@ModelAttribute ExpenseType expenseType, Model model, HttpSession session,
				HttpServletRequest request) {
				expenseService.saveExpenseType(expenseType);
				return Constants.SUCCESS_RESPONSE;
			
		}
		
		@RequestMapping("update_expense_type")
		public @ResponseBody String updateExpenseType(@ModelAttribute ExpenseType expenseType, Model model, HttpSession session,
				HttpServletRequest request) {
			ExpenseType expenseTypeOld=expenseService.fetchExpenseTypeByExpenseTypeId(expenseType.getExpenseTypeId());
			expenseTypeOld.setName(expenseType.getName());
			expenseTypeOld.setUpdatedDate(new Date());
			expenseService.updateExpenseType(expenseTypeOld);
			return Constants.SUCCESS_RESPONSE;
		}
		
		@RequestMapping("delete_expense_type")
		public @ResponseBody String deleteExpenseType(@ModelAttribute ExpenseType expenseType, Model model, HttpSession session,
				HttpServletRequest request) {
			ExpenseType expenseTypeOld=expenseService.fetchExpenseTypeByExpenseTypeId(expenseType.getExpenseTypeId());
			expenseTypeOld.setStatus(true);
			expenseService.updateExpenseType(expenseTypeOld);
			return Constants.SUCCESS_RESPONSE;
		}

		@RequestMapping("/fetchExpenseType")
		public @ResponseBody ExpenseType fetchExpenseType(Model model, HttpServletRequest request) {

			long expenseTypeId = Long.parseLong(request.getParameter("expenseTypeId"));
			ExpenseType expenseType = expenseService.fetchExpenseTypeByExpenseTypeId(expenseTypeId);
			return expenseType;
		}
		
		@RequestMapping("/fetchExpenseTypeList")
		public @ResponseBody List<ExpenseType> fetchExpenseTypeList(Model model, HttpServletRequest request) {

			List<ExpenseType> expenseTypeList = expenseService.fetchExpenseTypeList();
			
			return expenseTypeList;
		}

		

}
