package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.models.EmployeeAreaDetails;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.EmployeeService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.SelectedAccess;
@Controller
public class MainController {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	CompanyService companyService; 
	
	@Autowired
	AreaService areaService;
	
	
	@Transactional 	@RequestMapping("/")
	public ModelAndView indexPage(HttpSession session,Model model) 
	{		
		System.out.println("in main controller");
		model.addAttribute("pageName", "Index");
		return new ModelAndView("Index");
		//return new ModelAndView("chequePaymentReport");
	}
	
	@Transactional 	@RequestMapping("/login")
	public ModelAndView login(HttpSession session,Model model) 
	{
		
		System.out.println("in login controller");
		//return new ModelAndView("home");
		return new ModelAndView("login");
		
	}
	
	@Transactional 	@RequestMapping("/loginEmployee")
	public ModelAndView loginEmployee(Model model,HttpServletRequest request,HttpSession session) 
	{
		System.out.println("in loginEmployee controller");
		Company company=companyService.validateCompany(request.getParameter("userId"), request.getParameter("password"));
		Employee employee=employeeService.validate(request.getParameter("userId"), request.getParameter("password"));
		if(employee!=null)
		{
			if(employee.getDepartment().getName().equals(Constants.GATE_KEEPER_DEPT_NAME))
			{
				EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employee.getEmployeeId());
				session.setAttribute("employeeDetails", employeeDetails);			
				
				SelectedAccess selectedAccess=setSessionSelectedLocations(employeeDetails.getEmployeeDetailsId(),employeeDetails.getEmployee().getCompany().getCompanyId(),Constants.GATE_KEEPER_DEPT_NAME);				
				session.setAttribute("selectedAccess", selectedAccess);
				
				Long companyId=employee.getCompany().getCompanyId();
				List<Long> companyIdList=new ArrayList<>();
				companyIdList.add(companyId);

				session.setAttribute("loginName", employeeDetails.getName());
				
				session.setAttribute("sessionCopamnyName", employee.getCompany().getCompanyName());
				session.setAttribute("selectedCompanyIds", companyIdList);
				session.setAttribute("loginType", employee.getDepartment().getName());
				
				return new ModelAndView("redirect:/");
				
			}else if(employee.getDepartment().getName().equals(Constants.ADMIN)){
				
				//-start-//
				List<Long> companyIdList=new ArrayList<>();
				//all companies id set in companyIdList
				/*List<Company> companyList=companyService.fetchAllCompany();
				if(companyList!=null){
					for(Company company1: companyList){
						companyIdList.add(company1.getCompanyId());
					}
				}else{*/
					companyIdList.add((long)-1);
				//}
				session.setAttribute("sessionCopamnyName", "Co. Not Selected");
				session.setAttribute("selectedCompanyIds", companyIdList);
				//-end-//
				
				SelectedAccess selectedAccess=setSessionSelectedLocations(employee.getEmployeeId(),employee.getCompany().getCompanyId(),Constants.ADMIN);				
				session.setAttribute("selectedAccess", selectedAccess);
				
				/*EmployeeDetails employeeDetails=new EmployeeDetails();
				session.setAttribute("employeeDetails", employeeDetails);*/
				
				session.setAttribute("loginName", "Admin");
				
				session.setAttribute("loginType", employee.getDepartment().getName());
				
				session.setAttribute("showNavbar", false);
				return new ModelAndView("redirect:/companySettingInitial");
				
			}else{
				session.invalidate();
				model.addAttribute("validateMsg", "Login failed");
				return new ModelAndView("login");
			}
		}else if(company!=null){
			
			session.setAttribute("companyDetails", company);
			
			Long companyId=company.getCompanyId();
			List<Long> companyIdList=new ArrayList<>();
			companyIdList.add(companyId);
			
			session.setAttribute("companyDetails", company);
			session.setAttribute("sessionCopamnyName", company.getCompanyName());
			session.setAttribute("selectedCompanyIds", companyIdList);
			
			SelectedAccess selectedAccess=setSessionSelectedLocations(0,0,Constants.COMPANY_ADMIN);				
			session.setAttribute("selectedAccess", selectedAccess);
			
			session.setAttribute("loginName", company.getCompanyName());
			
			session.setAttribute("loginType", Constants.COMPANY_ADMIN);
			return new ModelAndView("redirect:/");
			
		}else{
			session.invalidate();
			model.addAttribute("validateMsg", "Login failed");
			return new ModelAndView("login");
		}
		
	}
	
	@Transactional 	@RequestMapping("/logoutEmployee")
	public ModelAndView logoutEmployee(HttpServletRequest request,HttpSession session) 
	{
		session.invalidate();
		
		return new ModelAndView("redirect:/");
	}
	
	/***
	 * 
	 * @param employeeId 
	 * @param uesrType : type of user i.e GK,DB,SM,Other
	 * @return
	 */
	public SelectedAccess setSessionSelectedLocations(long employeeId,long companyId,String uesrType){
		
		if(uesrType.equals(Constants.GATE_KEEPER_DEPT_NAME)){
			
			//branch setting data All
			EmployeeAreaDetails employeeAreaDetails = employeeDetailsService.fetchEmployeeAreaDetails(employeeId,companyId);
			
			/*Set<Long> countryIdSet=new HashSet<>();
			Set<Long> stateIdSet=new HashSet<>();*/
			Set<Long> cityIdSet=new HashSet<>();
			Set<Long> regionIdSet=new HashSet<>();
			Set<Long> areaIdSet=new HashSet<>();
			
			for(EmployeeAreaList employeeArea: employeeAreaDetails.getAreaList())
			{
				/*countryIdSet.add(employeeArea.getArea().getRegion().getCity().getState().getCountry().getCountryId());
				stateIdSet.add(employeeArea.getArea().getRegion().getCity().getState().getStateId());*/
				cityIdSet.add(employeeArea.getArea().getRegion().getCity().getCityId());
				regionIdSet.add(employeeArea.getArea().getRegion().getRegionId());
				areaIdSet.add(employeeArea.getArea().getAreaId()); 
			}
			
			List<Long> areaIdList=new ArrayList<>();
			List<Long> regionIdList=new ArrayList<>();
			List<Long> cityIdList=new ArrayList<>();
			List<Long> stateIdList=new ArrayList<>();
			List<Long> countryIdList=new ArrayList<>();
			
			areaIdList.addAll(areaIdSet);
			regionIdList.addAll(regionIdSet);
			cityIdList.addAll(cityIdSet);
			/*stateIdList.addAll(stateIdSet);
			countryIdList.addAll(countryIdSet);	*/
			
			return new SelectedAccess(areaIdList, regionIdList, cityIdList, stateIdList, countryIdList);
		} else {
			
			List<Area> areaList=areaService.fetchAllAreaForWebApp();
			
			/*Set<Long> countryIdSet=new HashSet<>();
			Set<Long> stateIdSet=new HashSet<>();*/
			Set<Long> cityIdSet=new HashSet<>();
			Set<Long> regionIdSet=new HashSet<>();
			Set<Long> areaIdSet=new HashSet<>();
			
			if(areaList!=null){
				for(Area area: areaList)
				{
					/*countryIdSet.add(area.getRegion().getCity().getState().getCountry().getCountryId());
					stateIdSet.add(area.getRegion().getCity().getState().getStateId());*/
					cityIdSet.add(area.getRegion().getCity().getCityId());
					regionIdSet.add(area.getRegion().getRegionId());
					areaIdSet.add(area.getAreaId());
				}
			}
			
			List<Long> areaIdList=new ArrayList<>();
			List<Long> regionIdList=new ArrayList<>();
			List<Long> cityIdList=new ArrayList<>();
			List<Long> stateIdList=new ArrayList<>();
			List<Long> countryIdList=new ArrayList<>();
			
			areaIdList.addAll(areaIdSet);
			regionIdList.addAll(regionIdSet);
			cityIdList.addAll(cityIdSet);
		/*	stateIdList.addAll(stateIdSet);
			countryIdList.addAll(countryIdSet);	*/
			
			return new SelectedAccess(areaIdList, regionIdList, cityIdList, stateIdList, countryIdList);
			
		}
	}
	
	
}

