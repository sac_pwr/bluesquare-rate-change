package com.bluesquare.rc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.BusinessNameDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CounterOrderReport;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.PaymentCounterReport;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.service.CounterOrderService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.InvoiceGenerator;

/**
 * <pre>
 * @author Sachin Pawar 
 * 
 * API EndPoints
 * 1.openCounter
 * 
 * </pre>
 */
@Controller
public class CounterOrderController {

	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	BusinessNameDAO businessNameDAO;
	
	@Autowired
	CounterOrderService counterOrderService;
	
	/**
	 * open counter page with product list and businessName list
	 * @param session
	 * @param model
	 * @return ModelAndView counterOrder.jsp
	 */
	@Transactional 	@RequestMapping("/openCounter")
	public ModelAndView openCounter(HttpSession session,Model model){	
		System.out.println("in openCounter controller");
		model.addAttribute("pageName", "Counter");
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		model.addAttribute("productList", productList);
		
		List<BusinessName> businessNameList=businessNameDAO.getBusinessNameListForWebApp();
		model.addAttribute("businessNameList", businessNameList);
		
		model.addAttribute("isEdit", false);
		
		return new ModelAndView("counterOrder");
	}
	
	/**
	 * open counter page with product list and businessName list for Edit
	 * @param session
	 * @param model
	 * @return ModelAndView counterOrder.jsp
	 */
	@Transactional 	@RequestMapping("/openCounterForEdit")
	public ModelAndView openCounterForEdit(HttpSession session,Model model,HttpServletRequest request){	
		System.out.println("in openCounter controller");
		model.addAttribute("pageName", "Counter");
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		model.addAttribute("productList", productList);
		
		List<BusinessName> businessNameList=businessNameDAO.getBusinessNameListForWebApp();
		model.addAttribute("businessNameList", businessNameList);
		
		model.addAttribute("isEdit", true);
		
		model.addAttribute("counterOrder", counterOrderService.fetchCounterOrder(request.getParameter("counterOrderId")));
		
		return new ModelAndView("counterOrder");
	}
	
	@Transactional 	@RequestMapping("/fetchCounterOrderProductDetailsByCounterOrderId")
	public @ResponseBody List<CounterOrderProductDetails> fetchCounterOrderProductDetailsByCounterOrderId(HttpSession session,HttpServletRequest request,Model model){	
		List<CounterOrderProductDetails> counterOrderProductDetailsList=counterOrderService.fetchCounterOrderProductDetails(request.getParameter("counterOrderId"));
		return counterOrderProductDetailsList;
	}
	
	@Transactional 	@RequestMapping("/fetchCounterOrderByCounterOrderId")
	public @ResponseBody CounterOrder fetchCounterOrderByCounterOrderId(HttpSession session,HttpServletRequest request,Model model){	
		CounterOrder counterOrder=counterOrderService.fetchCounterOrder(request.getParameter("counterOrderId"));
		return counterOrder;
	}
	
	
	/***
	 * save counter order
	 * @param session
	 * @param model
	 * @return ModelAndView redirect:/openCounter
	 */
	@Transactional 	@RequestMapping("/saveCounterOrder")
	public @ResponseBody BaseDomain saveCounterOrder(HttpSession session,HttpServletRequest request,Model model){	
		BaseDomain baseDomain=new BaseDomain();
		String counterOrderId;
		try {
			String counterOrderProductDetails=request.getParameter("productList");
			String businessNameId=request.getParameter("businessNameId");
			String paidAmount=request.getParameter("paidAmount");
			String balAmount=request.getParameter("balAmount");
			String dueDate=request.getParameter("dueDate");
			String payType=request.getParameter("payType");
			String paymentType=request.getParameter("paymentType");
			String bankName=request.getParameter("bankName");
			String chequeNumber=request.getParameter("chequeNumber");
			String chequeDate=request.getParameter("chequeDate");
			String custName=request.getParameter("custName");
			String mobileNo=request.getParameter("mobileNo");
			String gstNo=request.getParameter("gstNo");
			
			counterOrderId=counterOrderService.saveCounterOrder(counterOrderProductDetails, 
																businessNameId, 
																paidAmount, 
																balAmount, 
																dueDate, 
																payType, 
																paymentType,
																bankName, 
																chequeNumber, 
																chequeDate, 
																custName, 
																mobileNo, 
																gstNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Something Went Wrong");
			return baseDomain;
		}
		
		baseDomain.setErrorMsg(counterOrderId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		return baseDomain;
	}
	
	/***
	 * save counter order
	 * @param session
	 * @param model
	 * @return ModelAndView redirect:/openCounter
	 */
	@Transactional 	@RequestMapping("/updateCounterOrderForEdit")
	public @ResponseBody BaseDomain updateCounterOrderForEdit(HttpSession session,HttpServletRequest request,Model model){	
		BaseDomain baseDomain=new BaseDomain();
		String counterOrderId;
		try {
				   counterOrderId=request.getParameter("counterOrderId");
				   String counterOrderProductDetails=request.getParameter("productList");
					String businessNameId=request.getParameter("businessNameId");
					String paidAmount=request.getParameter("paidAmount");
					String balAmount=request.getParameter("balAmount");
					String dueDate=request.getParameter("dueDate");
					String payType=request.getParameter("payType");
					String paymentType=request.getParameter("paymentType");
					String bankName=request.getParameter("bankName");
					String chequeNumber=request.getParameter("chequeNumber");
					String chequeDate=request.getParameter("chequeDate");
					String custName=request.getParameter("custName");
					String mobileNo=request.getParameter("mobileNo");
					String gstNo=request.getParameter("gstNo");
					String refAmount=request.getParameter("refAmount");
					String paymentSituation=request.getParameter("paymentSituation");
					
			counterOrderId=counterOrderService.updateCounterOrderForEdit(counterOrderId,
																		counterOrderProductDetails, 
																		businessNameId, 
																		paidAmount, 
																		balAmount, 
																		refAmount,
																		paymentSituation,
																		dueDate, 
																		payType, 
																		paymentType,
																		bankName, 
																		chequeNumber, 
																		chequeDate, 
																		custName, 
																		mobileNo, 
																		gstNo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Something Went Wrong");
			return baseDomain;
		}
		
		baseDomain.setErrorMsg(counterOrderId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		return baseDomain;
	}
	
	@Transactional 	@GetMapping("/deleteCounterOrder")
	public ModelAndView deleteCounterOrder(Model model,HttpServletRequest request,HttpSession session){
		
		counterOrderService.deleteCounterOrder(request.getParameter("counterOrderId"));
		
		return new ModelAndView("redirect:/counterOrderReport?range=today");
	}
	
	/***
	 * fetch order product details and show 
	 * @param model
	 * @param request
	 * @param session
	 * @return ModelAndView OrderDetails.jsp
	 */
	@Transactional 	@GetMapping("/counterOrderProductDetailsListForWebApp")
	public ModelAndView orderProductDetailsListForWebApp(Model model,HttpServletRequest request,HttpSession session){
		model.addAttribute("pageName", "Order Product Details");
		
		
		List<OrderProductDetailListForWebApp> orderProductDetailListForWebApps=counterOrderService.fetchCounterOrderProductDetailsForShowOrderDetails(request.getParameter("counterOrderId"));
		model.addAttribute("counterOrderProductDetailListForWebApps", orderProductDetailListForWebApps);
		
		double totalAmount=0;
		double totalAmountWithTax=0;
		long totalQuantity=0;
		
		for(OrderProductDetailListForWebApp orderProductDetailListForWebApp: orderProductDetailListForWebApps)
		{
			totalAmount+=orderProductDetailListForWebApp.getTotalAmount();
			totalAmountWithTax+=orderProductDetailListForWebApp.getTotalAmountWithTax();
			totalQuantity+=orderProductDetailListForWebApp.getQuantity();
		}
		
		model.addAttribute("totalQuantity", totalQuantity);
		model.addAttribute("totalAmount", totalAmount);
		model.addAttribute("totalAmountWithTax", totalAmountWithTax);
		
		return new ModelAndView("counterOrderDetails");
	}
	
	/***
	 * counter order report by range
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView counterOrderReoprt.jsp
	 */
	@Transactional 	@RequestMapping("/counterOrderReport")
	public ModelAndView counterOrderReport(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Counter Order Report");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");	
	
		List<CounterOrderReport> counterOrderReportList=counterOrderService.fetchCounterOrderReport(range, startDate, endDate);
		model.addAttribute("counterOrderReportList", counterOrderReportList);
		
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", null);
		return new ModelAndView("counterOrderReoprt");
	}
	
	
	/**
	 * fetch payment did list for collection report
	 * @param session
	 * @param request
	 * @param model
	 * @return List PaymentCounter
	 */
	@Transactional 	@RequestMapping("/fetchPaymentCounterOrder")
	public @ResponseBody List<PaymentCounterReport> fetchPaymentCounterOrder(HttpSession session,HttpServletRequest request,Model model){	
		
		List<PaymentCounterReport> paymentCounterList=counterOrderService.fetchPaymentCounterReportListByCounterOrderId(request.getParameter("counterOrderId"));
		
		return paymentCounterList;
	}
	
	
	@Transactional 	@RequestMapping("/paymentCounterOrder")
	public  ModelAndView paymentEmployee(Model model,HttpServletRequest request,HttpSession session) {
		model.addAttribute("pageName", "Counter Order Payment");
	
		PaymentDoInfo paymentDoInfo = counterOrderService.fetchPaymentInfoByCounterOrderId(request.getParameter("counterOrderId"));
		
		model.addAttribute("paymentDoInfo", paymentDoInfo);
		
		return new ModelAndView("makePayment");
	}
	
	@Transactional 	@RequestMapping("/giveCounterOrderPayment")
	public ModelAndView giveEmployeePayment(HttpServletRequest request,HttpSession session,Model model) {
					
			double amount=Double.parseDouble(request.getParameter("amount"));
			String bankName=request.getParameter("bankName");
			String cheqNo=request.getParameter("cheqNo");
			String cheqDate=request.getParameter("cheqDate");
			String dueDate=request.getParameter("dueDate");
			
			CounterOrder counterOrder=counterOrderService.fetchCounterOrder(request.getParameter("inventoryId"));
			
			//take counterOrderId from inventoryId
			List<PaymentCounter> paymentCounterList=counterOrderService.fetchPaymentCounterListByCounterOrderId(request.getParameter("inventoryId"));
			
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			PaymentCounter paymentCounter=new PaymentCounter();	
			
			if(paymentCounterList==null){
				
				if(bankName.equals("")){
					paymentCounter.setChequeDate(null);
					paymentCounter.setBankName(null);
					paymentCounter.setChequeNumber(null);
					paymentCounter.setPayType(Constants.CASH_PAY_STATUS);
				}else{
					
					try { paymentCounter.setChequeDate(dateFormat.parse(cheqDate)); } catch (Exception e) {}
					paymentCounter.setPayType(Constants.CHEQUE_PAY_STATUS);
					paymentCounter.setBankName(bankName);
					paymentCounter.setChequeNumber(cheqNo);
				}
				paymentCounter.setLastDueDate(counterOrder.getPaymentDueDate());
				paymentCounter.setCurrentAmountPaid(amount);
				//paymentCounter.setTotalAmountPaid(amount);
				//paymentCounter.setBalanceAmount(counterOrder.getTotalAmountWithTax()-amount);	
				paymentCounter.setPaidDate(new Date());
				paymentCounter.setCounterOrder(counterOrder);
				paymentCounter.setChequeClearStatus(true);
				counterOrderService.savePaymentCounter(paymentCounter);
				
				if(amount==counterOrder.getTotalAmountWithTax()){
					counterOrder.setPayStatus(true);
				}else{
					try { counterOrder.setPaymentDueDate(dateFormat.parse(dueDate)); } catch (Exception e) {}
					counterOrder.setPayStatus(false);
				}
				counterOrderService.updateCounterOrder(counterOrder);
				
			}else{
				double amountPaid=0;
				for(PaymentCounter paymentCounter2: paymentCounterList){
					amountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
				}
				double totalAmountPaid=amountPaid;//paymentCounterList.get(0).getTotalAmountPaid();
				
				if(bankName.equals("")){
					paymentCounter.setChequeDate(null);
					paymentCounter.setBankName(null);
					paymentCounter.setChequeNumber(null);
					paymentCounter.setPayType(Constants.CASH_PAY_STATUS);
				}else{					
					try { paymentCounter.setChequeDate(dateFormat.parse(cheqDate)); } catch (Exception e) {}
					paymentCounter.setPayType(Constants.CHEQUE_PAY_STATUS);
					paymentCounter.setBankName(bankName);
					paymentCounter.setChequeNumber(cheqNo);
				}
				paymentCounter.setLastDueDate(counterOrder.getPaymentDueDate());
				paymentCounter.setCurrentAmountPaid(amount);
				//paymentCounter.setTotalAmountPaid(totalAmountPaid+amount);
				//paymentCounter.setBalanceAmount(counterOrder.getTotalAmountWithTax()-(totalAmountPaid+amount));	
				paymentCounter.setPaidDate(new Date());
				paymentCounter.setCounterOrder(counterOrder);
				paymentCounter.setChequeClearStatus(true);
				
				counterOrderService.savePaymentCounter(paymentCounter);
				
				if((totalAmountPaid+amount)==counterOrder.getTotalAmountWithTax()){
					counterOrder.setPayStatus(true);
				}else{
					try { counterOrder.setPaymentDueDate(dateFormat.parse(dueDate)); } catch (Exception e) {}
					counterOrder.setPayStatus(false);
				}
				counterOrderService.updateCounterOrder(counterOrder);
			}
	
			session.setAttribute("saveMsg", "Payment Done SuccessFully");
		return new ModelAndView("redirect:/counterOrderReport?range=today");
	}
	
	/***
	 * create counter order invoice and add in response
	 * @param request
	 * @param model
	 * @param session
	 * @param response
	 */
	@Transactional 	@RequestMapping("/counterOrderInvoice.pdf")
	public void counterBillPrint(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
	
		  model.addAttribute("pageName", "Invoice");
				
		  try {
			String counterOrderId=request.getParameter("counterOrderId");
				BillPrintDataModel billPrintDataModel=counterOrderService.fetchCounterBillPrintData(counterOrderId);
				
				//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
				String filePath="/resources/pdfFiles/invoice.pdf";
				
				ServletContext context = request.getServletContext();
				String appPath = context.getRealPath("/");
				System.out.println("appPath = " + appPath);

				// construct the complete absolute path of the file
				String fullPath = appPath + filePath;      
				//File downloadFile = new File(fullPath);
				
				File dFile= InvoiceGenerator.generateInvoicePdf(billPrintDataModel, fullPath);
								
				 // get your file as InputStream
				  InputStream is = new FileInputStream(dFile);
				  // copy it to response's OutputStream
				  response.setContentType("application/pdf");
				  org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				  response.flushBuffer();
				  response.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return;
	}
}
