package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Address;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Region;
import com.bluesquare.rc.entities.State;
import com.bluesquare.rc.models.BusinessNameList;
import com.bluesquare.rc.models.CustomerReportView;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.service.BusinessTypeService;
import com.bluesquare.rc.service.CityService;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.CountryService;
import com.bluesquare.rc.service.DepartmentService;
import com.bluesquare.rc.service.EmployeeAreaListService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.EmployeeService;
import com.bluesquare.rc.service.RegionService;
import com.bluesquare.rc.service.StateService;
import com.bluesquare.rc.utils.Constants;

@Controller
public class ManageBusiness {

	@Autowired
	CountryService countryService;
	
	@Autowired
	Country country;
	
	@Autowired
	StateService stateService;
	
	@Autowired
	State state;
	
	@Autowired
	CityService cityService;
	
	@Autowired
	City city;
	
	@Autowired
	RegionService regionService;
	
	@Autowired
	Region region;
	
	@Autowired
	AreaService areaService;
	
	@Autowired
	Area area;
	
	@Autowired
	Address address;
	
	@Autowired
	Department department;

	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	EmployeeAreaListService employeeAreaListService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	EmployeeDetails employeeDetails;
	
	@Autowired
	Employee employee;
	
	@Autowired
	EmployeeAreaList employeeAreaList;
	
	@Autowired
	Contact contact;
	
	@Autowired
	BusinessNameService businessNameService;
	
	@Autowired
	BusinessName businessName;
	
	@Autowired
	BusinessTypeService businessTypeService;
	
	@Autowired
	BusinessType businessType;
	

	@Autowired
	CompanyService companyService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;	
	
	@Transactional 	@RequestMapping("/addBusiness")
	public ModelAndView addBusiness(Model model,HttpSession session) {

		model.addAttribute("pageName", "Add Business");
		
		System.out.println("in addBusiness controller");
		
		/*
		this.country=countryService.fetchIndiaCountry();
		List<State> stateList=stateService.fetchStateByCountryIdforwebapp(this.country.getCountryId());*/
		
		List<City> cityList=new ArrayList<>();
		List<CompanyCities> companyCities=companyService.fetchCompanyCities(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		for(CompanyCities companyCity: companyCities){
			cityList.add(companyCity.getCity());
		}
		model.addAttribute("cityList", cityList);
		
		//List<Country> coutryList = countryService.fetchCountryListForWebApp();
		List<BusinessType> businessTypeList=businessTypeService.fetchBusinessTypeListForWebApp();
				
		//model.addAttribute("coutryList", coutryList);
		model.addAttribute("businessTypeList", businessTypeList);
		
		
		return new ModelAndView("addBusiness");
	}
	
	@Transactional 	@RequestMapping("/saveBusiness")
	public ModelAndView saveBusiness(HttpServletRequest request,Model model,HttpSession session) {
		System.out.println("in saveBusiness controller");
		
		
		String shopName=request.getParameter("shopName");
		String ownerName=request.getParameter("ownerName");
		long businessTypeId=Long.parseLong(request.getParameter("businessTypeId"));
		
		String telephoneNumber=request.getParameter("telephoneNumber");
		String mobileNumber=request.getParameter("mobileNumber");
		String emailId=request.getParameter("emailId");
		
		String address=request.getParameter("address");
		/*String line2=request.getParameter("line2");
		String landmark=request.getParameter("landmark");*/
		
		long areaId=Long.parseLong(request.getParameter("areaId"));
		String gstinNumber=request.getParameter("gstinNumber");
		String creditLimit;
		
		if(request.getParameter("creditLimit").trim().equals(""))
		{
			creditLimit="0";
		}
		else
		{
			creditLimit=request.getParameter("creditLimit");
		}
		
		businessType.setBusinessTypeId(businessTypeId);
				
		/*address.setLine1(line1);
		address.setLine2(line2);
		area.setAreaId(areaId);
		address.setArea(area);
		address.setLandmark(landmark);*/
		
		contact.setEmailId(emailId);
		contact.setMobileNumber(mobileNumber);
		contact.setTelephoneNumber(telephoneNumber);
		
		area.setAreaId(areaId);
		
		businessName.setBusinessType(businessType);
		businessName.setArea(area);
		businessName.setAddress(address);
		businessName.setContact(contact);
		businessName.setOwnerName(ownerName);
		businessName.setShopName(shopName);
		businessName.setGstinNumber(gstinNumber);
		businessName.setCreditLimit(Double.parseDouble(creditLimit));
		businessName.setBusinessAddedDatetime(new Date());
		businessName.setDisableDatetime(new Date());
		businessName.setStatus(false);

		
		businessNameService.saveForWebApp(businessName);
		session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
		return new ModelAndView("redirect:/fetchBusinessNameList");
	}
	
	@Transactional 	@RequestMapping("/checkBusinessDuplicationForSave")
	public @ResponseBody String checkBusinessDuplicationForSave(Model model,HttpServletRequest request,HttpSession session)
	{
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return businessNameService.checkBusinessDuplication(checkText, type, "0");
	}
	
	@Transactional 	@RequestMapping("/checkBusinessDuplicationForUpdate")
	public @ResponseBody String checkBusinessDuplicationForUpdate(Model model,HttpServletRequest request,HttpSession session)
	{
		String businessNameId=request.getParameter("businessNameId");
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		return businessNameService.checkBusinessDuplication(checkText, type, businessNameId);
	}
	
	@Transactional 	@RequestMapping("/updateBusiness")
	public ModelAndView updateBusiness(HttpServletRequest request,Model model,HttpSession session) {
		System.out.println("in updateBusiness controller");
		
		
		BusinessName businessName=businessNameService.fetchBusinessForWebApp(request.getParameter("businessNameId"));
		
		String shopName=request.getParameter("shopName");
		String ownerName=request.getParameter("ownerName");
		long businessTypeId=Long.parseLong(request.getParameter("businessTypeId"));
		
		String telephoneNumber=request.getParameter("telephoneNumber");
		String mobileNumber=request.getParameter("mobileNumber");
		String emailId=request.getParameter("emailId");
		
		String address=request.getParameter("address");/*
		String line2=request.getParameter("line2");
		String landmark=request.getParameter("landmark");*/
		
		long areaId=Long.parseLong(request.getParameter("areaId"));
		String gstinNumber=request.getParameter("gstinNumber");
		String creditLimit;
		
		if(request.getParameter("creditLimit").trim().equals(""))
		{
			creditLimit="0";
		}
		else
		{
			creditLimit=request.getParameter("creditLimit");
		}
		
		businessType.setBusinessTypeId(businessTypeId);
				
		/*address.setAddressId(businessName.getAddress().getAddressId());
		address.setLine1(line1);
		address.setLine2(line2);
		
		address.setArea(area);
		address.setLandmark(landmark);*/
		
		contact.setContactId(businessName.getContact().getContactId());
		contact.setEmailId(emailId);
		contact.setMobileNumber(mobileNumber);
		contact.setTelephoneNumber(telephoneNumber);
		
		area.setAreaId(areaId);
		
		businessName.setBusinessType(businessType);
		businessName.setAddress(address);
		businessName.setArea(area);
		businessName.setContact(contact);
		businessName.setOwnerName(ownerName);
		businessName.setShopName(shopName);
		businessName.setGstinNumber(gstinNumber);
		businessName.setCreditLimit(Double.parseDouble(creditLimit));
		businessName.setBusinessAddedDatetime(businessName.getBusinessAddedDatetime());
		businessName.setBusinessUpdatedDatetime(new Date());
		businessName.setDisableDatetime(businessName.getDisableDatetime());
		businessName.setStatus(businessName.isStatus());
		businessNameService.updateForWebApp(businessName);
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchBusinessNameList");
	}
	
	@Transactional 	@RequestMapping("/fetchBusiness")
	public ModelAndView fetchBusiness(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Update Business");
	
		
		businessName=businessNameService.fetchBusinessForWebApp(request.getParameter("businessNameId"));
		model.addAttribute("businessName", businessName);
		
		/*this.country=countryService.fetchIndiaCountry();
		List<State> stateList=stateService.fetchStateByCountryIdforwebapp(this.country.getCountryId());*/
		
		List<City> cityList=new ArrayList<>();
		List<CompanyCities> companyCities=companyService.fetchCompanyCities(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		for(CompanyCities companyCity: companyCities){
			cityList.add(companyCity.getCity());
		}
		model.addAttribute("cityList", cityList);
		
		//List<Country> coutryList = countryService.fetchCountryListForWebApp();
		List<BusinessType> businessTypeList=businessTypeService.fetchBusinessTypeListForWebApp();
		
		//model.addAttribute("coutryList", coutryList);
		model.addAttribute("businessTypeList", businessTypeList);
		
		return new ModelAndView("updateBusiness");
	}
	
	@Transactional 	@RequestMapping("/fetchBusinessNameList")
	public ModelAndView fetchBusinessNameList(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Business List");
		
		
		List<BusinessNameList> businessNameList=businessNameService.fetchBusinessNameList();
		model.addAttribute("businessNameList", businessNameList);
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		return new ModelAndView("ManageBusiness");
	}
	
	
	//sendSMSTOShops
	@Transactional 	@RequestMapping("/sendSMSTOShops")
	public @ResponseBody String sendSMSTOShops(HttpServletRequest request) {
		
		try {
			String shopsIds=request.getParameter("shopsIds");		
			String smsText=request.getParameter("smsText");	
			String mobileNumber=request.getParameter("mobileNumber");
			businessNameService.sendSMSTOShops(shopsIds, smsText,mobileNumber);
			
			return "Success";
		} catch (Exception e) {
			System.out.println(e.toString());
			return "Failed";
		}
	}
	
	@Transactional 	@RequestMapping("/fetchBusinessNameForReport")
	public ModelAndView fetchBusinessNameForReport(Model model,HttpSession session,HttpServletRequest request) {
		
		model.addAttribute("pageName", "Business Report");
		
		
		
		    String range=request.getParameter("range");
			String startDate=request.getParameter("startDate");
			String endDate=request.getParameter("endDate");	
		  
		CustomerReportView customerReportView=businessNameService.fetchBusinessNameForReport(range, startDate, endDate);
		model.addAttribute("customerReportView", customerReportView);
		
		
		String partUrl="";
		
		if(range.equals("range"))
		{
			partUrl="range=range&startDate="+startDate+"&endDate="+endDate;
		}
		else if(range.equals("today"))
		{
			partUrl="range=today";
		}
		else if(range.equals("yesterday"))
		{
			partUrl="range=yesterday";
		}
		else if(range.equals("last7days"))
		{
			partUrl="range=last7days";
		}
		else if(range.equals("last3Months"))
		{
			partUrl="range=last6month";
		}
		else if(range.equals("lastMonth"))
		{
			partUrl="range=lastMonth";
		}
		else if(range.equals("currentMonth"))
		{
			partUrl="range=currentMonth";
		}
		else if(range.equals("pickDate"))
		{
			partUrl="range=pickDate&startDate="+startDate;
		}
		else if(range.equals("viewAll"))
		{
			partUrl="range=viewAll";
		}
		model.addAttribute("partUrl", partUrl);
		
		return new ModelAndView("CustomerReport");
	}
	
	@Transactional 	@RequestMapping("/disableBusiness")
	public ModelAndView disableEmployee(Model model,HttpSession session,HttpServletRequest request) {
	
		
		  businessName=businessNameService.fetchBusinessForWebApp(request.getParameter("businessNameId"));
		  if(businessName.isStatus())
		  {
			  businessName.setStatus(false);
		  }
		  else
		  {
			  businessName.setStatus(true);
		  }
		  businessName.setDisableDatetime(new Date());
		  businessNameService.updateForWebApp(businessName);
		  
		return new ModelAndView("redirect:/fetchBusinessNameList");
	}
}
