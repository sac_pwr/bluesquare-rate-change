/**
 * 
 */
package com.bluesquare.rc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.models.ReIssueOrderReport;
import com.bluesquare.rc.models.ReturnItemReportMain;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.ReturnOrderService;

/**
 * @author aNKIT
 *
 */
@Controller
public class ReturnOrderControllerForWeb {

	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Transactional 	@RequestMapping("/returnOrderReport")
	public ModelAndView returnOrderReport(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Return Order Report");
		
		
			String filter=request.getParameter("range");
			String startDate=request.getParameter("startDate");
			String endDate=request.getParameter("endDate");
		  
		List<ReturnItemReportMain> returnItemReportMainList= returnOrderService.fetchReturnOrderProductList(filter,startDate,endDate);
		model.addAttribute("returnItemReportMainList", returnItemReportMainList);
		
		return new ModelAndView("ReturnItemReport");
	}
	
	@Transactional 	@RequestMapping("/returnOrderDetailsByReturnOrderId")
	public ModelAndView returnOrderDetailsByReturnOrderId(HttpServletRequest request, Model model,HttpSession session) {
		model.addAttribute("pageName", "Return Order Details");
		
		
		List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderService.fetchReturnOrderProductDetailsByReturnOrderProductId(request.getParameter("returnOrderId"));
		model.addAttribute("employeeDetailsSM", employeeDetailsService.getEmployeeDetailsByemployeeId(returnOrderProductDetailsList.get(0).getReturnOrderProduct().getOrderDetails().getEmployeeSM().getEmployeeId()));
		model.addAttribute("returnOrderProductDetailsList", returnOrderProductDetailsList);
		
		return new ModelAndView("ReturnItemDetails");
	}
	
	@Transactional 	@RequestMapping("/reIssueOrderReport")
	public ModelAndView reIssueOrderReport(Model model,HttpSession session) {
		model.addAttribute("pageName", "ReIssue Report");
		
		
		List<ReIssueOrderReport> reIssueOrderReportList= returnOrderService.fetchReIssueOrderReportList();
		model.addAttribute("reIssueOrderReportList", reIssueOrderReportList);
		
		return new ModelAndView("ReIssueItemReport");
	}
	
	@Transactional 	@RequestMapping("/reIssueOrderDetailsByReturnOrderId")
	public ModelAndView rIssueOrderDetailsByReturnOrderId(HttpServletRequest request, Model model,HttpSession session) {
		model.addAttribute("pageName", "ReIssue Order Details");
		
		
		List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=orderDetailsService.fetchReIssueOrderProductDetailsListByReIssueOrderDetailsId(Long.parseLong(request.getParameter("reIssueOrderId")));
		model.addAttribute("reIssueOrderProductDetailsList", reIssueOrderProductDetailsList);
		
		return new ModelAndView("ReIssueItemDetails");
	}
}
