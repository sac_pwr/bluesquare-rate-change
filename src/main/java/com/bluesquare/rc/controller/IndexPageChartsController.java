/**
 * 
 */
package com.bluesquare.rc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bluesquare.rc.models.ChartDetailsResponse;
import com.bluesquare.rc.models.CollectionReportMain;
import com.bluesquare.rc.models.ProductReportView;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.ReturnOrderService;

/**
 * @author aNKIT
 *
 */
@Controller
public class IndexPageChartsController {

	@Autowired
	PaymentService paymentService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	OrderDetailsService orderDetailsService;  
	
	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	InventoryService inventoryService;
	
	/*
	    Range
		Last 6 Month
		Last 3 Month
		last Month
		Current Month
	 */
	
	@Transactional 	@RequestMapping("/fetchTopChart")
	public @ResponseBody ChartDetailsResponse fetchTopChart(Model model ,HttpServletRequest request)
	{

		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		
		double totalSales=orderDetailsService.fetchTotalSaleForIndexPage(range, startDate, endDate);
		double totalAmountInvestInMarket=orderDetailsService.fetchTotalAmountInvestInMarketForIndexPage(range, startDate, endDate);
		double totalValueOfCurrentInventory=inventoryService.fetchTotalValueOfCurrrentInventory();
		long productUnderThresholdCount=inventoryService.fetchProductUnderThresholdCount();
		
		ChartDetailsResponse chartDetailsResponse=new ChartDetailsResponse(totalSales, 
																			totalAmountInvestInMarket, 
																			totalValueOfCurrentInventory, 
																			productUnderThresholdCount);
		return chartDetailsResponse;
	}
	
	@Transactional 	@RequestMapping("/fetchTotalAmountChart")
	public @ResponseBody ChartDetailsResponse fetchTotalAmountChart(Model model,HttpServletRequest request)
	{
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		
		CollectionReportMain collectionReportMain=paymentService.getCollectionReportDetails(startDate, endDate, range);
		ChartDetailsResponse chartDetailsResponse=new ChartDetailsResponse( collectionReportMain.getAmountToBePaid()-collectionReportMain.getPendingAmount(), 
																			collectionReportMain.getPendingAmount());
		return chartDetailsResponse;
	}
	
	@Transactional 	@RequestMapping("/fetchTopFiveProductChart")
	public @ResponseBody ChartDetailsResponse fetchTopFiveProductChart(Model model,HttpServletRequest request) {
		System.out.println("in fetchChartDetails controller");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		
		List<ProductReportView> productReportViews=productService.fetchProductListForReport(range, startDate, endDate, "");
		
		String productName1="";
		float productPercentage1=0;
		String productName2="";
		float productPercentage2=0;
		String productName3="";
		float productPercentage3=0;
		String productName4="";
		float productPercentage4=0;
		String productName5="";
		float productPercentage5=0;
		
		double totalAmount=0;
		int count=0;
		for(ProductReportView productReportView: productReportViews)
		{
			if(count==5)
			{
				break;
			}
			totalAmount+=productReportView.getAmountWithTax();
			count++;			
		}
		
		count=0;
		for(ProductReportView productReportView: productReportViews)
		{
			count++;
			
			if(count==1)
			{
				productName1=productReportView.getProduct().getProductName(); 
				productPercentage1=(float)( productReportView.getAmountWithTax() / totalAmount )*100;
			}
			else if(count==2)
			{
				productName2=productReportView.getProduct().getProductName(); 
				productPercentage2=(float)( productReportView.getAmountWithTax() / totalAmount )*100;
			}
			else if(count==3)
			{
				productName3=productReportView.getProduct().getProductName(); 
				productPercentage3=(float)( productReportView.getAmountWithTax() / totalAmount )*100;
			}
			else if(count==4)
			{
				productName4=productReportView.getProduct().getProductName(); 
				productPercentage4=(float)( productReportView.getAmountWithTax() / totalAmount )*100;
			}
			else if(count==5)
			{
				productName5=productReportView.getProduct().getProductName(); 
				productPercentage5=(float)( productReportView.getAmountWithTax() / totalAmount )*100;
			}
		}
				
		ChartDetailsResponse chartDetailsResponse=new ChartDetailsResponse();
		chartDetailsResponse.setProductNameChartDetailsResponse(			productName1, 
																			productPercentage1, 
																			productName2, 
																			productPercentage2, 
																			productName3, 
																			productPercentage3, 
																			productName4, 
																			productPercentage4, 
																			productName5, 
																			productPercentage5);
		
		return chartDetailsResponse;
	}
	
	@Transactional 	@RequestMapping("/fetchTopFiveSallersChart")
	public @ResponseBody ChartDetailsResponse fetchTopFiveSallersChart(Model model,HttpServletRequest request) {

		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		
		ChartDetailsResponse chartDetailsResp=orderDetailsService.fetchTopFiveSalesManByIssuedSale(range, startDate, endDate);
		
		String salesManName1=chartDetailsResp.getSalesManName1();
		float salesManPercentage1=chartDetailsResp.getSalesManPercentage1();
		String salesManName2=chartDetailsResp.getSalesManName2();
		float salesManPercentage2=chartDetailsResp.getSalesManPercentage2();
		String salesManName3=chartDetailsResp.getSalesManName3();
		float salesManPercentage3=chartDetailsResp.getSalesManPercentage3();
		String salesManName4=chartDetailsResp.getSalesManName4();
		float salesManPercentage4=chartDetailsResp.getSalesManPercentage4();
		String salesManName5=chartDetailsResp.getSalesManName5();
		float salesManPercentage5=chartDetailsResp.getSalesManPercentage5();
		
		ChartDetailsResponse chartDetailsResponse=new ChartDetailsResponse();
	chartDetailsResponse.setSalesManChartDetailsResponse(salesManName1, 
														salesManPercentage1, 
														salesManName2, 
														salesManPercentage2, 
														salesManName3, 
														salesManPercentage3, 
														salesManName4, 
														salesManPercentage4, 
														salesManName5, 
														salesManPercentage5);

		return chartDetailsResponse;
	}
	
	@Transactional 	@RequestMapping("/fetchTopFiveReturnReportChart")
	public @ResponseBody ChartDetailsResponse fetchTopFiveReturnReportChart(Model model,HttpServletRequest request) {
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		
		ChartDetailsResponse chartDetailsRespForReturnOrder=returnOrderService.fetchTopReturnOrderProducts(range, startDate, endDate);
		
		String returnProductName1=chartDetailsRespForReturnOrder.getReturnProductName1();
		float returnProductPercentage1=chartDetailsRespForReturnOrder.getReturnProductPercentage1();
		String returnProductName2=chartDetailsRespForReturnOrder.getReturnProductName2();
		float returnProductPercentage2=chartDetailsRespForReturnOrder.getReturnProductPercentage2();
		String returnProductName3=chartDetailsRespForReturnOrder.getReturnProductName3();
		float returnProductPercentage3=chartDetailsRespForReturnOrder.getReturnProductPercentage3();
		String returnProductName4=chartDetailsRespForReturnOrder.getReturnProductName4();
		float returnProductPercentage4=chartDetailsRespForReturnOrder.getReturnProductPercentage4();
		String returnProductName5=chartDetailsRespForReturnOrder.getReturnProductName5();
		float returnProductPercentage5=chartDetailsRespForReturnOrder.getReturnProductPercentage5();
		ChartDetailsResponse chartDetailsResponse=new ChartDetailsResponse();																	
		chartDetailsResponse.setReturnProductChartDetailsResponse(returnProductName1, 
																returnProductPercentage1, 
																returnProductName2, 
																returnProductPercentage2, 
																returnProductName3, 
																returnProductPercentage3, 
																returnProductName4, 
																returnProductPercentage4, 
																returnProductName5, 
																returnProductPercentage5);		
		return chartDetailsResponse;
	}
	
}
