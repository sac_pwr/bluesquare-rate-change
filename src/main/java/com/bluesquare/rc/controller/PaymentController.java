package com.bluesquare.rc.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.models.ChequePaymentReportModel;
import com.bluesquare.rc.models.CollectionReportMain;
import com.bluesquare.rc.models.EditOrderDetailsPaymentModel;
import com.bluesquare.rc.models.LedgerPaymentView;
import com.bluesquare.rc.models.PaymentListReport;
import com.bluesquare.rc.models.PaymentPendingList;
import com.bluesquare.rc.models.PaymentReportModel;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.CounterOrderService;
import com.bluesquare.rc.service.LedgerService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.SendSMS;

@Controller
public class PaymentController {
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	LedgerService ledgerService;
	
	@Autowired
	CompanyService companyService;
	
	@Autowired
	CounterOrderService counterOrderService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	@Transactional 	@RequestMapping("/fetchPaymentPendingList")
	public ModelAndView fetchPaymentPendingList(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Payment Pending List");
		
	
		
		List<PaymentPendingList> paymentPendingLists=paymentService.fetchPaymentPendingList();
		
		model.addAttribute("paymentPendingLists", paymentPendingLists);
		
		return new ModelAndView("payment");
	}
	

	@Transactional 	@RequestMapping("/fetchLedgerPaymentView")
	public ModelAndView fetchLedgerPaymentView(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Ledger");
		
		
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		List<LedgerPaymentView> ledgerPaymentViews=ledgerService.fetchLedgerPaymentView(startDate, endDate, range);
		model.addAttribute("ledgerPaymentViews", ledgerPaymentViews);
		
		Company company=companyService.fetchCompanyByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		model.addAttribute("balance", company.getBalance());
		
		return new ModelAndView("ledger");
	}
	
	@Transactional 	@RequestMapping("/getCollectionReportDetails")
	public ModelAndView getCollectionReportDetails(Model model,HttpServletRequest request,HttpSession session){
		
		
		  model.addAttribute("pageName", "Collection Report");
		  
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		CollectionReportMain collectionReportMain=paymentService.getCollectionReportDetails(startDate, endDate, range);
		model.addAttribute("collectionReportMain", collectionReportMain);
		
		return new ModelAndView("CollectionReport");
	}
	
	@Transactional 	@RequestMapping("/fetchPaymentListByOrderId")
	public @ResponseBody List<PaymentListReport> fetchPaymentListByOrderIdAndReOrderId(Model model,HttpServletRequest request,HttpSession session){
				
		List<PaymentListReport> paymentList=paymentService.fetchPaymentListByOrderDetailsIdForCollectionReport(request.getParameter("orderId"));
		
		return paymentList;
	}
	
	@Transactional 	@RequestMapping("/paymentReport")
	public ModelAndView paymentReport(Model model,HttpServletRequest request,HttpSession session){
		
		  model.addAttribute("pageName", "Payment Report");
		  
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		List<PaymentReportModel> paymentReportModelList=paymentService.fetchPaymentReportModelList(startDate, endDate, range);
				
		model.addAttribute("paymentReportModelList", paymentReportModelList);
		
		double totalAmountPaid=0;
		if(paymentReportModelList!=null){
			for(PaymentReportModel paymentReportModel: paymentReportModelList){
				totalAmountPaid+=paymentReportModel.getAmountPaid();
			}
			model.addAttribute("noOfBills", paymentReportModelList.size());
		}else{
			model.addAttribute("noOfBills", 0);
		}			
		
		model.addAttribute("totalAmountPaid", totalAmountPaid);
		
		
		return new ModelAndView("paymentReport");
	}
	
	@Transactional 	@RequestMapping("/editOrderDetailsPayment")
	public ModelAndView editOrderDetailsPayment(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Edit Payment");
		
		long paymentId=Long.parseLong(request.getParameter("paymentId")); 
		String orderId=request.getParameter("orderId");
		EditOrderDetailsPaymentModel editOrderDetailsPaymentModel; 
		
		if(orderId.contains("CORD")){
			List<PaymentCounter> paymentCounterList=counterOrderService.fetchPaymentCounterListByCounterOrderId(orderId);
			PaymentCounter paymentCounter=counterOrderService.fetchPaymentCounterByPaymentCounterId(paymentId);
			
			double totalAmount=paymentCounter.getCounterOrder().getTotalAmountWithTax(); 
			
			double amountPaid=0;
			for(PaymentCounter paymentCounter2: paymentCounterList){
				amountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
			}
			
			double totalAmountPaid=amountPaid;//paymentCounterList.get(0).getTotalAmountPaid(); 
			double balanceAmount=totalAmount-totalAmountPaid;
			
			String fullPartialStatus;
			if(totalAmount==totalAmountPaid){
				fullPartialStatus=Constants.FULL_PAY_STATUS;
			}else{
				fullPartialStatus=Constants.PARTIAL_PAY_STATUS;
			}
			editOrderDetailsPaymentModel=new EditOrderDetailsPaymentModel(
																		(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
																		orderId, 
																		paymentCounter.getPaymentCounterId(),
																		(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerMobileNumber():paymentCounter.getCounterOrder().getBusinessName().getContact().getMobileNumber(), 
																		(paymentCounter.getCounterOrder().getBusinessName()==null)?"":paymentCounter.getCounterOrder().getBusinessName().getAddress(), 
																		totalAmount, 
																		totalAmountPaid, 
																		balanceAmount, 
																		fullPartialStatus, 
																		paymentCounter.getPayType(), 
																		paymentCounter.getBankName(), 
																		(paymentCounter.getCurrentAmountRefund()==0)?paymentCounter.getCurrentAmountPaid():paymentCounter.getCurrentAmountRefund(), 
																		balanceAmount, 
																		paymentCounter.getChequeNumber(), 
																		(paymentCounter.getChequeDate()==null)?"":new SimpleDateFormat("yyyy-MM-dd").format(paymentCounter.getChequeDate()),
																		(paymentCounter.getDueDate()==null)?"":new SimpleDateFormat("yyyy-MM-dd").format(paymentCounter.getDueDate()),
																		(paymentCounter.getCurrentAmountRefund()==0)?"Paid":"Refund");
			
		}else{
			List<Payment> paymentList=paymentService.fetchPaymentListByOrderDetailsId(orderId);
			Payment payment=paymentService.fetchPaymentBYPaymentId(paymentId);
			
			double totalAmount=payment.getOrderDetails().getIssuedTotalAmountWithTax();
			double balanceAmount=paymentList.get(0).getDueAmount();
			double totalAmountPaid=totalAmount-balanceAmount;
			
			String fullPartialStatus;
			if(totalAmount==totalAmountPaid){
				fullPartialStatus=Constants.FULL_PAY_STATUS;
			}else{
				fullPartialStatus=Constants.PARTIAL_PAY_STATUS;
			}
			editOrderDetailsPaymentModel=new EditOrderDetailsPaymentModel(
																		payment.getOrderDetails().getBusinessName().getShopName(), 
																		orderId,
																		payment.getPaymentId(),
																		payment.getOrderDetails().getBusinessName().getContact().getMobileNumber(), 
																		payment.getOrderDetails().getBusinessName().getAddress(), 
																		totalAmount, 
																		totalAmountPaid, 
																		balanceAmount,  
																		fullPartialStatus, 
																		payment.getPayType(), 
																		payment.getBankName(), 
																		payment.getPaidAmount(), 
																		balanceAmount, 
																		payment.getChequeNumber(), 
																		(payment.getChequeDate()==null)?"":new SimpleDateFormat("yyyy-MM-dd").format(payment.getChequeDate()),
																		(payment.getDueDate()==null)?"":new SimpleDateFormat("yyyy-MM-dd").format(payment.getDueDate()),
																		"Paid");
			
		}
		
		model.addAttribute("payment", editOrderDetailsPaymentModel);
		return new ModelAndView("editPayment");
	}
	
	@Transactional 	@RequestMapping("/deleteOrderDetailsPayment")
	public ModelAndView deleteOrderDetailsPayment(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Edit Payment");
		
		long paymentId=Long.parseLong(request.getParameter("paymentId")); 
		String orderId=request.getParameter("orderId"); 
		
		if(orderId.contains("CORD")){
			counterOrderService.deletePayment(paymentId);
		}else{
			paymentService.deletePayment(paymentId);
		}
		
		return new ModelAndView("redirect:/paymentReport?range=currentMonth");
	}
	
	@Transactional 	@RequestMapping("/updateOrderDetailsPayment")
	public ModelAndView updateOrderDetailsPayment(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Edit Payment");
		
		long paymentId=Long.parseLong(request.getParameter("paymentId"));  
		String orderId=request.getParameter("orderId");
		String cashCheckStatus=request.getParameter("cashCheckStatus");
		String fullPartialPayment=request.getParameter("fullPartialPayment");
		String dueDate=request.getParameter("dueDate");
		String bankName=request.getParameter("bankName");
		String checkNo=request.getParameter("checkNo");
		String checkDate=request.getParameter("checkDate");
		double paidAmount=Double.parseDouble(request.getParameter("paidAmount"));		
		
		if(orderId.contains("CORD")){
			
			 PaymentCounter paymentCounter=counterOrderService.fetchPaymentCounterByPaymentCounterId(paymentId);	
			 CounterOrder counterOrder=counterOrderService.fetchCounterOrder(orderId);
		 		
		 	if(cashCheckStatus.equals(Constants.CASH_PAY_STATUS)){
				paymentCounter.setBankName(null);
				paymentCounter.setChequeDate(null);
				paymentCounter.setChequeNumber(null);	
				paymentCounter.setPayType(Constants.CASH_PAY_STATUS);
			}else{
				paymentCounter.setBankName(bankName);
				try {
				paymentCounter.setChequeDate(new SimpleDateFormat("yyyy-MM-dd").parse(checkDate));
				} catch (ParseException e) {}
				paymentCounter.setChequeNumber(checkNo);
				paymentCounter.setPayType(Constants.CHEQUE_PAY_STATUS);
			}
			 
			if(fullPartialPayment.equals(Constants.FULL_PAY_STATUS)){
				paymentCounter.setCurrentAmountPaid(paidAmount);
				paymentCounter.setDueDate(new Date());
				counterOrder.setPayStatus(true);
			}else{
				paymentCounter.setCurrentAmountPaid(paidAmount);
				try {
					paymentCounter.setDueDate(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
					counterOrder.setPaymentDueDate(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
				} catch (ParseException e) {}
				counterOrder.setPayStatus(false);
			}
			paymentCounter.setCounterOrder(counterOrder);
			counterOrderService.updatePayment(paymentCounter);
			
		}else{
			Payment payment=paymentService.fetchPaymentBYPaymentId(paymentId);
			OrderDetails orderDetails=payment.getOrderDetails();
			
			payment.setPaidAmount(paidAmount);			
			
			if(cashCheckStatus.equals(Constants.CASH_PAY_STATUS))
			{
				payment.setBankName(null);
				payment.setChequeDate(null);
				payment.setChequeNumber(null);	
				payment.setPayType(Constants.CASH_PAY_STATUS);
			}
			else
			{
				payment.setBankName(bankName);
				try {
				payment.setChequeDate(new SimpleDateFormat("yyyy-MM-dd").parse(checkDate));
				} catch (ParseException e) {}
				payment.setChequeNumber(checkNo);
				payment.setPayType(Constants.CHEQUE_PAY_STATUS);
			}
			
			if(fullPartialPayment.equals(Constants.FULL_PAY_STATUS))
			{
				payment.setDueDate(new Date());
				orderDetails.setPayStatus(true);
			}
			else
			{
				try {
					payment.setDueDate(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
					orderDetails.setOrderDetailsPaymentTakeDatetime(new SimpleDateFormat("yyyy-MM-dd").parse(dueDate));
				} catch (ParseException e) {}
				orderDetails.setPayStatus(false);
			}
			
			payment.setOrderDetails(orderDetails);
			paymentService.updatePayment(payment);		
		}		
		
		return new ModelAndView("redirect:/paymentReport?range=currentMonth");
	}
	
	@Transactional 	@RequestMapping("/chequeReport")
	public ModelAndView chequeReport(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Cheque Report");
		  
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String range=request.getParameter("range");
		
		List<ChequePaymentReportModel> chequePaymentReportModelList=paymentService.fetchChequePaymentReportModelList(startDate, endDate, range);
		model.addAttribute("chequePaymentReportModelList", chequePaymentReportModelList);
		
		long totalNoOfCheques=0,totalNoCleared=0,totalNoBounced=0;
		if(chequePaymentReportModelList!=null){
			for(ChequePaymentReportModel chequePaymentReportModel: chequePaymentReportModelList){
				totalNoOfCheques+=1;
				if(chequePaymentReportModel.isCheckClearStatus()){
					totalNoCleared+=1;
				}else{
					totalNoBounced+=1;
				}
			}
		}
		
		model.addAttribute("totalNoOfCheques",totalNoOfCheques );
		model.addAttribute("totalNoCleared",totalNoCleared );
		model.addAttribute("totalNoBounced", totalNoBounced);
		
		
		session.setAttribute("chequeReportRange", range);
		session.setAttribute("chequeReportStartDate", startDate);
		session.setAttribute("chequeReportEndDate", range);
		
		return new ModelAndView("chequePaymentReport");
	}
	
	@Transactional 	@RequestMapping("/defineChequeBounced")
	public ModelAndView defineChequeBounced(Model model,HttpServletRequest request,HttpSession session){
		
		model.addAttribute("pageName", "Edit Payment");
		
		long paymentId=Long.parseLong(request.getParameter("paymentId")); 
		String orderId=request.getParameter("orderId"); 
		
		if(orderId.contains("CORD")){
			counterOrderService.defineChequeBounced(paymentId);
		}else{
			paymentService.defineChequeBounced(paymentId);
		}
		
		String startDate=(String)session.getAttribute("chequeReportStartDate");
		String endDate=(String)session.getAttribute("chequeReportEndDate");
		String range=(String)session.getAttribute("chequeReportRange");
		
		return new ModelAndView("redirect:/chequeReport?range="+range+"&startDate="+startDate+"&endDate="+endDate);
	}
	
	@Transactional @RequestMapping("/sendSmsChequeReport")
	public @ResponseBody String sendSmsChequeReport(Model model,HttpServletRequest request,HttpSession session){
		
		String mobNo=request.getParameter("mobNo");
		String sms=request.getParameter("sms");
		boolean isSend=SendSMS.sendSMS(Long.parseLong(mobNo), sms);
		
		if(isSend)
			return "Success";
		else
			return "Failed";
	}
	
}
