package com.bluesquare.rc.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.service.DepartmentService;
import com.bluesquare.rc.utils.Constants;

@Controller
public class DepartmentController {

	@Autowired
	Department department;

	@Autowired
	DepartmentService departmentService;

	@Transactional 	@RequestMapping("/fetchDepartmentList")
	public ModelAndView fetchfDepartmentList(Model model,HttpSession session) {
		System.out.println("in fetchDepartmentList controller");
		model.addAttribute("pageName", "Department List");
				
		
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();
		model.addAttribute("departmentList", DepartmentList);

		return new ModelAndView("addDepartment");
	}

	@Transactional 	@RequestMapping("/fetchDepartment")
	public @ResponseBody Department fetchDepartment(Model model, HttpServletRequest request) {
		System.out.println("in fetchBrand controller");
		long DepartmentId = Long.parseLong(request.getParameter("departmentId"));
		department = departmentService.fetchDepartmentForWebApp(DepartmentId);

		return department;
	}
	/*@Transactional
	@Transactional 	@RequestMapping("/saveDept")
	public ModelAndView saveDept(HttpServletRequest request, Model model,HttpSession session) {
	
		this.department.setDepartmentAddedDatetime(new Date());
		this.department.setDetails("Check");
		this.department.setName("Checking");
		this.department.setShortNameForEmployeeId("CH");		
		departmentService.saveForWebApp(this.department);
		
		//System.out.println("in saveDepartment : "+(1/0));
		
		return new ModelAndView("redirect:/fetchDepartmentList");
	
	}*/

	// saveCountry	
	@Transactional 	@RequestMapping("/saveDepartment")
	public ModelAndView saveBrandForWeb(HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("in saveDepartment");
		
		this.department.setDepartmentId(Long.parseLong(request.getParameter("departmentId")));
		this.department.setName(request.getParameter("departmentName"));
		this.department.setShortNameForEmployeeId(request.getParameter("departmentshortName"));

		
		model.addAttribute("saveMsg", "");

		department.setName((Character.toString(department.getName().charAt(0)).toUpperCase()
				+ department.getName().substring(1)));

		

		boolean flag = false;
		List<Department> DepartmentList = departmentService.fetchDepartmentListForWebApp();

		if (department.getDepartmentId() == 0) {
			
			if (DepartmentList != null) {
				for (Department DepartmentFromDb : DepartmentList) {
					if (DepartmentFromDb.getName().trim().toUpperCase().equals(department.getName().trim().toUpperCase()) || 
							DepartmentFromDb.getShortNameForEmployeeId().trim().toUpperCase().equals(department.getShortNameForEmployeeId().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			if (!flag) {
				department.setDepartmentAddedDatetime(new Date());
				departmentService.saveForWebApp(department);
				model.addAttribute("saveMsg", Constants.SAVE_SUCCESS);
				return new ModelAndView("redirect:/fetchDepartmentList");
			}
		}
		if (department.getDepartmentId() != 0) {
			
			flag = false;
			List<Department> DepartmentList2 = departmentService.fetchDepartmentListForWebApp();
			if (DepartmentList != null) {
				for (Department DepartmentFromDb : DepartmentList2) {
					if ( (DepartmentFromDb.getName().trim().toUpperCase().equals(department.getName().trim().toUpperCase()) ||
						DepartmentFromDb.getShortNameForEmployeeId().trim().toUpperCase().equals(department.getShortNameForEmployeeId().trim().toUpperCase()))
						&& 	DepartmentFromDb.getDepartmentId()!=department.getDepartmentId()) {
						flag = true;
						break;
					}
				}
			}
			
			if (!flag) {
				System.out.println("Moving request for Update");
				model.addAttribute("saveMsg", Constants.UPDATE_SUCCESS);
				return updateDepartmentForWeb(department, model,session);
			}
		}

		model.addAttribute("saveMsg", Constants.ALREADY_EXIST);
		return new ModelAndView("redirect:/fetchDepartmentList");

	}

	@Transactional 	@RequestMapping("/updateDepartment")
	public ModelAndView updateDepartmentForWeb(@ModelAttribute Department department, Model model,HttpSession session) {

		System.out.println("in updateDepartment");

		if (department.getDepartmentId() == 0 || department.getName() == null
				|| department.getName().equals("")) {
			System.out.println("con not update because 0 city not available");
			return new ModelAndView("redirect:/fetchDepartmentList");
		}

		this.department = departmentService.fetchDepartmentForWebApp(department.getDepartmentId());
		department.setDepartmentAddedDatetime(this.department.getDepartmentAddedDatetime());
		departmentService.updateForWebApp(department);

		return new ModelAndView("redirect:/fetchDepartmentList");

	}
}
