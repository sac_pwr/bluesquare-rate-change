/**
 * 
 */
package com.bluesquare.rc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.OrderDetailsForWeb;
import com.bluesquare.rc.models.OrderDetailsList;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;
import com.bluesquare.rc.models.ReturnOrderFromDeliveryBoyReport;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.BrandAndCategoryRequest;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.OrderDetailResponse;
import com.bluesquare.rc.rest.models.OrderIssueRequest;
import com.bluesquare.rc.rest.models.OrderProductIssueListForIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderProductIssueReportRequest;
import com.bluesquare.rc.rest.models.OrderProductIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderReIssueRequest;
import com.bluesquare.rc.rest.models.ProductAddInventory;
import com.bluesquare.rc.rest.models.ReIssueOrderDetailsReportResponse;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeRequest;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeResponse;
import com.bluesquare.rc.rest.models.ReturnOrderProductDetailsReportResponse;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.ReturnOrderService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.EmailSender;
import com.bluesquare.rc.utils.InvoiceGenerator;
import com.bluesquare.rc.utils.JsonWebToken;

/**
 * @author aNKIT
 *
 */
@Controller
public class GKAdminController {

	@Autowired
	OrderDetailsService orderDetailsService;

	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	BrandService brandService;
	
	@Autowired
	CategoriesService categoryService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;

	@Transactional 	@RequestMapping("/GKOrderDetailsTodayList")
	public ModelAndView OrderDetailsTodaysList(HttpServletRequest request,Model model,HttpSession session) {
		model.addAttribute("pageName", "Order Details");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		 
		
		OrderDetailsList orderDetailsList = new OrderDetailsList();
		com.bluesquare.rc.rest.models.OrderDetailsList orderDetailsList2=new com.bluesquare.rc.rest.models.OrderDetailsList();
		try {
			session.setAttribute("orderDetailListForIssue", null);
			session.setAttribute("orderProductDetailListOld", null);
			long employeeId = ((EmployeeDetails)session.getAttribute("employeeDetails")).getEmployee().getEmployeeId();

			orderDetailsList2 = orderDetailsService.fetchOrderListByAreaId( employeeId);
			if (orderDetailsList2.getOrderDetailsList() == null) {
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Orders Today");
			} else {
				List<OrderDetailsForWeb> orderDetailsForWebList=new ArrayList<>();
				for(OrderDetails orderDetails : orderDetailsList2.getOrderDetailsList())
				{
					orderDetailsForWebList.add(new OrderDetailsForWeb(
							orderDetails.getOrderId(), 
							orderDetails.getTotalAmount(), 
							orderDetails.getTotalAmountWithTax(), 
							orderDetails.getTotalQuantity(), 
							employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()), 
							orderDetails.getBusinessName(), 
							orderDetails.getPaymentPeriodDays(),
							orderDetails.getOrderStatus(), 
							orderDetails.isPayStatus(), 
							orderDetails.getIssuedTotalAmount(), 
							orderDetails.getIssuedTotalAmountWithTax(), 
							orderDetails.getIssuedTotalQuantity(), 
							orderDetails.getOrderDetailsPaymentTakeDatetime(), 
							orderDetails.getOrderDetailsAddedDatetime(), 
							orderDetails.getCancelDate(), 
							orderDetails.getIssueDate(), 
							orderDetails.getPackedDate(), 
							orderDetails.getDeliveryDate(), 
							orderDetails.getConfirmDate(), 
							orderDetails.getConfirmTotalAmount(), 
							orderDetails.getConfirmTotalAmountWithTax(), 
							orderDetails.getConfirmTotalQuantity()));
				}
				orderDetailsList.setOrderDetailsList(orderDetailsForWebList);
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
			}
		} catch (Exception e) {
			orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
			orderDetailsList.setErrorMsg("Something Went Wrong ");
		}
		orderDetailsList.setAreaList(orderDetailsList2.getAreaList());
		orderDetailsList.setPageName("Current");
		model.addAttribute("orderDetailsList", orderDetailsList);
		
		session.setAttribute("lastUrl", "GKOrderDetailsTodayList");
		
		return new ModelAndView("gkOrderList");
	}

	@Transactional 	@GetMapping("/GKOrderDetailsPendingList")
	public ModelAndView OrderDetailsPendingList( Model model,HttpSession session,HttpServletRequest request) {
		model.addAttribute("pageName", "Order Details");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		 
		
		OrderDetailsList orderDetailsList = new OrderDetailsList();
		com.bluesquare.rc.rest.models.OrderDetailsList orderDetailsList2=new com.bluesquare.rc.rest.models.OrderDetailsList();
		try {
			session.setAttribute("orderDetailListForIssue", null);
			session.setAttribute("orderProductDetailListOld", null);
			long employeeId = ((EmployeeDetails)session.getAttribute("employeeDetails")).getEmployee().getEmployeeId();;

			orderDetailsList2 = orderDetailsService.fetchPendingOrderListByAreaId( employeeId);
			if (orderDetailsList2.getOrderDetailsList() == null) {
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Pending Orders");
			} else {
				List<OrderDetailsForWeb> orderDetailsForWebList=new ArrayList<>();
				for(OrderDetails orderDetails : orderDetailsList2.getOrderDetailsList())
				{
					orderDetailsForWebList.add(new OrderDetailsForWeb(
							orderDetails.getOrderId(), 
							orderDetails.getTotalAmount(), 
							orderDetails.getTotalAmountWithTax(), 
							orderDetails.getTotalQuantity(), 
							employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetails.getEmployeeSM().getEmployeeId()), 
							orderDetails.getBusinessName(), 
							orderDetails.getPaymentPeriodDays(),
							orderDetails.getOrderStatus(), 
							orderDetails.isPayStatus(), 
							orderDetails.getIssuedTotalAmount(), 
							orderDetails.getIssuedTotalAmountWithTax(), 
							orderDetails.getIssuedTotalQuantity(), 
							orderDetails.getOrderDetailsPaymentTakeDatetime(), 
							orderDetails.getOrderDetailsAddedDatetime(), 
							orderDetails.getCancelDate(), 
							orderDetails.getIssueDate(), 
							orderDetails.getPackedDate(), 
							orderDetails.getDeliveryDate(), 
							orderDetails.getConfirmDate(), 
							orderDetails.getConfirmTotalAmount(), 
							orderDetails.getConfirmTotalAmountWithTax(), 
							orderDetails.getConfirmTotalQuantity()));
				}
				orderDetailsList.setOrderDetailsList(orderDetailsForWebList);
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
			}

		} catch (Exception e) {
			orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
			orderDetailsList.setErrorMsg("Something Went Wrong ");
		}
		orderDetailsList.setAreaList(orderDetailsList2.getAreaList());
		orderDetailsList.setPageName("Pending");
		model.addAttribute("orderDetailsList", orderDetailsList);
		
		session.setAttribute("lastUrl", "GKOrderDetailsPendingList");
		
		return new ModelAndView("gkOrderList");
	}
	
	@Transactional 	@GetMapping("/fetchIssueProductDetailsForIssueReportByOrderIdForWeb")
	public ModelAndView fetchIssueProductDetailsForIssueReportByOrderId(HttpServletRequest request ,Model model,HttpSession session){
		model.addAttribute("pageName", "Order Product Details");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  
		
		OrderProductIssueListForIssueReportResponse orderProductIssueListForIssueReportResponse=new OrderProductIssueListForIssueReportResponse();
		System.out.println("fetchIssueProductDetailsForIssueReportByOrderId");
		
		try {
			 String orderId=request.getParameter("orderId");
			// Fetching orderProductIssueDetails By orderId
			OrderProductIssueDetails orderProductIssueDetails=orderDetailsService.fetchOrderProductIssueListForIssueReportByOrderId(orderId);
			if(orderProductIssueDetails==null)
			{
				orderProductIssueListForIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);	
				orderProductIssueListForIssueReportResponse.setErrorMsg("No Orderdetails");
			}
			orderProductIssueListForIssueReportResponse.setOrderProductIssueDetails(orderProductIssueDetails);
			
			// in below 2 lines we r fetching SalesManNAame and DeliveryBoy Name by getting empId from orderProductIssueDetails 
			EmployeeDetails employeeDetailsSM =employeeDetailsService.getEmployeeDetailsByemployeeId(orderProductIssueDetails.getOrderDetails().getEmployeeSM().getEmployeeId());
			EmployeeDetails employeeDetailsDB =employeeDetailsService.getEmployeeDetailsByemployeeId(orderProductIssueDetails.getEmployeeDB().getEmployeeId());
			
			//here we are fetching OrderProductDetalis By OrderId and making the ProductImage Null
			List<OrderProductDetails> orderProductDetailList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
			//orderProductDetailList=orderDetailsService.makeProductImageMNullorderProductDetailsList(orderProductDetailList);
			 
			if(orderProductDetailList==null)
			{
				orderProductIssueListForIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				orderProductIssueListForIssueReportResponse.setErrorMsg("Order Details not Found");
			}
			else
			{
				double allTotalAmount=0;
				long totalOrderQuantity=0,totalIssuedQuantity=0;
				for(OrderProductDetails orderProductDetails:orderProductDetailList)
				{
					allTotalAmount+=orderProductDetails.getIssueAmount();
					totalOrderQuantity+=orderProductDetails.getPurchaseQuantity();
					totalIssuedQuantity+=orderProductDetails.getIssuedQuantity();
				}
				
				model.addAttribute("allTotalAmount", allTotalAmount);
				model.addAttribute("totalOrderQuantity", totalOrderQuantity);
				model.addAttribute("totalIssuedQuantity", totalIssuedQuantity);
				
				orderProductIssueListForIssueReportResponse.setOrderProductDetailsList(orderProductDetailList);
				orderProductIssueListForIssueReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				orderProductIssueListForIssueReportResponse.setSalesman(employeeDetailsSM.getName());
				orderProductIssueListForIssueReportResponse.setDeliveryBoy(employeeDetailsDB.getName());
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			System.out.println("fetchIssueProductDetailsForIssueReportByOrderId Error:"+e.toString());
			orderProductIssueListForIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
		}
		
		
		model.addAttribute("orderProductIssueListForIssueReportResponse", orderProductIssueListForIssueReportResponse);
        return new ModelAndView("gkorderIssueReportDetails");
		
	}

	@Transactional 	@GetMapping("/GKfetchOrderDetailsByOrderId")
	public ModelAndView fetchOrderDetailsByOrderId(Model model,HttpSession session,HttpServletRequest request) {
		model.addAttribute("pageName", "Issue Order Product Details");
		EmployeeDetails employeeDetails1=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails1==null)
		  {
			  return new ModelAndView("login");
		  }
		
		OrderDetailResponse orderDetailResponse = new OrderDetailResponse();

		try {
			String orderId=request.getParameter("orderId");
			List<OrderProductDetails> orderProductDetailList = orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
			long employeeId = ((EmployeeDetails)session.getAttribute("employeeDetails")).getEmployee().getEmployeeId();
			List<EmployeeNameAndId> employeeNameAndIdSMAndDBList=employeeDetailsService.fetchDBByGateKeeperId(employeeId);
			orderDetailResponse.setEmployeeNameAndIdSMAndDBList(employeeNameAndIdSMAndDBList);
			if (orderProductDetailList == null) {
				orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
				model.addAttribute("orderDetailResponse", orderDetailResponse);
				return new ModelAndView("gkOrderDetailsForIssue");
			}
			//orderProductDetailList = orderDetailsService.makeProductImageMNullorderProductDetailsList(orderProductDetailList);

			OrderDetails orderDetailList = orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
			if (orderDetailList == null) {
				orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
				model.addAttribute("orderDetailResponse", orderDetailResponse);
				return new ModelAndView("gkOrderDetailsForIssue");
			}
			EmployeeDetails employeeDetails = employeeDetailsService
					.getEmployeeDetailsByemployeeId(orderDetailList.getEmployeeSM().getEmployeeId());

			orderDetailResponse.setStatus(Constants.SUCCESS_RESPONSE);
			orderDetailResponse.setOrderDetailList(orderDetailList);
			orderDetailResponse.setOrderProductDetailList(orderProductDetailList);
			orderDetailResponse.setSalesPersonName(employeeDetails.getName());
			
			session.setAttribute("orderDetailListForIssue", orderDetailList);
			
			List<OrderProductDetails> orderProductDetailListNew=new ArrayList<>();
			for(OrderProductDetails orderProductDetails:  orderDetailResponse.getOrderProductDetailList())
			{
				double sellingRate=orderProductDetails.getSellingRate();
				orderProductDetails.setIssueAmount(orderProductDetails.getPurchaseQuantity()*sellingRate);
				orderProductDetails.setIssuedQuantity(orderProductDetails.getPurchaseQuantity());
				
				orderProductDetailListNew.add(orderProductDetails);
			}
			session.setAttribute("orderProductDetailListOld", orderProductDetailListNew);			
			
			model.addAttribute("orderDetailResponse", orderDetailResponse);
			return new ModelAndView("gkOrderDetailsForIssue");
		} catch (Exception e) {
			System.out.println("/fetchOrderDetailsByOrderId/{orderId} Error : " + e.toString());
			orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
			orderDetailResponse.setErrorMsg("Something Went Wrong");
			model.addAttribute("orderDetailResponse", orderDetailResponse);
			return new ModelAndView("gkOrderDetailsForIssue");
		}

	}
	
	@Transactional 	@RequestMapping("/issueProductDetailsCalculate")
	public @ResponseBody List<OrderProductDetails> issueProductDetailsCalculate(HttpSession session,HttpServletRequest request)
	{
		List<OrderProductDetails> orderProductDetailListNew=new ArrayList<>();
		List<OrderProductDetails> orderProductDetailListOld=(List<OrderProductDetails>)session.getAttribute("orderProductDetailListOld");
		
		long issuedQuantity=Long.parseLong(request.getParameter("issuedQuantity"));
		long orderProductDetailsid=Long.parseLong(request.getParameter("orderProductDetailsid"));
		
		for(OrderProductDetails orderProductDetails: orderProductDetailListOld)
		{
			if(orderProductDetails.getOrderProductDetailsid()==orderProductDetailsid)
			{
				double sellingRate=orderProductDetails.getSellingRate();
				
				orderProductDetails.setIssueAmount(issuedQuantity*sellingRate);
				orderProductDetails.setIssuedQuantity(issuedQuantity);
			}				
			orderProductDetailListNew.add(orderProductDetails);
		}
		session.setAttribute("orderProductDetailListOld", orderProductDetailListNew);
		
		return orderProductDetailListNew;
	}
	
	@Transactional 	@PostMapping("/packedOrderToDeliverBoyForWeb")
	public @ResponseBody BaseDomain issueOrderToDeliverBoy(HttpSession session,HttpServletRequest request){
		
		BaseDomain baseDomain=new BaseDomain();
		OrderIssueRequest orderIssueRequest=new OrderIssueRequest();
		
		try 
		{
			List<OrderProductDetails> orderProductDetailListCart=(List<OrderProductDetails>)session.getAttribute("orderProductDetailListOld");
			
			List<OrderProductDetails> orderProductDetailListCartTemp=new ArrayList<>();
			for(OrderProductDetails orderProductDetails: orderProductDetailListCart){
				orderProductDetails.getProduct().setProductId(orderProductDetails.getProduct().getProduct().getProductId());
				orderProductDetailListCartTemp.add(orderProductDetails);
			}
			orderProductDetailListCart=orderProductDetailListCartTemp;
			
			/* check order product quantity with current inventory*/
			Map<Long,Long> productListWithQty=new HashMap<>();
			for(OrderProductDetails orderProductDetails : orderProductDetailListCart){
				if(productListWithQty.containsKey(orderProductDetails.getProduct().getProduct().getProductId())){
					long qty=productListWithQty.get(orderProductDetails.getProduct().getProduct().getProductId())+orderProductDetails.getIssuedQuantity();
					productListWithQty.put(orderProductDetails.getProduct().getProduct().getProductId(),qty);
				}else{
					productListWithQty.put(orderProductDetails.getProduct().getProduct().getProductId(),orderProductDetails.getIssuedQuantity());
				}
			}
			
			for(Map.Entry<Long,Long> entry :productListWithQty.entrySet()){
				Product product=productService.fetchProductForWebApp(entry.getKey());
				if(product.getCurrentQuantity()<entry.getValue()){
					
					baseDomain.setStatus(product.getProductName()+" qty exceeds Current Qty. Max Available : "+product.getCurrentQuantity());
					return baseDomain;				
				}
			}
			/*End*/
			
			OrderDetails orderDetails=(OrderDetails)session.getAttribute("orderDetailListForIssue");
			
			long totalIssuedQuantity=0;
			double totalIssuedTotalAmount=0;
			double totalIssuedTotalAmountWithTax=0;
			for(OrderProductDetails orderProductDetails: orderProductDetailListCart)
			{
				CalculateProperTaxModel calculateProperTaxModel=productService.calculateProperAmountModel(
						orderProductDetails.getSellingRate(), 
						orderProductDetails.getProduct().getCategories().getIgst());
				
				totalIssuedQuantity+=orderProductDetails.getIssuedQuantity();
				totalIssuedTotalAmount+=orderProductDetails.getIssuedQuantity()*calculateProperTaxModel.getUnitprice();
				totalIssuedTotalAmountWithTax+=orderProductDetails.getIssueAmount();
			}
			orderDetails.setIssueDate(new Date());
			orderDetails.setIssuedTotalAmount(totalIssuedTotalAmount);
			orderDetails.setIssuedTotalAmountWithTax(totalIssuedTotalAmountWithTax);
			orderDetails.setIssuedTotalQuantity(totalIssuedQuantity);
			Date date=new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("deliveryDate")); 
			orderIssueRequest.setDeliveryDate(new SimpleDateFormat("yyyy/MM/dd").format(date));
			orderIssueRequest.setEmployeeIdDB(Long.parseLong(request.getParameter("deliveryBoyId")));
			orderIssueRequest.setEmployeeIdGk(((EmployeeDetails)session.getAttribute("employeeDetails")).getEmployee().getEmployeeId());
			orderIssueRequest.setOrderDetails(orderDetails);
			orderIssueRequest.setOrderProductDetailsList(orderProductDetailListCart);			
			
			String result=orderDetailsService.packedOrderToDeliverBoy(orderIssueRequest);
			if(result.equals("Success"))
			{
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				baseDomain.setErrorMsg(orderDetails.getOrderId());
				session.setAttribute("orderDetailListForIssue", null);
				session.setAttribute("orderProductDetailListOld", null);
			}
			else
			{
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			}
		} 
		catch (Exception e) 
		{
			baseDomain.setStatus("Something Went Wrong");
		}
		
		return baseDomain;
	}
	
	//report start
	@Transactional 	@RequestMapping("/fetchOrderIssueReportForWeb")
	public ModelAndView fetchOrderIssueReportForWeb(Model model,HttpSession session)
	{
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  
		
		OrderProductIssueReportRequest orderProductIssueReportRequest=new OrderProductIssueReportRequest();
		orderProductIssueReportRequest.setRange("pickDate");
		orderProductIssueReportRequest.setFromDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		session.setAttribute("range", "pickDate");
		return fetchOrderProductIssueDetailsListByEmpIdAndDateRangeAndAreaId(orderProductIssueReportRequest, model, session);
	}
	
	@Transactional 	@RequestMapping("/fetchFilteredOrderIssueReportForWeb")
	public ModelAndView fetchFilteredOrderIssueReportForWeb(Model model,HttpSession session,HttpServletRequest request)
	{
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		 
		
		OrderProductIssueReportRequest orderProductIssueReportRequest=new OrderProductIssueReportRequest();
		
		String range=request.getParameter("range");
		String fromDate=request.getParameter("startDate");
		String toDate=request.getParameter("endDate");
		if(range==null)
		{
			orderProductIssueReportRequest.setRange((String)session.getAttribute("range"));
			if(((String)session.getAttribute("range")).equals("pickDate"))
			{
				orderProductIssueReportRequest.setFromDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			}
		}
		else
		{
			session.setAttribute("range",range);
			orderProductIssueReportRequest.setRange(range);
		}
		orderProductIssueReportRequest.setFromDate(fromDate);
		orderProductIssueReportRequest.setToDate(toDate);
		return fetchOrderProductIssueDetailsListByEmpIdAndDateRangeAndAreaId(orderProductIssueReportRequest, model, session);
	}
			
	
	@Transactional 	@RequestMapping("/fetchOrderProductIssueDetailsListReportByEmpIdAndDateRangeAndAreaIdForWeb")
	public ModelAndView fetchOrderProductIssueDetailsListByEmpIdAndDateRangeAndAreaId(
			@RequestBody OrderProductIssueReportRequest orderProductIssueReportRequest,Model model,HttpSession session) {
		System.out.println("fetchOrderProductIssueDetailsListReportByEmpIdAndDateRangeAndAreaIdForWeb");
		
		model.addAttribute("pageName", "Issue Order Details");
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  
		
		orderProductIssueReportRequest.setEmployeeId(employeeDetails.getEmployee().getEmployeeId());
		OrderProductIssueReportResponse orderProductIssueReportResponse = new OrderProductIssueReportResponse();

		try 
		{
			OrderProductIssueReportResponse orderProductIssueDetailsList = orderDetailsService.fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaIdWeb(orderProductIssueReportRequest.getEmployeeId(),orderProductIssueReportRequest.getFromDate(), orderProductIssueReportRequest.getToDate(), orderProductIssueReportRequest.getRange());
										
			if(orderProductIssueDetailsList.getOrderProductIssueDetailsList()==null)
			{
				orderProductIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				orderProductIssueReportResponse.setAreaList(orderProductIssueDetailsList.getAreaList());
				orderProductIssueReportResponse.setErrorMsg("No Order Found");
			}
			else
			{
				orderProductIssueReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				orderProductIssueReportResponse.setAreaList(orderProductIssueDetailsList.getAreaList());
				orderProductIssueReportResponse.setOrderProductIssueDetailsList(orderProductIssueDetailsList.getOrderProductIssueDetailsList());
			}
		} 
		catch (Exception e)
		{
			orderProductIssueReportResponse.setErrorMsg("Something Went Wrong");
			orderProductIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
		}		
		model.addAttribute("orderProductIssueReportResponse", orderProductIssueReportResponse);
		return new ModelAndView("gkorderIssueReport");
	}
	
	@Transactional 	@RequestMapping("/fetchReplacementOrderReportForWeb")
	public ModelAndView fetchReplacementOrderReportForWeb(Model model,HttpSession session)
	{
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
				
		ReturnOrderDateRangeRequest returnOrderDateRangeRequest=new ReturnOrderDateRangeRequest();
		returnOrderDateRangeRequest.setRange("pickDate");
		returnOrderDateRangeRequest.setFromDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		session.setAttribute("rangeRepl", "pickDate");
		return fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRange(model, returnOrderDateRangeRequest, session);
	}
	
	@Transactional 	@RequestMapping("/fetchFilteredReplacementOrderReportForWeb")
	public ModelAndView fetchFilteredReplacementOrderReportForWeb(Model model,HttpSession session,HttpServletRequest request)
	{
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		 		
		ReturnOrderDateRangeRequest returnOrderDateRangeRequest=new ReturnOrderDateRangeRequest();
		
		String range=request.getParameter("range");
		String fromDate=request.getParameter("fromDate");
		String toDate=request.getParameter("toDate");
		
		if(range==null)
		{
			returnOrderDateRangeRequest.setRange((String)session.getAttribute("rangeRepl"));
			if(((String)session.getAttribute("rangeRepl")).equals("pickDate"))
			{
				returnOrderDateRangeRequest.setFromDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			}
		}
		else
		{
			session.setAttribute("rangeRepl",range);
			returnOrderDateRangeRequest.setRange(range);
		}
		returnOrderDateRangeRequest.setFromDate(fromDate);
		returnOrderDateRangeRequest.setToDate(toDate);
		
		return fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRange(model, returnOrderDateRangeRequest, session);
	}
	
	@Transactional 	@PostMapping("/fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeForWeb")
	public ModelAndView fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRange(Model model,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest,HttpSession session){
		model.addAttribute("pageName", "Replacement Order Details");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		 
		
		returnOrderDateRangeRequest.setEmployeeId(((EmployeeDetails)session.getAttribute("employeeDetails")).getEmployee().getEmployeeId());
		ReIssueOrderDetailsReportResponse replacementReIssueOrderDetailsReportResponse=new ReIssueOrderDetailsReportResponse();
		
		System.out.println("fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRangeForWeb");
		
		try 
		{
			List<ReIssueOrderDetailsReport> reIssueOrderDetailsList=returnOrderService.fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeWeb(returnOrderDateRangeRequest.getEmployeeId(), returnOrderDateRangeRequest.getFromDate(), returnOrderDateRangeRequest.getToDate(), returnOrderDateRangeRequest.getRange());
			if(reIssueOrderDetailsList==null)
			{
				replacementReIssueOrderDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				replacementReIssueOrderDetailsReportResponse.setErrorMsg("No Record Found");
			}
			else
			{
				replacementReIssueOrderDetailsReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				replacementReIssueOrderDetailsReportResponse.setReIssueOrderDetailsReportsList(reIssueOrderDetailsList);
			}
		} catch (Exception e) {    
			
			e.printStackTrace();
			System.out.println("fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRange Error:"+e.toString());
			replacementReIssueOrderDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
		}
		model.addAttribute("replacementReIssueOrderDetailsReportResponse", replacementReIssueOrderDetailsReportResponse);
		return new ModelAndView("gkreplacementReport");
	}
	
	@Transactional 	@GetMapping("/fetchReIssueOrderProductDetailsForReplacementReport")
	public @ResponseBody List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsForReplacmentReport(HttpServletRequest request,Model model,HttpSession session){
				
		List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=new ArrayList<>();
		System.out.println("fetchReIssueOrderProductDetailsForReplacmentReport");
		
		try {
			long reIssueOrderId=Long.parseLong(request.getParameter("reIssueOrderId"));
			reIssueOrderProductDetailsList=returnOrderService.fetchReIssueOrderProductDetailsForReplacementReportByReIssueOrderId(reIssueOrderId);
			
			//String orderId=request.getParameter("orderId");
			//reIssueOrderProductDetailsList=returnOrderService.fetchReIssueOrderProductDetailsForReplacementReportByOrderId(orderId);
			//reIssueOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(reIssueOrderProductDetailsList);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchReIssueOrderProductDetailsForReplacmentReport Error"+e.toString());	
		}
		return reIssueOrderProductDetailsList;
	}
	
	
	/*@Transactional 	@GetMapping("/fetchReIssueOrderProductDetailsForReplacementReport")
	public ModelAndView fetchReIssueOrderProductDetailsForReplacmentReport(HttpServletRequest request,Model model,HttpSession session){
		model.addAttribute("pageName", "ReplaceMent Order Product Details");
		EmployeeDetails employeeDetails1=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails1==null)
		  {
			  return new ModelAndView("login");
		  }
		
		ReIssueOrderProductDetailsReportResponse replacementReportOrderProductDetailsResponse=new ReIssueOrderProductDetailsReportResponse();
		System.out.println("fetchReIssueOrderProductDetailsForReplacmentReport");
		
		try {
			String orderId=request.getParameter("orderId");
			ReIssueOrderDetails reIssueOrderDetails=returnOrderService.fetchReIssueOrderDetailsForReplacementReportByOrderId(orderId);
			if(reIssueOrderDetails==null)
			{
				replacementReportOrderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
				replacementReportOrderProductDetailsResponse.setErrorMsg("No Record found");
			}
			List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=returnOrderService.fetchReIssueOrderProductDetailsForReplacementReportByOrderId(orderId);
			reIssueOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(reIssueOrderProductDetailsList);
							
			EmployeeDetails employeeDetails =employeeDetailsService.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId());

			if(reIssueOrderProductDetailsList==null)
			{
				replacementReportOrderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
				replacementReportOrderProductDetailsResponse.setErrorMsg("No Record found");
			}
			else
			{				
				replacementReportOrderProductDetailsResponse.setStatus(Constants.SUCCESS_RESPONSE);
				replacementReportOrderProductDetailsResponse.setReIssueOrderDetails(reIssueOrderDetails);
				replacementReportOrderProductDetailsResponse.setReIssueOrderProductDetailsList(reIssueOrderProductDetailsList);
				replacementReportOrderProductDetailsResponse.setDeliveryPersonName(employeeDetails.getName());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchReIssueOrderProductDetailsForReplacmentReport Error"+e.toString());
			replacementReportOrderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);			
		}
		model.addAttribute("replacementReportOrderProductDetailsResponse", replacementReportOrderProductDetailsResponse);
		return new ModelAndView("gkreplacementReportDetails");
	}*/
	
	@Transactional 	@GetMapping("/fetchOrderDetailsByOrderIdForReIssueForWeb")
    public ModelAndView fetchOrderDetailsByOrderId(HttpServletRequest request,HttpSession session ,Model model){
       
		model.addAttribute("pageName", "ReIssue Order Product Details");
		
		EmployeeDetails employeeDetails1=(EmployeeDetails)session.getAttribute("employeeDetails");
		  if(employeeDetails1==null)
		  {
			  return new ModelAndView("login");
		  }
		
		OrderDetailResponse orderDetailResponse=new OrderDetailResponse();
        
        try 
        {
        	String orderId=request.getParameter("orderId");
        	long gateKeeperId=((EmployeeDetails)session.getAttribute("employeeDetails")).getEmployee().getEmployeeId();
        	
            List<OrderProductDetails> orderProductDetailList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
            if(orderProductDetailList==null){
                orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
                return new ModelAndView("gkReturnOrderDetailsForReIssue"); 
            }
            //orderProductDetailList=orderDetailsService.makeProductImageMNullorderProductDetailsList(orderProductDetailList);
            
            OrderDetails orderDetailList=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
            if(orderDetailList==null){
                orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
                return new ModelAndView("gkReturnOrderDetailsForReIssue"); 
            }
            EmployeeDetails employeeDetails =employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetailList.getEmployeeSM().getEmployeeId());
            List<EmployeeNameAndId> employeeNameAndIdSMAndDBList=employeeDetailsService.fetchSMandDBByGateKeeperId(gateKeeperId);
            
            ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderProductByReIssueStatus(orderId);
            
            orderDetailResponse.setReturnOrderProduct(returnOrderProduct);
            orderDetailResponse.setStatus(Constants.SUCCESS_RESPONSE);
            orderDetailResponse.setOrderDetailList(orderDetailList);
            orderDetailResponse.setOrderProductDetailList(orderProductDetailList);
            orderDetailResponse.setSalesPersonName(employeeDetails.getName());
            orderDetailResponse.setEmployeeNameAndIdSMAndDBList(employeeNameAndIdSMAndDBList);
            
            session.setAttribute("orderDetailResponseForReIssue", orderDetailResponse);
            
            List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=new ArrayList<>();
            for(OrderProductDetails orderProductDetails :orderProductDetailList)
            {
            	ReIssueOrderProductDetails reIssueOrderProductDetails=new ReIssueOrderProductDetails();
            	reIssueOrderProductDetails.setIssuedQuantity(orderProductDetails.getIssuedQuantity());
            	reIssueOrderProductDetails.setProduct(orderProductDetails.getProduct());
            	reIssueOrderProductDetails.setReturnQuantity(orderProductDetails.getIssuedQuantity()-orderProductDetails.getConfirmQuantity());
            	reIssueOrderProductDetails.setSellingRate(orderProductDetails.getSellingRate());
            	reIssueOrderProductDetails.setType(orderProductDetails.getType());
            	reIssueOrderProductDetails.setReIssueAmountWithTax(reIssueOrderProductDetails.getReturnQuantity()*orderProductDetails.getSellingRate());
				reIssueOrderProductDetails.setReIssueQuantity(reIssueOrderProductDetails.getReturnQuantity());
            	
            	reIssueOrderProductDetailsList.add(reIssueOrderProductDetails);            	
            }
            session.setAttribute("reIssueOrderProductDetailsListOld", reIssueOrderProductDetailsList);
            
            model.addAttribute("orderDetailResponse", orderDetailResponse);
            return new ModelAndView("gkReturnOrderDetailsForReIssue"); 
        } catch (Exception e) {
            System.out.println("/fetchOrderDetailsByOrderId/{orderId} Error : "+e.toString());
            orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
            return new ModelAndView("gkReturnOrderDetailsForReIssue");            
        }
    
    }
	
	@Transactional 	@RequestMapping("/reIssueProductDetailsCalculate")
	public @ResponseBody List<ReIssueOrderProductDetails> reIssueProductDetailsCalculate(HttpSession session,HttpServletRequest request)
	{
		List<ReIssueOrderProductDetails> reIssueOrderProductDetailsListNew=new ArrayList<>();		
		List<ReIssueOrderProductDetails> reIssueOrderProductDetailsListOld=(List<ReIssueOrderProductDetails>)session.getAttribute("reIssueOrderProductDetailsListOld");
		
		long reIssuedQuantity=Long.parseLong(request.getParameter("reIssuedQuantity"));
		long productId=Long.parseLong(request.getParameter("productId"));
		
		for(ReIssueOrderProductDetails reIssueOrderProductDetails: reIssueOrderProductDetailsListOld)
		{
			if(reIssueOrderProductDetails.getProduct().getProductId()==productId)
			{
				double sellingRate=reIssueOrderProductDetails.getSellingRate();
				
				reIssueOrderProductDetails.setReIssueAmountWithTax(reIssuedQuantity*sellingRate);
				reIssueOrderProductDetails.setReIssueQuantity(reIssuedQuantity);
			}				
			reIssueOrderProductDetailsListNew.add(reIssueOrderProductDetails);
		}
		session.setAttribute("reIssueOrderProductDetailsListOld", reIssueOrderProductDetailsListNew);
		
		return reIssueOrderProductDetailsListNew;
	}
	
	
	@Transactional 	@PostMapping("reIssueForWeb")
	public @ResponseBody BaseDomain reIssueOrderDetails(HttpSession session,HttpServletRequest request){
		
		BaseDomain baseDomain=new BaseDomain();
		
		try {
			OrderReIssueRequest orderReIssueRequest=new OrderReIssueRequest();			
			
			OrderDetailResponse orderDetailResponse=(OrderDetailResponse)session.getAttribute("orderDetailResponseForReIssue");
			List<ReIssueOrderProductDetails> reIssueOrderProductDetailsListOld=(List<ReIssueOrderProductDetails>)session.getAttribute("reIssueOrderProductDetailsListOld");
			ReIssueOrderDetails reIssueOrderDetails=new ReIssueOrderDetails();			
			
			
			/* check order product quantity with current inventory*/
			Map<Long,Long> productListWithQty=new HashMap<>();
			for(ReIssueOrderProductDetails reIssueOrderProductDetails : reIssueOrderProductDetailsListOld){
				if(productListWithQty.containsKey(reIssueOrderProductDetails.getProduct().getProduct().getProductId())){
					long qty=productListWithQty.get(reIssueOrderProductDetails.getProduct().getProduct().getProductId())+reIssueOrderProductDetails.getReIssueQuantity();
					productListWithQty.put(reIssueOrderProductDetails.getProduct().getProduct().getProductId(),qty);
				}else{
					productListWithQty.put(reIssueOrderProductDetails.getProduct().getProduct().getProductId(),reIssueOrderProductDetails.getReIssueQuantity());
				}
			}
			
			for(Map.Entry<Long,Long> entry :productListWithQty.entrySet()){
				Product product=productService.fetchProductForWebApp(entry.getKey());
				if(product.getCurrentQuantity()<entry.getValue()){
					
					baseDomain.setStatus(product.getProductName()+" qty exceeds Current Qty. Max Available : "+product.getCurrentQuantity());
					
					return baseDomain;			
				}
			}
			/*End*/
			
			double totalAmount=0;
			double totalAmountWithTax=0;
			long totalQuantity=0;
			
			Iterator<ReIssueOrderProductDetails> itr=reIssueOrderProductDetailsListOld.iterator();
			while(itr.hasNext())
			{
				ReIssueOrderProductDetails reIssueOrderProductDetails=itr.next();
				
				/*if(!(reIssueOrderProductDetails.getReIssueQuantity()>0))
				{
					itr.remove();
				}
				else
				{*/
					CalculateProperTaxModel calculateProperTaxModel=productService.calculateProperAmountModel(
							reIssueOrderProductDetails.getSellingRate(), 
							reIssueOrderProductDetails.getProduct().getCategories().getIgst());
					
					totalAmount+=calculateProperTaxModel.getUnitprice()*reIssueOrderProductDetails.getReIssueQuantity();
					totalAmountWithTax+=reIssueOrderProductDetails.getReIssueAmountWithTax();
					totalQuantity+=reIssueOrderProductDetails.getReIssueQuantity();
				//}
			}
			reIssueOrderDetails.setEmployeeGK(((EmployeeDetails)session.getAttribute("employeeDetails")).getEmployee());
			
			Employee employee=new Employee();
			employee.setEmployeeId(Long.parseLong(request.getParameter("deliveryBoyId")));
			
			reIssueOrderDetails.setEmployeeSMDB(employee);
			reIssueOrderDetails.setReturnOrderProduct(orderDetailResponse.getReturnOrderProduct());
			reIssueOrderDetails.setTotalAmount(totalAmount);
			reIssueOrderDetails.setTotalAmountWithTax(totalAmountWithTax);
			reIssueOrderDetails.setTotalQuantity(totalQuantity);
			
			Date date=new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("deliveryDate")); 
			orderReIssueRequest.setReIssueDeliveryDate(new SimpleDateFormat("yyyy/MM/dd").format(date));
			orderReIssueRequest.setReIssueOrderDetails(reIssueOrderDetails);
			orderReIssueRequest.setReIssueOrderProductDetailsList(reIssueOrderProductDetailsListOld);			
			
			String response=orderDetailsService.reIssueOrderDetails(orderReIssueRequest);
			if(response.equals("Success"))
			{
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				session.setAttribute("orderDetailResponseForReIssue",null);
				session.setAttribute("reIssueOrderProductDetailsListOld",null);
			}
			else
			{
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			}
		} catch (Exception e) {
			baseDomain.setStatus("Something went wrong");
		}
		
		return baseDomain;
	}
	
	
	@Transactional 	@RequestMapping("/fetchReturnOrderReportForWeb")
	public ModelAndView fetchReturnOrderReportForWeb(Model model,HttpSession session)
	{
		
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		
		
		session.setAttribute("orderDetailResponseForReIssue",null);
		session.setAttribute("reIssueOrderProductDetailsListOld",null);
		
		ReturnOrderDateRangeRequest returnOrderDateRangeRequest=new ReturnOrderDateRangeRequest();
		returnOrderDateRangeRequest.setRange("pickDate");
		returnOrderDateRangeRequest.setFromDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
		session.setAttribute("rangeRetu", "pickDate");
		return fetchReturnOrderProductListByFilter(model, returnOrderDateRangeRequest, session);
	}
	
	@Transactional 	@RequestMapping("/fetchFilteredReturnOrderReportForWeb")
	public ModelAndView fetchFilteredReturnOrderReportForWeb(Model model,HttpSession session,HttpServletRequest request)
	{
		
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		
		
		session.setAttribute("orderDetailResponseForReIssue",null);
		session.setAttribute("reIssueOrderProductDetailsListOld",null);
		
		ReturnOrderDateRangeRequest returnOrderDateRangeRequest=new ReturnOrderDateRangeRequest();
		
		String range=request.getParameter("range");
		String fromDate=request.getParameter("startDate");
		String toDate=request.getParameter("endDate");
		
		if(range==null)
		{
			returnOrderDateRangeRequest.setRange((String)session.getAttribute("rangeRetu"));
			if(((String)session.getAttribute("rangeRetu")).equals("pickDate"))
			{
				returnOrderDateRangeRequest.setFromDate(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
			}
		}
		else
		{
			session.setAttribute("rangeRetu",range);
			returnOrderDateRangeRequest.setRange(range);
		}
		returnOrderDateRangeRequest.setFromDate(fromDate);
		returnOrderDateRangeRequest.setToDate(toDate);
		return fetchReturnOrderProductListByFilter(model, returnOrderDateRangeRequest, session);
	}
	
	
	@Transactional 	@PostMapping("/fetchReturnOrderProductListByFilterForWeb")
	public ModelAndView fetchReturnOrderProductListByFilter(Model model,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest,HttpSession session){
		ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
		model.addAttribute("pageName", "Return Order Details Reports");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		
		
		System.out.println("fetchReturnOrderProductListByFilterForWeb");
		
		try 
		{
			returnOrderDateRangeRequest.setEmployeeId(((EmployeeDetails)session.getAttribute("employeeDetails")).getEmployee().getEmployeeId());
			
			returnOrderDateRangeResponse=returnOrderService.fetchReturnOrderProductListByFilterWeb(returnOrderDateRangeRequest.getEmployeeId(), returnOrderDateRangeRequest.getRange(), returnOrderDateRangeRequest.getFromDate(), returnOrderDateRangeRequest.getToDate());
			if(returnOrderDateRangeResponse.getReturnOrderProductList()==null)
			{
				returnOrderDateRangeResponse.setErrorMsg("List not found");
				returnOrderDateRangeResponse.setStatus(Constants.FAILURE_RESPONSE);
			}
			else
			{
				returnOrderDateRangeResponse.setStatus(Constants.SUCCESS_RESPONSE);
			}				
		}
		catch (Exception e) 
		{
			returnOrderDateRangeResponse.setErrorMsg(Constants.FAILURE_RESPONSE);
		}
		model.addAttribute("returnOrderDateRangeResponse", returnOrderDateRangeResponse);
		return new ModelAndView("gkReturnOrdersReport");
	}
	
	@Transactional 	@RequestMapping("/fetchReturnOrderProductDetailsByReturnOrderProductIdForReport")
	public ModelAndView fetchReturnOrderProductDetailsByOrderIdForGKReport(HttpServletRequest request,Model model,HttpSession session){
		model.addAttribute("pageName", "Return Order Product Details");
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		 
		  
		ReturnOrderProductDetailsReportResponse gKReturnOrderReportResponse=new ReturnOrderProductDetailsReportResponse();
		System.out.println("fetchReturnOrderProductDetailsByOrderIdForGKReport");
		
		try {
			session.setAttribute("orderDetailResponseForReIssue",null);
			session.setAttribute("reIssueOrderProductDetailsListOld",null);
			String returnOrderProductId =request.getParameter("returnOrderProductId");
			ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderForGKReportByReturnOrderProductId(returnOrderProductId);
			if(returnOrderProduct==null)
			{
				gKReturnOrderReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				gKReturnOrderReportResponse.setErrorMsg("No OrderFound");
			}
				List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderService.fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(returnOrderProductId);
				//returnOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(returnOrderProductDetailsList);
				if(returnOrderProductDetailsList==null)
				{
					gKReturnOrderReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					gKReturnOrderReportResponse.setErrorMsg("No ProductDetails found");
				}	
	
				gKReturnOrderReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				gKReturnOrderReportResponse.setReturnOrderProductDetailsList(returnOrderProductDetailsList);
				gKReturnOrderReportResponse.setReturnOrderProduct(returnOrderProduct);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("fetchReturnOrderProductDetailsByOrderIdForGKReport Error"+e.toString());
			gKReturnOrderReportResponse.setStatus(Constants.FAILURE_RESPONSE);
		}
		model.addAttribute("gKReturnOrderReportResponse", gKReturnOrderReportResponse);
	return new ModelAndView("gkReturnOrdersReportDetails");
	}
	
	
	@Transactional 	@RequestMapping("/Invoice.pdf")
	public void billPrint(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
	
		  model.addAttribute("pageName", "Issue Order Details Bill");
				
		  try {
			    String orderId=request.getParameter("orderId");
			    long companyId;
				try {
					companyId = Long.parseLong(request.getParameter("companyId"));
				} catch (Exception e) {
					companyId=Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds());
				}
				BillPrintDataModel billPrintDataModel=orderDetailsService.fetchBillPrintData(orderId,companyId);
				
				//OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
				String filePath="/resources/pdfFiles/invoice.pdf";
				
				ServletContext context = request.getServletContext();
				String appPath = context.getRealPath("/");
				System.out.println("appPath = " + appPath);

				// construct the complete absolute path of the file
				String fullPath = appPath + filePath;      
				//File downloadFile = new File(fullPath);
				
				File dFile= InvoiceGenerator.generateInvoicePdf(billPrintDataModel, fullPath);
				
				model.addAttribute("fullPath", fullPath);
				
				model.addAttribute("orderId", orderId);
				
				 // get your file as InputStream
				  InputStream is = new FileInputStream(dFile);
				  // copy it to response's OutputStream
				  response.setContentType("application/pdf");
				  org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
				  response.flushBuffer();
				  response.getOutputStream().close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return;
	}
			
		
	@Transactional 	@RequestMapping("/openIssuedBill")
	public ModelAndView openIssuedBill(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
				
		System.out.println("Open Bill controller");
		
		try {
			String orderId=request.getParameter("orderId");
			/*BillPrintDataModel billPrintDataModel=orderDetailsService.fetchBillPrintData(orderId);
			
			OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
			String filePath="/resources/pdfFiles/invoice.pdf";
			
			ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("/");
			System.out.println("appPath = " + appPath);
 
			// construct the complete absolute path of the file
			String fullPath = appPath + filePath;      
			//File downloadFile = new File(fullPath);
			
			File dFile= InvoiceGenerator.generateInvoicePdf(billPrintDataModel, fullPath);
			
			model.addAttribute("fullPath", fullPath);*/
			
			model.addAttribute("orderId", orderId);
			
			 /*// get your file as InputStream
			  InputStream is = new FileInputStream(dFile);
			  // copy it to response's OutputStream
			  org.apache.commons.io.IOUtils.copy(is, response.getOutputStream());
			  response.flushBuffer();*/
			  
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ModelAndView("BillShow");
	}
	
	@Transactional 	@RequestMapping("/sendMailIssuedBill")
	public @ResponseBody String  sendMailIssuedBill(HttpServletRequest request,Model model,HttpSession session,HttpServletResponse response){
		
		System.out.println("Send Bill Mail controller");
		
		try {
			String orderId=request.getParameter("orderId");
			long companyId;
			try {
				companyId = Long.parseLong(request.getParameter("companyId"));
			} catch (Exception e) {
				companyId=Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds());
			}
			BillPrintDataModel billPrintDataModel=orderDetailsService.fetchBillPrintData(orderId,companyId);
			OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
			String filePath="/resources/pdfFiles/invoice.pdf";
			
			ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("/");
			System.out.println("appPath = " + appPath);
 
			// construct the complete absolute path of the file
			String fullPath = appPath + filePath;      
			//File downloadFile = new File(fullPath);
			
			File dFile= InvoiceGenerator.generateInvoicePdf(billPrintDataModel, fullPath);
			
			String fileName=orderDetails.getInvoiceNumber()+".pdf";
			
			EmailSender emailSender=new EmailSender(mailSender, sessionFactory);
			emailSender.sendEmail("Order Invoice", " ", orderDetails.getBusinessName().getContact().getEmailId(), true, dFile,fileName);
			
			  return "Success";
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "Failed";
	}
	
	@Transactional 	@RequestMapping("/fetchProductByBrandAndCategoryIdWeb")
	public @ResponseBody List<ProductAddInventory> fetchProductByBrandAndCategoryIdWeb(Model model,HttpServletRequest request){
		BrandAndCategoryRequest brandAndCategoryRequest=new BrandAndCategoryRequest();
		brandAndCategoryRequest.setBrandId(Long.parseLong(request.getParameter("brandId")));
		brandAndCategoryRequest.setCategoryId(Long.parseLong(request.getParameter("categoryId")));
				
		List<ProductAddInventory> list=productService.fetchProductByBrandAndCategory(brandAndCategoryRequest);
		
		return list;
	}
	
	@Transactional 	@RequestMapping("/editOrder")
	public ModelAndView editOrder(Model model,HttpServletRequest request,HttpSession session){
		
		
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  
		
		OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(request.getParameter("orderId"));
		
		model.addAttribute("pageName", "Edit "+orderDetails.getOrderStatus().getStatus()+" Order");
		
		List<OrderProductDetails> orderProductDetailsList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(request.getParameter("orderId"));
		
		for(int i=0; i<orderProductDetailsList.size(); i++)
		{
			if(orderProductDetailsList.get(i).getIssuedQuantity()==0 && orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
			{
				orderProductDetailsList.remove(i);
			}
		}
		
		model.addAttribute("orderDetails",orderDetails);
		model.addAttribute("orderProductDetailsList",orderProductDetailsList);
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoryList=categoryService.fetchCategoriesListForWebApp();
		List<Product> productList=productService.fetchProductListForWebApp();
		model.addAttribute("brandList",brandList);
		model.addAttribute("categoryList",categoryList);
		model.addAttribute("productList",productList);
		
		return new ModelAndView("editOrder");
	}
	
	@Transactional 	@RequestMapping("/fetchOrderProductDetailsEditOrder")
	public  @ResponseBody List<OrderProductDetails> fetchOrderProductDetails(HttpServletRequest request)
	{		
		List<OrderProductDetails> orderProductDetailsList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(request.getParameter("orderId"));
		
		//orderProductDetailsList=orderDetailsService.makeProductImageMNullorderProductDetailsList(orderProductDetailsList);
		OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(request.getParameter("orderId"));
		
		for(int i=0; i<orderProductDetailsList.size(); i++)
		{
			if(orderProductDetailsList.get(i).getIssuedQuantity()==0 && orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_ISSUED))
			{
				orderProductDetailsList.remove(i);
			}
		}
		
		return orderProductDetailsList;
	}
	
	@Transactional 	@RequestMapping("/updateEditedOrder")
	public  ModelAndView updateEditedOrder(HttpServletRequest request,HttpSession session)
	{	
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		  
		
		System.out.println("inside update edited order");
		
		String updateOrderProductListId=request.getParameter("updateOrderProductListId");
		String orderId=request.getParameter("orderId");
		
		orderDetailsService.updateEditOrder(updateOrderProductListId, orderId);
		
		return new ModelAndView("redirect:/"+session.getAttribute("lastUrl"));
	}
	
	@Transactional 	@RequestMapping("/fetchReturnOrderFromDeliveryBoyReport")
	public ModelAndView fetchReturnOrderFromDeliveryBoyReport(Model model,HttpServletRequest request,HttpSession session)
	{
		model.addAttribute("pageName", "Return From DeliveryBoy");		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");		  
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		List<ReturnOrderFromDeliveryBoyReport>  orderFromDeliveryBoyReportList=orderDetailsService.fetchReturnOrderFromDeliveryBoyReport(range, startDate, endDate);
		model.addAttribute("orderFromDeliveryBoyReportList", orderFromDeliveryBoyReportList);
		return new ModelAndView("returnFromDeliveryBoy");
	}
	
	@Transactional 	@RequestMapping("/fetchReturnFromDeliveryBoy")
	public ModelAndView fetchReturnFromDeliveryBoy(Model model,HttpServletRequest request,HttpSession session)
	{
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		
		model.addAttribute("pageName", "Return From DeliveryBoy");
		
		String returnFromDeliveryBoyMainId=request.getParameter("returnFromDeliveryBoyMainId");
		
		List<ReturnFromDeliveryBoy>  returnFromDeliveryBoyList=orderDetailsService.fetchReturnFromDeliveryBoyList(returnFromDeliveryBoyMainId);
		
		model.addAttribute("returnFromDeliveryBoyList", returnFromDeliveryBoyList);
		EmployeeDetails employeeDetails1=employeeDetailsService.getEmployeeDetailsByemployeeId(returnFromDeliveryBoyList.get(0).getReturnFromDeliveryBoyMain().getOrderDetails().getEmployeeSM().getEmployeeId());
		model.addAttribute("smName", employeeDetails1.getName());
		return new ModelAndView("returnFromDeliveryBoyDetails");
	}
	
	@Transactional 	@RequestMapping("/fetchReturnFromDeliveryBoyAjax")
	public @ResponseBody List<ReturnFromDeliveryBoy> fetchReturnFromDeliveryBoyAjax(Model model,HttpServletRequest request)
	{
		String orderId=request.getParameter("returnFromDeliveryBoyMainId");
		
		List<ReturnFromDeliveryBoy>  returnFromDeliveryBoyList=orderDetailsService.fetchReturnFromDeliveryBoyList(orderId);
		
	  /*List<ReturnFromDeliveryBoy>  returnFromDeliveryBoyListTemp=new ArrayList<>();
		for(ReturnFromDeliveryBoy returnFromDeliveryBoy: returnFromDeliveryBoyList)
		{
			returnFromDeliveryBoy.getProduct().setProductImage(null);
			returnFromDeliveryBoy.getProduct().getProduct().setProductImage(null);
			returnFromDeliveryBoyListTemp.add(returnFromDeliveryBoy);
		}
		returnFromDeliveryBoyList=returnFromDeliveryBoyListTemp;*/
		return returnFromDeliveryBoyList;
	}
	
	@Transactional 	@RequestMapping(value ="/updateReturnFromDeliveryBoy",method = RequestMethod.POST,
	        consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
	        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public ModelAndView updateReturnFromDeliveryBoy(Model model,HttpServletRequest request,HttpSession session)
	{
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
				  
		String returnFromDeliveryboyListUpdated=request.getParameter("returnFromDeliveryboyListUpdated");
		String orderId=request.getParameter("returnFromDeliveryBoyMainId");

		orderDetailsService.updateReturnFromDeliveryBoy(orderId, returnFromDeliveryboyListUpdated);
		
		return new ModelAndView("redirect:/fetchReturnOrderFromDeliveryBoyReport?range=today");
	}
}

