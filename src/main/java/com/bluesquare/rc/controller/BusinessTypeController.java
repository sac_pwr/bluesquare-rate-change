package com.bluesquare.rc.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.BusinessTypeService;
import com.bluesquare.rc.utils.Constants;

/**
 * <pre>
 * @author Sachin Pawar		19-03-2018 Code Documentation
 * 
 * API Endpoints:
 * 1. fetchBusinessTypeList		 
 * 2. fetchBusinessType
 * 3. saveBusinessType
 * 4. updateBusinessType
 * </pre>
 */

@Controller
public class BusinessTypeController {

	@Autowired
	BusinessType businessType;

	@Autowired
	BusinessTypeService businessTypeService;

	@Autowired
	AreaService areaService;
	
	@Autowired
	Company company;
	
	/**
	 * fetch business Type list
	 * @param model 
	 * @param session
	 * @return ModelAndView addBusinessType.jsp
	 */
	@Transactional 	@RequestMapping("/fetchBusinessTypeList")
	public ModelAndView fetchBusinessTypeList(Model model,HttpSession session) {
		System.out.println("in manageSupplier controller");
		model.addAttribute("pageName", "Business Type List");
				 
	    List<Area> areaList=areaService.fetchAllAreaForWebApp();
		model.addAttribute("areaList", areaList);
		
		List<BusinessType> businessTypeList = businessTypeService.fetchBusinessTypeListForWebApp();
		model.addAttribute("businessTypeList", businessTypeList);
		
		/**
		 * for redirect message passing using session
		 */
		model.addAttribute("saveMsg", session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		
		return new ModelAndView("addBusinessType");
	}

	/**
	 * <pre>
	 * fetch business Type by business Type Id
	 * For Ajax
	 * @param model
	 * @param request
	 * @return businessType
	 * </pre> 
	 */
	@Transactional 	@RequestMapping("/fetchBusinessType")
	public @ResponseBody BusinessType fetchBusinessType(Model model, HttpServletRequest request) {
		System.out.println("in fetchBrand controller");
		
		long businessTypeId = Long.parseLong(request.getParameter("businessTypeId"));
		businessType = businessTypeService.fetchBusinessTypeForWebApp(businessTypeId);

		return businessType;
	}

	/**
	 * save business Type 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBusinessTypeList
	 */
	@Transactional 	@RequestMapping("/saveBusinessType")
	public ModelAndView saveBrandForWeb(HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("in Save Brand");

		this.businessType.setBusinessTypeId(Long.parseLong(request.getParameter("businessTypeId")));
		this.businessType.setName(request.getParameter("businessTypeName"));

		session.setAttribute("saveMsg", "");

		//here make first character Capital and other in small case
		businessType.setName((Character.toString(businessType.getName().charAt(0)).toUpperCase()
				+ businessType.getName().substring(1)));

		

		//here check businessType name already exist or not 
		boolean flag = false;
		List<BusinessType> businessTypeList = businessTypeService.fetchBusinessTypeListForWebApp();
		
		//check business type name with all old name
		//if found its already exist then flag will true otherwise false
		if (businessType.getBusinessTypeId() == 0) {
			
			if (businessTypeList != null) {
				for (BusinessType businessTypeFromDb : businessTypeList) {
					if (businessTypeFromDb.getName().trim().toUpperCase()
							.equals(businessType.getName().trim().toUpperCase())) {
						flag = true;
						break;
					}
				}
			}
			
			//if flag is false is then record insert other wise it send msg as Already exist
			if (flag == false) {
				businessType.setBusinessTypeAddedDatetime(new Date());
				businessTypeService.saveForWebApp(businessType);
				session.setAttribute("saveMsg", Constants.SAVE_SUCCESS);
				return new ModelAndView("redirect:/fetchBusinessTypeList");
			}
		}
		//check business type name with all old name
		//if found its already exist except checking self record then flag will true otherwise false
		if (businessType.getBusinessTypeId() != 0) {
			
			if (businessTypeList != null) {
				for (BusinessType businessTypeFromDb : businessTypeList) {
					if (businessTypeFromDb.getName().trim().toUpperCase().equals(businessType.getName().trim().toUpperCase())
							&& businessTypeFromDb.getBusinessTypeId()!=businessType.getBusinessTypeId()) {
						flag = true;
						break;
					}
				}
			}
			
			//if flag is false is then record update other wise it send msg as Already exist
			if (flag == false) 
			{
				System.out.println("Moving request for Update");
				session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
				return updateBusinessTypeForWeb(businessType, model,session);
			}
		}

		session.setAttribute("saveMsg", Constants.ALREADY_EXIST);
		return new ModelAndView("redirect:/fetchBusinessTypeList");

	}

	/**
	 * update business Type 
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchBusinessTypeList
	 */
	@Transactional 	@RequestMapping("/updateBusinessType")
	public ModelAndView updateBusinessTypeForWeb(@ModelAttribute BusinessType businessType, Model model,HttpSession session) {

		System.out.println("in Update Country");
			
		//only user allowed to update business type if its businessTypeId is not zero
		if (businessType.getBusinessTypeId() == 0 || businessType.getName() == null
				|| businessType.getName().equals("")) {
			System.out.println("can not update because 0 city not available");
			return new ModelAndView("redirect:/fetchBusinessTypeList");
		}
		
		this.businessType = businessTypeService.fetchBusinessTypeForWebApp(businessType.getBusinessTypeId());
		businessType.setBusinessTypeAddedDatetime(this.businessType.getBusinessTypeAddedDatetime());
		businessType.setBusinessTypeUpdatedDatetime(new Date());
		businessTypeService.updateForWebApp(businessType);
		session.setAttribute("saveMsg", Constants.UPDATE_SUCCESS);
		return new ModelAndView("redirect:/fetchBusinessTypeList");

	}
}
