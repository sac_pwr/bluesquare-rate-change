package com.bluesquare.rc.controller;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.CategoryAndBrandList;
import com.bluesquare.rc.models.InventoryDetailsForEdit;
import com.bluesquare.rc.models.InventoryProduct;
import com.bluesquare.rc.models.InventoryReportView;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.SupplierService;

@Controller
public class ManageInventory {

	@Autowired
	InventoryService inventoryService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	SupplierService supplierService;
	
	@Autowired
	ProductDAO productDAO;
	
	
	@Autowired
	InventoryDetails inventoryDetails;
	
	@Autowired
	Inventory inventory;
	
	@Autowired
	BrandService brandService; 
	
	@Autowired
	CategoriesService categoriesService;
	
	@Autowired
	Supplier supplier;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	@Transactional 	@RequestMapping("/fetchProductListForInventory")
	public ModelAndView fetchProductListForInventory(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Manage inventory");
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		model.addAttribute("inventoryProductsList", inventoryProductsList);
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		model.addAttribute("categorieslist", categoriesList);
		
		/*List<Product> productList=productService.fetchProductListForWebApp();
		model.addAttribute("productlist", productList);*/
		
		return new ModelAndView("ManageInventory");
	}
	
	@Transactional 	@RequestMapping("/fetchProductListForUnderThresholdValue")
	public ModelAndView fetchProductListForUnderThresholdValue(Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Manage Inventory");
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		List<InventoryProduct> inventoryProductsListNew=new ArrayList<>();
		long srno=1;		
		for(InventoryProduct inventoryProduct :inventoryProductsList)
		{
			Product product=inventoryProduct.getProduct();
			if(product.getCurrentQuantity()<=product.getThreshold())
			{
				inventoryProduct.setSrno(srno++);
				inventoryProductsListNew.add(inventoryProduct);
			}
		}
		
		model.addAttribute("inventoryProductsList", inventoryProductsListNew);
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		model.addAttribute("categorieslist", categoriesList);
		
		return new ModelAndView("ManageInventory");
	}
	
	@Transactional 	@RequestMapping("/fetchProductListForInventoryAjax")
	public @ResponseBody List<InventoryProduct> fetchProductListForInventoryAjax() {
		
		List<InventoryProduct> inventoryProductsList=inventoryService.inventoryProductList();
		//inventoryProductsList=inventoryService.makeInventoryProductViewProductNull(inventoryProductsList);
		
		return inventoryProductsList;
	}
	
	
	@Transactional 	@RequestMapping("/fetchSupplierListForAddQuantity")
	public @ResponseBody List<SupplierProductList> fetchSupplierListForAddQuantity(HttpServletRequest request) {
		
		List<SupplierProductList> supplierProductLists = inventoryService.fetchSupplierListByProductId(Long.parseLong(request.getParameter("productId")));
		
		/*if(supplierProductLists != null)
		{
			supplierProductLists=productService.makeSupplierProductImageNull(supplierProductLists);
		}	*/
		
		return supplierProductLists;
	}
	
	@Transactional 	@RequestMapping("/fetchSupplierById")
	public @ResponseBody SupplierProductList fetchSupplierById(HttpServletRequest request) {
		
		SupplierProductList supplierProductList=supplierService.fetchSupplierProductRate(Long.parseLong(request.getParameter("productId")),request.getParameter("supplierId"));
						
		return supplierProductList; 
	}
	
	@Transactional 	@RequestMapping("/saveInventory")
	public ModelAndView saveInventory(Model model,HttpServletRequest request,HttpSession session) throws ParseException {
		
			System.out.println("inside add inventory");
			
			String productIdList=request.getParameter("productIdList");
			String supplierId=request.getParameter("supplierId");
			String paymentDate=request.getParameter("paymentDate");
			String billDate=request.getParameter("billDate");
			String billNumber=request.getParameter("billNumber");
			inventory.setInventoryAddedDatetime(new Date());
			
			supplier=supplierService.fetchSupplier(supplierId);		
			inventory.setSupplier(supplier);
			inventory.setBillDate(dateFormat.parse(billDate));
			inventory.setBillNumber(billNumber);
			inventory.setInventoryPaymentDatetime(dateFormat.parse(paymentDate));
			inventoryService.addInventory(inventory,productIdList);
		
		
		return new ModelAndView("redirect:/fetchProductListForInventory"); 
	}
	
	@Transactional 	@RequestMapping("/saveOneInventory")
	public @ResponseBody String saveOneInventory(Model model,HttpServletRequest request) throws ParseException {
		
		System.out.println("inside add inventory");
		
			String productIdList=request.getParameter("productIdList");
			String supplierId=request.getParameter("supplierId");
			String paymentDate=request.getParameter("paymentDate");
			String billDate=request.getParameter("billDate");
			String billNumber=request.getParameter("billNumber");
			inventory.setInventoryAddedDatetime(new Date());
			
			supplier=supplierService.fetchSupplier(supplierId);			
			inventory.setBillDate(dateFormat.parse(billDate));
			inventory.setBillNumber(billNumber);
			inventory.setSupplier(supplier);
			inventory.setInventoryPaymentDatetime(dateFormat.parse(paymentDate));
			
			inventoryService.addInventory(inventory,productIdList);
		
		
		return "Success"; 
	}
	
	@Transactional 	@RequestMapping("/openAddMultipleInventory")
	public ModelAndView openAddMultipleInventory(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Add Multiple Inventory");
		
		
		
		List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();
		model.addAttribute("supplierList", supplierList);
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		model.addAttribute("categorieslist", categoriesList);
		
		model.addAttribute("invtUrl", "saveInventory");
		
		return new ModelAndView("addMultipleInventory"); 
	}
	
	@Transactional 	@RequestMapping("/fetchProductBySupplierId")
	public @ResponseBody List<Product> fetchProductBySupplierId(HttpServletRequest request) {
		
		List<Product> list=productService.fetchProductListBySupplierId(request.getParameter("supplierId"));
		
		return list; 
	}
	
	@Transactional 	@RequestMapping("/fetchProductBySupplierIdAndCategoryIdAndBrandId")
	public @ResponseBody List<Product> fetchProductBySupplierIdAndCategoryIdAndBrandId(HttpServletRequest request) {
		
		List<Product> list=productService.fetchProductListByBrandIdAndCategoryIdForWebApp(request.getParameter("supplierId"), Long.parseLong(request.getParameter("categoryId")), Long.parseLong(request.getParameter("brandId")));
		
		return list; 
	}
	
	@Transactional 	@RequestMapping("/fetchBrandAndCategoryList")
	public @ResponseBody CategoryAndBrandList fetchBrandAndCategoryList(HttpSession session) {
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		List<Supplier> fetchSupplierList=supplierService.fetchSupplierForWebAppList();
		//session.setAttribute("supplierOrderCartMap", null);
		return new CategoryAndBrandList(categoriesList, brandList,fetchSupplierList); 
	}
	
	@Transactional 	@RequestMapping("/fetchSupplierBySupplierId")
	public @ResponseBody Supplier fetchSupplierBySupplierId(HttpServletRequest request) {
		
		supplier=supplierService.fetchSupplier(request.getParameter("supplierId"));
		
		return supplier; 
	}
	
	@Transactional 	@RequestMapping("/fetchInventoryReportView")
	public ModelAndView fetchInventoryReportView(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Inventory Report");
		
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String supplierId=request.getParameter("supplierId");
		if(supplierId==null)
		{
			supplierId="";
		}
		
		List<InventoryReportView> inventoryReportViews=inventoryService.fetchInventoryReportView(supplierId, startDate, endDate, range);
		model.addAttribute("inventoryReportViews", inventoryReportViews);
		
		model.addAttribute("supplierId", supplierId);
		model.addAttribute("startDate", startDate);
		model.addAttribute("endDate", endDate);
		model.addAttribute("range", range);
		
		model.addAttribute("saveMsg",session.getAttribute("saveMsg"));
		session.setAttribute("saveMsg", "");
		
		session.setAttribute("lastUrl", "fetchInventoryReportView?range="+range+"&startDate="+startDate+"&endDate="+endDate+"&supplierId="+supplierId);
		
		return new ModelAndView("InventoryReport"); 
	}
	
	@Transactional 	@RequestMapping("/fetchInventoryReportViewAjax")
	public @ResponseBody List<InventoryReportView> fetchInventoryReportViewAjax(HttpServletRequest request,Model model,HttpSession session) {
				
		String range=request.getParameter("range");
		String startDate=request.getParameter("startDate");
		String endDate=request.getParameter("endDate");
		String supplierId=request.getParameter("supplierId");
		if(supplierId==null)
		{
			supplierId="";
		}
		
		List<InventoryReportView> inventoryReportViews=inventoryService.fetchInventoryReportView(supplierId, startDate, endDate, range);
		
		return inventoryReportViews; 
	}
	
	@Transactional 	@RequestMapping("/fetchTrasactionDetailsByInventoryId")
	public @ResponseBody List<InventoryDetails> fetchTrasactionDetailsByInventoryId(HttpServletRequest request) {
		
		List<InventoryDetails> inventoryDetailList=inventoryService.fetchTrasactionDetailsByInventoryId(request.getParameter("inventoryId"));
		//inventoryDetailList=inventoryService.makeProductImageNullOfInventoryDetailsList(inventoryDetailList);
		return inventoryDetailList; 
	}
	
	@Transactional 	@RequestMapping("/openEditMultipleInventory")
	public ModelAndView openEditMultipleInventory(HttpServletRequest request,Model model,HttpSession session) {
		
		model.addAttribute("pageName", "Edit Multiple Inventory");
				
	    List<InventoryDetails> inventoryDetailsList=inventoryService.fetchTrasactionDetailsByInventoryId(request.getParameter("inventoryId"));
	    Inventory inventory=inventoryService.fetchInventory(request.getParameter("inventoryId"));
	    
	    double totalAmountWithoutTax=0;
	    double totalAmountWithTax=0;
	    
	    List<InventoryDetailsForEdit> inventoryDetailsList2=new ArrayList<>();
	    for(InventoryDetails inventoryDetails:inventoryDetailsList)
	    {
	    	long qty=inventoryDetails.getQuantity();
      		double rate=  supplierService.fetchSupplierByProductIdAndSupplierId(inventoryDetails.getProduct().getProduct().getProductId(),inventory.getSupplier().getSupplierId()).getSupplierRate();
      		float tempIgst=inventoryDetails.getProduct().getCategories().getIgst();
      		double ttl=qty*rate;
      		//double amtwithtax=ttl + ( (ttl*tempIgst)/100 ); 
	    	
      		double mrp=rate+((rate*tempIgst)/100);
			mrp=Double.parseDouble(new DecimalFormat("###").format(mrp));
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(mrp, tempIgst);
			
			double amtwithtax=calculateProperTaxModel.getMrp()*qty;
      		
      		totalAmountWithoutTax+=ttl;
      		totalAmountWithTax+=amtwithtax;
	    	inventoryDetailsList2.add(new InventoryDetailsForEdit(ttl, amtwithtax, inventoryDetails.getProduct(), rate, qty));
	    }
	    
	    model.addAttribute("totalAmount", totalAmountWithoutTax);
	    model.addAttribute("totalAmountWithTax", totalAmountWithTax);
	    
	    
	    model.addAttribute("inventoryDetailsList", inventoryDetailsList2);
	    	    
		List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();
		model.addAttribute("supplierList", supplierList);
		
		
		List<Product> productList=productService.fetchProductListByBrandIdAndCategoryIdForWebApp(inventory.getSupplier().getSupplierId(), 0, 0);
		model.addAttribute("productList", productList);
		model.addAttribute("paymentDate", new SimpleDateFormat("yyyy-MM-dd").format(inventory.getInventoryPaymentDatetime()));
		
		List<Brand> brandList=brandService.fetchBrandListForWebApp();
		model.addAttribute("brandlist", brandList);
		
		List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
		model.addAttribute("categorieslist", categoriesList);
		
		model.addAttribute("invtUrl", "editInventory");
		model.addAttribute("inventory", inventory);
		
		return new ModelAndView("addMultipleInventory"); 
	}	
	
	@Transactional 	@RequestMapping("/editInventory")
	public ModelAndView editInventory(Model model,HttpServletRequest request) throws ParseException {
		
		System.out.println("inside edit inventory");
			String inventoryId=request.getParameter("inventoryId");
			String productIdList=request.getParameter("productIdList");
			String supplierId=request.getParameter("supplierId");
			String paymentDate=request.getParameter("paymentDate");
			String billDate=request.getParameter("billDate");
			String billNumber=request.getParameter("billNumber");
			
			inventory=inventoryService.fetchInventory(inventoryId);
			
			supplier=supplierService.fetchSupplier(supplierId);		
			inventory.setBillDate(dateFormat.parse(billDate));
			inventory.setBillNumber(billNumber);
			inventory.setSupplier(supplier);
			inventory.setInventoryPaymentDatetime(dateFormat.parse(paymentDate));
			
			inventoryService.editInventory(inventory,productIdList);
				
		return new ModelAndView("redirect:/fetchInventoryReportView?range=currentMonth&supplierId="); 
	}
	
	@Transactional 	@RequestMapping("/deleteInventory")
	public ModelAndView deleteInventory(Model model,HttpServletRequest request) {
		System.out.println("inside delete inventory");
		inventoryService.deleteInventory(request.getParameter("inventoryId"));
		return new ModelAndView("redirect:/fetchInventoryReportView?range=currentMonth&supplierId=");
	}
	
	@Transactional 	@RequestMapping("/defineDamage")
	public ModelAndView defineDamage(Model model,HttpServletRequest request,HttpSession session) {
		System.out.println("inside define Damage");
		
		long productId=Long.parseLong(request.getParameter("productId"));;
		long damageQuantity=Long.parseLong(request.getParameter("damageQuantity"));
		String damageType=request.getParameter("damageType");
		String reason=request.getParameter("damageQuantityReason");
		
		Product product=productService.fetchProductForWebApp(productId);
		product.setDamageQuantity(product.getDamageQuantity()+damageQuantity);
		product.setCurrentQuantity(product.getCurrentQuantity()-damageQuantity);
		productService.updateProductForWebApp(product);
		
		//update OrderUsedProduct Current Quantity same as Product
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
		if(damageType.equals("claimed")){
			productService.saveUpdateDamageRecoveryMonthWise(productId,damageQuantity);
			
		}else{
			//notClaimed
			//not decide.. what to do for direct loss
		}
		
		
		//damagedefine for report
		String orderId="",collectingPerson="",departmentName="";
		long companyId=Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds());
		EmployeeDetails employeeDetails = (EmployeeDetails) session.getAttribute("employeeDetails");
		
		if(employeeDetails==null){
			orderId="Company";
			collectingPerson="Company";
			departmentName="Company";
		}else{
			orderId=employeeDetails.getName();
			collectingPerson=employeeDetails.getName();
			departmentName=employeeDetails.getEmployee().getDepartment().getName();
		}
		
		DamageDefine damageDefine=new DamageDefine( orderId, product, collectingPerson, departmentName, damageQuantity, new Date(), reason);
		productService.saveDamageDefine(damageDefine);
		
		return new ModelAndView("redirect:/fetchProductListForInventory");
	}
}
