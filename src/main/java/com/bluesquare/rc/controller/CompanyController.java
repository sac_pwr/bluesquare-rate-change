package com.bluesquare.rc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.Contact;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.service.CityService;
import com.bluesquare.rc.service.CompanyService;
import com.bluesquare.rc.service.CountryService;
import com.bluesquare.rc.service.StateService;

/**
 *  <pre>
 * @author Sachin Pawar 19-03-2018 Code Documentation
 * 
 * <b>API EndPoints</b>
 * 1.addCompany - open add_company.jsp with country list for fill records to create company
 * 2.saveCompany
 * 3.fetchCompany
 * 4.fetchCompanyCities
 * 5.checkCompanyName
 * 6.checkCompanyNameForUpdate
 * 7.checkCompanyGSTNumberForSave
 * 8.checkCompanyGSTNumberForUpdate
 * 9.checkCompanyUserName
 * 10.checkCompanyUserNameForUpdate
 * 11.updateCompany
 * 12.updateCompanyForReset
 * 13.fetchAllCompany
 * 14.companySetting
 * 15.companySettingInitial
 * 16.setCompany
 * 17.sendSMSTOCompany
 * 18.openForgetPasswordCompany
 *  </pre>
 */
@Controller
public class CompanyController {
	@Autowired
	CompanyService companyService;
	
	@Autowired
	Company company;
	
	@Autowired
	Contact contact;
	
	@Autowired
	CountryService countryService;
	
	@Autowired
	StateService stateService;
	
	@Autowired
	CityService cityService;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	/**
	 * send country list to jsp for open create new company
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView add_company.jsp
	 */
	@Transactional 	@RequestMapping("/addCompany")
	public ModelAndView addCompany(HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("in add Company");
		
		model.addAttribute("pageName", "Add Company");
		
		List<Country> countryList=countryService.fetchCountryListForWebApp();		
		model.addAttribute("countryList", countryList);		
		
		return new ModelAndView("add_company");
	}
	
	/**
	 * save company
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView redirect:/fetchAllCompany
	 */
	@Transactional 	@RequestMapping("/saveCompany")
	public ModelAndView saveCompany(HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("in Save Company");
		
		company.setCompanyName(request.getParameter("companyName"));
		company.setUserId(request.getParameter("userId"));
		company.setPassword(request.getParameter("password"));
		company.setAddress(request.getParameter("address"));
		company.setGstinno(request.getParameter("gstinno"));
		company.setPanNumber(request.getParameter("panNumber"));
		
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobNo"));
		contact.setTelephoneNumber(request.getParameter("telephoneNo"));
		company.setContact(contact);
		
		String cityIdList=request.getParameter("cityIdList");
		
		companyService.saveCompany(company,cityIdList);
		
		return new ModelAndView("redirect:/fetchAllCompany");
	}
	/**
	 * <pre>
	 * fetch company details by companyId and 
	 * send it to update_company.jsp for update company info
	 * </pre>
	 * @param request
	 * @param model
	 * @param session
	 * @return ModelAndView update_company.jsp
	 */
	@Transactional 	@RequestMapping("/fetchCompany")
	public ModelAndView fetchCompany(HttpServletRequest request, Model model,HttpSession session) {
		model.addAttribute("pageName", "Manage Company");
		
		Company company=companyService.fetchCompanyByCompanyId(Long.parseLong(request.getParameter("companyId")));
		List<CompanyCities> companyCities=companyService.fetchCompanyCities(Long.parseLong(request.getParameter("companyId")));
		model.addAttribute("company", company);
		model.addAttribute("companyCities", companyCities);
		
		List<Country> countryList=countryService.fetchCountryListForWebApp();		
		model.addAttribute("countryList", countryList);		
		
		return new ModelAndView("update_company");
	}
	
	@Transactional 	@RequestMapping("/fetchCompanyCities")
	public @ResponseBody List<CompanyCities> fetchCompanyCities(HttpServletRequest request){
		List<CompanyCities> companyCities=companyService.fetchCompanyCities(Long.parseLong(request.getParameter("companyId")));
		return companyCities;
	}
	
	@Transactional 	@RequestMapping("/checkDuplication")
	public @ResponseBody String checkDuplication(HttpServletRequest request, Model model,HttpSession session) {
		
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		
		return companyService.checkDuplication(checkText, type, 0);	
		
		/*Company company=companyService.checkCompanyName(request.getParameter("companyName"));
		if(company==null){
			return "Success";
		}
		return "Failed";*/
	}
	
	@Transactional 	@RequestMapping("/checkDuplicationForUpdate")
	public @ResponseBody String checkDuplicationForUpdate(HttpServletRequest request, Model model,HttpSession session) {
		
		long companyId=Long.parseLong(request.getParameter("companyId"));
		String checkText=request.getParameter("checkText");
		String type=request.getParameter("type");
		
		return companyService.checkDuplication(checkText, type, companyId);	
		
		/*Company company=companyService.checkCompanyName(request.getParameter("companyName"));
		if(company==null){
			return "Success";
		}
		return "Failed";*/
	}
	
	/*
	@Transactional 	@RequestMapping("/checkCompanyNameForUpdate")
	public @ResponseBody String checkCompanyNameForUpdate(HttpServletRequest request, Model model,HttpSession session) {
		
		Company company=companyService.checkCompanyNameForUpdate(request.getParameter("companyName"),Long.parseLong(request.getParameter("companyId")));
		if(company==null){
			return "Success";
		}
		return "Failed";
	}
	
	@Transactional 	@RequestMapping("/checkCompanyGSTNumberForSave")
	public @ResponseBody String checkCompanyGSTNumberForSave(HttpServletRequest request, Model model,HttpSession session) {
		
		Company company=companyService.checkCompanyGstIn(request.getParameter("gstinNo"));
		if(company==null){
			return "Success";
		}
		return "Failed";
	}
	
	@Transactional 	@RequestMapping("/checkCompanyGSTNumberForUpdate")
	public @ResponseBody String checkCompanyGSTNumberForUpdate(HttpServletRequest request, Model model,HttpSession session) {
		
		Company company=companyService.checkCompanyGstInForUpdate(request.getParameter("gstinNo"),Long.parseLong(request.getParameter("companyId")));
		if(company==null){
			return "Success";
		}
		return "Failed";
	}
	
	@Transactional 	@RequestMapping("/checkCompanyUserName")
	public @ResponseBody String checkCompanyUserName(HttpServletRequest request, Model model,HttpSession session) {
		
		Company company=companyService.checkCompanyUserName(request.getParameter("userName"));
		if(company==null){
			return "Success";
		}
		return "Failed";
	}
	@Transactional 	@RequestMapping("/checkCompanyUserNameForUpdate")
	public @ResponseBody String checkCompanyUserNameForUpdate(HttpServletRequest request, Model model,HttpSession session) {
		
		Company company=companyService.checkCompanyUserNameForUpdate(request.getParameter("userName"),Long.parseLong(request.getParameter("companyId")));
		if(company==null){
			return "Success";
		}
		return "Failed";
	}*/
	
	@Transactional 	@RequestMapping("/updateCompany")
	public ModelAndView updateCompany(HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("in Update Company");
		
		company=companyService.fetchCompanyByCompanyId(Long.parseLong(request.getParameter("companyId")));
		
		company.setCompanyName(request.getParameter("companyName"));
		company.setUserId(request.getParameter("userId"));
		company.setPassword(request.getParameter("password"));
		company.setAddress(request.getParameter("address"));
		company.setGstinno(request.getParameter("gstinno"));
		company.setPanNumber(request.getParameter("panNumber"));
		
		contact=company.getContact();
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobNo"));
		contact.setTelephoneNumber(request.getParameter("telephoneNo"));
		company.setContact(contact);
		
		String cityIdList=request.getParameter("cityIdList");
		
		companyService.updateCompany(company,cityIdList);
		
		return new ModelAndView("redirect:/fetchAllCompany");
	}
	
	@Transactional 	@RequestMapping("/updateCompanyForReset")
	public ModelAndView updateCompanyForReset(HttpServletRequest request, Model model,HttpSession session) {
		System.out.println("in Update Company");
		
		company=companyService.fetchCompanyByCompanyId(Long.parseLong(request.getParameter("companyId")));
		
		company.setCompanyName(request.getParameter("companyName"));
		company.setUserId(request.getParameter("userId"));
		company.setPassword(request.getParameter("password"));
		company.setAddress(request.getParameter("address"));
		company.setGstinno(request.getParameter("gstinno"));
		company.setPanNumber(request.getParameter("panNumber"));
		
		contact=company.getContact();
		contact.setEmailId(request.getParameter("emailId"));
		contact.setMobileNumber(request.getParameter("mobNo"));
		contact.setTelephoneNumber(request.getParameter("telephoneNo"));
		company.setContact(contact);
		companyService.updateCompany(company);
		session.setAttribute("companyDetails", company);
		
		return new ModelAndView("redirect:/openForgetPasswordCompany");
	}
	
	@Transactional 	@RequestMapping("/fetchAllCompany")
	public ModelAndView fetchAllCompany(HttpServletRequest request, Model model,HttpSession session) {
		model.addAttribute("pageName", "Manage Company");
		
		List<Company> companyList=companyService.fetchAllCompany();
		
		model.addAttribute("companyList", companyList);
		
		return new ModelAndView("manage_companies");
	}
	
	@Transactional 	@RequestMapping("/companySetting")
	public ModelAndView companySetting(Model model,HttpSession session) {
	session.setAttribute("showNavbar", true);
	return new ModelAndView("redirect:/companySettingInitial");
	}
	
	@Transactional 	@RequestMapping("/companySettingInitial")
	public ModelAndView companySettingInitial(Model model,HttpSession session) {
		System.out.println("in companySettingInitial");
		
		model.addAttribute("pageName", "Company Setting");
		
		List<Company> companyList=companyService.fetchAllCompany();
		
		model.addAttribute("companyList", companyList);
		
		return new ModelAndView("setCompanyInitial");
	}
	

	@Transactional 	@RequestMapping("/setCompany")
	public ModelAndView setCompany(HttpServletRequest request,HttpSession session){
		System.out.println("in setCompany");
		long companyId=Long.parseLong(request.getParameter("companyId"));
		
		List<Long> companyIdList=new ArrayList<>();
		
		if(companyId==-1){
			/*List<Company> companyList=companyService.fetchAllCompany();
			if(companyList!=null){
				for(Company company: companyList){
					companyIdList.add(company.getCompanyId());
				}
			}else{*/
				session.setAttribute("sessionCopamnyName", "Co. Not Selected");
				companyIdList.add(companyId);
			//}
		}else{
			Company company=companyService.fetchCompanyByCompanyId(companyId);
			session.setAttribute("companyDetails", company);
			session.setAttribute("sessionCopamnyName", company.getCompanyName());
			
			companyIdList.add(companyId);
		}		
		
		session.setAttribute("selectedCompanyIds", companyIdList);
		
		return new ModelAndView("redirect:/");
	}
	
	//sendSMSTOCompany
	@Transactional 	@RequestMapping("/sendSMSTOCompany")
	public @ResponseBody String sendSMSTOShops(HttpServletRequest request) {
				
			String shopsIds=request.getParameter("shopsIds");		
			String smsText=request.getParameter("smsText");	
			String mobileNumber=request.getParameter("mobileNumber");	
			
			companyService.sendSMSTOCompanies(shopsIds, smsText, mobileNumber);
			
			return "Success";
	}
	
	@Transactional 	@RequestMapping("/openForgetPasswordCompany")
	public ModelAndView openForgetPasswordCompany(Model model,HttpServletRequest request,HttpSession session){
		System.out.println("in openForgetPasswordCompany");
		
		model.addAttribute("pageName", "Update Details");
		
		return new ModelAndView("forget_password_company");
	}
	
	/*@RequestMapping("/forgetPasswordSendOtp")
	public @ResponseBody String forgetPasswordSendOtp(HttpServletRequest request,HttpSession session){
		System.out.println("in forgetPasswordSendOtp");
		
		String emailIdAndMobileNumber=request.getParameter("emailIdAndMobNo");
		
		Company company=companyService.fetchCompanyByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		String responseText="";
		String otpNumber;
		if(company.getContact().getEmailId().equals(emailIdAndMobileNumber.trim())){			
			otpNumber=companyService.sendOTPToCompanyUsingMailAndSMS(emailIdAndMobileNumber, false);
			responseText="OTP send to Your EmailId";			
		}else if(company.getContact().getMobileNumber().equals(emailIdAndMobileNumber.trim())){			
			otpNumber=companyService.sendOTPToCompanyUsingMailAndSMS(emailIdAndMobileNumber, true);
			responseText="OTP send to Your Mobile Number";			
		}else{
			otpNumber="";
			responseText="Failed";			
		}
		session.setAttribute("forgetPasswordOtpNumber", otpNumber);
		return responseText;
	}
	
	@Transactional 	@RequestMapping("/checkCompanyOTP")
	public @ResponseBody String checkCompanyOTP(HttpServletRequest request,HttpSession session){
		String otp=request.getParameter("otp");
		
		if( ((String)session.getAttribute("forgetPasswordOtpNumber")).equals(otp)){
			return "success";
		}else{
			return "failed";
		}
	}
	
	@Transactional 	@RequestMapping("/forgetPasswordCompany")
	public ModelAndView forgetPasswordCompany(HttpServletRequest request,HttpSession session){
		System.out.println("in forgetPasswordCompany");
		
		String password=request.getParameter("password");		
		
		Company company=companyService.fetchCompanyByCompanyId(Long.parseLong(tokenHandlerDAO.getSessionSelectedCompaniesIds()));
		company.setPassword(password);
		
		companyService.updateCompany(company);
		
		return new ModelAndView("redirect:/");
	}*/
}
