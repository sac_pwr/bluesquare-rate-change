package com.bluesquare.rc.dao;

import com.bluesquare.rc.rest.models.SnapShotReportResponse;

public interface SnapShotDAO {

	public SnapShotReportResponse fetchRecordForSnapShot(long employeeId,String fromDate,String toDate,String range);
	
}
