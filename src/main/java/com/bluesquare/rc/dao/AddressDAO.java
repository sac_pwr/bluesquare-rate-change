package com.bluesquare.rc.dao;

import com.bluesquare.rc.entities.Address;
/** 
 * <pre>
 * @author <b><font color="green">Sachin Pawar</font></b>	19-03-2018 Code Documentation
 * <pre>
 * Provides Interface Definition for following methods
 * 1. save
 * 2. update
 * 3. delete
 */
public interface AddressDAO {

	/**
	 * save Address 
	 * @param address
	 */
	public void save(Address address);

	/**
	 * update Address
	 * @param address
	 */
	public void update(Address address);

	/**
	 * delete Address by id
	 * @param id
	 */
	public void delete(long id);

}
