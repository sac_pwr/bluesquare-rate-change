package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Brand;

public interface BrandDAO {

	public void saveBrandForWebApp(Brand brand);
	public void updateBrandForWebApp(Brand brand);
	public Brand fetchBrandForWebApp(long brandId);
	public List<Brand> fetchBrandListForWebApp();	
}
