package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.OrderProductIssueDetails;

public interface OrderProductIssueDetailsDAO {

	//public void issueOrderProduct(IssueProductRequest issueProductRequest);

	public void saveIssueOrderProduct(OrderProductIssueDetails orderProductIssueDetails);

	//public void updateDeliveryDate(DeliveryOrderRequest deliveryOrderRequest);

	public List<OrderProductIssueDetails> getDeliveryOrderList(long businessNameId,String currentDate);

	public List<OrderProductIssueDetails> getDeliveryListByCurrentDate(String currentDate);

	public List<OrderProductIssueDetails> getItemReceivedList(String currentDate);

	public List<OrderProductIssueDetails> getOrderListByEmployee(long employeeId,String currentDate);

	public List<OrderProductIssueDetails> getDeliveryOrderAreaList(String areaId,String currentDate);

	/*public List<OrderProductIssueDetails> getDeliveryOrderByDate(
			FetchOrderListByDateRequest fetchOrderListByDateRequest);*/

//	public List<OrderProductIssueDetails> getItemReceivedReportList(String orderId);

	public List<OrderProductIssueDetails> getIssuedReportList();
	
	/*public List<OrderProductIssueDetails> getItemReceivedListByDateAndEmployeeId(ItemReceivedReportRequest itemReceivedReportRequest);
	
	public List<OrderProductIssueDetails> getItemDeliveredListByDateAndEmployeeId(ItemReceivedReportRequest itemReceivedReportRequest);

	public List<OrderProductIssueDetails> getReportListByDate(FetchOrderListByDateRequest fetchIssuedReportListByDate);*/

}
