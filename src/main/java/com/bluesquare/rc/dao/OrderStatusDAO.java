package com.bluesquare.rc.dao;

import com.bluesquare.rc.entities.OrderStatus;

public interface OrderStatusDAO {
	
	public OrderStatus fetchOrderStatus(String name);

}
