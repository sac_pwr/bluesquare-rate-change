package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.models.LedgerPaymentView;

public interface LedgerDAO {

	public void saveLedger(Ledger ledger);
	public void updateLedger(Ledger ledger);
	public List<Ledger> fetchBeforeLedgerList(long  lederId);
	public List<Ledger> fetchAfterLedgerList(long lederId);
	public Ledger fetchLedger(String type,String id);
	public List<LedgerPaymentView> fetchLedgerPaymentView(String startDate, String endDate,String range);
	public void createLedgerEntry(Ledger ledger);
	public void updateBalanceLedgerListAfterGivenLedgerId(long ledgerId,double balance);
}
