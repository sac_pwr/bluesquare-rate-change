package com.bluesquare.rc.dao;

public interface TokenHandlerDAO {
	public String modifyQueryForSelectedCompaniesAccessForOtherEntities(String hql);
	public String getSessionSelectedCompaniesIds();
	public long getAppLoggedEmployeeId();
	public String getEndTextForSMS();
}
