package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.EmployeeChatStatus;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;

public interface ChatDAO {

	
	public List<Chat> fetchChatRecords(long employeeId,long firstChatId,int count,long lastChatId);
	public void setStatusByEmployeeId(long employeeId,String status);
	public EmployeeChatStatus getStatusByEmployeeId(long employeeId);
	public Chat saveChat(Chat chat);
	public void deleteChat(long chatId);
}
