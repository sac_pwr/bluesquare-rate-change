package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Categories;

public interface CategoriesDAO {


	
	//webapp

		public void saveCategoriesForWebApp(Categories categories);
		public void updateCategoriesForWebApp(Categories categories);
		public Categories fetchCategoriesForWebApp(long categoriesId);
		public List<Categories> fetchCategoriesListForWebApp();

}
