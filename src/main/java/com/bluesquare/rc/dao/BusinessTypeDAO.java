package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.BusinessType;

public interface BusinessTypeDAO {

	
	//webApp
	public void saveForWebApp(BusinessType businessType);

	public void updateForWebApp(BusinessType businessType);
	
	public List<BusinessType> fetchBusinessTypeListForWebApp();
	
	public BusinessType fetchBusinessTypeForWebApp(long businessTypeId);
	
	
}
