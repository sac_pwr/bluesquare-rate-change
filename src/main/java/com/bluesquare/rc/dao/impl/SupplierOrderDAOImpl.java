package com.bluesquare.rc.dao.impl;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.SupplierDAO;
import com.bluesquare.rc.dao.SupplierOrderDAO;
import com.bluesquare.rc.entities.Address;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OrderUsedBrand;
import com.bluesquare.rc.entities.OrderUsedCategories;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierOrder;
import com.bluesquare.rc.entities.SupplierOrderDetails;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.SupplierOrderProduct;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.OrderIdSupplierGenerator;
import com.bluesquare.rc.utils.SendSMS;

@Repository("supplierOrderDAO")
@Component
public class SupplierOrderDAOImpl extends TokenHandler implements SupplierOrderDAO {
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	SupplierOrder supplierOrder;
	
	@Autowired
	SupplierDAO supplierDAO;
	
	@Autowired
	OrderIdSupplierGenerator orderIdSupplierGenerator;
	
	@Autowired
	Company company;
	
	public SupplierOrderDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public SupplierOrderDAOImpl() {
	}

	@Transactional
	public void save(Address address) {
		sessionFactory.getCurrentSession().save(address);

	}
	@Transactional
	public void saveSupplierOrder(String productWithSupplier) {
		//SupplierDAOimpl supplierDAO=new SupplierDAOimpl(sessionFactory);
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
				
		double totalAmount=0;
		double totalAmountWithTax=0;
		long totalQuantity=0;
		String[] inventoryRecordList = productWithSupplier.split(",");
		
		for(int i=0; i<inventoryRecordList.length; i++)
		{
			String[] inventoryRecord=inventoryRecordList[i].split("-");
			
			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(inventoryRecord[1]), inventoryRecord[0]);
			totalAmount=totalAmount+(Long.parseLong(inventoryRecord[2])*supplierProductList.getSupplierRate());
			
			totalQuantity+=Long.parseLong(inventoryRecord[2]);
			
			Product product=new Product();
			product=productDAO.fetchProductForWebApp(Long.parseLong(inventoryRecord[1]));
			
			double productRateWithTax=supplierProductList.getSupplierRate()+((supplierProductList.getSupplierRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			System.out.println("MRP : "+productRateWithTax);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(productRateWithTax, product.getCategories().getIgst());
			
			totalAmountWithTax+=calculateProperTaxModel.getMrp()*Long.parseLong(inventoryRecord[2]);
		}

		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		supplierOrder.setCompany(company);
		
		supplierOrder.setStatus(true);
		supplierOrder.setTotalAmount(totalAmount);
		supplierOrder.setTotalAmountWithTax(totalAmountWithTax);
		supplierOrder.setTotalQuantity(totalQuantity);
		supplierOrder.setSupplierOrderDatetime(new Date());
		
		supplierOrder.setSupplierOrderId(orderIdSupplierGenerator.generate());
		sessionFactory.getCurrentSession().save(supplierOrder);				
		
		inventoryRecordList = productWithSupplier.split(",");
		
		Map<Long,List<SupplierOrderProduct>> mapSupplierOrderProduct=new HashMap<>();
		
		for(int i=0; i<inventoryRecordList.length; i++)
		{
			SupplierOrderDetails supplierOrderDetails=new SupplierOrderDetails();
			
			supplierOrderDetails.setSupplierOrder(supplierOrder);
			
			String[] inventoryRecord=inventoryRecordList[i].split("-");
			Product product=new Product();
			product=productDAO.fetchProductForWebApp(Long.parseLong(inventoryRecord[1]));
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst());
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					product.getProductImage(), 
					product.getProductContentType(),
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity());
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			supplierOrderDetails.setProduct(orderUsedProduct);
			
			supplierOrderDetails.setQuantity(Long.parseLong(inventoryRecord[2]));
			
			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(inventoryRecord[1]), inventoryRecord[0]);
			totalAmount=supplierProductList.getSupplierRate()*Long.parseLong(inventoryRecord[2]);
			supplierOrderDetails.setTotalAmount(totalAmount);
			
			totalAmountWithTax=0;
			
			double productRateWithTax=supplierProductList.getSupplierRate()+((supplierProductList.getSupplierRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			System.out.println("MRP : "+productRateWithTax);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(productRateWithTax, product.getCategories().getIgst());
			
			totalAmountWithTax=calculateProperTaxModel.getMrp()*Long.parseLong(inventoryRecord[2]);
			supplierOrderDetails.setTotalAmountWithTax(totalAmountWithTax);
			
			supplierOrderDetails.setSupplierRate(supplierProductList.getSupplierRate());	
			supplierOrderDetails.setSupplier(supplierDAO.fetchSupplier(inventoryRecord[0]));
			
			sessionFactory.getCurrentSession().save(supplierOrderDetails);
			
			List<SupplierOrderProduct> supplierOrderProductList=mapSupplierOrderProduct.get(Long.parseLong(inventoryRecord[3]));
			if(supplierOrderProductList==null)
			{
				supplierOrderProductList=new ArrayList<>();
				supplierOrderProductList.add(new SupplierOrderProduct(product.getProductName(), supplierOrderDetails.getQuantity()));
				mapSupplierOrderProduct.put(Long.parseLong(inventoryRecord[3]), supplierOrderProductList);
			}
			else
			{
				for(Map.Entry<Long,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
				{					 
					if(entry.getKey()==Long.parseLong(inventoryRecord[3]))
					{
						List<SupplierOrderProduct> supplierOrderProductList2=entry.getValue();
						supplierOrderProductList2.add(new SupplierOrderProduct(product.getProductName(), supplierOrderDetails.getQuantity()));
						mapSupplierOrderProduct.put(Long.parseLong(inventoryRecord[3]), supplierOrderProductList2);
					}
				}
			}
		}	
		
		System.out.println(" Supplier SMS going start .....");
		
		String startingText="";
		
		String loginType=(String)session.getAttribute("loginType");
		if(loginType.equals(Constants.GATE_KEEPER_DEPT_NAME)){
			
			EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
			startingText=supplierOrder.getSupplierOrderId()+"  Order Book From BlueSquare "+employeeDetails.getName()+"(GK-"+employeeDetails.getEmployee().getCompany().getCompanyName()+")";
			
		}else if(loginType.equals(Constants.ADMIN)){
			
			startingText=supplierOrder.getSupplierOrderId()+"  Order Book From BlueSquare  --Admin";
			
		}else if(loginType.equals(Constants.COMPANY_ADMIN)){
			
			Company company=(Company)session.getAttribute("companyDetails");
			startingText=supplierOrder.getSupplierOrderId()+"  Order Book From BlueSquare "+company.getCompanyName();
			
		}
		
		for(Map.Entry<Long,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
		{
			long mobileno=entry.getKey();
			
			String smsText=startingText+" : \r\n";
			
			for(SupplierOrderProduct supplierOrderProduct : entry.getValue())
			{
				smsText+=supplierOrderProduct.getProductName()+" - "+supplierOrderProduct.getQuatity()+" qty\r\n";
			}
			
			SendSMS.sendSMS(mobileno, smsText);
		}
		
		
		
	}
	
	@Transactional
	public void editSupplierOrder(String productWithSupplier, SupplierOrder supplierOrder) {
		
		//SupplierDAOimpl supplierDAO=new SupplierDAOimpl(sessionFactory);
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
				
		double totalAmount=0;
		double totalAmountWithTax=0;
		long totalQuantity=0;
		
		String[] inventoryRecordList = productWithSupplier.split(",");
		
		for(int i=0; i<inventoryRecordList.length; i++)
		{
			String[] inventoryRecord=inventoryRecordList[i].split("-");
			
			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(inventoryRecord[1]), inventoryRecord[0]);
			totalAmount=totalAmount+(Long.parseLong(inventoryRecord[2])*supplierProductList.getSupplierRate());

			totalQuantity+=Long.parseLong(inventoryRecord[2]);
			
			
			Product product=new Product();
			product=productDAO.fetchProductForWebApp(Long.parseLong(inventoryRecord[1]));
			double productRateWithTax=supplierProductList.getSupplierRate()+((supplierProductList.getSupplierRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			System.out.println("MRP : "+productRateWithTax);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(productRateWithTax, product.getCategories().getIgst());
			
			totalAmountWithTax+=calculateProperTaxModel.getMrp()*Long.parseLong(inventoryRecord[2]);
		}

		supplierOrder.setTotalAmount(totalAmount);
		supplierOrder.setTotalAmountWithTax(totalAmountWithTax);
		supplierOrder.setTotalQuantity(totalQuantity);
		supplierOrder.setSupplierOrderUpdateDatetime(new Date());
		
		supplierOrder=(SupplierOrder)sessionFactory.getCurrentSession().merge(supplierOrder);
		sessionFactory.getCurrentSession().update(supplierOrder);		
		
		String hql="from SupplierOrderDetails where supplierOrder.supplierOrderId='"+supplierOrder.getSupplierOrderId()+
				"' and supplierOrder.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierOrderDetails> supplierOrderDetailList=(List<SupplierOrderDetails>)query.list();
		for(SupplierOrderDetails supplierOrderDetail : supplierOrderDetailList)
		{
			supplierOrderDetail=(SupplierOrderDetails)sessionFactory.getCurrentSession().merge(supplierOrderDetail);
			sessionFactory.getCurrentSession().delete(supplierOrderDetail);	
		}
		
		inventoryRecordList = productWithSupplier.split(",");
		
		Map<Long,List<SupplierOrderProduct>> mapSupplierOrderProduct=new HashMap<>();
		
		for(int i=0; i<inventoryRecordList.length; i++)
		{
			SupplierOrderDetails supplierOrderDetails=new SupplierOrderDetails();
			
			supplierOrderDetails.setSupplierOrder(supplierOrder);
			
			String[] inventoryRecord=inventoryRecordList[i].split("-");
			Product product=new Product();
			product=productDAO.fetchProductForWebApp(Long.parseLong(inventoryRecord[1]));
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst());
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					product.getProductImage(), 
					product.getProductContentType(),
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity());
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			supplierOrderDetails.setProduct(orderUsedProduct);
			
			supplierOrderDetails.setQuantity(Long.parseLong(inventoryRecord[2]));
			
			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(inventoryRecord[1]), inventoryRecord[0]);
			totalAmount=supplierProductList.getSupplierRate()*Long.parseLong(inventoryRecord[2]);
			supplierOrderDetails.setTotalAmount(totalAmount);
			
			totalAmountWithTax=0;

			double productRateWithTax=supplierProductList.getSupplierRate()+((supplierProductList.getSupplierRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			System.out.println("MRP : "+productRateWithTax);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(productRateWithTax, product.getCategories().getIgst());
			
			totalAmountWithTax=calculateProperTaxModel.getMrp()*Long.parseLong(inventoryRecord[2]);
			supplierOrderDetails.setTotalAmountWithTax(totalAmountWithTax);
			
			supplierOrderDetails.setSupplierRate(supplierProductList.getSupplierRate());	
			supplierOrderDetails.setSupplier(supplierDAO.fetchSupplier(inventoryRecord[0]));
			
			sessionFactory.getCurrentSession().save(supplierOrderDetails);
			List<SupplierOrderProduct> supplierOrderProductList=mapSupplierOrderProduct.get(Long.parseLong(inventoryRecord[3]));
			if(supplierOrderProductList==null)
			{
				supplierOrderProductList=new ArrayList<>();
				supplierOrderProductList.add(new SupplierOrderProduct(product.getProductName(), supplierOrderDetails.getQuantity()));
				mapSupplierOrderProduct.put(Long.parseLong(inventoryRecord[3]), supplierOrderProductList);
			}
			else
			{
				for(Map.Entry<Long,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
				{					
					if(entry.getKey()==Long.parseLong(inventoryRecord[3]))
					{
						List<SupplierOrderProduct> supplierOrderProductList2=entry.getValue();
						supplierOrderProductList2.add(new SupplierOrderProduct(product.getProductName(), supplierOrderDetails.getQuantity()));
						mapSupplierOrderProduct.put(Long.parseLong(inventoryRecord[3]), supplierOrderProductList2);
					}
				}
			}
		}	
		
		System.out.println(" Supplier SMS going start .....");
		
		String startingText="";
		
		String loginType=(String)session.getAttribute("loginType");
		if(loginType.equals(Constants.GATE_KEEPER_DEPT_NAME)){
			
			EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
			startingText=supplierOrder.getSupplierOrderId()+"  Order Update From "+employeeDetails.getName()+"(GK-"+employeeDetails.getEmployee().getCompany().getCompanyName()+")";
			
		}else if(loginType.equals(Constants.ADMIN)){
			
			startingText=supplierOrder.getSupplierOrderId()+"  Order Update From  --Admin";
			
		}else if(loginType.equals(Constants.COMPANY_ADMIN)){
			
			Company company=(Company)session.getAttribute("companyDetails");
			startingText=supplierOrder.getSupplierOrderId()+"  Order Update From "+company.getCompanyName();
			
		}
		
		for(Map.Entry<Long,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
		{
			long mobileno=entry.getKey();
			String smsText=startingText+" : \r\n";
			
			for(SupplierOrderProduct supplierOrderProduct : entry.getValue())
			{
				smsText+=supplierOrderProduct.getProductName()+" - "+supplierOrderProduct.getQuatity()+" qty\r\n";
			}
			
			SendSMS.sendSMS(mobileno, smsText);
		}
	}
	
	@Transactional
	public SupplierOrder fetchSupplierOrder(String supplierOrderId)
	{		
		String hql="from SupplierOrder where supplierOrderId='"+supplierOrderId+"' ";
				hql+=" and company.companyId in ("+getSessionSelectedCompaniesIds()+") ";
		Query query= sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierOrder> supplierOrderList=(List<SupplierOrder>)query.list();
		if(supplierOrderList.isEmpty())
		{
			return null;
		}
		return supplierOrderList.get(0);
	}
	
	@Transactional	
	public List<SupplierOrder> fetchSupplierOrderList(String range,String startDate,String endDate)
	{
		String hql="";
		Calendar cal = Calendar.getInstance();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if (range.equals("today")) {
			hql="from SupplierOrder where date(supplierOrderDatetime) = date(CURRENT_DATE()) ";
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from SupplierOrder where date(supplierOrderDatetime) = '"+dateFormat.format(cal.getTime())+"' ";
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from SupplierOrder where date(supplierOrderDatetime) > '"+dateFormat.format(cal.getTime())+"'";
		}
		else if (range.equals("currentMonth")) {
			hql="from SupplierOrder where (date(supplierOrderDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(supplierOrderDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
		else if (range.equals("lastMonth")) {
			hql="from SupplierOrder where (date(supplierOrderDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(supplierOrderDatetime) <= '"+DatePicker.getLastMonthLastDate()+"') ";
		}
		else if (range.equals("last3Months")) {
			hql="from SupplierOrder where (date(supplierOrderDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(supplierOrderDatetime) <= '"+DatePicker.getLast3MonthLastDate()+"') ";
		}
		else if (range.equals("last6Months")) {
			hql="from SupplierOrder where (date(supplierOrderDatetime) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(supplierOrderDatetime) <= '"+DatePicker.getLast6MonthLastDate()+"') ";
		}
		else if (range.equals("range")) {
			hql="from SupplierOrder where (date(supplierOrderDatetime) >= '"+startDate+"' and date(supplierOrderDatetime) <= '"+endDate+"') ";
		}
		else if (range.equals("pickDate")) {
			hql="from SupplierOrder where (date(supplierOrderDatetime) = '"+startDate+"') ";
		}
		else if (range.equals("viewAll")) {
			hql="from SupplierOrder where 1=1 ";
		}
		
		hql+=" and company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
					 "  order by supplierOrderDatetime desc";		
		
		Query query= sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierOrder> supplierOrderList=(List<SupplierOrder>)query.list();
		if(supplierOrderList.isEmpty())
		{
			return null;
		}
		return supplierOrderList;
	}
	
	@Transactional	
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsList(String supplierOrderId)
	{
		String hql="from SupplierOrderDetails where supplierOrder.supplierOrderId='"+supplierOrderId+"'"
				+ " and supplierOrder.company.companyId="+getSessionSelectedCompaniesIds();
		Query query= sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierOrderDetails> supplierOrderDetailsList=(List<SupplierOrderDetails>)query.list();
		if(supplierOrderDetailsList.isEmpty())
		{
			return null;
		}
		return supplierOrderDetailsList;
	}

	

	@Transactional
	public void deleteSupplierOrder(String supplierOrderId){
		SupplierOrder supplierOrder=fetchSupplierOrder(supplierOrderId);
		supplierOrder.setStatus(false);
		supplierOrder=(SupplierOrder)sessionFactory.getCurrentSession().merge(supplierOrder);
		sessionFactory.getCurrentSession().update(supplierOrder);
		
		Map<Long,List<SupplierOrderProduct>> mapSupplierOrderProduct=new HashMap<>();
		List<SupplierOrderDetails> supplierOrderDetailsList=fetchSupplierOrderDetailsList(supplierOrderId);
		for(SupplierOrderDetails supplierOrderDetails: supplierOrderDetailsList){
			List<SupplierOrderProduct> supplierOrderProductList=mapSupplierOrderProduct.get(Long.parseLong(supplierOrderDetails.getSupplier().getContact().getMobileNumber()));
			if(supplierOrderProductList==null)
			{
				supplierOrderProductList=new ArrayList<>();
				supplierOrderProductList.add(new SupplierOrderProduct(supplierOrderDetails.getProduct().getProduct().getProductName(), supplierOrderDetails.getQuantity()));
				mapSupplierOrderProduct.put(Long.parseLong(supplierOrderDetails.getSupplier().getContact().getMobileNumber()), supplierOrderProductList);
			}
			else
			{
				for(Map.Entry<Long,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
				{					 
					if(entry.getKey()==supplierOrderDetails.getProduct().getProduct().getProductId())
					{
						List<SupplierOrderProduct> supplierOrderProductList2=entry.getValue();
						supplierOrderProductList2.add(new SupplierOrderProduct(supplierOrderDetails.getProduct().getProduct().getProductName(), supplierOrderDetails.getQuantity()));
						mapSupplierOrderProduct.put(Long.parseLong(supplierOrderDetails.getSupplier().getContact().getMobileNumber()), supplierOrderProductList2);
					}
				}
			}
		}
		
		System.out.println(" Supplier SMS going start .....");
		
		String startingText="";
		
		String loginType=(String)session.getAttribute("loginType");
		if(loginType.equals(Constants.GATE_KEEPER_DEPT_NAME)){
			
			EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
			startingText=supplierOrder.getSupplierOrderId()+"  Order Cancel From "+employeeDetails.getName()+"(GK-"+employeeDetails.getEmployee().getCompany().getCompanyName()+")";
			
		}else if(loginType.equals(Constants.ADMIN)){
			
			startingText=supplierOrder.getSupplierOrderId()+"  Order Cancel From  --Admin";
			
		}else if(loginType.equals(Constants.COMPANY_ADMIN)){
			
			Company company=(Company)session.getAttribute("companyDetails");
			startingText=supplierOrder.getSupplierOrderId()+"  Order Cancel From "+company.getCompanyName();
			
		}
		
		for(Map.Entry<Long,List<SupplierOrderProduct>> entry : mapSupplierOrderProduct.entrySet())
		{
			long mobileno=entry.getKey();
			String smsText=startingText+" : \r\n";
			
			for(SupplierOrderProduct supplierOrderProduct : entry.getValue())
			{
				smsText+=supplierOrderProduct.getProductName()+" - "+supplierOrderProduct.getQuatity()+" qty\r\n";
			}
			
			SendSMS.sendSMS(mobileno, smsText);
		}
		
	}
	
}
