package com.bluesquare.rc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.models.LedgerPaymentView;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DatePicker;

@Repository("ledgerDAO")
@Component
public class LedgerDAOImpl extends TokenHandler implements LedgerDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Transactional
	public void saveLedger(Ledger ledger) {
		sessionFactory.getCurrentSession().save(ledger);
	}

	@Transactional
	public void updateLedger(Ledger ledger) {
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().update(ledger);
	}

	@Transactional
	public List<Ledger> fetchAfterLedgerList(long lederId) {
		String hql="from Ledger where ledgerId>"+lederId;
		hql+=" and company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Ledger> ledgerList=(List<Ledger>)query.list();
		if(ledgerList.isEmpty()){
			return null;
		}
		return ledgerList;
	}
	
	@Transactional
	public List<Ledger> fetchBeforeLedgerList(long  lederId) {
		String hql="from Ledger where ledgerId<"+lederId;
		hql+=" and company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Ledger> ledgerList=(List<Ledger>)query.list();
		if(ledgerList.isEmpty()){
			return null;
		}
		return ledgerList;
	}

	@Transactional
	public Ledger fetchLedger(String type,String id){
		String hql="from Ledger where 1=1 ";
		
		if(type.equals("supplier")){
			hql+=" and paymentPaySupplier.paymentPayId="+id;
		}else if(type.equals("counter")){
			hql+=" and paymentCounter.paymentCounterId="+id;
		}else if(type.equals("order")){
			hql+=" and payment.paymentId="+id;
		}else if(type.equals("employee")){
			hql+=" and employeeSalary.employeeSalaryId="+id;
		}else if(type.equals("expense")){
			hql+=" and expense.expenseId='"+id+"'";
		}
		hql+=" and company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Ledger> ledgerList=(List<Ledger>)query.list();
		if(ledgerList.isEmpty()){
			return null;
		}
		return ledgerList.get(0);
	}
	
	@Transactional
	public List<LedgerPaymentView> fetchLedgerPaymentView(String startDate, String endDate,
			String range){
		
		String hql="";
		Query query;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if (range.equals("lastMonth")) {			
			hql="from Ledger where date(dateTime)>='"+DatePicker.getLastMonthFirstDate()+"' and date(dateTime)<='"+DatePicker.getLastMonthLastDate()+"'";			
		}
		else if (range.equals("last3Months")) {
			cal.add(Calendar.MONTH, -12);
			hql="from Ledger where date(dateTime)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(dateTime)<='"+DatePicker.getLast3MonthLastDate()+"'";			
		}
		else if (range.equals("viewAll")) {
			hql="from Ledger where 1=1 ";			
		}
		else if (range.equals("currentMonth")) {
			hql="from Ledger where date(dateTime)>='"+DatePicker.getCurrentMonthStartDate()+"' and date(dateTime)<='"+DatePicker.getCurrentMonthLastDate()+"'";			
		}
		else if (range.equals("range")) {
			hql="from Ledger where date(dateTime)>='"+startDate+"' and date(dateTime)<='"+endDate+"'";			
		}
		else if (range.equals("pickDate")) {
			hql="from Ledger where date(dateTime)='"+startDate+"'";			
		}
		else if (range.equals("today")) {			
			hql="from Ledger where date(dateTime)=date(CURRENT_DATE())";			
		}
		else if (range.equals("yesterday")) {		
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from Ledger where date(dateTime)='"+dateFormat.format(cal.getTime())+"'";			
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from Ledger where date(dateTime)>='"+dateFormat.format(cal.getTime())+"'";			
		}
		hql+=" and company.companyId in ("+getSessionSelectedCompaniesIds()+") ";
		hql+=" order by dateTime desc";
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Ledger> ledgerList=(List<Ledger>)query.list();
		
		List<LedgerPaymentView> ledgerPaymentViews=new ArrayList<>();
		
		for(Ledger ledger: ledgerList){
			ledgerPaymentViews.add(new LedgerPaymentView(	ledger.getDateTime(), 
															ledger.getDescription(), 
															ledger.getReference(), 
															ledger.getPayMode(), 
															ledger.getDebit(),
															ledger.getCredit(), 
															ledger.getBalance()));
		}
		return ledgerPaymentViews;
	}
	
	@Transactional
	public void createLedgerEntry(Ledger ledger){
		Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		
		double balance=company.getBalance();
		double debit=ledger.getDebit();	
		double credit=ledger.getCredit();	
		
		if(debit==0){
			balance=balance+credit;
		}else{
			balance=balance-debit;
		}
		
		ledger.setCompany(company);
		ledger.setBalance(balance);
		saveLedger(ledger);
		company.setBalance(balance);
		companyDAO.updateCompany(company);
	}
	
	@Transactional
	public void updateBalanceLedgerListAfterGivenLedgerId(long ledgerId,double balance){
		Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		List<Ledger> ledgerListAfter=fetchAfterLedgerList(ledgerId);
		if(ledgerListAfter!=null){			
			for(Ledger ledger2: ledgerListAfter){
				if(ledger2.getCredit()==0){
					balance=balance-ledger2.getDebit();
					ledger2.setBalance(balance);
				}else{
					balance=balance+ledger2.getCredit();
					ledger2.setBalance(balance);
				}
				updateLedger(ledger2);
			}			
		}
		company.setBalance(balance);
		companyDAO.updateCompany(company);
	}
}
