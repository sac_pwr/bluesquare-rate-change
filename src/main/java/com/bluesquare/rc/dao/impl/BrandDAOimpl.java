package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.BrandDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Company;

@Repository("brandDAO")
@Component
public class BrandDAOimpl extends TokenHandler implements BrandDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	AreaDAO areaDAO;
		
	@Autowired
	Company company;
	
	
	@Transactional
	public void saveBrandForWebApp(Brand brand) {
		// TODO Auto-generated method stub
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		brand.setCompany(company);
		sessionFactory.getCurrentSession().save(brand);
	}

	@Transactional
	public void updateBrandForWebApp(Brand brand) {
		// TODO Auto-generated method stub
		brand=(Brand)sessionFactory.getCurrentSession().merge(brand);
		sessionFactory.getCurrentSession().update(brand);
	}

	@Transactional
	public Brand fetchBrandForWebApp(long brandId) {
		String hql="from Brand where brandId="+brandId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Brand> list=(List<Brand>)query.list();
		if(list.isEmpty())
		{
			return null;
		}		
		return list.get(0);
	}

	@Transactional
	public List<Brand> fetchBrandListForWebApp() {
		String hql="from Brand where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql+=" order by name";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Brand> list=(List<Brand>)query.list();
		if(list.isEmpty())
		{
			return null;
		}		
		return list;
	}

}
