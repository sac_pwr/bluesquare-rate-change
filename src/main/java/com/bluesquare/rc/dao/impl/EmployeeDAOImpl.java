package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDAO;
import com.bluesquare.rc.entities.AppVersion;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Supplier;


@Repository("employeeDAO")

@Component
public class EmployeeDAOImpl extends TokenHandler implements EmployeeDAO{

	@Autowired 
	SessionFactory sessionFactory;
	
	@Autowired
	Employee employee;	
	
	@Autowired
	Company company;
	
	public EmployeeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public EmployeeDAOImpl() { }

	
	@Transactional
	public Employee validate(String username, String password) {
		String hql = "from Employee where userId= :user and password=:pass";// and department.name='"+Constants.GATE_KEEPER_DEPT_NAME+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("user", username);
		query.setParameter("pass", password);
		
		@SuppressWarnings("unchecked")
		List<Employee> list = (List<Employee>) query.list();

		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	@Transactional
    public String checkAppVersion(String appVersion){
            // TODO Auto-generated method stub
            
            String hql="from AppVersion ";
            Query query=sessionFactory.getCurrentSession().createQuery(hql);
            List<AppVersion> appVersions=(List<AppVersion>)query.list();
            if(appVersions.get(0).getAppVersion().equalsIgnoreCase(appVersion)){
                    return "UptoDate";
            }
            return "Update available";
    }


	@Transactional
	public EmployeeDetails getEmployeeDetails(long employeeId) {
		String hql = "from EmployeeDetails where employee=" + employeeId+
				" and employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeDetailsList= (List<EmployeeDetails>) query.list();
		if (employeeDetailsList.isEmpty()) {
			return null;
		}
		return employeeDetailsList.get(0);
	}

	@Transactional
	public Employee loginCredentialsForRC(String userid, String password) {
		
		try {
			String hql = "from Employee where userId= :user and password=:pass";
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("user", userid);
			query.setParameter("pass", password);
			
			@SuppressWarnings("unchecked")
			List<Employee> listEmp=(List<Employee>) query.list();
			if(listEmp.isEmpty()){
				return null;
			}
			return listEmp.get(0);
		} catch (Exception e) {
			System.out.println("loginCredentialsForRC Error : " + e.toString());
			return null;
		}
				
	}
	
	@Transactional
	public void logout(long employeeId) {
		// TODO Auto-generated method stub

		/*try {
			String hql = "from EmployeeDetails where employee.employeeId =" + employeeId;
			hql+=" and employee.company.companyId="+getSessionSelectedCompaniesIds();
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeDetails> employeeDetails = (List<EmployeeDetails>) query.list();
			employeeDetails.get(0).setToken("");
			EmployeeDetails employeeDetails2=employeeDetails.get(0);  
			
			employeeDetails2=(EmployeeDetails)sessionFactory.getCurrentSession().merge(employeeDetails2);
			sessionFactory.getCurrentSession().update(employeeDetails2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.toString());
		}*/
	}
	


	@Transactional
	public void saveForWebApp(Employee employee) {
		// TODO Auto-generated method stub

		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		employee.setCompany(company);
		sessionFactory.getCurrentSession().save(employee);
	}

	@Transactional
	public void updateForWebApp(Employee employee) {
		// TODO Auto-generated method stub
		employee=(Employee)sessionFactory.getCurrentSession().merge(employee);
		sessionFactory.getCurrentSession().update(employee);
	}

	@Transactional
	public String checkEmployeeDuplication(String checkText,String type,long employeeDetailsId){
		
		String hql="";
		if(type.equals("userId")){
			hql="from EmployeeDetails where employee.userId='"+checkText+"'";
		}else if(type.equals("mobileNumber")){
			hql="from EmployeeDetails where contact.mobileNumber='"+checkText+"'";
		}else if(type.equals("emailId")){
			hql="from EmployeeDetails where contact.emailId='"+checkText+"'";
		}		
		if(employeeDetailsId!=0){
			hql+=" and employeeDetailsId<>'"+employeeDetailsId+"'";
		}
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeDetails> employeeList=(List<EmployeeDetails>)query.list();
		
		if(type.equals("userId")){
			hql="from Company where userId='"+checkText+"'";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<Company> companyList=(List<Company>)query.list();
			
			if(employeeList.isEmpty() && companyList.isEmpty()){
				return "Success";
			}else{
				return "Failed";
			}
		}
		
		if(employeeList.isEmpty()){
			return "Success";
		}
		
		return "Failed";		
	}


}