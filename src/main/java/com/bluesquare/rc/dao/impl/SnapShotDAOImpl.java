package com.bluesquare.rc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.SnapShotDAO;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.rest.models.SnapShotReportResponse;
import com.bluesquare.rc.utils.Constants;



@Repository("snapShotDAO")

@Component
public class SnapShotDAOImpl extends TokenHandler implements SnapShotDAO {
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	

	@SuppressWarnings("unchecked")
	@Transactional
	public SnapShotReportResponse fetchRecordForSnapShot(long employeeId, String fromDate, String toDate,
			String range) {
		// TODO Auto-generated method stub
		
		SnapShotReportResponse snapShotReportResponse=new SnapShotReportResponse();
		String hql="";
		Query query;
		long noOfOrderDetails=0;
		long noOfOrderDetailsCanceled=0;
		double totalAmtSale=0;
		double totalRtnAmt=0;
		double retplaceOrderAmt=0;;
		double totalCollectAmt=0;
		double totalIssueAmt=0;
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		
		if(range.equals("range"))
						{
							hql="from OrderDetails where employeeSM.employeeId=" +employeeId + "  And (date(orderDetailsAddedDatetime)>='"+fromDate+"'  And date(orderDetailsAddedDatetime)<='"+toDate+"')"+
									" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by orderDetailsAddedDatetime desc";
							
							query=sessionFactory.getCurrentSession().createQuery(hql);
							List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
							
							for(OrderDetails orderDetailsList1:orderDetailsList)
							{
								if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
								{
									noOfOrderDetailsCanceled+=1;
								}								
								noOfOrderDetails+=1;
							}
							//snapShotReportResponse.setTotalNoOrderTaken(NoOfOrderDetails);
							
							for(OrderDetails orderDetailsList1:orderDetailsList)
							{
								if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
								{
									totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
									totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
								}								
							}
							
							hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And (date(returnOrderProductDatetime)>='"+fromDate+"'  And date(returnOrderProductDatetime)<='"+toDate+"')"+
									" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
							query=sessionFactory.getCurrentSession().createQuery(hql);
							List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
							for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
							{
								totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
							}
							
							hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And (date(reIssueDate)>='"+fromDate+"'  And date(reIssueDate)<='"+toDate+"')"+
									" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
							query=sessionFactory.getCurrentSession().createQuery(hql);
							List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
							for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
							{
								retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
							}
							
							hql="from Payment where status=false and orderDetails.employeeSM.employeeId=" +employeeId + " And (date(paidDate)>='"+fromDate+"'  And date(paidDate)<='"+toDate+"')"+
									" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by paidDate desc";
							query=sessionFactory.getCurrentSession().createQuery(hql);
							List<Payment> paymentList=(List<Payment>)query.list();
							for(Payment paymentList1:paymentList)
							{
								totalCollectAmt+=paymentList1.getPaidAmount();
							}
							
							/*// in below 3 lines we are fetching  returnOrderProductList By orderId
								hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
								
								// here we calculate the totalRtn amt
									for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
									{
										totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
									}
									
									// in below 3 line we calculate the payment list by orderId 
									hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<Payment> PaymentList=(List<Payment>)query.list();
									
									// here we calculate the total paid amount
									for(Payment paymentList1:PaymentList)
									{
										totalCollectAmt+=paymentList1.getPaidAmount();
									}
								
								
							}*/
							
							}
			else if(range.equals("last7days"))
								{
									cal.add(Calendar.DAY_OF_MONTH, -7);
									hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
											" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by orderDetailsAddedDatetime desc";
											
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
									
									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											noOfOrderDetailsCanceled+=1;
										}
										else
										{
											noOfOrderDetails+=1;
										}
									}
									
									snapShotReportResponse.setTotalNoOrderTaken(noOfOrderDetails);
									
									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}								
									}
									
									
									hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
									for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
									{
										totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
									}
									
									hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'"+
											" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
									for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
									{
										retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
									}
									
									hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'"+
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by paidDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<Payment> paymentList=(List<Payment>)query.list();
									for(Payment paymentList1:paymentList)
									{
										totalCollectAmt+=paymentList1.getPaidAmount();
									}
									
									
									/*
										// in below 3 lines we are fetching  returnOrderProductList By orderId
										hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										
										// here we calculate the totalRtn amt
											for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
											{
												totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
											}
											
											// in below 3 line we calculate the payment list by orderId 
											hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<Payment> PaymentList=(List<Payment>)query.list();
											
											// here we calculate the total paid amount
											for(Payment paymentList1:PaymentList)
											{
												totalCollectAmt+=paymentList1.getPaidAmount();
											}
										
										
									}*/
								}
			else if(range.equals("last1month"))
								{
									cal.add(Calendar.MONTH, -1);
									hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
											" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by orderDetailsAddedDatetime desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											noOfOrderDetailsCanceled+=1;
										}
										else
										{
											noOfOrderDetails+=1;
										}
									}
									
									snapShotReportResponse.setTotalNoOrderTaken(noOfOrderDetails);
									
									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}								
									}
									
									
									hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
									for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
									{
										totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
									}
									
									hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'"+
											" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
									for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
									{
										retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
									}
									
									hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'"+
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by paidDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<Payment> paymentList=(List<Payment>)query.list();
									for(Payment paymentList1:paymentList)
									{
										totalCollectAmt+=paymentList1.getPaidAmount();
									}
										
										
										
										/*
										// in below 3 lines we are fetching  returnOrderProductList By orderId
										hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										
										// here we calculate the totalRtn amt
											for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
											{
												totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
											}
											
											// in below 3 line we calculate the payment list by orderId 
											hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<Payment> PaymentList=(List<Payment>)query.list();
											
											// here we calculate the total paid amount
											for(Payment paymentList1:PaymentList)
											{
												totalCollectAmt+=paymentList1.getPaidAmount();
											}
										
										
									}*/
							
								}
			else if(range.equals("last3months"))
									{
										cal.add(Calendar.MONTH, -3);
										hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
												" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by orderDetailsAddedDatetime desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

										for(OrderDetails orderDetailsList1:orderDetailsList)
										{
											if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
											{
												noOfOrderDetailsCanceled+=1;
											}
											else
											{
												noOfOrderDetails+=1;
											}
										}
										
										snapShotReportResponse.setTotalNoOrderTaken(noOfOrderDetails);
										
										for(OrderDetails orderDetailsList1:orderDetailsList)
										{
											if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
											{
												totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
												totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
											}								
										}
										
										
										hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'"+
												" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
										
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
										{
											totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
										}
										
										hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'"+
												" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
										for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
										{
											retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
										}
										
										hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'"+
												" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by paidDate desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<Payment> paymentList=(List<Payment>)query.list();
										for(Payment paymentList1:paymentList)
										{
											totalCollectAmt+=paymentList1.getPaidAmount();
										}
											/*
											// in below 3 lines we are fetching  returnOrderProductList By orderId
											hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
											
											// here we calculate the totalRtn amt
												for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
												{
													totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
												}
												
												// in below 3 line we calculate the payment list by orderId 
												hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
												query=sessionFactory.getCurrentSession().createQuery(hql);
												List<Payment> PaymentList=(List<Payment>)query.list();
												
												// here we calculate the total paid amount
												for(Payment paymentList1:PaymentList)
												{
													totalCollectAmt+=paymentList1.getPaidAmount();
												}
											
											
										}*/
										}
			else if(range.equals("pickDate"))
									{
										hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)='"+fromDate +"'"+
												" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by orderDetailsAddedDatetime desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

										for(OrderDetails orderDetailsList1:orderDetailsList)
										{
											if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
											{
												noOfOrderDetailsCanceled+=1;
											}
											else
											{
												noOfOrderDetails+=1;
											}
										}
										
										snapShotReportResponse.setTotalNoOrderTaken(noOfOrderDetails);
										
										for(OrderDetails orderDetailsList1:orderDetailsList)
										{
											if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
											{
												totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
												totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
											}								
										}
										
										
										hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)='"+dateFormat.format(cal.getTime())+"'"+
												" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
										{
											totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
										}
										
										hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)='"+dateFormat.format(cal.getTime())+"'"+
												" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
										for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
										{
											retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
										}
										
										hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)='"+dateFormat.format(cal.getTime())+"'"+
												" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by paidDate desc";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<Payment> paymentList=(List<Payment>)query.list();
										for(Payment paymentList1:paymentList)
										{
											totalCollectAmt+=paymentList1.getPaidAmount();
										}
											
											/*// in below 3 lines we are fetching  returnOrderProductList By orderId
											hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
											
											// here we calculate the totalRtn amt
												for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
												{
													totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
												}
												
												// in below 3 line we calculate the payment list by orderId 
												hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
												query=sessionFactory.getCurrentSession().createQuery(hql);
												List<Payment> PaymentList=(List<Payment>)query.list();
												
												// here we calculate the total paid amount
												for(Payment paymentList1:PaymentList)
												{
													totalCollectAmt+=paymentList1.getPaidAmount();
												}
											
											
										}*/
									}
			else if(range.equals("viewAll"))
								{
									hql="from OrderDetails where employeeSM.employeeId=" +employeeId+
											" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by orderDetailsAddedDatetime desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											noOfOrderDetailsCanceled+=1;
										}
										else
										{
											noOfOrderDetails+=1;
										}
									}
									
									snapShotReportResponse.setTotalNoOrderTaken(noOfOrderDetails);
									
									for(OrderDetails orderDetailsList1:orderDetailsList)
									{
										if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
										{
											totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
											totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
										}								
									}
									
									
									hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId +
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
									
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
									for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
									{
										totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
									}
									
									hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId +
											" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
									for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
									{
										retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
									}
									
									hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId +
											" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by paidDate desc";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<Payment> paymentList=(List<Payment>)query.list();
									for(Payment paymentList1:paymentList)
									{
										totalCollectAmt+=paymentList1.getPaidAmount();
									}
										
										
										/*// in below 3 lines we are fetching  returnOrderProductList By orderId
										hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
										
										// here we calculate the totalRtn amt
											for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
											{
												totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
											}
											
											// in below 3 line we calculate the payment list by orderId 
											hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
											query=sessionFactory.getCurrentSession().createQuery(hql);
											List<Payment> PaymentList=(List<Payment>)query.list();
											
											// here we calculate the total paid amount
											for(Payment paymentList1:PaymentList)
											{
												totalCollectAmt+=paymentList1.getPaidAmount();
											}
										
										
									}*/
								}
			else if(range.equals("currentDate"))
							{
								hql="from OrderDetails where employeeSM.employeeId=" +employeeId +" And date(orderDetailsAddedDatetime)=date(CURRENT_DATE()) "+
										" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by orderDetailsAddedDatetime desc";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();

								for(OrderDetails orderDetailsList1:orderDetailsList)
								{
									if(orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
									{
										noOfOrderDetailsCanceled+=1;
									}
									else
									{
										noOfOrderDetails+=1;
									}
								}
								
								snapShotReportResponse.setTotalNoOrderTaken(noOfOrderDetails);
								
								for(OrderDetails orderDetailsList1:orderDetailsList)
								{
									if(!orderDetailsList1.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_CANCELED))
									{
										totalAmtSale+=orderDetailsList1.getTotalAmountWithTax();// here we calculate TotalAmtSale
										totalIssueAmt+=orderDetailsList1.getIssuedTotalAmountWithTax();// here we calculate total Issue Amt
									}								
								}
								
								
								hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId=" +employeeId + "  And date(returnOrderProductDatetime)='"+dateFormat.format(cal.getTime())+"'"+
										" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
								for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
								{
									totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
								}
								
								hql="from ReIssueOrderDetails where returnOrderProduct.orderDetails.employeeSM.employeeId=" +employeeId + "  And date(reIssueDate)='"+dateFormat.format(cal.getTime())+"'"+
										" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
								for(ReIssueOrderDetails reIssueOrderDetails:reIssueOrderDetailsList)
								{
									retplaceOrderAmt+=reIssueOrderDetails.getTotalAmountWithTax();
								}
								
								hql="from Payment where status=false and  orderDetails.employeeSM.employeeId=" +employeeId + " And date(paidDate)='"+dateFormat.format(cal.getTime())+"'"+
										" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by paidDate desc";
								query=sessionFactory.getCurrentSession().createQuery(hql);
								List<Payment> paymentList=(List<Payment>)query.list();
								for(Payment paymentList1:paymentList)
								{
									totalCollectAmt+=paymentList1.getPaidAmount();
								}
/*									// in below 3 lines we are fetching  returnOrderProductList By orderId
									hql="from ReturnOrderProduct where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
									query=sessionFactory.getCurrentSession().createQuery(hql);
									List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
									
									// here we calculate the totalRtn amt
										for(ReturnOrderProduct returnOrderProductList1:returnOrderProductList)
										{
											totalRtnAmt+=returnOrderProductList1.getTotalAmountWithTax();
										}
										
										// in below 3 line we calculate the payment list by orderId 
										hql="from Payment where orderDetails.orderId='"+OrderDetailsList1.getOrderId()+"'";
										query=sessionFactory.getCurrentSession().createQuery(hql);
										List<Payment> PaymentList=(List<Payment>)query.list();
										
										// here we calculate the total paid amount
										for(Payment paymentList1:PaymentList)
										{
											totalCollectAmt+=paymentList1.getPaidAmount();
										}
									
									
								}*/
							}
			
		
		
		
		
		
		//hql="from OrderDetails where employeeSM.employeeId="+employeeId;
		snapShotReportResponse.setTotalNoOrderTaken(noOfOrderDetails);
		snapShotReportResponse.setTotalNoOrderCanceled(noOfOrderDetailsCanceled);
		snapShotReportResponse.setTotalAmtSales(totalAmtSale);
		snapShotReportResponse.setTotalIssueAmt(totalIssueAmt);
		snapShotReportResponse.setRtnOrderAmt(totalRtnAmt);
		snapShotReportResponse.setReplaceOrderAmt(retplaceOrderAmt);
		snapShotReportResponse.setTotalCollectionAmt(totalCollectAmt);
		
		if(snapShotReportResponse.equals(null))
		{
			return null;
		}
		
		return snapShotReportResponse;
	}

}

