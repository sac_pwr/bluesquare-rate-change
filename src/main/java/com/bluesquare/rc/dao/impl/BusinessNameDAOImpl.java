package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.BusinessNameDAO;
import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.dao.CounterOrderDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.models.BusinessNameList;
import com.bluesquare.rc.models.CustomerReport;
import com.bluesquare.rc.models.CustomerReportView;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.utils.BusinessNameIdGenerator;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.SendSMS;

@Repository("businessNameDAO")

@Component
public class BusinessNameDAOImpl extends TokenHandler implements BusinessNameDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	BusinessName businessName;
	
	@Autowired
	AreaDAO areaDAO;
	
	@Autowired
	Company company;
	
	@Autowired
	ContactDAO contactDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	BusinessNameIdGenerator businessNameIdGenerator;
	
	@Autowired
	CounterOrderDAO counterOrderDAO;
	
	@Transactional
	public void saveBusinessName(BusinessName businessName) {
		// TODO Auto-generated method stub
		
		company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		businessName.setCompany(company);
		
		/*AddressDAOImpl addressDAOImpl=new AddressDAOImpl(sessionFactory);
		addressDAOImpl.save(businessName.getAddress());*/
		
		//AreaDAOImpl areaDAO=new AreaDAOImpl(sessionFactory);
		Area area=areaDAO.fetchAreaForWebApp(businessName.getArea().getAreaId());
		if(area.getRegion().getCity().getState().getName().equals(Constants.MAHARASHTRA_STATE_NAME))
		{
			businessName.setTaxType(Constants.INTRA_STATUS);
		}
		else
		{
			businessName.setTaxType(Constants.INTER_STATUS);
		}
		//ContactDAOImpl contactDAO=new ContactDAOImpl(sessionFactory);
		contactDAO.save(businessName.getContact());
		businessName.setBusinessNameId(businessNameIdGenerator.generateBusinessNameId());
		businessName.setBusinessAddedDatetime(new Date());
		businessName.setBusinessUpdatedDatetime(new Date());
		businessName.setStatus(false);
		businessName.setDisableDatetime(new Date());
		
		sessionFactory.getCurrentSession().save(businessName);
		try {
			long mobileno=Long.parseLong(businessName.getContact().getMobileNumber());
			String smsText="Welcome To "+company.getCompanyName()+", Your Account Number is : "+businessName.getBusinessNameId();
			SendSMS.sendSMS(mobileno, smsText);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	//for WebApp

	@Transactional
	public void saveForWebApp(BusinessName businessName) {
		// TODO Auto-generated method stub
				
		company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		businessName.setCompany(company);
		
		/*AddressDAOImpl addressDAOImpl=new AddressDAOImpl(sessionFactory);
		addressDAOImpl.save(businessName.getAddress());*/
		
		//AreaDAOImpl areaDAO=new AreaDAOImpl(sessionFactory);
		Area area=areaDAO.fetchAreaForWebApp(businessName.getArea().getAreaId());
		if(area.getRegion().getCity().getState().getName().equals(Constants.MAHARASHTRA_STATE_NAME))
		{
			businessName.setTaxType(Constants.INTRA_STATUS);
		}
		else
		{
			businessName.setTaxType(Constants.INTER_STATUS);
		}
		
		//ContactDAOImpl contactDAO=new ContactDAOImpl(sessionFactory);
		contactDAO.save(businessName.getContact());
		
		businessName.setBusinessNameId(businessNameIdGenerator.generateBusinessNameId());
		
		sessionFactory.getCurrentSession().save(businessName);
		try {
			long mobileno=Long.parseLong(businessName.getContact().getMobileNumber());
			String smsText="Welcome To "+company.getCompanyName()+", Your Account Number is : "+businessName.getBusinessNameId();
			SendSMS.sendSMS(mobileno, smsText);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Transactional
	public void updateForWebApp(BusinessName businessName) {
		// TODO Auto-generated method stub
		
		/*AddressDAOImpl addressDAOImpl=new AddressDAOImpl(sessionFactory);
		addressDAOImpl.update(businessName.getAddress());*/
		
		//AreaDAOImpl areaDAO=new AreaDAOImpl(sessionFactory);
		Area area=areaDAO.fetchAreaForWebApp(businessName.getArea().getAreaId());
		if(area.getRegion().getCity().getState().getName().equals(Constants.MAHARASHTRA_STATE_NAME))
		{
			businessName.setTaxType(Constants.INTRA_STATUS);
		}
		else
		{
			businessName.setTaxType(Constants.INTER_STATUS);
		}
		
		//ContactDAOImpl contactDAO=new ContactDAOImpl(sessionFactory);
		contactDAO.update(businessName.getContact());
		
		businessName=(BusinessName)sessionFactory.getCurrentSession().merge(businessName);
		sessionFactory.getCurrentSession().update(businessName);
		
		try {
			long mobileno=Long.parseLong(businessName.getContact().getMobileNumber());
			String smsText="Welcome To "+businessName.getCompany().getCompanyName()+", Your Account Number is : "+businessName.getBusinessNameId()+", Your Account Edited SuccessFully ";
			SendSMS.sendSMS(mobileno, smsText);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Transactional
	public List<BusinessName> getBusinessNameListForWebApp() {
		String hql = "from BusinessName where 1=1 ";
		hql=areaDAO.modifyQueryForSelectedAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);		
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessName> businessNameList = (List<BusinessName>) query.list();

		if (businessNameList.isEmpty()) {
			return null;
		}
		return businessNameList;
	}

	@Transactional
	public BusinessName fetchBusinessForWebApp(String businessNameId) {
		String hql = "from BusinessName where businessNameId='" + businessNameId+"'";
		hql=areaDAO.modifyQueryForSelectedAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessName> businessNameList = (List<BusinessName>) query.list();
		if (businessNameList.isEmpty()) {
			return null;
		}
		return businessNameList.get(0);
	}
	@Transactional
    public List<BusinessName> businessNameByAreaIdAndBusinessTypeIdForWebApp(long businessTypeId, long areaId) {

        Query query = null;
        String hql;
        
        if (areaId == 0 && businessTypeId == 0) {
            return null;
        } else if (businessTypeId == 0) {

            hql = "from BusinessName where area.areaId=" + areaId;

        } else if (areaId == 0) {
            hql = "from BusinessName where businessType.businessTypeId=" + businessTypeId;

        } else {
            hql = "from BusinessName where businessType.businessTypeId=" + businessTypeId + " and  area.areaId="+ areaId;
        }

        hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
        
        query = sessionFactory.getCurrentSession().createQuery(hql);
        List<BusinessName> businessNameList = (List<BusinessName>) query.list();
        if (businessNameList.isEmpty()) {
            return null;

        }
        return businessNameList;

    }

	@Transactional
	public List<BusinessNameList> fetchBusinessNameList() {
		List<BusinessNameList> businessNameLists=new ArrayList<>();
		String hql = "from BusinessName where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
        List<BusinessName> businessNameList = (List<BusinessName>) query.list();
        
        if (businessNameList.isEmpty()) {
            return null;
        }
        double duePayment=0;
        for(BusinessName businessName : businessNameList)
        {
        	duePayment=0;
        	String hql2="from OrderDetails where businessName.businessNameId='"+businessName.getBusinessNameId()+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')"
        			+ " and businessName.company.companyId="+getSessionSelectedCompaniesIds();
        	Query query2 = sessionFactory.getCurrentSession().createQuery(hql2);
        	List<OrderDetails> orderDetailsList=(List<OrderDetails>)query2.list();
        	for(OrderDetails orderDetails : orderDetailsList)
        	{
        		if(orderDetails.isPayStatus()==false)
        		{
        			String hql3="from Payment where status=false and orderDetails.orderId='"+orderDetails.getOrderId()+"'"
        					+ " and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
                	Query query3 = sessionFactory.getCurrentSession().createQuery(hql3);
                	List<Payment> paymentList=(List<Payment>)query3.list();
                	if(paymentList.isEmpty())
                	{
                		duePayment=+duePayment+orderDetails.getIssuedTotalAmountWithTax();
                	}
                	else
                	{
                		duePayment=+duePayment+paymentList.get(0).getDueAmount();
                	}
                	
        		}
        	}
        	businessNameLists.add(new BusinessNameList(businessName, duePayment));
        }
        
		return businessNameLists;
	}
	
	/*public static void main(String[] args) {
		String smsText="Do your order payment as early as posible \n --Bluesquare";
		SendSMS.sendSMS(8425986282L, smsText);
	}*/
	
	@Transactional
	public String sendSMSTOShops(String shopsId,String smsText,String mobileNumber) {

		try {
			String endText=getEndTextForSMS();			
				
			String[] shopsId2 = shopsId.split(",");
			
			if(shopsId2.length==1){
				SendSMS.sendSMS(Long.parseLong(mobileNumber), smsText+endText);
				System.out.println("SMS send to : "+shopsId2[0]);
				return "Success";
			}
			
			for(int i=0; i<shopsId2.length; i++)
			{
				businessName=fetchBusinessForWebApp(shopsId2[i]);
				SendSMS.sendSMS(Long.parseLong(businessName.getContact().getMobileNumber()), smsText+endText);
				System.out.println("SMS send to : "+shopsId2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}
	@Transactional
	public List<BusinessName> fetchBusinessNameByAreaId(long areaId) {
		
		String hql="from BusinessName where area.areaId="+areaId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> businessNameList=(List<BusinessName>)query.list();
		if(businessNameList.isEmpty()){
			return null;
		}
		
		return businessNameList;
	}
	
	@Transactional
	public List<BusinessName> fetchBusinessNameByAreaIds(String areaIds) {
		
		String hql="from BusinessName where area.areaId in ("+areaIds+") ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> businessNameList=(List<BusinessName>)query.list();
		if(businessNameList.isEmpty()){
			return null;
		}
		
		return businessNameList;
	}
	
	@Transactional
	public CustomerReportView fetchBusinessNameForReport(String range,String startDate,String endDate){
		
		List<CustomerReport> customerReportList=new ArrayList<>();
		
		String hql="from BusinessName where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<BusinessName> businessNameList=(List<BusinessName>)query.list();
		long srno=1;
		double totalAmountAllBusiness=0;
		for(BusinessName businessName : businessNameList){
			double totalAmount=0;
			long noOfOrders=0;
			//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
			List<OrderReportList> orderReportList=orderDetailsDAO.showOrderReportByBusinessNameId(businessName.getBusinessNameId(), range, startDate, endDate);
			List<OrderReportList> counterOrderReportList=counterOrderDAO.showCounterOrderReportByBusinessNameId(businessName.getBusinessNameId(), range, startDate, endDate);
			
			//counterOrders add in salesman orders
			if(orderReportList!=null && counterOrderReportList!=null){
				orderReportList.addAll(counterOrderReportList);
			}else if(orderReportList==null && counterOrderReportList!=null){
				orderReportList=new ArrayList<>();
				orderReportList.addAll(counterOrderReportList);
			}
			
			
			if(orderReportList!=null)
			{
				for(OrderReportList orderReport:orderReportList)
				{
					totalAmount+=orderReport.getAmountWithTax();
				}
				noOfOrders=orderReportList.size();
			}
			customerReportList.add(new CustomerReport(srno, 
													businessName.getBusinessNameId(), 
													businessName.getShopName(), 
													totalAmount, 
													businessName.getArea().getName(), 
													businessName.getArea().getRegion().getName(), 
													businessName.getArea().getRegion().getCity().getName(), 
													businessName.getArea().getRegion().getCity().getState().getName(), 
													businessName.getArea().getRegion().getCity().getState().getCountry().getName(), 
													businessName.getBusinessAddedDatetime(),
													noOfOrders));
			srno++;
			totalAmountAllBusiness+=totalAmount;
		}
		
		//AreaDAOImpl areaDAO=new AreaDAOImpl(sessionFactory);		
		
		return new CustomerReportView(totalAmountAllBusiness, customerReportList, areaDAO.fetchAllAreaForWebApp());
	}
	
	@Transactional
	public String checkBusinessDuplication(String checkText,String type,String businessNameId){
		
		String hql="";
		if(type.equals("telephoneNumber")){
			hql="from BusinessName where contact.telephoneNumber='"+checkText+"'";
		}else if(type.equals("gstNo")){
			hql="from BusinessName where gstinNumber='"+checkText+"'";
		}else if(type.equals("mobileNumber")){
			hql="from BusinessName where contact.mobileNumber='"+checkText+"'";
		}else if(type.equals("emailId")){
			hql="from BusinessName where contact.emailId='"+checkText+"'";
		}		
		if(!businessNameId.equals("0")){
			hql+=" and businessNameId<>'"+businessNameId+"'";
		}
		hql+=" and company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Supplier> supplierList=(List<Supplier>)query.list();
		if(supplierList.isEmpty()){
			return "Success";
		}
		return "Failed";		
	}
}
