package com.bluesquare.rc.dao.impl;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BusinessNameDAO;
import com.bluesquare.rc.dao.CounterOrderDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.OrderStatusDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.entities.OrderUsedBrand;
import com.bluesquare.rc.entities.OrderUsedCategories;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.CategoryWiseAmountForBill;
import com.bluesquare.rc.models.CounterOrderReport;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.PaymentCounterReport;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.models.ProductListForBill;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.CounterOrderIdGenerator;
import com.bluesquare.rc.utils.DateDiff;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.InvoiceNumberGenerate;
import com.bluesquare.rc.utils.NumberToWordsConverter;

@Repository("counterOrderDAO")
@Component
public class CounterOrderDAOImpl extends TokenHandler implements CounterOrderDAO {
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	OrderStatusDAO orderStatusDAO;
	
	@Autowired
	CounterOrder counterOrder;
	
	@Autowired
	BusinessName businessName;
	
	@Autowired
	HttpSession session;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	LedgerDAO ledgerDAO;
	
	@Autowired
	BusinessNameDAO businessNameDAO;
	
	@Autowired
	CounterOrderIdGenerator counterOrderIdGenerator; 
	
	@Autowired
	InvoiceNumberGenerate invoiceNumberGenerate;
	
	@Transactional
	public String saveCounterOrder(String roductDetailsList,
			String businessNameId,String paidAmount,String balAmount,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo) {

		counterOrder=new CounterOrder();
		String productDataList[]=roductDetailsList.split("-");
		double totalAmount=0,totalAmountWithTax=0;
		long totalQuantity=0;
		
		List<CounterOrderProductDetails> counterOrderProductDetailsList=new ArrayList<>();
		
		for(String productData : productDataList){
			String productDataSplit[]=productData.split(",");
			
			String productId=productDataSplit[0];
			String qty=productDataSplit[1];	
			String mrp=productDataSplit[2];
			String total=productDataSplit[3];
			String type=productDataSplit[4];
			
			Product product=productDAO.fetchProductForWebApp(Long.parseLong(productId));
			
			//less Current quantity
			product.setCurrentQuantity(product.getCurrentQuantity()-Long.parseLong(qty));
			productDAO.Update(product);
			//update daily stock
			productDAO.updateDailyStockExchange(product.getProductId(), Long.parseLong(qty), false);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(Float.parseFloat(mrp), product.getCategories().getIgst());
			
			totalAmount+=calculateProperTaxModel.getUnitprice()*Long.parseLong(qty);
			totalAmountWithTax+=Float.parseFloat(total);
			totalQuantity+=Long.parseLong(qty);
			
			CounterOrderProductDetails counterOrderProductDetails=new CounterOrderProductDetails(); 
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst());
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					product.getProductImage(),
					product.getProductContentType(),
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity());
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			counterOrderProductDetails.setProduct(orderUsedProduct);
			counterOrderProductDetails.setPurchaseAmount(Float.parseFloat(total));
			counterOrderProductDetails.setPurchaseQuantity(Long.parseLong(qty));
			counterOrderProductDetails.setSellingRate(Float.parseFloat(mrp));
			counterOrderProductDetails.setType(type);
			
			counterOrderProductDetailsList.add(counterOrderProductDetails);
		}
		if(businessNameId.equals("0")){
			counterOrder.setCustomerGstNumber(gstNo);
			counterOrder.setCustomerMobileNumber(mobileNo);
			counterOrder.setCustomerName(custName);			
		}else{
			businessName=businessNameDAO.fetchBusinessForWebApp(businessNameId);
			counterOrder.setBusinessName(businessName);
		}
		//CounterOrderIdGenerator counterOrderIdGenerator=new CounterOrderIdGenerator(sessionFactory);
		counterOrder.setCounterOrderId(counterOrderIdGenerator.generate());
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");	
		counterOrder.setEmployeeGk(employeeDetails.getEmployee());
		
		counterOrder.setDateOfOrderTaken(new Date());
		
		//InvoiceNumberGenerate invoiceNumberGenerate=new InvoiceNumberGenerate(sessionFactory);
		counterOrder.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber());
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		try {	
			if(paymentType.equals("PartialPay") || paymentType.equals("Credit")){
			counterOrder.setPaymentDueDate(dateFormat.parse(dueDate));
			}
		} catch (ParseException e) {}
		
		counterOrder.setPayStatus(false);
		counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED));
		counterOrder.setTotalAmount(totalAmount);
		counterOrder.setTotalAmountWithTax(totalAmountWithTax);
		counterOrder.setTotalQuantity(totalQuantity);
		
		sessionFactory.getCurrentSession().save(counterOrder);
		
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
			counterOrderProductDetails.setCounterOrder(counterOrder);
			sessionFactory.getCurrentSession().save(counterOrderProductDetails);
		}
		
		//payment section start
		PaymentCounter paymentCounter=new PaymentCounter();	
		
		Employee employee=new Employee(); 
		employee.setEmployeeId(getAppLoggedEmployeeId());
		paymentCounter.setEmployee(employee);
		
		if(paymentType.equals("InstantPay")){
			paymentCounter.setCounterOrder(counterOrder);
			//paymentCounter.setBalanceAmount(0);
			//try { paymentCounter.setDueDate(null); } catch (Exception e) {}
			paymentCounter.setCurrentAmountPaid(totalAmountWithTax);
			paymentCounter.setCurrentAmountRefund(0);
			//paymentCounter.setTotalAmountPaid(totalAmountWithTax);
			paymentCounter.setLastDueDate(new Date());
			paymentCounter.setPaidDate(new Date());
			paymentCounter.setPayType(payType);
			if(payType.equals("Cash")){				
			}else{
				paymentCounter.setBankName(bankName);
				paymentCounter.setChequeNumber(chequeNumber);
				try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
			}
			paymentCounter.setChequeClearStatus(true);
			sessionFactory.getCurrentSession().save(paymentCounter);
			
			//ledger entry create
			double credit=paymentCounter.getCurrentAmountPaid();
			Ledger ledger=new Ledger(
										(paymentCounter.getPayType().equals("Cash"))?paymentCounter.getPayType():paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
										paymentCounter.getPaidDate(), 
										null, 
										paymentCounter.getCounterOrder().getCounterOrderId()+" Payment", 
										(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
										0, 
										credit, 
										0, 
										paymentCounter
									);
			ledgerDAO.createLedgerEntry(ledger);
			//ledger entry created
			
			counterOrder.setPayStatus(true);
			sessionFactory.getCurrentSession().update(counterOrder);
			
		}else if(paymentType.equals("Credit")){
			//No Payment
		}else{ //PartialPay
			paymentCounter.setCounterOrder(counterOrder);
			//paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
			try { paymentCounter.setDueDate(dateFormat.parse(dueDate)); 
				  paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
			} catch (Exception e) {}
			paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
			paymentCounter.setCurrentAmountRefund(0);
			//paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
			paymentCounter.setPaidDate(new Date());
			paymentCounter.setPayType(payType);
			if(payType.equals("Cash")){				
			}else{
				paymentCounter.setBankName(bankName);
				paymentCounter.setChequeNumber(chequeNumber);
				try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
			}
			paymentCounter.setChequeClearStatus(true);
			sessionFactory.getCurrentSession().save(paymentCounter);
			
			//ledger entry create
			double credit=paymentCounter.getCurrentAmountPaid();
			Ledger ledger=new Ledger(
						(paymentCounter.getPayType().equals("Cash"))?paymentCounter.getPayType():paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
						paymentCounter.getPaidDate(), 
						null, 
						paymentCounter.getCounterOrder().getCounterOrderId()+" Payment", 
						(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
						0, 
						credit, 
						0, 
						paymentCounter
					);
			ledgerDAO.createLedgerEntry(ledger);
			//ledger entry created
		}/*
		if(paymentCounter.getCounterOrder().getBusinessName()!=null){
			BusinessName businessName=businessNameDAO.fetchBusinessForWebApp(paymentCounter.getCounterOrder().getBusinessName().getBusinessNameId());
			paymentCounter.getCounterOrder().setBusinessName(businessName);
		}*/
		
		
		//payment section end
		
		//update order used product update current quantity
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
		return counterOrder.getCounterOrderId();
	}
	
	@Transactional
	public String updateCounterOrderForEdit(String counterOrderId,String roductDetailsList,
			String businessNameId,String paidAmount,String balAmount,String refAmount,String paymentSituation,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo) {

		counterOrder=fetchCounterOrder(counterOrderId);
		double oldTotalAmountWithTax=counterOrder.getTotalAmountWithTax();
		List<CounterOrderProductDetails> counterOrderProductDetailsListOld=fetchCounterOrderProductDetails(counterOrderId);
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrderId);
				
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsListOld) {
			
			Product product=counterOrderProductDetails.getProduct().getProduct();
			
			//less Current quantity
			product.setCurrentQuantity(product.getCurrentQuantity()+counterOrderProductDetails.getPurchaseQuantity());
			productDAO.Update(product);
			
			//update daily stock
			productDAO.updateDailyStockExchange(product.getProductId(), counterOrderProductDetails.getPurchaseQuantity(), true);
			
			orderDetailsDAO.deleteOrderUsedProduct(counterOrderProductDetails.getProduct());
			
			counterOrderProductDetails=(CounterOrderProductDetails)sessionFactory.getCurrentSession().merge(counterOrderProductDetails);
			sessionFactory.getCurrentSession().delete(counterOrderProductDetails);
		}
		
		String productDataList[]=roductDetailsList.split("-");
		double totalAmount=0,totalAmountWithTax=0;
		long totalQuantity=0;
		
		List<CounterOrderProductDetails> counterOrderProductDetailsList=new ArrayList<>();
		
		for(String productData : productDataList){
			String productDataSplit[]=productData.split(",");
			
			String productId=productDataSplit[0];
			String qty=productDataSplit[1];	
			String mrp=productDataSplit[2];
			String total=productDataSplit[3];
			String type=productDataSplit[4];
			
			Product product=productDAO.fetchProductForWebApp(Long.parseLong(productId));
			
			//less Current quantity
			product.setCurrentQuantity(product.getCurrentQuantity()-Long.parseLong(qty));
			productDAO.Update(product);
			//update daily stock
			productDAO.updateDailyStockExchange(product.getProductId(), Long.parseLong(qty), false);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(Float.parseFloat(mrp), product.getCategories().getIgst());
			
			totalAmount+=calculateProperTaxModel.getUnitprice()*Long.parseLong(qty);
			totalAmountWithTax+=Float.parseFloat(total);
			totalQuantity+=Long.parseLong(qty);
			
			CounterOrderProductDetails counterOrderProductDetails=new CounterOrderProductDetails(); 
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst());
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					product.getProductImage(),
					product.getProductContentType(),
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity());
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			counterOrderProductDetails.setProduct(orderUsedProduct);
			counterOrderProductDetails.setPurchaseAmount(Float.parseFloat(total));
			counterOrderProductDetails.setPurchaseQuantity(Long.parseLong(qty));
			counterOrderProductDetails.setSellingRate(Float.parseFloat(mrp));
			counterOrderProductDetails.setType(type);
			
			counterOrderProductDetailsList.add(counterOrderProductDetails);
		}
		if(businessNameId.equals("0")){
			counterOrder.setCustomerGstNumber(gstNo);
			counterOrder.setCustomerMobileNumber(mobileNo);
			counterOrder.setCustomerName(custName);	
			
			counterOrder.setBusinessName(null);
		}else{
			counterOrder.setCustomerGstNumber(null);
			counterOrder.setCustomerMobileNumber(null);
			counterOrder.setCustomerName(null);
			
			businessName=businessNameDAO.fetchBusinessForWebApp(businessNameId);
			counterOrder.setBusinessName(businessName);
		}
		
	/*	EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");	
		counterOrder.setEmployeeGk(employeeDetails.getEmployee());*/
		
		//counterOrder.setDateOfOrderTaken(new Date());
		
		/*InvoiceNumberGenerate invoiceNumberGenerate=new InvoiceNumberGenerate(sessionFactory);
		counterOrder.setInvoiceNumber(invoiceNumberGenerate.generateInvoiceNumber());*/
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		try {	
			if(!dueDate.equals("")) {
				if(paymentCounterList==null){
					counterOrder.setPaymentDueDate(dateFormat.parse(dueDate));
				}else{
					double totalPaid=0;
					for(PaymentCounter paymentCounter2: paymentCounterList){
						totalPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
					}
					if(totalPaid<totalAmountWithTax){
						counterOrder.setPaymentDueDate(dateFormat.parse(dueDate));
					}
				}
			}
		} catch (ParseException e) {}
		
		//counterOrder.setPayStatus(false);
		counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED));
		counterOrder.setTotalAmount(totalAmount);
		counterOrder.setTotalAmountWithTax(totalAmountWithTax);
		counterOrder.setTotalQuantity(totalQuantity);
		
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
		
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
			counterOrderProductDetails.setCounterOrder(counterOrder);
			sessionFactory.getCurrentSession().save(counterOrderProductDetails);
		}
		
		//payment section start
		boolean doLedgerEntry=true;
		PaymentCounter paymentCounter=new PaymentCounter();	
		Employee employee=new Employee(); 
		employee.setEmployeeId(getAppLoggedEmployeeId());
		paymentCounter.setEmployee(employee);
		if(paymentSituation.equals("Refund")){
			
			paymentCounter.setCounterOrder(counterOrder);
			//paymentCounter.setBalanceAmount(0);
			//try { paymentCounter.setDueDate(null); } catch (Exception e) {}
			paymentCounter.setCurrentAmountRefund(Float.parseFloat(refAmount));
			//paymentCounter.setTotalAmountPaid(totalAmountWithTax);
			paymentCounter.setPaidDate(new Date());
			paymentCounter.setPayType(payType);
			if(payType.equals("Cash")){				
			}else{
				paymentCounter.setBankName(bankName);
				paymentCounter.setChequeNumber(chequeNumber);
				try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
			}
			if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
				paymentCounter.setChequeClearStatus(true);
				sessionFactory.getCurrentSession().save(paymentCounter);
			}
			counterOrder.setPayStatus(true);
			sessionFactory.getCurrentSession().save(counterOrder);
			
			doLedgerEntry=false;
			
			/*counterOrder.setRefundAmount(Float.parseFloat(refAmount));
			counterOrder.setPaymentDueDate(null);
			counterOrder.setPayStatus(true);
			sessionFactory.getCurrentSession().update(counterOrder);*/
		}else{
			if(paymentCounterList!=null){
				double totalPaid=0;
				for(PaymentCounter paymentCounter2: paymentCounterList){
					totalPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
				}
				
				if(totalPaid==oldTotalAmountWithTax){
					double extraPay=totalAmountWithTax-oldTotalAmountWithTax;
					 	
					if(paymentType.equals("InstantPay")){
						
						paymentCounter.setCounterOrder(counterOrder);
						//paymentCounter.setBalanceAmount(0);
						//try { paymentCounter.setDueDate(null); } catch (Exception e) {}
						paymentCounter.setCurrentAmountPaid(extraPay);
						//paymentCounter.setTotalAmountPaid(totalAmountWithTax); 
						paymentCounter.setLastDueDate(new Date());							
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if(payType.equals("Cash")){				
						}else{
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
						}
						if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
							paymentCounter.setChequeClearStatus(true);
							sessionFactory.getCurrentSession().save(paymentCounter);
						}
						counterOrder.setPayStatus(true);
						sessionFactory.getCurrentSession().save(counterOrder);
						
						doLedgerEntry=true;
						
					}else if(paymentType.equals("Credit")){
						//No Payment
						doLedgerEntry=false;
					}else{ //PartialPay
						
						paymentCounter.setCounterOrder(counterOrder);
						//paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
						try { paymentCounter.setDueDate(dateFormat.parse(dueDate));  
						  paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
							} catch (Exception e) {}
						paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
						//paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if(payType.equals("Cash")){				
						}else{
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
						}
						if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
							paymentCounter.setChequeClearStatus(true);
							sessionFactory.getCurrentSession().save(paymentCounter);
						}
						doLedgerEntry=true;
					}
					
					
				}else{
					
					double partiallyPaid=totalPaid;//paymentCounterList.get(0).getTotalAmountPaid();
					double extraPay=totalAmountWithTax-partiallyPaid;					
						
					if(paymentType.equals("InstantPay")){
						paymentCounter.setCounterOrder(counterOrder);
						//paymentCounter.setBalanceAmount(0);						 
						paymentCounter.setLastDueDate(new Date());					
						paymentCounter.setCurrentAmountPaid(extraPay);
						//paymentCounter.setTotalAmountPaid(totalAmountWithTax);
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if(payType.equals("Cash")){				
						}else{
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
						}
						if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
							paymentCounter.setChequeClearStatus(true);
							sessionFactory.getCurrentSession().save(paymentCounter);
						}
						counterOrder.setPayStatus(true);
						sessionFactory.getCurrentSession().save(counterOrder);
						doLedgerEntry=true;
					}else if(paymentType.equals("Credit")){
						//No Payment
						doLedgerEntry=false;
					}else{ //PartialPay
						paymentCounter.setCounterOrder(counterOrder);
						//paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
						try { paymentCounter.setDueDate(dateFormat.parse(dueDate));  
						  paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
							} catch (Exception e) {}
						paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
						//paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
						paymentCounter.setPaidDate(new Date());
						paymentCounter.setPayType(payType);
						if(payType.equals("Cash")){				
						}else{
							paymentCounter.setBankName(bankName);
							paymentCounter.setChequeNumber(chequeNumber);
							try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
						}
						if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
							paymentCounter.setChequeClearStatus(true);
							sessionFactory.getCurrentSession().save(paymentCounter);
						}	
						doLedgerEntry=true;
					}
				}
				
				/*if(!paymentType.equals("Credit")){
					Collections.reverse(paymentCounterList);
					paymentCounterList.add(paymentCounter);
					double remainingAmt=totalAmountWithTax;
					for(PaymentCounter paymentCounter2 : paymentCounterList){
						if(paymentCounter2.getCurrentAmountPaid()==0){
							remainingAmt=remainingAmt+paymentCounter2.getCurrentAmountRefund();
							paymentCounter2.setTotalAmountPaid(totalAmountWithTax+remainingAmt);
						}else{
							remainingAmt=remainingAmt-paymentCounter2.getCurrentAmountPaid();
							paymentCounter2.setTotalAmountPaid(totalAmountWithTax-remainingAmt);
						}
						paymentCounter2.setBalanceAmount(remainingAmt);
						paymentCounter2=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter2);
						sessionFactory.getCurrentSession().update(paymentCounter2);
					}
				}*/
			}else{
						
				if(paymentType.equals("InstantPay")){
					paymentCounter.setCounterOrder(counterOrder);
					//paymentCounter.setBalanceAmount(0);
					//try { paymentCounter.setDueDate(null); } catch (Exception e) {}
					paymentCounter.setCurrentAmountPaid(totalAmountWithTax);
					//paymentCounter.setTotalAmountPaid(totalAmountWithTax); 
					paymentCounter.setLastDueDate(new Date());
					paymentCounter.setPaidDate(new Date());
					paymentCounter.setPayType(payType);
					if(payType.equals("Cash")){				
					}else{
						paymentCounter.setBankName(bankName);
						paymentCounter.setChequeNumber(chequeNumber);
						try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
					}
					if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
						paymentCounter.setChequeClearStatus(true);
						sessionFactory.getCurrentSession().save(paymentCounter);
					}
					counterOrder.setPayStatus(true);
					sessionFactory.getCurrentSession().save(counterOrder);
					doLedgerEntry=true;
				}else if(paymentType.equals("Credit")){
					//No Payment
					doLedgerEntry=false;
				}else{ //PartialPay
					paymentCounter.setCounterOrder(counterOrder);
					//paymentCounter.setBalanceAmount(Float.parseFloat(balAmount));
					try { paymentCounter.setDueDate(dateFormat.parse(dueDate)); 
					  paymentCounter.setLastDueDate(dateFormat.parse(dueDate));
						 } catch (Exception e) {}
					paymentCounter.setCurrentAmountPaid(Float.parseFloat(paidAmount));
					//paymentCounter.setTotalAmountPaid(Float.parseFloat(paidAmount));
					paymentCounter.setPaidDate(new Date());
					paymentCounter.setPayType(payType);
					if(payType.equals("Cash")){				
					}else{
						paymentCounter.setBankName(bankName);
						paymentCounter.setChequeNumber(chequeNumber);
						try { paymentCounter.setChequeDate(dateFormat.parse(chequeDate)); } catch (Exception e) {}
					}
					if(paymentCounter.getCurrentAmountPaid()!=0 || paymentCounter.getCurrentAmountRefund()!=0){
						paymentCounter.setChequeClearStatus(true);
						sessionFactory.getCurrentSession().save(paymentCounter);
					}		
					doLedgerEntry=true;
				}
			}
		}		
				
		if(doLedgerEntry){
			//ledger entry create		
			double credit=paymentCounter.getCurrentAmountPaid();
			double debit=paymentCounter.getCurrentAmountRefund();
			Ledger ledger=null;
			
			if(debit==0){
				ledger=new Ledger(
						(paymentCounter.getPayType().equals("Cash"))?paymentCounter.getPayType():paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
						paymentCounter.getPaidDate(), 
						null, 
						paymentCounter.getCounterOrder().getCounterOrderId()+" Payment", 
						(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
						0, 
						credit, 
						0, 
						paymentCounter
					);
			}else{
				ledger=new Ledger(
						(paymentCounter.getPayType().equals("Cash"))?paymentCounter.getPayType():paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
						paymentCounter.getPaidDate(), 
						null, 
						paymentCounter.getCounterOrder().getCounterOrderId()+" Refund", 
						(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
						debit, 
						0, 
						0, 
						paymentCounter
					);
			}
			
			ledgerDAO.createLedgerEntry(ledger);
			//ledger entry created
		}
		//payment section end
		
		//update order used product update current quantity
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
		return counterOrder.getCounterOrderId();
	}
	
	@Transactional
	public BillPrintDataModel fetchCounterBillPrintData(String counterOrderId)
	{
		
		//get counter order Details
		CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
		
		//get counter order product list 
		List<CounterOrderProductDetails> counterOrderProductDetailsList=fetchCounterOrderProductDetails(counterOrderId);
		
		BusinessName businessName=counterOrder.getBusinessName();
		
		String invoiceNumber=counterOrder.getInvoiceNumber();
		String orderDate=new SimpleDateFormat("dd-MM-yyyy").format(counterOrder.getDateOfOrderTaken());
		
		String deliveryDate=new SimpleDateFormat("dd-MM-yyyy").format(counterOrder.getDateOfOrderTaken());
		
		List<ProductListForBill> productListForBillList=new ArrayList<>();
		
		double totalAmountWithoutTax=0;
		
		double cGSTAmount=0;
		double iGSTAmount=0;
		double sGSTAmount=0;
		
		long totalQuantity=0;
		double totalAmountWithTax=0;
		String totalAmountWithTaxInWord="";
		
		List<CategoryWiseAmountForBill> categoryWiseAmountForBills=new ArrayList<>();
		
		double totalAmount=0;
		double taxAmount=0;
		String taxAmountInWord="";
		
		double totalCGSTAmount=0;
		double totalIGSTAmount=0;
		double totalSGSTAmount=0;
		
		DecimalFormat decimalFormat=new DecimalFormat("#0.00");
		
		long srno=1;
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
		{
			double amountWithoutTax=0;
			CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			
			if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{	
				amountWithoutTax=counterOrderProductDetails.getPurchaseQuantity()*calculateProperTaxModel.getUnitprice();
			
				totalAmountWithoutTax+=amountWithoutTax;
				
				float igst=counterOrderProductDetails.getProduct().getCategories().getIgst();
				float cgst=counterOrderProductDetails.getProduct().getCategories().getCgst();
				float sgst=counterOrderProductDetails.getProduct().getCategories().getSgst();
				
				double rate=calculateProperTaxModel.getUnitprice();
				long issuedQuantity=counterOrderProductDetails.getPurchaseQuantity();
				
				if(businessName!=null){
					if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
					{
						cGSTAmount+=( (rate*issuedQuantity)*cgst) /100;
						iGSTAmount+=0;
						sGSTAmount+=( (rate*issuedQuantity)*sgst) /100;
						
					}
					else
					{
						cGSTAmount+=0;
						iGSTAmount+=( (rate*issuedQuantity)*igst) /100;
						sGSTAmount+=0;						
					}
				}else{//external customer - assumed intra tax type
					cGSTAmount+=( (rate*issuedQuantity)*cgst) /100;
					iGSTAmount+=0;
					sGSTAmount+=( (rate*issuedQuantity)*sgst) /100;
				}
			}
			totalQuantity+=counterOrderProductDetails.getPurchaseQuantity();
			productListForBillList.add(new ProductListForBill(
					String.valueOf(srno),
					counterOrderProductDetails.getProduct().getProductName()+(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "-(Free)" :""), 
					counterOrderProductDetails.getProduct().getCategories().getHsnCode(),
					String.valueOf(((long)counterOrderProductDetails.getProduct().getCategories().getIgst())),
					String.valueOf(counterOrderProductDetails.getPurchaseQuantity()), 
					(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "0" :decimalFormat.format(calculateProperTaxModel.getUnitprice())), 
					(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_FREE)? "0.0" :decimalFormat.format(amountWithoutTax) )));
			srno++;
			//double tax=cGSTAmountSingle+iGSTAmountSingle+sGSTAmountSingle;
			//totalAmountWithTax+=amountWithoutTax+tax;	
			
		}
		
		for(ProductListForBill productListForBill2 :productListForBillList)
		{
			totalAmountWithTax+=Double.parseDouble(productListForBill2.getAmountWithoutTax());
		}
		totalAmountWithTax=Double.parseDouble(decimalFormat.format(totalAmountWithTax));
		cGSTAmount=Double.parseDouble(decimalFormat.format(cGSTAmount));
		iGSTAmount=Double.parseDouble(decimalFormat.format(iGSTAmount));
		sGSTAmount=Double.parseDouble(decimalFormat.format(sGSTAmount));
		
		totalAmountWithTax+=cGSTAmount+iGSTAmount+sGSTAmount;
		
		Set<OrderUsedCategories> categoriesList=new HashSet<>();
		
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
		{
			if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
			{
				categoriesList.add(counterOrderProductDetails.getProduct().getCategories());
			}
		}
		
		Set<String> categoriesHsnCodeList=new HashSet<>();
		for(OrderUsedCategories categories: categoriesList)
		{
			String hsncode="";			
			double taxableValue=0;
			float cgstPercentage=0; 
			float igstPercentage=0;			
			float sgstPercentage=0;
			float cgstRate=0; 
			float igstRate=0;
			float sgstRate=0;
			
			boolean gotDuplicateHsnCode=false;
			
			for(String hsnCode : categoriesHsnCodeList)
			{
				if(categories.getHsnCode().equals(hsnCode))
				{
					gotDuplicateHsnCode=true;
				}
			}
			if(gotDuplicateHsnCode)
			{
				continue;
			}
			
			categoriesHsnCodeList.add(categories.getHsnCode());
			
			for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList)
			{
				if(counterOrderProductDetails.getType().equals(Constants.ORDER_PRODUCT_DETAIL_TYPE_NON_FREE))
				{
					if(categories.getHsnCode().equals(counterOrderProductDetails.getProduct().getCategories().getHsnCode()))
					{
						CalculateProperTaxModel  calculateProperTaxModel=productDAO.calculateProperAmountModel(counterOrderProductDetails.getSellingRate(), 
								   counterOrderProductDetails.getProduct().getCategories().getIgst());
						taxableValue+=counterOrderProductDetails.getPurchaseQuantity()*calculateProperTaxModel.getUnitprice();
						
						float igst=counterOrderProductDetails.getProduct().getCategories().getIgst();
						float cgst=counterOrderProductDetails.getProduct().getCategories().getCgst();
						float sgst=counterOrderProductDetails.getProduct().getCategories().getSgst();
						double rate=calculateProperTaxModel.getUnitprice();
						long issuedQuantity=counterOrderProductDetails.getPurchaseQuantity();
						
						if(businessName!=null){
							if(businessName.getTaxType().equals(Constants.INTRA_STATUS))
							{
								cgstRate+=( (rate*issuedQuantity)*cgst) /100;
								igstRate+=0;
								sgstRate+=( (rate*issuedQuantity)*sgst) /100;
							}
							else
							{
								cgstRate+=0;
								igstRate+=( (rate*issuedQuantity)*igst) /100;
								sgstRate+=0;
							}
						}else { //external customer - assumed as Intra tax type
							cgstRate+=( (rate*issuedQuantity)*cgst) /100;
							igstRate+=0;
							sgstRate+=( (rate*issuedQuantity)*sgst) /100;
						}
						cgstPercentage=counterOrderProductDetails.getProduct().getCategories().getCgst();
						igstPercentage=counterOrderProductDetails.getProduct().getCategories().getIgst();	
						sgstPercentage=counterOrderProductDetails.getProduct().getCategories().getSgst();
						hsncode=counterOrderProductDetails.getProduct().getCategories().getHsnCode();
					}
				}
			}
			
			categoryWiseAmountForBills.add(new CategoryWiseAmountForBill(
					hsncode, 
					decimalFormat.format(taxableValue), 
					decimalFormat.format(cgstPercentage), 
					decimalFormat.format(cgstRate), 
					decimalFormat.format(igstPercentage), 
					decimalFormat.format(igstRate), 
					decimalFormat.format(sgstPercentage), 
					decimalFormat.format(sgstRate)));
			
			totalAmount+=taxableValue;
			totalCGSTAmount+=cgstRate;
			totalSGSTAmount+=sgstRate;
			totalIGSTAmount+=igstRate;			
		}
		taxAmount=(
				Double.parseDouble(decimalFormat.format(totalSGSTAmount))+
				Double.parseDouble(decimalFormat.format(totalCGSTAmount))+
				Double.parseDouble(decimalFormat.format(totalIGSTAmount)));
		taxAmountInWord=NumberToWordsConverter.convertWithPaisa(Double.parseDouble(decimalFormat.format(taxAmount)));
		
		totalAmountWithTax=Double.parseDouble(decimalFormat.format(totalAmountWithTax));
		double decimalAmount=totalAmountWithTax-(int)totalAmountWithTax;
		double roundOf;
		String roundOfAmount="";
		if(decimalAmount==0)
		{
			roundOf=0.0f;
			roundOfAmount=""+decimalFormat.format(roundOf);
			totalAmountWithTax=totalAmountWithTax+roundOf;
		}
		else if(decimalAmount>=0.5)
		{
			roundOf=1-decimalAmount;
			roundOfAmount="+"+decimalFormat.format(roundOf);
			totalAmountWithTax=totalAmountWithTax+roundOf;
		}
		else
		{
			roundOf=decimalAmount;
			roundOfAmount="-"+decimalFormat.format(roundOf);
			totalAmountWithTax=totalAmountWithTax-roundOf;
		}	
		
		/*if(decimalAmount>0.0f)
		{
			roundOf=1-decimalAmount;
			roundOfAmount="-"+Double.parseDouble(decimalFormat.format(roundOf));
			totalAmountWithTax=totalAmountWithTax-roundOf;
		}*/
		
		totalAmountWithTaxInWord=NumberToWordsConverter.convertWithPaisa(Double.parseDouble(decimalFormat.format(totalAmountWithTax)));
			if(businessName!=null){
				return new BillPrintDataModel(
											counterOrderId,
											invoiceNumber, 
											orderDate, 
											null,
											businessName, 
											null,
											deliveryDate, 
											productListForBillList, 
											decimalFormat.format(cGSTAmount), 
											decimalFormat.format(iGSTAmount), 
											decimalFormat.format(sGSTAmount), 
											roundOfAmount,
											decimalFormat.format(totalAmountWithoutTax),
											String.valueOf(totalQuantity), 
											decimalFormat.format(totalAmountWithTax), 
											totalAmountWithTaxInWord, 
											categoryWiseAmountForBills, 
											decimalFormat.format(totalAmount), 
											taxAmountInWord, 
											decimalFormat.format(totalCGSTAmount), 
											decimalFormat.format(totalIGSTAmount), 
											decimalFormat.format(totalSGSTAmount));
			}else {
				return new BillPrintDataModel(
						counterOrderId,
						invoiceNumber, 
						orderDate, 
						null,
						counterOrder.getCustomerName(),
						counterOrder.getCustomerMobileNumber(),
						counterOrder.getCustomerGstNumber(),
						counterOrder.getEmployeeGk(),
						deliveryDate, 
						productListForBillList, 
						decimalFormat.format(cGSTAmount), 
						decimalFormat.format(iGSTAmount), 
						decimalFormat.format(sGSTAmount), 
						roundOfAmount,
						decimalFormat.format(totalAmountWithoutTax),
						String.valueOf(totalQuantity), 
						decimalFormat.format(totalAmountWithTax), 
						totalAmountWithTaxInWord, 
						categoryWiseAmountForBills, 
						decimalFormat.format(totalAmount), 
						taxAmountInWord, 
						decimalFormat.format(totalCGSTAmount), 
						decimalFormat.format(totalIGSTAmount), 
						decimalFormat.format(totalSGSTAmount));
			}
	}

	@Transactional
	public void updateCounterOrder(CounterOrder counterOrder) {
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}

	@Transactional
	public CounterOrder fetchCounterOrder(String counterId) {
		// TODO Auto-generated method stub
		String hql="from CounterOrder where counterOrderId='"+counterId+"'"+
					" and employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		if(counterOrderList.isEmpty()){
			return null;
		}
		return counterOrderList.get(0);
	}

	@Transactional
	public List<CounterOrderProductDetails> fetchCounterOrderProductDetails(String counterId) {
		String hql="from CounterOrderProductDetails where counterOrder.counterOrderId='"+counterId+"'";
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrderProductDetails> CounterOrderProductDetailsList=(List<CounterOrderProductDetails>)query.list();
		if(CounterOrderProductDetailsList.isEmpty()){
			return null;
		}
		
		return CounterOrderProductDetailsList;
	}
	
	@Transactional
	public List<OrderProductDetailListForWebApp> fetchCounterOrderProductDetailsForShowOrderDetails(String counterId) {

		List<CounterOrderProductDetails> counterOrderProductDetailsList=fetchCounterOrderProductDetails(counterId);
		if(counterOrderProductDetailsList==null){
			return null;
		}
		List<OrderProductDetailListForWebApp> orderProductDetailListForWebAppList=new ArrayList<>();
		long srno=1;
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(
					counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getProduct().getCategories().getIgst());
			float totalAmountWithoutTax=calculateProperTaxModel.getUnitprice()*counterOrderProductDetails.getPurchaseQuantity();
			
			orderProductDetailListForWebAppList.add(new OrderProductDetailListForWebApp(srno, 
					counterOrderProductDetails.getCounterOrder().getCounterOrderId(), 
					counterOrderProductDetails.getProduct(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrderProductDetails.getCounterOrder().getEmployeeGk().getEmployeeId()).getName(), 
					counterOrderProductDetails.getCounterOrder().getEmployeeGk().getEmployeeId(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrderProductDetails.getCounterOrder().getEmployeeGk().getEmployeeId()).getEmployeeDetailsGenId(), 
					counterOrderProductDetails.getSellingRate(), 
					counterOrderProductDetails.getPurchaseQuantity(), 					
					totalAmountWithoutTax, 
					counterOrderProductDetails.getPurchaseAmount(), 
					counterOrderProductDetails.getCounterOrder().getDateOfOrderTaken(), 
					counterOrderProductDetails.getType()));
			srno++;
		}
		return orderProductDetailListForWebAppList;
	}
	
	@Transactional
	public List<CounterOrder> fetchCounterOrderByRange(String businessNameId,String range,String startDate,String endDate){
		
		String hql="";
		Calendar cal = Calendar.getInstance();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if (range.equals("today")) {
			hql="from CounterOrder where date(dateOfOrderTaken) = date(CURRENT_DATE()) ";
		}
		else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from CounterOrder where date(dateOfOrderTaken) = '"+dateFormat.format(cal.getTime())+"' ";
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from CounterOrder where date(dateOfOrderTaken) > '"+dateFormat.format(cal.getTime())+"'";
		}
		else if (range.equals("currentMonth")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(dateOfOrderTaken) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
		else if (range.equals("lastMonth")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(dateOfOrderTaken) <= '"+DatePicker.getLastMonthLastDate()+"') ";
		}
		else if (range.equals("last3Months")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(dateOfOrderTaken) <= '"+DatePicker.getLast3MonthLastDate()+"') ";
		}
		else if (range.equals("last6Months")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(dateOfOrderTaken) <= '"+DatePicker.getLast6MonthLastDate()+"') ";
		}
		else if (range.equals("range")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) >= '"+startDate+"' and date(dateOfOrderTaken) <= '"+endDate+"') ";
		}
		else if (range.equals("pickDate")) {
			hql="from CounterOrder where (date(dateOfOrderTaken) = '"+startDate+"') ";
		}
		else if (range.equals("viewAll")) {
			hql="from CounterOrder where 1=1 ";
		}else if (range.equals("TopProducts")) {
			hql="from CounterOrder where 1=1 ";
		}
		
		if(businessNameId!=null){
			hql+=" and employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
				 " and businessName.businessNameId='"+businessNameId+"'"+
				 "  order by dateOfOrderTaken desc";
		}else{
			hql+=" and employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+
					 "  order by dateOfOrderTaken desc";
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		if(counterOrderList.isEmpty()){
			return null;
		}
		return counterOrderList;
	}
	
	@Transactional
	public List<OrderReportList> showCounterOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate)
	{
		
		List<CounterOrder> counterOrderlist=fetchCounterOrderByRange(businessNameId, range, startDate, endDate);
		if(counterOrderlist==null)
		{
			return null;
		}
		
		//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
		List<OrderReportList> list2=new ArrayList<>();
		long srno=1;
		for(CounterOrder counterOrder : counterOrderlist){
			double amount=0,amountWithTax=0;
			long quantity=0;
			
			SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd"); 
			SimpleDateFormat simpleTimeFormat=new SimpleDateFormat("HH:mm:ss");
			
			String orderStatusSM = "";
			String orderStatusSMDate = "";
			String orderStatusSMTime = "";
			String orderStatusGK = "";
			String orderStatusGKDate = "";
			String orderStatusGKTime = "";
			String orderStatusDB = "";
			String orderStatusDBDate = "";
			String orderStatusDBTime = "";
			
			orderStatusSM="--";
			orderStatusSMDate="--";
			orderStatusSMTime="--";
			orderStatusGK=counterOrder.getOrderStatus().getStatus();
			orderStatusGKDate=simpleDateFormat.format(counterOrder.getDateOfOrderTaken());
			orderStatusGKTime= simpleTimeFormat.format(counterOrder.getDateOfOrderTaken());
			orderStatusDB = "--";
			orderStatusDBDate = "--";
			orderStatusDBTime= "--";
			amount=counterOrder.getTotalAmount();
			amountWithTax=counterOrder.getTotalAmountWithTax();
			quantity=counterOrder.getTotalQuantity();
					
			list2.add(new OrderReportList(srno,
					counterOrder.getCounterOrderId(),
					amount,
					amountWithTax,
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getArea().getName():"NA",
					quantity, 
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getShopName():counterOrder.getCustomerName(), 
					(counterOrder.getBusinessName()!=null)?counterOrder.getBusinessName().getBusinessNameId():"NA", 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getName(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getEmployeeDetailsId(),
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getEmployeeDetailsGenId(),
					(counterOrder.getPaymentDueDate()!=null)?DateDiff.findDifferenceInTwoDatesByDay(counterOrder.getDateOfOrderTaken(), counterOrder.getPaymentDueDate()):0, 
					counterOrder.getDateOfOrderTaken(),
							orderStatusSM,
							orderStatusSMDate,
							orderStatusSMTime,
							orderStatusGK,
							orderStatusGKDate,
							orderStatusGKTime,
							orderStatusDB,
							orderStatusDBDate,
							orderStatusDBTime,
							Constants.ORDER_STATUS_DELIVERED));
			srno++;
		}
		
		return list2;
	}
	
	@Transactional
	public List<CounterOrderReport> fetchCounterOrderReport(String range,String startDate,String endDate){
	
		List<CounterOrder> counterOrderlist=fetchCounterOrderByRange(null, range, startDate, endDate);
		if(counterOrderlist==null)
		{
			return null;
		}
		
		List<CounterOrderReport> counterOrderReportList=new ArrayList<>();
		int srno=01;
		for(CounterOrder counterOrder : counterOrderlist){
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
			
			String customerName;			
			if(counterOrder.getBusinessName()==null){
				customerName=counterOrder.getCustomerName();
			}else{
				customerName=counterOrder.getBusinessName().getShopName();
			}
			
			String paymentDate;
			if(counterOrder.getPaymentDueDate()==null){
				paymentDate="NA";
			}else{
				paymentDate=dateFormat.format(counterOrder.getPaymentDueDate());
			}
			
			String paymentStatus;
			List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrder.getCounterOrderId());
			if(paymentCounterList==null){
				paymentStatus="UnPaid";
			}else{
				double totalAmountPaid=0;
				for(PaymentCounter paymentCounter: paymentCounterList){
					totalAmountPaid+=paymentCounter.getCurrentAmountPaid()-paymentCounter.getCurrentAmountRefund();
				}
				totalAmountPaid=(totalAmountPaid>0)?totalAmountPaid:0;
				if(totalAmountPaid>=counterOrder.getTotalAmountWithTax()){
					paymentStatus="Paid";
				}else if(totalAmountPaid==0){
					paymentStatus="UnPaid";
				}
				else{
					paymentStatus="Partially Paid";
				}
			}
			String orderTakenDate=dateFormat.format(counterOrder.getDateOfOrderTaken());
			String orderTakenTime=timeFormat.format(counterOrder.getDateOfOrderTaken());
			
			counterOrderReportList.add(new CounterOrderReport(srno,
															counterOrder.getCounterOrderId(), 
															employeeDetailsDAO.getEmployeeDetailsByemployeeId(counterOrder.getEmployeeGk().getEmployeeId()).getName(), 
															customerName, 
															counterOrder.getTotalAmount(), 
															counterOrder.getTotalAmountWithTax(),
															counterOrder.getTotalQuantity(),
															counterOrder.getOrderStatus().getStatus(),
															paymentDate, 
															paymentStatus, 
															orderTakenDate, 
															orderTakenTime));
			srno++;
		}
		
		return counterOrderReportList;
	}
	
	@Transactional
	public void savePaymentCounter(PaymentCounter paymentCounter){
		Employee employee=new Employee(); 
		employee.setEmployeeId(getAppLoggedEmployeeId());
		paymentCounter.setEmployee(employee);

		sessionFactory.getCurrentSession().save(paymentCounter);
		
		//ledger entry create
		double credit=paymentCounter.getCurrentAmountPaid();
		Ledger ledger=new Ledger(
									(paymentCounter.getPayType().equals("Cash"))?paymentCounter.getPayType():paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
									paymentCounter.getPaidDate(), 
									paymentCounter.getChequeDate(), 
									paymentCounter.getCounterOrder().getCounterOrderId()+" Payment", 
									(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
									0, 
									credit, 
									0, 
									paymentCounter
								);
		ledgerDAO.createLedgerEntry(ledger);
		//ledger entry created
		
	}
	
	@Transactional
	public List<PaymentCounter> fetchPaymentCounterListByCounterOrderId(String counterOrderId){
		
		String hql="from PaymentCounter where status=false and counterOrder.counterOrderId='"+counterOrderId+"'"
				+ " and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+")  order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		if(paymentCounterList.isEmpty()){
			return null;
		}
		return paymentCounterList;
	}
	
	@Transactional
	public List<PaymentCounterReport> fetchPaymentCounterReportListByCounterOrderId(String counterOrderId){
		
		String hql="from PaymentCounter where status=false and counterOrder.counterOrderId='"+counterOrderId+"' "
				+ " and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+")  order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		if(paymentCounterList.isEmpty()){
			return null;
		}
		
		List<PaymentCounterReport> paymentCounterReportList=new ArrayList<>();
		for(PaymentCounter paymentCounter: paymentCounterList){
			paymentCounterReportList.add(new PaymentCounterReport(paymentCounter.getPaymentCounterId(),
																paymentCounter.getCurrentAmountPaid(), 
																paymentCounter.getCurrentAmountRefund(), 
																paymentCounter.getPaidDate(), 
																paymentCounter.getDueDate(), 
																paymentCounter.getLastDueDate(), 
																paymentCounter.getPayType(), 
																paymentCounter.getChequeNumber(), 
																paymentCounter.getBankName(), 
																paymentCounter.getChequeDate(), 
																paymentCounter.getCounterOrder(), 
																employeeDetailsDAO.getEmployeeDetailsByemployeeId(paymentCounter.getEmployee().getEmployeeId()).getName(), 
																paymentCounter.isStatus(), 
																paymentCounter.isChequeClearStatus()));
		}
		
		return paymentCounterReportList;
	}
	
	@Transactional
	public PaymentDoInfo fetchPaymentInfoByCounterOrderId(String counterOrderId){
		CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
		
		String id;
		String name;
		String inventoryId=counterOrder.getCounterOrderId();
		double amountPaid=0;
		double amountUnPaid;
		double totalAmount=counterOrder.getTotalAmountWithTax();
		String type="counter";
		String url="giveCounterOrderPayment";
		
		if(counterOrder.getBusinessName()==null){
			id=null;
			name=counterOrder.getCustomerName();
		}else{
			id=counterOrder.getBusinessName().getBusinessNameId();
			name=counterOrder.getBusinessName().getShopName();
		}
		
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrderId);
		if(paymentCounterList==null){
			amountPaid=0;
			amountUnPaid=counterOrder.getTotalAmountWithTax();
		}else{
			for(PaymentCounter paymentCounter: paymentCounterList){
				amountPaid+=paymentCounter.getCurrentAmountPaid()-paymentCounter.getCurrentAmountRefund();
			}
			amountPaid=((amountPaid>0)?amountPaid:0);
			//amountPaid=paymentCounterList.get(0).getTotalAmountPaid();
			amountUnPaid=counterOrder.getTotalAmountWithTax()-amountPaid;
		}
		
		return new PaymentDoInfo(0,id, name, inventoryId, amountPaid, amountUnPaid, totalAmount, type, url);
	}
	
	@Transactional
	public void deleteCounterOrder(String counterOrderId){
		CounterOrder counterOrder=fetchCounterOrder(counterOrderId);
		List<CounterOrderProductDetails> counterOrderProductDetailsList=fetchCounterOrderProductDetails(counterOrderId);
		//List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(counterOrderId);
		
		for(CounterOrderProductDetails counterOrderProductDetails: counterOrderProductDetailsList){
			
			Product product=counterOrderProductDetails.getProduct().getProduct();
			product.setCurrentQuantity(product.getCurrentQuantity()+counterOrderProductDetails.getPurchaseQuantity());
			productDAO.Update(product);
			
			productDAO.updateDailyStockExchange(product.getProductId(), product.getCurrentQuantity(), true);
			
			/*orderDetailsDAO.deleteOrderUsedProduct(counterOrderProductDetails.getProduct());
			
			sessionFactory.getCurrentSession().delete(counterOrderProductDetails);*/
		}
		
		/*if(paymentCounterList!=null){
			for(PaymentCounter paymentCounter: paymentCounterList){
				sessionFactory.getCurrentSession().delete(paymentCounter);
			}
		}*/
		counterOrder.setOrderStatus(orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_CANCELED));
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}
	
	@Transactional
	public List<PaymentCounter> fetchPaymentCounterList(String startDate,String endDate,String range){
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		String hql="";
		
		if (range.equals("today")) {
			cal.add(Calendar.MONTH, -6);
			hql="from PaymentCounter where date(paidDate)=date(CURRENT_DATE()) ";					
		}else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from PaymentCounter where date(paidDate)='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from PaymentCounter where date(paidDate)>='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("lastMonth")) {
			hql="from PaymentCounter where (date(paidDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLastMonthLastDate()+"') ";			
		}
		else if (range.equals("last3Months")) {
			hql="from PaymentCounter where (date(paidDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLast3MonthLastDate()+"') ";			
		}
		else if (range.equals("viewAll")) {
			hql="from PaymentCounter where 1=1 ";			
		}
		else if (range.equals("currentMonth")) {
			hql="from PaymentCounter where MONTH(paidDate)=MONTH(CURRENT_DATE) ";
		}
		else if (range.equals("range")) {
			hql="from PaymentCounter where (date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"') ";			
		}
		else if (range.equals("pickDate")) {
			hql="from PaymentCounter where date(paidDate)='"+startDate+"' ";
		}
		
		hql+=" and status=false and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+")  order by paidDate desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		if(paymentCounterList.isEmpty()){
			return null;
		}
		return paymentCounterList;
	}
	
	@Transactional
	public List<PaymentCounter> fetchPaymentCounterListForChequeReport(String startDate,String endDate,String range){
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		String hql="";
		
		if (range.equals("today")) {
			cal.add(Calendar.MONTH, -6);
			hql="from PaymentCounter where date(paidDate)=date(CURRENT_DATE()) ";					
		}else if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from PaymentCounter where date(paidDate)='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from PaymentCounter where date(paidDate)>='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("lastMonth")) {
			hql="from PaymentCounter where (date(paidDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLastMonthLastDate()+"') ";			
		}
		else if (range.equals("last3Months")) {
			hql="from PaymentCounter where (date(paidDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLast3MonthLastDate()+"') ";			
		}
		else if (range.equals("viewAll")) {
			hql="from PaymentCounter where 1=1 ";			
		}
		else if (range.equals("currentMonth")) {
			hql="from PaymentCounter where MONTH(paidDate)=MONTH(CURRENT_DATE) ";
		}
		else if (range.equals("range")) {
			hql="from PaymentCounter where (date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"') ";			
		}
		else if (range.equals("pickDate")) {
			hql="from PaymentCounter where date(paidDate)='"+startDate+"' ";
		}
		
		hql+=" and payType!='Cash' and counterOrder.employeeGk.company.companyId in ("+getSessionSelectedCompaniesIds()+")  order by paidDate desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		if(paymentCounterList.isEmpty()){
			return null;
		}
		return paymentCounterList;
	}
	
	@Transactional
	public PaymentCounter fetchPaymentCounterByPaymentCounterId(long paymentCounterId){
		String hql="from PaymentCounter where paymentCounterId="+paymentCounterId;
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentCounter> paymentCounterList=(List<PaymentCounter>)query.list();
		return paymentCounterList.get(0);
	}

	@Transactional
	public void updatePayment(PaymentCounter paymentCounter){
		updateCounterOrder(paymentCounter.getCounterOrder());
		paymentCounter=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter);
		sessionFactory.getCurrentSession().update(paymentCounter);
		
		//ledger update
		double balance=0,debit=0,creditOld=0,credit=0;		
		Ledger ledger=ledgerDAO.fetchLedger("counter", String.valueOf(paymentCounter.getPaymentCounterId()));
		List<Ledger> ledgerListBefore=ledgerDAO.fetchBeforeLedgerList(ledger.getLedgerId());
		if(ledger!=null){

			if(ledgerListBefore==null){
				balance=0;
			}else{
				balance=ledgerListBefore.get(ledgerListBefore.size()-1).getBalance();
			}
			
			creditOld=ledger.getCredit();
			credit=paymentCounter.getCurrentAmountPaid();	
			balance=balance+credit;
			
			ledger.setBalance(balance);
			ledger.setCredit(credit);
			ledgerDAO.updateLedger(ledger);
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//ledger update done
		
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(paymentCounter.getCounterOrder().getCounterOrderId());
		CounterOrder counterOrder=paymentCounter.getCounterOrder();
		if(paymentCounterList==null){
			counterOrder.setPayStatus(false);
		}else{
			double amountPaid=0;
			for(PaymentCounter paymentCounter2: paymentCounterList){
				amountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
			}
			/*Collections.reverse(paymentCounterList);
			double totalAmountWithTax=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			double remainingAmt=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			for(PaymentCounter paymentCounter2 : paymentCounterList){
				if(paymentCounter2.getCurrentAmountPaid()==0){
					remainingAmt=remainingAmt+paymentCounter2.getCurrentAmountRefund();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax+remainingAmt);
				}else{
					remainingAmt=remainingAmt-paymentCounter2.getCurrentAmountPaid();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax-remainingAmt);
				}
				paymentCounter2.setBalanceAmount(remainingAmt);
				
				paymentCounter2=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter2);
				sessionFactory.getCurrentSession().update(paymentCounter2);
			}*/
			double remainingAmt=counterOrder.getTotalAmountWithTax()-amountPaid;
			if(remainingAmt>0){
				counterOrder.setPayStatus(false);
			}
		}
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}
	@Transactional
	public void deletePayment(long paymentId){
		
		
		
		PaymentCounter paymentCounter=fetchPaymentCounterByPaymentCounterId(paymentId);
		paymentCounter.setStatus(true);
		paymentCounter=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter);
		sessionFactory.getCurrentSession().update(paymentCounter);
		
		//assign due date of payment after delete
		String hql="select max(paymentId) from PaymentCounter where counterOrder.counterOrderId='"+paymentCounter.getCounterOrder().getCounterOrderId()+"'";
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> lastPaymentCounterId=(List<Long>)query.list();
		if(lastPaymentCounterId.isEmpty()){
			counterOrder.setPaymentDueDate(paymentCounter.getLastDueDate());
		}else{	
			if(lastPaymentCounterId.get(0)==paymentId){
				counterOrder.setPaymentDueDate(paymentCounter.getLastDueDate());
			}
		}
		//end
		
		//delete ledger
		double balance=0,debit=0,creditOld=0,credit=0;
		Ledger ledger=ledgerDAO.fetchLedger("counter", String.valueOf(paymentCounter.getPaymentCounterId()));
		
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			creditOld=ledger.getCredit();
			balance=balance-creditOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//delete ledger end
		CounterOrder counterOrder=paymentCounter.getCounterOrder();
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(paymentCounter.getCounterOrder().getCounterOrderId());
		
		if(paymentCounterList==null){
			counterOrder.setPayStatus(false);
		}else{
			
			counterOrder.setPayStatus(false);
			
			/*Collections.reverse(paymentCounterList);
			double totalAmountWithTax=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			double remainingAmt=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			
			for(PaymentCounter paymentCounter2 : paymentCounterList){
				if(paymentCounter2.getCurrentAmountPaid()==0){
					remainingAmt=remainingAmt+paymentCounter2.getCurrentAmountRefund();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax+remainingAmt);
				}else{
					remainingAmt=remainingAmt-paymentCounter2.getCurrentAmountPaid();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax-remainingAmt);
				}
				paymentCounter2.setBalanceAmount(remainingAmt);
				
				paymentCounter2=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter2);
				sessionFactory.getCurrentSession().update(paymentCounter2);
			}*/
			
		}
		
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}
	
	@Transactional
	public void defineChequeBounced(long paymentId){
		PaymentCounter paymentCounter=fetchPaymentCounterByPaymentCounterId(paymentId);
		paymentCounter.setStatus(true);
		paymentCounter.setChequeClearStatus(false);
		paymentCounter=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter);
		sessionFactory.getCurrentSession().update(paymentCounter);
		
		//assign due date of payment after delete
		String hql="select max(paymentCounterId) from PaymentCounter where counterOrder.counterOrderId='"+paymentCounter.getCounterOrder().getCounterOrderId()+"'";
		hql+=" and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> lastPaymentCounterId=(List<Long>)query.list();
		if(lastPaymentCounterId.isEmpty()){
			counterOrder.setPaymentDueDate(paymentCounter.getLastDueDate());
		}else{	
			if(lastPaymentCounterId.get(0)==paymentId){
				counterOrder.setPaymentDueDate(paymentCounter.getLastDueDate());
			}
		}
		//end
		
		//delete ledger
		double balance=0,debit=0,creditOld=0,credit=0;
		Ledger ledger=ledgerDAO.fetchLedger("counter", String.valueOf(paymentCounter.getPaymentCounterId()));
		
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			creditOld=ledger.getCredit();
			balance=balance-creditOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//delete ledger end
		CounterOrder counterOrder=paymentCounter.getCounterOrder();
		List<PaymentCounter> paymentCounterList=fetchPaymentCounterListByCounterOrderId(paymentCounter.getCounterOrder().getCounterOrderId());
		if(paymentCounterList==null){
			counterOrder.setPayStatus(false);
		}else{
			
			counterOrder.setPayStatus(false);
			
			/*Collections.reverse(paymentCounterList);
			double totalAmountWithTax=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			double remainingAmt=paymentCounter.getCounterOrder().getTotalAmountWithTax();
			
			for(PaymentCounter paymentCounter2 : paymentCounterList){
				if(paymentCounter2.getCurrentAmountPaid()==0){
					remainingAmt=remainingAmt+paymentCounter2.getCurrentAmountRefund();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax+remainingAmt);
				}else{
					remainingAmt=remainingAmt-paymentCounter2.getCurrentAmountPaid();
					paymentCounter2.setTotalAmountPaid(totalAmountWithTax-remainingAmt);
				}
				paymentCounter2.setBalanceAmount(remainingAmt);
				
				paymentCounter2=(PaymentCounter)sessionFactory.getCurrentSession().merge(paymentCounter2);
				sessionFactory.getCurrentSession().update(paymentCounter2);
			}*/
			
		}
		
		counterOrder=(CounterOrder)sessionFactory.getCurrentSession().merge(counterOrder);
		sessionFactory.getCurrentSession().update(counterOrder);
	}
	
	@Transactional
	public double totalSaleAmountForProfitAndLoss(String startDate,String endDate){
		
		String hql="from CounterOrder where (date(dateOfOrderTaken)>= '"+startDate+"' and date(dateOfOrderTaken)<='"+endDate+"')";
		hql+=" and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"')";
		hql+=" and employeeGk.company.companyId="+getSessionSelectedCompaniesIds();
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
		
		double totalSale=0;
		for(CounterOrder counterOrder: counterOrderList){
			totalSale+=counterOrder.getTotalAmountWithTax();
		}
		
		return totalSale;
	}
}
