package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.entities.Address;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.SelectedAccess;

@Repository("areaDAO")

@Component
public class AreaDAOImpl extends TokenHandler implements AreaDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	HttpSession session;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	public AreaDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public AreaDAOImpl() {
	}

	

	@Transactional
	public void saveForWebApp(Area area) {
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		area.setCompany(company);
		sessionFactory.getCurrentSession().save(area);
		
	}

	@Transactional
	public void updateForWebApp(Area area) {
		area=(Area)sessionFactory.getCurrentSession().merge(area);
		sessionFactory.getCurrentSession().update(area);		
	}

	@Transactional
	public List<Area> fetchAllAreaForWebApp() {
		String hql = "from Area where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<Area> areaList = (List<Area>) query.list();
		if (areaList.isEmpty()) {
			return null;
		}
		return areaList;
	}

	@Transactional
	public List<Area> fetchAreaListByRegionIdForWebApp(long regionId) {
		String hql = "from Area where region=" + regionId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Area> areaList = (List<Area>) query.list();
		if (areaList.isEmpty()) {
			return null;
		}
		return areaList;
	}
	
	@Transactional
	public boolean checkAreaDuplication(long pincode,long areaId) {
		String hql = "from Area where pincode=" + pincode;
		
		if(areaId!=0){
			hql+=" and areaId<>"+areaId;
		}
		
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		List<Area> areaList = (List<Area>) query.list();
		if (areaList.isEmpty()) {
			return false;
		}
		return true;
	}
	
	@Transactional
	public Area fetchAreaForWebApp(long areaId) {
		String hql = "from Area where areaId=" + areaId;
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Area> areaList = (List<Area>) query.list();
		if (areaList.isEmpty()) {
			return null;
		}
		return areaList.get(0);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.dao.AreaDAO#fetchAreaListByEmployeeId(long)
	 * Oct 26, 20175:39:02 PM
	 */
	@Transactional
	public String fetchAreaListByEmployeeId(long employeeId) {
		String hql="";
		Query query;
		hql="from EmployeeAreaList where employeeDetails.employee.employeeId="+employeeId;		
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeAreaList> employeeAreaList=(List<EmployeeAreaList>)query.list();
		
		List<Area> areaList=new ArrayList<>();
		for(EmployeeAreaList employeeAreaList1:employeeAreaList)
		{
			areaList.add(employeeAreaList1.getArea());
		}
		String areaConcation="";
		for(Area areaList1:areaList)
		{
			areaConcation+=areaList1.getName()+",";
		}
		
		areaConcation=areaConcation.substring(0, areaConcation.length() - 1);
		return areaConcation;
	}

	/*@Override
	public String modifyQueryForAllowedAccess(String hql) {
		AllAccess allAccess=(AllAccess)session.getAttribute("allAccess");
		
		String ids="";
		for(Long areaId : allAccess.getAreaIdList())
		{
			ids+=areaId+",";
		}
		ids=ids.substring(0, ids.length()-1);
		
		hql+=" and areaId in ("+ids+")";
		
		return hql;
	}*/

	@Transactional
	public String modifyQueryForSelectedAccess(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			
			hql+="";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
				
				String ids="";
				for(Long areaId : areaIdList)
				{
					ids+=areaId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and areaId in ("+ids+")";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}

	@Transactional
	public String modifyQueryForSelectedAccessForOtherEntities(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			
			hql+="";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
				
				String ids="";
				for(Long areaId : areaIdList)
				{
					ids+=areaId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and area.areaId in ("+ids+") ";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}
	
	@Transactional
	public String getSessionAreaIdsForOtherEntities() {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			
			SelectedAccess selectedAccess=(SelectedAccess)session.getAttribute("selectedAccess");
			
			String ids="";
			for(Long areaId : selectedAccess.getAreaIdList()){
				ids+=areaId+",";
			}
			ids=ids.substring(0, ids.length()-1);
			
			return ids;
		}else{
			try {
					DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
					
					@SuppressWarnings("unchecked")
					Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
					Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
					
					String ids="";
					for(Long areaId : areaIdList){
						ids+=areaId+",";
					}
					ids=ids.substring(0, ids.length()-1);
					
					return ids;
				} catch (JWTDecodeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
			}
	}
}
