package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.CategoriesDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Company;

@Repository("categoriesDAO")

public class CategoriesDAOImpl extends TokenHandler implements CategoriesDAO {
	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	AreaDAO areaDAO;
	
	@Autowired
	Company company;
	
	public CategoriesDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public CategoriesDAOImpl() {
	}

	

	@Transactional
	public void saveCategoriesForWebApp(Categories categories) {
		// TODO Auto-generated method stub

		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		categories.setCompany(company);
		sessionFactory.getCurrentSession().save(categories);
	}

	@Transactional
	public void updateCategoriesForWebApp(Categories categories) {
		// TODO Auto-generated method stub
		categories=(Categories)sessionFactory.getCurrentSession().merge(categories);
		sessionFactory.getCurrentSession().update(categories);
	}

	@Transactional
	public Categories fetchCategoriesForWebApp(long categoriesId) {
		String hql = "from Categories where categoryId=" + categoriesId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> categoriesList = (List<Categories>) query.list();
		if (categoriesList.isEmpty()) {
			return null;
		}

		return categoriesList.get(0);
	}

	@Transactional
	public List<Categories> fetchCategoriesListForWebApp() {
		String hql = "from Categories where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		hql+=" order by categoryName";
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Categories> categoriesList = (List<Categories>) query.list();

		if (categoriesList.isEmpty()) {
			return null;
		}
		return categoriesList;
	}

}
