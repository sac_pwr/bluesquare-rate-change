package com.bluesquare.rc.dao.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.dao.CityDAO;
import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.utils.AllAccess;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.SelectedAccess;

@Repository("cityDAO")

@Component
public class CityDAOImpl extends TokenHandler implements CityDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	HttpSession session;

	@Autowired
	JsonWebToken jsonWebToken;
	
	

	@Transactional
	public List<City> fetchAllCityForWebApp() {
		String hql = "from City where 1=1 ";
		hql=modifyQueryForSelectedAccess(hql);
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<City> cities = (List<City>) query.list();
		if (cities.isEmpty()) {
			return null;
		} 
		return cities;
	}

	@Transactional
	public void saveForWebApp(City city) {
		sessionFactory.getCurrentSession().save(city);
		
	}

	@Transactional
	public void updateForWebApp(City city) {
		city=(City)sessionFactory.getCurrentSession().merge(city);
		sessionFactory.getCurrentSession().update(city);
		
	}

	@Transactional
	public City fetchCityForWebApp(long cityId) {
		String hql = "from City where cityId=" + cityId;
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<City> cityList = (List<City>) query.list();
		if (cityList.isEmpty()) {
			return null;
		}

		return cityList.get(0);
	}

	@Transactional
	public List<City> fetchCityByStateIdForWebApp(long stateId) {
		String hql = "from City where state=" + stateId;
		hql=modifyQueryForSelectedAccess(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("Query : " + query);
		@SuppressWarnings("unchecked")
		List<City> cities = (List<City>) query.list();
		if (cities.isEmpty()) {
			return null;
		} 
		return cities;
	}
	
	/*@Override
	public String modifyQueryForAllowedAccess(String hql) {
		
		AllAccess allAccess=(AllAccess)session.getAttribute("allAccess");
		
		String ids="";
		for(Long cityId : allAccess.getCityIdList())
		{
			ids+=cityId+",";
		}
		ids=ids.substring(0, ids.length()-1);
		
		hql+=" and cityId in ("+ids+")";
		
		return hql;
	}*/

	@Transactional
	public String modifyQueryForSelectedAccess(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			
			hql+="";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] cityIdList=decodedJWT.getClaim("cityIdList").asArray(longClass);
				
				String ids="";
				for(Long cityId : cityIdList)
				{
					ids+=cityId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and cityId in ("+ids+")";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}
	
	
	

}
