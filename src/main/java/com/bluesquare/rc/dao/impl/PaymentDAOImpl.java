package com.bluesquare.rc.dao.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CounterOrderDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.PaymentDAO;
import com.bluesquare.rc.dao.SupplierDAO;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.models.ChequePaymentReportModel;
import com.bluesquare.rc.models.CollectionReportMain;
import com.bluesquare.rc.models.CollectionReportPaymentDetails;
import com.bluesquare.rc.models.EmployeeSalaryStatus;
import com.bluesquare.rc.models.PaymentListReport;
import com.bluesquare.rc.models.PaymentPendingList;
import com.bluesquare.rc.models.PaymentReportModel;
import com.bluesquare.rc.rest.models.PaymentList;
import com.bluesquare.rc.rest.models.PaymentListModel;
import com.bluesquare.rc.rest.models.PaymentTakeRequest;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DatePicker;

@Repository("paymentDAO")

@Component

public class PaymentDAOImpl extends TokenHandler implements PaymentDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	Payment payment;

	@Autowired
	OrderDetails orderDetails;
	
	@Autowired
	SupplierDAO supplierDAO; 
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	CounterOrderDAO counterOrderDAO;
	
	@Autowired
	LedgerDAO ledgerDAO;
	
	public PaymentDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public OrderDetails savePaymentStatus(PaymentTakeRequest paymentTakeRequest,OrderDetails orderDetails){
		
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		
		Employee employee=new Employee(); 
		employee.setEmployeeId(getAppLoggedEmployeeId());
		
		payment=new Payment();
		payment.setEmployee(employee);
		payment.setOrderDetails(orderDetails);
		payment.setTotalAmount(orderDetails.getIssuedTotalAmountWithTax());
		
		List<Payment> paymentList=fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
		if(paymentList==null)
		{
			payment.setPaidAmount(paymentTakeRequest.getPaidAmount());
			payment.setDueAmount(orderDetails.getIssuedTotalAmountWithTax()-paymentTakeRequest.getPaidAmount());
		}
		else
		{
			payment.setPaidAmount(paymentTakeRequest.getPaidAmount());
			payment.setDueAmount(paymentList.get(0).getDueAmount()-paymentTakeRequest.getPaidAmount());
		}
		
		Calendar cal=Calendar.getInstance();
		
		
		if(paymentTakeRequest.getCashCheckStatus().equals(Constants.CASH_PAY_STATUS))
		{
			payment.setBankName(null);
			payment.setChequeDate(null);
			payment.setChequeNumber(null);	
			payment.setPayType(Constants.CASH_PAY_STATUS);
		}
		else
		{
			payment.setBankName(paymentTakeRequest.getBankName());
			cal.setTimeInMillis(paymentTakeRequest.getCheckDate());
			payment.setChequeDate(cal.getTime());
			payment.setChequeNumber(paymentTakeRequest.getCheckNo());
			payment.setPayType(Constants.CHEQUE_PAY_STATUS);
		}
		
		if(paymentTakeRequest.getFullPartialPayment().equals(Constants.FULL_PAY_STATUS)){
			payment.setDueDate(new Date());
			orderDetails.setPayStatus(true);
		}else{
			cal.setTimeInMillis(paymentTakeRequest.getDueDate());
			payment.setDueDate(cal.getTime());
			orderDetails.setPayStatus(false);
			orderDetails.setOrderDetailsPaymentTakeDatetime(cal.getTime());
		}
		payment.setLastDueDate(orderDetails.getOrderDetailsPaymentTakeDatetime());
		payment.setPaidDate(new Date());
		
		payment.setChequeClearStatus(true);
		sessionFactory.getCurrentSession().save(payment);
		//orderDetailsDAO.updateOrderDetailsPaymentDays(orderDetails);
		
		//ledger entry create
		double credit=payment.getPaidAmount();
		Ledger ledger=new Ledger(
					(payment.getPayType().equals("Cash"))?payment.getPayType():payment.getBankName()+"-"+payment.getChequeNumber(), 
					payment.getPaidDate(), 
					null, 
					payment.getOrderDetails().getOrderId()+" Payment", 
					payment.getOrderDetails().getBusinessName().getShopName(), 
					0, 
					credit, 
					0, 
					payment
				);
		ledgerDAO.createLedgerEntry(ledger);
		//ledger entry created
				
		return orderDetails;
		
	}
	
	
	
	@Transactional
	public List<Payment> fetchPaymentListByOrderDetailsId(String orderDetailsId)
	{
		String hql="from Payment where status=false and orderDetails.orderId='"+orderDetailsId+"' and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds()+" order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Payment> paymentList=(List<Payment>)query.list();
		if(paymentList.isEmpty())
		{
			return null;
		}
		
		return paymentList;
	}
	
	@Transactional
	public List<PaymentListReport> fetchPaymentListByOrderDetailsIdForCollectionReport(String orderDetailsId){
		
		String hql="from Payment where status=false and orderDetails.orderId='"+orderDetailsId+"' and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds()+" order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Payment> paymentList=(List<Payment>)query.list();
		if(paymentList.isEmpty())
		{
			return null;
		}
		
		List<PaymentListReport> paymentListReportList=new ArrayList<>();
		for(Payment payment: paymentList){
			paymentListReportList.add(new PaymentListReport(payment.getPaymentId(),
					payment.getOrderDetails(), 
					employeeDetailsDAO.getEmployeeDetailsByemployeeId(payment.getEmployee().getEmployeeId()).getName(), 
					payment.getTotalAmount(), 
					payment.getDueAmount(), 
					payment.getDueDate(), 
					payment.getLastDueDate(), 
					payment.getPaidAmount(), 
					payment.getPaidDate(), 
					payment.getPayType(), 
					payment.getChequeNumber(), 
					payment.getBankName(), 
					payment.getChequeDate(), 
					payment.isStatus(), 
					payment.isChequeClearStatus()));
		}
		
		return paymentListReportList;
	}
	
	
	/**
	 * fetch pending payment list of supplier and employees  
	 * Supplier -> Inventory -> PaymentPaySupplier 
	 * @return
	 */
	@Transactional
	public List<PaymentPendingList> fetchPaymentPendingList()
	{
		String hql;
		Query query;		
		List<PaymentPendingList> paymentPendingLists=new ArrayList<>();
		List<PaymentPendingList> paymentPendingLists2=new ArrayList<>();
		long srno=1;
		
		List<Supplier> supplierList=supplierDAO.fetchSupplierForWebAppList();
				
		if(supplierList!=null)
		{
			for(Supplier supplier : supplierList)
			{
				double totalAmount=0;
				double totalAmountPaid=0;
				double totalAmountUnPaid=0;
				
				hql="from Inventory where supplier.supplierId='"+supplier.getSupplierId()+"' and date(inventoryPaymentDatetime) <= date(CURRENT_DATE()) and payStatus=false"+
					" and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+")  order by inventoryPaymentDatetime desc";
				
				query=sessionFactory.getCurrentSession().createQuery(hql);
				List<Inventory> inventoryList=(List<Inventory>)query.list();
				if(inventoryList.isEmpty())
				{
					break;
				}
				
				for(Inventory inventory : inventoryList)
				{
					totalAmount+=inventory.getTotalAmountTax();
					
					double duePay=0;
					hql="from PaymentPaySupplier where inventory.inventoryTransactionId='"+inventory.getInventoryTransactionId()+"'"
							+ " and inventory.supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by paidDate desc";
					query=sessionFactory.getCurrentSession().createQuery(hql);
					List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
					
					if(paymentPaySupplierList.isEmpty())
					{
						duePay=inventory.getTotalAmountTax();
					}
					else
					{
						duePay=paymentPaySupplierList.get(0).getDueAmount();
					}
					
					totalAmountUnPaid+=duePay;
				}
				totalAmountPaid=totalAmount-totalAmountUnPaid;
				if(totalAmountUnPaid!=0)
				{
					paymentPendingLists.add(new PaymentPendingList(srno, 0,supplier.getSupplierId(), supplier.getName(), totalAmount, totalAmountUnPaid, totalAmountPaid, "Supplier"));
				}
				srno++;
			}
		}
		List<EmployeeDetails> employeeDetailsList=employeeDetailsDAO.fetchEmployeeDetailsList();
		if(employeeDetailsList!=null){
			for(EmployeeDetails employeeDetails:employeeDetailsList)
			{
				double totalAmount=0;
				double totalAmountPaid=0;
				double totalAmountUnPaid=0;
				
				List<EmployeeSalaryStatus> employeeSalaryStatusList=employeeDetailsDAO.tofilterRangeEmployeeSalaryStatusForWebApp("", "", "ViewAll", employeeDetails.getEmployeeDetailsId());
					
				totalAmount=employeeSalaryStatusList.get(0).getTotalAmount();
				totalAmountPaid=employeeSalaryStatusList.get(0).getAmountPaidCurrentMonth();
				totalAmountUnPaid=employeeSalaryStatusList.get(0).getAmountPendingCurrentMonth();
				
				if(totalAmountUnPaid!=0)
				{				
					paymentPendingLists.add(new PaymentPendingList(srno, employeeDetails.getEmployeeDetailsId(), employeeDetails.getEmployeeDetailsGenId(), employeeDetails.getName(), totalAmount, totalAmountUnPaid, totalAmountPaid, employeeDetails.getEmployee().getDepartment().getName()));
				}
				srno++;
			}
		}
		Collections.sort(paymentPendingLists,new NameComparatorPaymentPendingList()); 
		srno=1;
		for(PaymentPendingList paymentPendingList : paymentPendingLists)
		{
			paymentPendingList.setSrno(srno);
			paymentPendingLists2.add(paymentPendingList);
			srno++;
		}
		
		return paymentPendingLists2;
	}
	
	class NameComparatorPaymentPendingList implements Comparator<PaymentPendingList>{  
		public int compare(PaymentPendingList s1,PaymentPendingList s2){  
		return s1.getName().compareTo(s2.getName());  
		}  
	}
	
	/*@Transactional
	public List<LedgerPaymentView> fetchLedgerPaymentView(String startDate, String endDate,
			String range)
	{
		String supplierHql = "",employeeHql="";
		Query supplierQuery,employeeQuery;
		List<LedgerPaymentView> ledgerPaymentViewLists=new ArrayList<>();
		List<LedgerPaymentView> sortedLedgerPaymentViewLists=new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
		if (range.equals("lastMonth")) {			
			supplierHql="from PaymentPaySupplier where date(paidDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLastMonthLastDate()+"'";			
			employeeHql="from EmployeeSalary where date(payingDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(payingDate)<='"+DatePicker.getLastMonthLastDate()+"'";			
		}
		else if (range.equals("last3Months")) {
			cal.add(Calendar.MONTH, -12);
			supplierHql="from PaymentPaySupplier where date(paidDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLast3MonthLastDate()+"'";			
			employeeHql="from EmployeeSalary where date(payingDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(payingDate)<='"+DatePicker.getLast3MonthLastDate()+"'";
		}
		else if (range.equals("viewAll")) {
			supplierHql="from PaymentPaySupplier where 1=1 ";			
			employeeHql="from EmployeeSalary where 1=1 ";
		}
		else if (range.equals("currentMonth")) {
			supplierHql="from PaymentPaySupplier where date(paidDate)>='"+DatePicker.getCurrentMonthStartDate()+"' and date(paidDate)<='"+DatePicker.getCurrentMonthLastDate()+"'";			
			employeeHql="from EmployeeSalary where date(payingDate)>='"+DatePicker.getCurrentMonthStartDate()+"' and date(payingDate)<='"+DatePicker.getCurrentMonthLastDate()+"'";
		}
		else if (range.equals("range")) {
			supplierHql="from PaymentPaySupplier where date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"'";			
			employeeHql="from EmployeeSalary where date(payingDate)>='"+startDate+"' and date(payingDate)<='"+endDate+"'";
		}
		else if (range.equals("pickDate")) {
			supplierHql="from PaymentPaySupplier where date(paidDate)='"+startDate+"'";			
			employeeHql="from EmployeeSalary where date(payingDate)='"+startDate+"'";
		}
		else if (range.equals("today")) {			
			supplierHql="from PaymentPaySupplier where date(paidDate)=date(CURRENT_DATE())";			
			employeeHql="from EmployeeSalary where date(payingDate)=date(CURRENT_DATE())";			
		}
		else if (range.equals("yesterday")) {		
			cal.add(Calendar.DAY_OF_MONTH, -1);
			supplierHql="from PaymentPaySupplier where date(paidDate)='"+dateFormat.format(cal.getTime())+"'";			
			employeeHql="from EmployeeSalary where date(payingDate)='"+dateFormat.format(cal.getTime())+"'";			
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			supplierHql="from PaymentPaySupplier where date(paidDate)>='"+dateFormat.format(cal.getTime())+"'";			
			employeeHql="from EmployeeSalary where date(payingDate)>='"+dateFormat.format(cal.getTime())+"'";					
		}
		
		supplierHql+=" and inventory.supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+" order by paidDate desc";
		supplierHql+=" order by paidDate desc";
		employeeHql+=" order by payingDate desc";
		employeeHql+=" and employeeDetails.employeeDetailsId in "+
					"(select emparea.employeeDetails.employeeDetailsId from EmployeeAreaList as emparea "+
					"where emparea.employeeDetails.employee.company.companyId in ("+getSessionSelectedCompaniesIds()+")) order by payingDate desc";
					
		supplierQuery=sessionFactory.getCurrentSession().createQuery(supplierHql);
		List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)supplierQuery.list();
		if(!paymentPaySupplierList.isEmpty())
		{
			for(PaymentPaySupplier paymentPaySupplier:paymentPaySupplierList)
			{
				String payMode;
				if(paymentPaySupplier.getChequeDate()==null)
				{
					payMode="Cash";
				}
				else
				{
					payMode=paymentPaySupplier.getBankName()+"-"+paymentPaySupplier.getChequeNumber();
				}
				
				ledgerPaymentViewLists.add(new LedgerPaymentView(	
											0, 
											0,
											paymentPaySupplier.getInventory().getSupplier().getSupplierId(), 
											paymentPaySupplier.getInventory().getSupplier().getName(), 
											paymentPaySupplier.getPaidAmount(), 
											payMode, 
											Constants.SUPPLIER_TYPE,  
											paymentPaySupplier.getPaidDate()
											));
			}
		}
		
		employeeQuery=sessionFactory.getCurrentSession().createQuery(employeeHql);
		List<EmployeeSalary> employeeSalaryList=(List<EmployeeSalary>)employeeQuery.list();
		if(!employeeSalaryList.isEmpty())
		{
			for(EmployeeSalary employeeSalary:employeeSalaryList)
			{
				String payMode;
				if(employeeSalary.getChequeDate()==null)
				{
					payMode="Cash";
				}
				else
				{
					payMode=employeeSalary.getBankName()+"-"+employeeSalary.getChequeNumber();
				}
				
				ledgerPaymentViewLists.add(new LedgerPaymentView(	
											0, 
											employeeSalary.getEmployeeDetails().getEmployeeDetailsId(),
											employeeSalary.getEmployeeDetails().getEmployeeDetailsGenId(), 
											employeeSalary.getEmployeeDetails().getName(), 
											employeeSalary.getPayingAmount(), 
											payMode, 
											employeeSalary.getEmployeeDetails().getEmployee().getDepartment().getName(),  
											employeeSalary.getPayingDate()
											));
			}
		}
		
		
		Collections.sort(ledgerPaymentViewLists,new DateComparatorLedgerPaymentView()); 
		
		long srno=1;
		for(LedgerPaymentView ledgerPaymentView : ledgerPaymentViewLists)
		{
			ledgerPaymentView.setSrno(srno);
			sortedLedgerPaymentViewLists.add(ledgerPaymentView);
			srno++;
		}
		
		return sortedLedgerPaymentViewLists;
	}*/
	
	/*class DateComparatorLedgerPaymentView implements Comparator<LedgerPaymentView>{  
		public int compare(LedgerPaymentView s1,LedgerPaymentView s2){  
		return s1.getPaymentPaidDate().compareTo(s2.getPaymentPaidDate())*-1;  
		}  
	}*/
	
	@Transactional
	public CollectionReportMain getCollectionReportDetails(String startDate, String endDate,String range)
	{
		String hql = "";
		Query query;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		List<CollectionReportPaymentDetails> collectionReportPaymentDetailList=new ArrayList<>();
		double amountToBePaid=0;
		double pendingAmount=0;
		double fullAmountCollected=0;
		double partialAmountCollected=0;
		
		if (range.equals("today")) {
			cal.add(Calendar.MONTH, -6);
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)=date(CURRENT_DATE()) and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";					
		}
		if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)='"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";					
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)>='"+dateFormat.format(cal.getTime())+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";					
		}
		else if (range.equals("lastMonth")) {
			hql="from OrderDetails where (date(orderDetailsPaymentTakeDatetime)>='"+DatePicker.getLastMonthFirstDate()+"' and date(orderDetailsPaymentTakeDatetime)<='"+DatePicker.getLastMonthLastDate()+"') and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";			
		}
		else if (range.equals("last3Months")) {
			hql="from OrderDetails where (date(orderDetailsPaymentTakeDatetime)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(orderDetailsPaymentTakeDatetime)>='"+DatePicker.getLast3MonthLastDate()+"') and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";			
		}
		else if (range.equals("viewAll")) {
			hql="from OrderDetails where orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";			
		}
		else if (range.equals("currentMonth")) {
			hql="from OrderDetails where (date(orderDetailsPaymentTakeDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(orderDetailsPaymentTakeDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"')) and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		else if (range.equals("range")) {
			hql="from OrderDetails where (date(orderDetailsPaymentTakeDatetime)>='"+startDate+"' and date(orderDetailsPaymentTakeDatetime)<='"+endDate+"') and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";			
		}
		else if (range.equals("pickDate")) {
			hql="from OrderDetails where date(orderDetailsPaymentTakeDatetime)='"+startDate+"' and orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')";
		}
		
		hql+=" and businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		long srnoUnPaid=1,srnoFull=1,srnoPartial=1;
		if(!orderDetailsList.isEmpty())
		{
			for(OrderDetails orderDetails:orderDetailsList)
			{
				/*if(orderDetails.getOrderId().equals("ORD1000000003"))
				{
					System.out.println("stop here ");
				}*/
				
				List<Payment> paymentList=fetchPaymentListByOrderDetailsId(orderDetails.getOrderId());
					
					if(paymentList==null)
					{
						collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoUnPaid, 
																								orderDetails.getBusinessName().getBusinessNameId(), 
																								orderDetails.getBusinessName().getShopName(), 
																								orderDetails.getOrderId(), 
																								orderDetails.getBusinessName().getContact().getMobileNumber(), 
																								orderDetails.getBusinessName().getArea().getName(), 
																								orderDetails.getBusinessName().getArea().getRegion().getName(), 
																								orderDetails.getBusinessName().getArea().getRegion().getCity().getName(), 
																								orderDetails.getIssuedTotalAmountWithTax(),
																								0, 
																								orderDetails.getIssuedTotalAmountWithTax(), 
																								orderDetails.getOrderDetailsPaymentTakeDatetime(), 
																								null, 
																								"--",
																								"UnPaid"));
						pendingAmount+=orderDetails.getIssuedTotalAmountWithTax();
						srnoUnPaid++;
					}
					else
					{
					    double amountDue=paymentList.get(0).getDueAmount();
						double amountPaid=orderDetails.getIssuedTotalAmountWithTax()-amountDue;
						String payMode="";
						
						if(paymentList.get(0).getChequeDate()==null)
						{
							payMode="Cash";
						}
						else
						{
							payMode=paymentList.get(0).getChequeNumber();
						}
						
						if(amountDue==0)
						{
							collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoFull, 
																								orderDetails.getBusinessName().getBusinessNameId(), 
																								orderDetails.getBusinessName().getShopName(), 
																								orderDetails.getOrderId(), 
																								orderDetails.getBusinessName().getContact().getMobileNumber(), 
																								orderDetails.getBusinessName().getArea().getName(), 
																								orderDetails.getBusinessName().getArea().getRegion().getName(), 
																								orderDetails.getBusinessName().getArea().getRegion().getCity().getName(), 
																								orderDetails.getIssuedTotalAmountWithTax(), 
																								amountPaid,
																								amountDue,
																								null,
																								paymentList.get(0).getPaidDate(),
																								payMode,
																								"Full")); 
							fullAmountCollected+=amountPaid;
							srnoFull++;
						}
						else
						{
							collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoPartial, 
																								orderDetails.getBusinessName().getBusinessNameId(), 
																								orderDetails.getBusinessName().getShopName(), 
																								orderDetails.getOrderId(), 
																								orderDetails.getBusinessName().getContact().getMobileNumber(), 
																								orderDetails.getBusinessName().getArea().getName(), 
																								orderDetails.getBusinessName().getArea().getRegion().getName(), 
																								orderDetails.getBusinessName().getArea().getRegion().getCity().getName(), 
																								orderDetails.getIssuedTotalAmountWithTax(), 
																								amountPaid,
																								amountDue, 
																								paymentList.get(0).getDueDate(),
																								paymentList.get(0).getPaidDate(),
																								payMode,
																								"Partial"));
							partialAmountCollected=+amountPaid;
							pendingAmount+=amountDue;
							srnoPartial++;							
						}
				  }
					amountToBePaid+=orderDetails.getIssuedTotalAmountWithTax();
				}
		}
		
		//counter order
		List<CounterOrder> counterOrderList=counterOrderDAO.fetchCounterOrderByRange(null, range, startDate, endDate);
		if(counterOrderList!=null){

			for(CounterOrder counterOrder:counterOrderList)
			{				
				List<PaymentCounter> paymentCounterList=counterOrderDAO.fetchPaymentCounterListByCounterOrderId(counterOrder.getCounterOrderId());
					
					if(paymentCounterList==null)
					{
						
						if(counterOrder.getBusinessName()!=null){
							collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoUnPaid, 
																									counterOrder.getBusinessName().getBusinessNameId(), 
																									counterOrder.getBusinessName().getShopName(), 
																									counterOrder.getCounterOrderId(), 
																									counterOrder.getBusinessName().getContact().getMobileNumber(), 
																									counterOrder.getBusinessName().getArea().getName(), 
																									counterOrder.getBusinessName().getArea().getRegion().getName(), 
																									counterOrder.getBusinessName().getArea().getRegion().getCity().getName(), 
																									counterOrder.getTotalAmountWithTax(),
																									0, 
																									counterOrder.getTotalAmountWithTax(), 
																									counterOrder.getPaymentDueDate(), 
																									null, 
																									"--",
																									"UnPaid"));
						}else{
							collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoUnPaid, 
																									"--", 
																									counterOrder.getCustomerName(), 
																									counterOrder.getCounterOrderId(), 
																									counterOrder.getCustomerMobileNumber(), 
																									"--", 
																									"--", 
																									"--", 
																									counterOrder.getTotalAmountWithTax(),
																									0, 
																									counterOrder.getTotalAmountWithTax(), 
																									counterOrder.getPaymentDueDate(), 
																									null, 
																									"--",
																									"UnPaid"));
						}
						pendingAmount+=counterOrder.getTotalAmountWithTax();
						srnoUnPaid++;
					}
					else
					{
						double amountPaid=0;
						for(PaymentCounter paymentCounter2: paymentCounterList){
							amountPaid+=paymentCounter2.getCurrentAmountPaid()-paymentCounter2.getCurrentAmountRefund();
						}
					    double amountDue=counterOrder.getTotalAmountWithTax()-amountPaid;//paymentCounterList.get(0).getBalanceAmount();
						//double amountPaid=counterOrder.getTotalAmountWithTax()-amountDue;
						String payMode="";
						
						if(paymentCounterList.get(0).getChequeDate()==null)
						{
							payMode="Cash";
						}
						else
						{
							payMode=paymentCounterList.get(0).getBankName()+"-"+paymentCounterList.get(0).getChequeNumber();
						}
						
						if(amountDue==0)
						{
							if(counterOrder.getBusinessName()!=null){
								collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoFull, 
																									counterOrder.getBusinessName().getBusinessNameId(), 
																									counterOrder.getBusinessName().getShopName(), 
																									counterOrder.getCounterOrderId(), 
																									counterOrder.getBusinessName().getContact().getMobileNumber(), 
																									counterOrder.getBusinessName().getArea().getName(), 
																									counterOrder.getBusinessName().getArea().getRegion().getName(), 
																									counterOrder.getBusinessName().getArea().getRegion().getCity().getName(), 
																									counterOrder.getTotalAmountWithTax(), 
																									amountPaid,
																									amountDue,
																									null,
																									paymentCounterList.get(0).getPaidDate(),
																									payMode,
																									"Full"));
							}else{
								collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoFull, 
																									"--", 
																									counterOrder.getCustomerName(), 
																									counterOrder.getCounterOrderId(), 
																									counterOrder.getCustomerMobileNumber(), 
																									"--",
																									"--",
																									"--",
																									counterOrder.getTotalAmountWithTax(), 
																									amountPaid,
																									amountDue,
																									null,
																									paymentCounterList.get(0).getPaidDate(),
																									payMode,
																									"Full"));
							}
							fullAmountCollected+=amountPaid;
							srnoFull++;
						}
						else
						{
							if(counterOrder.getBusinessName()!=null){
								collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoPartial, 
																									counterOrder.getBusinessName().getBusinessNameId(), 
																									counterOrder.getBusinessName().getShopName(), 
																									counterOrder.getCounterOrderId(), 
																									counterOrder.getBusinessName().getContact().getMobileNumber(), 
																									counterOrder.getBusinessName().getArea().getName(), 
																									counterOrder.getBusinessName().getArea().getRegion().getName(), 
																									counterOrder.getBusinessName().getArea().getRegion().getCity().getName(), 
																									counterOrder.getTotalAmountWithTax(), 
																									amountPaid,
																									amountDue, 
																									paymentCounterList.get(0).getDueDate(),
																									paymentCounterList.get(0).getPaidDate(),
																									payMode,
																									"Partial"));
							}else{
								collectionReportPaymentDetailList.add(new CollectionReportPaymentDetails(srnoPartial, 
																									"--", 
																									counterOrder.getCustomerName(), 
																									counterOrder.getCounterOrderId(), 
																									counterOrder.getCustomerMobileNumber(), 
																									"--",
																									"--",
																									"--",
																									counterOrder.getTotalAmountWithTax(), 
																									amountPaid,
																									amountDue, 
																									paymentCounterList.get(0).getDueDate(),
																									paymentCounterList.get(0).getPaidDate(),
																									payMode,
																									"Partial"));
							}
							partialAmountCollected=+amountPaid;
							pendingAmount+=amountDue;
							srnoPartial++;							
						}
				  }
					amountToBePaid+=counterOrder.getTotalAmountWithTax();
				}
		
		}
		
		return new CollectionReportMain(amountToBePaid, pendingAmount, fullAmountCollected, partialAmountCollected, collectionReportPaymentDetailList);
	}
	
	/*@Transactional
	public CollectionReportResponse fetchCollectionDetailsByOrderId(String orderId) {
		// TODO Auto-generated method stub
		
		List<Payment> paymentList=fetchPaymentListByOrderDetailsId(orderId);
		
		CollectionReportResponse collectionReportResponse;
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		orderDetails=orderDetailsDAO.fetchOrderDetailsByOrderId(orderId);
		if(paymentList.isEmpty()){
			
			collectionReportResponse=new CollectionReportResponse(orderDetails.getBusinessName().getShopName(),
																	orderDetails.getBusinessName().getContact().getMobileNumber(), 
																	orderDetails.getOrderId(), 
																	null,
																	orderDetails.getIssuedTotalAmountWithTax(),
																	0.0d, 
																	orderDetails.getIssuedTotalAmountWithTax(),
																	orderDetails.getOrderDetailsPaymentTakeDatetime(),  
																	null, 
																	null, 
																	null,
																	null);
		}else if(paymentList.get(0).getChequeNumber()==null){
			
				collectionReportResponse=new CollectionReportResponse(orderDetails.getBusinessName().getShopName(),
																	  orderDetails.getBusinessName().getContact().getMobileNumber(), 
																	  orderDetails.getOrderId(), 
																	  paymentList.get(0).getPaidDate(),
																	paymentList.get(0).getDueAmount(),
																	paymentList.get(0).getPaidAmount(),
																	orderDetails.getIssuedTotalAmountWithTax(),
																	orderDetails.getOrderDetailsPaymentTakeDatetime(), 
																	"Cash",
																	null,
																	null, 
																	null);
															}
		else{
			collectionReportResponse=new CollectionReportResponse(  orderDetails.getBusinessName().getShopName(),
																	orderDetails.getBusinessName().getContact().getMobileNumber(), 
																	orderDetails.getOrderId(), 
																	paymentList.get(0).getPaidDate(), 
																	paymentList.get(0).getDueAmount(), 
																	paymentList.get(0).getPaidAmount(),
																	orderDetails.getIssuedTotalAmountWithTax(),
																	orderDetails.getOrderDetailsPaymentTakeDatetime(), 
																	"Cheque",
																	paymentList.get(0).getBankName(),
																	paymentList.get(0).getChequeNumber(), 
																	paymentList.get(0).getChequeDate());
		}
		return collectionReportResponse;
	}*/
	
	@Transactional
	public List<Payment> fetchPaymentListbyRange(String orderId,String fromDate,String toDate,String range){
		String hql="";
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		if(range.equals("range"))
		{
			hql="from Payment where orderDetails.orderId='"+orderId+"' And (date(paidDate)>='"+fromDate+"' And date(paidDate)<='"+toDate+"')";
		}
		else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from Payment where orderDetails.orderId='"+orderId+"' And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'";
		}else if(range.equals("last1month")){
			cal.add(Calendar.MONTH, -1);
			hql="from Payment where orderDetails.orderId='"+orderId+"' And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'";					
		}else if(range.equals("last3months")){
			cal.add(Calendar.MONTH, -3);
			hql="from Payment where orderDetails.orderId='"+orderId+"' And date(paidDate)>='"+dateFormat.format(cal.getTime())+"'";
		}else if(range.equals("pickDate")){
			hql="from Payment where orderDetails.orderId='"+orderId+"' And date(paidDate)='"+fromDate+"'";
		}
		else if(range.equals("viewAll")){
			hql="from Payment where orderDetails.orderId='"+orderId+"' order by paidDate desc";
		}
		else if(range.equals("currentDate"))
		{
			hql="from Payment where orderDetails.orderId='"+orderId+"' And date(paidDate)=date(CURRENT_DATE())";
		}
		hql+=" and employee.employeeId="+getAppLoggedEmployeeId()+" and status=false and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds()+"  order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Payment> paymentList=(List<Payment>)query.list();
		if(paymentList.isEmpty()){
			return null;
		}
		return paymentList;		
	}
	
	@Transactional
	public List<OrderDetails> fetchOrderDetailsListPaymentDateByRange(String fromDate,String toDate,String range,String businessNameId,long employeeId){
		String hql="";
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		
		if(range.equals("range"))
		{
			hql="from Payment p where (date(p.paidDate)>='"+fromDate+"' And date(p.paidDate)<='"+toDate+"')";
		}
		else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from Payment p where date(p.paidDate)>='"+dateFormat.format(cal.getTime())+"'";
		}else if(range.equals("last1month")){
			cal.add(Calendar.MONTH, -1);
			hql="from Payment p where date(p.paidDate)>='"+dateFormat.format(cal.getTime())+"'";					
		}else if(range.equals("last3months")){
			cal.add(Calendar.MONTH, -3);
			hql="from Payment p where  date(p.paidDate)>='"+dateFormat.format(cal.getTime())+"'";
		}else if(range.equals("pickDate")){
			hql="from Payment p where  date(p.paidDate)='"+fromDate+"'";
		}
		else if(range.equals("viewAll")){
			hql="from Payment p where 1=1 ";
		}
		else if(range.equals("currentDate"))
		{
			hql="from Payment p where  date(p.paidDate)=date(CURRENT_DATE())";
		}
		hql=" select distinct p.orderDetails "+hql+" and p.status=false ";
		//hql+=" and p.orderDetails.employeeSM.employeeId=" +employeeId +
		//hql+=" and p.employee.employeeId=" +employeeId +
		hql+=" and p.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
			" AND p.orderDetails.businessName.businessNameId='"+businessNameId+"'"+
			" and p.orderDetails.orderStatus.status in ('"+Constants.ORDER_STATUS_DELIVERED+"','"+Constants.ORDER_STATUS_DELIVERED_PENDING+"')  order by p.paidDate desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();
		if(orderDetailsList.isEmpty()){
			return null;
		}
		
		return orderDetailsList;		
	}
	
	@Transactional
	public PaymentListModel fetchPaymentListByOrderIdForApp(String orderId){
		List<PaymentList> paymentLists=new ArrayList<>();
		OrderDetails orderDetails=orderDetailsDAO.fetchOrderDetailsByOrderId(orderId);
		List<Payment> paymentList=fetchPaymentListByOrderDetailsId(orderId);
		double paidAmt=0;
		for(Payment payment: paymentList){
			paymentLists.add(new PaymentList(payment.getPaidAmount(), 
											payment.getPaidDate(), 
											payment.getPayType(), 
											payment.getBankName(), 
											payment.getChequeNumber(), 
											payment.getChequeDate(),
											employeeDetailsDAO.getEmployeeDetailsByemployeeId(payment.getEmployee().getEmployeeId()).getName()));
			paidAmt+=payment.getPaidAmount();
		}		
		return new PaymentListModel(orderDetails, 
									paymentList.get(0).getPaidDate(), 
									paidAmt, 
									(orderDetails.getIssuedTotalAmountWithTax()-paidAmt), 
									paymentLists);
	}
	
	@Transactional
	public List<Payment> fetchPaymentList(String startDate,String endDate,String range){
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		String hql="";
		
		if (range.equals("today")) {
			cal.add(Calendar.MONTH, -6);
			hql="from Payment where date(paidDate)=date(CURRENT_DATE()) ";					
		}
		if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from Payment where date(paidDate)='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from Payment where date(paidDate)>='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("lastMonth")) {
			hql="from Payment where (date(paidDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLastMonthLastDate()+"') ";			
		}
		else if (range.equals("last3Months")) {
			hql="from Payment where (date(paidDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLast3MonthLastDate()+"') ";			
		}
		else if (range.equals("viewAll")) {
			hql="from Payment where 1=1 ";			
		}
		else if (range.equals("currentMonth")) {
			hql="from Payment where (date(paidDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(paidDate) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
		else if (range.equals("range")) {
			hql="from Payment where (date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"') ";			
		}
		else if (range.equals("pickDate")) {
			hql="from Payment where date(paidDate)='"+startDate+"' ";
		}
		
		hql+=" and status=false and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")  order by paidDate desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Payment> paymentList=(List<Payment>)query.list();
		if(paymentList.isEmpty()){
			return null;
		}		
		return paymentList;
	}
	
	@Transactional
	public List<Payment> fetchPaymentListForChequeReport(String startDate,String endDate,String range){
		
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal=Calendar.getInstance();
		String hql="";
		
		if (range.equals("today")) {
			cal.add(Calendar.MONTH, -6);
			hql="from Payment where date(paidDate)=date(CURRENT_DATE()) ";					
		}
		if (range.equals("yesterday")) {
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from Payment where date(paidDate)='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("last7days")) {
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from Payment where date(paidDate)>='"+dateFormat.format(cal.getTime())+"' ";					
		}
		else if (range.equals("lastMonth")) {
			hql="from Payment where (date(paidDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLastMonthLastDate()+"') ";			
		}
		else if (range.equals("last3Months")) {
			hql="from Payment where (date(paidDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(paidDate)<='"+DatePicker.getLast3MonthLastDate()+"') ";			
		}
		else if (range.equals("viewAll")) {
			hql="from Payment where 1=1 ";			
		}
		else if (range.equals("currentMonth")) {
			hql="from Payment where (date(paidDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(paidDate) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
		else if (range.equals("range")) {
			hql="from Payment where (date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"') ";			
		}
		else if (range.equals("pickDate")) {
			hql="from Payment where date(paidDate)='"+startDate+"' ";
		}
		
		hql+=" and payType!='Cash' and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")  order by paidDate desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Payment> paymentList=(List<Payment>)query.list();
		if(paymentList.isEmpty()){
			return null;
		}		
		return paymentList;
	}
	
	@Transactional
	public Payment fetchPaymentBYPaymentId(long paymentId){
		String hql="from Payment where paymentId="+paymentId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Payment> paymentList=(List<Payment>)query.list();
		if(paymentList.isEmpty()){
			return null;
		}		
		return paymentList.get(0);
	}
	
	@Transactional
	public List<Long> findLastPaymentIdsFromPaymentCounter(){
		String hql="select paymentCounterId from PaymentCounter a where status=false and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds()+" and paymentCounterId in "
				+ "(SELECT max(paymentCounterId) FROM PaymentCounter b where b.counterOrder.counterOrderId=a.counterOrder.counterOrderId)";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> paymentCounterIdList=(List<Long>)query.list();
		if(paymentCounterIdList.isEmpty()){
			return null;
		}
		return paymentCounterIdList;
	}
	
	@Transactional
	public List<Long> findLastPaymentIdsFromCequePaymentReport(){
		String hql="select paymentCounterId from PaymentCounter a where payType!='Cash' and counterOrder.employeeGk.company.companyId="+getSessionSelectedCompaniesIds()+" and paymentCounterId in "
				+ "(SELECT max(paymentCounterId) FROM PaymentCounter b where b.counterOrder.counterOrderId=a.counterOrder.counterOrderId)";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> paymentCounterIdList=(List<Long>)query.list();
		if(paymentCounterIdList.isEmpty()){
			return null;
		}
		return paymentCounterIdList;
	}
	
	@Transactional
	public List<PaymentReportModel> fetchPaymentReportModelList(String startDate,String endDate,String range){
		
		List<Long> paymentCounterIdList=findLastPaymentIdsFromPaymentCounter();
		if(paymentCounterIdList==null){
			paymentCounterIdList=new ArrayList<>();
		}
			
		List<Payment> paymentList=fetchPaymentList(startDate,endDate,range);
		List<PaymentCounter> paymentCounterList=counterOrderDAO.fetchPaymentCounterList(startDate,endDate,range);
		List<PaymentReportModel> paymentReportModelList=new ArrayList<>();
		
		if(paymentList==null && paymentCounterList==null){
			return null;			
		}else if(paymentList==null){
			long srno=1;
			for(PaymentCounter paymentCounter: paymentCounterList){
				String empName=employeeDetailsDAO.getEmployeeDetailsByemployeeId(paymentCounter.getEmployee().getEmployeeId()).getName();
					if(paymentCounterIdList.contains(paymentCounter.getPaymentCounterId())){
						paymentReportModelList.add(new PaymentReportModel(
							srno, 
							paymentCounter.getPaymentCounterId(),
							paymentCounter.getCounterOrder().getCounterOrderId(), 
							(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
							(paymentCounter.getCurrentAmountRefund()==0)?paymentCounter.getCurrentAmountPaid():paymentCounter.getCurrentAmountRefund(), 
							(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS))?Constants.CASH_PAY_STATUS:paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
							paymentCounter.getPaidDate(),
							(paymentCounter.getCurrentAmountRefund()==0)?"Paid":"Refund",
							true,
							empName));
					}else{
						paymentReportModelList.add(new PaymentReportModel(
								srno, 
								paymentCounter.getPaymentCounterId(),
								paymentCounter.getCounterOrder().getCounterOrderId(), 
								(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
								(paymentCounter.getCurrentAmountRefund()==0)?paymentCounter.getCurrentAmountPaid():paymentCounter.getCurrentAmountRefund(), 
								(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS))?Constants.CASH_PAY_STATUS:paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
								paymentCounter.getPaidDate(),
								(paymentCounter.getCurrentAmountRefund()==0)?"Paid":"Refund",
								false,
								empName));
					}
			}
		}else if(paymentCounterList==null){
			long srno=1;
			for(Payment payment: paymentList){
				String empName=employeeDetailsDAO.getEmployeeDetailsByemployeeId(payment.getEmployee().getEmployeeId()).getName();
				paymentReportModelList.add(new PaymentReportModel(
					srno, 
					payment.getPaymentId(),
					payment.getOrderDetails().getOrderId(), 
					payment.getOrderDetails().getBusinessName().getShopName(), 
					payment.getPaidAmount(), 
					(payment.getPayType().equals(Constants.CASH_PAY_STATUS))?Constants.CASH_PAY_STATUS:payment.getBankName()+"-"+payment.getChequeNumber(), 
					payment.getPaidDate(),
					"Paid",
					true,
					empName));
				srno++;
			}
		}else{
			long srno=1;
			for(PaymentCounter paymentCounter: paymentCounterList){
				String empName=employeeDetailsDAO.getEmployeeDetailsByemployeeId(paymentCounter.getEmployee().getEmployeeId()).getName();
				if(paymentCounterIdList.contains(paymentCounter.getPaymentCounterId())){
					paymentReportModelList.add(new PaymentReportModel(
						srno, 
						paymentCounter.getPaymentCounterId(),
						paymentCounter.getCounterOrder().getCounterOrderId(), 
						(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
						(paymentCounter.getCurrentAmountRefund()==0)?paymentCounter.getCurrentAmountPaid():paymentCounter.getCurrentAmountRefund(), 
						(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS))?Constants.CASH_PAY_STATUS:paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
						paymentCounter.getPaidDate(),
						(paymentCounter.getCurrentAmountRefund()==0)?"Paid":"Refund",
						true,
						empName));
				}else{
					paymentReportModelList.add(new PaymentReportModel(
							srno, 
							paymentCounter.getPaymentCounterId(),
							paymentCounter.getCounterOrder().getCounterOrderId(), 
							(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
							(paymentCounter.getCurrentAmountRefund()==0)?paymentCounter.getCurrentAmountPaid():paymentCounter.getCurrentAmountRefund(), 
							(paymentCounter.getPayType().equals(Constants.CASH_PAY_STATUS))?Constants.CASH_PAY_STATUS:paymentCounter.getBankName()+"-"+paymentCounter.getChequeNumber(), 
							paymentCounter.getPaidDate(),
							(paymentCounter.getCurrentAmountRefund()==0)?"Paid":"Refund",
							false,
							empName));
				}
				
				srno++;
			}
			for(Payment payment: paymentList){
				String empName=employeeDetailsDAO.getEmployeeDetailsByemployeeId(payment.getEmployee().getEmployeeId()).getName();
				paymentReportModelList.add(new PaymentReportModel(
					srno, 
					payment.getPaymentId(),
					payment.getOrderDetails().getOrderId(), 
					payment.getOrderDetails().getBusinessName().getShopName(), 
					payment.getPaidAmount(), 
					(payment.getPayType().equals(Constants.CASH_PAY_STATUS))?Constants.CASH_PAY_STATUS:payment.getBankName()+"-"+payment.getChequeNumber(), 
					payment.getPaidDate(),
					"Paid",
					true,
					empName));
				srno++;
			}
			
			Collections.sort(paymentReportModelList, new Comparator<PaymentReportModel>() {
			    public int compare(PaymentReportModel m1, PaymentReportModel m2) {
			        return m1.getPaidDate().compareTo(m2.getPaidDate());
			    }
			});
			
			srno=1;
			Collections.reverse(paymentReportModelList);
			
			List<PaymentReportModel> paymentReportModelListTemp=new ArrayList<>();
			for(PaymentReportModel paymentReportModel : paymentReportModelList){
				paymentReportModel.setSrno(srno);
				paymentReportModelListTemp.add(paymentReportModel);
				srno++;
			}
			
			paymentReportModelList=paymentReportModelListTemp;
		}
		return paymentReportModelList;
	}
	
	@Transactional
	public List<ChequePaymentReportModel> fetchChequePaymentReportModelList(String startDate,String endDate,String range){
		
		List<Long> paymentCounterIdList=findLastPaymentIdsFromCequePaymentReport();
		if(paymentCounterIdList==null){
			paymentCounterIdList=new ArrayList<>();
		}
		
		List<Payment> paymentList=fetchPaymentListForChequeReport(startDate,endDate,range);
		List<PaymentCounter> paymentCounterList=counterOrderDAO.fetchPaymentCounterListForChequeReport(startDate,endDate,range);
		List<ChequePaymentReportModel> chequePaymentReportModelList=new ArrayList<>();
		
		if(paymentList==null && paymentCounterList==null){
			return null;			
		}else if(paymentList==null){
			long srno=1;
			for(PaymentCounter paymentCounter: paymentCounterList){
					if(paymentCounterIdList.contains(paymentCounter.getPaymentCounterId())){
						chequePaymentReportModelList.add(new ChequePaymentReportModel(srno,
																					paymentCounter.getCounterOrder().getCounterOrderId(), 
																					paymentCounter.getBankName(), 
																					paymentCounter.getChequeNumber(), 
																					paymentCounter.getCurrentAmountPaid(), 
																					paymentCounter.getChequeDate(), 
																					(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
																					(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerMobileNumber():paymentCounter.getCounterOrder().getBusinessName().getContact().getMobileNumber(), 
																					!paymentCounter.isStatus(),
																					paymentCounter.isChequeClearStatus(),														
																					paymentCounter.getPaymentCounterId(),
																					true,
																					paymentCounter.getPaidDate()));
					}else{
						chequePaymentReportModelList.add(new ChequePaymentReportModel(
								srno,
								paymentCounter.getCounterOrder().getCounterOrderId(), 
								paymentCounter.getBankName(), 
								paymentCounter.getChequeNumber(), 
								paymentCounter.getCurrentAmountPaid(), 
								paymentCounter.getChequeDate(), 
								(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
								(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerMobileNumber():paymentCounter.getCounterOrder().getBusinessName().getContact().getMobileNumber(), 
								!paymentCounter.isStatus(), 
								paymentCounter.isChequeClearStatus(),
								paymentCounter.getPaymentCounterId(),
								false,
								paymentCounter.getPaidDate()));
					}
					srno++;
			}
		}else if(paymentCounterList==null){
			long srno=1;
			for(Payment payment: paymentList){
				chequePaymentReportModelList.add(new ChequePaymentReportModel(srno,
						payment.getOrderDetails().getOrderId(), 
						payment.getBankName(), 
						payment.getChequeNumber(), 
						payment.getPaidAmount(), 
						payment.getChequeDate(), 
						payment.getOrderDetails().getBusinessName().getShopName(), 
						payment.getOrderDetails().getBusinessName().getContact().getMobileNumber(), 
						!payment.isStatus(), 
						payment.isChequeClearStatus(),
						payment.getPaymentId(),
						true,
						payment.getPaidDate()));
				srno++;
			}
		}else{
			long srno=1;
			for(PaymentCounter paymentCounter: paymentCounterList){
				if(paymentCounterIdList.contains(paymentCounter.getPaymentCounterId())){
					chequePaymentReportModelList.add(new ChequePaymentReportModel(srno,
																				paymentCounter.getCounterOrder().getCounterOrderId(), 
																				paymentCounter.getBankName(), 
																				paymentCounter.getChequeNumber(), 
																				paymentCounter.getCurrentAmountPaid(), 
																				paymentCounter.getChequeDate(), 
																				(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
																				(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerMobileNumber():paymentCounter.getCounterOrder().getBusinessName().getContact().getMobileNumber(), 
																				!paymentCounter.isStatus(), 
																				paymentCounter.isChequeClearStatus(),
																				paymentCounter.getPaymentCounterId(),
																				true,
																				paymentCounter.getPaidDate()));
				}else{
					chequePaymentReportModelList.add(new ChequePaymentReportModel(
							srno,
							paymentCounter.getCounterOrder().getCounterOrderId(), 
							paymentCounter.getBankName(), 
							paymentCounter.getChequeNumber(), 
							paymentCounter.getCurrentAmountPaid(), 
							paymentCounter.getChequeDate(), 
							(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerName():paymentCounter.getCounterOrder().getBusinessName().getShopName(), 
							(paymentCounter.getCounterOrder().getBusinessName()==null)?paymentCounter.getCounterOrder().getCustomerMobileNumber():paymentCounter.getCounterOrder().getBusinessName().getContact().getMobileNumber(), 
							!paymentCounter.isStatus(), 
							paymentCounter.isChequeClearStatus(),
							paymentCounter.getPaymentCounterId(),
							false,
							paymentCounter.getPaidDate()));
				}
				srno++;
		}
			for(Payment payment: paymentList){
				chequePaymentReportModelList.add(new ChequePaymentReportModel(srno,
						payment.getOrderDetails().getOrderId(), 
						payment.getBankName(), 
						payment.getChequeNumber(), 
						payment.getPaidAmount(), 
						payment.getChequeDate(), 
						payment.getOrderDetails().getBusinessName().getShopName(), 
						payment.getOrderDetails().getBusinessName().getContact().getMobileNumber(), 
						!payment.isStatus(), 
						payment.isChequeClearStatus(),
						payment.getPaymentId(),
						true,
						payment.getPaidDate()));
				srno++;
			}
			
			Collections.sort(chequePaymentReportModelList, new Comparator<ChequePaymentReportModel>() {
			    public int compare(ChequePaymentReportModel m1, ChequePaymentReportModel m2) {
			        return m1.getPaidDate().compareTo(m2.getPaidDate());
			    }
			});
			
			srno=1;
			Collections.reverse(chequePaymentReportModelList);
			
			List<ChequePaymentReportModel> chequePaymentReportModelTemp=new ArrayList<>();
			for(ChequePaymentReportModel chequePaymentReportModel : chequePaymentReportModelList){
				chequePaymentReportModel.setSrno(srno);
				chequePaymentReportModelTemp.add(chequePaymentReportModel);
				srno++;
			}
			
			chequePaymentReportModelList=chequePaymentReportModelTemp;
		}
		return chequePaymentReportModelList;
	}
	
	
	@Transactional
	public void updatePayment(Payment payment){
		orderDetailsDAO.updateOrderDetailsPaymentDays(payment.getOrderDetails());
		
		payment=(Payment)sessionFactory.getCurrentSession().merge(payment);
		sessionFactory.getCurrentSession().update(payment);
		resetDueAmountOfPaymentList(payment.getOrderDetails().getOrderId());
		
		//ledger update
		double balance=0,debit=0,creditOld=0,credit=0;		
		Ledger ledger=ledgerDAO.fetchLedger("order", String.valueOf(payment.getPaymentId()));
		List<Ledger> ledgerListBefore=ledgerDAO.fetchBeforeLedgerList(ledger.getLedgerId());
		if(ledger!=null){

			if(ledgerListBefore==null){
				balance=0;
			}else{
				balance=ledgerListBefore.get(ledgerListBefore.size()-1).getBalance();
			}
			creditOld=ledger.getCredit();
			credit=payment.getPaidAmount();			
			balance=balance+credit;
			
			ledger.setBalance(balance);
			ledger.setCredit(credit);
			ledgerDAO.updateLedger(ledger);
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//ledger update done
	}
	
	@Transactional
	public void resetDueAmountOfPaymentList(String orderId){
		List<Payment> paymentList=fetchPaymentListByOrderDetailsId(orderId);		
		if(paymentList!=null){
			OrderDetails orderDetails=orderDetailsDAO.fetchOrderDetailsByOrderId(orderId);
			double totalAmount=orderDetails.getIssuedTotalAmountWithTax();
			Collections.reverse(paymentList);
			for(Payment payment :paymentList){
				totalAmount-=payment.getPaidAmount();
				payment.setDueAmount(totalAmount);
				payment=(Payment)sessionFactory.getCurrentSession().merge(payment);
				sessionFactory.getCurrentSession().update(payment);
			}
		}		
	}
	
	@Transactional
	public void deletePayment(long paymentId){
		payment=fetchPaymentBYPaymentId(paymentId);
		payment.setStatus(true);
		payment=(Payment)sessionFactory.getCurrentSession().merge(payment);
		sessionFactory.getCurrentSession().update(payment);
		
		OrderDetails orderDetails=payment.getOrderDetails();
		
		//assign due date of payment after delete
		String hql="select max(paymentId) from Payment where orderDetails.orderId='"+orderDetails.getOrderId()+"'";
		hql+=" and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> lastPaymentCounterId=(List<Long>)query.list();
		if(lastPaymentCounterId.isEmpty()){
			orderDetails.setOrderDetailsPaymentTakeDatetime(payment.getLastDueDate());
		}else{	
			if(lastPaymentCounterId.get(0)==paymentId){
				orderDetails.setOrderDetailsPaymentTakeDatetime(payment.getLastDueDate());
			}
		}
		//end
		
		resetDueAmountOfPaymentList(orderDetails.getOrderId());
		
		orderDetails.setPayStatus(false);
		
		orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
		sessionFactory.getCurrentSession().update(orderDetails);
		
		
		
		//delete ledger
		double balance=0,debit=0,creditOld=0,credit=0;
		Ledger ledger=ledgerDAO.fetchLedger("order", String.valueOf(payment.getPaymentId()));
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			creditOld=ledger.getCredit();
			balance=balance-creditOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//delete ledger end
		
	}
	
	@Transactional
	public void defineChequeBounced(long paymentId){
		payment=fetchPaymentBYPaymentId(paymentId);
		payment.setStatus(true);
		payment.setChequeClearStatus(false);
		payment=(Payment)sessionFactory.getCurrentSession().merge(payment);
		sessionFactory.getCurrentSession().update(payment);
		
		OrderDetails orderDetails=payment.getOrderDetails();
		
		//assign due date of payment after delete
		String hql="select max(paymentId) from Payment where orderDetails.orderId='"+orderDetails.getOrderId()+"'";
		hql+=" and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Long> lastPaymentCounterId=(List<Long>)query.list();
		if(lastPaymentCounterId.isEmpty()){
			orderDetails.setOrderDetailsPaymentTakeDatetime(payment.getLastDueDate());
		}else{	
			if(lastPaymentCounterId.get(0)==paymentId){
				orderDetails.setOrderDetailsPaymentTakeDatetime(payment.getLastDueDate());
			}
		}
		//end
		
		resetDueAmountOfPaymentList(orderDetails.getOrderId());
		
		orderDetails.setPayStatus(false);
		
		orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
		sessionFactory.getCurrentSession().update(orderDetails);
		
		//delete ledger
		double balance=0,debit=0,creditOld=0,credit=0;
		Ledger ledger=ledgerDAO.fetchLedger("order", String.valueOf(payment.getPaymentId()));
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			creditOld=ledger.getCredit();
			balance=balance-creditOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//delete ledger end
		
	}
}
