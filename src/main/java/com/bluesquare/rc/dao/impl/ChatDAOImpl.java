package com.bluesquare.rc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.ChatDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeChatStatus;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DatePicker;


@Repository("chatDAO")
@Component
public class ChatDAOImpl extends TokenHandler implements ChatDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	AreaDAO areaDAO;
	
	@Autowired
	HttpSession session;

	@Transactional
	public Chat fetchLastChat(long employeeId){
		
		String hql="";
		hql="from Chat where( "+
				  "(employeeFrom.employeeId ="+employeeId+" and employeeTo.employeeId="+getAppLoggedEmployeeId()+") "
				  +" or  "+
				  "(employeeFrom.employeeId ="+getAppLoggedEmployeeId()+" and employeeTo.employeeId="+employeeId+") "
				  + ") and deleteStatus=false";
		hql+=" ORDER by chatId DESC LIMIT 1";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Chat> chatList=(List<Chat>)query.list();
		
		if(chatList.isEmpty()){
			return null;
		}
		
		return chatList.get(0);
	}
	
	@Transactional
	public List<Chat> fetchChatRecords(long employeeId,long firstChatId,int count,long lastChatId ) {
		
		String hql="";
		hql="from Chat where( "+
				  "(employeeFrom.employeeId ="+employeeId+" and employeeTo.employeeId="+getAppLoggedEmployeeId()+") "
				  +" or  "+
				  "(employeeFrom.employeeId ="+getAppLoggedEmployeeId()+" and employeeTo.employeeId="+employeeId+") "
				  + ") and deleteStatus=false";
		/*hql="from Chat where (employeeFrom.employeeId in ("+employeeId+","+getAppLoggedEmployeeId()+") "
				  +" or employeeTo.employeeId in ("+employeeId+","+getAppLoggedEmployeeId()+") "+
				   ") and deleteStatus=false";*/
		if(lastChatId!=-1){
			hql+=" and chatId>"+lastChatId;
		}else if(firstChatId!=-1){
			hql+=" and chatId<"+firstChatId;
		}
		hql+=" ORDER by chatId DESC";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		/*query.setFirstResult(start);
		query.setMaxResults(count);*/
		query.setMaxResults(count);
		List<Chat> chatList=(List<Chat>)query.list();
		
		if(chatList.isEmpty()){
			return null;
		}
		
		return chatList;
	}
	
	@Transactional
	public boolean isAnyRecordsForGivenDate(long employeeId,String date) {
		
		String hql="";
		hql="from Chat where( "+
				  "(employeeFrom.employeeId ="+employeeId+" and employeeTo.employeeId="+getAppLoggedEmployeeId()+") "
				  +" or  "+
				  "(employeeFrom.employeeId ="+getAppLoggedEmployeeId()+" and employeeTo.employeeId="+employeeId+") "
				  + ") and deleteStatus=false and date(dateTime)='"+date+"'";
		
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Chat> chatList=(List<Chat>)query.list();
		
		if(chatList.isEmpty()){
			return false;
		}
		
		return true;
	}

	@Transactional
	public void setStatusByEmployeeId(long employeeId,String status){
		EmployeeChatStatus employeeChatStatus=getStatusByEmployeeId(employeeId);
		if(employeeChatStatus!=null){
			if(status.equals(Constants.CHAT_ONLINE) || status.equals(Constants.CHAT_OFFLINE)){
				employeeChatStatus.setStatus(status);
				employeeChatStatus.setTypingStatus(false);
			}else{
				employeeChatStatus.setStatus(Constants.CHAT_ONLINE);
				employeeChatStatus.setTypingStatus(true);
			}
			employeeChatStatus=(EmployeeChatStatus)sessionFactory.getCurrentSession().merge(employeeChatStatus);
			sessionFactory.getCurrentSession().update(employeeChatStatus);
		}else{
			
			Employee employee=new Employee();
			employee.setEmployeeId(employeeId);
			
			employeeChatStatus=new EmployeeChatStatus();
			employeeChatStatus.setEmployee(employee);
			if(status.equals(Constants.CHAT_ONLINE) || status.equals(Constants.CHAT_OFFLINE)){
				employeeChatStatus.setStatus(status);
				employeeChatStatus.setTypingStatus(false);
			}else{
				employeeChatStatus.setStatus(Constants.CHAT_ONLINE);
				employeeChatStatus.setTypingStatus(true);
			}
			
			sessionFactory.getCurrentSession().save(employeeChatStatus);
		}
	}
	
	@Transactional
	public EmployeeChatStatus getStatusByEmployeeId(long employeeId){
		String hql="from EmployeeChatStatus where employee.employeeId="+employeeId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeChatStatus> employeeChatStatusList=(List<EmployeeChatStatus>)query.list();
		if(employeeChatStatusList.isEmpty()){
			return null;
		}
		return employeeChatStatusList.get(0);
	}
	
	@Transactional
	public Chat saveChat(Chat chat){
		Chat chatLast=fetchLastChat(chat.getEmployeeTo().getEmployeeId());
		
		if(chatLast==null){
			chatLast=new Chat();
			chatLast.setChatId(-1);
		}
		
		Employee employeeFrom=new Employee();
		employeeFrom.setEmployeeId(getAppLoggedEmployeeId());
		
		chat.setEmployeeFrom(employeeFrom);
		
		if(chat.getDateTime()!=null){
			if(chat.getDateTime().getTime()==0){
				chat.setDateTime(new Date());
			}
		}else{
			chat.setDateTime(new Date());
		}
		chat.setDeleteStatus(false);
				
		
		if(isAnyRecordsForGivenDate(chat.getEmployeeTo().getEmployeeId(), new SimpleDateFormat("yyyy-MM-dd").format(new Date()))){
			chat.setTodayFirstRecord(false);
		}else{
			chat.setTodayFirstRecord(true);
		}
		
		sessionFactory.getCurrentSession().save(chat);
		
		return chatLast;
	}
	
	@Transactional
	public void deleteChat(long chatId){
		Chat chat=fetchChat(chatId);
		if(chat.getEmployeeFrom().getEmployeeId()==getAppLoggedEmployeeId()){
			chat.setDeleteStatus(true);
			chat=(Chat)sessionFactory.getCurrentSession().merge(chat);
			sessionFactory.getCurrentSession().update(chat);
		}		
	}
	
	public Chat fetchChat(long chatId){
		String hql="from Chat where chatId="+chatId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Chat> chatList=(List<Chat>)query.list();
		return chatList.get(0);
	}
}
