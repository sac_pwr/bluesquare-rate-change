package com.bluesquare.rc.dao.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.entities.Address;
import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.EmailSender;
import com.bluesquare.rc.utils.JsonWebToken;
import com.bluesquare.rc.utils.OTPGenerator;
import com.bluesquare.rc.utils.SendSMS;

@Repository("companyDAO")

@Component
public class CompanyDAOImpl extends TokenHandler implements CompanyDAO{

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	HttpSession session;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	@Autowired
	ContactDAO contactDAO;
	
	@Autowired
	EmailSender emailSender;
	
	@Transactional
	public void saveCompany(Company company,String cityIdList) {
		// TODO Auto-generated method stub.
		contactDAO.save(company.getContact());
		sessionFactory.getCurrentSession().save(company);
		
		String[] cityIds=cityIdList.split(",");
		for(String cityId : cityIds){
			CompanyCities companyCities=new CompanyCities();
			City city=new City();
			city.setCityId(Long.parseLong(cityId));
			companyCities.setCity(city);
			companyCities.setCompany(company);
			sessionFactory.getCurrentSession().save(companyCities);
		}
		
	}

	@Transactional
	public List<CompanyCities> fetchCompnyCities(long companyId){
		String hql="from CompanyCities where company.companyId="+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<CompanyCities> CompanyCities=(List<CompanyCities>)query.list();
		return CompanyCities;
	}
	
	@Transactional
	public void updateCompany(Company company){
		contactDAO.update(company.getContact());
		company=(Company)sessionFactory.getCurrentSession().merge(company);
		sessionFactory.getCurrentSession().update(company);
	}
	
	@Transactional
	public void updateCompany(Company company,String cityIdList) {
		// TODO Auto-generated method stub
		contactDAO.update(company.getContact());
		company=(Company)sessionFactory.getCurrentSession().merge(company);
		sessionFactory.getCurrentSession().update(company);
		
		List<CompanyCities> companyCityList=fetchCompnyCities(company.getCompanyId());
		for(CompanyCities companyCity: companyCityList){
			companyCity=(CompanyCities)sessionFactory.getCurrentSession().merge(companyCity);
			sessionFactory.getCurrentSession().delete(companyCity);
		}
		
		String[] cityIds=cityIdList.split(",");
		for(String cityId : cityIds){
			CompanyCities companyCities=new CompanyCities();
			City city=new City();
			city.setCityId(Long.parseLong(cityId));
			companyCities.setCity(city);
			companyCities.setCompany(company);
			sessionFactory.getCurrentSession().save(companyCities);
		}
	}

	@Transactional
	public Company fetchCompanyByCompanyId(long companyId) {
		// TODO Auto-generated method stub
		String hql="from Company where companyId="+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);			
	}

	@Transactional
	public List<Company> fetchAllCompany() {
		String hql="from Company where companyId<>0";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList;		
	}
	
	@Transactional
	public String checkDuplication(String checkText,String type,long companyId){
		
		String hql="";
		if(type.equals("companyName")){
			hql="from Company where companyName='"+checkText+"'";
		}else if(type.equals("panNumber")){
			hql="from Company where panNumber='"+checkText+"'";
		}else if(type.equals("gstNo")){
			hql="from Company where gstinno='"+checkText+"'";
		}else if(type.equals("mobileNumber")){
			hql="from Company where contact.mobileNumber='"+checkText+"'";
		}else if(type.equals("userId")){
			hql="from Company where userId='"+checkText+"'";
		}else if(type.equals("telephoneNumber")){
			hql="from Company where contact.telephoneNumber='"+checkText+"'";
		}else if(type.equals("emailId")){
			hql="from Company where contact.emailId='"+checkText+"'";
		}		
		if(companyId!=0){
			hql+=" and companyId<>"+companyId;
		}
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();		
		
		if(type.equals("userId")){			
			hql="from EmployeeDetails where employee.userId='"+checkText+"' and employee.department.name='"+Constants.GATE_KEEPER_DEPT_NAME+"'";
			 query=sessionFactory.getCurrentSession().createQuery(hql);
			List<EmployeeDetails> employeeList=(List<EmployeeDetails>)query.list();
			if(companyList.isEmpty() && employeeList.isEmpty()){
				return "Success";
			}else{
				return "Failed";
			}
		}
		
		if(companyList.isEmpty()){
			return "Success";
		}
		
		return "Failed";
		
	}
	
	/*@Transactional
	public Company checkCompanyName(String companyName) {
		String hql="from Company where companyName='"+companyName.toLowerCase().trim()+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}
	
	@Transactional
	public Company checkCompanyNameForUpdate(String companyName,long companyId) {
		String hql="from Company where companyName='"+companyName.toLowerCase().trim()+"' and companyId<>"+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}

	@Transactional
	public Company checkCompanyUserName(String userName) {
		String hql="from Company where userId='"+userName.toLowerCase().trim()+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}
	
	@Transactional
	public Company checkCompanyUserNameForUpdate(String userName,long companyId) {
		String hql="from Company where userId='"+userName.toLowerCase().trim()+"' and companyId<>"+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}
	
	@Transactional
	public Company checkCompanyGstIn(String gstinno) {
		String hql="from Company where gstinno='"+gstinno.toLowerCase().trim()+"'";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}
	
	@Transactional
	public Company checkCompanyGstInForUpdate(String gstinno,long companyId) {
		String hql="from Company where gstinno='"+gstinno.toLowerCase().trim()+"' and companyId<>"+companyId;
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Company> companyList=(List<Company>)query.list();
		if(companyList.isEmpty()){
			return null;
		}
		return companyList.get(0);		
	}*/
	
	@Transactional
	public Company validateCompany(String username, String password) {
		String hql = "from Company where userId= :user and password=:pass";// and department.name='"+Constants.GATE_KEEPER_DEPT_NAME+"'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("user", username);
		query.setParameter("pass", password);
		
		@SuppressWarnings("unchecked")
		List<Company> list = (List<Company>) query.list();

		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	
	@Transactional
	public String sendSMSTOCompanies(String companiesId,String smsText,String mobileNumber) {

		try {
			 Company companySession=(Company)session.getAttribute("companyDetails");
			 String endText=" \n --"+companySession.getCompanyName();
			
			String[] companiesId2 = companiesId.split(",");
			
			if(companiesId2.length==1){
				SendSMS.sendSMS(Long.parseLong(mobileNumber), smsText+endText);
				System.out.println("SMS send to : "+companiesId2[0]);
				return "success";
			}
			
			for(int i=0; i<companiesId2.length; i++){
				Company company=fetchCompanyByCompanyId(Long.parseLong(companiesId2[i]));
				SendSMS.sendSMS(Long.parseLong(company.getContact().getMobileNumber()), smsText+endText);
				System.out.println("SMS send to : "+companiesId2[i]);
			}
			
			return "Success";
		} catch (Exception e) {
			System.out.println("sms sending failed "+e.toString());
			return "Failed";
			
		}
		
	}
	
	@Transactional
	public String sendOTPToCompanyUsingMailAndSMS(String emailIdAndMobileNumber,boolean isMobileNumber) {
			
			String otpNumber=OTPGenerator.generateOtp();
			if(isMobileNumber){
				SendSMS.sendSMS(Long.parseLong(emailIdAndMobileNumber), "Your Verification OTP is "+otpNumber);
			}else{
				String subject="OTP for Forget Password";
				String body="Your Verification OTP is "+otpNumber;
				String to=emailIdAndMobileNumber;
				boolean isHtmlMail=true;
				emailSender.sendEmail(subject, body, to, isHtmlMail);
			}
			System.out.println("Your Verification OTP is "+otpNumber);	
			
			return otpNumber;
	}
	
	
}
