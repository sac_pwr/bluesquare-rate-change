package com.bluesquare.rc.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDAO;
import com.bluesquare.rc.dao.ExpenseDAO;
import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeHolidays;
import com.bluesquare.rc.entities.Expense;
import com.bluesquare.rc.entities.ExpenseType;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.models.ExpenseListModel;
import com.bluesquare.rc.models.ProfitAndLossEntity;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.ExpenseIdGenerator;
import com.bluesquare.rc.utils.JsonWebToken;

@Repository("expenseDAO")
@Component
public class ExpenseDAOImpl extends TokenHandler implements ExpenseDAO{

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	HttpSession session;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	@Autowired
	EmployeeDAO employeeDAO;
	
	@Autowired
	LedgerDAO ledgerDAO;
	
	@Autowired
	ExpenseIdGenerator expenseIdGenerator;
	
	@Transactional
	public void saveExpense(Expense expense) {	
		
		long employeeId=getAppLoggedEmployeeId();
		if(employeeId==0){
			Company company=new Company();
			company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
			expense.setCompany(company);
		}else{
			Employee employee=new Employee();
			employee.setEmployeeId(employeeId);
			expense.setEmployee(employee);
		}
		//ExpenseIdGenerator expenseIdGenerator=new ExpenseIdGenerator(sessionFactory);
		expense.setExpenseId(expenseIdGenerator.generateExpenseId());		
				
		sessionFactory.getCurrentSession().save(expense);		
		
		//ledger entry create and company balance update
		/*Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		double balance=company.getBalance();
		double debit=expense.getAmount();
		balance=balance-debit;
		Ledger ledger=new Ledger(
				expense.getType(), 
				expense.getAddDate(), 
				null, 
				expense.getDescription(), 
				expense.getReference(), 
				debit, 
				0, 
				balance, 
				company,
				expense
				);
		ledgerDAO.saveLedger(ledger);
		company.setBalance(balance);
		companyDAO.updateCompany(company);*/
		
		double debit=expense.getAmount();
		Ledger ledger=new Ledger(
				(expense.getType().equals("Cash"))?expense.getType():expense.getBankName()+"-"+expense.getChequeNumber(), 
				expense.getAddDate(), 
				null, 
				expense.getExpenseType().getName(), 
				expense.getReference(), 
				debit, 
				0, 
				0, 
				expense
				);
		ledgerDAO.createLedgerEntry(ledger);
	}
	
	@Transactional
	public void updateExpense(Expense expense) {	
		
		//ledger entry and company balance update
		//Expense expenseOld=fetchExpense(expense.getExpenseId());
		//Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		double balance=0,debit=0,debitOld=0,credit=0;
		
		Ledger ledger=ledgerDAO.fetchLedger("expense", expense.getExpenseId());
		List<Ledger> ledgerListBefore=ledgerDAO.fetchBeforeLedgerList(ledger.getLedgerId());
		if(ledger!=null){

			if(ledgerListBefore==null){
				balance=0;
			}else{
				balance=ledgerListBefore.get(ledgerListBefore.size()-1).getBalance();
			}
			debitOld=ledger.getDebit();
			debit=expense.getAmount();			
			balance=balance-debit;
			
			ledger.setBalance(balance);
			ledger.setDebit(debit);
			ledgerDAO.updateLedger(ledger);
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
			/*List<Ledger> ledgerListAfter=ledgerDAO.fetchAfterLedgerList(ledger.getLedgerId());
			if(ledgerListAfter!=null){
				
				for(Ledger ledger2: ledgerListAfter){
					if(ledger2.getCredit()==0){
						balance=balance-ledger2.getDebit();
						ledger2.setBalance(balance);
					}else{
						balance=balance+ledger2.getDebit();
						ledger2.setBalance(balance);
					}
					ledgerDAO.updateLedger(ledger2);
				}
				
			}
			company.setBalance(balance);
			companyDAO.updateCompany(company);*/
		}
		expense=(Expense)sessionFactory.getCurrentSession().merge(expense);
		sessionFactory.getCurrentSession().update(expense);		
	}
	
	@Transactional
	public void deleteExpense(Expense expense) {	
		
		//Company company=companyDAO.fetchCompanyByCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		double balance=0,debit=0,debitOld=0,credit=0;
		
		Ledger ledger=ledgerDAO.fetchLedger("expense", expense.getExpenseId());
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			debitOld=ledger.getDebit();
			balance=balance+debitOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
			/*List<Ledger> ledgerList=ledgerDAO.fetchAfterLedgerList(ledger.getLedgerId());
			if(ledgerList!=null){				
				for(Ledger ledger2: ledgerList){
					if(ledger2.getCredit()==0){
						balance=balance-ledger2.getDebit();
						ledger2.setBalance(balance);
					}else{
						balance=balance+ledger2.getDebit();
						ledger2.setBalance(balance);
					}
					ledgerDAO.updateLedger(ledger2);
				}				
			}
			company.setBalance(balance);
			companyDAO.updateCompany(company);*/
		}
		expense=(Expense)sessionFactory.getCurrentSession().merge(expense);
		sessionFactory.getCurrentSession().delete(expense);		
	}
	
	@Transactional
	public Expense fetchExpense(String expenseId){
		String hql="from Expense where expenseId='"+expenseId+"'";
		hql+="and (company.companyId in ("+getSessionSelectedCompaniesIds()+") "
 			+ "or employee.employeeId in (select employeeId from Employee where company.companyId in ("+getSessionSelectedCompaniesIds()+")) )";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Expense> expenseList=(List<Expense>)query.list();
		if(expenseList.isEmpty()){
			return null;
		}
		return expenseList.get(0);
	}
	
	@Transactional
	public List<Expense> fetchExpenseList(){
		String hql="from Expense where 1=1 ";
		long employeeId=getAppLoggedEmployeeId();
		if(employeeId==0){
			hql+="and company.companyId="+getSessionSelectedCompaniesIds();
		}else{
			hql+="and employee.employeeId="+employeeId;
		}
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Expense> expenseList=(List<Expense>)query.list();
		if(expenseList.isEmpty()){
			return null;
		}
		return expenseList;
	}
	
	@Transactional
	public List<ExpenseListModel> fetchExpenseListByRange(String range,String startDate,String endDate){
		String hql="";
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
				
		if(range.equals("range"))
		{
			hql="from Expense where  (date(addDate)>='"+startDate+"' And date(addDate)<='"+endDate+"')";
		}
		else if(range.equals("last7days"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from Expense where  date(addDate)>='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("today"))
		{
			hql="from Expense where  date(addDate)=date(CURRENT_DATE())";
		}
		else if(range.equals("yesterday"))
		{
			cal.add(Calendar.DAY_OF_MONTH, -1);
			hql="from Expense where  date(addDate)='"+dateFormat.format(cal.getTime())+"'";
		}
		else if(range.equals("lastMonth"))
		{
			hql="from Expense where  (date(addDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(addDate)<='"+DatePicker.getLastMonthLastDate()+"')";					
		}else if(range.equals("last3Months")){
			cal.add(Calendar.MONTH, -6);
			hql="from Expense where  (date(addDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(addDate)<='"+DatePicker.getLast3MonthLastDate()+"')";
		}
		else if(range.equals("pickDate")){
			hql="from Expense where  date(addDate)='"+startDate+"'";
		}
		else if(range.equals("viewAll")){
			hql="from Expense where 1=1 ";
		}
		else if(range.equals("currentMonth"))
		{
			hql="from Expense where  (date(addDate) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(addDate) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
		}
				
		long employeeId=getAppLoggedEmployeeId();
		if(employeeId==0){
			hql+="and (company.companyId in ("+getSessionSelectedCompaniesIds()+") or employee.employeeId in (select employeeId from Employee where company.companyId in ("+getSessionSelectedCompaniesIds()+")) )";
		}else{
			hql+="and employee.employeeId="+employeeId;
		}		
		hql+=" and status=true order by addDate desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Expense> expenseList=(List<Expense>)query.list();
		if(expenseList.isEmpty()){
			return null;
		}
		
		List<ExpenseListModel> expenseListModels=new ArrayList<>();  
		
		for(Expense expense: expenseList){
			String userName;
			if(expense.getEmployee()==null){
				userName="Co.";
			}else{
				userName=employeeDAO.getEmployeeDetails(expense.getEmployee().getEmployeeId()).getName();
			}
			expenseListModels.add(new ExpenseListModel( 
					expense.getExpenseId(), 
					expense.getExpenseType().getName(), 
					expense.getReference(), 
					expense.getAmount(), 
					expense.getType().equals(Constants.CASH_PAY_STATUS)?Constants.CASH_PAY_STATUS:expense.getBankName()+"-"+expense.getChequeNumber(), 
					expense.getAddDate(), 
					expense.getUpdateDate(), 
					userName
					));
		}
		
		return expenseListModels;
	}
	
	@Transactional
	public List<ProfitAndLossEntity> expenseListForProfitAndLoss(String startDate,String endDate){
		
		List<ProfitAndLossEntity> profitAndLossEntities=new ArrayList<>();


		String hql="from Expense where (company.companyId in ("+getSessionSelectedCompaniesIds()+") or employee.employeeId in (select employeeId from Employee where company.companyId in ("+getSessionSelectedCompaniesIds()+")) )";
		hql+=" and (date(addDate)>='"+startDate+"' and date(addDate)<='"+endDate+"')";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Expense> expenseList=(List<Expense>)query.list();
		

		List<ExpenseType> expenseTypeList=fetchExpenseTypeList();
		if(expenseTypeList!=null){
			for(ExpenseType expenseType: expenseTypeList){
				double totalExpense=0;
				for(Expense expense: expenseList){
					if(expense.getExpenseType().getExpenseTypeId()==expenseType.getExpenseTypeId()){
						totalExpense+=expense.getAmount();
					}
				}
				profitAndLossEntities.add(new ProfitAndLossEntity(expenseType.getName(), totalExpense));
			}
		}
		
		return profitAndLossEntities;
	}
	
	//ExpenseType
	@Transactional
	public void saveExpenseType(ExpenseType expenseType){
		Company company=new Company();
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		expenseType.setCompany(company);
		expenseType.setStatus(false);
		expenseType.setAddedDate(new Date());
		expenseType.setUpdatedDate(null);
		sessionFactory.getCurrentSession().save(expenseType);
	}
	
	@Transactional
	public void updateExpenseType(ExpenseType expenseType){
			expenseType.setUpdatedDate(new Date());
			
			expenseType=(ExpenseType)sessionFactory.getCurrentSession().merge(expenseType);
			sessionFactory.getCurrentSession().update(expenseType);
	}
	
	@Transactional
	public ExpenseType fetchExpenseTypeByExpenseTypeId(long expenseTypeId){
			
		return (ExpenseType)sessionFactory.getCurrentSession().get(ExpenseType.class, expenseTypeId);
	}
	
	@Transactional
	public List<ExpenseType> fetchExpenseTypeList(){
		String hql="from ExpenseType where status=false and company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ExpenseType> expenseTypeList=(List<ExpenseType>)query.list();
		if(expenseTypeList.isEmpty()){
			return null;
		}
		return expenseTypeList;
	}
	
	@Transactional
	public String checkExistByExpenseTypeIdForAdd(String expenseTypeName){
			
		List<ExpenseType> expenseTypeList=fetchExpenseTypeList();
		if(expenseTypeList!=null){
			for(ExpenseType expenseType: expenseTypeList){
				if(expenseType.getName().toLowerCase().equals(expenseTypeName.toLowerCase().trim())){
					return Constants.FAILURE_RESPONSE;
				}
			}
		}
		
		return Constants.SUCCESS_RESPONSE;
	}
	
	@Transactional
	public String checkExistByExpenseTypeIdForUpdate(String expenseTypeName,long expenseTypeId){
			
		List<ExpenseType> expenseTypeList=fetchExpenseTypeList();
		if(expenseTypeList!=null){
			for(ExpenseType expenseType: expenseTypeList){
				if(expenseType.getName().toLowerCase().equals(expenseTypeName.toLowerCase().trim()) && expenseType.getExpenseTypeId()!=expenseTypeId){
					return Constants.FAILURE_RESPONSE;
				}
			}
		}
		
		return Constants.SUCCESS_RESPONSE;
	}
	
	
}
