package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.dao.BusinessTypeDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.entities.Company;

@Repository("businessTypeDAO")

@Component
public class BusinessTypeDAOImpl extends TokenHandler implements BusinessTypeDAO {

	@Autowired
	SessionFactory sessionFactory;

	@Autowired
	AreaDAO areaDAO;
	
	@Autowired
	Company company;
	
	public BusinessTypeDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public BusinessTypeDAOImpl() {
	}
	
	@Transactional
	public void saveForWebApp(BusinessType businessType) {
		// TODO Auto-generated method stub
		
		company.setCompanyId(Long.parseLong(getSessionSelectedCompaniesIds()));
		businessType.setCompany(company);
		
		sessionFactory.getCurrentSession().save(businessType);
	}

	@Transactional
	public void updateForWebApp(BusinessType businessType) {
		// TODO Auto-generated method stub
		businessType=(BusinessType)sessionFactory.getCurrentSession().merge(businessType);
		sessionFactory.getCurrentSession().update(businessType);
	}

	@Transactional
	public List<BusinessType> fetchBusinessTypeListForWebApp() {
		String hql = "from BusinessType where 1=1 ";
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessType> businessTypeList = (List<BusinessType>) query.list();

		if (businessTypeList.isEmpty()) {
			return null;
		}
		return businessTypeList;
	}

	@Transactional
	public BusinessType fetchBusinessTypeForWebApp(long businessTypeId) {
		String hql = "from BusinessType where businessTypeId=" + businessTypeId;
		hql=modifyQueryForSelectedCompaniesAccessForOtherEntities(hql);
		
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<BusinessType> businessTypeList = (List<BusinessType>) query.list();
		if (businessTypeList.isEmpty()) {
			return null;
		}

		return businessTypeList.get(0);
	}

}
