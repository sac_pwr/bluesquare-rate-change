package com.bluesquare.rc.dao.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeAreaListDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeDetails;
@Repository("employeeAreaListDAO")

@Component
public class EmployeeAreaListDAOimpl extends TokenHandler implements EmployeeAreaListDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	EmployeeAreaList employeeAreaList;
	
	@Autowired
	EmployeeDetails employeeDetails;
	
	@Autowired
	Area area;
	
	public EmployeeAreaListDAOimpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Transactional
	public void saveEmployeeAreasForWebApp(String areaIdLists, long employeeDetailsId) {

		String[] prdutIdList = areaIdLists.split(",");
		
		for(int i=0; i<prdutIdList.length; i++)
		{
			EmployeeAreaList employeeAreaList=new EmployeeAreaList();
			
			area.setAreaId(Long.parseLong(prdutIdList[i]));
			employeeAreaList.setArea(area);
			
			employeeDetails.setEmployeeDetailsId(employeeDetailsId);
			employeeAreaList.setEmployeeDetails(employeeDetails);
			
			sessionFactory.getCurrentSession().save(employeeAreaList);
		}

	}

	@Transactional
	public void updateEmployeeAreasForWebApp(String areaIdLists, long employeeDetailsId) {

		String hql="from EmployeeAreaList where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);	
		
		List<EmployeeAreaList> list=query.list();
		Iterator<EmployeeAreaList> itr=list.iterator();
		while(itr.hasNext())
		{
			EmployeeAreaList employeeAreaList=itr.next();
			employeeAreaList=(EmployeeAreaList)sessionFactory.getCurrentSession().merge(employeeAreaList);
			sessionFactory.getCurrentSession().delete(employeeAreaList);
		}
		
		
		String[] prdutIdList = areaIdLists.split(",");
		
		for(int i=0; i<prdutIdList.length; i++)
		{
			EmployeeAreaList employeeAreaList=new EmployeeAreaList();
			
			area.setAreaId(Long.parseLong(prdutIdList[i]));
			employeeAreaList.setArea(area);
			EmployeeDetails employeeDetails=new EmployeeDetails();
			employeeDetails.setEmployeeDetailsId(employeeDetailsId);
			employeeAreaList.setEmployeeDetails(employeeDetails);
			
			sessionFactory.getCurrentSession().save(employeeAreaList);
		}
	}

	@Transactional
	public void deleteForWebApp(long employeeAreaListId) {
		// TODO Auto-generated method stub
		
		employeeAreaList.setEmployeeAreaListId(employeeAreaListId);
		employeeAreaList=(EmployeeAreaList)sessionFactory.getCurrentSession().merge(employeeAreaList);
		sessionFactory.getCurrentSession().delete(employeeAreaList);
	}

	@Transactional
	public List<EmployeeAreaList> fetchEmployeeAreaListByEmployeeDetailsId(long employeeDetailsId) {

		String hql="from EmployeeAreaList where employeeDetails.employeeDetailsId='"+employeeDetailsId+"'";
		hql+=" and employeeDetails.employee.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<EmployeeAreaList> list=(List<EmployeeAreaList>)query.list();
		
		if(list.isEmpty())
		{
			return null;
		}
		
		return list;
	}

	@Transactional
	public String getAreaIdList(List<EmployeeAreaList> employeeAreaList) {
		List<EmployeeAreaList> sPList=new ArrayList<>();
		
		Iterator<EmployeeAreaList> itr=employeeAreaList.iterator();
		String areaIds="";
		while(itr.hasNext())
		{
			EmployeeAreaList employeeAreaListt=itr.next();
			areaIds=areaIds+employeeAreaListt.getArea().getAreaId()+",";
			sPList.add(employeeAreaListt);
		}			

		areaIds=areaIds.substring(0, areaIds.length() - 1);
	return areaIds;
	}

}
