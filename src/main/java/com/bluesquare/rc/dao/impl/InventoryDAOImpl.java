package com.bluesquare.rc.dao.impl;


import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.InventoryDAO;
import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.SupplierDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.Expense;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.entities.OrderUsedBrand;
import com.bluesquare.rc.entities.OrderUsedCategories;
import com.bluesquare.rc.entities.OrderUsedProduct;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.InventoryProduct;
import com.bluesquare.rc.models.InventoryReportView;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.rest.models.InventorySaveRequestModel;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.InventoryTransactionIdGenerator;

@Repository("inventoryDAO")
@Component
public class InventoryDAOImpl extends TokenHandler implements InventoryDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	LedgerDAO ledgerDAO;
	
	@Autowired
	InventoryTransactionIdGenerator inventoryTransactionIdGenerator;
	
	@Autowired
	SupplierDAO supplierDAO;
	
	SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy/MM/dd");
	
	public InventoryDAOImpl(	SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}
	
	public InventoryDAOImpl() {
		// TODO Auto-generated constructor stub
	}
	
	@Transactional
	public List<InventoryProduct> inventoryProductList() {

		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory); 
		
		List<InventoryProduct> inventoryProductsList=new ArrayList<>();
		
		List<Product> productList=productDAO.fetchProductListForWebApp();
		if(productList==null)
		{
			return null;
		}
		long srno=1;
		for(Product product : productList)
		{
			/*double igst=product.getCategories().getIgst();
			double rate=product.getRate();
			double rateWithTax= ((rate*igst)/100)+rate;
			rateWithTax=Double.parseDouble(new DecimalFormat("###").format(rateWithTax));
			double curQty=product.getCurrentQuantity();
			double taxableAmount=curQty*rate;
			double taxAmount=curQty*rateWithTax;
			double tax=	taxAmount-taxableAmount;*/
			
			double productRateWithTax=product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);
			productRateWithTax=Double.parseDouble(new DecimalFormat("###").format(productRateWithTax));
			System.out.println("MRP : "+productRateWithTax);
			
			CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(productRateWithTax, product.getCategories().getIgst());
			
			double taxableTotal = product.getCurrentQuantity() * calculateProperTaxModel.getUnitprice();
			double igstamount=product.getCurrentQuantity()*calculateProperTaxModel.getIgst();
			double cgstamount =product.getCurrentQuantity()*calculateProperTaxModel.getCgst();
			double sgstamount=product.getCurrentQuantity()*calculateProperTaxModel.getSgst();		
			double total=product.getCurrentQuantity()*calculateProperTaxModel.getMrp();
			
			inventoryProductsList.add(new InventoryProduct(
															srno,
															product, 
															calculateProperTaxModel.getMrp(), 
															taxableTotal, 
															igstamount,
															total));
			srno++;
		}
		
		return inventoryProductsList;
	}

	
	@Transactional
	public List<SupplierProductList> fetchSupplierListByProductId(long productId) {

		String hql="from SupplierProductList where product.productId="+productId+
				   " and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<SupplierProductList> supplierProductLists=(List<SupplierProductList>)query.list();
		if(supplierProductLists.isEmpty())
		{
			return null;
		}
		
		return supplierProductLists;
	}
	
	@Transactional
	public void addInventory(Inventory inventory,String productIdList)
	{
		
		EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
		Company company=(Company)session.getAttribute("companyDetails");
		
		if(employeeDetails==null){
			inventory.setCompany(company);
		}else{
			inventory.setEmployee(employeeDetails.getEmployee());
		}
		
		//supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		
		inventory.setInventoryTransactionId(inventoryTransactionIdGenerator.generateInventoryTransactionId());
		double totalAmount=0;
		double totalAmountWithTax=0;
	    long totalQuantity=0;
		
		String[] prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			
			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
			totalAmount=totalAmount+(Long.parseLong(prdutIdAndRate[1])*supplierProductList.getSupplierRate());
			
			Product product=new Product();
			product=productDAO.fetchProductForWebApp(Long.parseLong(prdutIdAndRate[0]));
			float igst=product.getCategories().getIgst();
			double rateWithTax=supplierProductList.getSupplierRate()+((igst*supplierProductList.getSupplierRate())/100);
			rateWithTax=Float.parseFloat(new DecimalFormat("###").format(rateWithTax));
			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(rateWithTax, product.getCategories().getIgst());
			
			totalAmountWithTax=totalAmountWithTax+(rateWithTax*Long.parseLong(prdutIdAndRate[1]));
			totalQuantity+=Long.parseLong(prdutIdAndRate[1]);
		}
		
		

		inventory.setTotalQuantity(totalQuantity);
		inventory.setTotalAmount(totalAmount);
		inventory.setTotalAmountTax(totalAmountWithTax);
		inventory.setPayStatus(false);
		sessionFactory.getCurrentSession().save(inventory);
		
		prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			InventoryDetails inventoryDetails=new InventoryDetails();
			
			inventoryDetails.setInventory(inventory);
			
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			Product product=new Product();
			product=productDAO.fetchProductForWebApp(Long.parseLong(prdutIdAndRate[0]));
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst());
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					product.getProductImage(),
					product.getProductContentType(),
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity());
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			inventoryDetails.setProduct(orderUsedProduct);
			
			inventoryDetails.setQuantity(Long.parseLong(prdutIdAndRate[1]));
			
			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
			
			float igst=product.getCategories().getIgst();
			double ratewithtax=supplierProductList.getSupplierRate()+(igst*supplierProductList.getSupplierRate())/100;
			ratewithtax=Double.parseDouble(new DecimalFormat("###").format(ratewithtax));
			System.out.println("MRP : "+ratewithtax);
			
			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(ratewithtax, product.getCategories().getIgst());
			
			inventoryDetails.setRate((float)ratewithtax);			
			
			inventoryDetails.setAmount(ratewithtax*Long.parseLong(prdutIdAndRate[1]));
			
			product.setCurrentQuantity(product.getCurrentQuantity()+Long.parseLong(prdutIdAndRate[1]));
			productDAO.updateDailyStockExchange(product.getProductId(), Long.parseLong(prdutIdAndRate[1]), true);
			productDAO.Update(product);
			
			sessionFactory.getCurrentSession().save(inventoryDetails);
		}
		
		//here update order product current quantity
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
		
	}
	
	
	@Transactional
	public void editInventory(Inventory inventory,String productIdList)
	{
		//supplierIdForOrder,productIdForOrder, orderQuantity,supplierMobileNumber
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		
		double totalAmount=0;
		double totalAmountWithTax=0;
	    long totalQuantity=0;
		
		String[] prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			
			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
			totalAmount=totalAmount+(Long.parseLong(prdutIdAndRate[1])*supplierProductList.getSupplierRate());
			
			Product product=new Product();
			product=productDAO.fetchProductForWebApp(Long.parseLong(prdutIdAndRate[0]));
			float igst=product.getCategories().getIgst();
			double rateWithTax=supplierProductList.getSupplierRate()+((igst*supplierProductList.getSupplierRate())/100);
			rateWithTax=Float.parseFloat(new DecimalFormat("###").format(rateWithTax));
			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(rateWithTax, product.getCategories().getIgst());
			
			totalAmountWithTax=totalAmountWithTax+(rateWithTax*Long.parseLong(prdutIdAndRate[1]));
			totalQuantity+=Long.parseLong(prdutIdAndRate[1]);
		}

		inventory.setTotalQuantity(totalQuantity);
		inventory.setTotalAmount(totalAmount);
		inventory.setTotalAmountTax(totalAmountWithTax);
		inventory.setPayStatus(false);
		
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().update(inventory);
		
		
		List<InventoryDetails> inventoryDetailsList=fetchTrasactionDetailsByInventoryId(inventory.getInventoryTransactionId());
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		for(InventoryDetails inventoryDetails:inventoryDetailsList)
		{
			Product product=productDAO.fetchProductForWebApp(inventoryDetails.getProduct().getProduct().getProductId());
			product.setCurrentQuantity(product.getCurrentQuantity()-inventoryDetails.getQuantity());
			
			productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), false);
			productDAO.Update(product);
			
			//here update order product current quantity -- not required bcos it will done below
			//orderDetailsDAO.updateOrderUsedProductCurrentQuantity(product);
			
			orderDetailsDAO.deleteOrderUsedProduct(inventoryDetails.getProduct());
			inventoryDetails=(InventoryDetails)sessionFactory.getCurrentSession().merge(inventoryDetails);
			sessionFactory.getCurrentSession().delete(inventoryDetails);
		}
		
		prdutIdAndRateList = productIdList.split(",");
		
		for(int i=0; i<prdutIdAndRateList.length; i++)
		{
			InventoryDetails inventoryDetails=new InventoryDetails();
			
			inventoryDetails.setInventory(inventory);
			
			String[] prdutIdAndRate=prdutIdAndRateList[i].split("-");
			Product product=new Product();
			product=productDAO.fetchProductForWebApp(Long.parseLong(prdutIdAndRate[0]));
			
			OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
			orderUsedBrand.setName(product.getBrand().getName());
			sessionFactory.getCurrentSession().save(orderUsedBrand);
			
			OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
					product.getCategories().getCategoryName(), 
					product.getCategories().getHsnCode(), 
					product.getCategories().getCgst(), 
					product.getCategories().getSgst(), 
					product.getCategories().getIgst());
			sessionFactory.getCurrentSession().save(orderUsedCategories);
			
			OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
					product,
					product.getProductName(), 
					product.getProductCode(), 
					orderUsedCategories, 
					orderUsedBrand, 
					product.getRate(), 
					product.getProductImage(),
					product.getProductContentType(),
					product.getThreshold(), 
					product.getCurrentQuantity(),
					product.getDamageQuantity());
							
			sessionFactory.getCurrentSession().save(orderUsedProduct);
			
			inventoryDetails.setProduct(orderUsedProduct);
			
			inventoryDetails.setQuantity(Long.parseLong(prdutIdAndRate[1]));
			
			SupplierProductList supplierProductList=supplierDAO.fetchSupplierByProductIdAndSupplierId(Long.parseLong(prdutIdAndRate[0]), inventory.getSupplier().getSupplierId());
			
			
			float igst=product.getCategories().getIgst();
			double ratewithtax=supplierProductList.getSupplierRate()+(igst*supplierProductList.getSupplierRate())/100;
			ratewithtax=Double.parseDouble(new DecimalFormat("###").format(ratewithtax));
			System.out.println("MRP : "+ratewithtax);
			
			//CalculateProperTaxModel calculateProperTaxModel=productDAO.calculateProperAmountModel(ratewithtax, product.getCategories().getIgst());
			
			inventoryDetails.setRate((float)ratewithtax);			
			
			inventoryDetails.setAmount(ratewithtax*Long.parseLong(prdutIdAndRate[1]));
			
			product.setCurrentQuantity(product.getCurrentQuantity()+Long.parseLong(prdutIdAndRate[1]));
			productDAO.updateDailyStockExchange(product.getProductId(), Long.parseLong(prdutIdAndRate[1]), true);
			productDAO.Update(product);
			
			//here update order product current quantity
			orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
			
			sessionFactory.getCurrentSession().save(inventoryDetails);
		}
		
	}

	@Transactional
	public Inventory fetchInventory(String inventoryId) {		
		Inventory inventory=fetchInventoryByInventoryTransactionIdforGateKeeperReport(inventoryId);
		return inventory;
	}
	
	@Transactional
	public PaymentDoInfo fetchPaymentStatus(String inventoryTransactionId)
	{		
		Inventory inventory=fetchInventory(inventoryTransactionId);
				
		String id=inventory.getSupplier().getSupplierId();
		String name=inventory.getSupplier().getName();
		String inventoryId=inventory.getInventoryTransactionId();
		double amountPaid=0.0d;
		double amountUnPaid=inventory.getTotalAmountTax();
		double totalAmount=inventory.getTotalAmountTax();
		String type="supplier";
		String url="giveSupplierOrderPayment";
		
		if(inventory.isPayStatus()==false)
		{
			List<PaymentPaySupplier> paymentPaySupplierList=fetchPaymentPaySupplierListByInventoryId(inventoryTransactionId);
			
			if(paymentPaySupplierList!=null)
			{
				amountUnPaid=paymentPaySupplierList.get(0).getDueAmount();
				amountPaid=totalAmount-amountUnPaid;
			}
		}
		//new PaymentDoInfo(pkId, id, name, inventoryId, amountPaid, amountUnPaid, totalAmount, type, url);
		return new PaymentDoInfo(0,id, name, inventoryId, amountPaid, amountUnPaid, totalAmount, type, url);
	}
	
	@Transactional
	public PaymentDoInfo fetchPaymentPaySupplierForEdit(String paymentPaySupplierId){
		PaymentPaySupplier paymentPaySupplier=fetchPaymentPaySupplierListByPaymentPaySupplierId(paymentPaySupplierId);
		PaymentDoInfo paymentDoInfo=fetchPaymentStatus(paymentPaySupplier.getInventory().getInventoryTransactionId());
		
		paymentDoInfo.setPaidAmount(paymentPaySupplier.getPaidAmount());
		paymentDoInfo.setBankName(paymentPaySupplier.getBankName());
		paymentDoInfo.setCheckNumber(paymentPaySupplier.getChequeNumber());
		try {
			paymentDoInfo.setCheckDate(new SimpleDateFormat("yyyy-MM-dd").format(paymentPaySupplier.getChequeDate()));
		} catch (Exception e) {
			paymentDoInfo.setCheckDate("");
		}
		paymentDoInfo.setPaymentId(paymentPaySupplier.getPaymentPayId());
		paymentDoInfo.setPayType(paymentPaySupplier.getPayType());
		paymentDoInfo.setUrl("updatePaymentSupplier");
		
		return paymentDoInfo;
	}
	
	@Transactional
	public void givePayment(PaymentPaySupplier paymentPaySupplier)
	{
		Inventory inventory=paymentPaySupplier.getInventory();
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().update(inventory);
		sessionFactory.getCurrentSession().save(paymentPaySupplier);
		
		//ledger entry create
		double debit=paymentPaySupplier.getPaidAmount();
		Ledger ledger=new Ledger(
					(paymentPaySupplier.getPayType().equals("Cash"))?paymentPaySupplier.getPayType():paymentPaySupplier.getBankName()+"-"+paymentPaySupplier.getChequeNumber(), 
					paymentPaySupplier.getPaidDate(), 
					null, 
					paymentPaySupplier.getInventory().getInventoryTransactionId()+" Payment", 
					paymentPaySupplier.getInventory().getSupplier().getName(), 
					debit, 
					0, 
					0, 
					paymentPaySupplier
				);
		ledgerDAO.createLedgerEntry(ledger);
		//ledger entry created
		
	}
	
	@Transactional
	public void updateSupplierPayment(PaymentPaySupplier paymentPaySupplier)
	{
		Inventory inventory=paymentPaySupplier.getInventory();
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().update(inventory);
		
		paymentPaySupplier=(PaymentPaySupplier)sessionFactory.getCurrentSession().merge(paymentPaySupplier);
		sessionFactory.getCurrentSession().update(paymentPaySupplier);
		
		updatePaymentSupplierDueAmount(paymentPaySupplier.getInventory().getInventoryTransactionId());
		
		//ledger update
		double balance=0,debit=0,debitOld=0,credit=0;		
		Ledger ledger=ledgerDAO.fetchLedger("supplier", String.valueOf(paymentPaySupplier.getPaymentPayId()));
		List<Ledger> ledgerListBefore=ledgerDAO.fetchBeforeLedgerList(ledger.getLedgerId());
		if(ledger!=null){

			if(ledgerListBefore==null){
				balance=0;
			}else{
				balance=ledgerListBefore.get(ledgerListBefore.size()-1).getBalance();
			}
			debitOld=ledger.getDebit();
			debit=paymentPaySupplier.getPaidAmount();			
			balance=balance-debit;
			
			ledger.setBalance(balance);
			ledger.setDebit(debit);
			ledgerDAO.updateLedger(ledger);
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//ledger update done 
		
	}
	
	@Transactional
	public void updatePaymentSupplierDueAmount(String inventoryTransactionId){
		Inventory inventory=fetchInventory(inventoryTransactionId);
		double totalAmountWithTax=inventory.getTotalAmountTax();
		
		List<PaymentPaySupplier> paymentPaySupplierList=fetchPaymentPaySupplierListByInventoryId(inventoryTransactionId);
		if(paymentPaySupplierList!=null){
			Collections.reverse(paymentPaySupplierList);
			for(PaymentPaySupplier paymentPaySupplier: paymentPaySupplierList){
				totalAmountWithTax-=paymentPaySupplier.getPaidAmount();
				paymentPaySupplier.setDueAmount(totalAmountWithTax);
				
				paymentPaySupplier=(PaymentPaySupplier)sessionFactory.getCurrentSession().merge(paymentPaySupplier);
				sessionFactory.getCurrentSession().update(paymentPaySupplier);
			}
		}
	}
	

	@Transactional
	public List<InventoryReportView> fetchInventoryReportView(String supplierId,String startDate,String endDate, String range)
	{
		List<InventoryReportView> inventoryReportViews=new ArrayList<>();
		
		String hql="";
		Query query;
		Calendar cal=Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		/**
		 * here only show inventory details which payment unpaid till current date
		 * 
		 * */
		if(supplierId.equals(""))
		{
			if(range.equals("range")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+startDate+"' and date(inventoryAddedDatetime) <= '"+endDate+"') and payStatus=false";
			}
			else if(range.equals("yesterday")){
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+dateFormat.format(cal.getTime())+"') and payStatus=false";
			}
			else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false";
			}
			else if(range.equals("lastMonth")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getLastMonthLastDate()+"') and payStatus=false";
			}
			else if(range.equals("last3Months")){
				cal.add(Calendar.MONTH, -3);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getLast3MonthLastDate()+"')  and payStatus=false";
			}
			else if(range.equals("today")){
				hql="from Inventory where (date(inventoryAddedDatetime) = date(CURRENT_DATE()) ) and payStatus=false";
			}
			else if(range.equals("currentMonth")){
				cal.add(Calendar.MONTH, -12);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"') ";
			}
			else if(range.equals("viewAll")){
				hql="from Inventory where payStatus=false";
			}
			else if(range.equals("pickDate")){
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+startDate+"') and payStatus=false";
			}
		}
		else
		{
			if(range.equals("range")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+startDate+"' and date(inventoryAddedDatetime) <= '"+endDate+"') and payStatus=false and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("yesterday")){
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+dateFormat.format(cal.getTime())+"') and payStatus=false and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+dateFormat.format(cal.getTime())+"') and payStatus=false and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("lastMonth")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getLastMonthLastDate()+"') and payStatus=false and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("last3Months")){
				cal.add(Calendar.MONTH, -3);
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getLast3MonthLastDate()+"') and payStatus=false and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("currentMonth")){
				hql="from Inventory where (date(inventoryAddedDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(inventoryAddedDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"')  and payStatus=false and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("viewAll")){
				hql="from Inventory where payStatus=false and supplier.supplierId='"+supplierId+"' order by inventoryAddedDatetime desc";
			}
			else if(range.equals("pickdate")){
				hql="from Inventory where (date(inventoryAddedDatetime) = '"+startDate+"') and payStatus=false and supplier.supplierId='"+supplierId+"'";
			}
			else if(range.equals("today")){
				hql="from Inventory where (date(inventoryAddedDatetime) = DATE(CURRENT_DATE()) ) and payStatus=false and supplier.supplierId='"+supplierId+"'";
			}
		}		
		
		hql+=" and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+") "+" order by inventoryAddedDatetime desc";
		//hql+=" order by inventoryAddedDatetime desc";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Inventory> inventoryList=(List<Inventory>)query.list();
		long srno=1;
		for(Inventory inventory:inventoryList)
		{
			double amountPaid=0;
			double amountUnPaid=0;
			
			List<PaymentPaySupplier> paymentPaySupplierList=fetchPaymentPaySupplierListByInventoryId(inventory.getInventoryTransactionId());
			
			
			if(paymentPaySupplierList==null)
			{
				amountUnPaid=inventory.getTotalAmountTax();
			}
			else
			{
				for(PaymentPaySupplier paymentPaySupplier: paymentPaySupplierList){
					amountPaid+=paymentPaySupplier.getPaidAmount();
				}
				amountUnPaid=inventory.getTotalAmountTax()-amountPaid;
			}
			
			String payStatus="";
			
			if(amountPaid>=inventory.getTotalAmountTax())
			{
				payStatus="Paid";
			}
			else if(amountUnPaid<inventory.getTotalAmountTax() && amountUnPaid>0)
			{
				payStatus="Partially Paid";
			}
			else
			{
				payStatus="UnPaid";
			}
			
			//who made this inventory
			String byUserName="";
			if(inventory.getCompany()!=null){
				byUserName="Company";
			}else{
				byUserName=employeeDetailsDAO.getEmployeeDetailsByemployeeId(inventory.getEmployee().getEmployeeId()).getName();
			}			
			inventoryReportViews.add(new InventoryReportView(
					srno, 
					inventory.getInventoryTransactionId(), 
					inventory.getSupplier(), 
					inventory.getTotalQuantity(), 
					inventory.getTotalAmount(), 
					inventory.getTotalAmountTax(), 
					amountPaid, 
					amountUnPaid, 
					inventory.getInventoryAddedDatetime(),
					inventory.getInventoryPaymentDatetime(), 
					byUserName,
					payStatus,
					inventory.getBillDate(),
					inventory.getBillNumber()));
			srno++;
		}
		
		return inventoryReportViews;
	}
	
	@Transactional
	public List<PaymentPaySupplier> fetchPaymentPaySupplierListByInventoryId(String inventoryId) {
		String hql="from PaymentPaySupplier where status=false and inventory.inventoryTransactionId='"+inventoryId+"'"+
				" and inventory.supplier.company.companyId="+getSessionSelectedCompaniesIds()+
				" order by paidDate desc";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
		if(paymentPaySupplierList.isEmpty()){
			return null;
		}
		return paymentPaySupplierList;
	}
	
	@Transactional
	public PaymentPaySupplier fetchPaymentPaySupplierListByPaymentPaySupplierId(String paymentPaySupplierId) {
		String hql="from PaymentPaySupplier where status=false and paymentPayId="+paymentPaySupplierId+
				" and inventory.supplier.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
		if(paymentPaySupplierList.isEmpty()){
			return null;
		}
		return paymentPaySupplierList.get(0);
	}
	
	@Transactional
	public void deletePaymentPaySupplierListByInventoryId(String paymentPaySupplierId) {
		
		PaymentPaySupplier paymentPaySupplier=fetchPaymentPaySupplierListByPaymentPaySupplierId(paymentPaySupplierId);
		paymentPaySupplier.setStatus(true);
		
		paymentPaySupplier=(PaymentPaySupplier)sessionFactory.getCurrentSession().merge(paymentPaySupplier);
		sessionFactory.getCurrentSession().update(paymentPaySupplier);
		
		updatePaymentSupplierDueAmount(paymentPaySupplier.getInventory().getInventoryTransactionId());

		//delete ledger
		double balance=0,debit=0,debitOld=0,credit=0;
		
		Ledger ledger=ledgerDAO.fetchLedger("supplier", String.valueOf(paymentPaySupplier.getPaymentPayId()));
		ledger=(Ledger)sessionFactory.getCurrentSession().merge(ledger);
		sessionFactory.getCurrentSession().delete(ledger);	
		if(ledger!=null){
			balance=ledger.getBalance();
			debitOld=ledger.getDebit();
			balance=balance+debitOld;
			
			ledgerDAO.updateBalanceLedgerListAfterGivenLedgerId(ledger.getLedgerId(), balance);
		}
		//delete ledger end
	}
	
	@Transactional
	public List<InventoryDetails> fetchTrasactionDetailsByInventoryId(String inventoryTransactionId)
	{
		String hql="from InventoryDetails where inventory.inventoryTransactionId='"+inventoryTransactionId+"'"+
					" and inventory.supplier.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<InventoryDetails> inventoryDetailList=(List<InventoryDetails>)query.list();
		
		return inventoryDetailList;
	}
	
	/*public List<InventoryProduct> makeInventoryProductViewProductNull(List<InventoryProduct> inventoryProductsList){
		
		List<InventoryProduct> inventoryProducts=new ArrayList<>();
		
		for(InventoryProduct inventoryProduct : inventoryProductsList)
		{
			inventoryProduct.getProduct().setProductImage(null);
			inventoryProducts.add(inventoryProduct);			
		}
		
		return inventoryProducts;
	}*/
	@Transactional
	public void saveInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel) {
		// TODO Auto-generated method stub
		
			
		
			Inventory inventory=inventorySaveRequestModel.getInventory();
			
			
			Supplier supplier=supplierDAO.fetchSupplier(inventory.getSupplier().getSupplierId());
			inventory.setSupplier(supplier);
			
			//set logged employee to inventory as adding user
			Long employeeId=getAppLoggedEmployeeId();
			Employee employee=new Employee();
			employee.setEmployeeId(employeeId);
			inventory.setEmployee(employee);
			
			inventory.setInventoryAddedDatetime(new Date());	
			try {
				inventory.setBillDate(simpleDateFormat.parse(inventorySaveRequestModel.getBillDate()));
				inventory.setInventoryPaymentDatetime(simpleDateFormat.parse(inventorySaveRequestModel.getPaymentDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			inventory.setInventoryTransactionId(inventoryTransactionIdGenerator.generateInventoryTransactionId());
			
			sessionFactory.getCurrentSession().save(inventory);  // here we are saveing the inventory object
			
			// here we are saveing the Listof InventoryDetails 
			List<InventoryDetails> finalListToSave=inventorySaveRequestModel.getInventoryDetails();
			
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			
			for(InventoryDetails inventoryDetails:finalListToSave)
			{
				Product product=new Product();
				product=productDAO.fetchProductForWebApp(inventoryDetails.getProduct().getProductId());

				OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
				orderUsedBrand.setName(product.getBrand().getName());
				sessionFactory.getCurrentSession().save(orderUsedBrand);
				
				OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
						product.getCategories().getCategoryName(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst());
				sessionFactory.getCurrentSession().save(orderUsedCategories);
				
				OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
						product,
						product.getProductName(), 
						product.getProductCode(), 
						orderUsedCategories, 
						orderUsedBrand, 
						product.getRate(), 
						product.getProductImage(), 
						product.getProductContentType(),
						product.getThreshold(), 
						product.getCurrentQuantity(),
						product.getDamageQuantity());
								
				sessionFactory.getCurrentSession().save(orderUsedProduct);
				inventoryDetails.setProduct(orderUsedProduct);
				product.setCurrentQuantity(product.getCurrentQuantity()+inventoryDetails.getQuantity());
				productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), true);
				productDAO.Update(product);
				
				inventoryDetails.setInventory(inventory);
				sessionFactory.getCurrentSession().save(inventoryDetails);
			}
			
			//here update order product current quantity
			orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
	
	}
	
	@Transactional
	public void editInventoryForApp(InventorySaveRequestModel inventorySaveRequestModel) {
		// TODO Auto-generated method stub
		
			//old inventory 
			Inventory inverntoryOld=fetchInventory(inventorySaveRequestModel.getInventory().getInventoryTransactionId());
			
			Inventory inventory=inventorySaveRequestModel.getInventory();
			inventory.setInventoryTransactionPkId(inverntoryOld.getInventoryTransactionPkId());
			inventory.setCompany(inverntoryOld.getCompany());
			inventory.setSupplier(inverntoryOld.getSupplier());
			inventory.setPayStatus(false);
			inventory.setEmployee(inverntoryOld.getEmployee());
			inventory.setInventoryAddedDatetime(inverntoryOld.getInventoryAddedDatetime());	
			try {
				inventory.setBillDate(simpleDateFormat.parse(inventorySaveRequestModel.getBillDate()));
				inventory.setInventoryPaymentDatetime(simpleDateFormat.parse(inventorySaveRequestModel.getPaymentDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace(); 
			}
			inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
			sessionFactory.getCurrentSession().update(inventory);  // here we are saveing the inventory object
			
			Product product=new Product();
			
			//old inventory details list
			List<InventoryDetails> inventoryDetailsList=fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(inventorySaveRequestModel.getInventory().getInventoryTransactionId());
			for(InventoryDetails inventoryDetails: inventoryDetailsList){
				
				product=inventoryDetails.getProduct().getProduct();
				product.setCurrentQuantity(product.getCurrentQuantity()-inventoryDetails.getQuantity());
				productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), false);
				productDAO.Update(product);
				
				orderDetailsDAO.deleteOrderUsedProduct(inventoryDetails.getProduct());
				inventoryDetails=(InventoryDetails)sessionFactory.getCurrentSession().merge(inventoryDetails);
				sessionFactory.getCurrentSession().delete(inventoryDetails);
				
			}
			
			
			// here we are saveing the Listof InventoryDetails 
			List<InventoryDetails> finalListToSave=inventorySaveRequestModel.getInventoryDetails();
			
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			
			for(InventoryDetails inventoryDetails:finalListToSave)
			{
				product=productDAO.fetchProductForWebApp(inventoryDetails.getProduct().getProductId());

				OrderUsedBrand orderUsedBrand=new OrderUsedBrand();
				orderUsedBrand.setName(product.getBrand().getName());
				sessionFactory.getCurrentSession().save(orderUsedBrand);
				
				OrderUsedCategories orderUsedCategories=new OrderUsedCategories(
						product.getCategories().getCategoryName(), 
						product.getCategories().getHsnCode(), 
						product.getCategories().getCgst(), 
						product.getCategories().getSgst(), 
						product.getCategories().getIgst());
				sessionFactory.getCurrentSession().save(orderUsedCategories);
				
				OrderUsedProduct  orderUsedProduct=new OrderUsedProduct(
						product,
						product.getProductName(), 
						product.getProductCode(), 
						orderUsedCategories, 
						orderUsedBrand, 
						product.getRate(), 
						product.getProductImage(), 
						product.getProductContentType(),
						product.getThreshold(), 
						product.getCurrentQuantity(),
						product.getDamageQuantity());
								
				sessionFactory.getCurrentSession().save(orderUsedProduct);
				
				inventoryDetails.setProduct(orderUsedProduct);
				
				product.setCurrentQuantity(product.getCurrentQuantity()+inventoryDetails.getQuantity());
				productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), true);
				productDAO.Update(product);
				
				inventoryDetails.setInventory(inventory);
				sessionFactory.getCurrentSession().save(inventoryDetails);
			}
			
			//here update order product current quantity
			orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
	}
	
	@Transactional
	public List<Inventory> fetchAddedInventoryforGateKeeperReportByDateRange(String fromDate,
			String toDate, String range) {
		
		String hql="";
		Query query;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance(); 
	
		if(range.equals("range")){
			hql="from Inventory where date(inventoryAddedDatetime)>='"+fromDate+"' And date(inventoryAddedDatetime)<='"+toDate+"'"; 
			}else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from Inventory where date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			}else if(range.equals("last1month")){
				cal.add(Calendar.MONTH, -1);
				hql="from Inventory where  date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		
			}else if(range.equals("last3months")){
				cal.add(Calendar.MONTH, -3);
		
				hql="from Inventory where  date(inventoryAddedDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			}else if(range.equals("pickDate")){
				hql="from Inventory where  date(inventoryAddedDatetime)='"+fromDate +"'";
			}
			else if(range.equals("currentDate"))
            {
                hql="from Inventory where  date(inventoryAddedDatetime)=date(CURRENT_DATE())";
            }
			else if(range.equals("viewAll"))
			{
				hql="from Inventory where 1=1 ";
			}
		
			hql+=" and supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+")   order by inventoryAddedDatetime desc";
			//hql+="  order by inventoryAddedDatetime desc";

			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<Inventory> inventoryAddedList=(List<Inventory>)query.list();
			
			if(inventoryAddedList.isEmpty()){
				return null;
			}
			return inventoryAddedList;					
	}
	@Transactional
	public Inventory fetchInventoryByInventoryTransactionIdforGateKeeperReport(String inventoryTransactionId) {
		// TODO Auto-generated method stub
		
		String hql="";
		Query query;
		
		hql="from Inventory where inventoryTransactionId='"+inventoryTransactionId +"'"+
				" and supplier.company.companyId="+getSessionSelectedCompaniesIds();
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<Inventory> inventoryList=(List<Inventory>)query.list();
		
		if(inventoryList.isEmpty()){
			return null;
		}
		return inventoryList.get(0);
	}
	@Transactional
	public List<InventoryDetails> fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(
			String inventoryTransactionId) {
		// TODO Auto-generated method stub
		String hql="";
		Query query;
		
		hql="from InventoryDetails where inventory.inventoryTransactionId='"+inventoryTransactionId +"'"+
			" and inventory.supplier.company.companyId="+getSessionSelectedCompaniesIds();
		query=sessionFactory.getCurrentSession().createQuery(hql);
		List<InventoryDetails> inventoryDetailsList=(List<InventoryDetails>)query.list();
		if(inventoryDetailsList.isEmpty()){
			return null;
		}
		
		
		return inventoryDetailsList;
	}
	
	/*public List<InventoryDetails> makeProductImageNullOfInventoryDetailsList(List<InventoryDetails> inventoryDetailsList)
	{
		List<InventoryDetails> inventoryDetailsList2=new ArrayList<>();
		
		for(InventoryDetails inventoryDetails: inventoryDetailsList)
		{
			inventoryDetails.getProduct().setProductImage(null);
			inventoryDetails.getProduct().getProduct().setProductImage(null);
			inventoryDetailsList2.add(inventoryDetails);
		}
		return inventoryDetailsList2;
	}*/
	
	@Transactional
	public double fetchTotalValueOfCurrrentInventory()
	{
		////ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		List<Product> productsList=productDAO.fetchProductListForWebApp();
		
		double totalValueOfCurrrentInventory=0;
		if(productsList!=null){
			for(Product product: productsList)
			{
				double rate=0;
				
				rate = product.getRate()+((product.getRate()*product.getCategories().getIgst())/100);
				
				totalValueOfCurrrentInventory+=product.getCurrentQuantity()*Double.parseDouble(new DecimalFormat("###").format(rate));
			}
		}
		return totalValueOfCurrrentInventory;
	}
	
	@Transactional
	public long fetchProductUnderThresholdCount(){
		
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		List<Product> productsList=productDAO.fetchProductListForWebApp();
		
		long productUnderThresholdCount=0;
		if(productsList!=null){			
			for(Product product: productsList)
			{
				if(product.getCurrentQuantity()<=product.getThreshold())
				{
					productUnderThresholdCount++;
				}
			}
		}
		return productUnderThresholdCount;
	}
	
	@Transactional
	public void deleteInventory(String inventoryId)
	{
		Inventory inventory=fetchInventory(inventoryId);
		
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
		Product product;
		//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
		List<InventoryDetails> inventoryDetailsList=fetchTrasactionDetailsByInventoryId(inventoryId);
		for(InventoryDetails inventoryDetails: inventoryDetailsList)
		{
			product=inventoryDetails.getProduct().getProduct();
			product.setCurrentQuantity(product.getCurrentQuantity()-inventoryDetails.getQuantity());
			productDAO.updateDailyStockExchange(product.getProductId(), inventoryDetails.getQuantity(), false);
			productDAO.Update(product);
			
			orderDetailsDAO.deleteOrderUsedProduct(inventoryDetails.getProduct());
			inventoryDetails=(InventoryDetails)sessionFactory.getCurrentSession().merge(inventoryDetails);
			sessionFactory.getCurrentSession().delete(inventoryDetails);
		}
		
		inventory=(Inventory)sessionFactory.getCurrentSession().merge(inventory);
		sessionFactory.getCurrentSession().delete(inventory);
		
		orderDetailsDAO.updateOrderUsedProductCurrentQuantity();
	}
	
	@Transactional
	public double totalSupplierPaidAmountForProfitAndLoss(String startDate,String endDate){
		
		String hql="from PaymentPaySupplier where (date(paidDate)>='"+startDate+"' and date(paidDate)<='"+endDate+"') and status=false and inventory.supplier.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<PaymentPaySupplier> paymentPaySupplierList=(List<PaymentPaySupplier>)query.list();
		double totalAmountPaid=0;
		for(PaymentPaySupplier paymentPaySupplier : paymentPaySupplierList){
			totalAmountPaid+=paymentPaySupplier.getPaidAmount();
		}
		return totalAmountPaid;
	}
	
}
