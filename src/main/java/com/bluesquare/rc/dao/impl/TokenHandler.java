package com.bluesquare.rc.dao.impl;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@Repository("tokenHandlerDAO")
@Component
public class TokenHandler implements TokenHandlerDAO{

	@Autowired
	HttpSession session;
	
	@Autowired
	JsonWebToken jsonWebToken;
	
	@Autowired
	CompanyDAO companyDAO;
	
	public String token="NA";
	
	public String modifyQueryForSelectedCompaniesAccessForOtherEntities(String hql){
		token=(String)session.getAttribute("authToken");
		if(token==null){
			List<Long> companyIds=(List<Long>)session.getAttribute("selectedCompanyIds");
			
			String ids="";
			if(companyIds.get(0)!=-1){
				for(Long companyId : companyIds)
				{
					ids+=companyId+",";
				}
			}else{
				List<Company> companyList=companyDAO.fetchAllCompany();
				if(companyList==null){
					ids+=companyIds.get(0)+",";
				}else{
					for(Company company : companyList)
					{
						ids+=company.getCompanyId()+",";
					}
				}					
			}
			ids=ids.substring(0, ids.length()-1);
			
			hql+=" and company.companyId in ("+ids+")";
			
			return hql;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] companyIds=decodedJWT.getClaim("jwtSelectedCompanyIds").asArray(longClass);
				
				String ids="";
				for(Long companyId : companyIds)
				{
					ids+=companyId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and company.companyId in ("+ids+")";
				
				return hql;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}
	
	public String getSessionSelectedCompaniesIds()
	{
		token=(String)session.getAttribute("authToken");
		if(token==null){
			List<Long> companyIds=(List<Long>)session.getAttribute("selectedCompanyIds");
			
			String ids="";
			//if(companyIds.get(0)!=-1){
				for(Long companyId : companyIds)
				{
					ids+=companyId+",";
				}
			/*}else{
				List<Company> companyList=companyDAO.fetchAllCompany();
				if(companyList==null){
					ids+=companyIds.get(0)+",";
				}else{
					for(Company company : companyList)
					{
						ids+=company.getCompanyId()+",";
					}
				}					
			}*/
			ids=ids.substring(0, ids.length()-1);
			
			return ids;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] companyIds=decodedJWT.getClaim("jwtSelectedCompanyIds").asArray(longClass);
				
				String ids="";
				for(Long companyId : companyIds)
				{
					ids+=companyId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				return ids;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}

	/***
	 * App logged employee id get from token
	 */
	@Transactional
	public long getAppLoggedEmployeeId() {
		token=(String)session.getAttribute("authToken");
		if(token!=null){
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class longClass = (Class) Class.forName("java.lang.Long");
				Long employeeId=decodedJWT.getClaim("employeeId").asLong();
				
				return employeeId;
			} catch (Exception e) {
				e.printStackTrace();
				return 0;
			}
		}else{
			EmployeeDetails  employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails"); 
			if(employeeDetails==null){
				return 0;
			}else{
				return employeeDetails.getEmployee().getEmployeeId();
			}
			
		}
		
	}

	@Transactional
	public String getEndTextForSMS() {
		String endText="";
		
		String loginType=(String)session.getAttribute("loginType");
		if(loginType.equals(Constants.GATE_KEEPER_DEPT_NAME)){
			
			EmployeeDetails employeeDetails=(EmployeeDetails)session.getAttribute("employeeDetails");
			endText=" \n --"+employeeDetails.getName()+"(GK-"+employeeDetails.getEmployee().getCompany().getCompanyName()+")";
			
		}else if(loginType.equals(Constants.ADMIN)){
			
			endText=" \n --Admin";
			
		}else if(loginType.equals(Constants.COMPANY_ADMIN)){
			
			Company company=(Company)session.getAttribute("companyDetails");
			endText=" \n --"+company.getCompanyName();
			
		}
		return endText;
	}
	
	
	
	/*public String modifyQueryForSelectedAccessForOtherEntities(String hql) {
		token=(String)session.getAttribute("authToken");
		if(token==null){
			SelectedAccess selectedAccess=(SelectedAccess)session.getAttribute("selectedAccess");
			
			String ids="";
			for(Long areaId : selectedAccess.getAreaIdList())
			{
				ids+=areaId+",";
			}
			ids=ids.substring(0, ids.length()-1);
			
			hql+=" and area.areaId in ("+ids+")";

			return "";
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				@SuppressWarnings("unchecked")
				Class<Long> longClass = (Class<Long>) Class.forName("java.lang.Long");
				Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
				
				String ids="";
				for(Long areaId : areaIdList)
				{
					ids+=areaId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				hql+=" and area.areaId in ("+ids+")";
				
				return "";
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}
	public String getSessionSelectedIds()
	{
		token=(String)session.getAttribute("authToken");
		if(token==null){
			SelectedAccess selectedAccess=(SelectedAccess)session.getAttribute("selectedAccess");
			
			String ids="";
			for(Long areaId : selectedAccess.getAreaIdList())
			{
				ids+=areaId+",";
			}
			ids=ids.substring(0, ids.length()-1);
			
			return ids;
		}else{
			try {
				DecodedJWT decodedJWT=jsonWebToken.verifyAndDecode(token);
				
				Class<Long> longClass = (Class<Long>) Class.forName("Long");
				Long[] areaIdList=decodedJWT.getClaim("areaIdList").asArray(longClass);
				
				String ids="";
				for(Long areaId : areaIdList)
				{
					ids+=areaId+",";
				}
				ids=ids.substring(0, ids.length()-1);
				
				return ids;
			} catch (JWTDecodeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}
	}*/
}
