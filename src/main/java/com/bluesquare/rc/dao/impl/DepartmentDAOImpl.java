package com.bluesquare.rc.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.DepartmentDAO;
import com.bluesquare.rc.entities.Country;
import com.bluesquare.rc.entities.Department;

@Repository("departmentDAO")

@Component
public class DepartmentDAOImpl extends TokenHandler implements DepartmentDAO {

	
	@Autowired
	SessionFactory sessionFactory;

	public DepartmentDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public DepartmentDAOImpl() {
	}
	
	@Transactional
	public void save(Department department) {
		sessionFactory.getCurrentSession().save(department);
	}
	
	@Transactional
	public void update(Department department) {
		department=(Department)sessionFactory.getCurrentSession().merge(department);
		sessionFactory.getCurrentSession().update(department);
	}
	
	@Transactional
	public List<Department> getDepartmentListAdmin() {
		String hql = "from Department";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Department> departmentList = (List<Department>) query.list();

		if (departmentList != null && !departmentList.isEmpty()) {
			return departmentList;
		}
		return departmentList;
	}

	@Transactional
	public boolean getDepartmentByNameBoolean(String name) {
		String hql = "from Department where name = '" + name + "' ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Department> list = (List<Department>) query.list();
		if (list != null && !list.isEmpty()) {
			return true;
		}
		return false;
	}

	@Transactional
	public Department getById(long id) {
		String hql = "from Department where departmentId=" + id;
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Department> departmentList = (List<Department>) query.list();
		if (departmentList != null && ! departmentList.isEmpty()) {
			return departmentList.get(0);
		}

		return departmentList.get(0);
	}

	@Transactional
	public void delete(long id) {
		Department departmentToDelete = new Department();
		departmentToDelete.setDepartmentId(id);
		departmentToDelete=(Department)sessionFactory.getCurrentSession().merge(departmentToDelete);
		sessionFactory.getCurrentSession().delete(departmentToDelete);
	}

	@Transactional
	public Department getDepartmentByName(String departmentName) {
		String hql = "from Department where name= '" + departmentName + "' ";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		System.out.println("department query:"+query);
		@SuppressWarnings("unchecked")
		List<Department> departmentList = (List<Department>) query.list();
		if (departmentList != null && !departmentList.isEmpty()) {
			return departmentList.get(0);
		}
		return departmentList.get(0);
	}

	@Transactional
	public void saveForWebApp(Department department) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(department);
	}

	@Transactional
	public void updateForWebApp(Department department) {
		// TODO Auto-generated method stub
		department=(Department)sessionFactory.getCurrentSession().merge(department);
		sessionFactory.getCurrentSession().update(department);
	}

	@Transactional
	public List<Department> fetchDepartmentListForWebApp() {
		String hql = "from Department where name<>'Admin'";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Department> departmentList = (List<Department>) query.list();

		if (departmentList.isEmpty()) {
			return null;
		}
		return departmentList;
	}

	@Transactional
	public Department fetchDepartmentForWebApp(long departmentId) {
		// TODO Auto-generated method stub
		return (Department)sessionFactory.getCurrentSession().get(Department.class, departmentId);
	}
	

}
