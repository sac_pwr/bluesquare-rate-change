package com.bluesquare.rc.dao.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.dao.ReturnOrderDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderStatus;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.Region;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.models.ChartDetailsResponse;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;
import com.bluesquare.rc.models.ReIssueOrderReport;
import com.bluesquare.rc.models.ReturnItemReportMain;
import com.bluesquare.rc.rest.models.GkReturnReportResponse;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeResponse;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.service.ReturnOrderService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.DatePicker;
import com.bluesquare.rc.utils.ReturnOrderIdGenerator;

@Repository("returnOrderDAO")
@Component
public class ReturnOrderDAOImpl extends TokenHandler implements ReturnOrderDAO {
	
	
	@Autowired
	ReturnOrderDAO returnOrderDAO;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	ReturnOrderIdGenerator returnOrderIdGenerator;
	
	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	OrderDetailsDAO orderDetailsDAO;
	
	@Autowired
	ProductDAO productDAO;
	
	SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy/MM/dd");

	public ReturnOrderDAOImpl(SessionFactory sessionFactory) {
		// TODO Auto-generated constructor stub
		this.sessionFactory=sessionFactory;
	}
	
	@Transactional
	public void update (ReturnOrderProduct returnOrderProduct) {
	
		returnOrderProduct=(ReturnOrderProduct)sessionFactory.getCurrentSession().merge(returnOrderProduct);
		sessionFactory.getCurrentSession().update(returnOrderProduct);
		
	}

	@Transactional
	public void save(ReturnOrderRequest returnOrderRequest) {
		
		ReturnOrderProduct returnOrderProduct=returnOrderRequest.getReturnOrderProduct();
		
		OrderDetails orderDetails=orderDetailsDAO.fetchOrderDetailsByOrderId(returnOrderProduct.getOrderDetails().getOrderId()); 
		returnOrderProduct.setOrderDetails(orderDetails);
		
		returnOrderProduct.setReturnOrderProductDatetime(new Date());
		//ReturnOrderIdGenerator returnOrderIdGenerator=new ReturnOrderIdGenerator(sessionFactory);
		returnOrderProduct.setReturnOrderProductId(returnOrderIdGenerator.generate());
		returnOrderProduct.setReIssueStatus(Constants.RETURN_ORDER_PENDING);
		
		Employee employee=new Employee();
		employee.setEmployeeId(getAppLoggedEmployeeId());
		returnOrderProduct.setEmployee(employee);
		
		sessionFactory.getCurrentSession().save(returnOrderProduct);
		
		List<ReturnOrderProductDetails> finalListToSave=returnOrderRequest.getReturnOrderProductList();
		//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);	
		List<OrderProductDetails> orderProductDetailsList=orderDetailsDAO.fetchOrderProductDetailByOrderId(returnOrderProduct.getOrderDetails().getOrderId());
		
		for(ReturnOrderProductDetails returnOrderProductDetails: finalListToSave){
			returnOrderProductDetails.setReturnOrderProduct(returnOrderProduct);
			sessionFactory.getCurrentSession().save(returnOrderProductDetails);
			
			/*Product product=returnOrderProductDetails.getProduct();
			product.setCurrentQuantity(product.getCurrentQuantity()+returnOrderProductDetails.getReturnQuantity());
			sessionFactory.getCurrentSession().save(product);*/
			
			for(OrderProductDetails orderProductDetails : orderProductDetailsList)
			{
				if(orderProductDetails.getProduct().getProductId()==returnOrderProductDetails.getProduct().getProductId())
				{
					orderProductDetails.setConfirmQuantity(orderProductDetails.getConfirmQuantity()-returnOrderProductDetails.getReturnQuantity());
					orderProductDetails.setConfirmAmount(orderProductDetails.getConfirmQuantity()*orderProductDetails.getSellingRate());
					
					orderProductDetails=(OrderProductDetails)sessionFactory.getCurrentSession().merge(orderProductDetails);
					sessionFactory.getCurrentSession().update(orderProductDetails);
				}				
			}
		}
		
		double totalConfirmAmount=0;
		double totalConfirmAmountWithTax=0;
		long totalConfirmQuantity=0;
		orderProductDetailsList=orderDetailsDAO.fetchOrderProductDetailByOrderId(returnOrderProduct.getOrderDetails().getOrderId());
		for(OrderProductDetails orderProductDetails : orderProductDetailsList)
		{
			totalConfirmAmount+=orderProductDetails.getSellingRate();
			totalConfirmAmountWithTax+=orderProductDetails.getConfirmAmount();
			totalConfirmQuantity+=orderProductDetails.getConfirmQuantity();
		}
		
		//OrderDetails orderDetails=orderDetailsDAO.fetchOrderDetailsByOrderId(returnOrderProduct.getOrderDetails().getOrderId());
		orderDetails.setConfirmTotalAmount(totalConfirmAmount);
		orderDetails.setConfirmTotalAmountWithTax(totalConfirmAmountWithTax);
		orderDetails.setConfirmTotalQuantity(totalConfirmQuantity);
		OrderStatus orderStatus=orderDetailsDAO.fetchOrderStatus(Constants.ORDER_STATUS_DELIVERED_PENDING);
		orderDetails.setOrderStatus(orderStatus);
		
		orderDetails=(OrderDetails)sessionFactory.getCurrentSession().merge(orderDetails);
		sessionFactory.getCurrentSession().update(orderDetails);
		
	}	

	@Transactional
	public List<ReturnOrderProduct> fetchReturnOrderDetailsListByDateRange(long employeeId,String fromDate , String toDate,
			String range) {
		// TODO Auto-generated method stub
		String hql="";
		Query query;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				
		if(range.equals("range")){
			hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId="+employeeId+ " And date(returnOrderProductDatetime)>='"+fromDate+"' And date(returnOrderProductDatetime)<='"+toDate+"'";
		}
		else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId="+employeeId+ " And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}else if(range.equals("last1month")){
			cal.add(Calendar.MONTH, -1);
			hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId="+employeeId+ " And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
	
		}else if(range.equals("last3months")){
			cal.add(Calendar.MONTH, -3);
			hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId="+employeeId+ " And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		
		}else if(range.equals("pickDate")){
			hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId="+employeeId+ "  And date(returnOrderProductDatetime)='"+fromDate +"' ";
		}
		else if(range.equals("viewAll"))
		{
			hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId="+employeeId +"";
		}
		else if(range.equals("currentDate"))
		{
			hql="from ReturnOrderProduct where orderDetails.employeeSM.employeeId="+employeeId +" And date(returnOrderProductDatetime)>=date(CURRENT_DATE())";
		}
		
		//hql+=" order by returnOrderProductDatetime desc";
		hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		@SuppressWarnings("unchecked")
		List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
		if(returnOrderProductList.isEmpty())
		{
			return null;
		}
		return returnOrderProductList;
		
	}

	@Transactional
	public List<ReturnOrderProductDetails> fetchReturnOrderProductDetailsByReturnOrderProductId(
			String returnOrderProductId) {
		// TODO Auto-generated method stub

		String hql="from ReturnOrderProductDetails where returnOrderProduct.returnOrderProductId='"+returnOrderProductId+"'"
				+ " and returnOrderProduct.orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		
		List<ReturnOrderProductDetails> returnOrderProductDetailsList=(List<ReturnOrderProductDetails>)query.list();
		
		
		if(returnOrderProductDetailsList.isEmpty()){
			return null;
		}
		return returnOrderProductDetailsList;
		
	}

	/*@Transactional
	public List<ReturnOrderProductDetails> makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(
			List<ReturnOrderProductDetails> returnOrderProductDetails) {
		// TODO Auto-generated method stub
		
		//List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderProductDetails;
		List<ReturnOrderProductDetails> returnOrderProductDetailsList2=new ArrayList<>();
		//List<ReturnOrderProductDetails> returnOrderProductDetailsList2=new ArrayList<>();
		
		for(ReturnOrderProductDetails  returnOrderProductList:returnOrderProductDetails){
			returnOrderProductList.getProduct().setProductImage(null);
			returnOrderProductList.getProduct().getProduct().setProductImage(null);
			returnOrderProductDetailsList2.add(returnOrderProductList);
			}         
		if(returnOrderProductDetailsList2.isEmpty()){
			return null;	
		}
		
		return returnOrderProductDetailsList2;
	}*/
	
	@Transactional
	public List<ReturnOrderProduct> fetchReturnOrderProductByOrderDetailsId(String orderDetailsId)
	{
		
		String hql="from ReturnOrderProduct where orderDetails.orderId='"+orderDetailsId+"'";
		//hql+=" order by returnOrderProductDatetime desc";
		hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
		
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
		if(returnOrderProductList.isEmpty())
		{
			return null;
		}
		
		return returnOrderProductList;
	}
	@Transactional
	public List<ReturnOrderProductDetails> fetchReturnOrderProductDetailsByReturnOrderDetailsId(String returnOrderProductId)
	{		
		String hql="from ReturnOrderProductDetails where returnOrderProduct.returnOrderProductId='"+returnOrderProductId+"'"
				+ " and returnOrderProduct.orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
		Query query=sessionFactory.getCurrentSession().createQuery(hql);
		List<ReturnOrderProductDetails> returnOrderProductDetailsList=(List<ReturnOrderProductDetails>)query.list();
		if(returnOrderProductDetailsList.isEmpty())
		{
			return null;
		}
		
		return returnOrderProductDetailsList;
	}
	
	
	//sachin 
	
	@Transactional
	public List<ReturnOrderProduct> fetchReturnOrderProductListForDBReportByEmpIdDateRange(long employeeId, 
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String hql="";
		Query query=null;
		Calendar cal = Calendar.getInstance();			
		
		List<EmployeeAreaList> employeeAreaLists=employeeDetailsDAO.fetchEmployeeAreaListByEmployeeId(employeeId);
		
		List<Long> longList=new ArrayList<>();
		
		for(EmployeeAreaList employeeAreaList: employeeAreaLists)
		{
			longList.add(employeeAreaList.getArea().getAreaId());			
		}
		
		if(range.equals("range")){
			hql="from ReturnOrderProduct r where (date(r.returnOrderProductDatetime)>='"+fromDate+"' And date(r.returnOrderProductDatetime)<='"+toDate+"')";
		}
		else if(range.equals("last7days")){
			cal.add(Calendar.DAY_OF_MONTH, -7);
			hql="from ReturnOrderProduct r where date(r.returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}else if(range.equals("last1month")){
			cal.add(Calendar.MONTH, -1);
			hql="from ReturnOrderProduct r where date(r.returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
		}else if(range.equals("last3months")){
			cal.add(Calendar.MONTH, -3);
			hql="from ReturnOrderProduct r where date(r.returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
			
		}else if(range.equals("pickDate")){
			hql="from ReturnOrderProduct r where date(r.returnOrderProductDatetime)='"+fromDate +"' ";
		}
		else if(range.equals("viewAll"))
		{
			hql="from ReturnOrderProduct r where 1=1 ";
		}
		else if(range.equals("currentDate"))
		{
			hql="from ReturnOrderProduct r where date(r.returnOrderProductDatetime)>=date(CURRENT_DATE()) ";
		}

		//hql+=" order by returnOrderProductDatetime desc";
		hql+=" and "+employeeId+"=(select e.employeeDB.employeeId from OrderProductIssueDetails e where e.orderDetails.orderId=r.orderDetails.orderId)"+
			 " and r.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by r.returnOrderProductDatetime desc";
		
		query=sessionFactory.getCurrentSession().createQuery(hql);
		//query.setParameterList("ids", longList);
		
		List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
		
		if(returnOrderProductList.isEmpty()){
			return null;
		}
		return returnOrderProductList;
	}
	
	// here we are fetching ReturnOrderProduct Object By returnOrderProductId
		@Transactional
		public ReturnOrderProduct fetchReturnOrderForGKReportByReturnOrderProductId(String returnOrderProductId) {
			// TODO Auto-generated method stub
			
			String hql="from ReturnOrderProduct where returnOrderProductId='"+returnOrderProductId+"'"
					+ " and orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReturnOrderProduct> returnOrderProduct=(List<ReturnOrderProduct>)query.list();
			if(returnOrderProduct.isEmpty()){
				return null;
			}
			return returnOrderProduct.get(0);
		}
		// here we are fetching ReturnOrderProductDetalis List  By returnOrderProductId 
		@Transactional
		public List<ReturnOrderProductDetails> fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(String returnOrderProductId) {
			// TODO Auto-generated method stub
			
			String hql="from ReturnOrderProductDetails where returnOrderProduct.returnOrderProductId='"+returnOrderProductId+"'"
					+ " and returnOrderProduct.orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReturnOrderProductDetails> returnOrderProductDetailsList=(List<ReturnOrderProductDetails>)query.list();
			if(returnOrderProductDetailsList==null){
				return null;
			}
			return returnOrderProductDetailsList;
		}
		//some problem is here we hv to resolve it
		@Transactional
		public GkReturnReportResponse fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId(long employeeId,
				long areaId, String fromDate, String toDate, String range) {
		
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql="";
			Query query;
			Calendar cal = Calendar.getInstance();
			
			GkReturnReportResponse gkReturnReportResponse= new GkReturnReportResponse();
			
			List<EmployeeAreaList> employeeAreaLists=employeeDetailsDAO.fetchEmployeeAreaListByEmployeeId(employeeId);
			
			List<Long> longList=new ArrayList<>();
			List<Area> areaList=new ArrayList<>();
			for(EmployeeAreaList employeeAreaList: employeeAreaLists)
			{
				longList.add(employeeAreaList.getArea().getAreaId());
				areaList.add(employeeAreaList.getArea());
			}
			
			gkReturnReportResponse.setAreaId(areaList);
			//query.setParameterList("ids", longList);
				
			
			if(areaId==0){
							if(range.equals("range")){
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) And (date(returnOrderProductDatetime)>='"+fromDate+"' And date(returnOrderProductDatetime)<='"+toDate+"')";
							}
							else if(range.equals("last7days")){
								cal.add(Calendar.DAY_OF_MONTH, -7);
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
							}else if(range.equals("last1month")){
								cal.add(Calendar.MONTH, -1);
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";								
							}else if(range.equals("last3months")){
								cal.add(Calendar.MONTH, -3);
                                hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
							}else if(range.equals("pickDate")){
								hql=  "from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) And date(returnOrderProductDatetime)='"+fromDate +"' ";
							}
							else if(range.equals("viewAll"))
							{
								  hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids)";
							}
							else if(range.equals("currentDate"))
							{
								  hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) And date(returnOrderProductDatetime)>=date(CURRENT_DATE())";
							}
							
							//hql+=" order by returnOrderProductDatetime desc";
							hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
							
							query=sessionFactory.getCurrentSession().createQuery(hql);
							query.setParameterList("ids", longList);
			} 
			else 
			{
							if(range.equals("range")){
							hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId ="+ areaId+" And (date(returnOrderProductDatetime)>='"+fromDate+"' And date(returnOrderProductDatetime)<='"+toDate+"') ";
							}
							else if(range.equals("last7days")){
								cal.add(Calendar.DAY_OF_MONTH, -7);
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId  ="+ areaId+" And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
							}else if(range.equals("last1month")){
								cal.add(Calendar.MONTH, -1);
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId  ="+ areaId+" And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
						
							}else if(range.equals("last3months")){
								cal.add(Calendar.MONTH, -3);
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId  ="+ areaId+" And date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"'";
							
							}else if(range.equals("pickDate")){
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId  ="+ areaId+"  And date(returnOrderProductDatetime)='"+fromDate +"' ";
							}
							else if(range.equals("viewAll"))
							{
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId ="+ areaId;
							}
							else if(range.equals("currentDate"))
							{
								hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId  ="+ areaId+" And date(returnOrderProductDatetime)>=date(CURRENT_DATE())";
							}
							
							//hql+=" order by returnOrderProductDatetime desc";
							hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
						    query=sessionFactory.getCurrentSession().createQuery(hql);
			}
			
			List<ReturnOrderProduct> GkReturnReportResponseList=(List<ReturnOrderProduct>)query.list();
			if(GkReturnReportResponseList.isEmpty()){
				gkReturnReportResponse.setReturnOrderProductList(null);
			}
			else
			{
				gkReturnReportResponse.setReturnOrderProductList(GkReturnReportResponseList);
			}
			return  gkReturnReportResponse;
		}
		@Transactional
		public ReturnOrderDateRangeResponse fetchReturnOrderProductListByFilter(long employeeId,String range,String fromDate,String toDate,long areaId)
		{
			ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
			String hql="";
			Query query=null;
			Calendar cal = Calendar.getInstance();
						
			//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
			returnOrderDateRangeResponse.setAreaList(areaList);
					
			List<Long> pnjId = new ArrayList<>();
		    Iterator<Area> iterator = areaList.iterator();
		    while (iterator.hasNext()) {
		    	Area area = iterator.next();
		        pnjId.add(area.getAreaId());
		    }
			
			if(areaId==0)
			{
				if(range.equals("range"))
				{
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and (date(returnOrderProductDatetime)>='"+fromDate+"' And date(returnOrderProductDatetime)<='"+toDate+"') and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"' ";
				}
				else if(range.equals("last7days")){
					cal.add(Calendar.DAY_OF_MONTH, -7);
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)>='"+simpleDateFormat.format(cal.getTime())+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"' ";
				}else if(range.equals("last1month")){
					cal.add(Calendar.MONTH, -1);
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)>='"+simpleDateFormat.format(cal.getTime())+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"' ";
				}else if(range.equals("last3months")){
					cal.add(Calendar.MONTH, -3);
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)>='"+simpleDateFormat.format(cal.getTime())+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";					
				}else if(range.equals("pickDate")){
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)='"+fromDate+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";	
				}
				else if(range.equals("viewAll"))
				{
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
				}
				else if(range.equals("currentDate"))
				{				
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)=date(CURRENT_DATE()) and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
				}

				//hql+=" order by returnOrderProductDatetime desc";
				hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
				query.setParameterList("ids", pnjId);
			}
			else
			{
				if(range.equals("range")){
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId="+areaId+" and (date(returnOrderProductDatetime)>='"+fromDate+"' And date(returnOrderProductDatetime)<='"+toDate+"') and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
				}
				else if(range.equals("last7days")){
					cal.add(Calendar.DAY_OF_MONTH, -7);
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId="+areaId+" and date(returnOrderProductDatetime)>='"+simpleDateFormat.format(cal.getTime())+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
				}else if(range.equals("last1month")){
					cal.add(Calendar.MONTH, -1);
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId="+areaId+" and date(returnOrderProductDatetime)>='"+simpleDateFormat.format(cal.getTime())+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
				}else if(range.equals("last3months")){
					cal.add(Calendar.MONTH, -3);
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId="+areaId+" and date(returnOrderProductDatetime)>='"+simpleDateFormat.format(cal.getTime())+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";					
				}else if(range.equals("pickDate")){
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId="+areaId+" and date(returnOrderProductDatetime)='"+fromDate+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
				}
				else if(range.equals("viewAll"))
				{
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId="+areaId+" and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"'";
				}
				else if(range.equals("currentDate"))
				{				
					hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId="+areaId+" and date(returnOrderProductDatetime)=date(CURRENT_DATE()) and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
				}
			
				//hql+=" order by returnOrderProductDatetime desc";
				hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
			}
			
			List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list(); 
			if(returnOrderProductList.isEmpty())
			{
				returnOrderDateRangeResponse.setReturnOrderProductList(null);
			}
			else
			{
				returnOrderDateRangeResponse.setReturnOrderProductList(returnOrderProductList);
			}
			
			return returnOrderDateRangeResponse;
		}
		
		@Transactional
		public ReturnOrderDateRangeResponse fetchReturnOrderProductListByFilterWeb(long employeeId,String range,String fromDate,String toDate)
		{
			ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
			String hql="";
			Query query=null;
			Calendar cal = Calendar.getInstance();
						
			//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			List<Area> areaList=employeeDetailsDAO.fetchAreaByEmployeeId(employeeId);
			returnOrderDateRangeResponse.setAreaList(areaList);
					
			List<Long> pnjId = new ArrayList<>();
		    Iterator<Area> iterator = areaList.iterator();
		    while (iterator.hasNext()) {
		    	Area area = iterator.next();
		        pnjId.add(area.getAreaId());
		    }
			
			if(range.equals("range"))
			{
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and (date(returnOrderProductDatetime)>='"+fromDate+"' And date(returnOrderProductDatetime)<='"+toDate+"') and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
			}
			else if(range.equals("last7days")){
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)>='"+simpleDateFormat.format(cal.getTime())+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
			}
			else if(range.equals("currentMonth")){
				cal.add(Calendar.MONTH, -1);
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and (date(returnOrderProductDatetime)>='"+DatePicker.getCurrentMonthStartDate()+"' and date(returnOrderProductDatetime)<='"+DatePicker.getCurrentMonthLastDate()+"') and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";	
			}
			else if(range.equals("lastMonth")){
				cal.add(Calendar.MONTH, -1);
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and (date(returnOrderProductDatetime)>='"+DatePicker.getLastMonthFirstDate()+"' and date(returnOrderProductDatetime)<='"+DatePicker.getLastMonthLastDate()+"') and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
			}else if(range.equals("last3Months")){
				cal.add(Calendar.MONTH, -3);
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and (date(returnOrderProductDatetime)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(returnOrderProductDatetime)<='"+DatePicker.getLast3MonthLastDate()+"') and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";				
			}else if(range.equals("pickDate")){
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)='"+fromDate+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";			
			}
			else if(range.equals("viewAll"))
			{
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
			}
			else if(range.equals("today"))
			{				
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)=date(CURRENT_DATE()) and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
			}
			else if(range.equals("yesterday"))
			{				
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from ReturnOrderProduct where orderDetails.businessName.area.areaId in (:ids) and date(returnOrderProductDatetime)='"+simpleDateFormat.format(cal.getTime())+"' and orderDetails.orderStatus.status='"+Constants.ORDER_STATUS_DELIVERED_PENDING+"' and  reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"'";
			}

			//hql+=" order by returnOrderProductDatetime desc";			
			hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameterList("ids", pnjId);
			
			List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list(); 
			if(returnOrderProductList.isEmpty())
			{
				returnOrderDateRangeResponse.setReturnOrderProductList(null);
			}
			else
			{
				returnOrderDateRangeResponse.setReturnOrderProductList(returnOrderProductList);
			}
			
			return returnOrderDateRangeResponse;
		}
		
		@Transactional
		public ReturnOrderProduct fetchReturnOrderProductByReIssueStatus(String orderId)
		{
			String hql="from ReturnOrderProduct where reIssueStatus='"+Constants.RETURN_ORDER_PENDING+"' and orderDetails.orderId='"+orderId+"'"+
					   " order by returnOrderProductDatetime desc"+
					   " and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";	
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReturnOrderProduct> list=(List<ReturnOrderProduct>)query.list();
			if(list.isEmpty())
			{
					return null;
			}
			return list.get(0);
		}
		
		@Transactional
		public ChartDetailsResponse fetchTopReturnOrderProducts(String range,String startDate,String endDate)
		{
			Map<Product,Long> productQuantity=new HashMap<>();
			
			//ProductDAOImpl productDAO=new ProductDAOImpl(sessionFactory);
			List<Product> productList=productDAO.fetchProductListForWebApp();
			
			/*String hql="from ReturnOrderProductDetails "+
						" order by returnOrderProduct.returnOrderProductDatetime desc";*/
			String hql="";
				
			if (range.equals("currentMonth")) {
				hql="from ReturnOrderProductDetails where (date(returnOrderProduct.returnOrderProductDatetime) >= '"+DatePicker.getCurrentMonthStartDate()+"' and date(returnOrderProduct.returnOrderProductDatetime) <= '"+DatePicker.getCurrentMonthLastDate()+"')";
			}
			else if (range.equals("lastMonth")) {
				hql="from ReturnOrderProductDetails where (date(returnOrderProduct.returnOrderProductDatetime) >= '"+DatePicker.getLastMonthFirstDate()+"' and date(returnOrderProduct.returnOrderProductDatetime) <= '"+DatePicker.getLastMonthLastDate()+"') ";
			}
			else if (range.equals("last3Months")) {
				hql="from ReturnOrderProductDetails where (date(returnOrderProduct.returnOrderProductDatetime) >= '"+DatePicker.getLast3MonthFirstDate()+"' and date(returnOrderProduct.returnOrderProductDatetime) <= '"+DatePicker.getLast3MonthLastDate()+"')";
			}
			else if (range.equals("last6Months")) {
				hql="from ReturnOrderProductDetails where (date(returnOrderProduct.returnOrderProductDatetime) >= '"+DatePicker.getLast6MonthFirstDate()+"' and date(returnOrderProduct.returnOrderProductDatetime) <= '"+DatePicker.getLast6MonthLastDate()+"')";
			}
			else if (range.equals("range")) {
				hql="from ReturnOrderProductDetails where (date(returnOrderProduct.returnOrderProductDatetime) >= '"+startDate+"' and date(returnOrderProduct.returnOrderProductDatetime) <= '"+endDate+"')";
			}
					
			hql+=" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")"+
					" order by returnOrderProduct.returnOrderProductDatetime desc";
			
			String returnProductName1="";
			float returnProductPercentage1=0;
			String returnProductName2="";
			float returnProductPercentage2=0;
			String returnProductName3="";
			float returnProductPercentage3=0;
			String returnProductName4="";
			float returnProductPercentage4=0;
			String returnProductName5="";
			float returnProductPercentage5=0;
			
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReturnOrderProductDetails> returnOrderProductDetailsList=(List<ReturnOrderProductDetails>)query.list();
			if(productList!=null){
				for(Product product : productList)
				{
					long totalProductQuantity=0;
					for(ReturnOrderProductDetails returnOrderProductDetails: returnOrderProductDetailsList)
					{
						if(product.getProductId()==returnOrderProductDetails.getProduct().getProduct().getProductId())
						{
							totalProductQuantity+=returnOrderProductDetails.getReturnQuantity();
						}
					}	
					if(totalProductQuantity>0)
					{
						productQuantity.put(product, totalProductQuantity);
					}
				}
			
				Set<Entry<Product,Long>> set = productQuantity.entrySet();
		        List<Entry<Product,Long>> list = new ArrayList<Entry<Product,Long>>(set);
		        Collections.sort( list, new Comparator<Map.Entry<Product,Long>>()
		        {
		            public int compare( Map.Entry<Product, Long> o1, Map.Entry<Product, Long> o2 )
		            {
		                return (o2.getValue()).compareTo( o1.getValue() );
		            }
		        });
		        
		        
				
				long totalQuantity=0;
				int count=0;
		        for(Map.Entry<Product,Long> entry:list)
		        {
		        	count++;
		        	totalQuantity+=entry.getValue();
		        	if(count==5)
		        	{
		        		break;
		        	}
		        }     
				
				count=0;
		        for(Map.Entry<Product,Long> entry : list){
	
		        	count++;
					if(count==1)
					{
						returnProductName1=entry.getKey().getProductName();
						returnProductPercentage1=(float) ((entry.getValue()/totalQuantity)*100);
					}
					else if(count==2)
					{
						returnProductName2=entry.getKey().getProductName();
						returnProductPercentage2=(float) ((entry.getValue()/totalQuantity)*100);
					}
					else if(count==3)
					{
						returnProductName3=entry.getKey().getProductName();
						returnProductPercentage3=(float) ((entry.getValue()/totalQuantity)*100);
					}
					else if(count==4)
					{
						returnProductName4=entry.getKey().getProductName();
						returnProductPercentage4=(float) ((entry.getValue()/totalQuantity)*100);
					}
					else if(count==5)
					{
						returnProductName5=entry.getKey().getProductName();
						returnProductPercentage5=(float) ((entry.getValue()/totalQuantity)*100);
					}
					else
					{
						break;
					}
		        }
			}
	        ChartDetailsResponse chartDetailsResponse=new ChartDetailsResponse();
			 chartDetailsResponse.setReturnProductChartDetailsResponse(returnProductName1, 
											returnProductPercentage1, 
											returnProductName2, 
											returnProductPercentage2, 
											returnProductName3, 
											returnProductPercentage3, 
											returnProductName4, 
											returnProductPercentage4, 
											returnProductName5, 
											returnProductPercentage5);
			 return chartDetailsResponse;
		}
		
		
		
		
		
		
		
		
		@Transactional
		public List<ReIssueOrderDetails> fetchReIssueOrderDetailsForReplacementReportByEmpIdAndDateRange(
				long employeeId, String fromDate, String toDate, String range) {
			// TODO Auto-generated method stub
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql="";
			Query query;
			Calendar cal = Calendar.getInstance();	
			
				if(range.equals("range"))
				{
				    hql="from ReIssueOrderDetails where employeeSMDB.employeeId="+ employeeId +" And (date(reIssueDate)>='"+fromDate+"' And date(reIssueDate)<='"+toDate+"') And status='"+ Constants.RETURN_ORDER_COMPLETE+"'";
				}
				else if(range.equals("last7days"))
				{
					cal.add(Calendar.DAY_OF_MONTH, -7);
					hql="from ReIssueOrderDetails where employeeSMDB.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"' And status='"+ Constants.RETURN_ORDER_COMPLETE+"'";
				}
				else if(range.equals("last1month"))
				{
					cal.add(Calendar.MONTH, -1);
					hql="from ReIssueOrderDetails where employeeSMDB.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'And status='"+ Constants.RETURN_ORDER_COMPLETE+"'";
				}
				else if(range.equals("last3months"))
				{
					cal.add(Calendar.MONTH, -3);
					hql="from ReIssueOrderDetails where employeeSMDB.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"' And status='"+ Constants.RETURN_ORDER_COMPLETE+"'";
				}
				else if(range.equals("pickDate"))
				{
					hql="from ReIssueOrderDetails where employeeSMDB.employeeId="+ employeeId +" And date(reIssueDate)='"+fromDate +"' And status='"+ Constants.RETURN_ORDER_COMPLETE+"'";
				}
				else if(range.equals("viewAll"))
				{
					hql="from ReIssueOrderDetails where employeeSMDB.employeeId="+ employeeId +" And status='"+ Constants.RETURN_ORDER_COMPLETE+"'";
				}
				else if(range.equals("currentDate"))
				{
					hql="from ReIssueOrderDetails where employeeSMDB.employeeId="+ employeeId +" And date(reIssueDate)>=date(CURRENT_DATE()) And status='"+ Constants.RETURN_ORDER_COMPLETE+"'";
				}
				
				//hql+=" order by reIssueDate desc";
				hql+=" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
				query=sessionFactory.getCurrentSession().createQuery(hql);
			
			List<ReIssueOrderDetails> reIssueOrderProductList=(List<ReIssueOrderDetails>)query.list();
			
			if(reIssueOrderProductList.isEmpty())
			{
				return null;
			}
			return reIssueOrderProductList;
		}

		@Transactional
		public ReIssueOrderDetails fetchReIssueOrderDetailsForReplacementReportByOrderId(long reIssueOrderId) {
			// TODO Auto-generated method stub
			String hql="";
			Query query;
			
			hql="from ReIssueOrderDetails where reIssueOrderId='"+ reIssueOrderId+"'";
			hql+=" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+")";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			
			List<ReIssueOrderDetails> reIssueOrderDetails=(List<ReIssueOrderDetails>)query.list();
			if(reIssueOrderDetails.isEmpty()) 
			{
				return null;
			}
			
			return reIssueOrderDetails.get(0);
		}

		//ReIssueOrderProductDetails
		@Transactional
		public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsForReplacementReportByOrderId(
				String orderId) {
			// TODO Auto-generated method stub
			
			String hql="";
			Query query;
			
			hql="from ReIssueOrderProductDetails  where  reIssueOrderDetails.returnOrderProduct.orderDetails.orderId='"+ orderId+"'"
					+ " and reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=(List<ReIssueOrderProductDetails>)query.list();
			if(reIssueOrderProductDetailsList.isEmpty())
			{
				return null;
			}
			
			return reIssueOrderProductDetailsList;
		}
		
		@Transactional
		public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsForReplacementReportByReIssueOrderId(
				long reIssueOrderId) {
			// TODO Auto-generated method stub
			
			String hql="";
			Query query;
			
			hql="from ReIssueOrderProductDetails  where  reIssueOrderDetails.reIssueOrderId="+ reIssueOrderId
					+ " and reIssueOrderDetails.returnOrderProduct.orderDetails.businessName.company.companyId="+getSessionSelectedCompaniesIds();
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=(List<ReIssueOrderProductDetails>)query.list();
			if(reIssueOrderProductDetailsList.isEmpty())
			{
				return null;
			}
			
			return reIssueOrderProductDetailsList;
		}

		
		// Replacement Report For GateKeeper
		
		// Replacement Report For GateKeeper
		
				@Transactional
				public List<ReIssueOrderDetails> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeApp(
						long employeeId, String fromDate, String toDate, String range) {
					// TODO Auto-generated method stub
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					String hql="";
					Query query = null;
					Calendar cal = Calendar.getInstance();	
					
					
					if(range.equals("range"))
					{
					    hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And (date(reIssueDate)>='"+fromDate+"' And date(reIssueDate)<='"+toDate+"')";
					}
					else if(range.equals("last7days"))
					{
						cal.add(Calendar.DAY_OF_MONTH, -7);
						hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'";
					}
					else if(range.equals("last1month"))
					{
						cal.add(Calendar.MONTH, -1);
						hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'";
				
					}
					else if(range.equals("last3months"))
					{
						cal.add(Calendar.MONTH, -3);
						hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'";
					
					}
					else if(range.equals("pickDate"))
					{
						hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)='"+fromDate +"'";
					}
					else if(range.equals("viewAll"))
					{
						hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +"";
					}
					else if(range.equals("currentDate"))
					{
						hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)>=date(CURRENT_DATE())";
					}
				
					//hql+=" order by reIssueDate desc";
					hql+=" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
					query=sessionFactory.getCurrentSession().createQuery(hql);
					List<ReIssueOrderDetails> reIssueOrderDetails=(List<ReIssueOrderDetails>)query.list();
					
					if(reIssueOrderDetails.isEmpty())
					{
						return null;
					}   
					return reIssueOrderDetails;
				}
		
		@Transactional
		public List<ReIssueOrderDetailsReport> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRange(long employeeId, String fromDate, String toDate, String range) {
			// TODO Auto-generated method stub
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql="";
			Query query = null;
			Calendar cal = Calendar.getInstance();				
			
			if(range.equals("range"))
			{
			    hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And (date(reIssueDate)>='"+fromDate+"' And date(reIssueDate)<='"+toDate+"')";
			}
			else if(range.equals("last7days"))
			{
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'";
			}
			else if(range.equals("last1month"))
			{
				cal.add(Calendar.MONTH, -1);
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'";		
			}
			else if(range.equals("last3months")) 
			{
				cal.add(Calendar.MONTH, -3);
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)>='"+dateFormat.format(cal.getTime())+"'";
			}
			else if(range.equals("pickDate"))
			{
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)='"+fromDate +"'";
			}
			else if(range.equals("viewAll"))
			{
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +"";
			}
			else if(range.equals("currentDate"))
			{
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDate)>=date(CURRENT_DATE())";
			}
			
			//hql+=" order by reIssueDate desc";
            hql+=" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
			
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
			
			if(reIssueOrderDetailsList.isEmpty())
			{
				return null;
			}   
			
			List<ReIssueOrderDetailsReport> reIssueOrderDetailsReportList=new ArrayList<>();
			//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			for(ReIssueOrderDetails reIssueOrderDetails: reIssueOrderDetailsList)
			{
				reIssueOrderDetailsReportList.add(new ReIssueOrderDetailsReport(reIssueOrderDetails, employeeDetailsDAO.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId())));
			}
			
			return reIssueOrderDetailsReportList;
		}
		
		@Transactional
		public List<ReIssueOrderDetailsReport> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeWeb(long employeeId, String fromDate, String toDate, String range) {
			// TODO Auto-generated method stub
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String hql="";
			Query query = null;
			Calendar cal = Calendar.getInstance();				
			
			if(range.equals("range"))
			{
			    hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And (date(reIssueDeliveredDate)>='"+fromDate+"' And date(reIssueDeliveredDate)<='"+toDate+"')";
			}
			else if(range.equals("last7days"))
			{
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDeliveredDate)>='"+dateFormat.format(cal.getTime())+"'";
			}
			else if(range.equals("currentMonth"))
			{
				cal.add(Calendar.MONTH, -1);
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And (date(reIssueDeliveredDate)>='"+DatePicker.getCurrentMonthStartDate()+"' and date(reIssueDeliveredDate)<='"+DatePicker.getCurrentMonthLastDate()+"')";		
			}
			else if(range.equals("lastMonth"))
			{
				cal.add(Calendar.MONTH, -1);
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And (date(reIssueDeliveredDate)>='"+DatePicker.getLastMonthFirstDate()+"' and date(reIssueDeliveredDate)<='"+DatePicker.getLastMonthLastDate()+"')";		
			}
			else if(range.equals("last3Months")) 
			{
				cal.add(Calendar.MONTH, -3);
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And (date(reIssueDeliveredDate)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(reIssueDeliveredDate)<='"+DatePicker.getLast3MonthLastDate()+"')";
			}
			else if(range.equals("pickDate"))
			{
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDeliveredDate)='"+fromDate +"'";
			}
			else if(range.equals("viewAll"))
			{
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +"";
			}
			else if(range.equals("today"))
			{
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDeliveredDate)>=date(CURRENT_DATE())";
			}
			else if(range.equals("yesterday"))
			{
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from ReIssueOrderDetails where employeeGK.employeeId="+ employeeId +" And date(reIssueDeliveredDate)='"+dateFormat.format(cal.getTime())+"'";
			}
			
			//hql+=" order by reIssueDeliveredDate desc";
			hql+=" and returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDeliveredDate desc";
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
			
			if(reIssueOrderDetailsList.isEmpty())
			{
				return null;
			}   
			
			List<ReIssueOrderDetailsReport> reIssueOrderDetailsReportList=new ArrayList<>();
			//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory);
			for(ReIssueOrderDetails reIssueOrderDetails: reIssueOrderDetailsList)
			{
				reIssueOrderDetailsReportList.add(new ReIssueOrderDetailsReport(reIssueOrderDetails, employeeDetailsDAO.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId())));
			}
			
			return reIssueOrderDetailsReportList;
		}

		/*@Transactional
		public List<ReIssueOrderProductDetails> makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(
				List<ReIssueOrderProductDetails> reIssueOrderProductDetails) {
			// TODO Auto-generated method stub
			
			//List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderProductDetails;
			List<ReIssueOrderProductDetails> reIssueOrderProductDetails1=new ArrayList<>();
			//List<ReturnOrderProductDetails> returnOrderProductDetailsList2=new ArrayList<>();
			
			for(ReIssueOrderProductDetails  reIssueOrderProductDetailsList:reIssueOrderProductDetails)
			{
				reIssueOrderProductDetailsList.getProduct().setProductImage(null);
				reIssueOrderProductDetailsList.getProduct().getProduct().setProductImage(null);
				reIssueOrderProductDetails1.add(reIssueOrderProductDetailsList);
				}         
			if(reIssueOrderProductDetails1.isEmpty()){
				return null;	
			}
			
			return reIssueOrderProductDetails1;
		}*/
		@Transactional
		public List<ReturnItemReportMain> fetchReturnOrderProductList(String range,String startDate,String endDate)
		{
			List<ReturnItemReportMain> returnItemReportMainList=new ArrayList<>();
			String hql="";
			Query query;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal=Calendar.getInstance();
			
			if(range.equals("range"))
			{
				hql="from ReturnOrderProduct where (date(returnOrderProductDatetime)>='"+startDate+"' And date(returnOrderProductDatetime)<='"+endDate+"')";
			}
			else if(range.equals("today"))
			{
				hql="from ReturnOrderProduct where (date(returnOrderProductDatetime)='"+dateFormat.format(cal.getTime())+"')";
			}
			else if(range.equals("yesterday"))
			{
				cal.add(Calendar.DAY_OF_MONTH, -1);
				hql="from ReturnOrderProduct where (date(returnOrderProductDatetime)='"+dateFormat.format(cal.getTime())+"')";
			}
			else if(range.equals("last7days"))
			{
				cal.add(Calendar.DAY_OF_MONTH, -7);
				hql="from ReturnOrderProduct where (date(returnOrderProductDatetime)>='"+dateFormat.format(cal.getTime())+"')";
			}
			else if(range.equals("currentMonth"))
			{
				hql="from ReturnOrderProduct where (date(returnOrderProductDatetime)>='"+DatePicker.getCurrentMonthStartDate()+"' and date(returnOrderProductDatetime)<='"+DatePicker.getCurrentMonthLastDate()+"')";
			}
			else if(range.equals("lastMonth"))
			{
				hql="from ReturnOrderProduct where (date(returnOrderProductDatetime)>='"+DatePicker.getLastMonthFirstDate()+"' and date(returnOrderProductDatetime)<='"+DatePicker.getLastMonthLastDate()+"')";
			}
			else if(range.equals("last3Months"))
			{
				hql="from ReturnOrderProduct where (date(returnOrderProductDatetime)>='"+DatePicker.getLast3MonthFirstDate()+"' and date(returnOrderProductDatetime)<='"+DatePicker.getLast3MonthLastDate()+"')";
			}
			else if(range.equals("pickDate"))
			{
				hql="from ReturnOrderProduct where (date(returnOrderProductDatetime)='"+startDate+"')";
			}
			else if(range.equals("viewAll"))
			{
				hql="from ReturnOrderProduct where 1=1 ";
			}
			
			//hql+=" order by returnOrderProductDatetime desc";
			hql+=" and orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by returnOrderProductDatetime desc";
			
			query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReturnOrderProduct> returnOrderProductList=(List<ReturnOrderProduct>)query.list();
			if(returnOrderProductList.isEmpty())
			{
				return null;
			}
			
			//OrderDetailsDAOImpl orderDetailsDAO=new OrderDetailsDAOImpl(sessionFactory);
			//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory); 
			
			for(ReturnOrderProduct returnOrderProduct : returnOrderProductList)	
			{
				ReIssueOrderDetails reIssueOrderDetails=orderDetailsDAO.fetchReIssueOrderDetails(returnOrderProduct.getReturnOrderProductId());
				returnItemReportMainList.add(new ReturnItemReportMain(	returnOrderProduct, 
																		employeeDetailsDAO.getEmployeeDetailsByemployeeId(returnOrderProduct.getOrderDetails().getEmployeeSM().getEmployeeId()), 
																		reIssueOrderDetails));
			}
			
			return returnItemReportMainList;
		}
		
		@Transactional
		public List<ReIssueOrderReport> fetchReIssueOrderReportList()
		{
			List<ReIssueOrderReport> reIssueOrderReportList=new ArrayList<>();
			/*String hql="from ReIssueOrderDetails  order by reIssueDate desc";*/

			String hql="from ReIssueOrderDetails where "+
			" returnOrderProduct.orderDetails.businessName.company.companyId in ("+getSessionSelectedCompaniesIds()+") order by reIssueDate desc";
			
			Query query=sessionFactory.getCurrentSession().createQuery(hql);
			List<ReIssueOrderDetails> reIssueOrderDetailsList=(List<ReIssueOrderDetails>)query.list();
			if(reIssueOrderDetailsList.isEmpty())
			{
				return null;
			}
			
			//EmployeeDetailsDAOImpl employeeDetailsDAO=new EmployeeDetailsDAOImpl(sessionFactory); 
			
			for(ReIssueOrderDetails reIssueOrderDetails : reIssueOrderDetailsList)	
			{				
				reIssueOrderReportList.add(new ReIssueOrderReport(
											reIssueOrderDetails, 
											employeeDetailsDAO.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getReturnOrderProduct().getOrderDetails().getEmployeeSM().getEmployeeId()), 
											employeeDetailsDAO.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId())
											));
			}
			
			return reIssueOrderReportList;
		}
}
