package com.bluesquare.rc.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.ContactDAO;
import com.bluesquare.rc.entities.Chat;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.entities.Contact;

@Repository("contactDAO")


@Component
public class ContactDAOImpl extends TokenHandler implements ContactDAO {

	@Autowired
	SessionFactory sessionFactory;
	
	public ContactDAOImpl(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public ContactDAOImpl() { }

	@Transactional
	public void save(Contact contact) {
		sessionFactory.getCurrentSession().save(contact);
	}

	@Transactional
	public void update(Contact contact) {
		contact=(Contact)sessionFactory.getCurrentSession().merge(contact);
		sessionFactory.getCurrentSession().update(contact);
	}
	
	@Transactional
	public void delete(long id) {
		Contact ContactToDelete = new Contact();
		ContactToDelete.setContactId(id);
		ContactToDelete=(Contact)sessionFactory.getCurrentSession().merge(ContactToDelete);
		sessionFactory.getCurrentSession().delete(ContactToDelete);
	}

}
