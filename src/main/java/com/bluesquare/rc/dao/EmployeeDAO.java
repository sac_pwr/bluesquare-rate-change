package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeDetails;

public interface EmployeeDAO {


	public Employee validate(String username, String password);


	public EmployeeDetails getEmployeeDetails(long employeeId);

	public Employee loginCredentialsForRC(String userid, String password);
	
	public void logout(long employeeId);
	
	public void saveForWebApp(Employee employee);
	
	public void updateForWebApp(Employee employee);
	
		
	public String checkEmployeeDuplication(String checkText,String type,long employeeDetailsId);
	
	 public String checkAppVersion(String appVersion);
}