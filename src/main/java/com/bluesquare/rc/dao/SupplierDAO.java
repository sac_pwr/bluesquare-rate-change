package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.FetchSupplierList;
import com.bluesquare.rc.rest.models.ProductAddInventory;

public interface SupplierDAO {
	
	public List<Supplier> fetchSupplierForWebAppList();
	public Supplier fetchSupplier(String supplierId);
	public Supplier saveSupplier(Supplier supplier);
	public Supplier updateSupplier(Supplier supplier);
	public Supplier saveSupplierProductList(String productIdList,String supplierId);
	public List<FetchSupplierList> fetchSupplierForWebApp();
	public List<SupplierProductList> fetchSupplierProductListBySupplierIdForWebApp(String supplierId);
	public List<SupplierProductList> fetchSupplierProductListForChangeUnitPriceByTaxSlab(long categoryId);
	public void updateSupplierProductList(SupplierProductList supplierProductList);
	//public List<SupplierProductList> makeSupplierProductListNullForWebApp(List<SupplierProductList> supplierProductList);
	public String getProductIdList(List<SupplierProductList> supplierProductList);
	public Supplier updateSupplierProductList(String productIdList,String supplierId);
	public SupplierProductList fetchSupplierProductRate(long productId,String supplierId);
	public SupplierProductList fetchSupplierByProductIdAndSupplierId(long productId,String supplierId);
	public List<ProductAddInventory> fetchProductBySupplierId(String supplierId);
	public String sendSMSTOSupplier(String supplierIds,String smsText, String mobileNumber);
	public String checkSupplierDuplication(String checkText,String type,String supplierId);
}
