package com.bluesquare.rc.dao;

import java.util.List;

import com.bluesquare.rc.entities.Department;

public interface DepartmentDAO {

	public void save(Department department);

	public void update(Department department);

	public List<Department> getDepartmentListAdmin();

	public boolean getDepartmentByNameBoolean(String name);

	public Department getById(long id);

	public void delete(long id);
	
	public Department getDepartmentByName(String departmentName);
	
	//webApp
		public void saveForWebApp(Department department);

		public void updateForWebApp(Department department);
		
		public List<Department> fetchDepartmentListForWebApp();
		
		public Department fetchDepartmentForWebApp(long departmentId);
}
