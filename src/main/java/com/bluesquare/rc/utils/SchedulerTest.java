package com.bluesquare.rc.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.ProductDAO;



@Component
@EnableScheduling
@Configuration
public class SchedulerTest {

	

	@Autowired
	ProductDAO productDAO;

	// Daily generate bill
	// second, minute, hour, day of month, month, day(s) of week
	//@Scheduled(cron = "05 00 00 * * *")
	@Scheduled(cron = "00 00 12 * * *")
	public void dailyCronJob() {
		
		System.out.println("inside dailyCronJob");
		if(productDAO.fetchTodaysDailyStockDetails()==null){
			productDAO.creatFirstRecordOfDailyStockReport();
		}
	}

}
