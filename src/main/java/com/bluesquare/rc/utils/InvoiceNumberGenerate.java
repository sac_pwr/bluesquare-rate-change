/**
 * 
 */
package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.OrderDetails;

/**
 * @author aNKIT
 *
 */
@Component
public class InvoiceNumberGenerate {

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public InvoiceNumberGenerate(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generateInvoiceNumber() {

        try {
        	String prefix = "INV";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	//OrderDetails Start
        	String hql="from OrderDetails where invoiceNumber is not null and businessName.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds();        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<OrderDetails> orderDetailsList=(List<OrderDetails>)query.list();        	      	
        	//OrderDetails End
        	
        	//CounterOrder Start
        	hql="from CounterOrder where invoiceNumber is not null and employeeGk.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds();        	
        	query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<CounterOrder> counterOrderList=(List<CounterOrder>)query.list();
        	//CounterOrder End
        	
        	long ids[]=new long[orderDetailsList.size()+counterOrderList.size()]; 
        	int i=0;
        	
        	if(orderDetailsList.isEmpty() && counterOrderList.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateInvoiceNumber : " + generatedId);
                
                return generatedId;
        	}else if(counterOrderList.isEmpty()){
        		for(OrderDetails orderDetails : orderDetailsList){
            		
            		ids[i]=Long.parseLong(orderDetails.getInvoiceNumber().substring(3));
    	        	i++;
            	}
        	}else if(orderDetailsList.isEmpty()){
        		for(CounterOrder counterOrder : counterOrderList){
            		
            		ids[i]=Long.parseLong(counterOrder.getInvoiceNumber().substring(3));
    	        	i++;
            	}
        	}else{
        		for(OrderDetails orderDetails : orderDetailsList){            		
            		ids[i]=Long.parseLong(orderDetails.getInvoiceNumber().substring(3));
    	        	i++;
            	}
				for(CounterOrder counterOrder : counterOrderList){					
					ids[i]=Long.parseLong(counterOrder.getInvoiceNumber().substring(3));
					i++;
				}
        	}        	
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
            
            if(ids.length==0)
            {
            	String generatedId = prefix + new Long(1).toString();
                System.out.println("generateInvoiceNumber : " + generatedId);
                
                return generatedId;
            }
        	
            long id=max+1;
            String generatedId = prefix + new Long(id).toString();
            System.out.println("InvoiceNumberGenerate Id: " + generatedId);
            
            return generatedId;
            
        } catch (Exception e) {
            System.out.println("InvoiceNumberGenerate Error : "+e.toString());        }

        return null;
	}

}
