package com.bluesquare.rc.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MathUtils {

	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public static float roundFloat(Double value, int places) {
	    return roundFromFloat(value.floatValue(),places);
	}
	
	public static float roundFromFloatMain(float value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	    String valueToBeAdded="0.";
	    for(int i=0;i<places;i++){
	    	valueToBeAdded +="0";
	    }
	    valueToBeAdded+="1";
	    Float floatAddition = Float.parseFloat(valueToBeAdded);
	    value+=floatAddition;
//	    switch(places){
//	    	case 5:
//	    		value+=0.000001;
//	    		break;
//	    	case 4:
//	    		value+=0.00001;
//	    		break;
//	    	case 3:
//	    		value+=0.0001;
//	    		break;
//	    	case 2:
//	    		value+=0.001;
//	    		break;
//	    	case 1:
//	    		value+=0.01;
//	    		break;
//	    	case 0:
//	    		value+=0.1;
//	    		break;
//	    }
	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.floatValue();
	}
	
	
	public static float roundFromFloat(float value, int places) {
	    if (places < 0) throw new IllegalArgumentException();
	    else if(places==0){
	    	return Float.parseFloat(new DecimalFormat("#").format(value));
	    }else{
	    	String stringValue = value+"";
		    String[] strInfo = stringValue.split("\\.");
		    int length = strInfo[1].length();
		    
		    for(int i=length;i>places;i--){
		    	value=roundFromFloatMain(value,i-1);
		    }
		    return value;
	    }
	    
	}
	
	
	public static void main(String args[]){
		System.out.println(roundFromFloat(101.7143f,2));
		
//		BigDecimal bd = new BigDecimal(15.2545);
//	    bd = bd.setScale(2, RoundingMode.HALF_UP);
//	    
//	    System.out.println(bd.floatValue());
//		System.out.println(new DecimalFormat("#.##").format(15.2545));
		/**
		 String stringValue = value+"";
	    String[] strInfo = stringValue.split(".");
	    int length = strInfo[1].length();
	    switch(length){
	    	case 5:
	    		value+=0.00001;
	    		break;
	    	case 4:
	    		value+=0.0001;
	    		break;
	    	case 3:
	    		value+=0.001;
	    		break;
	    	case 2:
	    		value+=0.01;
	    		break;
	    	case 1:
	    		value+=0.1;
	    		break;
	    	case 0:
	    		value+=0.0;
	    		break;
	    }
		 */
	}
}