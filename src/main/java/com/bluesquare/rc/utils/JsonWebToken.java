package com.bluesquare.rc.utils;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.bluesquare.rc.entities.Employee;

@Component
public class JsonWebToken {

	private String issuer;
	private final static String algoSecret="secret";
	
	/*public String create(Map<String,List<Long>> claimMap, String issuer){
		String token="NA";
		try {
			this.issuer=issuer;
			
		    Algorithm algorithm = Algorithm.HMAC256(algoSecret);
		    
		    Builder jwtBuilder= JWT.create()
	        .withIssuer(issuer);		    
		    
		    for (Map.Entry<String,List<Long>> entry : claimMap.entrySet()){
		    	List<Long> entryList = entry.getValue();
		    	Long[] entryArray = new Long[entryList.size()];
		    	
		    	int i=0;
		    	for(Long id: entryList){
		    		entryArray[i]=id;
		    		i++;
		    	}
		    	
		    	jwtBuilder=jwtBuilder.withArrayClaim(entry.getKey(), entryArray);
		    }
		    
		    token = jwtBuilder.sign(algorithm);
		    return token;
		} catch (UnsupportedEncodingException exception){
		    //UTF-8 encoding not supported
			System.out.println("JsonWebToken Error "+exception.toString());
			return token;
		} catch (JWTCreationException exception){
		    //Invalid Signing configuration / Couldn't convert Claims.
			System.out.println("JsonWebToken Error "+exception.toString());
			return token;
		}
	}*/
	public String create(Map<String,List<Long>> claimMap,Long companyIds[], String issuer,long employeeId){
		String token="NA";
		try {
			this.issuer=issuer;
			
		    Algorithm algorithm = Algorithm.HMAC256(algoSecret);
		    
		    Builder jwtBuilder= JWT.create()
	        .withIssuer(issuer);		    
		    
		    jwtBuilder=jwtBuilder.withClaim("employeeId", employeeId);
		    jwtBuilder=jwtBuilder.withArrayClaim("jwtSelectedCompanyIds", companyIds);
		    
		    for (Map.Entry<String,List<Long>> entry : claimMap.entrySet()){
		    	List<Long> entryList = entry.getValue();
		    	Long[] entryArray = new Long[entryList.size()];
		    	
		    	int i=0;
		    	for(Long id: entryList){
		    		entryArray[i]=id;
		    		i++;
		    	}
		    	
		    	jwtBuilder=jwtBuilder.withArrayClaim(entry.getKey(), entryArray);
		    }
		    
		    token = jwtBuilder.sign(algorithm);
		    return token;
		} catch (UnsupportedEncodingException exception){
		    //UTF-8 encoding not supported
			System.out.println("JsonWebToken Error "+exception.toString());
			return token;
		} catch (JWTCreationException exception){
		    //Invalid Signing configuration / Couldn't convert Claims.
			System.out.println("JsonWebToken Error "+exception.toString());
			return token;
		}
	}
	
	
	public DecodedJWT verifyAndDecode(String token){
				
		try {
		    Algorithm algorithm = Algorithm.HMAC256(algoSecret);
		    JWTVerifier verifier = JWT.require(algorithm)
		        .withIssuer(issuer)
		        //.acceptLeeway(1)
		        //.acceptExpiresAt(5)
		        .build(); //Reusable verifier instance
		    DecodedJWT jwt = verifier.verify(token);
		    /*Class<Long> longClass = (Class<Long>) Class.forName("Long");
		    jwt.getClaim("").asArray(longClass);*/
		  return jwt;  
		} catch (UnsupportedEncodingException exception){
		    //UTF-8 encoding not supported
			System.out.println("verify token Error "+exception.toString());
			return null;
		} catch (JWTVerificationException exception){
		    //Invalid signature/claims
			System.out.println("verify token Error "+exception.toString());
			return null;
		}/* catch (ClassNotFoundException exception) {
			// TODO Auto-generated catch block
			System.out.println("verify token Error "+exception.toString());
			return null;
		}*/
	}
	
	public boolean verifyToken(String token){
		if(verifyAndDecode(token)==null){
			return true;
		}
		return false;
	}
	
}
