package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoyMain;

@Component
public class ReturnFromDeliveryBoyGenerator{

	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public ReturnFromDeliveryBoyGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public ReturnFromDeliveryBoyGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
	
	public String generateReturnFromDeliveryBoyMainId() {

        try {
        	
        	String prefix = "RFD";

        	String hql="from ReturnFromDeliveryBoyMain where orderDetails.businessName.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds();
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<ReturnFromDeliveryBoyMain> list=(List<ReturnFromDeliveryBoyMain>)query.list();
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateReturnFromDeliveryBoyMainId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain : list)
        	{
        		ids[i]=Long.parseLong(returnFromDeliveryBoyMain.getReturnFromDeliveryBoyMainId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
            
            String generatedId = prefix + new Long(id).toString();
            System.out.println("generateReturnFromDeliveryBoyMainId : " + generatedId);
            
            return generatedId;
            
        } catch (Exception e) {
            System.out.println("generateReturnFromDeliveryBoyMainId Error : "+e.toString());        }

        return null;
	}

}
