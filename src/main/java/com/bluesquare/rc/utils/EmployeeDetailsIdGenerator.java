package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.entities.EmployeeDetails;

@Component
public class EmployeeDetailsIdGenerator{

	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public EmployeeDetailsIdGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public EmployeeDetailsIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	


	public String generateEmployeeDetailsId(Department department) {

        try {
        	
        	String prefix = department.getShortNameForEmployeeId();

        	String hql="from EmployeeDetails where employeeDetailsGenId LIKE '"+prefix+"%' and employee.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds();
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<EmployeeDetails> list=(List<EmployeeDetails>)query.list();
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateEmployeeDetailsGenId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(EmployeeDetails employeeDetails : list)
        	{
        		ids[i]=Long.parseLong(employeeDetails.getEmployeeDetailsGenId().substring(2));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++) 
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
            String generatedId = prefix + new Long(id).toString();
            System.out.println("generateEmployeeDetailsGenId : " + generatedId);
            
            return generatedId;
            
        } catch (Exception e) {
            System.out.println("generateEmployeeDetailsGenId Error : "+e.toString());        }

        return null;
	}

}
