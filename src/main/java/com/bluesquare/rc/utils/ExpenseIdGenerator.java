package com.bluesquare.rc.utils;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Expense;
import com.bluesquare.rc.entities.OrderDetails;

@Component
public class ExpenseIdGenerator{

	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public ExpenseIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
    
    public String generateExpenseId() {

        try {
        	String prefix = "EXP";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="from Expense where company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds();
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<Expense> list=(List<Expense>)query.list();
        	
        	if(list.isEmpty())
        	{
        		String generatedId = prefix + new Long(1).toString();
                System.out.println("generateExpenseId : " + generatedId);
                
                return generatedId;
        	}
        	
        	
        	long ids[]=new long[list.size()]; 
        	int i=0;
        	for(Expense expense : list)
        	{
        		ids[i]=Long.parseLong(expense.getExpenseId().substring(3));
        		i++;
        	}
        	
        	long max = ids[0];
            for(int p = 0; p < ids.length; p++)
            {
                if(max < ids[p])
                {
                    max = ids[p];
                }
            }
        	
            long id=max+1;
                String generatedId = prefix + new Long(id).toString();
                System.out.println("generateExpenseId Id: " + generatedId);
                return generatedId;
            
        } catch (Exception e) {
            System.out.println("generateExpenseId Error : "+e.toString());        }

        return null;
	}

}
