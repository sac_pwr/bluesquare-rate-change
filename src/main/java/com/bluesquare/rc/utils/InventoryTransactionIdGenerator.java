package com.bluesquare.rc.utils;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.Inventory;

@Component
public class InventoryTransactionIdGenerator{

	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	public InventoryTransactionIdGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public InventoryTransactionIdGenerator(SessionFactory sessionFactory) {
		this.sessionFactory=sessionFactory;
	}
	
	@Transactional
	public String generateInventoryTransactionId() {
		
    try {
        	String prefix = "TSN";

        	//long count=(long) sessionFactory.getCurrentSession().createCriteria("Inventory").setProjection(Projections.rowCount()).uniqueResult();
        	
        	String hql="select inventoryTransactionId from Inventory where supplier.company.companyId="+tokenHandlerDAO.getSessionSelectedCompaniesIds()+" order by inventoryTransactionId";
        	
        	Query query=sessionFactory.getCurrentSession().createQuery(hql); 
        	List<String> inventorySupplierViewList=(List<String>)query.list();
        	
            long id;
    		
    		if(inventorySupplierViewList.isEmpty()){
    			id=1;
    		}else{
    		
    		  // id = gettingMissingNo(inventorySupplierViewList);
	    		//if(id==0){
	    			String lastNo=inventorySupplierViewList.get(inventorySupplierViewList.size()-1);
	
	    			String[] part = lastNo.split("(?<=\\D)(?=\\d)");
	    			 id = Long.parseLong(part[1]);
	    			 id+=1;
	    		//}
    		}
            
                String generatedId = prefix + new Long(id).toString();
                System.out.println("InventoryTransactionIdGenerator Id: " + generatedId);
                return generatedId;
            
        } catch (Exception e) {
            System.out.println("InventoryTransactionIdGenerator Error : "+e.toString());        }

        return null;
	}
	public static long gettingMissingNo(List<String> inventorySupplierList) {

		ArrayList<Integer> arr = new ArrayList<Integer>();		

		ArrayList<Integer> totalNumbers = new ArrayList<>();

		for (int i = 0; i < inventorySupplierList.size(); i++) {

			String[] part = inventorySupplierList.get(i).split("(?<=\\D)(?=\\d)");
			String number = part[1];
			
			totalNumbers.add(Integer.parseInt(number));
		}

		int j = 1001;
		for (int i = 0; i < totalNumbers.size(); i++) {
			if (j == totalNumbers.get(i)) {
				j++;
				continue;
			} else {
				arr.add(j);
				break;
			}
		}

		//System.out.println("missing no" + arr.get(0));
		if(arr.size()<1){
			return 0;
		}
		return arr.get(0);
	}
}
