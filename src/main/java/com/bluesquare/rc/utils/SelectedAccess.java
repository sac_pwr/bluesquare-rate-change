package com.bluesquare.rc.utils;

import java.util.List;

public class SelectedAccess {

	private List<Long> areaIdList;
	private List<Long> regionIdList;
	private List<Long> cityIdList;
	private List<Long> stateIdList;
	private List<Long> countryIdList;
	
	
	
	public SelectedAccess(List<Long> areaIdList, List<Long> regionIdList, List<Long> cityIdList, List<Long> stateIdList,
			List<Long> countryIdList) {
		super();
		this.areaIdList = areaIdList;
		this.regionIdList = regionIdList;
		this.cityIdList = cityIdList;
		this.stateIdList = stateIdList;
		this.countryIdList = countryIdList;
	}
	public List<Long> getAreaIdList() {
		return areaIdList;
	}
	public void setAreaIdList(List<Long> areaIdList) {
		this.areaIdList = areaIdList;
	}
	public List<Long> getRegionIdList() {
		return regionIdList;
	}
	public void setRegionIdList(List<Long> regionIdList) {
		this.regionIdList = regionIdList;
	}
	public List<Long> getCityIdList() {
		return cityIdList;
	}
	public void setCityIdList(List<Long> cityIdList) {
		this.cityIdList = cityIdList;
	}
	public List<Long> getStateIdList() {
		return stateIdList;
	}
	public void setStateIdList(List<Long> stateIdList) {
		this.stateIdList = stateIdList;
	}
	public List<Long> getCountryIdList() {
		return countryIdList;
	}
	public void setCountryIdList(List<Long> countryIdList) {
		this.countryIdList = countryIdList;
	}
	@Override
	public String toString() {
		return "AllAccess [areaIdList=" + areaIdList + ", regionIdList=" + regionIdList + ", cityIdList=" + cityIdList
				+ ", stateIdList=" + stateIdList + ", countryIdList=" + countryIdList + "]";
	}
}
 