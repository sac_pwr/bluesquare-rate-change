package com.bluesquare.rc.utils;

import java.net.InetAddress;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.java7.Java7SmackInitializer;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.jxmpp.jid.parts.Localpart;

public class ChatRegistration {

	public static void chatRegistration(String userId,String password){
		try {
			// Create a connection to the jabber.org server on a specific port.
			/*XMPPTCPConnectionConfiguration config = XMPPTCPConnectionConfiguration.builder()
			  //.setUsernameAndPassword("username", "password")
			  .setXmppDomain("vaisansar")
			  .setHost("116.73.9.173")
			  .setPort(5222)
			  .build();*/
			new Java7SmackInitializer().initialize();
			HostnameVerifier verifier = new HostnameVerifier() {
				@Override
				public boolean verify(String arg0, SSLSession arg1) {
					// TODO Auto-generated method stub
					return false;
				}
            };
			XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
            configBuilder.setUsernameAndPassword("admin", "admin");
            configBuilder.setXmppDomain("vaisansar");
            configBuilder.setResource(".");
            configBuilder.setSecurityMode(SecurityMode.disabled);
            //configBuilder.setXmppDomain("jabber.org");
            InetAddress addr = InetAddress.getByName("116.73.9.173");
            configBuilder.setHostnameVerifier(verifier);
            configBuilder.setHostAddress(addr);
            
            XMPPTCPConnectionConfiguration configuration = configBuilder.build();

			AbstractXMPPConnection connection = new XMPPTCPConnection(configuration);
			connection.connect();
			
			Localpart userName = Localpart.from(userId);
			
			AccountManager accountManager = AccountManager.getInstance(connection);
            accountManager.sensitiveOperationOverInsecureConnection(true);
            accountManager.createAccount(userName,password);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Unable to Create Chat Id : "+e.toString());
		}
	}
	
	/*public static void main(String[] args) {
		chatRegistration("sujit", "sujit");
	}*/
	
}
