package com.bluesquare.rc.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateDiff {
	public static void main(String[] args) {
		try {
			SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
			Date dt1 = dateFormat.parse("2018-03-21");
			Date dt2 = dateFormat.parse("2018-03-28");
			System.out.println(findDifferenceInTwoDatesByDay(dt1, dt2));
			/*long diff = dt2.getTime() - dt1.getTime();
			long diffSeconds = diff / 1000;
			long diff = dt2.getTime() - dt1.getTime();
			long diffSeconds = diff / 1000 % 60;
			long diffMinutes = diff / (60 * 1000) % 60;
			long diffHours = diff / (60 * 60 * 1000);
			int diffInDays = (int) ((dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24));

			System.out.println(diffInDays);*/
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static boolean findDifferenceIsMoreThanTwoMinute(Date d1,Date d2)
	{
		long diff = d2.getTime() - d1.getTime();
		long diffSeconds = diff / 1000;
		if(diffSeconds>120)
		{
			return true;
		}
		return false;
	}
	
	public static long findDifferenceInTwoDatesByDay(Date d1,Date d2)
	{
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd");
		try {
			d1 = dateFormat.parse(dateFormat.format(d1));
			d2=dateFormat.parse(dateFormat.format(d2));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int diffInDays = (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
		return diffInDays;
	}
	
}
