package com.bluesquare.rc.utils;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class EncryptorDecryptor {
    public static String encrypt(String key, String initVector, String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            
            //If you don't want to depend on 3rd party Apache Commons Codec library you can use 
        	//JDK's javax.xml.bind.DatatypeConverter to perform Base64 encoding/decoding:
            return DatatypeConverter.printBase64Binary(encrypted);
            
            //3rd party : org.apache.commons.codec.binary.Base64;
            //return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String decrypt(String key, String initVector, String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            //3rd party : org.apache.commons.codec.binary.Base64;
            //byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));
            
            //If you don't want to depend on 3rd party Apache Commons Codec library you can use 
        	//JDK's javax.xml.bind.DatatypeConverter to perform Base64 encoding/decoding:
            byte[] original = cipher.doFinal(DatatypeConverter.parseBase64Binary(encrypted));
            
            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static void main(String[] args) {
        //Fix Key 
    	/*String key = "sac84259sac84259"; // 128 bit key
        String initVector = "bluesquare2018-4"; // 16 bytes IV
        String password="saAhin";
        
        
        String encryptedPassword=encrypt(key, initVector, password);
        System.out.println(encryptedPassword);
        
        String decryptedPassword=decrypt(key, initVector,encryptedPassword);
        System.out.println(decryptedPassword);*/
    	
      //dynamically key generated 
      /*try{ 
    	    byte[] plainBytes = "blue12354".getBytes();
    	  
            // Generate the key first
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128);  // Key size
            Key key = keyGen.generateKey();
            
            System.out.println(DatatypeConverter.printBase64Binary(key.getEncoded()));
            
           // Create Cipher instance and initialize it to encrytion mode
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");  // Transformation of the algorithm
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] cipherBytes = cipher.doFinal(plainBytes);
             
            // Reinitialize the Cipher to decryption mode
            cipher.init(Cipher.DECRYPT_MODE,key, cipher.getParameters());
            byte[] plainBytesDecrypted = cipher.doFinal(cipherBytes);
             
            System.out.println("DECRUPTED DATA : "+new String(plainBytesDecrypted));    
                          
        }catch(Exception ex){
            ex.printStackTrace();
        }*/
    	
    }
    
   
}