package com.bluesquare.rc.utils;

import java.util.Random;

public class OTPGenerator {
	public static String generateOtp() {
		
		String numbers = "0123456789";
		
		// Using random method
		Random rndm_method = new Random();

		char[] password = new char[4];

		for (int i = 0; i < 4; i++) {
			// Use of charAt() method : to get character value
			// Use of nextInt() as it is scanning the value as int
			password[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
		}

		return new String(password);

	}
}
