package com.bluesquare.rc.utils;

public class Constants {
	public static String SUCCESS_RESPONSE="Success";
	public static String FAILURE_RESPONSE="Failure";
	
	public static String RETURN_EMPTY_DATA="---------------------Returning Empty Data";
	public static String RETURN_DATA="---------------------Returning Data";
	
	public static String ORDER_RESPONSE="Order Succesfully Done";
	
	public static String ADMIN = "Admin";
	public static String COMPANY_ADMIN = "CompanyAdmin";
	public static String GATE_KEEPER_DEPT_NAME = "GateKeeper";
	public static String SALESMAN_DEPT_NAME = "SalesMan";
	public static String DELIVERYBOY_DEPT_NAME = "DeliveryBoy";
	public static String SUPPLIER_TYPE = "Supplier";
	
	public static String ALREADY_EXIST="Already  exist";
	public static String SAVE_SUCCESS="SuccessFully  Saved";
	public static String UPDATE_SUCCESS="SuccessFully  Updated";
	public static String NOT_FOUND="Not Found";
	public static String NO_CONTENT="Data Not Found";
	public static String LOGIN_FAILED="Invalid UserId or Password";
	public static String NONE="None";
	
	public static String ORDER_STATUS_BOOKED="Booked";
	//public static String ORDER_STATUS_DELIVERED_BOOKED="Delivered Booked";
	public static String ORDER_STATUS_PACKED="Packed";
	//public static String ORDER_STATUS_DELIVERED_PACKED="Delivered Packed";
	public static String ORDER_STATUS_ISSUED="Issued";
	//public static String ORDER_STATUS_DELIVERED_ISSUED="Delivered Issued";
	public static String ORDER_STATUS_DELIVERED="Delivered";
	public static String ORDER_STATUS_DELIVERED_PENDING="Delivered Pending";
	public static String ORDER_STATUS_CANCELED="Cancelled";
	
	public static String CASH_PAY_STATUS="Cash";
	public static String CHEQUE_PAY_STATUS="Cheque";
	
	public static String FULL_PAY_STATUS="Full Payment";
	public static String PARTIAL_PAY_STATUS="Partial Payment";	
	
	public static String PAY_STATUS_CURRENT_DATE_PAYMENT="Current";
	public static String PAY_STATUS_PENDING_DATE_PAYMENT="Pending";
	public static String PAY_STATUS_FUTURE_DATE_PAYMENT="Future";
	public static String PAY_STATUS_PAID_DATE_PAYMENT="Paid";
	
/*	public static String ISSUE_STATUS_ISSUED="Issued";
	public static String ISSUE_STATUS_REISSUED="ReIssued";*/
	
	public static String ORDER_PRODUCT_DETAIL_TYPE_FREE="Free";
	public static String ORDER_PRODUCT_DETAIL_TYPE_NON_FREE="NonFree";
	
	public static String RETURN_ORDER_PENDING="Pending";
	public static String RETURN_ORDER_COMPLETE="Completed";
	
	public static String INTRA_STATUS="Intra";
	public static String INTER_STATUS="Inter";
	
	public static String MAHARASHTRA_STATE_NAME="Maharashtra";
	
	public static String EDIT_MODE = "Edit";
    public static String NON_EDIT_MODE = "Non Edit";
    
    //Employee Chat status 
    public static String CHAT_ONLINE = "Online";
    public static String CHAT_OFFLINE = "Offline";
    public static String CHAT_TYPING = "Typing";
    
    public static String CHAT_TYPE_TEXT = "Text";
    public static String CHAT_TYPE_IMAGE = "Image";
    public static String CHAT_TYPE_PDF = "Pdf";
}
