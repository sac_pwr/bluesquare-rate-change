/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;

/**
 * @author aNKIT
 *
 */
public class SupplierOrderCartReply {

	private Supplier supplier;
	private long mobileNumber;
	private Product product;
	private double supplieRate;
	private long orderQuantity;
	private long productId;
	public SupplierOrderCartReply(Supplier supplier, long mobileNumber, Product product, double supplieRate,
			long orderQuantity, long productId) {
		super();
		this.supplier = supplier;
		this.mobileNumber = mobileNumber;
		this.product = product;
		this.supplieRate = supplieRate;
		this.orderQuantity = orderQuantity;
		this.productId = productId;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public long getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(long mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getSupplieRate() {
		return supplieRate;
	}
	public void setSupplieRate(double supplieRate) {
		this.supplieRate = supplieRate;
	}
	public long getOrderQuantity() {
		return orderQuantity;
	}
	public void setOrderQuantity(long orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	@Override
	public String toString() {
		return "SupplierOrderCartReply [supplier=" + supplier + ", mobileNumber=" + mobileNumber + ", product="
				+ product + ", supplieRate=" + supplieRate + ", orderQuantity=" + orderQuantity + ", productId="
				+ productId + "]";
	}


	
}
