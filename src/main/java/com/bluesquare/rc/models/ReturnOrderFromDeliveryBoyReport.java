package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.ReturnFromDeliveryBoyMain;

public class ReturnOrderFromDeliveryBoyReport {

	private long srno;
	private String smName;
	private ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain;
	public ReturnOrderFromDeliveryBoyReport(long srno, String smName,
			ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain) {
		super();
		this.srno = srno;
		this.smName = smName;
		this.returnFromDeliveryBoyMain = returnFromDeliveryBoyMain;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getSmName() {
		return smName;
	}
	public void setSmName(String smName) {
		this.smName = smName;
	}
	public ReturnFromDeliveryBoyMain getReturnFromDeliveryBoyMain() {
		return returnFromDeliveryBoyMain;
	}
	public void setReturnFromDeliveryBoyMain(ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain) {
		this.returnFromDeliveryBoyMain = returnFromDeliveryBoyMain;
	}
	@Override
	public String toString() {
		return "ReturnOrderFromDeliveryBoyReport [srno=" + srno + ", smName=" + smName + ", returnFromDeliveryBoyMain="
				+ returnFromDeliveryBoyMain + "]";
	}
}

