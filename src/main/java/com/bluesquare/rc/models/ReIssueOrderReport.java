/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.ReIssueOrderDetails;

/**
 * @author aNKIT
 *
 */
public class ReIssueOrderReport {
	private ReIssueOrderDetails reIssueOrderDetails;
	private EmployeeDetails employeeDetailsSM;
	private EmployeeDetails employeeDetailsDB;

	public ReIssueOrderReport(ReIssueOrderDetails reIssueOrderDetails, EmployeeDetails employeeDetailsSM,
			EmployeeDetails employeeDetailsDB) {
		super();
		this.reIssueOrderDetails = reIssueOrderDetails;
		this.employeeDetailsSM = employeeDetailsSM;
		this.employeeDetailsDB = employeeDetailsDB;
	}

	public ReIssueOrderDetails getReIssueOrderDetails() {
		return reIssueOrderDetails;
	}

	public void setReIssueOrderDetails(ReIssueOrderDetails reIssueOrderDetails) {
		this.reIssueOrderDetails = reIssueOrderDetails;
	}

	public EmployeeDetails getEmployeeDetailsSM() {
		return employeeDetailsSM;
	}

	public void setEmployeeDetailsSM(EmployeeDetails employeeDetailsSM) {
		this.employeeDetailsSM = employeeDetailsSM;
	}

	public EmployeeDetails getEmployeeDetailsDB() {
		return employeeDetailsDB;
	}

	public void setEmployeeDetailsDB(EmployeeDetails employeeDetailsDB) {
		this.employeeDetailsDB = employeeDetailsDB;
	}

	@Override
	public String toString() {
		return "ReIssueOrderReport [reIssueOrderDetails=" + reIssueOrderDetails + ", employeeDetailsSM="
				+ employeeDetailsSM + ", employeeDetailsDB=" + employeeDetailsDB + "]";
	}

}
