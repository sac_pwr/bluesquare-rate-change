package com.bluesquare.rc.models;
public class ProfitAndLossRequest {
	
	public String fromDate,endDate;

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		return "ProfitAndLossRequest [fromDate=" + fromDate + ", endDate=" + endDate + "]";
	} 
	
	

}
