package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.BusinessName;

/**
 * @author aNKIT
 *
 */
public class OrderDetailsListOfBusiness {

	private BusinessName businessName;
	private double  totalBusinessAmount;
	private double amountPaid;
	private double amountUnPaid;
	private List<OrderDetailsListOfBusinessSub> orderDetailsListOfBusinessSubList;
	public OrderDetailsListOfBusiness(BusinessName businessName, double totalBusinessAmount, double amountPaid,
			double amountUnPaid, List<OrderDetailsListOfBusinessSub> orderDetailsListOfBusinessSubList) {
		super();
		this.businessName = businessName;
		this.totalBusinessAmount = totalBusinessAmount;
		this.amountPaid = amountPaid;
		this.amountUnPaid = amountUnPaid;
		this.orderDetailsListOfBusinessSubList = orderDetailsListOfBusinessSubList;
	}
	public BusinessName getBusinessName() {
		return businessName;
	}
	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}
	public double getTotalBusinessAmount() {
		return totalBusinessAmount;
	}
	public void setTotalBusinessAmount(double totalBusinessAmount) {
		this.totalBusinessAmount = totalBusinessAmount;
	}
	public double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public double getAmountUnPaid() {
		return amountUnPaid;
	}
	public void setAmountUnPaid(double amountUnPaid) {
		this.amountUnPaid = amountUnPaid;
	}
	public List<OrderDetailsListOfBusinessSub> getOrderDetailsListOfBusinessSubList() {
		return orderDetailsListOfBusinessSubList;
	}
	public void setOrderDetailsListOfBusinessSubList(
			List<OrderDetailsListOfBusinessSub> orderDetailsListOfBusinessSubList) {
		this.orderDetailsListOfBusinessSubList = orderDetailsListOfBusinessSubList;
	}
	@Override
	public String toString() {
		return "OrderDetailsListOfBusiness [businessName=" + businessName + ", totalBusinessAmount="
				+ totalBusinessAmount + ", amountPaid=" + amountPaid + ", amountUnPaid=" + amountUnPaid
				+ ", orderDetailsListOfBusinessSubList=" + orderDetailsListOfBusinessSubList + "]";
	}
	
}
