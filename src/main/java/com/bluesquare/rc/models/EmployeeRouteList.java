package com.bluesquare.rc.models;

import java.util.Date;

public class EmployeeRouteList {
	private  String lat;
	private String lng;
	private String address;
	private Date datetime;
	private String info;
	
	public EmployeeRouteList(String lat, String lng, String address, Date datetime, String info) {
		super();
		this.lat = lat;
		this.lng = lng;
		this.address = address;
		this.datetime = datetime;
		this.info = info;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getDatetime() {
		return datetime;
	}
	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	@Override
	public String toString() {
		return "EmployeeRouteList [lat=" + lat + ", lng=" + lng + ", address=" + address + ", datetime=" + datetime
				+ ", info=" + info + "]";
	}
	
	
}
