/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.Product;

/**
 * @author aNKIT
 *
 */
public class ProductListForBill {

	private String srno;
	private String productName;
	private String hsnCode;
	private String taxSlab;
	private String quantityIssued;
	private String ratePerProduct;
	private String amountWithoutTax;
	public ProductListForBill(String srno, String productName, String hsnCode, String taxSlab, String quantityIssued,
			String ratePerProduct, String amountWithoutTax) {
		super();
		this.srno = srno;
		this.productName = productName;
		this.hsnCode = hsnCode;
		this.taxSlab = taxSlab;
		this.quantityIssued = quantityIssued;
		this.ratePerProduct = ratePerProduct;
		this.amountWithoutTax = amountWithoutTax;
	}
	public String getSrno() {
		return srno;
	}
	public void setSrno(String srno) {
		this.srno = srno;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getHsnCode() {
		return hsnCode;
	}
	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}
	public String getTaxSlab() {
		return taxSlab;
	}
	public void setTaxSlab(String taxSlab) {
		this.taxSlab = taxSlab;
	}
	public String getQuantityIssued() {
		return quantityIssued;
	}
	public void setQuantityIssued(String quantityIssued) {
		this.quantityIssued = quantityIssued;
	}
	public String getRatePerProduct() {
		return ratePerProduct;
	}
	public void setRatePerProduct(String ratePerProduct) {
		this.ratePerProduct = ratePerProduct;
	}
	public String getAmountWithoutTax() {
		return amountWithoutTax;
	}
	public void setAmountWithoutTax(String amountWithoutTax) {
		this.amountWithoutTax = amountWithoutTax;
	}
	@Override
	public String toString() {
		return "ProductListForBill [srno=" + srno + ", productName=" + productName + ", hsnCode=" + hsnCode
				+ ", taxSlab=" + taxSlab + ", quantityIssued=" + quantityIssued + ", ratePerProduct=" + ratePerProduct
				+ ", amountWithoutTax=" + amountWithoutTax + "]";
	}
	
	
}
