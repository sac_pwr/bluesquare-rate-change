package com.bluesquare.rc.models;

public class EmployeePaymentModel {

	private String name;
	private double totalsalary;
	private double amountPaid;
	private double amountPending;
	
	
	public EmployeePaymentModel(String name, double totalsalary, double amountPaid, double amountPending) {
		super();
		this.name = name;
		this.totalsalary = totalsalary;
		this.amountPaid = amountPaid;
		this.amountPending = amountPending;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getTotalsalary() {
		return totalsalary;
	}
	public void setTotalsalary(double totalsalary) {
		this.totalsalary = totalsalary;
	}
	public double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public double getAmountPending() {
		return amountPending;
	}
	public void setAmountPending(double amountPending) {
		this.amountPending = amountPending;
	}
	@Override
	public String toString() {
		return "EmployeePaymentModel [name=" + name + ", totalsalary=" + totalsalary + ", amountPaid=" + amountPaid
				+ ", amountPending=" + amountPending + "]";
	}
	
	
}
