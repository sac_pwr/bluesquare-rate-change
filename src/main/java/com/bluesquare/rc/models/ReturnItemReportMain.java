/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;

/**
 * @author aNKIT
 *
 */
public class ReturnItemReportMain {
	private ReturnOrderProduct returnOrderProductlist;
	private EmployeeDetails employeeDetails;
	private ReIssueOrderDetails reIssueOrderDetails;
	public ReturnItemReportMain(ReturnOrderProduct returnOrderProductlist, EmployeeDetails employeeDetails,
			ReIssueOrderDetails reIssueOrderDetails) {
		super();
		this.returnOrderProductlist = returnOrderProductlist;
		this.employeeDetails = employeeDetails;
		this.reIssueOrderDetails = reIssueOrderDetails;
	}
	public ReturnOrderProduct getReturnOrderProductlist() {
		return returnOrderProductlist;
	}
	public void setReturnOrderProductlist(ReturnOrderProduct returnOrderProductlist) {
		this.returnOrderProductlist = returnOrderProductlist;
	}
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public ReIssueOrderDetails getReIssueOrderDetails() {
		return reIssueOrderDetails;
	}
	public void setReIssueOrderDetails(ReIssueOrderDetails reIssueOrderDetails) {
		this.reIssueOrderDetails = reIssueOrderDetails;
	}
	@Override
	public String toString() {
		return "ReturnItemReportMain [returnOrderProductlist=" + returnOrderProductlist + ", employeeDetails="
				+ employeeDetails + ", reIssueOrderDetails=" + reIssueOrderDetails + "]";
	}


	
	
	
}
