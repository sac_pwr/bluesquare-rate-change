package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.BusinessName;

public class BusinessNameList {

	private BusinessName businessName;
	private double balanceAmount;
	public BusinessNameList(BusinessName businessName, double balanceAmount) {
		super();
		this.businessName = businessName;
		this.balanceAmount = balanceAmount;
	}
	public BusinessName getBusinessName() {
		return businessName;
	}
	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}
	public double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	@Override
	public String toString() {
		return "BusinessNameList [businessName=" + businessName + ", balanceAmount=" + balanceAmount + "]";
	}
	
	
}
