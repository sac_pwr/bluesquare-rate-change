package com.bluesquare.rc.models;

public class EmployeeViewModel {

	private long srno;
	private long employeePkId;
	private String employeeId;
	private String name;
	private String mobileNumber;
	private String email;
	private double totalAmount;
	private double paidAmount;
	private double unPaidAmount;
	private double incentives;
	private int noOfHolidays;
	private String departmentName;
	private boolean status;
	
	
	
	
	public EmployeeViewModel(long srno, long employeePkId, String employeeId, String name, String mobileNumber,
			String email, double totalAmount, double paidAmount, double unPaidAmount, double incentives,
			int noOfHolidays, String departmentName, boolean status) {
		super();
		this.srno = srno;
		this.employeePkId = employeePkId;
		this.employeeId = employeeId;
		this.name = name;
		this.mobileNumber = mobileNumber;
		this.email = email;
		this.totalAmount = totalAmount;
		this.paidAmount = paidAmount;
		this.unPaidAmount = unPaidAmount;
		this.incentives = incentives;
		this.noOfHolidays = noOfHolidays;
		this.departmentName = departmentName;
		this.status = status;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public long getEmployeePkId() {
		return employeePkId;
	}
	public void setEmployeePkId(long employeePkId) {
		this.employeePkId = employeePkId;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public double getUnPaidAmount() {
		return unPaidAmount;
	}
	public void setUnPaidAmount(double unPaidAmount) {
		this.unPaidAmount = unPaidAmount;
	}
	public double getIncentives() {
		return incentives;
	}
	public void setIncentives(double incentives) {
		this.incentives = incentives;
	}
	public int getNoOfHolidays() {
		return noOfHolidays;
	}
	public void setNoOfHolidays(int noOfHolidays) {
		this.noOfHolidays = noOfHolidays;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "EmployeeViewModel [srno=" + srno + ", employeePkId=" + employeePkId + ", employeeId=" + employeeId
				+ ", name=" + name + ", mobileNumber=" + mobileNumber + ", email=" + email + ", totalAmount="
				+ totalAmount + ", paidAmount=" + paidAmount + ", unPaidAmount=" + unPaidAmount + ", incentives="
				+ incentives + ", noOfHolidays=" + noOfHolidays + ", departmentName=" + departmentName + ", status="
				+ status + "]";
	}
	
	
	
	
	
}
