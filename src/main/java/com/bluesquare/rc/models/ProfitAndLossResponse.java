package com.bluesquare.rc.models;

import java.util.List;

public class ProfitAndLossResponse {

	public double totalSales;

	public double services;

	public double otherIncome;

	public double totalIncome;

	public List<ProfitAndLossEntity> profitAndLossEntities;

	public double totalExpenses;

	public double profitOrLoss;

	public double getTotalSales() {
		return totalSales;
	}

	public void setTotalSales(double totalSales) {
		this.totalSales = totalSales;
	}

	public double getServices() {
		return services;
	}

	public void setServices(double services) {
		this.services = services;
	}

	public double getOtherIncome() {
		return otherIncome;
	}

	public void setOtherIncome(double otherIncome) {
		this.otherIncome = otherIncome;
	}

	public double getTotalIncome() {
		return totalIncome;
	}

	public void setTotalIncome(double totalIncome) {
		this.totalIncome = totalIncome;
	}

	public List<ProfitAndLossEntity> getProfitAndLossEntities() {
		return profitAndLossEntities;
	}

	public void setProfitAndLossEntities(List<ProfitAndLossEntity> profitAndLossEntities) {
		this.profitAndLossEntities = profitAndLossEntities;
	}


	public double getTotalExpenses() {
		return totalExpenses;
	}

	public void setTotalExpenses(double totalExpenses) {
		this.totalExpenses = totalExpenses;
	}

	public double getProfitOrLoss() {
		return profitOrLoss;
	}

	public void setProfitOrLoss(double profitOrLoss) {
		this.profitOrLoss = profitOrLoss;
	}

	@Override
	public String toString() {
		return "ProfitAndLossResponse [totalSales=" + totalSales + ", services=" + services + ", otherIncome="
				+ otherIncome + ", totalIncome=" + totalIncome + ", profitAndLossEntities=" + profitAndLossEntities
				+ ", totalExpenses=" + totalExpenses + ", profitOrLoss=" + profitOrLoss + "]";
	}



}
