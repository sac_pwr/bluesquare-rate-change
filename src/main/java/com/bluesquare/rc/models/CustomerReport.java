/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.Date;

/**
 * @author aNKIT
 *
 */
public class CustomerReport {

	private long srno;
	private String businessNameId;
	private String  businessName;
	private double totalAmount;
	private String areaName;
	private String regionName;
	private String cityName;
	private String stateName;
	private String countryName;
	private Date dateAdded;
	private long noOfOrders;
	public CustomerReport(long srno, String businessNameId, String businessName, double totalAmount, String areaName,
			String regionName, String cityName, String stateName, String countryName, Date dateAdded, long noOfOrders) {
		super();
		this.srno = srno;
		this.businessNameId = businessNameId;
		this.businessName = businessName;
		this.totalAmount = totalAmount;
		this.areaName = areaName;
		this.regionName = regionName;
		this.cityName = cityName;
		this.stateName = stateName;
		this.countryName = countryName;
		this.dateAdded = dateAdded;
		this.noOfOrders = noOfOrders;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getBusinessNameId() {
		return businessNameId;
	}
	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public Date getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}
	public long getNoOfOrders() {
		return noOfOrders;
	}
	public void setNoOfOrders(long noOfOrders) {
		this.noOfOrders = noOfOrders;
	}
	@Override
	public String toString() {
		return "CustomerReport [srno=" + srno + ", businessNameId=" + businessNameId + ", businessName=" + businessName
				+ ", totalAmount=" + totalAmount + ", areaName=" + areaName + ", regionName=" + regionName
				+ ", cityName=" + cityName + ", stateName=" + stateName + ", countryName=" + countryName
				+ ", dateAdded=" + dateAdded + ", noOfOrders=" + noOfOrders + "]";
	}
	
	
	
	
}
