/**
 * 
 */
package com.bluesquare.rc.models;

/**
 * @author aNKIT
 *
 */
public class OrderDetailsListOfBusinessSub {

	private long srno;
	private String orderId;
	private String dateOfPayment;
	private String bankDetail;
	private String modeOfPayment;
	private double totalAmount;
	private double paidAmount;
	private double balanceAmount;
	private String statusOfPaid;
	public OrderDetailsListOfBusinessSub(long srno, String orderId, String dateOfPayment, String bankDetail,
			String modeOfPayment, double totalAmount, double paidAmount, double balanceAmount, String statusOfPaid) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.dateOfPayment = dateOfPayment;
		this.bankDetail = bankDetail;
		this.modeOfPayment = modeOfPayment;
		this.totalAmount = totalAmount;
		this.paidAmount = paidAmount;
		this.balanceAmount = balanceAmount;
		this.statusOfPaid = statusOfPaid;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getDateOfPayment() {
		return dateOfPayment;
	}
	public void setDateOfPayment(String dateOfPayment) {
		this.dateOfPayment = dateOfPayment;
	}
	public String getBankDetail() {
		return bankDetail;
	}
	public void setBankDetail(String bankDetail) {
		this.bankDetail = bankDetail;
	}
	public String getModeOfPayment() {
		return modeOfPayment;
	}
	public void setModeOfPayment(String modeOfPayment) {
		this.modeOfPayment = modeOfPayment;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public String getStatusOfPaid() {
		return statusOfPaid;
	}
	public void setStatusOfPaid(String statusOfPaid) {
		this.statusOfPaid = statusOfPaid;
	}
	@Override
	public String toString() {
		return "OrderDetailsListOfBusinessSub [srno=" + srno + ", orderId=" + orderId + ", dateOfPayment="
				+ dateOfPayment + ", bankDetail=" + bankDetail + ", modeOfPayment=" + modeOfPayment + ", totalAmount="
				+ totalAmount + ", paidAmount=" + paidAmount + ", balanceAmount=" + balanceAmount + ", statusOfPaid="
				+ statusOfPaid + "]";
	}
}
