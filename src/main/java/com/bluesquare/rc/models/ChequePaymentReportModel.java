package com.bluesquare.rc.models;

import java.util.Date;

public class ChequePaymentReportModel {

	private long srno;
	private String orderId;
	private String bankName;
	private String chequeNumber;
	private double amount;
	private Date chequeDate;
	private String customerBusinessName;
	private String mobileNumber;
	private boolean disableStatus;
	private boolean checkClearStatus;
	private long paymentId;
	private boolean findLast;
	private Date paidDate;
	public ChequePaymentReportModel(long srno, String orderId, String bankName, String chequeNumber, double amount,
			Date chequeDate, String customerBusinessName, String mobileNumber, boolean disableStatus,
			boolean checkClearStatus, long paymentId, boolean findLast, Date paidDate) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.bankName = bankName;
		this.chequeNumber = chequeNumber;
		this.amount = amount;
		this.chequeDate = chequeDate;
		this.customerBusinessName = customerBusinessName;
		this.mobileNumber = mobileNumber;
		this.disableStatus = disableStatus;
		this.checkClearStatus = checkClearStatus;
		this.paymentId = paymentId;
		this.findLast = findLast;
		this.paidDate = paidDate;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Date getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getCustomerBusinessName() {
		return customerBusinessName;
	}
	public void setCustomerBusinessName(String customerBusinessName) {
		this.customerBusinessName = customerBusinessName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public boolean isDisableStatus() {
		return disableStatus;
	}
	public void setDisableStatus(boolean disableStatus) {
		this.disableStatus = disableStatus;
	}
	public boolean isCheckClearStatus() {
		return checkClearStatus;
	}
	public void setCheckClearStatus(boolean checkClearStatus) {
		this.checkClearStatus = checkClearStatus;
	}
	public long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}
	public boolean isFindLast() {
		return findLast;
	}
	public void setFindLast(boolean findLast) {
		this.findLast = findLast;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	@Override
	public String toString() {
		return "ChequePaymentReportModel [srno=" + srno + ", orderId=" + orderId + ", bankName=" + bankName
				+ ", chequeNumber=" + chequeNumber + ", amount=" + amount + ", chequeDate=" + chequeDate
				+ ", customerBusinessName=" + customerBusinessName + ", mobileNumber=" + mobileNumber
				+ ", disableStatus=" + disableStatus + ", checkClearStatus=" + checkClearStatus + ", paymentId="
				+ paymentId + ", findLast=" + findLast + ", paidDate=" + paidDate + "]";
	}
	
	
	
}
