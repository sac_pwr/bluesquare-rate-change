package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Supplier;

public class CategoryAndBrandList {

	public List<Categories> categoryList;
	public List<Brand> brandList;
	public List<Supplier> supplierList;
	public CategoryAndBrandList(List<Categories> categoryList, List<Brand> brandList, List<Supplier> supplierList) {
		super();
		this.categoryList = categoryList;
		this.brandList = brandList;
		this.supplierList = supplierList;
	}
	public List<Categories> getCategoryList() {
		return categoryList;
	}
	public void setCategoryList(List<Categories> categoryList) {
		this.categoryList = categoryList;
	}
	public List<Brand> getBrandList() {
		return brandList;
	}
	public void setBrandList(List<Brand> brandList) {
		this.brandList = brandList;
	}
	public List<Supplier> getSupplierList() {
		return supplierList;
	}
	public void setSupplierList(List<Supplier> supplierList) {
		this.supplierList = supplierList;
	}
	@Override
	public String toString() {
		return "CategoryAndBrandList [categoryList=" + categoryList + ", brandList=" + brandList + ", supplierList="
				+ supplierList + "]";
	}
	


	
	
}
