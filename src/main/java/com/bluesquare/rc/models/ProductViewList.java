package com.bluesquare.rc.models;

import java.util.Date;

public class ProductViewList {

	public long srno;
	public long productId;
	public String productCode;
	public String productName;
	public String categoryName;
	public String brandName;
	public String hSNCode;
	public long thresholdValue;
	public double productRate;
	public double productRateWithTax;
	public long currentQuantity;
	public double taxableTotal;
	public float cgstPercentage;
	public double cgstamount;
	public float igstPercentage;
	public double igstamount;
	public float sgstPercentage;
	public double sgstamount;
	public double total;
	private Date productAddedDatetime;
	private Date productQuantityUpdatedDatetime;
	public ProductViewList(long srno, long productId, String productCode, String productName, String categoryName,
			String brandName, String hSNCode, long thresholdValue, double productRate, double productRateWithTax,
			long currentQuantity, double taxableTotal, float cgstPercentage, double cgstamount, float igstPercentage,
			double igstamount, float sgstPercentage, double sgstamount, double total, Date productAddedDatetime,
			Date productQuantityUpdatedDatetime) {
		super();
		this.srno = srno;
		this.productId = productId;
		this.productCode = productCode;
		this.productName = productName;
		this.categoryName = categoryName;
		this.brandName = brandName;
		this.hSNCode = hSNCode;
		this.thresholdValue = thresholdValue;
		this.productRate = productRate;
		this.productRateWithTax = productRateWithTax;
		this.currentQuantity = currentQuantity;
		this.taxableTotal = taxableTotal;
		this.cgstPercentage = cgstPercentage;
		this.cgstamount = cgstamount;
		this.igstPercentage = igstPercentage;
		this.igstamount = igstamount;
		this.sgstPercentage = sgstPercentage;
		this.sgstamount = sgstamount;
		this.total = total;
		this.productAddedDatetime = productAddedDatetime;
		this.productQuantityUpdatedDatetime = productQuantityUpdatedDatetime;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String gethSNCode() {
		return hSNCode;
	}
	public void sethSNCode(String hSNCode) {
		this.hSNCode = hSNCode;
	}
	public long getThresholdValue() {
		return thresholdValue;
	}
	public void setThresholdValue(long thresholdValue) {
		this.thresholdValue = thresholdValue;
	}
	public double getProductRate() {
		return productRate;
	}
	public void setProductRate(double productRate) {
		this.productRate = productRate;
	}
	public double getProductRateWithTax() {
		return productRateWithTax;
	}
	public void setProductRateWithTax(double productRateWithTax) {
		this.productRateWithTax = productRateWithTax;
	}
	public long getCurrentQuantity() {
		return currentQuantity;
	}
	public void setCurrentQuantity(long currentQuantity) {
		this.currentQuantity = currentQuantity;
	}
	public double getTaxableTotal() {
		return taxableTotal;
	}
	public void setTaxableTotal(double taxableTotal) {
		this.taxableTotal = taxableTotal;
	}
	public float getCgstPercentage() {
		return cgstPercentage;
	}
	public void setCgstPercentage(float cgstPercentage) {
		this.cgstPercentage = cgstPercentage;
	}
	public double getCgstamount() {
		return cgstamount;
	}
	public void setCgstamount(double cgstamount) {
		this.cgstamount = cgstamount;
	}
	public float getIgstPercentage() {
		return igstPercentage;
	}
	public void setIgstPercentage(float igstPercentage) {
		this.igstPercentage = igstPercentage;
	}
	public double getIgstamount() {
		return igstamount;
	}
	public void setIgstamount(double igstamount) {
		this.igstamount = igstamount;
	}
	public float getSgstPercentage() {
		return sgstPercentage;
	}
	public void setSgstPercentage(float sgstPercentage) {
		this.sgstPercentage = sgstPercentage;
	}
	public double getSgstamount() {
		return sgstamount;
	}
	public void setSgstamount(double sgstamount) {
		this.sgstamount = sgstamount;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Date getProductAddedDatetime() {
		return productAddedDatetime;
	}
	public void setProductAddedDatetime(Date productAddedDatetime) {
		this.productAddedDatetime = productAddedDatetime;
	}
	public Date getProductQuantityUpdatedDatetime() {
		return productQuantityUpdatedDatetime;
	}
	public void setProductQuantityUpdatedDatetime(Date productQuantityUpdatedDatetime) {
		this.productQuantityUpdatedDatetime = productQuantityUpdatedDatetime;
	}
	@Override
	public String toString() {
		return "ProductViewList [srno=" + srno + ", productId=" + productId + ", productCode=" + productCode
				+ ", productName=" + productName + ", categoryName=" + categoryName + ", brandName=" + brandName
				+ ", hSNCode=" + hSNCode + ", thresholdValue=" + thresholdValue + ", productRate=" + productRate
				+ ", productRateWithTax=" + productRateWithTax + ", currentQuantity=" + currentQuantity
				+ ", taxableTotal=" + taxableTotal + ", cgstPercentage=" + cgstPercentage + ", cgstamount=" + cgstamount
				+ ", igstPercentage=" + igstPercentage + ", igstamount=" + igstamount + ", sgstPercentage="
				+ sgstPercentage + ", sgstamount=" + sgstamount + ", total=" + total + ", productAddedDatetime="
				+ productAddedDatetime + ", productQuantityUpdatedDatetime=" + productQuantityUpdatedDatetime + "]";
	}
}
