package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;

public class EmployeeLastLocation {

	private EmployeeDetails employeeDetails;
	private String lat;
	private String lng;
	private String address;
	private List<EmployeeRouteListModel> employeeRouteListModelList;
	public EmployeeLastLocation(EmployeeDetails employeeDetails, String lat, String lng, String address,
			List<EmployeeRouteListModel> employeeRouteListModelList) {
		super();
		this.employeeDetails = employeeDetails;
		this.lat = lat;
		this.lng = lng;
		this.address = address;
		this.employeeRouteListModelList = employeeRouteListModelList;
	}
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List<EmployeeRouteListModel> getEmployeeRouteListModelList() {
		return employeeRouteListModelList;
	}
	public void setEmployeeRouteListModelList(List<EmployeeRouteListModel> employeeRouteListModelList) {
		this.employeeRouteListModelList = employeeRouteListModelList;
	}
	@Override
	public String toString() {
		return "EmployeeLastLocation [employeeDetails=" + employeeDetails + ", lat=" + lat + ", lng=" + lng
				+ ", address=" + address + ", employeeRouteListModelList=" + employeeRouteListModelList + "]";
	}
	
	


}