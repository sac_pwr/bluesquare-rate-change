package com.bluesquare.rc.models;

import java.util.Date;

public class PaymentReportModel {

	private long srno;
	private long paymentId;
	private String orderId;
	private String customerShopName;
	private double amountPaid;
	private String paymentMode;
	private Date paidDate;
	private String payType;
	private boolean findLast;
	private String employeeName;
	public PaymentReportModel(long srno, long paymentId, String orderId, String customerShopName, double amountPaid,
			String paymentMode, Date paidDate, String payType, boolean findLast, String employeeName) {
		super();
		this.srno = srno;
		this.paymentId = paymentId;
		this.orderId = orderId;
		this.customerShopName = customerShopName;
		this.amountPaid = amountPaid;
		this.paymentMode = paymentMode;
		this.paidDate = paidDate;
		this.payType = payType;
		this.findLast = findLast;
		this.employeeName = employeeName;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public long getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getCustomerShopName() {
		return customerShopName;
	}
	public void setCustomerShopName(String customerShopName) {
		this.customerShopName = customerShopName;
	}
	public double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public boolean isFindLast() {
		return findLast;
	}
	public void setFindLast(boolean findLast) {
		this.findLast = findLast;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	@Override
	public String toString() {
		return "PaymentReportModel [srno=" + srno + ", paymentId=" + paymentId + ", orderId=" + orderId
				+ ", customerShopName=" + customerShopName + ", amountPaid=" + amountPaid + ", paymentMode="
				+ paymentMode + ", paidDate=" + paidDate + ", payType=" + payType + ", findLast=" + findLast
				+ ", employeeName=" + employeeName + "]";
	}
	
}
