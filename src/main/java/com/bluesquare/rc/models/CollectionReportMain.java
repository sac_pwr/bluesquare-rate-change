package com.bluesquare.rc.models;

import java.util.List;

public class CollectionReportMain {

	private double amountToBePaid;
	private double pendingAmount;
	private double fullAmountCollected;
	private double partialAmountCollected;
	private List<CollectionReportPaymentDetails> collectionReportPaymentDetaillist;

	public CollectionReportMain(double amountToBePaid, double pendingAmount, double fullAmountCollected,
			double partialAmountCollected, List<CollectionReportPaymentDetails> collectionReportPaymentDetaillist) {
		super();
		this.amountToBePaid = amountToBePaid;
		this.pendingAmount = pendingAmount;
		this.fullAmountCollected = fullAmountCollected;
		this.partialAmountCollected = partialAmountCollected;
		this.collectionReportPaymentDetaillist = collectionReportPaymentDetaillist;
	}

	public double getAmountToBePaid() {
		return amountToBePaid;
	}

	public void setAmountToBePaid(double amountToBePaid) {
		this.amountToBePaid = amountToBePaid;
	}

	public double getPendingAmount() {
		return pendingAmount;
	}

	public void setPendingAmount(double pendingAmount) {
		this.pendingAmount = pendingAmount;
	}

	public double getFullAmountCollected() {
		return fullAmountCollected;
	}

	public void setFullAmountCollected(double fullAmountCollected) {
		this.fullAmountCollected = fullAmountCollected;
	}

	public double getPartialAmountCollected() {
		return partialAmountCollected;
	}

	public void setPartialAmountCollected(double partialAmountCollected) {
		this.partialAmountCollected = partialAmountCollected;
	}

	public List<CollectionReportPaymentDetails> getCollectionReportPaymentDetaillist() {
		return collectionReportPaymentDetaillist;
	}

	public void setCollectionReportPaymentDetaillist(
			List<CollectionReportPaymentDetails> collectionReportPaymentDetaillist) {
		this.collectionReportPaymentDetaillist = collectionReportPaymentDetaillist;
	}

	@Override
	public String toString() {
		return "CollectionReportMain [amountToBePaid=" + amountToBePaid + ", pendingAmount=" + pendingAmount
				+ ", fullAmountCollected=" + fullAmountCollected + ", partialAmountCollected=" + partialAmountCollected
				+ ", collectionReportPaymentDetaillist=" + collectionReportPaymentDetaillist + "]";
	}

}
