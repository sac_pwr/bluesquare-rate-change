/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.ReIssueOrderDetails;

/**
 * @author aNKIT
 *
 */
public class ReIssueOrderDetailsReport {

	private ReIssueOrderDetails reIssueOrderDetails;
	private EmployeeDetails employeeDetails;
	public ReIssueOrderDetails getReIssueOrderDetails() {
		return reIssueOrderDetails;
	}
	public void setReIssueOrderDetails(ReIssueOrderDetails reIssueOrderDetails) {
		this.reIssueOrderDetails = reIssueOrderDetails;
	}
	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}
	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}
	public ReIssueOrderDetailsReport(ReIssueOrderDetails reIssueOrderDetails, EmployeeDetails employeeDetails) {
		super();
		this.reIssueOrderDetails = reIssueOrderDetails;
		this.employeeDetails = employeeDetails;
	}
	@Override
	public String toString() {
		return "ReIssueOrderDetailsReport [reIssueOrderDetails=" + reIssueOrderDetails + ", employeeDetails="
				+ employeeDetails + "]";
	} 

	
	
}
