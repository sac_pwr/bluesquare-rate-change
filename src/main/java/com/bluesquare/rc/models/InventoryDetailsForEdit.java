/**
 * 
 */
package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.OrderUsedProduct;

/**
 * @author aNKIT
 *
 */
public class InventoryDetailsForEdit {
	private double totalAmountWithoutTax;
	private double totalAmountWithTax;
	private OrderUsedProduct orderUsedProduct;
	private double rate;
	private long quantity;
	public InventoryDetailsForEdit(double totalAmountWithoutTax, double totalAmountWithTax,
			OrderUsedProduct orderUsedProduct, double rate, long quantity) {
		super();
		this.totalAmountWithoutTax = totalAmountWithoutTax;
		this.totalAmountWithTax = totalAmountWithTax;
		this.orderUsedProduct = orderUsedProduct;
		this.rate = rate;
		this.quantity = quantity;
	}
	public double getTotalAmountWithoutTax() {
		return totalAmountWithoutTax;
	}
	public void setTotalAmountWithoutTax(double totalAmountWithoutTax) {
		this.totalAmountWithoutTax = totalAmountWithoutTax;
	}
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}
	public OrderUsedProduct getOrderUsedProduct() {
		return orderUsedProduct;
	}
	public void setOrderUsedProduct(OrderUsedProduct orderUsedProduct) {
		this.orderUsedProduct = orderUsedProduct;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "InventoryDetailsForEdit [totalAmountWithoutTax=" + totalAmountWithoutTax + ", totalAmountWithTax="
				+ totalAmountWithTax + ", orderUsedProduct=" + orderUsedProduct + ", rate=" + rate + ", quantity="
				+ quantity + "]";
	}
	
	
	
}
