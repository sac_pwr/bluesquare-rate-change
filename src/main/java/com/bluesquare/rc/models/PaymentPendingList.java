package com.bluesquare.rc.models;

import java.util.Comparator;

public class PaymentPendingList{  

	private long srno;
	private long Id;
	private String genId;
	private String name;
	private double totalAmount;
	private double amountRemaining;
	private double amountPaid;
	private String type;
	
	public PaymentPendingList() {
		// TODO Auto-generated constructor stub
	}

	public PaymentPendingList(long srno, long id, String genId, String name, double totalAmount, double amountRemaining,
			double amountPaid, String type) {
		super();
		this.srno = srno;
		Id = id;
		this.genId = genId;
		this.name = name;
		this.totalAmount = totalAmount;
		this.amountRemaining = amountRemaining;
		this.amountPaid = amountPaid;
		this.type = type;
	}

	public long getSrno() {
		return srno;
	}

	public void setSrno(long srno) {
		this.srno = srno;
	}

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getGenId() {
		return genId;
	}

	public void setGenId(String genId) {
		this.genId = genId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getAmountRemaining() {
		return amountRemaining;
	}

	public void setAmountRemaining(double amountRemaining) {
		this.amountRemaining = amountRemaining;
	}

	public double getAmountPaid() {
		return amountPaid;
	}

	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "PaymentPendingList [srno=" + srno + ", Id=" + Id + ", genId=" + genId + ", name=" + name
				+ ", totalAmount=" + totalAmount + ", amountRemaining=" + amountRemaining + ", amountPaid=" + amountPaid
				+ ", type=" + type + "]";
	}

	
	
}
