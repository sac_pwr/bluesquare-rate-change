package com.bluesquare.rc.models;

import com.bluesquare.rc.entities.Product;

public class InventoryProduct {

	private long srno;
	private Product product;
	private double rateWithTax;
	private double taxableAmount;
	private double tax;
	private double taxwithAmount;
	
	public InventoryProduct(long srno, Product product, double rateWithTax, double taxableAmount, double tax,
			double taxwithAmount) {
		super();
		this.srno = srno;
		this.product = product;
		this.rateWithTax = rateWithTax;
		this.taxableAmount = taxableAmount;
		this.tax = tax;
		this.taxwithAmount = taxwithAmount;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getRateWithTax() {
		return rateWithTax;
	}
	public void setRateWithTax(double rateWithTax) {
		this.rateWithTax = rateWithTax;
	}
	public double getTaxableAmount() {
		return taxableAmount;
	}
	public void setTaxableAmount(double taxableAmount) {
		this.taxableAmount = taxableAmount;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public double getTaxwithAmount() {
		return taxwithAmount;
	}
	public void setTaxwithAmount(double taxwithAmount) {
		this.taxwithAmount = taxwithAmount;
	}
	@Override
	public String toString() {
		return "InventoryProduct [srno=" + srno + ", product=" + product + ", rateWithTax=" + rateWithTax
				+ ", taxableAmount=" + taxableAmount + ", tax=" + tax + ", taxwithAmount=" + taxwithAmount + "]";
	}
	
}
