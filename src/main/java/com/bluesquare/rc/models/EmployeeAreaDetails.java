package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeAreaList;

public class EmployeeAreaDetails {

	private String name;
	private String departmentName;
	private String mobileNumber;
	private List<EmployeeAreaList> areaList;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public List<EmployeeAreaList> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<EmployeeAreaList> areaList) {
		this.areaList = areaList;
	}
	
	@Override
	public String toString() {
		return "EmployeeAreaDetails [name=" + name + ", departmentName=" + departmentName + ", mobileNumber="
				+ mobileNumber + ", areaList=" + areaList + "]";
	}
	
	
	
	
}
