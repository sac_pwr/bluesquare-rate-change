package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.entities.OrderDetails;

public class PaymentListReport {
	public long paymentId;

	public OrderDetails orderDetails;
	
	public String employeeName;

	public double totalAmount;
	
	public double dueAmount;

	public Date dueDate;
	
	public Date lastDueDate;

	public double paidAmount;

	public Date paidDate;

	public String payType;

	public String chequeNumber;

	public String bankName;

	public Date chequeDate;
	
	public boolean status;
	
	public boolean chequeClearStatus;

	public PaymentListReport(long paymentId, OrderDetails orderDetails, String employeeName, double totalAmount,
			double dueAmount, Date dueDate, Date lastDueDate, double paidAmount, Date paidDate, String payType,
			String chequeNumber, String bankName, Date chequeDate, boolean status, boolean chequeClearStatus) {
		super();
		this.paymentId = paymentId;
		this.orderDetails = orderDetails;
		this.employeeName = employeeName;
		this.totalAmount = totalAmount;
		this.dueAmount = dueAmount;
		this.dueDate = dueDate;
		this.lastDueDate = lastDueDate;
		this.paidAmount = paidAmount;
		this.paidDate = paidDate;
		this.payType = payType;
		this.chequeNumber = chequeNumber;
		this.bankName = bankName;
		this.chequeDate = chequeDate;
		this.status = status;
		this.chequeClearStatus = chequeClearStatus;
	}

	@Override
	public String toString() {
		return "PaymentListReport [paymentId=" + paymentId + ", orderDetails=" + orderDetails + ", employeeName="
				+ employeeName + ", totalAmount=" + totalAmount + ", dueAmount=" + dueAmount + ", dueDate=" + dueDate
				+ ", lastDueDate=" + lastDueDate + ", paidAmount=" + paidAmount + ", paidDate=" + paidDate
				+ ", payType=" + payType + ", chequeNumber=" + chequeNumber + ", bankName=" + bankName + ", chequeDate="
				+ chequeDate + ", status=" + status + ", chequeClearStatus=" + chequeClearStatus + "]";
	}

	public long getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(long paymentId) {
		this.paymentId = paymentId;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getDueAmount() {
		return dueAmount;
	}

	public void setDueAmount(double dueAmount) {
		this.dueAmount = dueAmount;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getLastDueDate() {
		return lastDueDate;
	}

	public void setLastDueDate(Date lastDueDate) {
		this.lastDueDate = lastDueDate;
	}

	public double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isChequeClearStatus() {
		return chequeClearStatus;
	}

	public void setChequeClearStatus(boolean chequeClearStatus) {
		this.chequeClearStatus = chequeClearStatus;
	}
	
	
	
}
