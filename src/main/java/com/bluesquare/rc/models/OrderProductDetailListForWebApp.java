package com.bluesquare.rc.models;

import java.util.Date;

import com.bluesquare.rc.entities.OrderUsedProduct;


public class OrderProductDetailListForWebApp {

	private long srno;
	private String orderId;
	private OrderUsedProduct product;
	private String salesPersonName;
	private long employeeId;
	private String employeeGenId;
	private double rateWithTax;
	private long quantity;
	private double totalAmount;
	private double totalAmountWithTax;
	private Date orderDate;
	private String type;
	public OrderProductDetailListForWebApp(long srno, String orderId, OrderUsedProduct product, String salesPersonName,
			long employeeId, String employeeGenId, double rateWithTax, long quantity, double totalAmount,
			double totalAmountWithTax, Date orderDate, String type) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.product = product;
		this.salesPersonName = salesPersonName;
		this.employeeId = employeeId;
		this.employeeGenId = employeeGenId;
		this.rateWithTax = rateWithTax;
		this.quantity = quantity;
		this.totalAmount = totalAmount;
		this.totalAmountWithTax = totalAmountWithTax;
		this.orderDate = orderDate;
		this.type = type;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public OrderUsedProduct getProduct() {
		return product;
	}
	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}
	public String getSalesPersonName() {
		return salesPersonName;
	}
	public void setSalesPersonName(String salesPersonName) {
		this.salesPersonName = salesPersonName;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeGenId() {
		return employeeGenId;
	}
	public void setEmployeeGenId(String employeeGenId) {
		this.employeeGenId = employeeGenId;
	}
	public double getRateWithTax() {
		return rateWithTax;
	}
	public void setRateWithTax(double rateWithTax) {
		this.rateWithTax = rateWithTax;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "OrderProductDetailListForWebApp [srno=" + srno + ", orderId=" + orderId + ", product=" + product
				+ ", salesPersonName=" + salesPersonName + ", employeeId=" + employeeId + ", employeeGenId="
				+ employeeGenId + ", rateWithTax=" + rateWithTax + ", quantity=" + quantity + ", totalAmount="
				+ totalAmount + ", totalAmountWithTax=" + totalAmountWithTax + ", orderDate=" + orderDate + ", type="
				+ type + "]";
	}


}
