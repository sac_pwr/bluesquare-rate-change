/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.entities.Employee;

/**
 * @author aNKIT
 *
 */
public class BillPrintDataModel {

	private String orderNumber;
	private String invoiceNumber;
	private String orderDate;
	private List<String> addressLineList;
	
	private BusinessName businessName;
	private Employee employeeGKCounterOrder;
	private String customerName;
	private String customerMobileNumber;
	private String customerGstNumber;
	
	private String deliveryDate;
	
	private List<ProductListForBill> productListForBill;
	
	private String cGSTAmount;
	private String iGSTAmount;
	private String sGSTAmount;
	
	private String roundOffAmount;
	
	private String totalAmountWithoutTax;
	
	private String totalQuantity;
	private String totalAmountWithTax;
	private String totalAmountWithTaxInWord;
	
	private List<CategoryWiseAmountForBill> categoryWiseAmountForBills;
	
	private String totalAmount;
	private String taxAmountInWord;
	
	private String totalCGSTAmount;
	private String totalIGSTAmount;
	private String totalSGSTAmount;
	
	//Order by BusinessName
	public BillPrintDataModel(String orderNumber, String invoiceNumber, String orderDate, List<String> addressLineList,
			BusinessName businessName,Employee employeeGKCounterOrder, String deliveryDate, List<ProductListForBill> productListForBill,
			String cGSTAmount, String iGSTAmount, String sGSTAmount, String roundOffAmount,
			String totalAmountWithoutTax, String totalQuantity, String totalAmountWithTax,
			String totalAmountWithTaxInWord, List<CategoryWiseAmountForBill> categoryWiseAmountForBills,
			String totalAmount, String taxAmountInWord, String totalCGSTAmount, String totalIGSTAmount,
			String totalSGSTAmount) {
		super();
		this.orderNumber = orderNumber;
		this.invoiceNumber = invoiceNumber;
		this.orderDate = orderDate;
		this.addressLineList = addressLineList;
		this.businessName = businessName;
		this.employeeGKCounterOrder=employeeGKCounterOrder;
		this.deliveryDate = deliveryDate;
		this.productListForBill = productListForBill;
		this.cGSTAmount = cGSTAmount;
		this.iGSTAmount = iGSTAmount;
		this.sGSTAmount = sGSTAmount;
		this.roundOffAmount = roundOffAmount;
		this.totalAmountWithoutTax = totalAmountWithoutTax;
		this.totalQuantity = totalQuantity;
		this.totalAmountWithTax = totalAmountWithTax;
		this.totalAmountWithTaxInWord = totalAmountWithTaxInWord;
		this.categoryWiseAmountForBills = categoryWiseAmountForBills;
		this.totalAmount = totalAmount;
		this.taxAmountInWord = taxAmountInWord;
		this.totalCGSTAmount = totalCGSTAmount;
		this.totalIGSTAmount = totalIGSTAmount;
		this.totalSGSTAmount = totalSGSTAmount;
	}

	//order by counter customer
	public BillPrintDataModel(String orderNumber, String invoiceNumber, String orderDate, List<String> addressLineList,
			String customerName, String customerMobileNumber, String customerGstNumber,Employee employeeGKCounterOrder, String deliveryDate,
			List<ProductListForBill> productListForBill, String cGSTAmount, String iGSTAmount, String sGSTAmount,
			String roundOffAmount, String totalAmountWithoutTax, String totalQuantity, String totalAmountWithTax,
			String totalAmountWithTaxInWord, List<CategoryWiseAmountForBill> categoryWiseAmountForBills,
			String totalAmount, String taxAmountInWord, String totalCGSTAmount, String totalIGSTAmount,
			String totalSGSTAmount) {
		super();
		this.orderNumber = orderNumber;
		this.invoiceNumber = invoiceNumber;
		this.orderDate = orderDate;
		this.addressLineList = addressLineList;
		this.customerName = customerName;
		this.customerMobileNumber = customerMobileNumber;
		this.customerGstNumber = customerGstNumber;
		this.employeeGKCounterOrder=employeeGKCounterOrder;
		this.deliveryDate = deliveryDate;
		this.productListForBill = productListForBill;
		this.cGSTAmount = cGSTAmount;
		this.iGSTAmount = iGSTAmount;
		this.sGSTAmount = sGSTAmount;
		this.roundOffAmount = roundOffAmount;
		this.totalAmountWithoutTax = totalAmountWithoutTax;
		this.totalQuantity = totalQuantity;
		this.totalAmountWithTax = totalAmountWithTax;
		this.totalAmountWithTaxInWord = totalAmountWithTaxInWord;
		this.categoryWiseAmountForBills = categoryWiseAmountForBills;
		this.totalAmount = totalAmount;
		this.taxAmountInWord = taxAmountInWord;
		this.totalCGSTAmount = totalCGSTAmount;
		this.totalIGSTAmount = totalIGSTAmount;
		this.totalSGSTAmount = totalSGSTAmount;
	}


	public String getOrderNumber() {
		return orderNumber;
	}


	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}


	public String getInvoiceNumber() {
		return invoiceNumber;
	}


	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}


	public String getOrderDate() {
		return orderDate;
	}


	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}


	public List<String> getAddressLineList() {
		return addressLineList;
	}


	public void setAddressLineList(List<String> addressLineList) {
		this.addressLineList = addressLineList;
	}


	public BusinessName getBusinessName() {
		return businessName;
	}


	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}


	public String getCustomerName() {
		return customerName;
	}


	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}


	public String getCustomerMobileNumber() {
		return customerMobileNumber;
	}


	public void setCustomerMobileNumber(String customerMobileNumber) {
		this.customerMobileNumber = customerMobileNumber;
	}


	public String getCustomerGstNumber() {
		return customerGstNumber;
	}


	public void setCustomerGstNumber(String customerGstNumber) {
		this.customerGstNumber = customerGstNumber;
	}


	public String getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public List<ProductListForBill> getProductListForBill() {
		return productListForBill;
	}


	public void setProductListForBill(List<ProductListForBill> productListForBill) {
		this.productListForBill = productListForBill;
	}


	public String getcGSTAmount() {
		return cGSTAmount;
	}


	public void setcGSTAmount(String cGSTAmount) {
		this.cGSTAmount = cGSTAmount;
	}


	public String getiGSTAmount() {
		return iGSTAmount;
	}


	public void setiGSTAmount(String iGSTAmount) {
		this.iGSTAmount = iGSTAmount;
	}


	public String getsGSTAmount() {
		return sGSTAmount;
	}


	public void setsGSTAmount(String sGSTAmount) {
		this.sGSTAmount = sGSTAmount;
	}


	public String getRoundOffAmount() {
		return roundOffAmount;
	}


	public void setRoundOffAmount(String roundOffAmount) {
		this.roundOffAmount = roundOffAmount;
	}


	public String getTotalAmountWithoutTax() {
		return totalAmountWithoutTax;
	}


	public void setTotalAmountWithoutTax(String totalAmountWithoutTax) {
		this.totalAmountWithoutTax = totalAmountWithoutTax;
	}


	public String getTotalQuantity() {
		return totalQuantity;
	}


	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}


	public String getTotalAmountWithTax() {
		return totalAmountWithTax;
	}


	public void setTotalAmountWithTax(String totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}


	public String getTotalAmountWithTaxInWord() {
		return totalAmountWithTaxInWord;
	}


	public void setTotalAmountWithTaxInWord(String totalAmountWithTaxInWord) {
		this.totalAmountWithTaxInWord = totalAmountWithTaxInWord;
	}


	public List<CategoryWiseAmountForBill> getCategoryWiseAmountForBills() {
		return categoryWiseAmountForBills;
	}


	public void setCategoryWiseAmountForBills(List<CategoryWiseAmountForBill> categoryWiseAmountForBills) {
		this.categoryWiseAmountForBills = categoryWiseAmountForBills;
	}


	public String getTotalAmount() {
		return totalAmount;
	}


	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}


	public String getTaxAmountInWord() {
		return taxAmountInWord;
	}


	public void setTaxAmountInWord(String taxAmountInWord) {
		this.taxAmountInWord = taxAmountInWord;
	}


	public String getTotalCGSTAmount() {
		return totalCGSTAmount;
	}


	public void setTotalCGSTAmount(String totalCGSTAmount) {
		this.totalCGSTAmount = totalCGSTAmount;
	}


	public String getTotalIGSTAmount() {
		return totalIGSTAmount;
	}


	public void setTotalIGSTAmount(String totalIGSTAmount) {
		this.totalIGSTAmount = totalIGSTAmount;
	}


	public String getTotalSGSTAmount() {
		return totalSGSTAmount;
	}


	public void setTotalSGSTAmount(String totalSGSTAmount) {
		this.totalSGSTAmount = totalSGSTAmount;
	}

	public Employee getEmployeeGKCounterOrder() {
		return employeeGKCounterOrder;
	}

	public void setEmployeeGKCounterOrder(Employee employeeGKCounterOrder) {
		this.employeeGKCounterOrder = employeeGKCounterOrder;
	}

	@Override
	public String toString() {
		return "BillPrintDataModel [orderNumber=" + orderNumber + ", invoiceNumber=" + invoiceNumber + ", orderDate="
				+ orderDate + ", addressLineList=" + addressLineList + ", businessName=" + businessName
				+ ", employeeGKCounterOrder=" + employeeGKCounterOrder + ", customerName=" + customerName
				+ ", customerMobileNumber=" + customerMobileNumber + ", customerGstNumber=" + customerGstNumber
				+ ", deliveryDate=" + deliveryDate + ", productListForBill=" + productListForBill + ", cGSTAmount="
				+ cGSTAmount + ", iGSTAmount=" + iGSTAmount + ", sGSTAmount=" + sGSTAmount + ", roundOffAmount="
				+ roundOffAmount + ", totalAmountWithoutTax=" + totalAmountWithoutTax + ", totalQuantity="
				+ totalQuantity + ", totalAmountWithTax=" + totalAmountWithTax + ", totalAmountWithTaxInWord="
				+ totalAmountWithTaxInWord + ", categoryWiseAmountForBills=" + categoryWiseAmountForBills
				+ ", totalAmount=" + totalAmount + ", taxAmountInWord=" + taxAmountInWord + ", totalCGSTAmount="
				+ totalCGSTAmount + ", totalIGSTAmount=" + totalIGSTAmount + ", totalSGSTAmount=" + totalSGSTAmount
				+ "]";
	}


	
	
	
	
	
}
