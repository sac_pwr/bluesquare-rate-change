package com.bluesquare.rc.models;

public class CounterOrderReport {

	private int srno;
	private String counterOrderId;
	private String gkName;
	private String customerName;
	private double totalAmount;
	private double totalAmountWithTax;
	private long totalQuantity;
	private String orderStatus;
	private String paymentDate;
	private String paymentStatus;
	private String orderTakenDate;
	private String orderTakenTime;
	public CounterOrderReport(int srno, String counterOrderId, String gkName, String customerName, double totalAmount,
			double totalAmountWithTax, long totalQuantity, String orderStatus, String paymentDate, String paymentStatus,
			String orderTakenDate, String orderTakenTime) {
		super();
		this.srno = srno;
		this.counterOrderId = counterOrderId;
		this.gkName = gkName;
		this.customerName = customerName;
		this.totalAmount = totalAmount;
		this.totalAmountWithTax = totalAmountWithTax;
		this.totalQuantity = totalQuantity;
		this.orderStatus = orderStatus;
		this.paymentDate = paymentDate;
		this.paymentStatus = paymentStatus;
		this.orderTakenDate = orderTakenDate;
		this.orderTakenTime = orderTakenTime;
	}
	@Override
	public String toString() {
		return "CounterOrderReport [srno=" + srno + ", counterOrderId=" + counterOrderId + ", gkName=" + gkName
				+ ", customerName=" + customerName + ", totalAmount=" + totalAmount + ", totalAmountWithTax="
				+ totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", orderStatus=" + orderStatus
				+ ", paymentDate=" + paymentDate + ", paymentStatus=" + paymentStatus + ", orderTakenDate="
				+ orderTakenDate + ", orderTakenTime=" + orderTakenTime + "]";
	}
	public int getSrno() {
		return srno;
	}
	public void setSrno(int srno) {
		this.srno = srno;
	}
	public String getCounterOrderId() {
		return counterOrderId;
	}
	public void setCounterOrderId(String counterOrderId) {
		this.counterOrderId = counterOrderId;
	}
	public String getGkName() {
		return gkName;
	}
	public void setGkName(String gkName) {
		this.gkName = gkName;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}
	public long getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getOrderTakenDate() {
		return orderTakenDate;
	}
	public void setOrderTakenDate(String orderTakenDate) {
		this.orderTakenDate = orderTakenDate;
	}
	public String getOrderTakenTime() {
		return orderTakenTime;
	}
	public void setOrderTakenTime(String orderTakenTime) {
		this.orderTakenTime = orderTakenTime;
	}
	
	
	
}
