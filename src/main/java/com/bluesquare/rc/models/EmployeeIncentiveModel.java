/**
 * 
 */
package com.bluesquare.rc.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeIncentives;

/**
 * @author aNKIT
 *
 */
public class EmployeeIncentiveModel {

	private EmployeeDetails employeeDetails;
	private List<EmployeeIncentives> employeeIncentiveList;

	public EmployeeIncentiveModel(EmployeeDetails employeeDetails, List<EmployeeIncentives> employeeIncentiveList) {
		super();
		this.employeeDetails = employeeDetails;
		this.employeeIncentiveList = employeeIncentiveList;
	}

	@Override
	public String toString() {
		return "EmployeeIncentiveModel [employeeDetails=" + employeeDetails + ", employeeIncentiveList="
				+ employeeIncentiveList + "]";
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public List<EmployeeIncentives> getEmployeeIncentiveList() {
		return employeeIncentiveList;
	}

	public void setEmployeeIncentiveList(List<EmployeeIncentives> employeeIncentiveList) {
		this.employeeIncentiveList = employeeIncentiveList;
	}

}
