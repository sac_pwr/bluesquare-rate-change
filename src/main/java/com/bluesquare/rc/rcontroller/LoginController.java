package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeAreaList;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.models.EmployeeAreaDetails;
import com.bluesquare.rc.rest.models.AppVersionCheckResponse;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.LoginRequest;
import com.bluesquare.rc.rest.models.LoginResponse;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.EmployeeService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@RestController
public class LoginController {
	
/*	@Autowired
	Employee employee;*/
	
	@Autowired
	EmployeeService  employeeService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	/*@Autowired
	EmployeeDetails employeeDetails;*/ 
	
	@Autowired
	JsonWebToken jsonWebToken; 
	
	@Transactional 	@PostMapping("/authentication")
	public ResponseEntity<LoginResponse> authenticateLogin(@RequestBody LoginRequest loginRequest){
		
		System.out.println("inside authentication");
		
		
		HttpStatus httpStatus;
		LoginResponse loginResponse=new LoginResponse();
		Employee employee=new Employee();
		EmployeeDetails employeeDetails=new EmployeeDetails();
		
			employee=employeeService.loginCredentialsForRC(loginRequest.getUserId(),loginRequest.getPassword());
			
			if(employee==null){
				loginResponse.setStatus(Constants.FAILURE_RESPONSE);
				loginResponse.setErrorMsg(Constants.LOGIN_FAILED);			
				httpStatus=HttpStatus.OK;			
			}else{
				
				employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employee.getEmployeeId());
				if(employeeDetails.isStatus())
				{
					loginResponse.setStatus(Constants.FAILURE_RESPONSE);
					loginResponse.setErrorMsg("This Employee Is Disabled");
					httpStatus=HttpStatus.OK;       
				}
				else
				{
					employeeDetailsService.clearToken(loginRequest.getToken(),employee.getEmployeeId());
					employeeDetails.setToken(loginRequest.getToken());				
					employeeDetailsService.updateForWebApp(employeeDetails,false);
					
					httpStatus=HttpStatus.OK;     
					loginResponse.setEmployeeName(employeeDetails.getName());
					loginResponse.setEmployee(employee);
					loginResponse.setStatus(Constants.SUCCESS_RESPONSE);
					loginResponse.setErrorMsg(Constants.NONE);
					
					//Fetch Ares for given Employee
					EmployeeAreaDetails employeeAreaDetails = employeeDetailsService.fetchEmployeeAreaDetails(employeeDetails.getEmployeeDetailsId(),employee.getCompany().getCompanyId());
					Set<Long> countryIdSet=new HashSet<>();
					Set<Long> stateIdSet=new HashSet<>();
					Set<Long> cityIdSet=new HashSet<>();
					Set<Long> regionIdSet=new HashSet<>();
					Set<Long> areaIdSet=new HashSet<>();
					
					for(EmployeeAreaList employeeArea: employeeAreaDetails.getAreaList())
					{
						countryIdSet.add(employeeArea.getArea().getRegion().getCity().getState().getCountry().getCountryId());
						stateIdSet.add(employeeArea.getArea().getRegion().getCity().getState().getStateId());
						cityIdSet.add(employeeArea.getArea().getRegion().getCity().getCityId());
						regionIdSet.add(employeeArea.getArea().getRegion().getRegionId());
						areaIdSet.add(employeeArea.getArea().getAreaId());
					}
					
					List<Long> areaIdList=new ArrayList<>();
					List<Long> regionIdList=new ArrayList<>();
					List<Long> cityIdList=new ArrayList<>();
					List<Long> stateIdList=new ArrayList<>();
					List<Long> countryIdList=new ArrayList<>();
					
					areaIdList.addAll(areaIdSet);
					regionIdList.addAll(regionIdSet);
					cityIdList.addAll(cityIdSet);
					stateIdList.addAll(stateIdSet);
					countryIdList.addAll(countryIdSet);
					
					Map<String,List<Long>> claimMap=new HashMap<>();
					claimMap.put("areaIdList", areaIdList);
					claimMap.put("regionIdList", regionIdList);
					claimMap.put("cityIdList", cityIdList);
					claimMap.put("stateIdList", stateIdList);
					claimMap.put("countryIdList", countryIdList);
					
					Long companyId=employee.getCompany().getCompanyId();
					Long[] companyIdList={companyId};
					
					String issuer="auth0";					
					String token=jsonWebToken.create(claimMap,companyIdList, issuer,employee.getEmployeeId());
					
					loginResponse.setToken(token);
				}
			}
		
			
		return new ResponseEntity<LoginResponse>(loginResponse, httpStatus);
	}
	
	@Transactional 	@GetMapping("/logout/{employeeId}")
	public ResponseEntity<BaseDomain> logoutToken(@ModelAttribute("employeeId") long employeeId) {
		BaseDomain baseDomain = new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("Inside Logout");

			employeeService.logout(employeeId);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
				
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus); 
	}

	@Transactional 	@GetMapping("/checkAppVersion")
    public ResponseEntity<AppVersionCheckResponse> checkAppVersion(HttpServletRequest request){
            String appVersion=request.getParameter("appVersion");
            AppVersionCheckResponse appVersionCheckResponse=new AppVersionCheckResponse();
            HttpStatus httpStatus;
            System.out.println("Inside checkAppVersion(Login Controller)");
            try {
                    String appVersionUpdate=employeeService.checkAppVersion(appVersion);
                    
                            appVersionCheckResponse.setAppVersionUpdateAvailable(appVersionUpdate);
                            appVersionCheckResponse.setStatus(Constants.SUCCESS_RESPONSE);
                            httpStatus=HttpStatus.OK;
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    System.out.println("Error Inside checkAppVersion is "+e.toString());
                    appVersionCheckResponse.setStatus(Constants.FAILURE_RESPONSE);
                    httpStatus=HttpStatus.NO_CONTENT;
            }
            
            return new ResponseEntity<AppVersionCheckResponse>(appVersionCheckResponse,httpStatus);
            
            
            
    }
}
