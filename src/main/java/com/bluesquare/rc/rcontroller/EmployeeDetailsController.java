package com.bluesquare.rc.rcontroller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.EmployeeLocation;
import com.bluesquare.rc.rest.models.AreaListResponse;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.DeliveryBoyResponse;
import com.bluesquare.rc.rest.models.EmployeedetailsEntityResponse;
import com.bluesquare.rc.rest.models.LocationRequest;
import com.bluesquare.rc.service.AreaService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@RestController
public class EmployeeDetailsController {

	 @Autowired
	    EmployeeDetailsService employeeDetailsService;
	 
	 @Autowired
	 AreaService areaService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;

	    /*@GetMapping("/fetchDeliveryBoyDetailbyAreaId/{areaId}")	    
	    public ResponseEntity<DeliveryBoyResponse> fetchDeliveryBoyDetailbyAreaId(@ModelAttribute("areaId") long areaId){
	        
	        System.out.println("fetchEmployeeDetailbyAreaId");
	        HttpStatus httpStatus;
	        DeliveryBoyResponse deliveryBoyResponse=new DeliveryBoyResponse();
	        
	        List<EmployeeDetails> deliveryBoyList=employeeDetailsService.fetchDBEmployeeDetailByAreaId(areaId);
	        
	        
	        
	        if(deliveryBoyList==null)
	        {
	            deliveryBoyResponse.setStatus(Constants.FAILURE_RESPONSE);
	            httpStatus=HttpStatus.NO_CONTENT;
	        }
	        else
	        {
	            deliveryBoyResponse.setDeliveryBoyList(deliveryBoyList);  
	            deliveryBoyResponse.setStatus(Constants.SUCCESS_RESPONSE);
	            httpStatus=HttpStatus.OK;
	        }
	        
	        return new ResponseEntity<DeliveryBoyResponse>(deliveryBoyResponse, httpStatus);
	  }*/
	    
	    @Transactional 	@GetMapping("/fetchAreaListByEmployeeId/{employeeId}")	    
	    public ResponseEntity<AreaListResponse> fetchAreaListByEmployeeId(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId){
	    	
	    	System.out.println("fetchAreaListByEmployeeId");
	        HttpStatus httpStatus;
	        AreaListResponse areaListResponse=new AreaListResponse();
	      
	        
	       
				List<Area> areaList=employeeDetailsService.fetchAreaByEmployeeId(employeeId);
				
				if(areaList==null)
				{
				    areaListResponse.setStatus(Constants.FAILURE_RESPONSE);
				    httpStatus=HttpStatus.NO_CONTENT;
				}
				else
				{
				    areaListResponse.setAreaList(areaList);  
				    areaListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				    httpStatus=HttpStatus.OK;
				}
			
	        
	        return new ResponseEntity<AreaListResponse>(areaListResponse, httpStatus);
	    	
	    }
	    
	   /* @GetMapping("/fetchEmployeeDetailbyAreaId/{areaId}")	    
	    public @ResponseBody List<EmployeeDetails> fetchEmployeeDetailbyAreaId(@ModelAttribute("areaId") long areaId){
	        
	        System.out.println("fetchEmployeeDetailbyAreaId");
	        
	        
	        List<EmployeeDetails> salesPersonBoyList=employeeDetailsService.fetchSMEmployeeDetailByAreaId(areaId);
	        
	        return salesPersonBoyList; 
	  }*/
	    
	    
	    @Transactional 	@GetMapping("/fetchDeliveryBoyListByBusinessNameAreaId/{businessNameAreaId}")
	    public ResponseEntity<DeliveryBoyResponse> fetchDeliveryBoyListByBusinessNameAreaId(@RequestHeader("Authorization") String token,@ModelAttribute("businessNameAreaId") long businessNameAreaId){
	    	
	    	System.out.println("fetchDeliveryBoyListByBusinessNameAreaId");
	    	DeliveryBoyResponse deliveryBoyResponse=new DeliveryBoyResponse();
	    	HttpStatus httpStatus;
	    	
	    	
	    	
	    	
				List<EmployeeDetails> deliveryBoyList=employeeDetailsService.fetchDeliveryBoyListByBusinessNameAreaId(businessNameAreaId);
				
			/*	if(deliveryBoyList==null){
					deliveryBoyResponse.setStatus(Constants.FAILURE_RESPONSE);
					    httpStatus=HttpStatus.NO_CONTENT;
				}*/
				deliveryBoyResponse.setStatus(Constants.SUCCESS_RESPONSE);
				deliveryBoyResponse.setDeliveryBoyList(deliveryBoyList);
				 httpStatus=HttpStatus.OK;
			
	    	
	    	return new ResponseEntity<DeliveryBoyResponse>(deliveryBoyResponse,httpStatus);
	    }
	    
	    @Transactional 	@GetMapping("/fetchEmployeeDetailsByEmployeeId/{employeeId}")
    public ResponseEntity<EmployeedetailsEntityResponse> fetchEmployeeDetailsByEmployeeId(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId")long employeeId){
	    	
	    	EmployeedetailsEntityResponse employeedetailsEntityResponse =new EmployeedetailsEntityResponse();
	    	HttpStatus httpStatus;
	    	
	    	
	    	
	    	
				EmployeeDetails employeeDetailList=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
				String areaList=areaService.fetchAreaListByEmployeeId(employeeId);
				if(employeeDetailList==null)
				{
					employeedetailsEntityResponse.setStatus(Constants.FAILURE_RESPONSE);
					employeedetailsEntityResponse.setErrorMsg("No Record Found");
					httpStatus=HttpStatus.NO_CONTENT;
				}
				else
				{
					employeedetailsEntityResponse.setStatus(Constants.SUCCESS_RESPONSE);
					employeedetailsEntityResponse.setEmployeeDetails(employeeDetailList);
					employeedetailsEntityResponse.setEmployeeAreaListString(areaList);
					httpStatus=HttpStatus.OK;
				}
			
	    	return new ResponseEntity<EmployeedetailsEntityResponse>(employeedetailsEntityResponse,httpStatus);
	    	
	    }
	    
	    @Transactional 	@PostMapping("/saveLocation")
	    public ResponseEntity<BaseDomain> saveLocation(@RequestHeader("Authorization") String token,@RequestBody LocationRequest locationRequest){
	    
	    	BaseDomain baseDomain =new BaseDomain();
	    	HttpStatus httpStatus;
	    	
	    	
	    	
	    	
				EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(locationRequest.getEmployeeId());
				
				EmployeeLocation employeeLocation=new EmployeeLocation();
				employeeLocation.setLatitude(locationRequest.getLatitude());
				employeeLocation.setLongitude(locationRequest.getLongitude());
				employeeLocation.setEmployeeDetails(employeeDetails);
				employeeLocation.setDatetime(new Date(locationRequest.getDateTime()));
				employeeLocation.setLocationInfo(locationRequest.getInfo());
				employeeLocation.setAddress(locationRequest.getAddress());
				employeeLocation.setNewRoute(locationRequest.isNewRoute());
				employeeDetailsService.saveEmployeeLocation(employeeLocation);
				
				httpStatus=HttpStatus.OK;
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			 	
	    	
	    	return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	    }
	    	
	    
	    
}
