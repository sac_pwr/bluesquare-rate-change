package com.bluesquare.rc.rcontroller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.rest.models.SnapShotReportRequest;
import com.bluesquare.rc.rest.models.SnapShotReportResponse;
import com.bluesquare.rc.service.SnapShotService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@RestController
public class SnapShotController {
	
	
	@Autowired
	SnapShotService snapShotService;
		
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Transactional 	@PostMapping("/fetchSnapShotRecordByEmpIdAndDateRange")
	public ResponseEntity<SnapShotReportResponse> fetchSnapShotRecordByEmpIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody SnapShotReportRequest snapShotReportRequest ){
		
		SnapShotReportResponse snapShotReportResponse=new SnapShotReportResponse();
		HttpStatus httpStatus;
		System.out.println("fetchSnapShotRecordByEmpIdAndDateRange");
		
	
		
		
			SnapShotReportResponse snapShotReportResponseList=snapShotService.fetchRecordForSnapShot(snapShotReportRequest.getEmployeeId(), snapShotReportRequest.getFromDate(), snapShotReportRequest.getToDate(), snapShotReportRequest.getRange());
			
			if(snapShotReportResponseList==null)
			{
				snapShotReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
				snapShotReportResponse.setErrorMsg("No Record Found");
			}
			else
			{
				snapShotReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;    
				snapShotReportResponse.setTotalNoOrderTaken(snapShotReportResponseList.getTotalNoOrderTaken());
				snapShotReportResponse.setTotalNoOrderCanceled(snapShotReportResponseList.getTotalNoOrderCanceled());
				snapShotReportResponse.setTotalAmtSales(snapShotReportResponseList.getTotalAmtSales());
				snapShotReportResponse.setTotalIssueAmt(snapShotReportResponseList.getTotalIssueAmt());
				snapShotReportResponse.setRtnOrderAmt(snapShotReportResponseList.getRtnOrderAmt());
				snapShotReportResponse.setTotalCollectionAmt(snapShotReportResponseList.getTotalCollectionAmt());
				snapShotReportResponse.setReplaceOrderAmt(snapShotReportResponseList.getReplaceOrderAmt());
			}
		
		
		
		return new ResponseEntity<SnapShotReportResponse>(snapShotReportResponse,httpStatus);
	}
	

}
