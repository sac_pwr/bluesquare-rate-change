package com.bluesquare.rc.rcontroller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.BusinessType;
import com.bluesquare.rc.rest.models.BusinessTypeListResponse;
import com.bluesquare.rc.service.BusinessTypeService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@RestController
public class BusinessTypeControllerForApp {

	@Autowired
	BusinessType businessType; 
	
	@Autowired
	BusinessTypeService businessTypeService; 	
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Transactional 	@PostMapping("/fetchBusinessTypeListForApp")
	public ResponseEntity<BusinessTypeListResponse> fetchBusinessTypeList(@RequestHeader("Authorization") String token)
	{		
		System.out.println("inside fetchBusinessTypeListForApp");
		HttpStatus httpStatus;	
		BusinessTypeListResponse businessTypeListResponse=new BusinessTypeListResponse();
		
		
		
		
			List<BusinessType> businessTypesList=businessTypeService.fetchBusinessTypeListForWebApp();
			if(businessTypesList==null)
			{
				businessTypeListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			else
			{
				businessTypeListResponse.setBusinessTypesList(businessTypesList);
				businessTypeListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
			return new ResponseEntity<BusinessTypeListResponse>(businessTypeListResponse, httpStatus);
		
	}
}
