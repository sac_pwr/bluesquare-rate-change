package com.bluesquare.rc.rcontroller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.dao.EmployeeDetailsDAO;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;
import com.bluesquare.rc.entities.PaymentPaySupplier;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.FetchProductBySupplierResponse;
import com.bluesquare.rc.rest.models.InventoryProductDetailsReportResponse;
import com.bluesquare.rc.rest.models.InventoryProductListRequest;
import com.bluesquare.rc.rest.models.InventoryProductListResponse;
import com.bluesquare.rc.rest.models.InventoryReportRequest;
import com.bluesquare.rc.rest.models.InventorySaveRequestModel;
import com.bluesquare.rc.rest.models.InventotryReportResponse;
import com.bluesquare.rc.rest.models.OpenInventoryResponseModel;
import com.bluesquare.rc.rest.models.ProductAddInventory;
import com.bluesquare.rc.service.BrandService;
import com.bluesquare.rc.service.CategoriesService;
import com.bluesquare.rc.service.InventoryService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.SupplierService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;


@RestController
public class InventoryController {

	@Autowired
	SupplierService supplierService;
	
	@Autowired
	BrandService brandService;
	
	@Autowired
	CategoriesService categoriesService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	InventoryService inventoryService;
		
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	
	@Autowired
	CompanyDAO companyDAO;
	
			// fetching SupplierList BrandList And CategoryList while adding inventory
		@Transactional 	@PostMapping("/addMultipleInventoryForApp")	
		public ResponseEntity<OpenInventoryResponseModel> openAddMultipleInventoryforApp(@RequestHeader("Authorization") String token){
			
			OpenInventoryResponseModel openInventoryResponseModel= new OpenInventoryResponseModel();
			HttpStatus httpStatus;
			
			
			
			
				List<Supplier> supplierList=supplierService.fetchSupplierForWebAppList();
				openInventoryResponseModel.setSupplier(supplierList);
				
				List<Brand> brandList=brandService.fetchBrandListForWebApp();
				openInventoryResponseModel.setBrand(brandList);
				
				List<Categories> categoriesList=categoriesService.fetchCategoriesListForWebApp();
				openInventoryResponseModel.setCategory(categoriesList);
				
				openInventoryResponseModel.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			
		
			return new ResponseEntity<OpenInventoryResponseModel>(openInventoryResponseModel,httpStatus);
			
		}

		
		// fetching ProductList By SupplierId for AddInventory Screen
@Transactional 	@GetMapping("/fetchProductBySupplierIdForApp/{supplierId}")
public ResponseEntity<InventoryProductListResponse> fetchProductBySupplierIdforApp(@RequestHeader("Authorization") String token,@ModelAttribute("supplierId") String supplierId){
	
	
	InventoryProductListResponse inventoryProductListResponse=new InventoryProductListResponse();
	HttpStatus httpStatus;
	
	
	
		List<Product> productlist=productService.fetchProductListBySupplierId(supplierId);
		
		if (productlist == null) {
			inventoryProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
			inventoryProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.NO_CONTENT;
			inventoryProductListResponse.setErrorMsg("No Product is available");
			
		}else{
			inventoryProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
			inventoryProductListResponse.setProduct(productlist);
		}
	
	
	return new ResponseEntity<InventoryProductListResponse>(inventoryProductListResponse,httpStatus);
}
	//// fetching ProductList By SupplierId ,CategoryId And BrandId for AddInventory Screen
	@Transactional 	@PostMapping("/fetchProductBySupplierIdAndCategoryIdAndBrandIdForApp")
		
	public ResponseEntity<FetchProductBySupplierResponse> fetchProductBySupplierIdAndCategoryIdAndBrandIdforApp(@RequestHeader("Authorization") String token,@RequestBody InventoryProductListRequest inventoryProductListRequest){
		
		FetchProductBySupplierResponse fetchProductBySupplierResponse=new FetchProductBySupplierResponse();
		HttpStatus httpStatus;
		System.out.println("fetchProductBySupplierIdAndCategoryIdAndBrandIdforApp");
		
		
		
		
			if(inventoryProductListRequest.getSupplierId()==null)
			{
				fetchProductBySupplierResponse.setStatus(Constants.FAILURE_RESPONSE);
				fetchProductBySupplierResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				fetchProductBySupplierResponse.setErrorMsg("Select Supplier");
			}
			else
			{
				List<ProductAddInventory> productList=productService.fetchProductListByBrandIdAndCategoryIdForApp(inventoryProductListRequest.getSupplierId(), inventoryProductListRequest.getCategoryId(), inventoryProductListRequest.getBrandId());
				
		
				if (productList == null) {
					fetchProductBySupplierResponse.setStatus(Constants.FAILURE_RESPONSE);
					fetchProductBySupplierResponse.setStatus(Constants.FAILURE_RESPONSE);
					httpStatus = HttpStatus.OK;
					fetchProductBySupplierResponse.setErrorMsg("No Product is available");
					
				}else{
					fetchProductBySupplierResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus = HttpStatus.OK;
					
					fetchProductBySupplierResponse.setProductAddInventories(productList);
				}
			}
		
		return new ResponseEntity<FetchProductBySupplierResponse>(fetchProductBySupplierResponse,httpStatus);
	}
	
	@Transactional 	@PostMapping("/saveAddedInventoryForApp")
	public ResponseEntity<BaseDomain> saveAddedInventoryForApp (@RequestHeader("Authorization") String token,@RequestBody InventorySaveRequestModel inventorySaveRequestModel){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("saveAddedInventoryForApp");
		
		
		
		
			inventoryService.saveInventoryForApp(inventorySaveRequestModel);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		
	}
	
	@Transactional 	@PostMapping("/editInventoryForApp")
	public ResponseEntity<BaseDomain> editInventoryForApp (@RequestHeader("Authorization") String token,@RequestBody InventorySaveRequestModel inventorySaveRequestModel){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("editInventoryForApp");
		
		
				
		inventoryService.editInventoryForApp(inventorySaveRequestModel);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		
	}
    
	
	@Transactional 	@GetMapping("/deleteInventoryForApp/{inventoryId}")
	public ResponseEntity<BaseDomain> deleteInventoryForApp (@RequestHeader("Authorization") String token,@ModelAttribute("inventoryId") String inventoryId){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("editInventoryForApp");
		
		
				
		inventoryService.deleteInventory(inventoryId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		
	}
	
	/***
	 * check here inventory payment done or not
	 * @param token
	 * @param inventoryId
	 * @return
	 */
	@Transactional 	@GetMapping("/checkInventoryPaymentForApp/{inventoryId}")
	public ResponseEntity<BaseDomain> checkInventoryPaymentForApp (@RequestHeader("Authorization") String token,@ModelAttribute("inventoryId") String inventoryId){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		System.out.println("checkInventoryPaymentForApp");
		
		
				
		//here PaymentPaySupplier list found means inventory payment is done so not allowed for edit and delete
		List<PaymentPaySupplier> paymentPaySupplierList=inventoryService.fetchPaymentPaySupplierListByInventoryId(inventoryId);
		if(paymentPaySupplierList==null){
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		}else{
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
		}
		
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		
	}
	
	@Transactional 	@PostMapping("/fetchAddInventoryForGateKeeperReportByDateRange")
	public ResponseEntity<InventotryReportResponse> fetchAddInventoryForGateKeeperReportByEmpIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody InventoryReportRequest inventoryReportRequest ){
		InventotryReportResponse inventotryReportResonse=new InventotryReportResponse();
		HttpStatus httpStatus;
		System.out.println("fetchAddInventoryForGateKeeperReportByDateRange");
		
		
		
		
			List<Inventory> inventoryList=inventoryService.fetchAddedInventoryforGateKeeperReportByDateRange(inventoryReportRequest.getFromDate(), inventoryReportRequest.getToDate(), inventoryReportRequest.getRange());
			if(inventoryList==null)
			{
				inventotryReportResonse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
			
				inventotryReportResonse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				inventotryReportResonse.setInventory(inventoryList);
			}
		
		
				return new ResponseEntity<InventotryReportResponse>(inventotryReportResonse,httpStatus);
	
	}
	@Transactional 	@GetMapping("/fetchInventoryProductDetailsForGateKeeperReport/{inventoryTransactionId}")
	public ResponseEntity<InventoryProductDetailsReportResponse> fetchInventoryProductDetailsforGateKeeperReport(@RequestHeader("Authorization") String token,@ModelAttribute ("inventoryTransactionId") String inventoryTransactionId){
		
		InventoryProductDetailsReportResponse inventoryProductDetailsReportResponse=new InventoryProductDetailsReportResponse();
		HttpStatus httpStatus;
		System.out.println("fetchInventoryProductDetailsforGateKeeperReport");
		
		
		
		
			Inventory inventory=inventoryService.fetchInventoryByInventoryTransactionIdforGateKeeperReport(inventoryTransactionId);
			if(inventory==null){
				inventoryProductDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
				return new ResponseEntity<InventoryProductDetailsReportResponse>(inventoryProductDetailsReportResponse,httpStatus);
			}
			List<InventoryDetails> inventoryDetailsList=inventoryService.fetchInventoryDetailsByInventoryTransactionIdforGateKeeperReport(inventoryTransactionId);
			if(inventoryDetailsList==null){
				inventoryProductDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
				}else{
					//inventoryDetailsList=inventoryService.makeProductImageNullOfInventoryDetailsList(inventoryDetailsList);
					inventoryProductDetailsReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					inventoryProductDetailsReportResponse.setInventoryDetails(inventoryDetailsList);
					inventoryProductDetailsReportResponse.setInventory(inventory);
					
					if(inventory.getEmployee()!=null){
						inventoryProductDetailsReportResponse.setName(employeeDetailsDAO.getEmployeeDetailsByemployeeId(inventory.getEmployee().getEmployeeId()).getName());
					}else{
						inventoryProductDetailsReportResponse.setName("Company");
					}
					
					httpStatus=HttpStatus.OK;
				}
		
		return new ResponseEntity<InventoryProductDetailsReportResponse>(inventoryProductDetailsReportResponse,httpStatus);
	}
	
}
