package com.bluesquare.rc.rcontroller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.rest.models.CustomerReportMainResponseModel;
import com.bluesquare.rc.rest.models.CustomerReportRequest;
import com.bluesquare.rc.rest.models.CustomerReportResponse;
import com.bluesquare.rc.rest.models.OrderDetailsList;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@RestController
public class CustomerReportController {

@Autowired
OrderDetails orderDetails;

@Autowired
OrderDetailsDAO orderDetailsDAO;

@Autowired
OrderDetailsService orderDetailsService;

@Autowired
EmployeeDetailsService employeeDetailsService;

@Autowired
JsonWebToken jsonWebToken;	

@Autowired
HttpSession session;

@Transactional 	@PostMapping("/fetchOrderDetailsForCustomerReportByEmpIdAndDateRange")
public ResponseEntity<CustomerReportMainResponseModel> fetchOrderDetailByEmployeeIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody CustomerReportRequest customerReportRequest){
	
	
	CustomerReportMainResponseModel customerReportMainResponseModel=new CustomerReportMainResponseModel();
	HttpStatus httpStatus;
	System.out.println("fetchOrderDetailByEmployeeIdAndDateRange");
	
	
	
	
		List<CustomerReportResponse> orderDetailslist=orderDetailsService.fetchOrderDetailsforCustomerReportByEmpIdAndDateRange(customerReportRequest.getEmployeeId(), customerReportRequest.getFromDate(), customerReportRequest.getToDate(), customerReportRequest.getRange());
		if(orderDetailslist.isEmpty()){
			customerReportMainResponseModel.setStatus(Constants.FAILURE_RESPONSE);
			customerReportMainResponseModel.setErrorMsg("Records Not Found");
			httpStatus=HttpStatus.OK;
		}else{
			customerReportMainResponseModel.setStatus(Constants.SUCCESS_RESPONSE);
			customerReportMainResponseModel.setErrorMsg(Constants.NONE);
			customerReportMainResponseModel.setCustomerReportResponse(orderDetailslist);
	
			httpStatus=HttpStatus.OK;
			
		}
	
	
	return new ResponseEntity<CustomerReportMainResponseModel>(customerReportMainResponseModel , httpStatus);
	

}
@Transactional 	@GetMapping("/fetchOrderDetailForCustomerReportByBusinessNameId/{businessNameId}/{employeeId}")
 public ResponseEntity<OrderDetailsList> fetchOrderDetailForCustomerReportByBusinessNameId(@RequestHeader("Authorization") String token,@ModelAttribute("businessNameId") String businessNameId,@ModelAttribute("employeeId") long employeeId){
	
	OrderDetailsList orderDetailsListResponse =new OrderDetailsList();
	HttpStatus httpStatus;
	
	System.out.println("fetchOrderDetailForCustomerReportByBusinessNameId");
	
	
	
	
		EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(employeeId);
		List<OrderDetails> orderDetailList=orderDetailsService.fetchOrderDetailForCustomerReportByBusinessNameId(businessNameId);
				
				if(orderDetailList==null){
					orderDetailsListResponse.setStatus(Constants.FAILURE_RESPONSE);
					orderDetailsListResponse.setErrorMsg(Constants.NO_CONTENT);
					httpStatus=HttpStatus.NO_CONTENT;
					}
				else{
					orderDetailsListResponse.setStatus(Constants.SUCCESS_RESPONSE);
					orderDetailsListResponse.setErrorMsg(Constants.NONE);
					orderDetailsListResponse.setOrderDetailsList(orderDetailList);
					orderDetailsListResponse.setSalesManName(employeeDetails.getName());
					httpStatus=HttpStatus.OK;
					
				}
	
	
	
	return new ResponseEntity<OrderDetailsList>(orderDetailsListResponse,httpStatus);
}

}	

