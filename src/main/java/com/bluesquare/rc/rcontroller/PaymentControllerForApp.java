package com.bluesquare.rc.rcontroller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.PaymentListModel;
import com.bluesquare.rc.rest.models.PaymentTakeRequest;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.PaymentService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@RestController
public class PaymentControllerForApp {

	@Autowired
	PaymentService paymentService;
	
	@Autowired
	OrderDetails orderDetails;
	
	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Transactional 	@PostMapping("/savePaymentStatus")
	public ResponseEntity<BaseDomain> savePaymentStatus(@RequestHeader("Authorization") String token,@RequestBody PaymentTakeRequest paymentTakeRequest) {
		BaseDomain baseDomain=new BaseDomain();                                                                                                                                                                                                   
		HttpStatus httpStatus;
		
		orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(paymentTakeRequest.getOrderId());
		orderDetails=paymentService.savePaymentStatus(paymentTakeRequest,orderDetails);
		orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}
	/*@GetMapping("/fetchCollectionDetailByOrderId/{orderId}")
	public ResponseEntity<CollectionReportResponse> fetchCollectionDetailByOrderId(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
		
		CollectionReportResponse collectionReportResponse=new CollectionReportResponse();
		HttpStatus httpStatus;
		
		if(jsonWebToken.verifyToken(token)){
			collectionReportResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.UNAUTHORIZED;
			return new ResponseEntity<CollectionReportResponse>(collectionReportResponse, httpStatus);
		}	
		session.setAttribute("authToken", token);
		
		
			collectionReportResponse=paymentService.fetchCollectionDetailsByOrderId(orderId);
			collectionReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
		
		
		return new ResponseEntity<CollectionReportResponse>(collectionReportResponse,httpStatus);
		
	}*/
	
	@Transactional 	@GetMapping("/fetchPaymentListByOrderIdForApp/{orderId}")
	public ResponseEntity<PaymentListModel> fetchPaymentListByOrderIdForApp(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
		
		PaymentListModel paymentListModel=new PaymentListModel();
		HttpStatus httpStatus;
		
		paymentListModel=paymentService.fetchPaymentListByOrderIdForApp(orderId);
		paymentListModel.setStatus(Constants.SUCCESS_RESPONSE);
		
		httpStatus=HttpStatus.OK;		
		
		return new ResponseEntity<PaymentListModel>(paymentListModel,httpStatus);
	}
}
