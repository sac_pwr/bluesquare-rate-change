package com.bluesquare.rc.rcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@Component
public class RequestProcessingInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	JsonWebToken jsonWebToken;

	@Autowired
	HttpSession session;

	static String[] ignoreUrls = { "downloadProductImage", "login", "loginEmployee", "authentication", "resources"};

	public static boolean isIgnoreUrl(String serviceUrl) {

		for (String url : ignoreUrls) {
			if (url.equals(serviceUrl.trim())) {
				return true;
			}
		}

		// do split for ignore file request i.e. jquery.js,script.js,invoice.pdf
		if ((serviceUrl.split("\\.")).length > 1) {
			return true;
		}

		return false;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// set few parameters to handle ajax request from different host
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS");
		response.addHeader("Access-Control-Max-Age", "1000");
		response.addHeader("Access-Control-Allow-Headers", "Content-Type");
		response.addHeader("Cache-Control", "private");

		String reqUri = request.getRequestURI();
		System.out.println(" incoming request uri : " + reqUri);

		String urlParts[] = reqUri.split("/");
		String serviceName = "";
		try {
			serviceName = urlParts[2];
		} catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			//assign blanck("") for http://localhost:8080/BlueSquare/
			serviceName = "";
		}

		// App side Authorization
		String token = request.getHeader("Authorization");

		if (token == null) {
			if (!isIgnoreUrl(serviceName)) {

				String loginType = (String) session.getAttribute("loginType");
				if (loginType == null) {
					response.sendRedirect(request.getContextPath() + "/login");
					return false;
				} else {
					if (loginType.equals(Constants.GATE_KEEPER_DEPT_NAME)) {
						EmployeeDetails employeeDetails = (EmployeeDetails) session.getAttribute("employeeDetails");
						if (employeeDetails == null) {
							response.sendRedirect(request.getContextPath() + "/login");
							return false;
						}
					}else if (loginType.equals(Constants.COMPANY_ADMIN)) {
						Company company=(Company)session.getAttribute("companyDetails");
						if (company == null) {
							response.sendRedirect(request.getContextPath() + "/login");
							return false;
						}
					}
				}
			}
		} else {
			// token verify
			if (!isIgnoreUrl(serviceName)) {
				if (jsonWebToken.verifyToken(token)) {
					throw new Exception("Token Invalid");
				}
				session.setAttribute("authToken", token);
			}

		}
		return super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		String reqUri = request.getRequestURI();
		System.out.println(" processing request uri : " + reqUri);

		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

		String reqUri = request.getRequestURI();
		System.out.println(" completed request uri : " + reqUri);
	}
}
