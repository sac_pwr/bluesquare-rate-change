package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.OrderDetailsDAO;
import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.GKReturnOrderReportRequest;
import com.bluesquare.rc.rest.models.GkReturnReportResponse;
import com.bluesquare.rc.rest.models.OrderDetailListByDateRangeRequest;
import com.bluesquare.rc.rest.models.OrderDetailsList;
import com.bluesquare.rc.rest.models.ReIssueOrderDetailsReportResponse;
import com.bluesquare.rc.rest.models.ReIssueOrderProductDetailsReportResponse;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeRequest;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeResponse;
import com.bluesquare.rc.rest.models.ReturnOrderProductDetailsReportResponse;
import com.bluesquare.rc.rest.models.ReturnOrderProductListResponse;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.rest.models.ReturnOrderResponse;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.ReturnOrderService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@RestController
public class ReturnOrderController {

	@Autowired
	OrderDetails orderDetails;
	@Autowired
	OrderProductDetails orderProductDetails;

	@Autowired
	OrderDetailsDAO orderDetailsDAO;

	@Autowired
	OrderDetailsService orderDetailsService;

	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;

	@Transactional 	@GetMapping("/fetchReturnOrderList/{orderId}")
	public ResponseEntity<ReturnOrderResponse> fetchReturnOrderListByOrderId(@RequestHeader("Authorization") String token,
			@ModelAttribute("orderId") String orderId) {

		System.out.println("fetchReturnOrderList");

		ReturnOrderResponse returnOrderResponse = new ReturnOrderResponse();
		HttpStatus httpStatus;

		orderDetails = orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
		if (orderDetails == null) {
			returnOrderResponse.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus = HttpStatus.OK;
			returnOrderResponse.setErrorMsg("OrderId is invalid");
			return new ResponseEntity<ReturnOrderResponse>(returnOrderResponse, httpStatus);

		}

		if (orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED)/* || orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED_PENDING)*/){

			System.out.println("fetchOrderprodutListByOrderId");
			List<OrderProductDetails> list = orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
			
			//this 4 line made for make OrderProductDetails product image null  
			returnOrderResponse.setOrderProductDetails(list);			
			List<ReturnOrderResponse> returnOrderResponseList=new ArrayList<>();
			returnOrderResponseList.add(returnOrderResponse);
			//returnOrderResponseList=orderDetailsService.makeProductsImageNullOrderDetailsWithOrderProductDetailsList(returnOrderResponseList);
			
			returnOrderResponse.setOrderProductDetails(returnOrderResponseList.get(0).getOrderProductDetails());
			returnOrderResponse.setOrderDetails(orderDetails);
			returnOrderResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;

			return new ResponseEntity<ReturnOrderResponse>(returnOrderResponse, httpStatus);

		}

		returnOrderResponse.setErrorMsg("Order not delivered Still");
		returnOrderResponse.setStatus(Constants.FAILURE_RESPONSE);
		httpStatus = HttpStatus.OK;
		return new ResponseEntity<ReturnOrderResponse>(returnOrderResponse, httpStatus);
	}

	@Transactional 	@PostMapping("/fetchReturnOrderDetailByBusinessIdAndDateRange")
	public ResponseEntity<OrderDetailsList> fetchReturnOrderDetailByBusinessIdAndDateRange(@RequestHeader("Authorization") String token,
			@RequestBody OrderDetailListByDateRangeRequest orderDetailListByDateRangeRequest) {
		System.out.println("fetchReturnOrderDetailByBusinessIdAndDateRange");

		OrderDetailsList orderDetailsResponseList = new OrderDetailsList();
		HttpStatus httpStatus;

		
		
		List<OrderDetails> orderDetailsList= orderDetailsService.fetchOrderDetailBybusinessNameIdAndDateRange(
				orderDetailListByDateRangeRequest.getBusinessNameId(), orderDetailListByDateRangeRequest.getFromDate(),
				orderDetailListByDateRangeRequest.getToDate());
		if(orderDetailsList==null)
		{
			orderDetailsResponseList.setStatus(Constants.FAILURE_RESPONSE);
			orderDetailsResponseList.setErrorMsg("Orders Not Available");
			httpStatus = HttpStatus.OK;
		}
		else
		{
			orderDetailsResponseList.setOrderDetailsList(orderDetailsList);
			orderDetailsResponseList.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<OrderDetailsList>(orderDetailsResponseList, httpStatus);
	}

	/*@PostMapping("/updateReturnOrder")
	 
	public ResponseEntity<BaseDomain> updateReturnOrder(@RequestBody ReturnOrderProduct returnOrderProduct){
		
		System.out.println("in update ReturnOrder"+returnOrderProduct);
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		ReturnOrderProduct returnOrderProductlist=returnOrderService.
		
		
	}*/
	
	@Transactional 	@PostMapping("/saveReturnOrder")
	public ResponseEntity<BaseDomain> saveReturnOrder(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderRequest returnOrderRequest){
		
		System.out.println("saveReturnOrder");
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		
		
					
			returnOrderService.save(returnOrderRequest);			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
		
		
		
	}
	
	@Transactional 	@PostMapping("/fetchReturnOrderDetailsListByDateRange")
	public ResponseEntity<ReturnOrderDateRangeResponse> fetchReturnOrderDetailsListByDateRange(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest){
		
		System.out.println("fetchReturnOrderDetailsListByDateRange");
		ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
		HttpStatus httpStatus;
		
		
		
		
			//calling the DaoImpl through service layer and passing the element through request model
			List<ReturnOrderProduct> returnOrderProductList=returnOrderService.fetchReturnOrderDetailsListByDateRange(returnOrderDateRangeRequest.getEmployeeId(),returnOrderDateRangeRequest.getFromDate(),returnOrderDateRangeRequest.getToDate(),returnOrderDateRangeRequest.getRange());
			if(returnOrderProductList==null){
				returnOrderDateRangeResponse.setStatus(Constants.FAILURE_RESPONSE);
				returnOrderDateRangeResponse.setErrorMsg(Constants.NO_CONTENT);
				
				httpStatus=HttpStatus.OK;
			}else{
				returnOrderDateRangeResponse.setStatus(Constants.SUCCESS_RESPONSE);
				returnOrderDateRangeResponse.setErrorMsg(Constants.NONE);
				returnOrderDateRangeResponse.setReturnOrderProductList(returnOrderProductList);
				httpStatus=HttpStatus.OK;
			}
			
		
		return new ResponseEntity<ReturnOrderDateRangeResponse>(returnOrderDateRangeResponse ,httpStatus);
	}

	@Transactional 	@GetMapping("/fetchReturnOrderProductListByReturnOrderProductId/{returnOrderProductId}")
	
	public ResponseEntity<ReturnOrderProductListResponse> fetchReturnOrderProductListByReturnOrderProductId(@RequestHeader("Authorization") String token,@ModelAttribute("returnOrderProductId") String returnOrderProductId){
		
		System.out.println("fetchReturnOrderProductListByReturnOrderProductId");
		ReturnOrderProductListResponse returnOrderProductListResponse=new ReturnOrderProductListResponse();
		HttpStatus httpStatus;
		
		
		
		
			List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderService.fetchReturnOrderProductDetailsByReturnOrderProductId(returnOrderProductId);
			 //returnOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(returnOrderProductDetailsList);
			if (returnOrderProductDetailsList == null) {
				returnOrderProductListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus = HttpStatus.OK;
				returnOrderProductListResponse.setErrorMsg("OrderId is invalid");
				
			}else{
			returnOrderProductListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus = HttpStatus.OK;
			returnOrderProductListResponse.setReturnOrderProductDetails(returnOrderProductDetailsList);
			}
			return new ResponseEntity<ReturnOrderProductListResponse>(returnOrderProductListResponse,httpStatus);
		
		
	}


	// Sachin sharma 21-10-2017
	
	@Transactional 	@PostMapping("/fetchReturnOrderDetailsForDBReportByEmpIdDateRange")
	public ResponseEntity<ReturnOrderDateRangeResponse> fetchReturnOrderDetailsForDBReportByEmpIdDateRange(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest ){
		
		ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
		HttpStatus httpStatus;
		System.out.println("fetchReturnOrderDetailsForDBReportByEmpIdDateRange");
		
		
		
		
			
			List<ReturnOrderProduct> returnOrderProductList=returnOrderService.fetchReturnOrderProductListForDBReportByEmpIdDateRange
																			   (returnOrderDateRangeRequest.getEmployeeId(),
																				returnOrderDateRangeRequest.getFromDate(),
																				returnOrderDateRangeRequest.getToDate(),
																				returnOrderDateRangeRequest.getRange());
			
			if(returnOrderProductList==null)
			{
				returnOrderDateRangeResponse.setStatus(Constants.FAILURE_RESPONSE);
				returnOrderDateRangeResponse.setErrorMsg("No Record Found");
				httpStatus=HttpStatus.OK;
			}else
			{
				returnOrderDateRangeResponse.setStatus(Constants.SUCCESS_RESPONSE);
				//returnOrderDateRangeResponse.se
				returnOrderDateRangeResponse.setReturnOrderProductList(returnOrderProductList);
				httpStatus=HttpStatus.OK;
				
			}
		
													
		return new ResponseEntity<ReturnOrderDateRangeResponse>(returnOrderDateRangeResponse,httpStatus);													
	}
	
	
	//here we r fetching ReturnOrderProductDetails By returnOrderProductId For GateKeeper report
	@Transactional 	@GetMapping("/fetchReturnOrderProductDetailsByReturnOrderProductIdForReport/{returnOrderProductId}")
	public ResponseEntity<ReturnOrderProductDetailsReportResponse> fetchReturnOrderProductDetailsByOrderIdForGKReport(@RequestHeader("Authorization") String token,@ModelAttribute("returnOrderProductId") String returnOrderProductId){
		
		ReturnOrderProductDetailsReportResponse gKReturnOrderReportResponse=new ReturnOrderProductDetailsReportResponse();
		HttpStatus httpStatus;
		System.out.println("fetchReturnOrderProductDetailsByOrderIdForGKReport");
		
		
		
		
			ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderForGKReportByReturnOrderProductId(returnOrderProductId);
			if(returnOrderProduct==null)
			{
				gKReturnOrderReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				gKReturnOrderReportResponse.setErrorMsg("No OrderFound");
				httpStatus=HttpStatus.NO_CONTENT;
			}
				List<ReturnOrderProductDetails> returnOrderProductDetailsList=returnOrderService.fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(returnOrderProductId);
				//returnOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(returnOrderProductDetailsList);
				if(returnOrderProductDetailsList==null)
				{
					gKReturnOrderReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					gKReturnOrderReportResponse.setErrorMsg("No ProductDetails found");
					httpStatus=HttpStatus.NO_CONTENT;	
				}	
	
				gKReturnOrderReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				gKReturnOrderReportResponse.setReturnOrderProductDetailsList(returnOrderProductDetailsList);
				gKReturnOrderReportResponse.setReturnOrderProduct(returnOrderProduct);
				httpStatus=HttpStatus.OK;
		
	
	return new ResponseEntity<ReturnOrderProductDetailsReportResponse>(gKReturnOrderReportResponse,httpStatus);
	}
	//here we re fetching Return OrderDetails By EmpId,DateRange and areaId
	@Transactional 	@PostMapping("/fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId")
	public ResponseEntity<GkReturnReportResponse> fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId(@RequestHeader("Authorization") String token,@RequestBody GKReturnOrderReportRequest gKReturnOrderReportRequest){
		
		GkReturnReportResponse gkReturnReportResponse =new GkReturnReportResponse();
		HttpStatus httpStatus;
		System.out.println("fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId");
		
		
		
		
			GkReturnReportResponse returnOrderProductList=returnOrderService.fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId
																								(gKReturnOrderReportRequest.getEmployeeId(), 
																								gKReturnOrderReportRequest.getAreaId(), 
																								gKReturnOrderReportRequest.getFromDate(),
																								gKReturnOrderReportRequest.getToDate(), 
																								gKReturnOrderReportRequest.getRange());
			if(returnOrderProductList.getReturnOrderProductList()==null){
				gkReturnReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				gkReturnReportResponse.setErrorMsg("No Return Order Found");
				httpStatus=HttpStatus.NO_CONTENT;
				
			}
			else
			{
				gkReturnReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				gkReturnReportResponse.setAreaId(returnOrderProductList.getAreaId());
				gkReturnReportResponse.setReturnOrderProductList(returnOrderProductList.getReturnOrderProductList());
				httpStatus=HttpStatus.OK;
			}
		
		
		return new ResponseEntity<GkReturnReportResponse>(gkReturnReportResponse,httpStatus);
		
	}
	
	//fetchReturnOrderProductListByFilter
	@Transactional 	@PostMapping("/fetchReturnOrderProductListByFilter")
	public ResponseEntity<ReturnOrderDateRangeResponse> fetchReturnOrderProductListByFilter(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest){
		ReturnOrderDateRangeResponse returnOrderDateRangeResponse=new ReturnOrderDateRangeResponse();
		HttpStatus httpStatus;
		System.out.println("fetchReturnOrderProductListByFilter");
		
		
		
		
			returnOrderDateRangeResponse=returnOrderService.fetchReturnOrderProductListByFilter(returnOrderDateRangeRequest.getEmployeeId(), returnOrderDateRangeRequest.getRange(), returnOrderDateRangeRequest.getFromDate(), returnOrderDateRangeRequest.getToDate(), returnOrderDateRangeRequest.getAreaId());
			if(returnOrderDateRangeResponse.getReturnOrderProductList()==null)
			{
				httpStatus=HttpStatus.OK;
				returnOrderDateRangeResponse.setErrorMsg("List not found");
				returnOrderDateRangeResponse.setStatus(Constants.FAILURE_RESPONSE);
			}
			else
			{
				httpStatus=HttpStatus.OK;
				returnOrderDateRangeResponse.setStatus(Constants.SUCCESS_RESPONSE);
			}		
			
		
		
		return new ResponseEntity<ReturnOrderDateRangeResponse>(returnOrderDateRangeResponse,httpStatus);
	}
	
	// here we are fetching Reissue product Details for replacement report by empId and dateRange
		@Transactional 	@PostMapping("/fetchReIssueProductDetailsForReplacementReport")
		public ResponseEntity<ReIssueOrderDetailsReportResponse> fetchReIssueProductDetailsForReplacementReport(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest ){
			
			ReIssueOrderDetailsReportResponse replacementReIssueOrderDetailsReportResponse=new ReIssueOrderDetailsReportResponse();
		   	HttpStatus httpStatus;
			System.out.println("fetchReIssueProductDetailsForReplacementReport");
			
			
			
			
				List<ReIssueOrderDetails> reIssueOrderDetailsList=returnOrderService.fetchReIssueOrderDetailsForReplacementReportByEmpIdAndDateRange(returnOrderDateRangeRequest.getEmployeeId(), returnOrderDateRangeRequest.getFromDate(), returnOrderDateRangeRequest.getToDate(), returnOrderDateRangeRequest.getRange());
				if(reIssueOrderDetailsList==null)
				{
					replacementReIssueOrderDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					replacementReIssueOrderDetailsReportResponse.setErrorMsg("No Record Found");
					httpStatus=HttpStatus.OK;
				}
				else
				{
					replacementReIssueOrderDetailsReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					replacementReIssueOrderDetailsReportResponse.setReIssueOrderDetailsList(reIssueOrderDetailsList); 
					httpStatus=HttpStatus.OK;
					
				}
			
			return new ResponseEntity<ReIssueOrderDetailsReportResponse>(replacementReIssueOrderDetailsReportResponse,httpStatus);

		}
		
		//@Transactional 	@GetMapping("/fetchReIssueOrderProductDetailsForReplacementReport/{orderId}")
		@Transactional 	@GetMapping("/fetchReIssueOrderProductDetailsForReplacementReport/{reIssueOrderId}")
		public ResponseEntity<ReIssueOrderProductDetailsReportResponse> fetchReIssueOrderProductDetailsForReplacmentReport(@RequestHeader("Authorization") String token,@ModelAttribute("reIssueOrderId") long reIssueOrderId ){
			
			ReIssueOrderProductDetailsReportResponse replacementReportOrderProductDetailsResponse=new ReIssueOrderProductDetailsReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchReIssueOrderProductDetailsForReplacmentReport");

				ReIssueOrderDetails reIssueOrderDetails=returnOrderService.fetchReIssueOrderDetailsForReplacementReportByOrderId(reIssueOrderId);
				if(reIssueOrderDetails==null)
				{
					replacementReportOrderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
					replacementReportOrderProductDetailsResponse.setErrorMsg("No Record found");
					httpStatus=HttpStatus.NO_CONTENT;
				}
				List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList=returnOrderService.fetchReIssueOrderProductDetailsForReplacementReportByReIssueOrderId(reIssueOrderId);
				//fetchReIssueOrderProductDetailsForReplacementReportByOrderId(orderId);
				//reIssueOrderProductDetailsList=returnOrderService.makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(reIssueOrderProductDetailsList);
								
				EmployeeDetails employeeDetails =employeeDetailsService.getEmployeeDetailsByemployeeId(reIssueOrderDetails.getEmployeeSMDB().getEmployeeId());

				if(reIssueOrderProductDetailsList==null)
				{
					replacementReportOrderProductDetailsResponse.setStatus(Constants.FAILURE_RESPONSE);
					replacementReportOrderProductDetailsResponse.setErrorMsg("No Record found");
					httpStatus=HttpStatus.NO_CONTENT;
				}
				else
				{				
					replacementReportOrderProductDetailsResponse.setStatus(Constants.SUCCESS_RESPONSE);
					replacementReportOrderProductDetailsResponse.setReIssueOrderDetails(reIssueOrderDetails);
					replacementReportOrderProductDetailsResponse.setReIssueOrderProductDetailsList(reIssueOrderProductDetailsList);
					replacementReportOrderProductDetailsResponse.setDeliveryPersonName(employeeDetails.getName());
					httpStatus=HttpStatus.OK;
				}
			
		
			return new ResponseEntity<ReIssueOrderProductDetailsReportResponse>(replacementReportOrderProductDetailsResponse,httpStatus);
		}
		    
		
		// here we are fetching OrderDetails For Gk Replacment Report By empId and Date range 
		@Transactional 	@PostMapping("/fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRange")
		public ResponseEntity<ReIssueOrderDetailsReportResponse> fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderDateRangeRequest returnOrderDateRangeRequest){
			
			ReIssueOrderDetailsReportResponse replacementReIssueOrderDetailsReportResponse=new ReIssueOrderDetailsReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchOrderDetailsForGKReplacmentReportByEmpIdAndDateRange");
			
			
			
			
				List<ReIssueOrderDetails> reIssueOrderDetailsList=returnOrderService.fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeApp(returnOrderDateRangeRequest.getEmployeeId(), returnOrderDateRangeRequest.getFromDate(), returnOrderDateRangeRequest.getToDate(), returnOrderDateRangeRequest.getRange());
				if(reIssueOrderDetailsList==null)
				{
					replacementReIssueOrderDetailsReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					replacementReIssueOrderDetailsReportResponse.setErrorMsg("No Record Found");
					httpStatus=HttpStatus.OK;
					
				}
				else
				{
					replacementReIssueOrderDetailsReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					replacementReIssueOrderDetailsReportResponse.setReIssueOrderDetailsList(reIssueOrderDetailsList);
					httpStatus=HttpStatus.OK;
					
				}
			
			return new ResponseEntity<ReIssueOrderDetailsReportResponse>(replacementReIssueOrderDetailsReportResponse,httpStatus);
		}

}
