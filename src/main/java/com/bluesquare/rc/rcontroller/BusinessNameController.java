package com.bluesquare.rc.rcontroller;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.dao.TokenHandlerDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.BusinessNameListRequest;
import com.bluesquare.rc.rest.models.BusinessNameListResponse;
import com.bluesquare.rc.rest.models.BusinessNameResponse;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;
@RestController
public class BusinessNameController {

	@Autowired
	BusinessName businessName;
	
	@Autowired
	BusinessNameService businessNameService;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	TokenHandlerDAO tokenHandlerDAO;
	
	
	@Transactional 	@PostMapping("/saveBusinessNameForApp")
	public ResponseEntity<BaseDomain> saveBusinessName(@RequestBody BusinessName businessName,
													   @RequestHeader("Authorization") String token)
	{
		System.out.println("inside saveBusinessName");
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		String response=businessNameService.checkBusinessDuplication(businessName.getContact().getMobileNumber(), "mobileNumber", "0");
		if(response.equals("Failed")){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Mobile Number Already Exist");
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		}
		if(businessName.getContact().getTelephoneNumber()!=null){
			if(!businessName.getContact().getTelephoneNumber().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getContact().getTelephoneNumber(), "telephoneNumber", "0");
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("Telephone Number Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		if(businessName.getGstinNumber()!=null){
			if(!businessName.getGstinNumber().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getGstinNumber(), "gstNo", "0");	
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("GST Number Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		if(businessName.getContact().getEmailId()!=null){
			if(!businessName.getContact().getEmailId().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getContact().getEmailId(), "emailId", "0");
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("Email Id Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		
		businessNameService.saveBusinessName(businessName);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
	}
	
	@Transactional 	@PostMapping("/updateBusinessNameForApp")
	public ResponseEntity<BaseDomain> updateBusinessNameForApp(@RequestBody BusinessName businessName,
			   												   @RequestHeader("Authorization") String token)
	{
		System.out.println("inside updateBusinessNameForApp");
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;		
		
		String response=businessNameService.checkBusinessDuplication(businessName.getContact().getMobileNumber(), "mobileNumber", businessName.getBusinessNameId());
		if(response.equals("Failed")){
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			baseDomain.setErrorMsg("Mobile Number Already Exist");
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		}
		if(businessName.getContact().getTelephoneNumber()!=null){
			if(!businessName.getContact().getTelephoneNumber().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getContact().getTelephoneNumber(), "telephoneNumber", businessName.getBusinessNameId());
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("Telephone Number Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		if(businessName.getGstinNumber()!=null){
			if(!businessName.getGstinNumber().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getGstinNumber(), "gstNo", businessName.getBusinessNameId());	
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("GST Number Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
		if(businessName.getContact().getEmailId()!=null){
			if(!businessName.getContact().getEmailId().isEmpty()){
				response=businessNameService.checkBusinessDuplication(businessName.getContact().getEmailId(), "emailId", businessName.getBusinessNameId());
				if(response.equals("Failed")){
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg("Email Id Already Exist");
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
				}
			}
		}
	
			BusinessName businessNm=businessNameService.fetchBusinessForWebApp(businessName.getBusinessNameId());
			businessName.setBusinessNamePkId(businessNm.getBusinessNamePkId());
			businessName.setBusinessAddedDatetime(businessNm.getBusinessAddedDatetime());
			businessName.setDisableDatetime(businessNm.getDisableDatetime());
			businessName.setBusinessUpdatedDatetime(new Date());
			businessName.setStatus(businessNm.isStatus());
			businessName.setCompany(businessNm.getCompany());
			businessNameService.updateForWebApp(businessName);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		
		
	}
	
	@Transactional 	@PostMapping("/fetchBusinessNameListForApp")
	public ResponseEntity<BusinessNameListResponse> fetchBusinessNameList(@RequestHeader("Authorization") String token)
	{
		System.out.println("inside fetchBusinessNameListForApp");
		BusinessNameListResponse businessNameListResponse=new BusinessNameListResponse();
		HttpStatus httpStatus;	
	
		
			List<BusinessName> businessNamesList=businessNameService.getBusinessNameListForWebApp();
			if(businessNamesList==null)
			{
				businessNameListResponse.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				businessNameListResponse.setBusinessNamesList(businessNamesList);
				businessNameListResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
			
			return new ResponseEntity<BusinessNameListResponse>(businessNameListResponse, httpStatus);
		
				
	}
	
	@Transactional 	@GetMapping("/fetchBusinessNameBybusinessNameIdForApp/{businessNameId}")
	public ResponseEntity<BusinessNameResponse> fetchBusinessNameNameBybusinessNameId(@ModelAttribute("businessNameId") String businessNameId,
																					  @RequestHeader("Authorization") String token)
	{
		System.out.println("inside fetchBusinessNameBybusinessNameIdForApp");
		BusinessNameResponse businessNameResponse=new BusinessNameResponse();
		HttpStatus httpStatus;	
		
		
		
		
			BusinessName businessName=businessNameService.fetchBusinessForWebApp(businessNameId);
			if(businessName==null)
			{
				businessNameResponse.setStatus(Constants.FAILURE_RESPONSE);
				businessNameResponse.setErrorMsg(Constants.NOT_FOUND);
				httpStatus=HttpStatus.OK;
			}
			else
			{
				if(businessName.isStatus())
				{
					businessNameResponse.setStatus(Constants.FAILURE_RESPONSE);
					businessNameResponse.setErrorMsg("This Business Is Disable by Admin");
					httpStatus=HttpStatus.OK;
				}
				else
				{
					businessNameResponse.setBusinessName(businessName);
					businessNameResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
				}
			}
			return new ResponseEntity<BusinessNameResponse>(businessNameResponse, httpStatus);
		
				
	}
	
	@Transactional 	@PostMapping("/businessNameListByAreaIdAndBusinessTypeId")
    public ResponseEntity<BusinessNameListResponse> BusinessNameByAreaIdAndBusinessTypeId(@RequestBody BusinessNameListRequest businessnameListRequest ,@RequestHeader("Authorization") String token){
        
        System.out.println("business Name By businessTypeId and AreaId ");
        BusinessNameListResponse businessNameListResponse=new BusinessNameListResponse();		
		HttpStatus httpStatus;
		
		
		
       
			  
			List<BusinessName> businessNameList=businessNameService.businessNameByAreaIdAndBusinessTypeIdForWebApp(businessnameListRequest.getBusinessTypeId(),businessnameListRequest.getAreaId());
			
			if (businessNameList==null) {
			    businessNameListResponse.setStatus(Constants.FAILURE_RESPONSE);
			    businessNameListResponse.setErrorMsg(Constants.NOT_FOUND);
			    httpStatus = HttpStatus.OK;
			}
			else
			{
				Iterator<BusinessName> itr=businessNameList.iterator();
				while(itr.hasNext())
				{
					if(itr.next().isStatus())
					{
						itr.remove();
					}
				}
			    businessNameListResponse.setBusinessNamesList(businessNameList);
			    businessNameListResponse.setStatus(Constants.SUCCESS_RESPONSE);
			    httpStatus = HttpStatus.OK;
			}
			
			
			return new ResponseEntity<BusinessNameListResponse>(businessNameListResponse, httpStatus);
		
    }
	
	@Transactional 	@GetMapping("/fetchBusinessNameByAreaId/{areaId}")
    public ResponseEntity<BusinessNameListResponse> fetchBusinessNameByAreaId(@ModelAttribute("areaId") long areaId,@RequestHeader("Authorization") String token){
		System.out.println("inside fetchBusinessNameBybusinessNameIdForApp");
		BusinessNameListResponse businessNameResponse=new BusinessNameListResponse();
		HttpStatus httpStatus;

		
		
		
				
			List<BusinessName> businessNamesList=businessNameService.fetchBusinessNameByAreaId(areaId);
			
			if(businessNamesList==null){
				
				businessNameResponse.setStatus(Constants.FAILURE_RESPONSE);
				businessNameResponse.setErrorMsg(Constants.NOT_FOUND);
				httpStatus=HttpStatus.OK;
				  return new ResponseEntity<BusinessNameListResponse>(businessNameResponse,httpStatus);
				
			}else{
				businessNameResponse.setBusinessNamesList(businessNamesList);
				//businessNameResponse.setBusinessName(businessName);
				businessNameResponse.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				return new ResponseEntity<BusinessNameListResponse>(businessNameResponse,httpStatus);
			}
		
}

}
