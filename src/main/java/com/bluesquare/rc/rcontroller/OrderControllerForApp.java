package com.bluesquare.rc.rcontroller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.models.ReturnOrderFromDeliveryBoyReport;
import com.bluesquare.rc.rest.models.BaseDomain;
import com.bluesquare.rc.rest.models.BookOrderMainRequest;
import com.bluesquare.rc.rest.models.BookOrderResponse;
import com.bluesquare.rc.rest.models.CustomerReportMainResponseModel;
import com.bluesquare.rc.rest.models.CustomerReportRequest;
import com.bluesquare.rc.rest.models.CustomerReportResponse;
import com.bluesquare.rc.rest.models.DBReportRequest;
import com.bluesquare.rc.rest.models.EmployeeNameAndId;
import com.bluesquare.rc.rest.models.GkSnapProductListMainResponse;
import com.bluesquare.rc.rest.models.GkSnapProductResponse;
import com.bluesquare.rc.rest.models.InventoryReportRequest;
import com.bluesquare.rc.rest.models.OrderDetailByBusinessNameIdEmployeeIdRequest;
import com.bluesquare.rc.rest.models.OrderDetailResponse;
import com.bluesquare.rc.rest.models.OrderDetailsForPayment;
import com.bluesquare.rc.rest.models.OrderDetailsList;
import com.bluesquare.rc.rest.models.OrderDetailsListForPayment;
import com.bluesquare.rc.rest.models.OrderDetailsPaymentList;
import com.bluesquare.rc.rest.models.OrderDetailsPaymentListByBusinessName;
import com.bluesquare.rc.rest.models.OrderIssueRequest;
import com.bluesquare.rc.rest.models.OrderProductIssueListForIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderProductIssueReportRequest;
import com.bluesquare.rc.rest.models.OrderProductIssueReportResponse;
import com.bluesquare.rc.rest.models.OrderReIssueRequest;
import com.bluesquare.rc.rest.models.OrderRequest;
import com.bluesquare.rc.rest.models.PaymentListModel;
import com.bluesquare.rc.rest.models.PaymentListRequest;
import com.bluesquare.rc.rest.models.ReIssueDelivered;
import com.bluesquare.rc.rest.models.ReIssueOrderDetailsListModel;
import com.bluesquare.rc.rest.models.ReIssueOrderProductDetailsListModel;
import com.bluesquare.rc.rest.models.ReturnOrderFromDeliveryBoyReportModel;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.service.BusinessNameService;
import com.bluesquare.rc.service.EmployeeDetailsService;
import com.bluesquare.rc.service.OrderDetailsService;
import com.bluesquare.rc.service.ProductService;
import com.bluesquare.rc.service.ReturnOrderService;
import com.bluesquare.rc.utils.Constants;
import com.bluesquare.rc.utils.JsonWebToken;

@RestController
public class OrderControllerForApp {

	@Autowired
	OrderDetailsService orderDetailsService;
	
	@Autowired
	EmployeeDetailsService employeeDetailsService;
	
	@Autowired
	OrderDetails  orderDetails;
	
	@Autowired
	ReturnOrderService returnOrderService;
	
	@Autowired
	BusinessNameService businessNameService;

	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	JsonWebToken jsonWebToken;	
	
	@Autowired
	HttpSession session;
	
	@Autowired
	ProductService productService;
	
	@Transactional 	@PostMapping("/bookOrder")
	public ResponseEntity<BookOrderResponse> bookOrder(@RequestHeader("Authorization") String token,@RequestBody BookOrderMainRequest bookOrderMainRequest){
		
		HttpStatus httpStatus;
		BookOrderResponse bookOrderResponse=new BookOrderResponse();
		
			//book order non free
			String orderDetailsId=orderDetailsService.bookOrder(bookOrderMainRequest.getOrderRequest());
			orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderDetailsId);
			
			//book free product
			if(bookOrderMainRequest.getFreeProductHave().equals("Yes"))
			{
				bookOrderMainRequest.getBookOrderFreeProductRequest().setOrderId(orderDetailsId);
				orderDetailsService.freeProductAddInOrderProductDetails(bookOrderMainRequest.getBookOrderFreeProductRequest());
			}
			//update payment period days
			orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderDetailsId);
			orderDetails.setPaymentPeriodDays(bookOrderMainRequest.getOrderPaymentPeriodDaysRequest().getOrderPaymentPeriodDays());
			orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
			
			//response
			bookOrderResponse.setBusinessName(orderDetails.getBusinessName().getShopName());
			bookOrderResponse.setOrderId(orderDetailsId);
			bookOrderResponse.setTotalAmount(orderDetails.getTotalAmount());
			bookOrderResponse.setTotalAmountWithTax(orderDetails.getTotalAmountWithTax());
			bookOrderResponse.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			
			return new ResponseEntity<BookOrderResponse>(bookOrderResponse, httpStatus);
					
	}
	
	/*@PostMapping("/bookOrderFreeProducts")
	public ResponseEntity<BaseDomain> bookOrder(@RequestBody BookOrderFreeProductRequest bookOrderFreeProductRequest){
		
		HttpStatus httpStatus;
		BaseDomain baseDomain=new BaseDomain();
		
		try 
		{
			orderDetailsService.freeProductAddInOrderProductDetails(bookOrderFreeProductRequest);
			
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
		} catch (Exception e) {
			
			httpStatus=HttpStatus.FORBIDDEN;
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		}		
	}
	*/
	@Transactional 	@PostMapping("/updateBookOrder")
	public ResponseEntity<BaseDomain> updateBookOrder(@RequestHeader("Authorization") String token,@RequestBody OrderRequest orderRequest){
		
		HttpStatus httpStatus;
		BaseDomain baseDomain=new BaseDomain();
		
		OrderDetails orderDetails=orderDetailsService.fetchOrderDetailsByOrderId(orderRequest.getOrderDetails().getOrderId());
		
		List<OrderProductDetails> orderProductDetailsList=orderDetailsService.fetchOrderProductDetailByOrderId(orderRequest.getOrderDetails().getOrderId());
		
		if(orderDetails.getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_BOOKED)){
			/* check order product quantity with current inventory*/
			Map<Long,Long> productListWithQty=new HashMap<>();
			for(OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList()){
				OrderProductDetails oldOrderProductDetails=null;
				
				for(OrderProductDetails orderProductDetailsOld :  orderProductDetailsList){
					if(orderProductDetails.getProduct().getProductId()==orderProductDetailsOld.getProduct().getProduct().getProductId() && orderProductDetails.getType().equals(orderProductDetailsOld.getType())){
						oldOrderProductDetails=orderProductDetailsOld;
					}
				}			
				
				if(productListWithQty.containsKey(orderProductDetails.getProduct().getProductId())){
					long qty;//=productListWithQty.get(orderProductDetails.getProduct().getProductId())+orderProductDetails.getIssuedQuantity();
					
					if(oldOrderProductDetails==null){
						qty=productListWithQty.get(orderProductDetails.getProduct().getProductId())+orderProductDetails.getIssuedQuantity();
					}else{
						qty=productListWithQty.get(orderProductDetails.getProduct().getProductId())+orderProductDetails.getIssuedQuantity()-oldOrderProductDetails.getIssuedQuantity();
					}
					if(qty>0){
						productListWithQty.put(orderProductDetails.getProduct().getProductId(),qty);
					}
				}else{
					long qty;
					if(oldOrderProductDetails==null){
						qty=orderProductDetails.getIssuedQuantity();
					}else{
						qty=orderProductDetails.getIssuedQuantity()-oldOrderProductDetails.getIssuedQuantity();
					}
					if(qty>0){
						productListWithQty.put(orderProductDetails.getProduct().getProductId(),qty);
					}
				}
			}
			
			for(Map.Entry<Long,Long> entry :productListWithQty.entrySet()){
				Product product=productService.fetchProductForWebApp(entry.getKey());
				System.out.println("product : "+product);
				System.out.println("productId : "+entry.getKey());
				if(product.getCurrentQuantity()<entry.getValue()){
					
					baseDomain.setStatus(Constants.FAILURE_RESPONSE);
					baseDomain.setErrorMsg(product.getProductName()+" qty exceeds Current Qty. Max Available : "+product.getCurrentQuantity());
					httpStatus=HttpStatus.OK;
					return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);				
				}
			}
			/*End*/
		}
		
		
		//set image and content type
		List<Product> productList=productService.fetchProductListForWebApp();
		List<OrderProductDetails> orderProductDetailsListTemp=new ArrayList<>();
		for(OrderProductDetails orderProductDetails : orderRequest.getOrderProductDetailList()){
			Product product=orderProductDetails.getProduct().getProduct();
			for(Product product2: productList){ 
				if(product2.getProductId()==product.getProductId()){
					product.setProductImage(product2.getProductImage());
					product.setProductContentType(product2.getProductContentType());
					
					product.setThreshold(product2.getThreshold());
					product.setFreeQuantity(product2.getFreeQuantity());
					product.setProductAddedDatetime(product2.getProductAddedDatetime());
					product.setProductQuantityUpdatedDatetime(product2.getProductQuantityUpdatedDatetime());
					product.setCompany(product2.getCompany());
					
					orderProductDetails.getProduct().setProduct(product);
					orderProductDetailsListTemp.add(orderProductDetails);
				}
			}
		}
		orderRequest.setOrderProductDetailList(orderProductDetailsListTemp);
		
		String orderDetailsId=orderDetailsService.updateBookOrder(orderRequest);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
			
	}
	
	/*@PostMapping("/orderPaymentPeriodDaysUpdate")
	public ResponseEntity<BaseDomain> orderPaymentPeriodDaysUpdate(@RequestBody OrderPaymentPeriodDaysRequest orderPaymentPeriodDaysRequest){
		
		HttpStatus httpStatus;
		BaseDomain baseDomain=new BaseDomain();
		
		try 
		{
			orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderPaymentPeriodDaysRequest.getOrderId());
			orderDetails.setPaymentPeriodDays(orderPaymentPeriodDaysRequest.getOrderPaymentPeriodDays());
			orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
		} catch (Exception e) {
			
			httpStatus=HttpStatus.FORBIDDEN;
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
		}
		
	}*/
	
	@Transactional 	@GetMapping("/OrderDetailsTodayList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> OrderDetailsTodaysList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		
		
		
			orderDetailsList = orderDetailsService.fetchOrderListByAreaId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

		

	}
	
	@Transactional 	@GetMapping("/OrderDetailsPendingList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> OrderDetailsPendingList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {
		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

	
		
		
			orderDetailsList = orderDetailsService.fetchPendingOrderListByAreaId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Pending Order");
			} else {

				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}

			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

		
	}
	@Transactional 	@GetMapping("/packedOrderDetailsTodayList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> PackedOrderDetailsTodayList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		

			orderDetailsList = orderDetailsService.fetchTodaysPackedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

	}
	
	@Transactional 	@GetMapping("/packedOrderDetailsPendingList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> packedOrderDetailsPendingList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		

			orderDetailsList = orderDetailsService.fetchPendingPackedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

		

	}
	
	@Transactional 	@GetMapping("/issuedOrderDetailsTodayList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> issuedOrderDetailsTodayList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		
			orderDetailsList = orderDetailsService.fetchTodaysIssuedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

	}
	
	@Transactional 	@GetMapping("/issuedOrderDetailsPendingList/{employeeId}/{areaId}")
	public ResponseEntity<OrderDetailsList> issuedOrderDetailsPendingList(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("areaId") long areaId) {

		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();

		
			orderDetailsList = orderDetailsService.fetchPendingIssuedOrderListByAreaIdAndEmployeeId(areaId, employeeId);
			if (orderDetailsList.getOrderDetailsList() == null) {
				httpStatus = HttpStatus.OK;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsList.setErrorMsg("No Order Today");
			} else {
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
			return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

		

	}
	
	@Transactional 	@GetMapping("/fetchOrderDetailsByOrderId/{orderId}")
    public ResponseEntity<OrderDetailResponse> fetchOrderDetailsByOrderId(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
        
        OrderDetailResponse orderDetailResponse=new OrderDetailResponse();
        HttpStatus httpStatus;
        
            List<OrderProductDetails> orderProductDetailList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
            if(orderProductDetailList==null){
                orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
                orderDetailResponse.setErrorMsg("Order Details Not Found");
                httpStatus=HttpStatus.FORBIDDEN;
                return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
            }
           
            OrderDetails orderDetailList=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
            if(orderDetailList==null){
            	
                orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
                orderDetailResponse.setErrorMsg("Order Details Not Found");
                httpStatus=HttpStatus.FORBIDDEN;
                return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
            }
            
            EmployeeDetails employeeDetails =employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetailList.getEmployeeSM().getEmployeeId());
            
            orderDetailResponse.setStatus(Constants.SUCCESS_RESPONSE);
            orderDetailResponse.setOrderDetailList(orderDetailList);
            orderDetailResponse.setOrderProductDetailList(orderProductDetailList);
            orderDetailResponse.setSalesPersonName(employeeDetails.getName());
            httpStatus=HttpStatus.OK;
            return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
       
    }
	
	@Transactional 	@GetMapping("/fetchOrderDetailsByOrderIdForReIssue/{orderId}/{gateKeeperId}")
    public ResponseEntity<OrderDetailResponse> fetchOrderDetailsByOrderId(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId,@ModelAttribute("gateKeeperId") long gateKeeperId){
        
        
        OrderDetailResponse orderDetailResponse=new OrderDetailResponse();
        HttpStatus httpStatus;
        
        
        
       
            List<OrderProductDetails> orderProductDetailList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
            if(orderProductDetailList==null){
                orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
                httpStatus=HttpStatus.FORBIDDEN;
                return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
            }
            //orderProductDetailList=orderDetailsService.makeProductImageMNullorderProductDetailsList(orderProductDetailList);
            
            OrderDetails orderDetailList=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
            if(orderDetailList==null){
                orderDetailResponse.setStatus(Constants.FAILURE_RESPONSE);
                httpStatus=HttpStatus.FORBIDDEN;
                return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
            }
            EmployeeDetails employeeDetails =employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetailList.getEmployeeSM().getEmployeeId());
            List<EmployeeNameAndId> employeeNameAndIdSMAndDBList=employeeDetailsService.fetchSMandDBByGateKeeperId(gateKeeperId);
            
            ReturnOrderProduct returnOrderProduct=returnOrderService.fetchReturnOrderProductByReIssueStatus(orderId);
            
            orderDetailResponse.setReturnOrderProduct(returnOrderProduct);
            orderDetailResponse.setStatus(Constants.SUCCESS_RESPONSE);
            orderDetailResponse.setOrderDetailList(orderDetailList);
            orderDetailResponse.setOrderProductDetailList(orderProductDetailList);
            orderDetailResponse.setSalesPersonName(employeeDetails.getName());
            orderDetailResponse.setEmployeeNameAndIdSMAndDBList(employeeNameAndIdSMAndDBList);
            httpStatus=HttpStatus.OK;
            return new ResponseEntity<OrderDetailResponse>(orderDetailResponse,httpStatus);
            
    }
    
    
	@Transactional 	@PostMapping("/fetchOrderListForPayment")
    public ResponseEntity<OrderDetailsListForPayment> fetchOrderListForPayment(@RequestHeader("Authorization") String token,@RequestBody PaymentListRequest paymentListRequest){
		
		OrderDetailsListForPayment orderDetailsListForPayment=new OrderDetailsListForPayment();
		HttpStatus httpStatus;
		
		
		
			List<OrderDetailsForPayment> orderDetailsForPaymentList=orderDetailsService.fetchOrderListForPayment(paymentListRequest);
			
			if(orderDetailsForPaymentList==null)
			{
				orderDetailsListForPayment.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				orderDetailsListForPayment.setOrderDetailsList(orderDetailsForPaymentList);
				orderDetailsListForPayment.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
		
		
		return new ResponseEntity<OrderDetailsListForPayment>(orderDetailsListForPayment,httpStatus);
	}


	@Transactional 	@GetMapping("/changePaymentDueDateSet/{orderId}/{date}")
    public ResponseEntity<BaseDomain> changePaymentDueDateSet(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId,@ModelAttribute("date") long date){
		
		HttpStatus httpStatus;
		BaseDomain baseDomain=new BaseDomain();
		
		
		
		
			orderDetails=orderDetailsService.fetchOrderDetailsByOrderIdForApp(orderId);
			Calendar cal=Calendar.getInstance();
			cal.setTimeInMillis(date);
			orderDetails.setOrderDetailsPaymentTakeDatetime(cal.getTime());
			orderDetailsService.updateOrderDetailsPaymentDays(orderDetails);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
			return new ResponseEntity<BaseDomain>(baseDomain, httpStatus);
			
		
	}
	
	@Transactional 	@PostMapping("/fetchOrderDetailsForTotalCollectionByEmployeeIdAndDateRange")
	public ResponseEntity<CustomerReportMainResponseModel> fetchOrderDetailByEmployeeIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody CustomerReportRequest customerReportRequest){
		
		CustomerReportMainResponseModel customerReportMainResponseModel=new CustomerReportMainResponseModel();
		HttpStatus httpStatus;
		
		
		
		List<CustomerReportResponse> totelCollectionlist=orderDetailsService.fetchTotalCollectionByDateRangeAndEmployeeId(customerReportRequest.getEmployeeId(), customerReportRequest.getFromDate(), customerReportRequest.getToDate(), customerReportRequest.getRange());
			
		if(totelCollectionlist.isEmpty())
		{
			customerReportMainResponseModel.setStatus(Constants.FAILURE_RESPONSE);
			customerReportMainResponseModel.setErrorMsg("Records Not Found");
			httpStatus=HttpStatus.OK;
		}
		else
		{
			customerReportMainResponseModel.setStatus(Constants.SUCCESS_RESPONSE);
			customerReportMainResponseModel.setErrorMsg(Constants.NONE);
			customerReportMainResponseModel.setCustomerReportResponse(totelCollectionlist);
			httpStatus=HttpStatus.OK;
		}
		return new ResponseEntity<CustomerReportMainResponseModel>(customerReportMainResponseModel,httpStatus);
	}
	@Transactional 	@PostMapping("/packedOrderToDeliverBoy")
	public ResponseEntity<BaseDomain> issueOrderToDeliverBoy(@RequestHeader("Authorization") String token,@RequestBody OrderIssueRequest orderIssueRequest){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
				
		/* check order product quantity with current inventory*/
		Map<Long,Long> productListWithQty=new HashMap<>();
		for(OrderProductDetails orderProductDetails : orderIssueRequest.getOrderProductDetailsList()){
			if(productListWithQty.containsKey(orderProductDetails.getProduct().getProductId())){
				long qty=productListWithQty.get(orderProductDetails.getProduct().getProductId())+orderProductDetails.getIssuedQuantity();
				productListWithQty.put(orderProductDetails.getProduct().getProductId(),qty);
			}else{
				productListWithQty.put(orderProductDetails.getProduct().getProductId(),orderProductDetails.getIssuedQuantity());
			}
		}
		
		for(Map.Entry<Long,Long> entry :productListWithQty.entrySet()){
			Product product=productService.fetchProductForWebApp(entry.getKey());
			if(product.getCurrentQuantity()<entry.getValue()){
				
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(product.getProductName()+" qty exceeds Current Qty. Max Available : "+product.getCurrentQuantity());
				httpStatus=HttpStatus.OK;
				return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);				
			}
		}
		/*End*/
		
		String result=orderDetailsService.packedOrderToDeliverBoy(orderIssueRequest);
		if(result.equals("Success"))
		{
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		}
		else
		{
			baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			httpStatus=HttpStatus.FORBIDDEN;
		}		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	//confirmIssuedOrderFromDB
	@Transactional 	@GetMapping("/confirmIssuedOrderFromDB/{orderId}")
	public ResponseEntity<BaseDomain> confirmIssuedOrderFromDB(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
				
		orderDetailsService.confirmPackedOrderFromDB(orderId);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	//orderDeliveredAndReturn
	@Transactional 	@PostMapping("/orderDeliveredAndReturn")
	public ResponseEntity<BaseDomain> orderDeliveredAndReturn(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderRequest returnOrderRequest,HttpServletRequest request){
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
			ServletContext context = request.getServletContext();
			String appPath = context.getRealPath("/");
			orderDetailsService.orderDeliveredAndReturn(returnOrderRequest,appPath);
			baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			httpStatus=HttpStatus.OK;
		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	@Transactional 	@GetMapping("/fetchOrderDetailsTodaysListByAreaId/{areaId}")
	public ResponseEntity<OrderDetailsList> fetchOrderDetailsTodaysListByAreaId(@RequestHeader("Authorization") String token,@ModelAttribute("areaId") long areaId) {
		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();
		
		


			List<OrderDetails> orderDetailsListtodays = orderDetailsService.fetchOrderDetailsTodaysListByAreaId(areaId);
			if (orderDetailsListtodays == null) {
				httpStatus = HttpStatus.NO_CONTENT;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
			} else {
				orderDetailsList.setOrderDetailsList(orderDetailsListtodays);
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
		

		return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

	}
	
	@Transactional 	@GetMapping("/fetchOrderDetailsPendingListByAreaId/{areaId}")
	public ResponseEntity<OrderDetailsList> fetchOrderDetailsPendingListByAreaId(@RequestHeader("Authorization") String token,
			@ModelAttribute("areaId") long areaId) {
		HttpStatus httpStatus;
		OrderDetailsList orderDetailsList = new OrderDetailsList();
		
		
		
			List<OrderDetails> orderDetailsListPending = orderDetailsService.fetchOrderDetailsPendingListByAreaId(areaId);
			if (orderDetailsListPending == null) {
				httpStatus = HttpStatus.NO_CONTENT;
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
			} else {
				orderDetailsList.setOrderDetailsList(orderDetailsListPending);
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus = HttpStatus.OK;
			}
		

		return new ResponseEntity<OrderDetailsList>(orderDetailsList, httpStatus);

	}
	@Transactional 	@PostMapping("/fetchOrderProductIssueDetailsListReportByEmpIdAndDateRangeAndAreaId")
	public ResponseEntity<OrderProductIssueReportResponse> fetchOrderProductIssueDetailsListByEmpIdAndDateRangeAndAreaId(@RequestHeader("Authorization") String token,
			@RequestBody OrderProductIssueReportRequest orderProductIssueReportRequest) {

		HttpStatus httpStatus;
		OrderProductIssueReportResponse orderProductIssueReportResponse = new OrderProductIssueReportResponse();

	
		
		
			OrderProductIssueReportResponse orderProductIssueDetailsList = orderDetailsService.fetchOrderProductIssueDetailsByEmpIdAndDateRangeAndAreaId(orderProductIssueReportRequest.getEmployeeId(),orderProductIssueReportRequest.getAreaId() , orderProductIssueReportRequest.getFromDate(), orderProductIssueReportRequest.getToDate(), orderProductIssueReportRequest.getRange());
										
			if(orderProductIssueDetailsList.getOrderProductIssueDetailsList()==null)
			{
				orderProductIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
				orderProductIssueReportResponse.setAreaList(orderProductIssueDetailsList.getAreaList());
				httpStatus=HttpStatus.OK;
				orderProductIssueReportResponse.setErrorMsg("No Order Found");
			}
			else
			{
				orderProductIssueReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
				orderProductIssueReportResponse.setAreaList(orderProductIssueDetailsList.getAreaList());
				orderProductIssueReportResponse.setOrderProductIssueDetailsList(orderProductIssueDetailsList.getOrderProductIssueDetailsList());
				httpStatus=HttpStatus.OK;
			}
				
		
		return new ResponseEntity<OrderProductIssueReportResponse>(orderProductIssueReportResponse, httpStatus);

	}
	
	@Transactional 	@GetMapping("/cancelOrderDetails/{orderId}/{employeeId}")
	public ResponseEntity<BaseDomain> cancelOrder (@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId,@ModelAttribute("employeeId") long employeeId){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		
		
		
			String responseText=orderDetailsService.cancelOrder(orderId,employeeId);
			if(responseText.equals("Success"))
			{
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			else
			{
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(responseText);
				httpStatus=HttpStatus.OK;
			}
		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}

	
	@Transactional 	@PostMapping("/reIssue")
	public ResponseEntity<BaseDomain> reIssueOrderDetails(@RequestHeader("Authorization") String token,@RequestBody OrderReIssueRequest orderReIssueRequest){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		
		
		/* check order product quantity with current inventory*/
		Map<Long,Long> productListWithQty=new HashMap<>();
		for(ReIssueOrderProductDetails reIssueOrderProductDetails : orderReIssueRequest.getReIssueOrderProductDetailsList()){
			if(productListWithQty.containsKey(reIssueOrderProductDetails.getProduct().getProduct().getProductId())){
				long qty=productListWithQty.get(reIssueOrderProductDetails.getProduct().getProduct().getProductId())+reIssueOrderProductDetails.getReIssueQuantity();
				productListWithQty.put(reIssueOrderProductDetails.getProduct().getProduct().getProductId(),qty);
			}else{
				productListWithQty.put(reIssueOrderProductDetails.getProduct().getProduct().getProductId(),reIssueOrderProductDetails.getReIssueQuantity());
			}
		}
		
		for(Map.Entry<Long,Long> entry :productListWithQty.entrySet()){
			Product product=productService.fetchProductForWebApp(entry.getKey());
			if(product.getCurrentQuantity()<entry.getValue()){
				
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
				baseDomain.setErrorMsg(product.getProductName()+" qty exceeds Current Qty. Max Available : "+product.getCurrentQuantity());
				httpStatus=HttpStatus.OK;
				
				return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);			
			}
		}
		/*End*/
		
		orderDetailsService.reIssueOrderDetails(orderReIssueRequest);
		baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
		httpStatus=HttpStatus.OK;		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	//fetching IssueProductDetailsList For GateKeeper IssueReport By OrderId
		@Transactional 	@GetMapping("/fetchIssueProductDetailsForIssueReportByOrderId/{orderId}")
		public ResponseEntity<OrderProductIssueListForIssueReportResponse> fetchIssueProductDetailsForIssueReportByOrderId(@RequestHeader("Authorization") String token,@ModelAttribute("orderId") String orderId){
			
			OrderProductIssueListForIssueReportResponse orderProductIssueListForIssueReportResponse=new OrderProductIssueListForIssueReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchIssueProductDetailsForIssueReportByOrderId");
						
				// Fetching orderProductIssueDetails By orderId
				OrderProductIssueDetails orderProductIssueDetails=orderDetailsService.fetchOrderProductIssueListForIssueReportByOrderId(orderId);
				if(orderProductIssueDetails==null)
				{
					orderProductIssueListForIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);	
					orderProductIssueListForIssueReportResponse.setErrorMsg("No Orderdetails");
					httpStatus=HttpStatus.OK;
				}
				orderProductIssueListForIssueReportResponse.setOrderProductIssueDetails(orderProductIssueDetails);
				
				// in below 2 lines we r fetching SalesManNAame and DeliveryBoy Name by getting empId from orderProductIssueDetails 
				EmployeeDetails employeeDetailsSM =employeeDetailsService.getEmployeeDetailsByemployeeId(orderProductIssueDetails.getOrderDetails().getEmployeeSM().getEmployeeId());
				EmployeeDetails employeeDetailsDB =employeeDetailsService.getEmployeeDetailsByemployeeId(orderProductIssueDetails.getEmployeeDB().getEmployeeId());
				
				//here we are fetching OrderProductDetalis By OrderId and making the ProductImage Null
				List<OrderProductDetails> orderProductDetailList=orderDetailsService.fetchOrderProductDetailByOrderIdForApp(orderId);
				//orderProductDetailList=orderDetailsService.makeProductImageMNullorderProductDetailsList(orderProductDetailList);
				 
				if(orderProductDetailList==null)
				{
					orderProductIssueListForIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					orderProductIssueListForIssueReportResponse.setErrorMsg("Order Details not Found");
				    httpStatus=HttpStatus.OK;
				}
				else
				{
					orderProductIssueListForIssueReportResponse.setOrderProductDetailsList(orderProductDetailList);
					orderProductIssueListForIssueReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					orderProductIssueListForIssueReportResponse.setSalesman(employeeDetailsSM.getName());
					orderProductIssueListForIssueReportResponse.setDeliveryBoy(employeeDetailsDB.getName());
					httpStatus=HttpStatus.OK;
				}
				        
	        return new ResponseEntity<OrderProductIssueListForIssueReportResponse>(orderProductIssueListForIssueReportResponse,httpStatus);
			
		}
		
		// here we are fetching OrderDetails For DB Report By DateRange and EmpId And orderStatus 	
		@Transactional 	@PostMapping("/fetchOrderDetailForDBReport")
		public ResponseEntity<OrderProductIssueReportResponse>  fetchOrderDetailForDBReport(@RequestHeader("Authorization") String token,@RequestBody DBReportRequest dBReportRequest){
			
			OrderProductIssueReportResponse orderProductIssueReportResponse=new OrderProductIssueReportResponse();
			HttpStatus httpStatus;
			System.out.println("fetchOrderDetailForDBReport");
						
				List<OrderProductIssueDetails> orderProductIssueDetailsList=orderDetailsService.fetchOrderDetailsForDBReportByDateRangeAndEmpIdOrderStatus
																						   (dBReportRequest.getEmployeeId(),
																							dBReportRequest.getFromDate(),
																							dBReportRequest.getToDate(),
																							dBReportRequest.getRange(),
																							dBReportRequest.getOrderStatus());


				long deliveredCount=0;
				long deliveredPendingCount=0;
				if(orderProductIssueDetailsList==null)
				{
					orderProductIssueReportResponse.setStatus(Constants.FAILURE_RESPONSE);
					orderProductIssueReportResponse.setErrorMsg("No Record Found");
					httpStatus=HttpStatus.OK;
					orderProductIssueReportResponse.setDeliveredCount(deliveredCount);
					orderProductIssueReportResponse.setDeliveredPendingCount(deliveredPendingCount);
				}
				else
				{
					
					for(OrderProductIssueDetails orderProductIssueDetailsList1 : orderProductIssueDetailsList)
					{
						if(orderProductIssueDetailsList1.getOrderDetails().getOrderStatus().getStatus().equals(Constants.ORDER_STATUS_DELIVERED))
						{
							deliveredCount++;
						}
						else
						{
							deliveredPendingCount++;
						}
					}
					orderProductIssueReportResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
					orderProductIssueReportResponse.setOrderProductIssueDetailsList(orderProductIssueDetailsList);
					orderProductIssueReportResponse.setDeliveredCount(deliveredCount);
					orderProductIssueReportResponse.setDeliveredPendingCount(deliveredPendingCount);
				}
			
			return new ResponseEntity<OrderProductIssueReportResponse>(orderProductIssueReportResponse,httpStatus);
		}
		
		
		// sachin 26/10/2017
		
		// here we r fetching OrderDetail for SalesReport BY empId date Range
	@Transactional 	@PostMapping("/fetchOrderDetailsForSalesReportByEmpIdAndDateRange")
	public ResponseEntity<OrderDetailsList> fetchOrderDetailsForSalesReportByEmployeeIdAndDateRange(@RequestHeader("Authorization") String token,@RequestBody CustomerReportRequest customerReportRequest){
		OrderDetailsList orderDetailsList=new OrderDetailsList();
		HttpStatus httpStatus;
		
		
		
		
			List<OrderDetails> orderDetailsList1=orderDetailsService.fetchOrderDetailsByDateRangeAndEmpId(customerReportRequest.getEmployeeId(), customerReportRequest.getFromDate(), customerReportRequest.getToDate(), customerReportRequest.getRange());
						
			if(orderDetailsList1==null)
			{
				orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.OK;
				orderDetailsList.setErrorMsg("No Record found");
			}
			else
			{
				orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
				orderDetailsList.setOrderDetailsList(orderDetailsList1);
				httpStatus=HttpStatus.OK;
			}
		
		return new ResponseEntity<OrderDetailsList>(orderDetailsList,httpStatus);
		
		
	}


	@Transactional 	@GetMapping("/fetchReplacementIssuedOrdersByEmployeeId/{employeeId}/{status}")
	public ResponseEntity<ReIssueOrderDetailsListModel> fetchOrderDetailsForSalesReportByEmployeeIdAndDateRange(@RequestHeader("Authorization") String token,@ModelAttribute("employeeId") long employeeId,@ModelAttribute("status") String status){
		ReIssueOrderDetailsListModel reIssueOrderDetailsListModel=new ReIssueOrderDetailsListModel();
		HttpStatus httpStatus;
		
		
		
	
			List<ReIssueOrderDetails> reIssueOrderDetailsLists=orderDetailsService.fetchReplacementIssuedOrdersByEmployeeId(employeeId,status);
			if(reIssueOrderDetailsLists==null)
			{
				reIssueOrderDetailsListModel.setStatus(Constants.FAILURE_RESPONSE);
				reIssueOrderDetailsListModel.setErrorMsg("No Order Found");
				httpStatus=HttpStatus.OK;
			}
			else
			{
				reIssueOrderDetailsListModel.setReIssueOrderDetailsList(reIssueOrderDetailsLists);
				reIssueOrderDetailsListModel.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
			
		
		return new ResponseEntity<ReIssueOrderDetailsListModel>(reIssueOrderDetailsListModel,httpStatus);
	}
					
					
	//doneReIssueAndOrderStatusDelivered
	@Transactional 	@PostMapping("/doneReIssueAndOrderStatusDelivered")
	public ResponseEntity<BaseDomain> doneReIssueAndOrderStatusDelivered(@RequestHeader("Authorization") String token,@RequestBody ReIssueDelivered reIssueDelivered){
		
		BaseDomain baseDomain=new BaseDomain();
		HttpStatus httpStatus;
		
		
		
			
			String response=orderDetailsService.doneReIssueAndOrderStatusDelivered(reIssueDelivered);
			if(response.equals("Success"))
			{
				httpStatus=HttpStatus.OK;
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
			}
			else
			{
				httpStatus=HttpStatus.FORBIDDEN;
				baseDomain.setStatus(Constants.FAILURE_RESPONSE);
			}
			
		
		
		return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
	}
	
	
	@Transactional 	@GetMapping("/fetchOrderDetailsForReIssueDeliveredByReIssueOrderId/{reIssueOrderId}")
	public ResponseEntity<ReIssueOrderProductDetailsListModel> doneReIssueAndOrderStatusDelivered(@RequestHeader("Authorization") String token,@ModelAttribute("reIssueOrderId") long reIssueOrderId){
		ReIssueOrderProductDetailsListModel reIssueOrderProductDetailsListModel=new ReIssueOrderProductDetailsListModel();
		HttpStatus httpStatus;
		
		
		
		
			reIssueOrderProductDetailsListModel=orderDetailsService.fetchOrderDetailsForReIssueDelivedByOrderId(reIssueOrderId);
			if(reIssueOrderProductDetailsListModel.getReIssueOrderProductDetailsList()==null)
			{
				reIssueOrderProductDetailsListModel.setStatus(Constants.FAILURE_RESPONSE);
				httpStatus=HttpStatus.NO_CONTENT;
			}
			else
			{
				reIssueOrderProductDetailsListModel.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
			}
		
		return new ResponseEntity<ReIssueOrderProductDetailsListModel>(reIssueOrderProductDetailsListModel,httpStatus);
	}
	
	// here we are fetching OrderDetail for TotalCollection Report ByBusinessNameIdEmployeeId
		@Transactional 	@PostMapping("/fetchOrderDetailByBusinessNameIdEmployeeId")
		public ResponseEntity<OrderDetailsPaymentListByBusinessName> fetchOrderDetailForTotalCollectionReportByBusinessNameIdEmployeeId(@RequestHeader("Authorization") String token,@RequestBody OrderDetailByBusinessNameIdEmployeeIdRequest orderDetailByBusinessNameIdEmployeeIdRequest){
			OrderDetailsPaymentListByBusinessName orderDetailsPaymentListByBusinessName =new OrderDetailsPaymentListByBusinessName();
			HttpStatus httpStatus;
			
		
			
			System.out.println("fetchOrderDetailForTotalCollectionReportByBusinessNameIdEmployeeId");
			
			EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(orderDetailByBusinessNameIdEmployeeIdRequest.getEmployeeId());
			List<OrderDetailsPaymentList> orderDetailsPaymentList=orderDetailsService.fetchOrderDetailForTotalCollectionReportByBusinessNameId(orderDetailByBusinessNameIdEmployeeIdRequest);
			if(orderDetailsPaymentList==null){
				orderDetailsPaymentListByBusinessName.setStatus(Constants.FAILURE_RESPONSE);
				orderDetailsPaymentListByBusinessName.setErrorMsg(Constants.NO_CONTENT);
				httpStatus=HttpStatus.NO_CONTENT;
				}
			else{
				orderDetailsPaymentListByBusinessName.setStatus(Constants.SUCCESS_RESPONSE);
				orderDetailsPaymentListByBusinessName.setErrorMsg(Constants.NONE);
				orderDetailsPaymentListByBusinessName.setOrderDetailsPaymentList(orderDetailsPaymentList);
				orderDetailsPaymentListByBusinessName.setBusinessName(businessNameService.fetchBusinessForWebApp(orderDetailByBusinessNameIdEmployeeIdRequest.getBusinessNameId()));
				orderDetailsPaymentListByBusinessName.setSalesManName(employeeDetails.getName());
				httpStatus=HttpStatus.OK;
				}
			
			return new ResponseEntity<OrderDetailsPaymentListByBusinessName>(orderDetailsPaymentListByBusinessName,httpStatus);
		}
		
		@Transactional 	@PostMapping("fetchCancelOrderByEmployeeId")
		ResponseEntity<OrderDetailsList> fetchCancelReport(@RequestHeader("Authorization") String token,@RequestBody InventoryReportRequest inventoryReportRequest){
			
			OrderDetailsList orderDetailsList=new OrderDetailsList();
			HttpStatus httpStatus;
			
		
			
			List<OrderDetails> list=new ArrayList<>();
			
				list=orderDetailsService.fetchCancelOrderReportByEmployeeId(inventoryReportRequest.getEmployeeId(),
						inventoryReportRequest.getFromDate(), inventoryReportRequest.getToDate(), inventoryReportRequest.getRange());
				
				if(list==null){
					orderDetailsList.setStatus(Constants.FAILURE_RESPONSE);
					orderDetailsList.setErrorMsg("Records Not Found");
					httpStatus=HttpStatus.OK;
				}else{
					orderDetailsList.setOrderDetailsList(list);
					orderDetailsList.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
				}
				
			
			
			return new ResponseEntity<OrderDetailsList>(orderDetailsList,httpStatus);
		}
		
		
		@Transactional 	@GetMapping("/fetchReturnOrderFromDeliveryBoyReportForApp/{range}")
		ResponseEntity<ReturnOrderFromDeliveryBoyReportModel> orderFromDeliveryBoyReport(@RequestHeader("Authorization") String token,@ModelAttribute("range") String range){
			
			ReturnOrderFromDeliveryBoyReportModel returnOrderFromDeliveryBoyReportModel=new ReturnOrderFromDeliveryBoyReportModel();
			HttpStatus httpStatus;
			
			
			
			
				String startDate="";
				String endDate="";
				List<ReturnOrderFromDeliveryBoyReport>  returnOrderFromDeliveryBoyReportList=orderDetailsService.fetchReturnOrderFromDeliveryBoyReport(range, startDate, endDate);
				
				if(returnOrderFromDeliveryBoyReportList.isEmpty()){
					returnOrderFromDeliveryBoyReportModel.setStatus(Constants.FAILURE_RESPONSE);
					returnOrderFromDeliveryBoyReportModel.setErrorMsg(Constants.NOT_FOUND);
					httpStatus=HttpStatus.OK;
				}else{
					returnOrderFromDeliveryBoyReportModel.setReturnOrderFromDeliveryBoyReportList(returnOrderFromDeliveryBoyReportList);
					returnOrderFromDeliveryBoyReportModel.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
				}
				
			
			
			return new ResponseEntity<ReturnOrderFromDeliveryBoyReportModel>(returnOrderFromDeliveryBoyReportModel,httpStatus);
		}
		
		@Transactional 	@GetMapping("/fetchReturnFromDeliveryBoyForApp/{returnFromDeliveryBoyMainId}")
		ResponseEntity<ReturnOrderFromDeliveryBoyReportModel> fetchReturnFromDeliveryBoyForApp(@RequestHeader("Authorization") String token,@ModelAttribute("returnFromDeliveryBoyMainId") String returnFromDeliveryBoyMainId){
			
			ReturnOrderFromDeliveryBoyReportModel returnOrderFromDeliveryBoyReportModel=new ReturnOrderFromDeliveryBoyReportModel();
			HttpStatus httpStatus;
			
				List<ReturnFromDeliveryBoy>  returnFromDeliveryBoyList=orderDetailsService.fetchReturnFromDeliveryBoyList(returnFromDeliveryBoyMainId);
				
				if(returnFromDeliveryBoyList==null){
					returnOrderFromDeliveryBoyReportModel.setErrorMsg(Constants.NOT_FOUND);
					returnOrderFromDeliveryBoyReportModel.setStatus(Constants.FAILURE_RESPONSE);
					httpStatus=HttpStatus.OK;
				}else{
					EmployeeDetails employeeDetails=employeeDetailsService.getEmployeeDetailsByemployeeId(returnFromDeliveryBoyList.get(0).getReturnFromDeliveryBoyMain().getOrderDetails().getEmployeeSM().getEmployeeId());
					returnOrderFromDeliveryBoyReportModel.setEmployeeDetails(employeeDetails);
					returnOrderFromDeliveryBoyReportModel.setReturnFromDeliveryBoyList(returnFromDeliveryBoyList);
					returnOrderFromDeliveryBoyReportModel.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
				}
				
			
			
			return new ResponseEntity<ReturnOrderFromDeliveryBoyReportModel>(returnOrderFromDeliveryBoyReportModel,httpStatus);
		}
		
		@Transactional 	@PostMapping("/updateReturnFromDeliveryBoy")
		ResponseEntity<BaseDomain> updateReturnFromDeliveryBoy(@RequestHeader("Authorization") String token,@RequestBody ReturnOrderFromDeliveryBoyReportModel returnOrderFromDeliveryBoyReportModel){
			
			BaseDomain baseDomain=new BaseDomain();
			HttpStatus httpStatus;
			
			
			
			
				orderDetailsService.updateReturnFromDeliveryBoyForApp(returnOrderFromDeliveryBoyReportModel.getReturnFromDeliveryBoyList());
				
				baseDomain.setStatus(Constants.SUCCESS_RESPONSE);
				httpStatus=HttpStatus.OK;
				
			
			
			return new ResponseEntity<BaseDomain>(baseDomain,httpStatus);
		}
		
		@Transactional 	@GetMapping("/fetchSnapShotForGK")
		public ResponseEntity<GkSnapProductListMainResponse> fetchSnapShotForGK(@RequestHeader("Authorization") String token){
			
			GkSnapProductListMainResponse gkSnapProductListMainResponse=new GkSnapProductListMainResponse();
			HttpStatus httpStatus;
			System.out.println("Inside fetchSnapShotForGK");
		
				List<GkSnapProductResponse> gkSnapProductResponses=orderDetailsService.fetchSnapProductDetailsForGk();
				if(gkSnapProductResponses==null){
					gkSnapProductListMainResponse.setStatus(Constants.FAILURE_RESPONSE);
					httpStatus=HttpStatus.OK;
					gkSnapProductListMainResponse.setErrorMsg("No Record Found");
					
				}else{
					gkSnapProductListMainResponse.setStatus(Constants.SUCCESS_RESPONSE);
					httpStatus=HttpStatus.OK;
					gkSnapProductListMainResponse.setGkSnapProductResponses(gkSnapProductResponses);
				}
			return new ResponseEntity<GkSnapProductListMainResponse>(gkSnapProductListMainResponse,httpStatus);
		}
		
		/*@PostMapping("/sendEmail")
	    public ResponseEntity<?> uploadFileMulti(HttpServletRequest request,HttpServletResponse response,
	    		@RequestParam("businessNameId") String businessNameId,
	    		@RequestParam("fileName") String fileName,
	            @RequestParam("files") MultipartFile[] uploadfiles) {

	        try {

	        	BusinessName businessName=businessNameService.fetchBusinessForWebApp(businessNameId);
	        	if(businessName.getContact().getEmailId()==null)
	        	{
	        		return new ResponseEntity("Email-Id not found",HttpStatus.BAD_REQUEST);
	        	}
	        // Get file name
	        String uploadedFileName = uploadfiles[0].getOriginalFilename();

	        if (StringUtils.isEmpty(uploadedFileName)) {
	            return new ResponseEntity("please select a file!", HttpStatus.OK);
	        }

	        for (MultipartFile file : uploadfiles) {

	            if (file.isEmpty()) {
	                continue; //next pls
	            }

	            byte[] bytes = file.getBytes();
	            
	            Path path = Paths.get(request.getContextPath()+"/" + file.getOriginalFilename());
	            Files.write(path, bytes);
	            String filePath="/resources/"+fileName;
		    	
		    	ServletContext context = request.getServletContext();
		        String appPath = context.getRealPath("");
		        
		 
		        // construct the complete absolute path of the file
		        String fullPath = appPath + filePath;
		        System.out.println("fullPath = " + fullPath);
		        File fileSend=new File(fullPath);
		        FileOutputStream fop = new FileOutputStream(fileSend);
		        
		        byte[] contentInBytes = file.getBytes();

				fop.write(contentInBytes);
				fop.flush();
				fop.close();
				
				EmailSender emailSender=new EmailSender(mailSender, sessionFactory);
				emailSender.sendEmail("Order Invoice", " ", businessName.getContact().getEmailId(), true, fileSend,fileName);
	        }
	        

	        return new ResponseEntity("Successfully Mail Send - "+ fileName, HttpStatus.OK);
	            //saveUploadedFiles(Arrays.asList(uploadfiles));

	        } catch (Exception e) {
	            return new ResponseEntity("Failed To Send Mail",HttpStatus.BAD_REQUEST);
	        }

	        

	    }*/
}
