package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.models.BusinessNameList;
import com.bluesquare.rc.models.CustomerReportView;

public interface BusinessNameService {
	public void saveBusinessName(BusinessName businessName);
	public List<BusinessName> businessNameByAreaIdAndBusinessTypeIdForWebApp(long businessTypeId, long areaId);
	//webapp
		public void saveForWebApp(BusinessName businessName);

		public void updateForWebApp(BusinessName businessName);

		public List<BusinessName> getBusinessNameListForWebApp();
		
		public BusinessName fetchBusinessForWebApp(String businessNameId);
		
		public List<BusinessNameList> fetchBusinessNameList();
		public String sendSMSTOShops(String shopsId,String smsText,String mobileNumber);
		public List<BusinessName> fetchBusinessNameByAreaId(long areaId);
		public CustomerReportView fetchBusinessNameForReport(String range,String startDate,String endDate);
		
		public String checkBusinessDuplication(String checkText,String type,String businessNameId);
}
