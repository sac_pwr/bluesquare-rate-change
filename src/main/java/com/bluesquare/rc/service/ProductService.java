package com.bluesquare.rc.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.DailyStockDetails;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.DamageRecoveryMonthWise;
import com.bluesquare.rc.entities.DamageRecoveryDetails;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.ProductReportView;
import com.bluesquare.rc.models.ProductViewList;
import com.bluesquare.rc.rest.models.BrandAndCategoryRequest;
import com.bluesquare.rc.rest.models.ProductAddInventory;

public interface ProductService {
	public void saveProductForWebApp(MultipartFile file,Product Product);
	public void updateProductForWebApp(MultipartFile file,Product Product);
	public void updateProductForWebApp(Product product);
	public Product fetchProductForWebApp(long ProductId);
	public Product fetchProductForImage(long productId,long companyId);
	public Product setNoImageToProductForImage(Product product);
	public ProductViewList fetchProductViewListForWebApp(long ProductId);
	public List<Product> fetchProductListForWebApp();
	public List<ProductViewList> fetchProductViewListForWebApp();
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(long categoryId,long brandId);
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(String supplierId,long categoryId, long brandId);
	//public List<Product> makeProductImageNull(List<Product> productList);
	//public List<SupplierProductList> makeSupplierProductImageNull(List<SupplierProductList> supplierProductLists);
	public List<Product> fetchProductListBySupplierId(String supplierId);
	public List<ProductAddInventory> fetchProductList();
	public List<ProductAddInventory> fetchProductForAddInventory();
	public List<ProductAddInventory> fetchProductByBrandAndCategory(BrandAndCategoryRequest brandAndCategoryRequest );
	public List<ProductAddInventory> fetchProductListByBrandIdAndCategoryIdForApp(String supplierId,long categoryId, long brandId);
	public List<ProductReportView> fetchProductListForReport(String range,String startDate,String endDate,String topProductNo);
	public  List<Product> fetchProductByCategoryIdForWebApp(long categoryId);
	public CalculateProperTaxModel calculateProperAmountModel(double mrp,double igstPer);
	public List<DailyStockDetails> fetchDailyStockDetails(String pickDate,long companyId);
	public void saveDamageDefine(DamageDefine damageDefine);
	public List<DamageDefine> fetchDamageDefineList(String range,String startDate,String endDate);
	
	//Damage Product recovery functions
	public List<DamageRecoveryMonthWise> fetchDamageRecoveryDayWise(String startMonth,String startYear,String endMonth,String endYear);
	public DamageRecoveryMonthWise saveUpdateDamageRecoveryDayWise(long productId,long damageQuantity);
	public List<DamageRecoveryDetails> fetchDamageRecoveryDetailsListByDamageRecoveryId(long damageRecoveryId);
	public DamageRecoveryDetails fetchDamageRecoveryDetailsByDamageRecoveryDetailsId(String damageRecoveryDetailsId);
	public void updateDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails);
	public void saveDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails,long damageRecoveryId);
	public DamageRecoveryMonthWise fetchDamageRecoveryByProductId(long productId);
	public DamageRecoveryMonthWise fetchDamageRecoveryByDamageRecoveryId(long damageRecoveryId);
	public DamageRecoveryMonthWise saveUpdateDamageRecoveryMonthWise(long productId,long damageQuantity);
}
