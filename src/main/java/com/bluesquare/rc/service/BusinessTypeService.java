package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.BusinessType;

public interface BusinessTypeService {

	//webApp
		public void saveForWebApp(BusinessType businessType);

		public void updateForWebApp(BusinessType businessType);
		
		public List<BusinessType> fetchBusinessTypeListForWebApp();
		
		public BusinessType fetchBusinessTypeForWebApp(long businessTypeId);
}
