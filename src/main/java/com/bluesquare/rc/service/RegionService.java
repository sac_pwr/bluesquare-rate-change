package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Region;

public interface RegionService {
	//webapp
		public void saveForWebApp(Region region);

		public void updateForWebApp(Region region);
		
		public List<Region> fetchAllRegionForWebApp();

		public List<Region> fetchSpecifcRegionsForWebApp(long cityId);
		//public List<Region> fetchSpecifcRegionsBranchConfig(long cityId) ;
		
		public Region fetchRegionForWebApp(long region);
		
}
