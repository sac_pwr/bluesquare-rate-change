package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CompanyCities;

public interface CompanyService { 
	public void saveCompany(Company company,String cityIdList);
	public void updateCompany(Company company,String cityIdList);
	public void updateCompany(Company company);
	public List<CompanyCities> fetchCompanyCities(long companyId);
	public Company fetchCompanyByCompanyId(long companyId);
	public List<Company> fetchAllCompany();
/*	public Company checkCompanyName(String companyName);
	public Company checkCompanyNameForUpdate(String companyName,long companyId);
	public Company checkCompanyUserName(String userName);
	public Company checkCompanyUserNameForUpdate(String userName,long companyId);
	public Company checkCompanyGstIn(String gstinno);
	public Company checkCompanyGstInForUpdate(String gstinno,long companyId);*/
	public String checkDuplication(String checkText,String type,long companyId);
	public String sendSMSTOCompanies(String companiesId,String smsText,String mobileNumber);
	public String sendOTPToCompanyUsingMailAndSMS(String emailIdAndMobileNumber,boolean isMobileNumber);
	public Company validateCompany(String username, String password);
}
