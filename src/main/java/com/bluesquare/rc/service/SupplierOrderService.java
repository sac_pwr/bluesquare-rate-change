package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.SupplierOrder;
import com.bluesquare.rc.entities.SupplierOrderDetails;

public interface SupplierOrderService {
	public void saveSupplierOrder(String productWithSupplier);
	public SupplierOrder fetchSupplierOrder(String supplierOrderId);
	public List<SupplierOrder> fetchSupplierOrderList(String range,String startDate,String endDate);
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsList(String supplierOrderId);
	public void editSupplierOrder(String productWithSupplier, SupplierOrder supplierOrder);
	public void deleteSupplierOrder(String supplierOrderId);
}
