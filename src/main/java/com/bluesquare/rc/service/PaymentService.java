package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.Payment;
import com.bluesquare.rc.models.ChequePaymentReportModel;
import com.bluesquare.rc.models.CollectionReportMain;
import com.bluesquare.rc.models.LedgerPaymentView;
import com.bluesquare.rc.models.PaymentListReport;
import com.bluesquare.rc.models.PaymentPendingList;
import com.bluesquare.rc.models.PaymentReportModel;
import com.bluesquare.rc.rest.models.CollectionReportResponse;
import com.bluesquare.rc.rest.models.PaymentListModel;
import com.bluesquare.rc.rest.models.PaymentTakeRequest;

public interface PaymentService {
	public OrderDetails savePaymentStatus(PaymentTakeRequest paymentTakeRequest,OrderDetails orderDetails);
	public List<PaymentPendingList> fetchPaymentPendingList();
	/*public List<LedgerPaymentView> fetchLedgerPaymentView(String startDate, String endDate,
			String range);*/
	public CollectionReportMain getCollectionReportDetails(String startDate, String endDate,String range);
	//public CollectionReportResponse fetchCollectionDetailsByOrderId(String orderId);
	public List<PaymentListReport> fetchPaymentListByOrderDetailsIdForCollectionReport(String orderDetailsId);
	public PaymentListModel fetchPaymentListByOrderIdForApp(String orderId);
	
	public void updatePayment(Payment payment);
	public void resetDueAmountOfPaymentList(String orderId);
	public List<PaymentReportModel> fetchPaymentReportModelList(String startDate,String endDate,String range);
	public List<ChequePaymentReportModel> fetchChequePaymentReportModelList(String startDate,String endDate,String range);
	public Payment fetchPaymentBYPaymentId(long paymentId);
	public List<Payment> fetchPaymentList(String startDate,String endDate,String range);
	public void deletePayment(long paymentId);
	public void defineChequeBounced(long paymentId);
	public List<Payment> fetchPaymentListByOrderDetailsId(String orderDetailsId);
}
