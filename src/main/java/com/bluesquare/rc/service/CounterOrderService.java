package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CounterOrderReport;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.PaymentCounterReport;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.rest.models.OrderReportList;

public interface CounterOrderService {
	public String saveCounterOrder(String counterOrderProductDetails,
			String businessNameId,String paidAmount,String balAmount,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo);
	public BillPrintDataModel fetchCounterBillPrintData(String counterOrderId);
	public List<OrderReportList> showCounterOrderReportByBusinessNameId(String businessNameId,String range,String startDate,String endDate);
	public CounterOrder fetchCounterOrder(String counterId);
	public List<CounterOrder> fetchCounterOrderByRange(String businessNameId,String range,String startDate,String endDate);
	public List<OrderProductDetailListForWebApp> fetchCounterOrderProductDetailsForShowOrderDetails(String counterId);
	public List<PaymentCounter> fetchPaymentCounterListByCounterOrderId(String counterOrderId);
	public List<PaymentCounterReport> fetchPaymentCounterReportListByCounterOrderId(String counterOrderId);
	public List<CounterOrderReport> fetchCounterOrderReport(String range,String startDate,String endDate);
	public PaymentDoInfo fetchPaymentInfoByCounterOrderId(String counterOrderId);
	public void savePaymentCounter(PaymentCounter paymentCounter);
	public void updateCounterOrder(CounterOrder counterOrder);
	public void deleteCounterOrder(String counterOrderId);
	public List<CounterOrderProductDetails> fetchCounterOrderProductDetails(String counterId);
	public String updateCounterOrderForEdit(String counterOrderId,String roductDetailsList,
			String businessNameId,String paidAmount,String balAmount,String refAmount,String paymentSituation,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo);
	public void deletePayment(long paymentId);
	public void defineChequeBounced(long paymentId);
	public void updatePayment(PaymentCounter paymentCounter);
	public PaymentCounter fetchPaymentCounterByPaymentCounterId(long paymentCounterId);
	public double totalSaleAmountForProfitAndLoss(String startDate,String endDate);
}
