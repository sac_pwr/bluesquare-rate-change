package com.bluesquare.rc.service;

import com.bluesquare.rc.entities.Employee;
import com.bluesquare.rc.entities.EmployeeSalary;

public interface EmployeeService {

	// webApp

	public void saveForWebApp(Employee employee);

	public void updateForWebApp(Employee employee);

	public Employee loginCredentialsForRC(String userid, String password);

	public void logout(long employeeId);

	public Employee validate(String username, String password);

	public String checkAppVersion(String appVersion);

	public String checkEmployeeDuplication(String checkText, String type, long employeeDetailsId);
}
