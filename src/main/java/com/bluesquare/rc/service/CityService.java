package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.City;

public interface CityService {

	public List<City> fetchAllCityForWebApp();
	
	public void saveForWebApp(City city);

	public void updateForWebApp(City city);
	
	public City fetchCityForWebApp(long cityId);
	public List<City> fetchCityByStateIdForWebApp(long stateId);
	//public List<City> fetchCityByStateIdForWebAppBranchConfig(long stateId);
}
