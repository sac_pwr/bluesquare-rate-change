package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CompanyDAO;
import com.bluesquare.rc.entities.City;
import com.bluesquare.rc.entities.Company;
import com.bluesquare.rc.entities.CompanyCities;
import com.bluesquare.rc.service.CompanyService;

@Component
@Transactional
@Service("CompanyService")
@Qualifier("CompanyService")
public class CompanyServiceImpl implements CompanyService{

	@Autowired
	CompanyDAO companyDAO; 
	
	@Override
	public void saveCompany(Company company,String cityIdList) {
		// TODO Auto-generated method stub
		companyDAO.saveCompany(company,cityIdList);
	}

	@Override
	public void updateCompany(Company company,String cityIdList) {
		// TODO Auto-generated method stub
		companyDAO.updateCompany(company,cityIdList);
	}

	@Override
	public Company fetchCompanyByCompanyId(long companyId) {
		// TODO Auto-generated method stub
		return companyDAO.fetchCompanyByCompanyId(companyId);
	}

	@Override
	public List<Company> fetchAllCompany() {
		// TODO Auto-generated method stub
		return companyDAO.fetchAllCompany();
	}

	/*@Override
	public Company checkCompanyName(String companyName) {
		// TODO Auto-generated method stub
		return companyDAO.checkCompanyName(companyName);
	}

	@Override
	public Company checkCompanyNameForUpdate(String companyName, long companyId) {
		// TODO Auto-generated method stub
		return companyDAO.checkCompanyNameForUpdate(companyName, companyId);
	}

	@Override
	public Company checkCompanyUserName(String userName) {
		// TODO Auto-generated method stub
		return companyDAO.checkCompanyUserName(userName);
	}

	@Override
	public Company checkCompanyUserNameForUpdate(String userName, long companyId) {
		// TODO Auto-generated method stub
		return companyDAO.checkCompanyUserNameForUpdate(userName, companyId);
	}*/

	@Override
	public String sendSMSTOCompanies(String companiesId, String smsText,String mobileNumber) {
		// TODO Auto-generated method stub
		return companyDAO.sendSMSTOCompanies(companiesId, smsText,mobileNumber);
	}

	@Override
	public Company validateCompany(String username, String password) {
		// TODO Auto-generated method stub
		return companyDAO.validateCompany(username, password);
	}

	@Override
	public List<CompanyCities> fetchCompanyCities(long companyId) {
		// TODO Auto-generated method stub
		return companyDAO.fetchCompnyCities(companyId);
	}

	@Override
	public String sendOTPToCompanyUsingMailAndSMS(String emailIdAndMobileNumber, boolean isMobileNumber) {
		// TODO Auto-generated method stub
		return companyDAO.sendOTPToCompanyUsingMailAndSMS(emailIdAndMobileNumber, isMobileNumber);
	}

	@Override
	public void updateCompany(Company company) {
		// TODO Auto-generated method stub
		companyDAO.updateCompany(company);
	}

	@Override
	public String checkDuplication(String checkText, String type, long companyId) {
		// TODO Auto-generated method stub
		return companyDAO.checkDuplication(checkText, type, companyId);
	}

	/*@Override
	public Company checkCompanyGstIn(String gstinno) {
		// TODO Auto-generated method stub
		return companyDAO.checkCompanyGstIn(gstinno);
	}

	@Override
	public Company checkCompanyGstInForUpdate(String gstinno, long companyId) {
		// TODO Auto-generated method stub
		return companyDAO.checkCompanyGstInForUpdate(gstinno, companyId);
	}*/

}
