package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.LedgerDAO;
import com.bluesquare.rc.entities.Ledger;
import com.bluesquare.rc.models.LedgerPaymentView;
import com.bluesquare.rc.service.LedgerService;

@Component
@Transactional
@Service("ledgerService")
@Qualifier("ledgerService")
public class LedgerServiceImpl implements LedgerService{

	@Autowired
	LedgerDAO ledgerDAO;
	


	@Override
	public List<LedgerPaymentView> fetchLedgerPaymentView(String startDate, String endDate, String range) {
		// TODO Auto-generated method stub
		return ledgerDAO.fetchLedgerPaymentView(startDate, endDate, range);
	}

}
