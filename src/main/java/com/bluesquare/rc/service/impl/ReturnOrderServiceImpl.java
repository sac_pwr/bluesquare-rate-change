package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.ReturnOrderDAO;
import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;
import com.bluesquare.rc.models.ChartDetailsResponse;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;
import com.bluesquare.rc.models.ReIssueOrderReport;
import com.bluesquare.rc.models.ReturnItemReportMain;
import com.bluesquare.rc.rest.models.GkReturnReportResponse;
import com.bluesquare.rc.rest.models.ReturnOrderDateRangeResponse;
import com.bluesquare.rc.rest.models.ReturnOrderRequest;
import com.bluesquare.rc.service.ReturnOrderService;


@Component
@Transactional
@Service("returnOrderService")
@Qualifier("returnOrderService")
public class ReturnOrderServiceImpl implements ReturnOrderService {

	@Autowired
	ReturnOrderDAO returnOrderDAO;
	@Override
	public void update(ReturnOrderProduct returnOrderProduct) {
		
		
		// TODO Auto-generated method stu
		returnOrderDAO.update(returnOrderProduct);
	}
	@Override
	public void save(ReturnOrderRequest returnOrderRequest) {
		// TODO Auto-generated method stub
		
		returnOrderDAO.save(returnOrderRequest);
		
	}
	@Override
	public List<ReturnOrderProduct> fetchReturnOrderDetailsListByDateRange(long employeeId, String fromDate,
			String toDate, String range) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderDetailsListByDateRange(employeeId, fromDate, toDate, range);
	}
	@Override
	public List<ReturnOrderProductDetails> fetchReturnOrderProductDetailsByReturnOrderProductId(
			String returnOrderProductId) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderProductDetailsByReturnOrderProductId(returnOrderProductId);
	}
	/*@Override
	public List<ReturnOrderProductDetails> makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(
			List<ReturnOrderProductDetails> returnOrderProductDetails) {
		// TODO Auto-generated method stub
		return returnOrderDAO.makeProductsImageNullReturnOrderProductDetailsWithReturnOrderProductId(returnOrderProductDetails);
	}*/
	
	@Override
    public List<ReturnOrderProduct> fetchReturnOrderProductListForDBReportByEmpIdDateRange(long employeeId,
            String fromDate, String toDate, String range) {
        // TODO Auto-generated method stub
        return returnOrderDAO.fetchReturnOrderProductListForDBReportByEmpIdDateRange(employeeId, fromDate, toDate, range);
    }
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReturnOrderForGKReportByReturnOrderProductId(java.lang.String)
	 * Oct 24, 201711:15:38 AM
	 */
	@Override
	public ReturnOrderProduct fetchReturnOrderForGKReportByReturnOrderProductId(String returnOrderProductId) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderForGKReportByReturnOrderProductId(returnOrderProductId);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(java.lang.String)
	 * Oct 24, 201711:17:23 AM
	 */
	@Override
	public List<ReturnOrderProductDetails> fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(
			String returnOrderProductId) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderProductDetailsListForGKReportByReturnOrderProductId(returnOrderProductId);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId(long, long, java.lang.String, java.lang.String, java.lang.String)
	 * Oct 24, 201711:36:15 AM
	 */
	@Override
	public GkReturnReportResponse fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId(long employeeId,
			long areaId, String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderDetailsForGkReportByEmpIdDateRangeAndAreaId(employeeId, areaId, fromDate, toDate, range);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReturnOrderProductListByFilter(long, java.lang.String, java.lang.String, java.lang.String, long)
	 * Oct 27, 20171:04:20 PM
	 */
	@Override
	public ReturnOrderDateRangeResponse fetchReturnOrderProductListByFilter(long employeeId, String range,
			String fromDate, String toDate, long areaId) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderProductListByFilter(employeeId, range, fromDate, toDate, areaId);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReturnOrderProductByReIssueStatus(java.lang.String)
	 * Oct 31, 201711:47:17 AM
	 */
	@Override
	public ReturnOrderProduct fetchReturnOrderProductByReIssueStatus(String orderId) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderProductByReIssueStatus(orderId);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchTopReturnOrderProducts()
	 * Oct 31, 20173:30:14 PM
	 */
	@Override
	public ChartDetailsResponse fetchTopReturnOrderProducts(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchTopReturnOrderProducts(range, startDate, endDate);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReIssueOrderDetailsForReplacementReportByEmpIdAndDateRange(long, java.lang.String, java.lang.String, java.lang.String)
	 * Oct 31, 20175:05:34 PM
	 */
	@Override
	public List<ReIssueOrderDetails> fetchReIssueOrderDetailsForReplacementReportByEmpIdAndDateRange(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReIssueOrderDetailsForReplacementReportByEmpIdAndDateRange(employeeId, fromDate, toDate, range);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReIssueOrderDetailsForReplacementReportByOrderId(java.lang.String)
	 * Oct 31, 20175:05:34 PM
	 */
	@Override
	public ReIssueOrderDetails fetchReIssueOrderDetailsForReplacementReportByOrderId(long reIssueOrderId) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReIssueOrderDetailsForReplacementReportByOrderId(reIssueOrderId);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReIssueOrderProductDetailsForReplacementReportByOrderId(java.lang.String)
	 * Oct 31, 20175:05:34 PM
	 */
	@Override
	public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsForReplacementReportByOrderId(
			String orderId) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReIssueOrderProductDetailsForReplacementReportByOrderId(orderId);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRange(long, java.lang.String, java.lang.String, java.lang.String)
	 * Oct 31, 20175:05:34 PM
	 */
	@Override
	public List<ReIssueOrderDetailsReport> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRange(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRange(employeeId, fromDate, toDate, range);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(java.util.List)
	 * Oct 31, 20175:05:34 PM
	 */
	/*@Override
	public List<ReIssueOrderProductDetails> makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(
			List<ReIssueOrderProductDetails> returnOrderProductDetails) {
		// TODO Auto-generated method stub
		return returnOrderDAO.makeProductsImageNullReturnOrderProductDetailsWithReplacementReturnOrderProductId(returnOrderProductDetails);
	}*/
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReturnOrderProductList()
	 * Nov 2, 20175:26:06 PM
	 */
	@Override
	public List<ReturnItemReportMain> fetchReturnOrderProductList(String filter,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderProductList(filter,startDate,endDate);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReIssueOrderReportList()
	 * Nov 2, 20178:25:49 PM
	 */
	@Override
	public List<ReIssueOrderReport> fetchReIssueOrderReportList() {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReIssueOrderReportList();
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeApp(long, java.lang.String, java.lang.String, java.lang.String)
	 * Dec 22, 20177:31:29 PM
	 */
	@Override
	public List<ReIssueOrderDetails> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeApp(long employeeId,
			String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeApp(employeeId, fromDate, toDate, range);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchReturnOrderProductListByFilterWeb(long, java.lang.String, java.lang.String, java.lang.String)
	 * Dec 29, 20173:05:32 PM
	 */
	@Override
	public ReturnOrderDateRangeResponse fetchReturnOrderProductListByFilterWeb(long employeeId, String range,
			String fromDate, String toDate) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReturnOrderProductListByFilterWeb(employeeId, range, fromDate, toDate);
	}
	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ReturnOrderService#fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeWeb(long, java.lang.String, java.lang.String, java.lang.String)
	 * Dec 29, 20173:33:23 PM
	 */
	@Override
	public List<ReIssueOrderDetailsReport> fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeWeb(
			long employeeId, String fromDate, String toDate, String range) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchOrderDetailsForGKReplacementReportByEmpIdAndDateRangeWeb(employeeId, fromDate, toDate, range);
	}
	@Override
	public List<ReIssueOrderProductDetails> fetchReIssueOrderProductDetailsForReplacementReportByReIssueOrderId(
			long reIssueOrderId) {
		// TODO Auto-generated method stub
		return returnOrderDAO.fetchReIssueOrderProductDetailsForReplacementReportByReIssueOrderId(reIssueOrderId);
	}

}
