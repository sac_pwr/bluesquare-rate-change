package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.bluesquare.rc.dao.ProductDAO;
import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.DailyStockDetails;
import com.bluesquare.rc.entities.DamageDefine;
import com.bluesquare.rc.entities.DamageRecoveryMonthWise;
import com.bluesquare.rc.entities.DamageRecoveryDetails;
import com.bluesquare.rc.entities.Product;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.CalculateProperTaxModel;
import com.bluesquare.rc.models.ProductReportView;
import com.bluesquare.rc.models.ProductViewList;
import com.bluesquare.rc.rest.models.BrandAndCategoryRequest;
import com.bluesquare.rc.rest.models.ProductAddInventory;
import com.bluesquare.rc.service.ProductService;

@Component
@Transactional
@Service("productService")
@Qualifier("productService")
public class ProductServiceimpl implements ProductService {

	@Autowired
	ProductDAO productDAO;
	
	@Override
	public void saveProductForWebApp(MultipartFile file,Product Product) {
		// TODO Auto-generated method stub
		productDAO.saveProductForWebApp(file,Product);
	}

	@Override
	public void updateProductForWebApp(MultipartFile file,Product Product) {
		// TODO Auto-generated method stub
		productDAO.updateProductForWebApp(file,Product);
	}

	@Override
	public Product fetchProductForWebApp(long ProductId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductForWebApp(ProductId);
	}

	@Override
	public Product fetchProductForImage(long productId,long companyId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductForImage(productId,companyId);
	}
	
	@Override
	public List<Product> fetchProductListForWebApp() {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListForWebApp();
	}

	@Override
	public List<ProductViewList> fetchProductViewListForWebApp() {
		// TODO Auto-generated method stub
		return productDAO.fetchProductViewListForWebApp();
	}

	//@Override
	public ProductViewList fetchProductViewListForWebApp(long ProductId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductViewListForWebApp(ProductId);
	}

	@Override
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(long categoryId, long brandId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListByBrandIdAndCategoryIdForWebApp(categoryId, brandId);
	}

	/*@Override
	public List<Product> makeProductImageNull(List<Product> productList) {
		// TODO Auto-generated method stub
		return productDAO.makeProductImageNull(productList);
	}*/

	/*@Override
	public List<SupplierProductList> makeSupplierProductImageNull(List<SupplierProductList> supplierProductLists) {
		// TODO Auto-generated method stub
		return productDAO.makeSupplierProductImageNull(supplierProductLists);
	}*/

	@Override
	public List<Product> fetchProductListBySupplierId(String supplierId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListBySupplierId(supplierId);
	}

	@Override
	public List<Product> fetchProductListByBrandIdAndCategoryIdForWebApp(String supplierId, long categoryId,
			long brandId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListByBrandIdAndCategoryIdForWebApp(supplierId, categoryId, brandId);
	}

	

	@Override
	public List<ProductAddInventory> fetchProductList() {
		// TODO Auto-generated method stub
		return productDAO.fetchProductList();
	}

	@Override
	public List<ProductAddInventory> fetchProductForAddInventory() {
		// TODO Auto-generated method stub
		return productDAO.fetchProductForAddInventory();
	}

	@Override
	public List<ProductAddInventory> fetchProductByBrandAndCategory(
			BrandAndCategoryRequest brandAndCategoryRequest) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductByBrandAndCategory(brandAndCategoryRequest);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ProductService#fetchProductListByBrandIdAndCategoryIdForApp(java.lang.String, long, long)
	 * Oct 14, 20176:21:00 PM
	 */
	@Override
	public List<ProductAddInventory> fetchProductListByBrandIdAndCategoryIdForApp(String supplierId, long categoryId,
			long brandId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListByBrandIdAndCategoryIdForApp(supplierId, categoryId, brandId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ProductService#fetchProductListForReport()
	 * Oct 20, 201711:36:30 AM
	 */
	@Override
	public List<ProductReportView> fetchProductListForReport(String range,String startDate,String endDate,String topProductNo) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductListForReport(range, startDate, endDate,topProductNo);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ProductService#fetchProductByCategoryIdForWebApp(long)
	 * Dec 20, 20175:44:43 PM
	 */
	@Override
	public  List<Product> fetchProductByCategoryIdForWebApp(long categoryId) {
		// TODO Auto-generated method stub
		return productDAO.fetchProductByCategoryIdForWebApp(categoryId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.ProductService#updateProductForWebApp(com.bluesquare.rc.entities.Product)
	 * Dec 20, 20176:07:47 PM
	 */
	@Override
	public void updateProductForWebApp(Product product) {
		// TODO Auto-generated method stub
		productDAO.updateProductForWebApp(product);
	}

	
	@Override
	public DamageRecoveryMonthWise saveUpdateDamageRecoveryDayWise(long productId, long damageQuantity) {
		// TODO Auto-generated method stub
		return productDAO.saveUpdateDamageRecoveryMonthWise(productId, damageQuantity);
	}

	@Override
	public List<DamageRecoveryDetails> fetchDamageRecoveryDetailsListByDamageRecoveryId(long damageRecoveryId) {
		// TODO Auto-generated method stub
		return productDAO.fetchDamageRecoveryDetailsByDamageRecoveryId(damageRecoveryId);
	}

	@Override
	public DamageRecoveryDetails fetchDamageRecoveryDetailsByDamageRecoveryDetailsId(String damageRecoveryDetailsId) {
		// TODO Auto-generated method stub
		return productDAO.fetchDamageRecoveryDetailsByDamageRecoveryDetailsId(damageRecoveryDetailsId);
	}

	@Override
	public void updateDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails) {
		// TODO Auto-generated method stub
		productDAO.updateDamageRecoveryDetails(damageRecoveryDetails);
	}

	@Override
	public void saveDamageRecoveryDetails(DamageRecoveryDetails damageRecoveryDetails, long damageRecoveryId) {
		// TODO Auto-generated method stub
		productDAO.saveDamageRecoveryDetails(damageRecoveryDetails, damageRecoveryId);
	}

	@Override
	public DamageRecoveryMonthWise fetchDamageRecoveryByProductId(long productId) {
		// TODO Auto-generated method stub
		return productDAO.fetchDamageRecoveryMonthWiseByProductId(productId);
	}

	@Override
	public DamageRecoveryMonthWise fetchDamageRecoveryByDamageRecoveryId(long damageRecoveryId) {
		// TODO Auto-generated method stub
		return productDAO.fetchDamageRecoveryMonthWiseByDamageRecoveryId(damageRecoveryId);
	}

	@Override
	public CalculateProperTaxModel calculateProperAmountModel(double mrp, double igstPer) {
		// TODO Auto-generated method stub
		return productDAO.calculateProperAmountModel(mrp, igstPer);
	}


	@Override
	public List<DamageRecoveryMonthWise> fetchDamageRecoveryDayWise(String startMonth, String startYear,
			String endMonth, String endYear) {
		// TODO Auto-generated method stub
		return productDAO.fetchDamageRecoveryDayWise(startMonth, startYear, endMonth, endYear);
	}

	@Override
	public DamageRecoveryMonthWise saveUpdateDamageRecoveryMonthWise(long productId, long damageQuantity) {
		// TODO Auto-generated method stub
		return productDAO.saveUpdateDamageRecoveryMonthWise(productId, damageQuantity);
	}

	@Override
	public Product setNoImageToProductForImage(Product product) {
		// TODO Auto-generated method stub
		return productDAO.setNoImageToProductForImage(product);
	}

	@Override
	public List<DailyStockDetails> fetchDailyStockDetails(String pickDate, long companyId) {
		// TODO Auto-generated method stub
		return productDAO.fetchDailyStockDetails(pickDate, companyId);
	}

	@Override
	public void saveDamageDefine(DamageDefine damageDefine) {
		// TODO Auto-generated method stub
		productDAO.saveDamageDefine(damageDefine);
	}

	@Override
	public List<DamageDefine> fetchDamageDefineList(String range, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return productDAO.fetchDamageDefineList(range, startDate, endDate);
	}

	

	

}
