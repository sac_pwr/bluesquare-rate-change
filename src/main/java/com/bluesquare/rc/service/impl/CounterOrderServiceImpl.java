package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CounterOrderDAO;
import com.bluesquare.rc.entities.CounterOrder;
import com.bluesquare.rc.entities.CounterOrderProductDetails;
import com.bluesquare.rc.entities.PaymentCounter;
import com.bluesquare.rc.models.BillPrintDataModel;
import com.bluesquare.rc.models.CounterOrderReport;
import com.bluesquare.rc.models.OrderProductDetailListForWebApp;
import com.bluesquare.rc.models.PaymentCounterReport;
import com.bluesquare.rc.models.PaymentDoInfo;
import com.bluesquare.rc.rest.models.OrderReportList;
import com.bluesquare.rc.service.CounterOrderService;

@Component
@Transactional
@Service("counterOrderService")
@Qualifier("counterOrderService")
public class CounterOrderServiceImpl implements CounterOrderService {

	@Autowired
	CounterOrderDAO counterOrderDAO;
	
	@Override
	public String saveCounterOrder(String counterOrderProductDetails, String businessNameId, String paidAmount,
			String balAmount, String dueDate, String payType,String paymentType, String bankName, String chequeNumber, String chequeDate,
			String custName, String mobileNo, String gstNo) {
		// TODO Auto-generated method stub
		return counterOrderDAO.saveCounterOrder(counterOrderProductDetails, businessNameId, paidAmount, balAmount, dueDate, payType,
										 paymentType, bankName, chequeNumber, chequeDate, custName, mobileNo, gstNo);
	}

	@Override
	public BillPrintDataModel fetchCounterBillPrintData(String counterOrderId) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchCounterBillPrintData(counterOrderId);
	}

	@Override
	public List<OrderReportList> showCounterOrderReportByBusinessNameId(String businessNameId, String range,
			String startDate, String endDate) {
		// TODO Auto-generated method stub
		return counterOrderDAO.showCounterOrderReportByBusinessNameId(businessNameId, range, startDate, endDate);
	}

	@Override
	public CounterOrder fetchCounterOrder(String counterId) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchCounterOrder(counterId);
	}

	@Override
	public List<CounterOrder> fetchCounterOrderByRange(String businessNameId, String range, String startDate,
			String endDate) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchCounterOrderByRange(businessNameId, range, startDate, endDate);
	}

	@Override
	public List<OrderProductDetailListForWebApp> fetchCounterOrderProductDetailsForShowOrderDetails(String counterId) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchCounterOrderProductDetailsForShowOrderDetails(counterId);
	}

	@Override
	public List<PaymentCounter> fetchPaymentCounterListByCounterOrderId(String counterOrderId) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchPaymentCounterListByCounterOrderId(counterOrderId);
	}

	@Override
	public List<CounterOrderReport> fetchCounterOrderReport(String range, String startDate, String endDate) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchCounterOrderReport(range, startDate, endDate);
	}

	@Override
	public PaymentDoInfo fetchPaymentInfoByCounterOrderId(String counterOrderId) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchPaymentInfoByCounterOrderId(counterOrderId);
	}

	@Override
	public void savePaymentCounter(PaymentCounter paymentCounter) {
		// TODO Auto-generated method stub
		counterOrderDAO.savePaymentCounter(paymentCounter);
	} 

	@Override
	public void updateCounterOrder(CounterOrder counterOrder) {
		// TODO Auto-generated method stub
		counterOrderDAO.updateCounterOrder(counterOrder);
	}

	@Override
	public void deleteCounterOrder(String counterOrderId) {
		// TODO Auto-generated method stub
		counterOrderDAO.deleteCounterOrder(counterOrderId);
	}

	@Override
	public List<CounterOrderProductDetails> fetchCounterOrderProductDetails(String counterId) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchCounterOrderProductDetails(counterId);
	}

	@Override
	public String updateCounterOrderForEdit(String counterOrderId, String roductDetailsList,
			String businessNameId,String paidAmount,String balAmount,String refAmount,String paymentSituation,String dueDate,String payType,String paymentType,String bankName
			,String chequeNumber,String chequeDate,String custName,String mobileNo,String gstNo) {
		// TODO Auto-generated method stub
		return counterOrderDAO.updateCounterOrderForEdit(counterOrderId, roductDetailsList, businessNameId, paidAmount, balAmount, refAmount,paymentSituation,dueDate, payType, paymentType, bankName, chequeNumber, chequeDate, custName, mobileNo, gstNo);
	}

	@Override
	public void deletePayment(long paymentId) {
		// TODO Auto-generated method stub
		counterOrderDAO.deletePayment(paymentId);
	}

	@Override
	public void updatePayment(PaymentCounter paymentCounter) {
		// TODO Auto-generated method stub
		counterOrderDAO.updatePayment(paymentCounter);
	}

	@Override
	public PaymentCounter fetchPaymentCounterByPaymentCounterId(long paymentCounterId) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchPaymentCounterByPaymentCounterId(paymentCounterId);
	}

	@Override
	public double totalSaleAmountForProfitAndLoss(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return counterOrderDAO.totalSaleAmountForProfitAndLoss(startDate, endDate);
	}

	@Override
	public void defineChequeBounced(long paymentId) {
		// TODO Auto-generated method stub
		counterOrderDAO.defineChequeBounced(paymentId);
	}

	@Override
	public List<PaymentCounterReport> fetchPaymentCounterReportListByCounterOrderId(String counterOrderId) {
		// TODO Auto-generated method stub
		return counterOrderDAO.fetchPaymentCounterReportListByCounterOrderId(counterOrderId);
	}

}
