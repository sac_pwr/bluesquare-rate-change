package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.SupplierOrderDAO;
import com.bluesquare.rc.entities.SupplierOrder;
import com.bluesquare.rc.entities.SupplierOrderDetails;
import com.bluesquare.rc.service.SupplierOrderService;

@Component
@Transactional
@Service("supplierOrderService")
@Qualifier("supplierOrderService")
public class SupplierOrderServiceImpl implements SupplierOrderService {

	@Autowired
	SupplierOrderDAO supplierOrderDAO;
	
	@Override
	public void saveSupplierOrder(String productWithSupplier) {
		// TODO Auto-generated method stub
		supplierOrderDAO.saveSupplierOrder(productWithSupplier);
	}

	@Override
	public SupplierOrder fetchSupplierOrder(String supplierOrderId) {
		// TODO Auto-generated method stub
		return supplierOrderDAO.fetchSupplierOrder(supplierOrderId);
	}

	@Override
	public List<SupplierOrderDetails> fetchSupplierOrderDetailsList(String supplierOrderId) {
		// TODO Auto-generated method stub
		return supplierOrderDAO.fetchSupplierOrderDetailsList(supplierOrderId);
	}

	@Override
	public void editSupplierOrder(String productWithSupplier, SupplierOrder supplierOrder) {
		// TODO Auto-generated method stub
		supplierOrderDAO.editSupplierOrder(productWithSupplier, supplierOrder);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.SupplierOrderService#fetchSupplierOrderList()
	 * Nov 3, 20174:08:24 PM
	 */
	@Override
	public List<SupplierOrder> fetchSupplierOrderList(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return supplierOrderDAO.fetchSupplierOrderList(range, startDate, endDate);
	}

	@Override
	public void deleteSupplierOrder(String supplierOrderId) {
		// TODO Auto-generated method stub
		supplierOrderDAO.deleteSupplierOrder(supplierOrderId);
	}

	

}
