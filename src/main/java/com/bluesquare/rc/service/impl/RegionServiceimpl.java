package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.RegionDAO;
import com.bluesquare.rc.entities.Region;
import com.bluesquare.rc.service.RegionService;

@Component
@Transactional
@Service("regionService")
@Qualifier("regionService")
public class RegionServiceimpl implements RegionService {

	@Autowired
	RegionDAO regionDAO;
	
	@Override
	public void saveForWebApp(Region region) {
		regionDAO.saveForWebApp(region);

	}

	@Override
	public void updateForWebApp(Region region) {
		regionDAO.updateForWebApp(region);

	}

	@Override
	public List<Region> fetchAllRegionForWebApp() {
		// TODO Auto-generated method stub
		return regionDAO.fetchAllRegionForWebApp();
	}

	@Override
	public List<Region> fetchSpecifcRegionsForWebApp(long cityId) {
		// TODO Auto-generated method stub
		return regionDAO.fetchSpecifcRegionsForWebApp(cityId);
	}

	@Override
	public Region fetchRegionForWebApp(long region) {
		// TODO Auto-generated method stub
		return regionDAO.fetchRegionForWebApp(region);
	}


}
