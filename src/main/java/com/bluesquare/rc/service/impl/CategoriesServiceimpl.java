package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.CategoriesDAO;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.service.CategoriesService;


@Component
@Transactional
@Service("categoriesService")
@Qualifier("categoriesService")
public class CategoriesServiceimpl implements CategoriesService {

	@Autowired
	CategoriesDAO categoriesDAO;
	
	@Override
	public void saveCategoriesForWebApp(Categories categories) {
		// TODO Auto-generated method stub
		categoriesDAO.saveCategoriesForWebApp(categories);
	}

	@Override
	public void updateCategoriesForWebApp(Categories categories) {
		// TODO Auto-generated method stub
		categoriesDAO.updateCategoriesForWebApp(categories);
	}

	@Override
	public Categories fetchCategoriesForWebApp(long categoriesId) {
		// TODO Auto-generated method stub
		return categoriesDAO.fetchCategoriesForWebApp(categoriesId);
	}

	@Override
	public List<Categories> fetchCategoriesListForWebApp() {
		// TODO Auto-generated method stub
		return categoriesDAO.fetchCategoriesListForWebApp();
	}

}
