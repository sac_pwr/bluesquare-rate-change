package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.SupplierDAO;
import com.bluesquare.rc.entities.Supplier;
import com.bluesquare.rc.entities.SupplierProductList;
import com.bluesquare.rc.models.FetchSupplierList;
import com.bluesquare.rc.rest.models.ProductAddInventory;
import com.bluesquare.rc.service.SupplierService;

@Component
@Transactional
@Service("supplierService")
@Qualifier("supplierService")
public class SupplierServiceimpl implements SupplierService{

	@Autowired
	SupplierDAO supplierDAO;
	
	@Override
	public List<Supplier> fetchSupplierForWebAppList() {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierForWebAppList();
	}

	@Override
	public Supplier fetchSupplier(String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplier(supplierId);
	}

	@Override
	public Supplier saveSupplier(Supplier supplier) {
		// TODO Auto-generated method stub
		return supplierDAO.saveSupplier(supplier);
	}

	@Override
	public Supplier updateSupplier(Supplier supplier) {
		// TODO Auto-generated method stub
		return supplierDAO.updateSupplier(supplier);
	}

	@Override
	public Supplier saveSupplierProductList(String productIdList, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.saveSupplierProductList(productIdList, supplierId);
	}

	@Override
	public List<FetchSupplierList> fetchSupplierForWebApp() {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierForWebApp();
	}

	@Override
	public List<SupplierProductList> fetchSupplierProductListBySupplierIdForWebApp(String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierProductListBySupplierIdForWebApp(supplierId);
	}

	/*@Override
	public List<SupplierProductList> makeSupplierProductListNullForWebApp(
			List<SupplierProductList> supplierProductList) {
		// TODO Auto-generated method stub
		return supplierDAO.makeSupplierProductListNullForWebApp(supplierProductList);
	}*/

	@Override
	public String getProductIdList(List<SupplierProductList> supplierProductList) {
		// TODO Auto-generated method stub
		return supplierDAO.getProductIdList(supplierProductList);
	}

	@Override
	public Supplier updateSupplierProductList(String productIdList, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.updateSupplierProductList(productIdList, supplierId);
	}

	@Override
	public SupplierProductList fetchSupplierProductRate(long productId, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierProductRate(productId, supplierId);
	}

	@Override
	public List<ProductAddInventory> fetchProductBySupplierId(String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchProductBySupplierId(supplierId);
	}

	@Override
	public String sendSMSTOSupplier(String supplierIds,String smsText, String mobileNumber) {
		// TODO Auto-generated method stub
		return supplierDAO.sendSMSTOSupplier(supplierIds, smsText, mobileNumber);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.SupplierService#fetchSupplierByProductIdAndSupplierId(long, java.lang.String)
	 * Dec 26, 20178:11:53 PM
	 */
	@Override
	public SupplierProductList fetchSupplierByProductIdAndSupplierId(long productId, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierByProductIdAndSupplierId(productId, supplierId);
	}

	@Override
	public String checkSupplierDuplication(String checkText, String type, String supplierId) {
		// TODO Auto-generated method stub
		return supplierDAO.checkSupplierDuplication(checkText, type, supplierId);
	}

	@Override
	public List<SupplierProductList> fetchSupplierProductListForChangeUnitPriceByTaxSlab(long categoryId) {
		// TODO Auto-generated method stub
		return supplierDAO.fetchSupplierProductListForChangeUnitPriceByTaxSlab(categoryId);
	}

	@Override
	public void updateSupplierProductList(SupplierProductList supplierProductList) {
		// TODO Auto-generated method stub
		supplierDAO.updateSupplierProductList(supplierProductList);
	}

}
