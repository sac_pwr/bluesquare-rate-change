package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.ExpenseDAO;
import com.bluesquare.rc.entities.Expense;
import com.bluesquare.rc.entities.ExpenseType;
import com.bluesquare.rc.models.ExpenseListModel;
import com.bluesquare.rc.models.ProfitAndLossEntity;
import com.bluesquare.rc.service.ExpenseService;

@Component
@Transactional
@Service("expenseService")
@Qualifier("expenseService")
public class ExpenseServiceImpl implements ExpenseService{

	@Autowired
	ExpenseDAO expenseDAO;
	
	@Override
	public List<ExpenseListModel> fetchExpenseListByRange(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return expenseDAO.fetchExpenseListByRange(range, startDate, endDate);
	}

	@Override
	public List<Expense> fetchExpenseList() {
		// TODO Auto-generated method stub
		return expenseDAO.fetchExpenseList();
	}

	@Override
	public Expense fetchExpense(String expenseId) {
		// TODO Auto-generated method stub
		return expenseDAO.fetchExpense(expenseId);
	}

	@Override
	public void deleteExpense(Expense expense) {
		// TODO Auto-generated method stub
		expenseDAO.deleteExpense(expense);
	}

	@Override
	public void updateExpense(Expense expense) {
		// TODO Auto-generated method stub
		expenseDAO.updateExpense(expense);
	}

	@Override
	public void saveExpense(Expense expense) {
		// TODO Auto-generated method stub
		expenseDAO.saveExpense(expense);
	}

	@Override
	public List<ExpenseType> fetchExpenseTypeList() {
		// TODO Auto-generated method stub
		return expenseDAO.fetchExpenseTypeList();
	}

	@Override
	public ExpenseType fetchExpenseTypeByExpenseTypeId(long expenseTypeId) {
		// TODO Auto-generated method stub
		return expenseDAO.fetchExpenseTypeByExpenseTypeId(expenseTypeId);
	}

	@Override
	public void updateExpenseType(ExpenseType expenseType) {
		// TODO Auto-generated method stub
		expenseDAO.updateExpenseType(expenseType);
	}

	@Override
	public void saveExpenseType(ExpenseType expenseType) {
		// TODO Auto-generated method stub
		expenseDAO.saveExpenseType(expenseType);
	}

	@Override
	public String checkExistByExpenseTypeIdForAdd(String expenseTypeName) {
		// TODO Auto-generated method stub
		return expenseDAO.checkExistByExpenseTypeIdForAdd(expenseTypeName);
	}

	@Override
	public String checkExistByExpenseTypeIdForUpdate(String expenseTypeName, long expenseTypeId) {
		// TODO Auto-generated method stub
		return expenseDAO.checkExistByExpenseTypeIdForUpdate(expenseTypeName, expenseTypeId);
	}

	@Override
	public List<ProfitAndLossEntity> expenseListForProfitAndLoss(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return expenseDAO.expenseListForProfitAndLoss(startDate, endDate);
	}

}
