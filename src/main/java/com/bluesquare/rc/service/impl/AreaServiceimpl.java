package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.AreaDAO;
import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.service.AreaService;

@Component
@Transactional
@Service("areaService")
@Qualifier("areaService")
public class AreaServiceimpl implements AreaService {

	@Autowired
	AreaDAO areaDAO;
	
	@Override
	public void saveForWebApp(Area area) {
		// TODO Auto-generated method stub
		areaDAO.saveForWebApp(area);
	}

	@Override
	public void updateForWebApp(Area area) {
		// TODO Auto-generated method stub
		areaDAO.updateForWebApp(area);
	}

	@Override
	public List<Area> fetchAllAreaForWebApp() {
		// TODO Auto-generated method stub
		return areaDAO.fetchAllAreaForWebApp();
	}

	@Override
	public List<Area> fetchAreaListForWebApp(long regionId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchAreaListByRegionIdForWebApp(regionId);
	}

	@Override
	public Area fetchAreaForWebApp(long areaId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchAreaForWebApp(areaId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.AreaService#fetchAreaListByEmployeeId(long)
	 * Oct 26, 20176:28:30 PM
	 */
	@Override
	public String fetchAreaListByEmployeeId(long employeeId) {
		// TODO Auto-generated method stub
		return areaDAO.fetchAreaListByEmployeeId(employeeId);
	}

	@Override
	public boolean checkAreaDuplication(long pincode,long areaId) {
		// TODO Auto-generated method stub
		return areaDAO.checkAreaDuplication(pincode,areaId);
	}


}
