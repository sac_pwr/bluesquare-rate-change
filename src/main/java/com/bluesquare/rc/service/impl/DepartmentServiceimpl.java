package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.DepartmentDAO;
import com.bluesquare.rc.entities.Department;
import com.bluesquare.rc.service.DepartmentService;


@Component
@Transactional
@Service("departmentService")
@Qualifier("departmentService")
public class DepartmentServiceimpl implements DepartmentService {

	@Autowired
	DepartmentDAO departmentDAO;
	
	
	@Override
	public void saveForWebApp(Department department) {
		// TODO Auto-generated method stub
		departmentDAO.saveForWebApp(department);
	}

	@Override
	public void updateForWebApp(Department department) {
		// TODO Auto-generated method stub
		departmentDAO.updateForWebApp(department);
	}

	@Override
	public List<Department> fetchDepartmentListForWebApp() {
		// TODO Auto-generated method stub
		return departmentDAO.fetchDepartmentListForWebApp();
	}

	@Override
	public Department fetchDepartmentForWebApp(long departmentId) {
		// TODO Auto-generated method stub
		return departmentDAO.fetchDepartmentForWebApp(departmentId);
	}

}
