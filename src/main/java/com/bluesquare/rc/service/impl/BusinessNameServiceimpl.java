package com.bluesquare.rc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bluesquare.rc.dao.BusinessNameDAO;
import com.bluesquare.rc.entities.BusinessName;
import com.bluesquare.rc.models.BusinessNameList;
import com.bluesquare.rc.models.CustomerReportView;
import com.bluesquare.rc.service.BusinessNameService;

@Component
@Transactional
@Service("businessNameService")
@Qualifier("businessNameService")
public class BusinessNameServiceimpl implements BusinessNameService {

	@Autowired
	BusinessNameDAO businessNameDAO;	
	
	@Override
	public void saveForWebApp(BusinessName businessName) {
		// TODO Auto-generated method stub
		businessNameDAO.saveForWebApp(businessName);
	}

	@Override
	public void updateForWebApp(BusinessName businessName) {
		// TODO Auto-generated method stub
		businessNameDAO.updateForWebApp(businessName);
	}

	@Override
	public List<BusinessName> getBusinessNameListForWebApp() {
		// TODO Auto-generated method stub
		return businessNameDAO.getBusinessNameListForWebApp();
	}

	@Override
	public BusinessName fetchBusinessForWebApp(String businessNameId) {
		// TODO Auto-generated method stub
		return businessNameDAO.fetchBusinessForWebApp(businessNameId);
	}

	@Override
	public void saveBusinessName(BusinessName businessName) {
		// TODO Auto-generated method stub
		businessNameDAO.saveBusinessName(businessName);
	}

	@Override
	public List<BusinessName> businessNameByAreaIdAndBusinessTypeIdForWebApp(long businessTypeId, long areaId) {
		// TODO Auto-generated method stub
		return businessNameDAO.businessNameByAreaIdAndBusinessTypeIdForWebApp(businessTypeId, areaId);
	}

	@Override
	public List<BusinessNameList> fetchBusinessNameList() {
		// TODO Auto-generated method stub
		return businessNameDAO.fetchBusinessNameList();
	}

	@Override
	public String sendSMSTOShops(String shopsId,String smsText,String mobileNumber) {
		// TODO Auto-generated method stub
		return businessNameDAO.sendSMSTOShops(shopsId, smsText,mobileNumber);
	}

	@Override
	public List<BusinessName> fetchBusinessNameByAreaId(long areaId) {
		// TODO Auto-generated method stub
		return businessNameDAO.fetchBusinessNameByAreaId(areaId);
	}

	/* (non-Javadoc)
	 * @see com.bluesquare.rc.service.BusinessNameService#fetchBusinessNameForReport()
	 * Oct 20, 20178:28:38 PM
	 */
	@Override
	public CustomerReportView fetchBusinessNameForReport(String range,String startDate,String endDate) {
		// TODO Auto-generated method stub
		return businessNameDAO.fetchBusinessNameForReport(range,startDate,endDate);
	}

	@Override
	public String checkBusinessDuplication(String checkText, String type, String businessNameId) {
		// TODO Auto-generated method stub
		return businessNameDAO.checkBusinessDuplication(checkText, type, businessNameId);
	}

}
