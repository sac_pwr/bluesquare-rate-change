package com.bluesquare.rc.service;

import com.bluesquare.rc.rest.models.SnapShotReportResponse;

public interface SnapShotService {
	
	public SnapShotReportResponse fetchRecordForSnapShot(long employeeId, String fromDate, String toDate,
			String range);

}
