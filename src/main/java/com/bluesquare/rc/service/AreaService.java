package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Area;

public interface AreaService {
	//webapp
		public void saveForWebApp(Area area);

		public void updateForWebApp(Area area);

		public List<Area> fetchAllAreaForWebApp();
		
		public List<Area> fetchAreaListForWebApp(long regionId);
		//public List<Area> fetchAreaListForBranchConfig(long regionId);
		
		public boolean checkAreaDuplication(long pincode,long areaId);
		
		public Area fetchAreaForWebApp(long areaId);
		
		public String fetchAreaListByEmployeeId(long employeeId);
}
