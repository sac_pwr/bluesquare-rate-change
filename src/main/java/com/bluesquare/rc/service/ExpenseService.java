package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Expense;
import com.bluesquare.rc.entities.ExpenseType;
import com.bluesquare.rc.models.ExpenseListModel;
import com.bluesquare.rc.models.ProfitAndLossEntity;

public interface ExpenseService {
	public List<ExpenseListModel> fetchExpenseListByRange(String range,String startDate,String endDate);
	public List<Expense> fetchExpenseList();
	public Expense fetchExpense(String expenseId);
	public void deleteExpense(Expense expense);
	public void updateExpense(Expense expense);
	public void saveExpense(Expense expense);
	public List<ProfitAndLossEntity> expenseListForProfitAndLoss(String startDate,String endDate);
	
	//ExpenseType CRUD
	public List<ExpenseType> fetchExpenseTypeList();
	public ExpenseType fetchExpenseTypeByExpenseTypeId(long expenseTypeId);
	public void updateExpenseType(ExpenseType expenseType);
	public void saveExpenseType(ExpenseType expenseType);
	public String checkExistByExpenseTypeIdForAdd(String expenseTypeName);
	public String checkExistByExpenseTypeIdForUpdate(String expenseTypeName,long expenseTypeId);
}
