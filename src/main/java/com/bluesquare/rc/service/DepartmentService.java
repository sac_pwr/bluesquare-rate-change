package com.bluesquare.rc.service;

import java.util.List;

import com.bluesquare.rc.entities.Department;

public interface DepartmentService {
	//webApp
			public void saveForWebApp(Department department);

			public void updateForWebApp(Department department);
			
			public List<Department> fetchDepartmentListForWebApp();
			
			public Department fetchDepartmentForWebApp(long departmentId);
}
