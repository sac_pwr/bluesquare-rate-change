package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "area")
@Component

public class Area {

	@Id
	@Column(name = "area_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long areaId;
	
	@Column(name = "name")
	private String name;

	@Column(name = "pincode")
	private long pincode;
	
	@ManyToOne
    @JoinColumn(name = "region_id")
	private Region region;
	
	@ManyToOne
    @JoinColumn(name = "company_id")
	private Company company;

	public long getAreaId() {
		return areaId;
	}

	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPincode() {
		return pincode;
	}

	public void setPincode(long pincode) {
		this.pincode = pincode;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "Area [areaId=" + areaId + ", name=" + name + ", pincode=" + pincode + ", region=" + region
				+ ", company=" + company + "]";
	}

	
	

}