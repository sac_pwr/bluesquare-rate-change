package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "inventory")
@Component
public class Inventory {
	
	@Id
	@Column(name = "inventory_transaction_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long inventoryTransactionPkId;	
	
	
	@Column(name = "inventory_transaction_id")
	private String inventoryTransactionId;	
	
	@ManyToOne
	@JoinColumn(name="supplier_id")
	private Supplier supplier;
	
	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="employee_id")
	private Employee employee;
	
	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmount;
	
	@Column(name = "total_amount_tax", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmountTax;
	
	@Column(name = "total_quantity")
	private long totalQuantity;
	
	@Column(name="inventory_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date inventoryAddedDatetime;
	
	@Column(name="inventory_payment_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date inventoryPaymentDatetime;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="paid_status")
	private boolean payStatus;
	
	@Column(name = "bill_number")
	private String billNumber;
	
	@Column(name = "bill_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date billDate;

	
	
	public long getInventoryTransactionPkId() {
		return inventoryTransactionPkId;
	}

	public void setInventoryTransactionPkId(long inventoryTransactionPkId) {
		this.inventoryTransactionPkId = inventoryTransactionPkId;
	}

	public String getInventoryTransactionId() {
		return inventoryTransactionId;
	}

	public void setInventoryTransactionId(String inventoryTransactionId) {
		this.inventoryTransactionId = inventoryTransactionId;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountTax() {
		return totalAmountTax;
	}

	public void setTotalAmountTax(double totalAmountTax) {
		this.totalAmountTax = totalAmountTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Date getInventoryAddedDatetime() {
		return inventoryAddedDatetime;
	}

	public void setInventoryAddedDatetime(Date inventoryAddedDatetime) {
		this.inventoryAddedDatetime = inventoryAddedDatetime;
	}

	public Date getInventoryPaymentDatetime() {
		return inventoryPaymentDatetime;
	}

	public void setInventoryPaymentDatetime(Date inventoryPaymentDatetime) {
		this.inventoryPaymentDatetime = inventoryPaymentDatetime;
	}

	public boolean isPayStatus() {
		return payStatus;
	}

	public void setPayStatus(boolean payStatus) {
		this.payStatus = payStatus;
	}

	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	@Override
	public String toString() {
		return "Inventory [inventoryTransactionPkId=" + inventoryTransactionPkId + ", inventoryTransactionId="
				+ inventoryTransactionId + ", supplier=" + supplier + ", company=" + company + ", employee=" + employee
				+ ", totalAmount=" + totalAmount + ", totalAmountTax=" + totalAmountTax + ", totalQuantity="
				+ totalQuantity + ", inventoryAddedDatetime=" + inventoryAddedDatetime + ", inventoryPaymentDatetime="
				+ inventoryPaymentDatetime + ", payStatus=" + payStatus + ", billNumber=" + billNumber + ", billDate="
				+ billDate + "]";
	}


}
