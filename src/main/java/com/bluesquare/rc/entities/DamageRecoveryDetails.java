package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "damage_recovery_details")
@Component

public class DamageRecoveryDetails {

	@Id
	@Column(name = "damage_recovery_details_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long damageRecoveryDetailsPkId;
	
	@Column(name = "damage_recovery_details_id")
	private String damageRecoveryDetailsId;

	@ManyToOne
	@JoinColumn(name = "damage_recovery_day_wise_id")
	private DamageRecoveryMonthWise damageRecoveryMonthWise;
	
	@ManyToOne
	@JoinColumn(name = "supplier_id")
	private Supplier supplier;

	@Column(name = "qty_given")
	private long quantityGiven;

	@Column(name = "qty_received")
	private long quantityReceived;
	
	@Column(name = "qty_not_claimed")
	private long quantityNotClaimed;
	
	@Column(name="damage_recovery_given_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date givenDate;
	
	@Column(name="damage_recovery_receive_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date receiveDate;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="receive_status")
	private boolean receiveStatus;
	
	public long getDamageRecoveryDetailsPkId() {
		return damageRecoveryDetailsPkId;
	}

	public void setDamageRecoveryDetailsPkId(long damageRecoveryDetailsPkId) {
		this.damageRecoveryDetailsPkId = damageRecoveryDetailsPkId;
	}

	public String getDamageRecoveryDetailsId() {
		return damageRecoveryDetailsId;
	}

	public void setDamageRecoveryDetailsId(String damageRecoveryDetailsId) {
		this.damageRecoveryDetailsId = damageRecoveryDetailsId;
	}
	
	public DamageRecoveryMonthWise getDamageRecoveryMonthWise() {
		return damageRecoveryMonthWise;
	}

	public void setDamageRecoveryMonthWise(DamageRecoveryMonthWise damageRecoveryMonthWise) {
		this.damageRecoveryMonthWise = damageRecoveryMonthWise;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public long getQuantityGiven() {
		return quantityGiven;
	}

	public void setQuantityGiven(long quantityGiven) {
		this.quantityGiven = quantityGiven;
	}

	public long getQuantityReceived() {
		return quantityReceived;
	}

	public void setQuantityReceived(long quantityReceived) {
		this.quantityReceived = quantityReceived;
	}

	public long getQuantityNotClaimed() {
		return quantityNotClaimed;
	}

	public void setQuantityNotClaimed(long quantityNotClaimed) {
		this.quantityNotClaimed = quantityNotClaimed;
	}

	public Date getGivenDate() {
		return givenDate;
	}

	public void setGivenDate(Date givenDate) {
		this.givenDate = givenDate;
	}

	public Date getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(Date receiveDate) {
		this.receiveDate = receiveDate;
	}

	public boolean isReceiveStatus() {
		return receiveStatus;
	}

	public void setReceiveStatus(boolean receiveStatus) {
		this.receiveStatus = receiveStatus;
	}

	@Override
	public String toString() {
		return "DamageRecoveryDetails [damageRecoveryDetailsPkId=" + damageRecoveryDetailsPkId
				+ ", damageRecoveryDetailsId=" + damageRecoveryDetailsId + ", damageRecoveryMonthWise="
				+ damageRecoveryMonthWise + ", supplier=" + supplier + ", quantityGiven=" + quantityGiven
				+ ", quantityReceived=" + quantityReceived + ", quantityNotClaimed=" + quantityNotClaimed
				+ ", givenDate=" + givenDate + ", receiveDate=" + receiveDate + ", receiveStatus=" + receiveStatus
				+ "]";
	}
}
