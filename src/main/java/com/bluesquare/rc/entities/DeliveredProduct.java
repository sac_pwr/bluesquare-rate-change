package com.bluesquare.rc.entities;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "delivered_product")
@Component
public class DeliveredProduct {
	@Id
	@Column(name = "delivered_product_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long deliveredProductId;
	
	@ManyToOne
	@JoinColumn(name = "order_details_id")
	private OrderDetails orderDetails;
	
	@JsonIgnore // disable this one too
	@Column(name = "order_receiver_signature")
	private Blob orderReceiverSignature;
	
	@Column(name = "order_receiver_signature_status")
	private String status;

	public long getDeliveredProductId() {
		return deliveredProductId;
	}

	public void setDeliveredProductId(long deliveredProductId) {
		this.deliveredProductId = deliveredProductId;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Blob getOrderReceiverSignature() {
		return orderReceiverSignature;
	}

	public void setOrderReceiverSignature(Blob orderReceiverSignature) {
		this.orderReceiverSignature = orderReceiverSignature;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "DeliveredProduct [deliveredProductId=" + deliveredProductId + ", orderDetails=" + orderDetails
				+ ", orderReceiverSignature=" + orderReceiverSignature + ", status=" + status + "]";
	}

	
	
}
