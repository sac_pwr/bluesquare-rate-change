package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "employee_incentives")
@Component

public class EmployeeIncentives {
	@Id
	@Column(name = "employee_incentives_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeIncentiveId;
	
	@Column(name="incentive_amount", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double incentiveAmount;
	
	@Column(name="reason")
	private String reason;
	
	@Column(name="incentive_given_date")
	private Date incentiveGivenDate;
	
	@ManyToOne
	@JoinColumn(name="employee_details_id")
	private EmployeeDetails employeeDetails;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="status")
	private boolean status;
	

	public long getEmployeeIncentiveId() {
		return employeeIncentiveId;
	}

	public void setEmployeeIncentiveId(long employeeIncentiveId) {
		this.employeeIncentiveId = employeeIncentiveId;
	}

	public double getIncentiveAmount() {
		return incentiveAmount;
	}

	public void setIncentiveAmount(double incentiveAmount) {
		this.incentiveAmount = incentiveAmount;
	}

	public Date getIncentiveGivenDate() {
		return incentiveGivenDate;
	}

	public void setIncentiveGivenDate(Date incentiveGivenDate) {
		this.incentiveGivenDate = incentiveGivenDate;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "EmployeeIncentives [employeeIncentiveId=" + employeeIncentiveId + ", incentiveAmount=" + incentiveAmount
				+ ", reason=" + reason + ", incentiveGivenDate=" + incentiveGivenDate + ", employeeDetails="
				+ employeeDetails + ", status=" + status + "]";
	}

	
}
