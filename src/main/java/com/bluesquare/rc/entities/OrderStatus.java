package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "order_status")
@Component
public class OrderStatus {
	@Id
	@Column(name = "order_status_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderStatusId;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "details")
	private String details;
	
	@Column(name="Order_status_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date OrderStatusAddedDatetime;

	public long getOrderStatusId() {
		return orderStatusId;
	}

	public void setOrderStatusId(long orderStatusId) {
		this.orderStatusId = orderStatusId;
	}

	
	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getOrderStatusAddedDatetime() {
		return OrderStatusAddedDatetime;
	}

	public void setOrderStatusAddedDatetime(Date orderStatusAddedDatetime) {
		OrderStatusAddedDatetime = orderStatusAddedDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "OrderStatus [orderStatusId=" + orderStatusId + ", status=" + status + ", details=" + details
				+ ", OrderStatusAddedDatetime=" + OrderStatusAddedDatetime + "]";
	}

}
