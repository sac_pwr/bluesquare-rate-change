package com.bluesquare.rc.entities;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "employee_holiday")
@Component
public class EmployeeHolidays {

	@Id
	@Column(name = "employee_holiday_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeHolidaysId;
	
	@Column(name="reason")
	private String reason;
	
	@ManyToOne
	@JoinColumn(name="employee_details_id")
	private EmployeeDetails employeeDetails;
	
	@Column(name = "from_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date fromDate;	
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="paid_holiday")
	private boolean paidHoliday;
	
	@Column(name = "to_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date toDate;
	
	@Column(name = "given_holiday_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date givenHolidayDate;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="status")
	private boolean status;

	public long getEmployeeHolidaysId() {
		return employeeHolidaysId;
	}

	public void setEmployeeHolidaysId(long employeeHolidaysId) {
		this.employeeHolidaysId = employeeHolidaysId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Date getGivenHolidayDate() {
		return givenHolidayDate;
	}

	public void setGivenHolidayDate(Date givenHolidayDate) {
		this.givenHolidayDate = givenHolidayDate;
	}

	

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isPaidHoliday() {
		return paidHoliday;
	}

	public void setPaidHoliday(boolean paidHoliday) {
		this.paidHoliday = paidHoliday;
	}

	


	@Override
	public String toString() {
		return "EmployeeHolidays [employeeHolidaysId=" + employeeHolidaysId + ", reason=" + reason
				+ ", employeeDetails=" + employeeDetails + ", fromDate=" + fromDate + ", paidHoliday=" + paidHoliday
				+ ", toDate=" + toDate + ", givenHolidayDate=" + givenHolidayDate + ", status=" + status + "]";
	}




	public static class NumericBooleanSerializer extends JsonSerializer<Boolean> {

		@Override
		public void serialize(Boolean bool, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			generator.writeString(bool ? "1" : "0");
		}
	}

	public static class NumericBooleanDeserializer extends JsonDeserializer<Boolean> {

		@Override
		public Boolean deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			return !"0".equals(parser.getText());
		}
	}
	
}
