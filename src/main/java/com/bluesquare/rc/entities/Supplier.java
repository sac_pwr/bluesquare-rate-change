package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "supplier")
@Component
public class Supplier {

	@Id
	@Column(name = "supplier_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long supplierPKId;
	
	@Column(name = "supplier_id")
	private String supplierId;

	@Column(name = "name")
	private String name;
	
	@ManyToOne
	@JoinColumn(name="contact_id")
	private Contact contact;
	
	@Column(name="address")
	private String address;
	
	@Column(name = "gstin_no")
	private String gstinNo;

	@Column(name="supplier_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date supplierAddedDatetime;

	@Column(name="supplier_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date supplierUpdatedDatetime;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public long getSupplierPKId() {
		return supplierPKId;
	}

	public void setSupplierPKId(long supplierPKId) {
		this.supplierPKId = supplierPKId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGstinNo() {
		return gstinNo;
	}

	public void setGstinNo(String gstinNo) {
		this.gstinNo = gstinNo;
	}

	public Date getSupplierAddedDatetime() {
		return supplierAddedDatetime;
	}

	public void setSupplierAddedDatetime(Date supplierAddedDatetime) {
		this.supplierAddedDatetime = supplierAddedDatetime;
	}

	public Date getSupplierUpdatedDatetime() {
		return supplierUpdatedDatetime;
	}

	public void setSupplierUpdatedDatetime(Date supplierUpdatedDatetime) {
		this.supplierUpdatedDatetime = supplierUpdatedDatetime;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "Supplier [supplierPKId=" + supplierPKId + ", supplierId=" + supplierId + ", name=" + name + ", contact="
				+ contact + ", address=" + address + ", gstinNo=" + gstinNo + ", supplierAddedDatetime="
				+ supplierAddedDatetime + ", supplierUpdatedDatetime=" + supplierUpdatedDatetime + ", company="
				+ company + "]";
	}
}
