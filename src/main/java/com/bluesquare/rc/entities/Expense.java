package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "expense")
@Component
public class Expense {

	@Id
	@Column(name = "expense_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long expensePkId;

	@Column(name = "expense_id")
	private String expenseId;
	
	@Column(name = "reference")
	private String reference;
	
	@Column(name = "amount", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double amount;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "cheque_number")
	private String chequeNumber;
	
	@Column(name = "bank_name")
	private String bankName;
	
	@Column(name="add_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date addDate;
	
	@Column(name="update_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date updateDate;
	
	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name = "expense_type_id")
	private ExpenseType expenseType;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "status")
	private boolean status;
	
	public long getExpensePkId() {
		return expensePkId;
	}

	public void setExpensePkId(long expensePkId) {
		this.expensePkId = expensePkId;
	}

	public String getExpenseId() {
		return expenseId;
	}

	public void setExpenseId(String expenseId) {
		this.expenseId = expenseId;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public ExpenseType getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(ExpenseType expenseType) {
		this.expenseType = expenseType;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Expense [expensePkId=" + expensePkId + ", expenseId=" + expenseId + ", reference=" + reference
				+ ", amount=" + amount + ", type=" + type + ", chequeNumber=" + chequeNumber + ", bankName=" + bankName
				+ ", addDate=" + addDate + ", updateDate=" + updateDate + ", employee=" + employee + ", company="
				+ company + ", expenseType=" + expenseType + ", status=" + status + "]";
	}
}
