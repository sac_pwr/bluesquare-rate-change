package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "employee_feedback_reply")
@Component
public class EmployeeFeedBackReply {
	@Id
	@Column(name = "feedback_reply_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long feedBackReplyId;
	
	@ManyToOne
	@JoinColumn(name="feedback_id")
	private EmployeeFeedBack employeeFeedBack;
	
	@Column(name="feedback_reply_msg")
	private String feedbackReplyMsg;
	
	@Column(name = "feedback_reply_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date feedBackReplyGivenDate;

	public long getFeedBackReplyId() {
		return feedBackReplyId;
	}

	public void setFeedBackReplyId(long feedBackReplyId) {
		this.feedBackReplyId = feedBackReplyId;
	}

	public EmployeeFeedBack getEmployeeFeedBack() {
		return employeeFeedBack;
	}

	public void setEmployeeFeedBack(EmployeeFeedBack employeeFeedBack) {
		this.employeeFeedBack = employeeFeedBack;
	}

	public String getFeedbackReplyMsg() {
		return feedbackReplyMsg;
	}

	public void setFeedbackReplyMsg(String feedbackReplyMsg) {
		this.feedbackReplyMsg = feedbackReplyMsg;
	}

	public Date getFeedBackReplyGivenDate() {
		return feedBackReplyGivenDate;
	}

	public void setFeedBackReplyGivenDate(Date feedBackReplyGivenDate) {
		this.feedBackReplyGivenDate = feedBackReplyGivenDate;
	}

	@Override
	public String toString() {
		return "EmployeeFeedBackReply [feedBackReplyId=" + feedBackReplyId + ", employeeFeedBack=" + employeeFeedBack
				+ ", feedbackReplyMsg=" + feedbackReplyMsg + ", feedBackReplyGivenDate=" + feedBackReplyGivenDate + "]";
	}
	
	
}
