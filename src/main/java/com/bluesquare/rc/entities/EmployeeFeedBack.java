package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeHolidays.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeHolidays.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "employee_feedback")
@Component
public class EmployeeFeedBack {

	@Id
	@Column(name = "feedBack_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long feedBackId;
	
	@ManyToOne
	@JoinColumn(name="employeeDetails_id")
	private EmployeeDetails employeeDetails;
	
	@Column(name="feedback_msg")
	private String feedbackMsg;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="reply_status")
	private boolean replyStatus;
	
	@Column(name = "feedback_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date feedBackGivenDate;
	
	@Column(name = "feedback_reply_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date feedBackReplyGivenDate;

	public long getFeedBackId() {
		return feedBackId;
	}

	public void setFeedBackId(long feedBackId) {
		this.feedBackId = feedBackId;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	public String getFeedbackMsg() {
		return feedbackMsg;
	}

	public void setFeedbackMsg(String feedbackMsg) {
		this.feedbackMsg = feedbackMsg;
	}

	public boolean isReplyStatus() {
		return replyStatus;
	}

	public void setReplyStatus(boolean replyStatus) {
		this.replyStatus = replyStatus;
	}

	public Date getFeedBackGivenDate() {
		return feedBackGivenDate;
	}

	public void setFeedBackGivenDate(Date feedBackGivenDate) {
		this.feedBackGivenDate = feedBackGivenDate;
	}

	public Date getFeedBackReplyGivenDate() {
		return feedBackReplyGivenDate;
	}

	public void setFeedBackReplyGivenDate(Date feedBackReplyGivenDate) {
		this.feedBackReplyGivenDate = feedBackReplyGivenDate;
	}

	@Override
	public String toString() {
		return "FeedBack [feedBackId=" + feedBackId + ", employeeDetails=" + employeeDetails + ", feedbackMsg="
				+ feedbackMsg + ", replyStatus=" + replyStatus + ", feedBackGivenDate=" + feedBackGivenDate
				+ ", feedBackReplyGivenDate=" + feedBackReplyGivenDate + "]";
	}
	
	
}
