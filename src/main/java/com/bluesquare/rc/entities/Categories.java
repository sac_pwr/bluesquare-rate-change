package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "categories")
@Component

public class Categories {

	@Id
	@Column(name = "category_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long categoryId;

	@Column(name = "category_name")
	private String categoryName;

	@Column(name = "category_description")
	private String categoryDescription;

	@Column(name = "hsn_code")
	private String hsnCode;

	@Column(name = "cgst", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private float cgst;

	@Column(name = "sgst", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private float sgst;

	@Column(name = "igst", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private float igst;

	@Column(name = "category_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date categoryDate;
	
	@Column(name = "category_update_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date categoryUpdateDate;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	
	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDescription() {
		return categoryDescription;
	}

	public void setCategoryDescription(String categoryDescription) {
		this.categoryDescription = categoryDescription;
	}

	public String getHsnCode() {
		return hsnCode;
	}

	public void setHsnCode(String hsnCode) {
		this.hsnCode = hsnCode;
	}

	public float getCgst() {
		return cgst;
	}

	public void setCgst(float cgst) {
		this.cgst = cgst;
	}

	public float getSgst() {
		return sgst;
	}

	public void setSgst(float sgst) {
		this.sgst = sgst;
	}

	public float getIgst() {
		return igst;
	}

	public void setIgst(float igst) {
		this.igst = igst;
	}

	public Date getCategoryDate() {
		return categoryDate;
	}

	public void setCategoryDate(Date categoryDate) {
		this.categoryDate = categoryDate;
	}

	public Date getCategoryUpdateDate() {
		return categoryUpdateDate;
	}

	public void setCategoryUpdateDate(Date categoryUpdateDate) {
		this.categoryUpdateDate = categoryUpdateDate;
	}

	@Override
	public String toString() {
		return "Categories [categoryId=" + categoryId + ", categoryName=" + categoryName + ", categoryDescription="
				+ categoryDescription + ", hsnCode=" + hsnCode + ", cgst=" + cgst + ", sgst=" + sgst + ", igst=" + igst
				+ ", categoryDate=" + categoryDate + ", categoryUpdateDate=" + categoryUpdateDate + ", company="
				+ company + "]";
	}

	
}
