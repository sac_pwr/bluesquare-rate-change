package com.bluesquare.rc.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "return_from_delivery_boy")
@Component
public class ReturnFromDeliveryBoy {
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long returnFromDeliveryBoyId;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private OrderUsedProduct product;
	
	@Column(name = "selling_rate", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double sellingRate;
	
	@Column(name = "return_quantity")
	private long returnQuantity;
	
	@Column(name = "issued_quantity")
	private long issuedQuantity;
	
	@Column(name = "delivery_quantity")
	private long deliveryQuantity;
	
	@Column(name = "damage_quantity")
	private long damageQuantity;
	
	@Column(name = "damage_quantity_reason")
	private String damageQuantityReason;
	
	@Column(name = "non_damage_quantity")
	private long nonDamageQuantity;

	@Column(name = "type")
	private String type;
	
	@ManyToOne
	@JoinColumn(name = "return_from_deliveryboy_main_id")
	private ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain;
	
	public ReturnFromDeliveryBoy() {
		// TODO Auto-generated constructor stub
	}

	public ReturnFromDeliveryBoy(OrderUsedProduct product, double sellingRate,
			long returnQuantity, long issuedQuantity, long deliveryQuantity, long damageQuantity,
			long nonDamageQuantity, String type) {
		this.product = product;
		this.sellingRate = sellingRate;
		this.returnQuantity = returnQuantity;
		this.issuedQuantity = issuedQuantity;
		this.deliveryQuantity = deliveryQuantity;
		this.damageQuantity = damageQuantity;
		this.nonDamageQuantity = nonDamageQuantity;
		this.type = type;
	}

	public long getReturnFromDeliveryBoyId() {
		return returnFromDeliveryBoyId;
	}

	public void setReturnFromDeliveryBoyId(long returnFromDeliveryBoyId) {
		this.returnFromDeliveryBoyId = returnFromDeliveryBoyId;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public long getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(long returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public long getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public long getDeliveryQuantity() {
		return deliveryQuantity;
	}

	public void setDeliveryQuantity(long deliveryQuantity) {
		this.deliveryQuantity = deliveryQuantity;
	}

	public long getDamageQuantity() {
		return damageQuantity;
	}

	public void setDamageQuantity(long damageQuantity) {
		this.damageQuantity = damageQuantity;
	}

	public long getNonDamageQuantity() {
		return nonDamageQuantity;
	}

	public void setNonDamageQuantity(long nonDamageQuantity) {
		this.nonDamageQuantity = nonDamageQuantity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ReturnFromDeliveryBoyMain getReturnFromDeliveryBoyMain() {
		return returnFromDeliveryBoyMain;
	}

	public void setReturnFromDeliveryBoyMain(ReturnFromDeliveryBoyMain returnFromDeliveryBoyMain) {
		this.returnFromDeliveryBoyMain = returnFromDeliveryBoyMain;
	}

	public String getDamageQuantityReason() {
		return damageQuantityReason;
	}

	public void setDamageQuantityReason(String damageQuantityReason) {
		this.damageQuantityReason = damageQuantityReason;
	}

	@Override
	public String toString() {
		return "ReturnFromDeliveryBoy [returnFromDeliveryBoyId=" + returnFromDeliveryBoyId + ", product=" + product
				+ ", sellingRate=" + sellingRate + ", returnQuantity=" + returnQuantity + ", issuedQuantity="
				+ issuedQuantity + ", deliveryQuantity=" + deliveryQuantity + ", damageQuantity=" + damageQuantity
				+ ", damageQuantityReason=" + damageQuantityReason + ", nonDamageQuantity=" + nonDamageQuantity
				+ ", type=" + type + ", returnFromDeliveryBoyMain=" + returnFromDeliveryBoyMain + "]";
	}



}
