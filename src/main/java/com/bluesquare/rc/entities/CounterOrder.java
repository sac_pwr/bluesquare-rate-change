package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "counter_order")
@Component
public class CounterOrder {

	@Id
	@Column(name = "counter_order_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long counterOrderPKId;
	
	@Column(name = "counter_order_id")
	private String counterOrderId;

	@ManyToOne
	@JoinColumn(name = "employee_gk_id")
	private Employee employeeGk;

	@ManyToOne
	@JoinColumn(name = "business_id")
	private BusinessName businessName;

	@Column(name = "customer_name")
	private String customerName;

	@Column(name = "customer_mob_number")
	private String customerMobileNumber;

	@Column(name = "customer_gst_number")
	private String customerGstNumber;

	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmount;

	@Column(name = "total_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double totalAmountWithTax;

	@Column(name = "total_quantity")
	private long totalQuantity;
	
	@Column(name = "date_of_order_taken")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date dateOfOrderTaken;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "paid_status")
	private boolean payStatus;

	@Column(name = "payment_due_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date paymentDueDate;

	@Column(name = "invoice_number")
	private String invoiceNumber;

	@ManyToOne
	@JoinColumn(name = "order_status_id")
	private OrderStatus orderStatus;

	
	
	public long getCounterOrderPKId() {
		return counterOrderPKId;
	}

	public void setCounterOrderPKId(long counterOrderPKId) {
		this.counterOrderPKId = counterOrderPKId;
	}

	public String getCounterOrderId() {
		return counterOrderId;
	}

	public void setCounterOrderId(String counterOrderId) {
		this.counterOrderId = counterOrderId;
	}

	public Employee getEmployeeGk() {
		return employeeGk;
	}

	public void setEmployeeGk(Employee employeeGk) {
		this.employeeGk = employeeGk;
	}

	public BusinessName getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerMobileNumber() {
		return customerMobileNumber;
	}

	public void setCustomerMobileNumber(String customerMobileNumber) {
		this.customerMobileNumber = customerMobileNumber;
	}

	public String getCustomerGstNumber() {
		return customerGstNumber;
	}

	public void setCustomerGstNumber(String customerGstNumber) {
		this.customerGstNumber = customerGstNumber;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public Date getDateOfOrderTaken() {
		return dateOfOrderTaken;
	}

	public void setDateOfOrderTaken(Date dateOfOrderTaken) {
		this.dateOfOrderTaken = dateOfOrderTaken;
	}

	public boolean isPayStatus() {
		return payStatus;
	}

	public void setPayStatus(boolean payStatus) {
		this.payStatus = payStatus;
	}

	public Date getPaymentDueDate() {
		return paymentDueDate;
	}

	public void setPaymentDueDate(Date paymentDueDate) {
		this.paymentDueDate = paymentDueDate;
	}

	public String getInvoiceNumber() {
		return invoiceNumber;
	}

	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}

	public OrderStatus getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(OrderStatus orderStatus) {
		this.orderStatus = orderStatus;
	}

	@Override
	public String toString() {
		return "CounterOrder [counterOrderPKId=" + counterOrderPKId + ", counterOrderId=" + counterOrderId
				+ ", employeeGk=" + employeeGk + ", businessName=" + businessName + ", customerName=" + customerName
				+ ", customerMobileNumber=" + customerMobileNumber + ", customerGstNumber=" + customerGstNumber
				+ ", totalAmount=" + totalAmount + ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity="
				+ totalQuantity + ", dateOfOrderTaken=" + dateOfOrderTaken + ", payStatus=" + payStatus
				+ ", paymentDueDate=" + paymentDueDate + ", invoiceNumber=" + invoiceNumber + ", orderStatus="
				+ orderStatus + "]";
	}

	
	
}
