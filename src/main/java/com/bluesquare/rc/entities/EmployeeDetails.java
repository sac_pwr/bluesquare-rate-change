package com.bluesquare.rc.entities;

import java.io.IOException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "employee_details")
@Component

public class EmployeeDetails {

	@Id
	@Column(name = "employee_details_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long employeeDetailsId;
	
	@Column(name = "employee_details_gen_id")
	private String employeeDetailsGenId;

	@Column(name = "name")
	private String name;
	
	@Column(name = "token")
	private String token;

	@Column(name = "address")
	private String address;
	
	@Column(name = "basic_salary", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double basicSalary;
	
	@ManyToOne
	@JoinColumn(name = "employee_id")
	private Employee employee;

	@ManyToOne
	@JoinColumn(name = "contact_id")
	private Contact contact;
	
	@Column(name="employee_details_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date employeeDetailsAddedDatetime;
	
	@Column(name="employee_details_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date employeeDetailsUpdatedDatetime;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="status")
	private boolean status;
	
	@Column(name="employee_details_disable_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date employeeDetailsDisableDatetime;


	@Override
	public String toString() {
		return "EmployeeDetails [employeeDetailsId=" + employeeDetailsId + ", employeeDetailsGenId="
				+ employeeDetailsGenId + ", name=" + name + ", token=" + token + ", address=" + address + ", basicSalary=" + basicSalary + ", employee=" + employee
				+ ", contact=" + contact + ", employeeDetailsAddedDatetime=" + employeeDetailsAddedDatetime
				+ ", employeeDetailsUpdatedDatetime=" + employeeDetailsUpdatedDatetime + ", status=" + status
				+ ", employeeDetailsDisableDatetime=" + employeeDetailsDisableDatetime + "]";
	}

	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}

	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}

	public String getEmployeeDetailsGenId() {
		return employeeDetailsGenId;
	}

	public void setEmployeeDetailsGenId(String employeeDetailsGenId) {
		this.employeeDetailsGenId = employeeDetailsGenId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public void setBasicSalary(double basicSalary) {
		this.basicSalary = basicSalary;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Date getEmployeeDetailsAddedDatetime() {
		return employeeDetailsAddedDatetime;
	}

	public void setEmployeeDetailsAddedDatetime(Date employeeDetailsAddedDatetime) {
		this.employeeDetailsAddedDatetime = employeeDetailsAddedDatetime;
	}

	public Date getEmployeeDetailsUpdatedDatetime() {
		return employeeDetailsUpdatedDatetime;
	}

	public void setEmployeeDetailsUpdatedDatetime(Date employeeDetailsUpdatedDatetime) {
		this.employeeDetailsUpdatedDatetime = employeeDetailsUpdatedDatetime;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getEmployeeDetailsDisableDatetime() {
		return employeeDetailsDisableDatetime;
	}

	public void setEmployeeDetailsDisableDatetime(Date employeeDetailsDisableDatetime) {
		this.employeeDetailsDisableDatetime = employeeDetailsDisableDatetime;
	}

	public static class NumericBooleanSerializer extends JsonSerializer<Boolean> {

		@Override
		public void serialize(Boolean bool, JsonGenerator generator, SerializerProvider provider)
				throws IOException, JsonProcessingException {
			generator.writeString(bool ? "1" : "0");
		}
	}

	public static class NumericBooleanDeserializer extends JsonDeserializer<Boolean> {

		@Override
		public Boolean deserialize(JsonParser parser, DeserializationContext context)
				throws IOException, JsonProcessingException {
			return !"0".equals(parser.getText());
		}
	}
}
