package com.bluesquare.rc.entities;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "business_name")
@Component

public class BusinessName {

	@Id
	@Column(name = "business_name_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long businessNamePkId;
	
	@Column(name = "business_name_id")
	private String businessNameId;

	@Column(name = "shop_name")
	private String shopName;

	@Column(name = "owner_name")
	private String ownerName;

	@Column(name = "comment")
	private String comment;

	@Column(name = "other")
	private String other;

	@Column(name = "credit_limit", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double creditLimit;
	
	@Column(name = "gstin_number")                                                                                                                               
	private String gstinNumber;
	
	@ManyToOne
	@JoinColumn(name = "businesstype_id")
	private BusinessType businessType;

	@Column(name = "address")
	private String address;
	
	@Column(name = "tax_type")
	private String taxType;

	@ManyToOne
	@JoinColumn(name = "contact_id")
	private Contact contact;
	
	@ManyToOne
	@JoinColumn(name="area_id")
	private Area area;
	
	@Column(name="business_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date businessAddedDatetime;
	
	@Column(name="business_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date businessUpdatedDatetime;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="status")
	private boolean status;

	@Column(name="employee_details_disable_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date disableDatetime;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;

	public long getBusinessNamePkId() {
		return businessNamePkId;
	}

	public void setBusinessNamePkId(long businessNamePkId) {
		this.businessNamePkId = businessNamePkId;
	}

	public String getBusinessNameId() {
		return businessNameId;
	}

	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public double getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(double creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getGstinNumber() {
		return gstinNumber;
	}

	public void setGstinNumber(String gstinNumber) {
		this.gstinNumber = gstinNumber;
	}

	public BusinessType getBusinessType() {
		return businessType;
	}

	public void setBusinessType(BusinessType businessType) {
		this.businessType = businessType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}

	public Date getBusinessAddedDatetime() {
		return businessAddedDatetime;
	}

	public void setBusinessAddedDatetime(Date businessAddedDatetime) {
		this.businessAddedDatetime = businessAddedDatetime;
	}

	public Date getBusinessUpdatedDatetime() {
		return businessUpdatedDatetime;
	}

	public void setBusinessUpdatedDatetime(Date businessUpdatedDatetime) {
		this.businessUpdatedDatetime = businessUpdatedDatetime;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getDisableDatetime() {
		return disableDatetime;
	}

	public void setDisableDatetime(Date disableDatetime) {
		this.disableDatetime = disableDatetime;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "BusinessName [businessNamePkId=" + businessNamePkId + ", businessNameId=" + businessNameId
				+ ", shopName=" + shopName + ", ownerName=" + ownerName + ", comment=" + comment + ", other=" + other
				+ ", creditLimit=" + creditLimit + ", gstinNumber=" + gstinNumber + ", businessType=" + businessType
				+ ", address=" + address + ", taxType=" + taxType + ", contact=" + contact + ", area=" + area
				+ ", businessAddedDatetime=" + businessAddedDatetime + ", businessUpdatedDatetime="
				+ businessUpdatedDatetime + ", status=" + status + ", disableDatetime=" + disableDatetime + ", company="
				+ company + "]";
	}
	
	
}
