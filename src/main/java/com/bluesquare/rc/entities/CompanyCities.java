package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "company_cities")
@Component
public class CompanyCities {

	@Id
	@Column(name = "company_city_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long companyId;
	
	@ManyToOne
    @JoinColumn(name = "city_id")
	private City city;
	
	@ManyToOne
    @JoinColumn(name = "company_id")
	private Company company;

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "CompanyCities [companyId=" + companyId + ", city=" + city + ", company=" + company + "]";
	}
	
	
}
