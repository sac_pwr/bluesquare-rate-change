package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "city")
@Component

public class City {

	@Id
	@Column(name = "city_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long cityId;
	
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne
    @JoinColumn(name = "state_id")
	private State state;

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
/*
	public Set<Region> getRegion() {
		return region;
	}

	public void setRegion(Set<Region> region) {
		this.region = region;
	}*/

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "City [cityId=" + cityId + ", name=" + name + ", state=" + state + "]";
	}
	
	
	
	

}