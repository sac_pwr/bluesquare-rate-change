package com.bluesquare.rc.entities;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "department")
@Component

public class Department {
	
	@Id
	@Column(name = "department_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long departmentId;
	
	/*@OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
	private Set<Employee> employee ;*/
	
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "short_name_for_employee_id")
	private String shortNameForEmployeeId;
	
	@Column(name = "details")
	private String details;
	
	@Column(name="department_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date departmentAddedDatetime;

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getDepartmentAddedDatetime() {
		return departmentAddedDatetime;
	}

	public void setDepartmentAddedDatetime(Date departmentAddedDatetime) {
		this.departmentAddedDatetime = departmentAddedDatetime;
	}



	@Override
	public String toString() {
		return "Department [departmentId=" + departmentId + ", name=" + name + ", shortNameForEmployeeId="
				+ shortNameForEmployeeId + ", details=" + details + ", departmentAddedDatetime="
				+ departmentAddedDatetime + "]";
	}

	public String getShortNameForEmployeeId() {
		return shortNameForEmployeeId;
	}

	public void setShortNameForEmployeeId(String shortNameForEmployeeId) {
		this.shortNameForEmployeeId = shortNameForEmployeeId;
	}

	

	

	/*public Set<Employee> getEmployee() {
		return employee;
	}

	public void setEmployee(Set<Employee> employee) {
		this.employee = employee;
	}*/
	
	
	
	

}
