package com.bluesquare.rc.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "region")
@Component

public class Region {

	@Id
	@Column(name = "region_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long regionId;
	
	@Column(name = "name")
	private String name;
	
	@ManyToOne
    @JoinColumn(name = "city_id")
	private City city;
	
	@ManyToOne
    @JoinColumn(name = "company_id")
	private Company company;

	public long getRegionId() {
		return regionId;
	}

	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "Region [regionId=" + regionId + ", name=" + name + ", city=" + city + ", company=" + company + "]";
	}

}