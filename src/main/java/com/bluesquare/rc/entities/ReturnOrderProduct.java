package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "return_order_product")
@Component
public class ReturnOrderProduct {
	@Id
	@Column(name = "return_order_product_pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long returnOrderProductPkId;
	
	@Column(name = "return_order_product_id")
	private String returnOrderProductId;
	
	@ManyToOne
	@JoinColumn(name = "return_emp_id")
	private Employee employee;
	
	@ManyToOne
	@JoinColumn(name = "order_details_id")
	private OrderDetails orderDetails;
	
	@Column(name="return_order_product_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date returnOrderProductDatetime;
	
	@Column(name = "total_amount", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmount;
	
	@Column(name = "total_amount_with_tax", precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double totalAmountWithTax;

	@Column(name = "total_quantity")
	private long totalQuantity;
	
	@Column(name = "reissue_status")
	private String reIssueStatus;

	public long getReturnOrderProductPkId() {
		return returnOrderProductPkId;
	}

	public void setReturnOrderProductPkId(long returnOrderProductPkId) {
		this.returnOrderProductPkId = returnOrderProductPkId;
	}

	public String getReturnOrderProductId() {
		return returnOrderProductId;
	}

	public void setReturnOrderProductId(String returnOrderProductId) {
		this.returnOrderProductId = returnOrderProductId;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Date getReturnOrderProductDatetime() {
		return returnOrderProductDatetime;
	}

	public void setReturnOrderProductDatetime(Date returnOrderProductDatetime) {
		this.returnOrderProductDatetime = returnOrderProductDatetime;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}

	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}

	public long getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getReIssueStatus() {
		return reIssueStatus;
	}

	public void setReIssueStatus(String reIssueStatus) {
		this.reIssueStatus = reIssueStatus;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@Override
	public String toString() {
		return "ReturnOrderProduct [returnOrderProductPkId=" + returnOrderProductPkId + ", returnOrderProductId="
				+ returnOrderProductId + ", employee=" + employee + ", orderDetails=" + orderDetails
				+ ", returnOrderProductDatetime=" + returnOrderProductDatetime + ", totalAmount=" + totalAmount
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", totalQuantity=" + totalQuantity + ", reIssueStatus="
				+ reIssueStatus + "]";
	}
}
