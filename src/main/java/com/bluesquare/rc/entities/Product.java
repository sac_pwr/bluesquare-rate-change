package com.bluesquare.rc.entities;

import java.sql.Blob;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Check;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product")
@Component
public class Product {

	@Id
	@Column(name = "product_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long productId;

	@Column(name = "product_name")
	private String productName;

	@Column(name = "product_code")
	private String productCode;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private Categories categories;

	@ManyToOne
	@JoinColumn(name = "brand_id")
	private Brand brand;

	@Column(name = "rate", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private float rate;

	@JsonIgnore //ignore this field when json create for response
	@Column(name = "product_image")
	private Blob productImage;

	@Column(name = "product_content_type")
	private String productContentType;
	
	@Column(name = "product_description")
	private String productDescription;

	@Column(name = "threshold")
	private long threshold;

	@Column(name = "current_quantity")
	private long currentQuantity;
	
	@Column(name = "damage_quantity")
	private long damageQuantity;
	
	@Column(name = "free_quantity")
	private long freeQuantity;

	@Column(name = "product_added_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date productAddedDatetime;

	@Column(name = "product_quantity_updated_datetime")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date productQuantityUpdatedDatetime;
	
	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public Blob getProductImage() {
		return productImage;
	}

	public void setProductImage(Blob productImage) {
		this.productImage = productImage;
	}

   /* // serialize as data uri instead
    @JsonProperty("productImage")
    public String getPhotoBase64() {
      // just assuming it is a jpeg. you would need to cater for different media types
      return "data:image/jpeg;base64," + new String(ImageConvertor.convertBlobToString(getProductImage()));
    }*/

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public long getThreshold() {
		return threshold;
	}

	public void setThreshold(long threshold) {
		this.threshold = threshold;
	}

	public long getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(long currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public long getDamageQuantity() {
		return damageQuantity;
	}

	public void setDamageQuantity(long damageQuantity) {
		this.damageQuantity = damageQuantity;
	}

	public Date getProductAddedDatetime() {
		return productAddedDatetime;
	}

	public void setProductAddedDatetime(Date productAddedDatetime) {
		this.productAddedDatetime = productAddedDatetime;
	}

	public Date getProductQuantityUpdatedDatetime() {
		return productQuantityUpdatedDatetime;
	}

	public void setProductQuantityUpdatedDatetime(Date productQuantityUpdatedDatetime) {
		this.productQuantityUpdatedDatetime = productQuantityUpdatedDatetime;
	}

	public String getProductContentType() {
		return productContentType;
	}

	public void setProductContentType(String productContentType) {
		this.productContentType = productContentType;
	}

	public long getFreeQuantity() {
		return freeQuantity;
	}

	public void setFreeQuantity(long freeQuantity) {
		this.freeQuantity = freeQuantity;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", productName=" + productName + ", productCode=" + productCode
				+ ", categories=" + categories + ", brand=" + brand + ", rate=" + rate + ", productImage="
				+ productImage + ", productContentType=" + productContentType + ", productDescription="
				+ productDescription + ", threshold=" + threshold + ", currentQuantity=" + currentQuantity
				+ ", damageQuantity=" + damageQuantity + ", freeQuantity=" + freeQuantity + ", productAddedDatetime="
				+ productAddedDatetime + ", productQuantityUpdatedDatetime=" + productQuantityUpdatedDatetime
				+ ", company=" + company + "]";
	}
}
