package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "counter_order_product_details")
@Component
public class CounterOrderProductDetails {

	@Id
	@Column(name = "counter_order_product_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long counterOrderProductId;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private OrderUsedProduct product;

	@Column(name = "purchase_quantity")
	private long purchaseQuantity;

	@Column(name = "selling_rate", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double sellingRate;

	@Column(name = "purchase_amount", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double purchaseAmount;

	@Column(name = "type")
	private String type;

	@ManyToOne
	@JoinColumn(name = "counter_order_id")
	private CounterOrder counterOrder;

	public long getCounterOrderProductId() {
		return counterOrderProductId;
	}

	public void setCounterOrderProductId(long counterOrderProductId) {
		this.counterOrderProductId = counterOrderProductId;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public long getPurchaseQuantity() {
		return purchaseQuantity;
	}

	public void setPurchaseQuantity(long purchaseQuantity) {
		this.purchaseQuantity = purchaseQuantity;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public double getPurchaseAmount() {
		return purchaseAmount;
	}

	public void setPurchaseAmount(double purchaseAmount) {
		this.purchaseAmount = purchaseAmount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public CounterOrder getCounterOrder() {
		return counterOrder;
	}

	public void setCounterOrder(CounterOrder counterOrder) {
		this.counterOrder = counterOrder;
	}

	@Override
	public String toString() {
		return "CounterOrderProductDetails [counterOrderProductId=" + counterOrderProductId + ", product=" + product
				+ ", purchaseQuantity=" + purchaseQuantity + ", sellingRate=" + sellingRate + ", purchaseAmount="
				+ purchaseAmount + ", type=" + type + ", counterOrder=" + counterOrder + "]";
	}
}
