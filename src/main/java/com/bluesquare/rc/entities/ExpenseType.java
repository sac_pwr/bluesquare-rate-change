package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.EmployeeDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "expense_type")
@Component
public class ExpenseType {

	@Id
	@Column(name = "expense_type_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long expenseTypeId;

	@Column(name = "name")
	private String name;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name="status")
	private boolean status;
	
	@Column(name = "expense_type_added_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date addedDate;
	
	@Column(name = "expense_type_updated_date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date updatedDate;

	public long getExpenseTypeId() {
		return expenseTypeId;
	}

	public void setExpenseTypeId(long expenseTypeId) {
		this.expenseTypeId = expenseTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Override
	public String toString() {
		return "ExpenseType [expenseTypeId=" + expenseTypeId + ", name=" + name + ", company=" + company + ", status="
				+ status + ", addedDate=" + addedDate + ", updatedDate=" + updatedDate + "]";
	}
}
