/**
 * 
 */
package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import com.bluesquare.rc.entities.OrderDetails.NumericBooleanDeserializer;
import com.bluesquare.rc.entities.OrderDetails.NumericBooleanSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author aNKIT
 *
 */
@Entity
@Table(name = "return_from_delivery_boy_main")
@Component
public class ReturnFromDeliveryBoyMain {

	@Id
	@Column(name = "pk_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long returnFromDeliveryBoyMainPkId;
	
	@Column(name = "id")
	private String returnFromDeliveryBoyMainId;

	@Column(name = "total_return_quantity")
	private long totalReturnQuantity;

	@Column(name = "total_issued_quantity")
	private long totalIssuedQuantity;

	@Column(name = "total_delivery_quantity")
	private long totalDeliveryQuantity;

	@Column(name = "total_damage_quantity")
	private long totalDamageQuantity;

	@Column(name = "total_non_damage_quantity")
	private long totalNonDamageQuantity;

	@ManyToOne
	@JoinColumn(name = "gatekeeper_id")
	private Employee employeeGK;

	@ManyToOne
	@JoinColumn(name = "deliveryboy_id")
	private Employee employeeDB;

	@ManyToOne
	@JoinColumn(name = "order_id")
	private OrderDetails orderDetails;

	@Column(name = "date")
	private Date date;

	@JsonProperty
	@JsonSerialize(using = NumericBooleanSerializer.class)
	@JsonDeserialize(using = NumericBooleanDeserializer.class)
	@Column(name = "received_status")
	private boolean receivedStatus;

	/**
	 * 
	 * Feb 2, 20187:09:36 PM
	 */
	public ReturnFromDeliveryBoyMain() {
		// TODO Auto-generated constructor stub
	}
			
	public ReturnFromDeliveryBoyMain(long totalReturnQuantity, long totalIssuedQuantity, long totalDeliveryQuantity,
			long totalDamageQuantity, long totalNonDamageQuantity, Employee employeeGK, Employee employeeDB,
			OrderDetails orderDetails, Date date, boolean receivedStatus) {
		super();
		this.totalReturnQuantity = totalReturnQuantity;
		this.totalIssuedQuantity = totalIssuedQuantity;
		this.totalDeliveryQuantity = totalDeliveryQuantity;
		this.totalDamageQuantity = totalDamageQuantity;
		this.totalNonDamageQuantity = totalNonDamageQuantity;
		this.employeeGK = employeeGK;
		this.employeeDB = employeeDB;
		this.orderDetails = orderDetails;
		this.date = date;
		this.receivedStatus = receivedStatus;
	}

	public long getReturnFromDeliveryBoyMainPkId() {
		return returnFromDeliveryBoyMainPkId;
	}

	public void setReturnFromDeliveryBoyMainPkId(long returnFromDeliveryBoyMainPkId) {
		this.returnFromDeliveryBoyMainPkId = returnFromDeliveryBoyMainPkId;
	}

	public String getReturnFromDeliveryBoyMainId() {
		return returnFromDeliveryBoyMainId;
	}

	public void setReturnFromDeliveryBoyMainId(String returnFromDeliveryBoyMainId) {
		this.returnFromDeliveryBoyMainId = returnFromDeliveryBoyMainId;
	}

	public long getTotalReturnQuantity() {
		return totalReturnQuantity;
	}

	public void setTotalReturnQuantity(long totalReturnQuantity) {
		this.totalReturnQuantity = totalReturnQuantity;
	}

	public long getTotalIssuedQuantity() {
		return totalIssuedQuantity;
	}

	public void setTotalIssuedQuantity(long totalIssuedQuantity) {
		this.totalIssuedQuantity = totalIssuedQuantity;
	}

	public long getTotalDeliveryQuantity() {
		return totalDeliveryQuantity;
	}

	public void setTotalDeliveryQuantity(long totalDeliveryQuantity) {
		this.totalDeliveryQuantity = totalDeliveryQuantity;
	}

	public long getTotalDamageQuantity() {
		return totalDamageQuantity;
	}

	public void setTotalDamageQuantity(long totalDamageQuantity) {
		this.totalDamageQuantity = totalDamageQuantity;
	}

	public long getTotalNonDamageQuantity() {
		return totalNonDamageQuantity;
	}

	public void setTotalNonDamageQuantity(long totalNonDamageQuantity) {
		this.totalNonDamageQuantity = totalNonDamageQuantity;
	}

	public Employee getEmployeeGK() {
		return employeeGK;
	}

	public void setEmployeeGK(Employee employeeGK) {
		this.employeeGK = employeeGK;
	}

	public Employee getEmployeeDB() {
		return employeeDB;
	}

	public void setEmployeeDB(Employee employeeDB) {
		this.employeeDB = employeeDB;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isReceivedStatus() {
		return receivedStatus;
	}

	public void setReceivedStatus(boolean receivedStatus) {
		this.receivedStatus = receivedStatus;
	}

	@Override
	public String toString() {
		return "ReturnFromDeliveryBoyMain [returnFromDeliveryBoyMainPkId=" + returnFromDeliveryBoyMainPkId
				+ ", returnFromDeliveryBoyMainId=" + returnFromDeliveryBoyMainId + ", totalReturnQuantity="
				+ totalReturnQuantity + ", totalIssuedQuantity=" + totalIssuedQuantity + ", totalDeliveryQuantity="
				+ totalDeliveryQuantity + ", totalDamageQuantity=" + totalDamageQuantity + ", totalNonDamageQuantity="
				+ totalNonDamageQuantity + ", employeeGK=" + employeeGK + ", employeeDB=" + employeeDB
				+ ", orderDetails=" + orderDetails + ", date=" + date + ", receivedStatus=" + receivedStatus + "]";
	}
}
