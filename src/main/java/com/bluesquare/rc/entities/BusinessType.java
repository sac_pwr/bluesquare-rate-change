package com.bluesquare.rc.entities;



import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "business_type")
@Component

public class BusinessType{

	@Id
	@Column(name = "businesstype_id")
	@GeneratedValue(strategy =GenerationType.AUTO)
	private long businessTypeId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "details")
	private String details;
	
	@Column(name="business_type_added_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date businessTypeAddedDatetime;
	
	@Column(name="business_type_updated_datetime")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date businessTypeUpdatedDatetime;

	@ManyToOne
	@JoinColumn(name = "company_id")
	private Company company;
	
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public long getBusinessTypeId() {
		return businessTypeId;
	}

	public void setBusinessTypeId(long businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public Date getBusinessTypeAddedDatetime() {
		return businessTypeAddedDatetime;
	}

	public void setBusinessTypeAddedDatetime(Date businessTypeAddedDatetime) {
		this.businessTypeAddedDatetime = businessTypeAddedDatetime;
	}

	public Date getBusinessTypeUpdatedDatetime() {
		return businessTypeUpdatedDatetime;
	}

	public void setBusinessTypeUpdatedDatetime(Date businessTypeUpdatedDatetime) {
		this.businessTypeUpdatedDatetime = businessTypeUpdatedDatetime;
	}

	@Override
	public String toString() {
		return "BusinessType [businessTypeId=" + businessTypeId + ", name=" + name + ", details=" + details
				+ ", businessTypeAddedDatetime=" + businessTypeAddedDatetime + ", businessTypeUpdatedDatetime="
				+ businessTypeUpdatedDatetime + ", company=" + company + "]";
	}

		
}
