package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "order_product_issue_details")
@Component
public class OrderProductIssueDetails {

	@Id
	@Column(name = "order_issue_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long orderIssueId;

	@ManyToOne
	@JoinColumn(name = "order_id")
	private OrderDetails orderDetails;

	@ManyToOne
	@JoinColumn(name = "issued_to_db")
	private Employee employeeDB;
	
	@ManyToOne
	@JoinColumn(name = "issued_by_gk")
	private Employee employeeGK;

	public long getOrderIssueId() {
		return orderIssueId;
	}

	public void setOrderIssueId(long orderIssueId) {
		this.orderIssueId = orderIssueId;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public Employee getEmployeeDB() {
		return employeeDB;
	}

	public void setEmployeeDB(Employee employeeDB) {
		this.employeeDB = employeeDB;
	}

	public Employee getEmployeeGK() {
		return employeeGK;
	}

	public void setEmployeeGK(Employee employeeGK) {
		this.employeeGK = employeeGK;
	}

	@Override
	public String toString() {
		return "OrderProductIssueDetails [orderIssueId=" + orderIssueId + ", orderDetails=" + orderDetails
				+ ", employeeDB=" + employeeDB + ", employeeGK=" + employeeGK + "]";
	}

}
