package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "ledger")
@Component
public class Ledger{
	
	@Id
	@Column(name="ledger_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long ledgerId;
	
	@Column(name="pay_mode")
	private String payMode;
	
	@Column(name="date_time")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dateTime;
	
	@Column(name="cheque_date")
	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date chequeDate;
	
	@Column(name="description")
	private String description;
	
	@Column(name="reference")
	private String reference;
	
	@Column(name="debit" ,precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double debit;
	
	@Column(name="credit" ,precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double credit;
	
	@Column(name="balance" ,precision = 19, scale = 2, columnDefinition="DECIMAL(19,2)")
	private double balance;

	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;
	
	@ManyToOne
	@JoinColumn(name="payment_supplier_id")
	private PaymentPaySupplier paymentPaySupplier;
	
	@ManyToOne
	@JoinColumn(name="payment_counter_id")
	private PaymentCounter paymentCounter;
	
	@ManyToOne
	@JoinColumn(name="payment_order_id")
	private Payment payment;
	
	@ManyToOne
	@JoinColumn(name="employee_salary_id")
	private EmployeeSalary employeeSalary;
	
	@ManyToOne
	@JoinColumn(name="expense_id")
	private Expense expense;

	public long getLedgerId() {
		return ledgerId;
	}

	public void setLedgerId(long ledgerId) {
		this.ledgerId = ledgerId;
	}

	public String getPayMode() {
		return payMode;
	}

	public void setPayMode(String payMode) {
		this.payMode = payMode;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public Date getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public double getDebit() {
		return debit;
	}

	public void setDebit(double debit) {
		this.debit = debit;
	}

	public double getCredit() {
		return credit;
	}

	public void setCredit(double credit) {
		this.credit = credit;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public PaymentPaySupplier getPaymentPaySupplier() {
		return paymentPaySupplier;
	}

	public void setPaymentPaySupplier(PaymentPaySupplier paymentPaySupplier) {
		this.paymentPaySupplier = paymentPaySupplier;
	}

	public PaymentCounter getPaymentCounter() {
		return paymentCounter;
	}

	public void setPaymentCounter(PaymentCounter paymentCounter) {
		this.paymentCounter = paymentCounter;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public EmployeeSalary getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(EmployeeSalary employeeSalary) {
		this.employeeSalary = employeeSalary;
	}

	public Expense getExpense() {
		return expense;
	}

	public void setExpense(Expense expense) {
		this.expense = expense;
	}
	
	public Ledger() {
		// TODO Auto-generated constructor stub
	}
	
	public Ledger(String payMode, Date dateTime, Date chequeDate, String description, String reference,
			double debit, double credit, double balance, PaymentPaySupplier paymentPaySupplier) {
		
		this.payMode = payMode;
		this.dateTime = dateTime;
		this.chequeDate = chequeDate;
		this.description = description;
		this.reference = reference;
		this.debit = debit;
		this.credit = credit;
		this.balance = balance;
		this.paymentPaySupplier = paymentPaySupplier;
	}	

	public Ledger( String payMode, Date dateTime, Date chequeDate, String description, String reference,
			double debit, double credit, double balance, PaymentCounter paymentCounter) {
		
		this.payMode = payMode;
		this.dateTime = dateTime;
		this.chequeDate = chequeDate;
		this.description = description;
		this.reference = reference;
		this.debit = debit;
		this.credit = credit;
		this.balance = balance;
		this.paymentCounter = paymentCounter;
	}

	public Ledger( String payMode, Date dateTime, Date chequeDate, String description, String reference,
			double debit, double credit, double balance, Payment payment) {
		
		this.payMode = payMode;
		this.dateTime = dateTime;
		this.chequeDate = chequeDate;
		this.description = description;
		this.reference = reference;
		this.debit = debit;
		this.credit = credit;
		this.balance = balance;
		this.payment = payment;
	}
	
	public Ledger( String payMode, Date dateTime, Date chequeDate, String description, String reference,
			double debit, double credit, double balance, EmployeeSalary employeeSalary) {
		
		this.payMode = payMode;
		this.dateTime = dateTime;
		this.chequeDate = chequeDate;
		this.description = description;
		this.reference = reference;
		this.debit = debit;
		this.credit = credit;
		this.balance = balance;
		this.employeeSalary = employeeSalary;
	}
	
	public Ledger( String payMode, Date dateTime, Date chequeDate, String description, String reference,
			double debit, double credit, double balance, Expense expense) {
		
		this.payMode = payMode;
		this.dateTime = dateTime;
		this.chequeDate = chequeDate;
		this.description = description;
		this.reference = reference;
		this.debit = debit;
		this.credit = credit;
		this.balance = balance;
		this.expense = expense;
	}

	
	@Override
	public String toString() {
		return "Ledger [ledgerId=" + ledgerId + ", payMode=" + payMode + ", dateTime=" + dateTime + ", chequeDate="
				+ chequeDate + ", description=" + description + ", reference=" + reference + ", debit=" + debit
				+ ", credit=" + credit + ", balance=" + balance + ", company=" + company + ", paymentPaySupplier="
				+ paymentPaySupplier + ", paymentCounter=" + paymentCounter + ", payment=" + payment
				+ ", employeeSalary=" + employeeSalary + ", expense=" + expense + "]";
	}
}
