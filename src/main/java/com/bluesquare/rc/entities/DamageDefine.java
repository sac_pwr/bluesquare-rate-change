package com.bluesquare.rc.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
@Table(name = "damage_define")
@Component
public class DamageDefine {

	@Id
	@Column(name = "damage_define_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long damageDefineId;
	
	@Column(name = "order_id_or_gk_name")
	private String orderId;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@Column(name = "collecting_person")
	private String collectingPerson;
	
	@Column(name = "dept_name")
	private String departmentName;
	
	@Column(name = "qty")
	private long qty;
	
	@Column(name = "date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	
	@Column(name = "reason")
	private String reason;
	
	public DamageDefine() {
		// TODO Auto-generated constructor stub
	}

	public DamageDefine( String orderId, Product product, String collectingPerson,
			String departmentName, long qty, Date date, String reason) {

		this.orderId = orderId;
		this.product = product;
		this.collectingPerson = collectingPerson;
		this.departmentName = departmentName;
		this.qty = qty;
		this.date = date;
		this.reason = reason;
	}

	public long getDamageDefineId() {
		return damageDefineId;
	}

	public void setDamageDefineId(long damageDefineId) {
		this.damageDefineId = damageDefineId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getCollectingPerson() {
		return collectingPerson;
	}

	public void setCollectingPerson(String collectingPerson) {
		this.collectingPerson = collectingPerson;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public long getQty() {
		return qty;
	}

	public void setQty(long qty) {
		this.qty = qty;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		return "DamageDefine [damageDefineId=" + damageDefineId + ", orderId=" + orderId + ", product=" + product
				+ ", collectingPerson=" + collectingPerson + ", departmentName=" + departmentName + ", qty=" + qty
				+ ", date=" + date + ", reason=" + reason + "]";
	}
	
}
