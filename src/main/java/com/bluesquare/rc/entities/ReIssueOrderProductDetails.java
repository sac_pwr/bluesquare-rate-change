package com.bluesquare.rc.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Entity
@Table(name = "reissue_order_product_details")
@Component

public class ReIssueOrderProductDetails {
	@Id
	@Column(name = "reissue_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long reIssueOrderProductDetailsid;

	@Column(name = "reissue_quantity")
	private long reIssueQuantity;

	@Column(name = "return_quantity")
	private long returnQuantity;

	@Column(name = "details")
	private String details;

	@Column(name = "issued_quantity")
	private long issuedQuantity;

	@Column(name = "reissue_amount_with_tax", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double reIssueAmountWithTax;

	@Column(name = "selling_rate", precision = 19, scale = 2, columnDefinition = "DECIMAL(19,2)")
	private double sellingRate;

	@ManyToOne
	@JoinColumn(name = "product_id")
	private OrderUsedProduct product;

	@Column(name = "type")
	private String type;

	// ReIssueOrderDetails
	@ManyToOne
	@JoinColumn(name = "reIssue_order_details_id")
	private ReIssueOrderDetails reIssueOrderDetails;

	public long getReIssueOrderProductDetailsid() {
		return reIssueOrderProductDetailsid;
	}

	public void setReIssueOrderProductDetailsid(long reIssueOrderProductDetailsid) {
		this.reIssueOrderProductDetailsid = reIssueOrderProductDetailsid;
	}

	public long getReIssueQuantity() {
		return reIssueQuantity;
	}

	public void setReIssueQuantity(long reIssueQuantity) {
		this.reIssueQuantity = reIssueQuantity;
	}

	public long getReturnQuantity() {
		return returnQuantity;
	}

	public void setReturnQuantity(long returnQuantity) {
		this.returnQuantity = returnQuantity;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public long getIssuedQuantity() {
		return issuedQuantity;
	}

	public void setIssuedQuantity(long issuedQuantity) {
		this.issuedQuantity = issuedQuantity;
	}

	public double getReIssueAmountWithTax() {
		return reIssueAmountWithTax;
	}

	public void setReIssueAmountWithTax(double reIssueAmountWithTax) {
		this.reIssueAmountWithTax = reIssueAmountWithTax;
	}

	public double getSellingRate() {
		return sellingRate;
	}

	public void setSellingRate(double sellingRate) {
		this.sellingRate = sellingRate;
	}

	public OrderUsedProduct getProduct() {
		return product;
	}

	public void setProduct(OrderUsedProduct product) {
		this.product = product;
	}

	public ReIssueOrderDetails getReIssueOrderDetails() {
		return reIssueOrderDetails;
	}

	public void setReIssueOrderDetails(ReIssueOrderDetails reIssueOrderDetails) {
		this.reIssueOrderDetails = reIssueOrderDetails;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ReIssueOrderProductDetails [reIssueOrderProductDetailsid=" + reIssueOrderProductDetailsid
				+ ", reIssueQuantity=" + reIssueQuantity + ", returnQuantity=" + returnQuantity + ", details=" + details
				+ ", issuedQuantity=" + issuedQuantity + ", reIssueAmountWithTax=" + reIssueAmountWithTax
				+ ", sellingRate=" + sellingRate + ", product=" + product + ", type=" + type + ", reIssueOrderDetails="
				+ reIssueOrderDetails + "]";
	}

}
