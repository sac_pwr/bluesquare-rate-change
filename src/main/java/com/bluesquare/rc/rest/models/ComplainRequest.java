package com.bluesquare.rc.rest.models;

public class ComplainRequest {

	private long employeeId;
	private String fromDate;
	private String toDate;
	
	
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	@Override
	public String toString() {
		return "ComplainRequest [employeeId=" + employeeId + ", fromDate=" + fromDate + ", toDate=" + toDate + "]";
	}
	
	
	
}
