package com.bluesquare.rc.rest.models;

public class OrderDetailsPaymentList {

	private String orderId;
	private double totalAmount;
	private double paidAmount;
	private double balanceAmount;
	private String status;
	
	
	
	public OrderDetailsPaymentList(String orderId, double totalAmount, double paidAmount, double balanceAmount,
			String status) {
		super();
		this.orderId = orderId;
		this.totalAmount = totalAmount;
		this.paidAmount = paidAmount;
		this.balanceAmount = balanceAmount;
		this.status = status;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public double getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(double balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "OrderDetailsPaymentList [orderId=" + orderId + ", totalAmount=" + totalAmount + ", paidAmount="
				+ paidAmount + ", balanceAmount=" + balanceAmount + ", status=" + status + "]";
	}

	
}
