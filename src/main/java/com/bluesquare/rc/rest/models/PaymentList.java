package com.bluesquare.rc.rest.models;

import java.util.Date;

public class PaymentList {

	private double amountPaid;
	private Date paidDate;
	private String payType;
	private String bankName;
	private String chequeNo;
	private Date chequeDate;
	private String employeeName;
	public PaymentList(double amountPaid, Date paidDate, String payType, String bankName, String chequeNo,
			Date chequeDate, String employeeName) {
		super();
		this.amountPaid = amountPaid;
		this.paidDate = paidDate;
		this.payType = payType;
		this.bankName = bankName;
		this.chequeNo = chequeNo;
		this.chequeDate = chequeDate;
		this.employeeName = employeeName;
	}
	public double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	public Date getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	@Override
	public String toString() {
		return "PaymentList [amountPaid=" + amountPaid + ", paidDate=" + paidDate + ", payType=" + payType
				+ ", bankName=" + bankName + ", chequeNo=" + chequeNo + ", chequeDate=" + chequeDate + ", employeeName="
				+ employeeName + "]";
	}

	
}
