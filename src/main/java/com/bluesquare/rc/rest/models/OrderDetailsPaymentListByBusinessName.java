package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.BusinessName;

public class OrderDetailsPaymentListByBusinessName extends BaseDomain {

	private List<OrderDetailsPaymentList> orderDetailsPaymentList;
	private BusinessName businessName;
	private String salesManName;
	public List<OrderDetailsPaymentList> getOrderDetailsPaymentList() {
		return orderDetailsPaymentList;
	}
	public void setOrderDetailsPaymentList(List<OrderDetailsPaymentList> orderDetailsPaymentList) {
		this.orderDetailsPaymentList = orderDetailsPaymentList;
	}
	public BusinessName getBusinessName() {
		return businessName;
	}
	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}
	public String getSalesManName() {
		return salesManName;
	}
	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}
	@Override
	public String toString() {
		return "OrderDetailsPaymentListByBusinessName [orderDetailsPaymentList=" + orderDetailsPaymentList
				+ ", businessName=" + businessName + ", salesManName=" + salesManName + "]";
	}

	
}
