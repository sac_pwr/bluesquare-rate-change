/**
 * 
 */
package com.bluesquare.rc.rest.models;

/**
 * @author aNKIT
 *
 */
public class ReIssueDelivered {
	
	private long reIssueOrderId;
	private String signBase64;
	public long getReIssueOrderId() {
		return reIssueOrderId;
	}
	public void setReIssueOrderId(long reIssueOrderId) {
		this.reIssueOrderId = reIssueOrderId;
	}
	public String getSignBase64() {
		return signBase64;
	}
	public void setSignBase64(String signBase64) {
		this.signBase64 = signBase64;
	}
	@Override
	public String toString() {
		return "ReIssueDelivered [reIssueOrderId=" + reIssueOrderId + ", signBase64=" + signBase64 + "]";
	}

}
