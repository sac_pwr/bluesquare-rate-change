package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.OrderProductIssueDetails;

public class OrderProductIssueListForIssueReportResponse extends BaseDomain{
	
	private String salesman;
	private String deliveryBoy;
	private OrderProductIssueDetails orderProductIssueDetails;
	private List<OrderProductDetails> orderProductDetailsList;
	
	public String getSalesman() {
		return salesman;
	}
	public void setSalesman(String salesman) {
		this.salesman = salesman;
	}
	public String getDeliveryBoy() {
		return deliveryBoy;
	}
	public void setDeliveryBoy(String deliveryBoy) {
		this.deliveryBoy = deliveryBoy;
	}
	public OrderProductIssueDetails getOrderProductIssueDetails() {
		return orderProductIssueDetails;
	}
	public void setOrderProductIssueDetails(OrderProductIssueDetails orderProductIssueDetails) {
		this.orderProductIssueDetails = orderProductIssueDetails;
	}
	public List<OrderProductDetails> getOrderProductDetailsList() {
		return orderProductDetailsList;
	}
	public void setOrderProductDetailsList(List<OrderProductDetails> orderProductDetailsList) {
		this.orderProductDetailsList = orderProductDetailsList;
	}
	@Override
	public String toString() {
		return "OrderProductIssueListForIssueReportResponse [salesman=" + salesman + ", deliveryBoy=" + deliveryBoy
				+ ", orderProductIssueDetails=" + orderProductIssueDetails + ", orderProductDetailsList="
				+ orderProductDetailsList + "]";
	}
	
		

}
