package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.Employee;

public class LoginResponse extends BaseDomain{
	
	public Employee employee;
	private String employeeName;
	private String token;
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	@Override
	public String toString() {
		return "LoginResponse [employee=" + employee + ", employeeName=" + employeeName + ", token=" + token + "]";
	}
	
	
	

}
