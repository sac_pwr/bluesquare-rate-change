package com.bluesquare.rc.rest.models;

import java.util.Date;

public class CollectionReportResponse extends BaseDomain{
	private String shopName;
	private String mobileNumber;
	private String orderId;
	
	private Date paymentDate;
	private double amountDue;
	private double amountPaid;
	private double totalAmountWithTax;

	
	private Date nextDueDate;
	
	private String paymentType;
	private String bankName;
	private String chequeNo;
	private Date chequeDate;
	public CollectionReportResponse() {
		// TODO Auto-generated constructor stub
	}
	public CollectionReportResponse(String shopName, String mobileNumber, String orderId, Date paymentDate,
			double amountDue, double amountPaid, double totalAmountWithTax, Date nextDueDate, String paymentType,
			String bankName, String chequeNo, Date chequeDate) {
		super();
		this.shopName = shopName;
		this.mobileNumber = mobileNumber;
		this.orderId = orderId;
		this.paymentDate = paymentDate;
		this.amountDue = amountDue;
		this.amountPaid = amountPaid;
		this.totalAmountWithTax = totalAmountWithTax;
		this.nextDueDate = nextDueDate;
		this.paymentType = paymentType;
		this.bankName = bankName;
		this.chequeNo = chequeNo;
		this.chequeDate = chequeDate;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public double getAmountDue() {
		return amountDue;
	}
	public void setAmountDue(double amountDue) {
		this.amountDue = amountDue;
	}
	public double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}
	public Date getNextDueDate() {
		return nextDueDate;
	}
	public void setNextDueDate(Date nextDueDate) {
		this.nextDueDate = nextDueDate;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	public Date getChequeDate() {
		return chequeDate;
	}
	public void setChequeDate(Date chequeDate) {
		this.chequeDate = chequeDate;
	}
	@Override
	public String toString() {
		return "CollectionReportResponse [shopName=" + shopName + ", mobileNumber=" + mobileNumber + ", orderId="
				+ orderId + ", paymentDate=" + paymentDate + ", amountDue=" + amountDue + ", amountPaid=" + amountPaid
				+ ", totalAmountWithTax=" + totalAmountWithTax + ", nextDueDate=" + nextDueDate + ", paymentType="
				+ paymentType + ", bankName=" + bankName + ", chequeNo=" + chequeNo + ", chequeDate=" + chequeDate
				+ "]";
	}

	
	

}
