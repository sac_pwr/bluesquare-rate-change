package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.ReturnOrderProduct;

public class ReturnOrderDateRangeResponse extends BaseDomain{
	
	public List<ReturnOrderProduct> returnOrderProductList;
	
	public List<Area> areaList;

	public List<ReturnOrderProduct> getReturnOrderProductList() {
		return returnOrderProductList;
	}

	public void setReturnOrderProductList(List<ReturnOrderProduct> returnOrderProductList) {
		this.returnOrderProductList = returnOrderProductList;
	}

	
	
	public List<Area> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}

	@Override
	public String toString() {
		return "ReturnOrderDateRangeResponse [returnOrderProductList=" + returnOrderProductList + ", areaList="
				+ areaList + "]";
	}

	
	
	

}
