package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.City;

public class CityListResponse extends BaseDomain{

	private List<City> cityList;

	public List<City> getCityList() {
		return cityList;
	}

	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}

	@Override
	public String toString() {
		return "CityListResponse [cityList=" + cityList + "]";
	}
	
	
	
}
