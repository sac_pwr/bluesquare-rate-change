package com.bluesquare.rc.rest.models;

public  class BaseDomain {

	public  String status;
	
	public String errorMsg;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	@Override
	public String toString() {
		return "BaseDomain [status=" + status + ", errorMsg=" + errorMsg + "]";
	}
	
	
}
