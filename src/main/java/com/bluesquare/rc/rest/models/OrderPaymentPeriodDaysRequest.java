package com.bluesquare.rc.rest.models;

public class OrderPaymentPeriodDaysRequest extends BaseDomain {

	private String orderId;
	
	private long orderPaymentPeriodDays;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public long getOrderPaymentPeriodDays() {
		return orderPaymentPeriodDays;
	}

	public void setOrderPaymentPeriodDays(long orderPaymentPeriodDays) {
		this.orderPaymentPeriodDays = orderPaymentPeriodDays;
	}

	@Override
	public String toString() {
		return "OrderPaymentPeriodDaysResponse [orderId=" + orderId + ", orderPaymentPeriodDays="
				+ orderPaymentPeriodDays + "]";
	}
	
	
}
