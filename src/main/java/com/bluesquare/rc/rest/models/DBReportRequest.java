package com.bluesquare.rc.rest.models;

public class DBReportRequest {
	
	private long employeeId;
	private String fromDate;
	private String toDate;
	private String range;
	private String orderStatus;
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	@Override
	public String toString() {
		return "DBReportRequest [employeeId=" + employeeId + ", fromDate=" + fromDate + ", toDate=" + toDate
				+ ", range=" + range + ", orderStatus=" + orderStatus + "]";
	}
	
	

}
