package com.bluesquare.rc.rest.models;

public class BrandAndCategoryRequest {
	
	public long brandId;
	
	public long categoryId;

	public long getBrandId() {
		return brandId;
	}

	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	@Override
	public String toString() {
		return "BrandAndCategoryRequest [brandId=" + brandId + ", categoryId=" + categoryId + "]";
	}
	
	

}
