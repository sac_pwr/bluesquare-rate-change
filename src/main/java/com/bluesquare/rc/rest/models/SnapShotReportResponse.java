package com.bluesquare.rc.rest.models;

public class SnapShotReportResponse extends BaseDomain {

	// private long totalNoOfShopVisited;
	private long totalNoOrderTaken;
	private double totalAmtSales;
	private double totalIssueAmt;
	private double rtnOrderAmt;
	private double replaceOrderAmt;
	private double totalCollectionAmt;
	private long totalNoOrderCanceled;

	public long getTotalNoOrderTaken() {
		return totalNoOrderTaken;
	}

	public void setTotalNoOrderTaken(long totalNoOrderTaken) {
		this.totalNoOrderTaken = totalNoOrderTaken;
	}

	public double getTotalAmtSales() {
		return totalAmtSales;
	}

	public void setTotalAmtSales(double totalAmtSales) {
		this.totalAmtSales = totalAmtSales;
	}

	public double getTotalIssueAmt() {
		return totalIssueAmt;
	}

	public void setTotalIssueAmt(double totalIssueAmt) {
		this.totalIssueAmt = totalIssueAmt;
	}

	public double getRtnOrderAmt() {
		return rtnOrderAmt;
	}

	public void setRtnOrderAmt(double rtnOrderAmt) {
		this.rtnOrderAmt = rtnOrderAmt;
	}

	public double getReplaceOrderAmt() {
		return replaceOrderAmt;
	}

	public void setReplaceOrderAmt(double replaceOrderAmt) {
		this.replaceOrderAmt = replaceOrderAmt;
	}

	public double getTotalCollectionAmt() {
		return totalCollectionAmt;
	}

	public void setTotalCollectionAmt(double totalCollectionAmt) {
		this.totalCollectionAmt = totalCollectionAmt;
	}

	public long getTotalNoOrderCanceled() {
		return totalNoOrderCanceled;
	}

	public void setTotalNoOrderCanceled(long totalNoOrderCanceled) {
		this.totalNoOrderCanceled = totalNoOrderCanceled;
	}

	@Override
	public String toString() {
		return "SnapShotReportResponse [totalNoOrderTaken=" + totalNoOrderTaken + ", totalAmtSales=" + totalAmtSales
				+ ", totalIssueAmt=" + totalIssueAmt + ", rtnOrderAmt=" + rtnOrderAmt + ", replaceOrderAmt="
				+ replaceOrderAmt + ", totalCollectionAmt=" + totalCollectionAmt + ", totalNoOrderCanceled="
				+ totalNoOrderCanceled + "]";
	}

	
}
