//InnerResponse for ProductAddInventory
package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Company;

public class ProductAddInventory{

	public long productId;
	
	public long currentQuantity;
	
	public String productCode;
	
	public String productName;
	
	public float rate;
	
	public long productBrandId;
	
	public long productCategoryId;
	
	public long productThreshold;
	
	public double supplierRate;
	
	public Categories categories;
	
	public Brand brand;

	public Company company;
	
	public ProductAddInventory() {
		// TODO Auto-generated constructor stub
	}

	public ProductAddInventory(long productId, long currentQuantity, String productCode, String productName, float rate,
			long productBrandId, long productCategoryId, long productThreshold, double supplierRate,
			Categories categories, Brand brand, Company company) {
		super();
		this.productId = productId;
		this.currentQuantity = currentQuantity;
		this.productCode = productCode;
		this.productName = productName;
		this.rate = rate;
		this.productBrandId = productBrandId;
		this.productCategoryId = productCategoryId;
		this.productThreshold = productThreshold;
		this.supplierRate = supplierRate;
		this.categories = categories;
		this.brand = brand;
		this.company = company;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(long currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public float getRate() {
		return rate;
	}

	public void setRate(float rate) {
		this.rate = rate;
	}

	public long getProductBrandId() {
		return productBrandId;
	}

	public void setProductBrandId(long productBrandId) {
		this.productBrandId = productBrandId;
	}

	public long getProductCategoryId() {
		return productCategoryId;
	}

	public void setProductCategoryId(long productCategoryId) {
		this.productCategoryId = productCategoryId;
	}

	public long getProductThreshold() {
		return productThreshold;
	}

	public void setProductThreshold(long productThreshold) {
		this.productThreshold = productThreshold;
	}

	public double getSupplierRate() {
		return supplierRate;
	}

	public void setSupplierRate(double supplierRate) {
		this.supplierRate = supplierRate;
	}

	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@Override
	public String toString() {
		return "ProductAddInventory [productId=" + productId + ", currentQuantity=" + currentQuantity + ", productCode="
				+ productCode + ", productName=" + productName + ", rate=" + rate + ", productBrandId=" + productBrandId
				+ ", productCategoryId=" + productCategoryId + ", productThreshold=" + productThreshold
				+ ", supplierRate=" + supplierRate + ", categories=" + categories + ", brand=" + brand + ", company="
				+ company + "]";
	}	
}

