package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;

public class InventorySaveRequestModel extends BaseDomain {

	private Inventory inventory;
	private List<InventoryDetails> inventoryDetails;
	private String paymentDate;
	private String billDate;
	public Inventory getInventory() {
		return inventory;
	}
	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}
	public List<InventoryDetails> getInventoryDetails() {
		return inventoryDetails;
	}
	public void setInventoryDetails(List<InventoryDetails> inventoryDetails) {
		this.inventoryDetails = inventoryDetails;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getBillDate() {
		return billDate;
	}
	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
	@Override
	public String toString() {
		return "InventorySaveRequestModel [inventory=" + inventory + ", inventoryDetails=" + inventoryDetails
				+ ", paymentDate=" + paymentDate + ", billDate=" + billDate + "]";
	}
		
	
	
}
