package com.bluesquare.rc.rest.models;

public class BookOrderResponse extends BaseDomain{

	private String orderId;
	private String businessName;
	private double totalAmount;
	private double totalAmountWithTax;
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalAmountWithTax() {
		return totalAmountWithTax;
	}
	public void setTotalAmountWithTax(double totalAmountWithTax) {
		this.totalAmountWithTax = totalAmountWithTax;
	}
	@Override
	public String toString() {
		return "BookOrderResponse [orderId=" + orderId + ", businessName=" + businessName + ", totalAmount="
				+ totalAmount + ", totalAmountWithTax=" + totalAmountWithTax + "]";
	}
	
	
	
	
	
	
}
