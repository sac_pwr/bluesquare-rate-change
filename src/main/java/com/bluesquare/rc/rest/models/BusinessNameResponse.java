package com.bluesquare.rc.rest.models;

import com.bluesquare.rc.entities.BusinessName;

public class BusinessNameResponse extends BaseDomain {

	private BusinessName businessName;

	public BusinessName getBusinessName() {
		return businessName;
	}

	public void setBusinessName(BusinessName businessName) {
		this.businessName = businessName;
	}

	@Override
	public String toString() {
		return "BusinessNameResponse [businessName=" + businessName + "]";
	}
	
	
}
