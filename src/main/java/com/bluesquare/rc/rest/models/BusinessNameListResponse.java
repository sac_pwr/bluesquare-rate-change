package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.BusinessName;

public class BusinessNameListResponse extends BaseDomain {

	private List<BusinessName> businessNamesList;

	public List<BusinessName> getBusinessNamesList() {
		return businessNamesList;
	}

	public void setBusinessNamesList(List<BusinessName> businessNamesList) {
		this.businessNamesList = businessNamesList;
	}

	@Override
	public String toString() {
		return "BusinessNameListResponse [businessNamesList=" + businessNamesList + "]";
	}
	
	
	
	
}
