package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Region;

public class RegionListResponse extends BaseDomain{
	private List<Region> regionList;

	public List<Region> getRegionList() {
		return regionList;
	}

	public void setRegionList(List<Region> regionList) {
		this.regionList = regionList;
	}

	@Override
	public String toString() {
		return "RegionListResponse [regionList=" + regionList + "]";
	}

	

}
