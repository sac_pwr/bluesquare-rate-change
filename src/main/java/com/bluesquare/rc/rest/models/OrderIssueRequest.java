package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;

public class OrderIssueRequest {

	private OrderDetails orderDetails;
	private List<OrderProductDetails> orderProductDetailsList;
	private String deliveryDate;
	private long employeeIdDB;
	private long employeeIdGk;
	private String returnOrderProductId;
	
	public OrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	public List<OrderProductDetails> getOrderProductDetailsList() {
		return orderProductDetailsList;
	}
	public void setOrderProductDetailsList(List<OrderProductDetails> orderProductDetailsList) {
		this.orderProductDetailsList = orderProductDetailsList;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public long getEmployeeIdDB() {
		return employeeIdDB;
	}
	public void setEmployeeIdDB(long employeeIdDB) {
		this.employeeIdDB = employeeIdDB;
	}
	public long getEmployeeIdGk() {
		return employeeIdGk;
	}
	public void setEmployeeIdGk(long employeeIdGk) {
		this.employeeIdGk = employeeIdGk;
	}
	public String getReturnOrderProductId() {
		return returnOrderProductId;
	}
	public void setReturnOrderProductId(String returnOrderProductId) {
		this.returnOrderProductId = returnOrderProductId;
	}
	@Override
	public String toString() {
		return "OrderIssueRequest [orderDetails=" + orderDetails + ", orderProductDetailsList="
				+ orderProductDetailsList + ", deliveryDate=" + deliveryDate + ", employeeIdDB=" + employeeIdDB
				+ ", employeeIdGk=" + employeeIdGk + ", returnOrderProductId=" + returnOrderProductId + "]";
	}
	
}
