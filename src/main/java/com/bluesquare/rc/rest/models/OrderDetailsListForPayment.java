package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;

public class OrderDetailsListForPayment extends BaseDomain {

	private List<OrderDetailsForPayment> orderDetailsList;

	public List<OrderDetailsForPayment> getOrderDetailsList() {
		return orderDetailsList;
	}

	public void setOrderDetailsList(List<OrderDetailsForPayment> orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}

	@Override
	public String toString() {
		return "OrderDetailsList [orderDetailsList=" + orderDetailsList + "]";
	}

	
	
}
