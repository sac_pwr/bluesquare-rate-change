package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.models.ReIssueOrderDetailsReport;


public class ReIssueOrderDetailsReportResponse extends BaseDomain{
	
	private List<ReIssueOrderDetailsReport> reIssueOrderDetailsReportsList;
	
	private List<ReIssueOrderDetails> reIssueOrderDetailsList;

	public List<ReIssueOrderDetailsReport> getReIssueOrderDetailsReportsList() {
		return reIssueOrderDetailsReportsList;
	}

	public void setReIssueOrderDetailsReportsList(List<ReIssueOrderDetailsReport> reIssueOrderDetailsReportsList) {
		this.reIssueOrderDetailsReportsList = reIssueOrderDetailsReportsList;
	}

	public List<ReIssueOrderDetails> getReIssueOrderDetailsList() {
		return reIssueOrderDetailsList;
	}

	public void setReIssueOrderDetailsList(List<ReIssueOrderDetails> reIssueOrderDetailsList) {
		this.reIssueOrderDetailsList = reIssueOrderDetailsList;
	}

	@Override
	public String toString() {
		return "ReIssueOrderDetailsReportResponse [reIssueOrderDetailsReportsList=" + reIssueOrderDetailsReportsList
				+ ", reIssueOrderDetailsList=" + reIssueOrderDetailsList + "]";
	}


}
