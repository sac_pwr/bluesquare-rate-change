package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.Department;

public class ComplainRaiseFirstResponse extends BaseDomain {
	
	private List<Area> areaList;
	private List<Department> departmentList;
	
	
	public List<Area> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}
	public List<Department> getDepartmentList() {
		return departmentList;
	}
	public void setDepartmentList(List<Department> departmentList) {
		this.departmentList = departmentList;
	}
	@Override
	public String toString() {
		return "ComplainRaiseFirstResponse [areaList=" + areaList + ", departmentList=" + departmentList + "]";
	}
	
	
	

}
