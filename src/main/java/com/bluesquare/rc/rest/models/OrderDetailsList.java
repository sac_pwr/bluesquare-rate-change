package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.OrderDetails;

public class OrderDetailsList extends BaseDomain {

	private List<OrderDetails> orderDetailsList;
	private List<Area> areaList;
	private String pageName;
	private String salesManName;
	
	public List<OrderDetails> getOrderDetailsList() {
		return orderDetailsList;
	}
	public void setOrderDetailsList(List<OrderDetails> orderDetailsList) {
		this.orderDetailsList = orderDetailsList;
	}
	public List<Area> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getSalesManName() {
		return salesManName;
	}
	public void setSalesManName(String salesManName) {
		this.salesManName = salesManName;
	}
	@Override
	public String toString() {
		return "OrderDetailsList [orderDetailsList=" + orderDetailsList + ", areaList=" + areaList + ", pageName="
				+ pageName + ", salesManName=" + salesManName + "]";
	}
	
}
