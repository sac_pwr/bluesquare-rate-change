package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Inventory;
import com.bluesquare.rc.entities.InventoryDetails;

public class InventoryProductDetailsReportResponse extends BaseDomain {
	
	private Inventory inventory;
	
	private String name;
	
	private List<InventoryDetails> inventoryDetails;

	public Inventory getInventory() {
		return inventory;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<InventoryDetails> getInventoryDetails() {
		return inventoryDetails;
	}

	public void setInventoryDetails(List<InventoryDetails> inventoryDetails) {
		this.inventoryDetails = inventoryDetails;
	}

	@Override
	public String toString() {
		return "InventoryProductDetailsReportResponse [inventory=" + inventory + ", name=" + name
				+ ", inventoryDetails=" + inventoryDetails + "]";
	}

}
