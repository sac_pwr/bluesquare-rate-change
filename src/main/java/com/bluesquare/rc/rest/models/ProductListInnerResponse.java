package com.bluesquare.rc.rest.models;

public class ProductListInnerResponse  {

	public long productId;
	
	public String productName;
	
	public long currentQuantity;
	
	public String encodedImage;	
	

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(long currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public String getEncodedImage() {
		return encodedImage;
	}

	public void setEncodedImage(String encodedImage) {
		this.encodedImage = encodedImage;
	}

	@Override
	public String toString() {
		return "ProductListInnerResponse [productId=" + productId + ", productName=" + productName
				+ ", currentQuantity=" + currentQuantity + ", encodedImage=" + encodedImage + "]";
	}

	
	
	
	

}
