/**
 * 
 */
package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;

/**
 * @author aNKIT
 *
 */
public class ReIssueOrderProductDetailsListModel extends BaseDomain{

	private ReIssueOrderDetails reIssueOrderDetails;
	private List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList;
	public ReIssueOrderDetails getReIssueOrderDetails() {
		return reIssueOrderDetails;
	}
	public void setReIssueOrderDetails(ReIssueOrderDetails reIssueOrderDetails) {
		this.reIssueOrderDetails = reIssueOrderDetails;
	}
	public List<ReIssueOrderProductDetails> getReIssueOrderProductDetailsList() {
		return reIssueOrderProductDetailsList;
	}
	public void setReIssueOrderProductDetailsList(List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList) {
		this.reIssueOrderProductDetailsList = reIssueOrderProductDetailsList;
	}
	@Override
	public String toString() {
		return "ReIssueOrderProductDetailsListModel [reIssueOrderDetails=" + reIssueOrderDetails
				+ ", reIssueOrderProductDetailsList=" + reIssueOrderProductDetailsList + "]";
	}
	
	
}
