package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;

public class ReIssueOrderProductDetailsReportResponse extends BaseDomain{
	
	private ReIssueOrderDetails reIssueOrderDetails;
	private List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList;
	private String deliveryPersonName;
	public ReIssueOrderDetails getReIssueOrderDetails() {
		return reIssueOrderDetails;
	}
	public void setReIssueOrderDetails(ReIssueOrderDetails reIssueOrderDetails) {
		this.reIssueOrderDetails = reIssueOrderDetails;
	}
	public List<ReIssueOrderProductDetails> getReIssueOrderProductDetailsList() {
		return reIssueOrderProductDetailsList;
	}
	public void setReIssueOrderProductDetailsList(List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList) {
		this.reIssueOrderProductDetailsList = reIssueOrderProductDetailsList;
	}
	public String getDeliveryPersonName() {
		return deliveryPersonName;
	}
	public void setDeliveryPersonName(String deliveryPersonName) {
		this.deliveryPersonName = deliveryPersonName;
	}
	@Override
	public String toString() {
		return "ReIssueOrderProductDetailsReportResponse [reIssueOrderDetails=" + reIssueOrderDetails
				+ ", reIssueOrderProductDetailsList=" + reIssueOrderProductDetailsList + ", deliveryPersonName="
				+ deliveryPersonName + "]";
	}
	
	

}
