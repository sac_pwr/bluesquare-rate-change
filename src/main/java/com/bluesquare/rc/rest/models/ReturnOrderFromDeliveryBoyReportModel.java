/**
 * 
 */
package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;
import com.bluesquare.rc.entities.ReturnFromDeliveryBoy;
import com.bluesquare.rc.models.ReturnOrderFromDeliveryBoyReport;

/**
 * @author aNKIT
 *
 */
public class ReturnOrderFromDeliveryBoyReportModel extends BaseDomain {

	private List<ReturnOrderFromDeliveryBoyReport> returnOrderFromDeliveryBoyReportList;
	
	private List<ReturnFromDeliveryBoy>  returnFromDeliveryBoyList;
	
	private EmployeeDetails employeeDetails;

	public List<ReturnOrderFromDeliveryBoyReport> getReturnOrderFromDeliveryBoyReportList() {
		return returnOrderFromDeliveryBoyReportList;
	}

	public void setReturnOrderFromDeliveryBoyReportList(
			List<ReturnOrderFromDeliveryBoyReport> returnOrderFromDeliveryBoyReportList) {
		this.returnOrderFromDeliveryBoyReportList = returnOrderFromDeliveryBoyReportList;
	}

	public List<ReturnFromDeliveryBoy> getReturnFromDeliveryBoyList() {
		return returnFromDeliveryBoyList;
	}

	public void setReturnFromDeliveryBoyList(List<ReturnFromDeliveryBoy> returnFromDeliveryBoyList) {
		this.returnFromDeliveryBoyList = returnFromDeliveryBoyList;
	}

	public EmployeeDetails getEmployeeDetails() {
		return employeeDetails;
	}

	public void setEmployeeDetails(EmployeeDetails employeeDetails) {
		this.employeeDetails = employeeDetails;
	}

	@Override
	public String toString() {
		return "ReturnOrderFromDeliveryBoyReportModel [returnOrderFromDeliveryBoyReportList="
				+ returnOrderFromDeliveryBoyReportList + ", returnFromDeliveryBoyList=" + returnFromDeliveryBoyList
				+ ", employeeDetails=" + employeeDetails + "]";
	}
}
