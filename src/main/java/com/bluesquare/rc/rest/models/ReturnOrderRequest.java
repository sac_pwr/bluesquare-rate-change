package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;

public class ReturnOrderRequest {
	
	private ReturnOrderProduct returnOrderProduct;
	 
	private List<ReturnOrderProductDetails> returnOrderProductList;
	
	private String signatureBase64;
	
	private OrderDetails orderDetails;
	
	private List<OrderProductDetails> orderProductDetailsList;

	public ReturnOrderProduct getReturnOrderProduct() {
		return returnOrderProduct;
	}

	public void setReturnOrderProduct(ReturnOrderProduct returnOrderProduct) {
		this.returnOrderProduct = returnOrderProduct;
	}

	public List<ReturnOrderProductDetails> getReturnOrderProductList() {
		return returnOrderProductList;
	}

	public void setReturnOrderProductList(List<ReturnOrderProductDetails> returnOrderProductList) {
		this.returnOrderProductList = returnOrderProductList;
	}

	public String getSignatureBase64() {
		return signatureBase64;
	}

	public void setSignatureBase64(String signatureBase64) {
		this.signatureBase64 = signatureBase64;
	}

	public OrderDetails getOrderDetails() {
		return orderDetails;
	}

	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}

	public List<OrderProductDetails> getOrderProductDetailsList() {
		return orderProductDetailsList;
	}

	public void setOrderProductDetailsList(List<OrderProductDetails> orderProductDetailsList) {
		this.orderProductDetailsList = orderProductDetailsList;
	}

	@Override
	public String toString() {
		return "ReturnOrderRequest [returnOrderProduct=" + returnOrderProduct + ", returnOrderProductList="
				+ returnOrderProductList + ", signatureBase64=" + signatureBase64 + ", orderDetails=" + orderDetails
				+ ", orderProductDetailsList=" + orderProductDetailsList + "]";
	}
}
