package com.bluesquare.rc.rest.models;

public class InventoryProductListRequest {

	
	
	private String supplierId;
	private long brandId;
	private long categoryId;
	
	
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public long getBrandId() {
		return brandId;
	}
	public void setBrandId(long brandId) {
		this.brandId = brandId;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	@Override
	public String toString() {
		return "inventoryProductListRequest [supplierId=" + supplierId + ", brandId=" + brandId + ", categoryId="
				+ categoryId + "]";
	}
	
	
}
