package com.bluesquare.rc.rest.models;

public class EmployeeChatStatusResponse extends BaseDomain {
	private String chatStatus;

	public String getChatStatus() {
		return chatStatus;
	}

	public void setChatStatus(String chatStatus) {
		this.chatStatus = chatStatus;
	}

	@Override
	public String toString() {
		return "EmployeeChatStatusResponse [chatStatus=" + chatStatus + "]";
	}

	
	

}
