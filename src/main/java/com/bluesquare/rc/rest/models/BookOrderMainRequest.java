/**
 * 
 */
package com.bluesquare.rc.rest.models;

/**
 * @author aNKIT
 *
 */
public class BookOrderMainRequest {

	private OrderRequest orderRequest;
	private BookOrderFreeProductRequest bookOrderFreeProductRequest;
	private OrderPaymentPeriodDaysRequest orderPaymentPeriodDaysRequest;
	String freeProductHave;
	public OrderRequest getOrderRequest() {
		return orderRequest;
	}
	public void setOrderRequest(OrderRequest orderRequest) {
		this.orderRequest = orderRequest;
	}
	public BookOrderFreeProductRequest getBookOrderFreeProductRequest() {
		return bookOrderFreeProductRequest;
	}
	public void setBookOrderFreeProductRequest(BookOrderFreeProductRequest bookOrderFreeProductRequest) {
		this.bookOrderFreeProductRequest = bookOrderFreeProductRequest;
	}
	public OrderPaymentPeriodDaysRequest getOrderPaymentPeriodDaysRequest() {
		return orderPaymentPeriodDaysRequest;
	}
	public void setOrderPaymentPeriodDaysRequest(OrderPaymentPeriodDaysRequest orderPaymentPeriodDaysRequest) {
		this.orderPaymentPeriodDaysRequest = orderPaymentPeriodDaysRequest;
	}
	public String getFreeProductHave() {
		return freeProductHave;
	}
	public void setFreeProductHave(String freeProductHave) {
		this.freeProductHave = freeProductHave;
	}
	@Override
	public String toString() {
		return "BookOrderMainRequest [orderRequest=" + orderRequest + ", bookOrderFreeProductRequest="
				+ bookOrderFreeProductRequest + ", orderPaymentPeriodDaysRequest=" + orderPaymentPeriodDaysRequest
				+ ", freeProductHave=" + freeProductHave + "]";
	}
}
