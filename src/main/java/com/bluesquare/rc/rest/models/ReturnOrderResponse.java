package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;

public class ReturnOrderResponse extends BaseDomain {
	
	private OrderDetails orderDetails;
	
		
	private List<OrderProductDetails> orderProductDetails;


	public ReturnOrderResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public ReturnOrderResponse(OrderDetails orderDetails, List<OrderProductDetails> orderProductDetails) {
		super();
		this.orderDetails = orderDetails;
		this.orderProductDetails = orderProductDetails;
	}


	public OrderDetails getOrderDetails() {
		return orderDetails;
	}


	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}


	public List<OrderProductDetails> getOrderProductDetails() {
		return orderProductDetails;
	}


	public void setOrderProductDetails(List<OrderProductDetails> orderProductDetails) {
		this.orderProductDetails = orderProductDetails;
	}


	@Override
	public String toString() {
		return "ReturnOrderResponse [orderDetails=" + orderDetails + ", orderProductDetails=" + orderProductDetails
				+ "]";
	}
	
	
	
	

}
