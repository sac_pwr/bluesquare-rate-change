/**
 * 
 */
package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.EmployeeDetails;

/**
 * @author aNKIT
 *
 */
public class SalesManReport {

	private List<SalesManReportSub> salesManReportSubList;
	private List<EmployeeDetails> employeeDetailsList;
	private double salesManCollectionAmount;
	
	public SalesManReport(List<SalesManReportSub> salesManReportSubList, List<EmployeeDetails> employeeDetailsList,
			double salesManCollectionAmount) {
		super();
		this.salesManReportSubList = salesManReportSubList;
		this.employeeDetailsList = employeeDetailsList;
		this.salesManCollectionAmount = salesManCollectionAmount;
	}
	public List<SalesManReportSub> getSalesManReportSubList() {
		return salesManReportSubList;
	}
	public void setSalesManReportSubList(List<SalesManReportSub> salesManReportSubList) {
		this.salesManReportSubList = salesManReportSubList;
	}
	public List<EmployeeDetails> getEmployeeDetailsList() {
		return employeeDetailsList;
	}
	public void setEmployeeDetailsList(List<EmployeeDetails> employeeDetailsList) {
		this.employeeDetailsList = employeeDetailsList;
	}
	public double getSalesManCollectionAmount() {
		return salesManCollectionAmount;
	}
	public void setSalesManCollectionAmount(double salesManCollectionAmount) {
		this.salesManCollectionAmount = salesManCollectionAmount;
	}
	@Override
	public String toString() {
		return "SalesManReport [salesManReportSubList=" + salesManReportSubList + ", employeeDetailsList="
				+ employeeDetailsList + ", salesManCollectionAmount=" + salesManCollectionAmount + "]";
	}

	
}
