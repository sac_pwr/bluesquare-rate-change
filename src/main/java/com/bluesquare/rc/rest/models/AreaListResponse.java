package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;

public class AreaListResponse extends BaseDomain {

	private List<Area> areaList;

	public List<Area> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}

	@Override
	public String toString() {
		return "AreaListResponse [areaList=" + areaList + "]";
	}
	
	
	
}
