package com.bluesquare.rc.rest.models;
import java.util.List;
import com.bluesquare.rc.entities.OrderDetails;
import com.bluesquare.rc.entities.OrderProductDetails;
import com.bluesquare.rc.entities.ReturnOrderProduct;
public class OrderDetailResponse extends BaseDomain {
    
    private OrderDetails orderDetailList;
    private List<OrderProductDetails> orderProductDetailList;
    private String salesPersonName;
    private List<EmployeeNameAndId> employeeNameAndIdSMAndDBList;	
    private ReturnOrderProduct returnOrderProduct;
	public OrderDetails getOrderDetailList() {
		return orderDetailList;
	}
	public void setOrderDetailList(OrderDetails orderDetailList) {
		this.orderDetailList = orderDetailList;
	}
	public List<OrderProductDetails> getOrderProductDetailList() {
		return orderProductDetailList;
	}
	public void setOrderProductDetailList(List<OrderProductDetails> orderProductDetailList) {
		this.orderProductDetailList = orderProductDetailList;
	}
	public String getSalesPersonName() {
		return salesPersonName;
	}
	public void setSalesPersonName(String salesPersonName) {
		this.salesPersonName = salesPersonName;
	}
	public List<EmployeeNameAndId> getEmployeeNameAndIdSMAndDBList() {
		return employeeNameAndIdSMAndDBList;
	}
	public void setEmployeeNameAndIdSMAndDBList(List<EmployeeNameAndId> employeeNameAndIdSMAndDBList) {
		this.employeeNameAndIdSMAndDBList = employeeNameAndIdSMAndDBList;
	}
	public ReturnOrderProduct getReturnOrderProduct() {
		return returnOrderProduct;
	}
	public void setReturnOrderProduct(ReturnOrderProduct returnOrderProduct) {
		this.returnOrderProduct = returnOrderProduct;
	}
	@Override
	public String toString() {
		return "OrderDetailResponse [orderDetailList=" + orderDetailList + ", orderProductDetailList="
				+ orderProductDetailList + ", salesPersonName=" + salesPersonName + ", employeeNameAndIdSMAndDBList="
				+ employeeNameAndIdSMAndDBList + ", returnOrderProduct=" + returnOrderProduct + "]";
	}
}