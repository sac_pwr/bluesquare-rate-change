package com.bluesquare.rc.rest.models;

import java.util.List;
import java.util.Locale.Category;

import com.bluesquare.rc.entities.Brand;
import com.bluesquare.rc.entities.Categories;
import com.bluesquare.rc.entities.Supplier;

public class OpenInventoryResponseModel extends BaseDomain{
	
	private List<Supplier> supplier;
	private List<Brand> brand;
	private List<Categories> category;
	public List<Supplier> getSupplier() {
		return supplier;
	}
	public void setSupplier(List<Supplier> supplier) {
		this.supplier = supplier;
	}
	public List<Brand> getBrand() {
		return brand;
	}
	public void setBrand(List<Brand> brand) {
		this.brand = brand;
	}
	public List<Categories> getCategory() {
		return category;
	}
	public void setCategory(List<Categories> category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "OpenInventoryResponseModel [supplier=" + supplier + ", brand=" + brand + ", category=" + category + "]";
	}
	
	
	

}
