package com.bluesquare.rc.rest.models;

public class ReturnOrderDateRangeRequest {
	
	String fromDate;
	String toDate;
	long employeeId;
	String range;
	long areaId;
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public long getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public long getAreaId() {
		return areaId;
	}
	public void setAreaId(long areaId) {
		this.areaId = areaId;
	}
	@Override
	public String toString() {
		return "ReturnOrderDateRangeRequest [fromDate=" + fromDate + ", toDate=" + toDate + ", employeeId=" + employeeId
				+ ", range=" + range + ", areaId=" + areaId + "]";
	}
	



}
