package com.bluesquare.rc.rest.models;

import java.util.List;

public class ProductAddInventoryResponse extends BaseDomain{
	
	List<ProductAddInventory> productAddInventory;

	public List<ProductAddInventory> getProductAddInventory() {
		return productAddInventory;
	}

	public void setProductAddInventory(List<ProductAddInventory> productAddInventory) {
		this.productAddInventory = productAddInventory;
	}

	@Override
	public String toString() {
		return "ProductAddInventoryResponse [productAddInventory=" + productAddInventory + "]";
	}
	
	

}
