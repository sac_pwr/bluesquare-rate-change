package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Chat;

public class ChatListResponse extends BaseDomain {
	private List<Chat> chatList;

	public List<Chat> getChatList() {
		return chatList;
	}

	public void setChatList(List<Chat> chatList) {
		this.chatList = chatList;
	}

	@Override
	public String toString() {
		return "ChatListResponse [chatList=" + chatList + "]";
	}
	
	
}
