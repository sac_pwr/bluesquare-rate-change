package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Inventory;

public class InventotryReportResponse extends BaseDomain{
	
	private List<Inventory> inventory;

	public List<Inventory> getInventory() {
		return inventory;
	}

	public void setInventory(List<Inventory> inventory) {
		this.inventory = inventory;
	}

	@Override
	public String toString() {
		return "InventotryReportResonse [inventory=" + inventory + "]";
	}

	

}
