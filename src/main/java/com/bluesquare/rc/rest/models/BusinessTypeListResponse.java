package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.BusinessType;

public class BusinessTypeListResponse extends BaseDomain{

	private List<BusinessType> businessTypesList;

	public List<BusinessType> getBusinessTypesList() {
		return businessTypesList;
	}

	public void setBusinessTypesList(List<BusinessType> businessTypesList) {
		this.businessTypesList = businessTypesList;
	}

	@Override
	public String toString() {
		return "BusinessTypeListResponse [businessTypesList=" + businessTypesList + "]";
	}
	
	
}
