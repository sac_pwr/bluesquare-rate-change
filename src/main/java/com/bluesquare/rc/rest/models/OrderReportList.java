package com.bluesquare.rc.rest.models;

import java.util.Date;

public class OrderReportList {

	private long srno;
	private String orderId;
	private double amountWithoutTax;
	private double amountWithTax;
	private String areaName;
	private long totalQuantity;
	private String shopName;
	private String businessNameId;
	private String employeeName;
	private long employeeDetailsId;
	private String employeeDetailsGenId;
	private long paymentPeriodDays;
	private Date orderDate;
	private String orderStatusSM;
	private String orderStatusSMDate;
	private String orderStatusSMTime;
	private String orderStatusGK;
	private String orderStatusGKDate;
	private String orderStatusGKTime;
	private String orderStatusDB;
	private String orderStatusDBDate;
	private String orderStatusDBTime;
	private String orderStatusCurrent;
	public OrderReportList(long srno, String orderId, double amountWithoutTax, double amountWithTax, String areaName,
			long totalQuantity, String shopName, String businessNameId, String employeeName, long employeeDetailsId,
			String employeeDetailsGenId, long paymentPeriodDays, Date orderDate, String orderStatusSM,
			String orderStatusSMDate, String orderStatusSMTime, String orderStatusGK, String orderStatusGKDate,
			String orderStatusGKTime, String orderStatusDB, String orderStatusDBDate, String orderStatusDBTime,
			String orderStatusCurrent) {
		super();
		this.srno = srno;
		this.orderId = orderId;
		this.amountWithoutTax = amountWithoutTax;
		this.amountWithTax = amountWithTax;
		this.areaName = areaName;
		this.totalQuantity = totalQuantity;
		this.shopName = shopName;
		this.businessNameId = businessNameId;
		this.employeeName = employeeName;
		this.employeeDetailsId = employeeDetailsId;
		this.employeeDetailsGenId = employeeDetailsGenId;
		this.paymentPeriodDays = paymentPeriodDays;
		this.orderDate = orderDate;
		this.orderStatusSM = orderStatusSM;
		this.orderStatusSMDate = orderStatusSMDate;
		this.orderStatusSMTime = orderStatusSMTime;
		this.orderStatusGK = orderStatusGK;
		this.orderStatusGKDate = orderStatusGKDate;
		this.orderStatusGKTime = orderStatusGKTime;
		this.orderStatusDB = orderStatusDB;
		this.orderStatusDBDate = orderStatusDBDate;
		this.orderStatusDBTime = orderStatusDBTime;
		this.orderStatusCurrent = orderStatusCurrent;
	}
	public long getSrno() {
		return srno;
	}
	public void setSrno(long srno) {
		this.srno = srno;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public double getAmountWithoutTax() {
		return amountWithoutTax;
	}
	public void setAmountWithoutTax(double amountWithoutTax) {
		this.amountWithoutTax = amountWithoutTax;
	}
	public double getAmountWithTax() {
		return amountWithTax;
	}
	public void setAmountWithTax(double amountWithTax) {
		this.amountWithTax = amountWithTax;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public long getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(long totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getBusinessNameId() {
		return businessNameId;
	}
	public void setBusinessNameId(String businessNameId) {
		this.businessNameId = businessNameId;
	}
	public String getEmployeeName() {
		return employeeName;
	}
	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}
	public long getEmployeeDetailsId() {
		return employeeDetailsId;
	}
	public void setEmployeeDetailsId(long employeeDetailsId) {
		this.employeeDetailsId = employeeDetailsId;
	}
	public String getEmployeeDetailsGenId() {
		return employeeDetailsGenId;
	}
	public void setEmployeeDetailsGenId(String employeeDetailsGenId) {
		this.employeeDetailsGenId = employeeDetailsGenId;
	}
	public long getPaymentPeriodDays() {
		return paymentPeriodDays;
	}
	public void setPaymentPeriodDays(long paymentPeriodDays) {
		this.paymentPeriodDays = paymentPeriodDays;
	}
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public String getOrderStatusSM() {
		return orderStatusSM;
	}
	public void setOrderStatusSM(String orderStatusSM) {
		this.orderStatusSM = orderStatusSM;
	}
	public String getOrderStatusSMDate() {
		return orderStatusSMDate;
	}
	public void setOrderStatusSMDate(String orderStatusSMDate) {
		this.orderStatusSMDate = orderStatusSMDate;
	}
	public String getOrderStatusSMTime() {
		return orderStatusSMTime;
	}
	public void setOrderStatusSMTime(String orderStatusSMTime) {
		this.orderStatusSMTime = orderStatusSMTime;
	}
	public String getOrderStatusGK() {
		return orderStatusGK;
	}
	public void setOrderStatusGK(String orderStatusGK) {
		this.orderStatusGK = orderStatusGK;
	}
	public String getOrderStatusGKDate() {
		return orderStatusGKDate;
	}
	public void setOrderStatusGKDate(String orderStatusGKDate) {
		this.orderStatusGKDate = orderStatusGKDate;
	}
	public String getOrderStatusGKTime() {
		return orderStatusGKTime;
	}
	public void setOrderStatusGKTime(String orderStatusGKTime) {
		this.orderStatusGKTime = orderStatusGKTime;
	}
	public String getOrderStatusDB() {
		return orderStatusDB;
	}
	public void setOrderStatusDB(String orderStatusDB) {
		this.orderStatusDB = orderStatusDB;
	}
	public String getOrderStatusDBDate() {
		return orderStatusDBDate;
	}
	public void setOrderStatusDBDate(String orderStatusDBDate) {
		this.orderStatusDBDate = orderStatusDBDate;
	}
	public String getOrderStatusDBTime() {
		return orderStatusDBTime;
	}
	public void setOrderStatusDBTime(String orderStatusDBTime) {
		this.orderStatusDBTime = orderStatusDBTime;
	}
	public String getOrderStatusCurrent() {
		return orderStatusCurrent;
	}
	public void setOrderStatusCurrent(String orderStatusCurrent) {
		this.orderStatusCurrent = orderStatusCurrent;
	}
	@Override
	public String toString() {
		return "OrderReportList [srno=" + srno + ", orderId=" + orderId + ", amountWithoutTax=" + amountWithoutTax
				+ ", amountWithTax=" + amountWithTax + ", areaName=" + areaName + ", totalQuantity=" + totalQuantity
				+ ", shopName=" + shopName + ", businessNameId=" + businessNameId + ", employeeName=" + employeeName
				+ ", employeeDetailsId=" + employeeDetailsId + ", employeeDetailsGenId=" + employeeDetailsGenId
				+ ", paymentPeriodDays=" + paymentPeriodDays + ", orderDate=" + orderDate + ", orderStatusSM="
				+ orderStatusSM + ", orderStatusSMDate=" + orderStatusSMDate + ", orderStatusSMTime="
				+ orderStatusSMTime + ", orderStatusGK=" + orderStatusGK + ", orderStatusGKDate=" + orderStatusGKDate
				+ ", orderStatusGKTime=" + orderStatusGKTime + ", orderStatusDB=" + orderStatusDB
				+ ", orderStatusDBDate=" + orderStatusDBDate + ", orderStatusDBTime=" + orderStatusDBTime
				+ ", orderStatusCurrent=" + orderStatusCurrent + "]";
	}
	
}
