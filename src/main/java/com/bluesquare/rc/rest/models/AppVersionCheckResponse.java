package com.bluesquare.rc.rest.models;

public class AppVersionCheckResponse extends BaseDomain{
	
	private String appVersionUpdateAvailable;

	public String getAppVersionUpdateAvailable() {
		return appVersionUpdateAvailable;
	}

	public void setAppVersionUpdateAvailable(String appVersionUpdateAvailable) {
		this.appVersionUpdateAvailable = appVersionUpdateAvailable;
	}

	@Override
	public String toString() {
		return "AppVersionCheckResponse [appVersionUpdateAvailable=" + appVersionUpdateAvailable + "]";
	}

	
}
