package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReturnOrderProduct;
import com.bluesquare.rc.entities.ReturnOrderProductDetails;

public class ReturnOrderProductDetailsReportResponse extends BaseDomain{
	
	private ReturnOrderProduct returnOrderProduct;
	private List<ReturnOrderProductDetails> returnOrderProductDetailsList;
	
	
	
	public ReturnOrderProduct getReturnOrderProduct() {
		return returnOrderProduct;
	}
	public void setReturnOrderProduct(ReturnOrderProduct returnOrderProduct) {
		this.returnOrderProduct = returnOrderProduct;
	}
	public List<ReturnOrderProductDetails> getReturnOrderProductDetailsList() {
		return returnOrderProductDetailsList;
	}
	public void setReturnOrderProductDetailsList(List<ReturnOrderProductDetails> returnOrderProductDetailsList) {
		this.returnOrderProductDetailsList = returnOrderProductDetailsList;
	}
	@Override
	public String toString() {
		return "ReturnOrderProductDetailsReportResponse [returnOrderProduct=" + returnOrderProduct
				+ ", returnOrderProductDetailsList=" + returnOrderProductDetailsList + "]";
	}
	
	
	

}
