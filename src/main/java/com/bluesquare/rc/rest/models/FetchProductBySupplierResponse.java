package com.bluesquare.rc.rest.models;

import java.util.List;

public class FetchProductBySupplierResponse extends BaseDomain{

	List<ProductAddInventory> productAddInventories;

	public List<ProductAddInventory> getProductAddInventories() {
		return productAddInventories;
	}

	public void setProductAddInventories(List<ProductAddInventory> productAddInventories) {
		this.productAddInventories = productAddInventories;
	}

	@Override
	public String toString() {
		return "fetchProductBySupplierResponse [productAddInventories=" + productAddInventories + "]";
	}
	
	
}
