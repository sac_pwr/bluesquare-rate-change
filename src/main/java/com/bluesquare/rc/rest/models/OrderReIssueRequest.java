/**
 * 
 */
package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.entities.ReIssueOrderProductDetails;

/**
 * @author aNKIT
 *
 */
public class OrderReIssueRequest {
	private ReIssueOrderDetails reIssueOrderDetails;
	private List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList;
	private String reIssueDeliveryDate;
	private long employeeIdForDelivery;
	private long employeeIdGk;

	public ReIssueOrderDetails getReIssueOrderDetails() {
		return reIssueOrderDetails;
	}

	public void setReIssueOrderDetails(ReIssueOrderDetails reIssueOrderDetails) {
		this.reIssueOrderDetails = reIssueOrderDetails;
	}

	public List<ReIssueOrderProductDetails> getReIssueOrderProductDetailsList() {
		return reIssueOrderProductDetailsList;
	}

	public void setReIssueOrderProductDetailsList(List<ReIssueOrderProductDetails> reIssueOrderProductDetailsList) {
		this.reIssueOrderProductDetailsList = reIssueOrderProductDetailsList;
	}

	public String getReIssueDeliveryDate() {
		return reIssueDeliveryDate;
	}

	public void setReIssueDeliveryDate(String reIssueDeliveryDate) {
		this.reIssueDeliveryDate = reIssueDeliveryDate;
	}

	public long getEmployeeIdForDelivery() {
		return employeeIdForDelivery;
	}

	public void setEmployeeIdForDelivery(long employeeIdForDelivery) {
		this.employeeIdForDelivery = employeeIdForDelivery;
	}

	public long getEmployeeIdGk() {
		return employeeIdGk;
	}

	public void setEmployeeIdGk(long employeeIdGk) {
		this.employeeIdGk = employeeIdGk;
	}

	@Override
	public String toString() {
		return "OrderReIssueRequest [reIssueOrderDetails=" + reIssueOrderDetails + ", reIssueOrderProductDetailsList="
				+ reIssueOrderProductDetailsList + ", reIssueDeliveryDate=" + reIssueDeliveryDate
				+ ", employeeIdForDelivery=" + employeeIdForDelivery + ", employeeIdGk=" + employeeIdGk + "]";
	}

}
