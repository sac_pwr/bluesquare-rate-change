package com.bluesquare.rc.rest.models;

import java.util.List;

public class CustomerReportMainResponseModel extends BaseDomain{
	
	private List<CustomerReportResponse> customerReportResponse;

	public List<CustomerReportResponse> getCustomerReportResponse() {
		return customerReportResponse;
	}

	public void setCustomerReportResponse(List<CustomerReportResponse> customerReportResponse) {
		this.customerReportResponse = customerReportResponse;
	}

	@Override
	public String toString() {
		return "CustomerReportMainResponseModel [customerReportResponse=" + customerReportResponse + "]";
	}
	
	
	

}
