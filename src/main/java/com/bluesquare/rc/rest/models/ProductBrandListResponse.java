package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Brand;

public class ProductBrandListResponse extends BaseDomain{
	
	List<Brand> brandList;

	public List<Brand> getBrandList() {
		return brandList;
	}

	public void setBrandList(List<Brand> brandList) {
		this.brandList = brandList;
	}

	@Override
	public String toString() {
		return "ProductBrandListResponse [brandList=" + brandList + "]";
	}

	
	
	

}
