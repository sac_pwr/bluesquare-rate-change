package com.bluesquare.rc.rest.models;

public class LoginRequest {

		private String userId;
	    
	    private String password;
	    
	    private String token;
	    
	    public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public LoginRequest() {
	    }

	    public String getUserId() {
	        return userId;
	    }

	    public void setUserId(String userId) {
	        this.userId = userId;
	    }

	    public String getPassword() {
	        return password;
	    }

	    public void setPassword(String password) {
	        this.password = password;
	    }

		@Override
		public String toString() {
			return "LoginRequest [userId=" + userId + ", password=" + password + ", token=" + token + "]";
		}

	   
	}

