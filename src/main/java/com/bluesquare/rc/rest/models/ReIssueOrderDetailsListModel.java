/**
 * 
 */
package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReIssueOrderDetails;
import com.bluesquare.rc.rest.models.BaseDomain;

/**
 * @author aNKIT
 *
 */
public class ReIssueOrderDetailsListModel extends BaseDomain
{

	private List<ReIssueOrderDetails> reIssueOrderDetailsList;

	public List<ReIssueOrderDetails> getReIssueOrderDetailsList() {
		return reIssueOrderDetailsList;
	}

	public void setReIssueOrderDetailsList(List<ReIssueOrderDetails> reIssueOrderDetailsList) {
		this.reIssueOrderDetailsList = reIssueOrderDetailsList;
	}

	@Override
	public String toString() {
		return "ReIssueOrderDetailsListModel [reIssueOrderDetailsList=" + reIssueOrderDetailsList + "]";
	}
	
	
}
