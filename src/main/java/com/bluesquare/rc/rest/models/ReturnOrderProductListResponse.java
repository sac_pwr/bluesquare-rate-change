package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.ReturnOrderProductDetails;

public class ReturnOrderProductListResponse extends BaseDomain{
	 private List<ReturnOrderProductDetails> returnOrderProductDetails;

	public List<ReturnOrderProductDetails> getReturnOrderProductDetails() {
		return returnOrderProductDetails;
	}

	public void setReturnOrderProductDetails(List<ReturnOrderProductDetails> returnOrderProductDetails) {
		this.returnOrderProductDetails = returnOrderProductDetails;
	}

	@Override
	public String toString() {
		return "ReturnOrderProductListResponse [returnOrderProductDetails=" + returnOrderProductDetails + "]";
	}
	 
	 

}
