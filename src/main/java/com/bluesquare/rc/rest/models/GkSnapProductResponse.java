package com.bluesquare.rc.rest.models;

public class GkSnapProductResponse {
	
	private String productName;
	private long totalOrderedQty;
	private long availableQty;
	private long requiredQty;

	public GkSnapProductResponse(String productName, long totalOrderedQty, long availableQty, long requiredQty) {
		super();
		this.productName = productName;
		this.totalOrderedQty = totalOrderedQty;
		this.availableQty = availableQty;
		this.requiredQty = requiredQty;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public long getTotalOrderedQty() {
		return totalOrderedQty;
	}
	public void setTotalOrderedQty(long totalOrderedQty) {
		this.totalOrderedQty = totalOrderedQty;
	}
	public long getAvailableQty() {
		return availableQty;
	}
	public void setAvailableQty(long availableQty) {
		this.availableQty = availableQty;
	}
	public long getRequiredQty() {
		return requiredQty;
	}
	public void setRequiredQty(long requiredQty) {
		this.requiredQty = requiredQty;
	}
	@Override
	public String toString() {
		return "GkSnapProductResponse [productName=" + productName + ", totalOrderedQty=" + totalOrderedQty
				+ ", availableQty=" + availableQty + ", requiredQty=" + requiredQty + "]";
	}

	
}
