package com.bluesquare.rc.rest.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.bluesquare.rc.entities.Employee;

public class ComplainReplyResponse {

	private long complainReplyId;
	private Employee employeeReplyId;
	
	private String employeeNameReply;
	private Employee employeeComplainId;
	private String employeeAreaList;
	private String employeeNameComplain;
	private String message;
	private String reply;
	private Date msgDateTime;
	private Date replyDateTime;
	private String employeeNameForList;
	private Date dateForList;	
	private String departmentName;
	public ComplainReplyResponse(long complainReplyId, Employee employeeReplyId, String employeeNameReply,
			Employee employeeComplainId, String employeeAreaList, String employeeNameComplain, String message,
			String reply, Date msgDateTime, Date replyDateTime, String employeeNameForList, Date dateForList,
			String departmentName) {
		super();
		this.complainReplyId = complainReplyId;
		this.employeeReplyId = employeeReplyId;
		this.employeeNameReply = employeeNameReply;
		this.employeeComplainId = employeeComplainId;
		this.employeeAreaList = employeeAreaList;
		this.employeeNameComplain = employeeNameComplain;
		this.message = message;
		this.reply = reply;
		this.msgDateTime = msgDateTime;
		this.replyDateTime = replyDateTime;
		this.employeeNameForList = employeeNameForList;
		this.dateForList = dateForList;
		this.departmentName = departmentName;
	}
	public long getComplainReplyId() {
		return complainReplyId;
	}
	public void setComplainReplyId(long complainReplyId) {
		this.complainReplyId = complainReplyId;
	}
	public Employee getEmployeeReplyId() {
		return employeeReplyId;
	}
	public void setEmployeeReplyId(Employee employeeReplyId) {
		this.employeeReplyId = employeeReplyId;
	}
	public String getEmployeeNameReply() {
		return employeeNameReply;
	}
	public void setEmployeeNameReply(String employeeNameReply) {
		this.employeeNameReply = employeeNameReply;
	}
	public Employee getEmployeeComplainId() {
		return employeeComplainId;
	}
	public void setEmployeeComplainId(Employee employeeComplainId) {
		this.employeeComplainId = employeeComplainId;
	}
	public String getEmployeeAreaList() {
		return employeeAreaList;
	}
	public void setEmployeeAreaList(String employeeAreaList) {
		this.employeeAreaList = employeeAreaList;
	}
	public String getEmployeeNameComplain() {
		return employeeNameComplain;
	}
	public void setEmployeeNameComplain(String employeeNameComplain) {
		this.employeeNameComplain = employeeNameComplain;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReply() {
		return reply;
	}
	public void setReply(String reply) {
		this.reply = reply;
	}
	public Date getMsgDateTime() {
		return msgDateTime;
	}
	public void setMsgDateTime(Date msgDateTime) {
		this.msgDateTime = msgDateTime;
	}
	public Date getReplyDateTime() {
		return replyDateTime;
	}
	public void setReplyDateTime(Date replyDateTime) {
		this.replyDateTime = replyDateTime;
	}
	public String getEmployeeNameForList() {
		return employeeNameForList;
	}
	public void setEmployeeNameForList(String employeeNameForList) {
		this.employeeNameForList = employeeNameForList;
	}
	public Date getDateForList() {
		return dateForList;
	}
	public void setDateForList(Date dateForList) {
		this.dateForList = dateForList;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	@Override
	public String toString() {
		return "ComplainReplyResponse [complainReplyId=" + complainReplyId + ", employeeReplyId=" + employeeReplyId
				+ ", employeeNameReply=" + employeeNameReply + ", employeeComplainId=" + employeeComplainId
				+ ", employeeAreaList=" + employeeAreaList + ", employeeNameComplain=" + employeeNameComplain
				+ ", message=" + message + ", reply=" + reply + ", msgDateTime=" + msgDateTime + ", replyDateTime="
				+ replyDateTime + ", employeeNameForList=" + employeeNameForList + ", dateForList=" + dateForList
				+ ", departmentName=" + departmentName + "]";
	}

}
