package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Area;
import com.bluesquare.rc.entities.OrderProductIssueDetails;

public class OrderProductIssueReportResponse extends BaseDomain{
	
	private List<OrderProductIssueDetails> orderProductIssueDetailsList;
	private List<Area> areaList;
	private long deliveredCount;
	private long deliveredPendingCount;
	
	
	
	public List<OrderProductIssueDetails> getOrderProductIssueDetailsList() {
		return orderProductIssueDetailsList;
	}
	public void setOrderProductIssueDetailsList(List<OrderProductIssueDetails> orderProductIssueDetailsList) {
		this.orderProductIssueDetailsList = orderProductIssueDetailsList;
	}
	public List<Area> getAreaList() {
		return areaList;
	}
	public void setAreaList(List<Area> areaList) {
		this.areaList = areaList;
	}
	public long getDeliveredCount() {
		return deliveredCount;
	}
	public void setDeliveredCount(long deliveredCount) {
		this.deliveredCount = deliveredCount;
	}
	public long getDeliveredPendingCount() {
		return deliveredPendingCount;
	}
	public void setDeliveredPendingCount(long deliveredPendingCount) {
		this.deliveredPendingCount = deliveredPendingCount;
	}
	@Override
	public String toString() {
		return "OrderProductIssueReportResponse [orderProductIssueDetailsList=" + orderProductIssueDetailsList
				+ ", areaList=" + areaList + ", deliveredCount=" + deliveredCount + ", deliveredPendingCount="
				+ deliveredPendingCount + "]";
	}
	
	


}
