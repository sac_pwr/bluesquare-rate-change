package com.bluesquare.rc.rest.models;

import java.util.List;

public class EmployeeNameAndIdResponse extends BaseDomain{
	
	private List<EmployeeNameAndId> employeeNameAndIdList;

	public List<EmployeeNameAndId> getEmployeeNameAndIdList() {
		return employeeNameAndIdList;
	}

	public void setEmployeeNameAndIdList(List<EmployeeNameAndId> employeeNameAndIdList) {
		this.employeeNameAndIdList = employeeNameAndIdList;
	}

	@Override
	public String toString() {
		return "EmployeeNameAndIdResponse [employeeNameAndIdList=" + employeeNameAndIdList + "]";
	}
	
	
}
