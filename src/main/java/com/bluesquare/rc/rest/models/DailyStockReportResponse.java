package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.DailyStockDetails;

public class DailyStockReportResponse extends BaseDomain {
    private List<DailyStockDetails> dailyStockDetails;

    public List<DailyStockDetails> getDailyStockDetails() {
        return dailyStockDetails;
    }

    public void setDailyStockDetails(List<DailyStockDetails> dailyStockDetails) {
        this.dailyStockDetails = dailyStockDetails;
    }

    @Override
    public String toString() {
        return "DailyStockReportResponse{" +
                "dailyStockDetails=" + dailyStockDetails +
                '}';
    }
}