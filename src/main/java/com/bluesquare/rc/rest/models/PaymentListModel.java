package com.bluesquare.rc.rest.models;

import java.util.Date;
import java.util.List;

import com.bluesquare.rc.entities.OrderDetails;

public class PaymentListModel extends BaseDomain {

	private OrderDetails orderDetails;
	private Date lastPaidDate;
	private double paidAmt;
	private double unPaidAmt;
	private List<PaymentList> paymentList;
	
	public PaymentListModel() {
		// TODO Auto-generated constructor stub
	}
	
	public PaymentListModel(OrderDetails orderDetails, Date lastPaidDate, double paidAmt, double unPaidAmt,
			List<PaymentList> paymentList) {
		super();
		this.orderDetails = orderDetails;
		this.lastPaidDate = lastPaidDate;
		this.paidAmt = paidAmt;
		this.unPaidAmt = unPaidAmt;
		this.paymentList = paymentList;
	}
	public OrderDetails getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(OrderDetails orderDetails) {
		this.orderDetails = orderDetails;
	}
	public Date getLastPaidDate() {
		return lastPaidDate;
	}
	public void setLastPaidDate(Date lastPaidDate) {
		this.lastPaidDate = lastPaidDate;
	}
	public double getPaidAmt() {
		return paidAmt;
	}
	public void setPaidAmt(double paidAmt) {
		this.paidAmt = paidAmt;
	}
	public double getUnPaidAmt() {
		return unPaidAmt;
	}
	public void setUnPaidAmt(double unPaidAmt) {
		this.unPaidAmt = unPaidAmt;
	}
	public List<PaymentList> getPaymentList() {
		return paymentList;
	}
	public void setPaymentList(List<PaymentList> paymentList) {
		this.paymentList = paymentList;
	}
	@Override
	public String toString() {
		return "PaymentListModel [orderDetails=" + orderDetails + ", lastPaidDate=" + lastPaidDate + ", paidAmt="
				+ paidAmt + ", unPaidAmt=" + unPaidAmt + ", paymentList=" + paymentList + "]";
	}

	
}
