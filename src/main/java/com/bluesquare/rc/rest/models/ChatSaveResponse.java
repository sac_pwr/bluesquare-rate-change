package com.bluesquare.rc.rest.models;

public class ChatSaveResponse extends BaseDomain{

	private long chatId;

	public long getChatId() {
		return chatId;
	}

	public void setChatId(long chatId) {
		this.chatId = chatId;
	}

	@Override
	public String toString() {
		return "ChatSaveResponse [chatId=" + chatId + "]";
	}
}
