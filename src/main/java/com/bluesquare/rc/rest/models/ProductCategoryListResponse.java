package com.bluesquare.rc.rest.models;

import java.util.List;

import com.bluesquare.rc.entities.Categories;

public class ProductCategoryListResponse extends BaseDomain {
	
	List<Categories> categoryListInnerResponse;

	public List<Categories> getCategoryListInnerResponse() {
		return categoryListInnerResponse;
	}

	public void setCategoryListInnerResponse(List<Categories> categoryListInnerResponse) {
		this.categoryListInnerResponse = categoryListInnerResponse;
	}

	@Override
	public String toString() {
		return "ProductCategoryListResponse [categoryListInnerResponse=" + categoryListInnerResponse + "]";
	}

	
	

}
