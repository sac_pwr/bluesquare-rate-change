-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 10, 2018 at 04:25 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bluesquare3`
--

-- --------------------------------------------------------

--
-- Table structure for table `address`
--

CREATE TABLE `address` (
  `address_id` bigint(20) NOT NULL,
  `landmark` varchar(255) DEFAULT NULL,
  `line1` varchar(255) DEFAULT NULL,
  `line2` varchar(255) DEFAULT NULL,
  `area_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `app_version`
--

CREATE TABLE `app_version` (
  `app_version_id` bigint(20) NOT NULL,
  `app_version` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_version`
--

INSERT INTO `app_version` (`app_version_id`, `app_version`) VALUES
(1, '1.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `area_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pincode` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `region_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`area_id`, `name`, `pincode`, `company_id`, `region_id`) VALUES
(1, 'Kandivali', 400096, 2, 1),
(2, 'Malad', 400087, 2, 1),
(3, 'Borivali', 400065, 2, 1),
(4, 'Andheri', 622258, 2, 1),
(5, 'ABC', 40098, 2, 2),
(6, 'Kothrud', 400097, 4, 3),
(7, 'Aundh', 40087, 4, 3),
(8, 'Balewadi', 977822, 4, 4),
(9, 'Swargate', 875421, 4, 4),
(10, 'Jogeshwari', 400088, 2, 1),
(11, 'Kothrud-Aundh	', 400087, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `brand_id` bigint(20) NOT NULL,
  `brand_added_datetime` datetime DEFAULT NULL,
  `brand_update_datetime` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`brand_id`, `brand_added_datetime`, `brand_update_datetime`, `name`, `company_id`) VALUES
(1, '2018-03-05 16:33:40', '2018-03-05 16:33:40', 'BLUE SQUARE', 2),
(2, '2018-03-07 11:08:37', '2018-03-07 11:08:37', 'BLUE SQUARE1_2', 4);

-- --------------------------------------------------------

--
-- Table structure for table `business_name`
--

CREATE TABLE `business_name` (
  `business_name_id` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `business_added_datetime` datetime DEFAULT NULL,
  `business_updated_datetime` datetime DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `credit_limit` decimal(19,2) DEFAULT NULL,
  `employee_details_disable_datetime` datetime DEFAULT NULL,
  `gstin_number` varchar(255) DEFAULT NULL,
  `other` varchar(255) DEFAULT NULL,
  `owner_name` varchar(255) DEFAULT NULL,
  `shop_name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `tax_type` varchar(255) DEFAULT NULL,
  `area_id` bigint(20) DEFAULT NULL,
  `businesstype_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `contact_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_name`
--

INSERT INTO `business_name` (`business_name_id`, `address`, `business_added_datetime`, `business_updated_datetime`, `comment`, `credit_limit`, `employee_details_disable_datetime`, `gstin_number`, `other`, `owner_name`, `shop_name`, `status`, `tax_type`, `area_id`, `businesstype_id`, `company_id`, `contact_id`) VALUES
('1001', 'malad', '2018-03-05 17:31:53', '2018-03-05 17:49:49', NULL, '45454.00', '2018-03-05 17:31:53', 'fsdffffffffffff', NULL, 'abc shukla', 'abc shop', 0, 'Intra', 2, 5, 2, 8),
('1002', 'Kandivali West', '2018-03-07 11:02:23', '2018-03-07 11:02:55', NULL, '27000.00', '2018-03-07 11:02:23', 'TESM279HSN5101F', NULL, 'Natraj', 'Natraj Electronics', 0, 'Intra', 1, 6, 2, 9),
('1003', 'Wagholi - Kothrud', '2018-03-07 13:18:16', '2018-03-07 13:18:16', NULL, '0.00', '2018-03-07 13:18:16', '', NULL, 'Shop', 'Shop Name', 0, 'Intra', 6, 9, 4, 13),
('1004', 'Andheri', '2018-03-07 15:03:48', '2018-03-13 11:45:12', NULL, '45000.00', '2018-03-07 15:03:48', 'qqqqqqqqqqqaqqa', NULL, 'Bhima Gaonkar ', 'Bhima Electronics', 0, 'Intra', 6, 9, 4, 14),
('1005', 'gshs', '2018-03-17 18:25:43', '2018-03-17 18:25:43', NULL, '12000.00', '2018-03-17 18:25:43', 'hsjsjsjdjdkdkkd', NULL, 'akka', 'ABC ', 0, 'Intra', 6, 9, 4, 17);

-- --------------------------------------------------------

--
-- Table structure for table `business_type`
--

CREATE TABLE `business_type` (
  `businesstype_id` bigint(20) NOT NULL,
  `business_type_added_datetime` datetime DEFAULT NULL,
  `business_type_updated_datetime` datetime DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `business_type`
--

INSERT INTO `business_type` (`businesstype_id`, `business_type_added_datetime`, `business_type_updated_datetime`, `details`, `name`, `company_id`) VALUES
(5, '2017-12-13 10:20:18', '2017-12-13 10:20:18', NULL, 'IT', 2),
(6, '2017-12-13 11:09:18', '2018-02-22 19:59:39', NULL, 'Electronics', 2),
(7, '2018-01-31 16:32:19', '2018-01-31 16:39:12', NULL, 'ABC3', 2),
(8, '2018-01-31 16:39:05', '2018-01-31 16:39:05', NULL, 'Stationery A', 2),
(9, '2018-03-07 13:15:24', '2018-03-07 13:15:24', NULL, 'IT', 4);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` bigint(20) NOT NULL,
  `category_date` datetime DEFAULT NULL,
  `category_description` varchar(255) DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `category_update_date` datetime DEFAULT NULL,
  `cgst` decimal(19,2) DEFAULT NULL,
  `hsn_code` varchar(255) DEFAULT NULL,
  `igst` decimal(19,2) DEFAULT NULL,
  `sgst` decimal(19,2) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_date`, `category_description`, `category_name`, `category_update_date`, `cgst`, `hsn_code`, `igst`, `sgst`, `company_id`) VALUES
(1, '2017-12-13 11:07:17', NULL, 'KEYBOARD', '2017-12-23 17:28:31', '6.00', '8471', '12.00', '6.00', 2),
(2, '2017-12-13 11:08:38', NULL, 'MOUSE', NULL, '9.00', '8472', '18.00', '9.00', 2),
(3, '2017-12-13 11:09:09', NULL, 'CABLES', NULL, '9.00', '8544', '18.00', '9.00', 2),
(4, '2017-12-13 11:09:41', NULL, 'SPIKE GUARD', '2017-12-23 17:31:57', '6.00', '8536', '12.00', '6.00', 2),
(5, '2017-12-13 11:10:07', NULL, 'SPEAKER', '2017-12-27 16:24:50', '6.00', '8518', '12.00', '6.00', 2),
(6, '2017-12-14 17:31:20', NULL, 'CARD', NULL, '9.00', '8517', '18.00', '9.00', 2),
(7, '2017-12-14 17:38:53', NULL, 'PENDRIVE', NULL, '9.00', '8523', '18.00', '9.00', 2),
(8, '2017-12-14 17:44:35', NULL, 'SMPS', NULL, '9.00', '8504', '18.00', '9.00', 2),
(9, '2017-12-23 19:09:18', NULL, 'Abc', '2017-12-23 19:09:37', '6.00', '12234', '12.00', '6.00', 2),
(10, '2018-01-01 11:39:52', NULL, 'Sundaram', '2018-01-01 11:39:52', '2.50', '87878787', '5.00', '2.50', 2),
(11, '2017-12-13 11:07:17', NULL, 'KEYBOARD', '2017-12-23 17:28:31', '6.00', '8471', '12.00', '6.00', 4),
(12, '2017-12-13 11:08:38', NULL, 'MOUSE', NULL, '9.00', '8472', '18.00', '9.00', 4),
(13, '2017-12-13 11:09:09', NULL, 'CABLES', NULL, '9.00', '8544', '18.00', '9.00', 4),
(14, '2017-12-13 11:09:41', NULL, 'SPIKE GUARD', '2017-12-23 17:31:57', '6.00', '8536', '12.00', '6.00', 4),
(15, '2017-12-13 11:10:07', NULL, 'SPEAKER', '2017-12-27 16:24:50', '6.00', '8518', '12.00', '6.00', 4),
(16, '2017-12-14 17:31:20', NULL, 'CARD', NULL, '9.00', '8517', '18.00', '9.00', 4),
(17, '2017-12-14 17:38:53', NULL, 'PENDRIVE', NULL, '9.00', '8523', '18.00', '9.00', 4),
(18, '2017-12-14 17:44:35', NULL, 'SMPS', NULL, '9.00', '8504', '18.00', '9.00', 4),
(19, '2017-12-23 19:09:18', NULL, 'Abc', '2017-12-23 19:09:37', '6.00', '12234', '12.00', '6.00', 4),
(20, '2018-01-01 11:39:52', NULL, 'Sundaram', '2018-01-01 11:39:52', '2.50', '87878787', '5.00', '2.50', 4);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `chat_id` bigint(20) NOT NULL,
  `date_time` datetime DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `employee_id_from` bigint(20) DEFAULT NULL,
  `employee_id_to` bigint(20) DEFAULT NULL,
  `img_base64` longtext,
  `pdf_base64` longtext,
  `typing_status` tinyint(1) DEFAULT NULL,
  `delete_status` tinyint(1) DEFAULT NULL,
  `today_first_record` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`chat_id`, `date_time`, `msg`, `type`, `employee_id_from`, `employee_id_to`, `img_base64`, `pdf_base64`, `typing_status`, `delete_status`, `today_first_record`) VALUES
(8, '2018-04-03 11:26:00', 'hsbsv', 'Text', 9, 11, NULL, NULL, NULL, 0, 1),
(9, '2018-04-03 11:26:12', 'jshs', 'Text', 9, 11, NULL, NULL, NULL, 0, 0),
(10, '2018-04-04 12:23:21', 'hello', 'Text', 9, 11, NULL, NULL, NULL, 0, 1),
(11, '2018-04-07 18:22:40', '\\uD83D\\uDE02', 'Text', 11, 9, NULL, NULL, NULL, 0, 1),
(12, '2018-04-07 18:32:49', '\\uD83D\\uDE09', 'Text', 11, 9, NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `state_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `name`, `state_id`) VALUES
(1, 'Mumbai', 1),
(2, 'Pune', 1),
(3, 'ABC', 2),
(4, 'Abc', 1),
(5, 'Abc2', 1),
(6, 'BLUE1', 1),
(7, 'BLUE SQUARE1', 1),
(8, 'Abc5', 1),
(9, 'Sangali', 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `company_id` bigint(20) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `contact_id` bigint(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `gstin_no` varchar(255) DEFAULT NULL,
  `pan_number` varchar(255) DEFAULT NULL,
  `balance` decimal(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`company_id`, `company_name`, `password`, `user_id`, `contact_id`, `address`, `gstin_no`, `pan_number`, `balance`) VALUES
(0, 'admin', 'admin', 'admin', NULL, NULL, NULL, NULL, '0.00'),
(2, 'BlueSquare', 'bluesquare', 'bluesquare', 1, 'abc', '', '11223', '-100.00'),
(4, 'BlueSquare2', '123', 'bluesquare2', 3, 'malad', '22222222', NULL, '8963.00'),
(5, 'BlueSquare3', 'rohan2', 'rohan2', 16, 'malad', '444444444', NULL, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `company_cities`
--

CREATE TABLE `company_cities` (
  `company_city_id` bigint(20) NOT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_cities`
--

INSERT INTO `company_cities` (`company_city_id`, `city_id`, `company_id`) VALUES
(10, 1, 5),
(11, 9, 5),
(15, 2, 4),
(16, 5, 4),
(23, 1, 2),
(24, 2, 2),
(25, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `complainreply`
--

CREATE TABLE `complainreply` (
  `complain_Reply_Id` bigint(20) NOT NULL,
  `message` varchar(255) DEFAULT NULL,
  `Msg_date_time` datetime DEFAULT NULL,
  `reply` varchar(255) DEFAULT NULL,
  `Reply_date_time` datetime DEFAULT NULL,
  `employee_complain_id` bigint(20) DEFAULT NULL,
  `employee_reply_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `complainreply`
--

INSERT INTO `complainreply` (`complain_Reply_Id`, `message`, `Msg_date_time`, `reply`, `Reply_date_time`, `employee_complain_id`, `employee_reply_id`) VALUES
(1, 'Error', '2018-03-07 17:37:26', NULL, NULL, 11, 10),
(2, 'Ord1001 is cancelled', '2018-03-13 12:17:11', 'Ok.\r\ni will check it.', '2018-03-13 12:17:46', 14, 13),
(3, 'Hey . Ord1001 is not cancelled still.', '2018-03-13 12:19:04', 'Ok .\nI will check it\n', '2018-03-13 12:19:39', 13, 14),
(4, 'hey', '2018-03-26 15:21:23', NULL, NULL, 11, 9);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` bigint(20) NOT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `fax_no` varchar(255) DEFAULT NULL,
  `mobile_no` varchar(255) DEFAULT NULL,
  `tel_no` varchar(255) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `email_id`, `fax_no`, `mobile_no`, `tel_no`, `website_url`) VALUES
(1, 'sachinupawar25@gmail.com', NULL, '8425986282', '', NULL),
(3, 'bl@df.v', NULL, '8425965323', '', NULL),
(4, 'sachinupawar24@gmail.com', NULL, '8425986282', NULL, NULL),
(5, 'sachinupawar24@gmail.com', NULL, '8425986282', NULL, NULL),
(6, 'sachinupawar24@gmail.com', NULL, '8425986282', NULL, NULL),
(7, 'sachinupawar24@gmail.com', NULL, '8425986282', NULL, NULL),
(8, 'sachinupawar24@gmail.com', NULL, '8425986282', '', NULL),
(9, 'natraj@natraj.in', NULL, '8888438568', '02525263127', NULL),
(10, 'nehal@gmail.com', NULL, '7506048815', NULL, NULL),
(11, 'parag@gmail.com', NULL, '8425986282', NULL, NULL),
(12, 'ali@gmail.com', NULL, '8754213265', NULL, NULL),
(13, '', NULL, '8888438568', '', NULL),
(14, 'sachinupawar24@gmail.com', NULL, '8425986282', '', NULL),
(15, 'sachinupawar24@gmail.com', NULL, '8754213265', NULL, NULL),
(16, 'bl@df.v', NULL, '8425986282', '', NULL),
(17, '', NULL, '8425986282', '', NULL),
(18, '', NULL, '9324006186', NULL, NULL),
(19, '', NULL, '9324006186', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `counter_order`
--

CREATE TABLE `counter_order` (
  `counter_order_id` varchar(255) NOT NULL,
  `customer_gst_number` varchar(255) DEFAULT NULL,
  `customer_mob_number` varchar(255) DEFAULT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `date_of_order_taken` datetime DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `paid_status` tinyint(1) DEFAULT NULL,
  `payment_due_date` datetime DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `total_quantity` bigint(20) DEFAULT NULL,
  `business_id` varchar(255) DEFAULT NULL,
  `employee_gk_id` bigint(20) DEFAULT NULL,
  `order_status_id` bigint(20) DEFAULT NULL,
  `refund_amount` decimal(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counter_order`
--

INSERT INTO `counter_order` (`counter_order_id`, `customer_gst_number`, `customer_mob_number`, `customer_name`, `date_of_order_taken`, `invoice_number`, `paid_status`, `payment_due_date`, `total_amount`, `total_amount_with_tax`, `total_quantity`, `business_id`, `employee_gk_id`, `order_status_id`, `refund_amount`) VALUES
('CORD1001', NULL, NULL, NULL, '2018-04-05 12:49:17', 'INV1000000067', 1, '2018-04-06 00:00:00', '1605.94', '1820.00', 30, '1002', 11, 3, NULL),
('CORD1002', '', '1245789865', 'Tushar', '2018-04-05 12:50:41', 'INV1000000068', 0, NULL, '203.40', '240.00', 12, NULL, 11, 3, NULL),
('CORD1003', '', '8754213265', 'Sachin Pawar', '2018-04-06 13:04:39', 'INV1000000069', 1, NULL, '3408.38', '3904.00', 22, NULL, 13, 3, NULL),
('CORD1004', NULL, NULL, NULL, '2018-04-06 13:06:00', 'INV1000000070', 0, '2018-04-06 00:00:00', '3928.60', '4400.00', 20, '1004', 13, 3, NULL),
('CORD1005', '', '8521479635', 'Tushar Goavnkar', '2018-04-06 13:08:08', 'INV1000000071', 0, '2018-04-06 00:00:00', '5084.80', '6000.00', 20, NULL, 13, 3, NULL),
('CORD1006', NULL, NULL, NULL, '2018-04-06 13:08:45', 'INV1000000072', 0, NULL, '3305.00', '3900.00', 20, '1004', 13, 3, NULL),
('CORD1007', NULL, NULL, NULL, '2018-04-07 11:41:56', 'INV1000000073', 1, NULL, '2142.84', '2400.00', 12, '1003', 13, 3, NULL),
('CORD1008', '', '5455511111', 'sac pwr', '2018-04-07 11:46:24', 'INV1000000074', 1, NULL, '3237.30', '3820.00', 5, NULL, 13, 3, NULL),
('CORD1009', '', '1111111111', 'f', '2018-04-07 11:47:31', 'INV1000000075', 1, NULL, '120.34', '142.00', 2, NULL, 13, 3, NULL),
('CORD1010', '', '4567893215', 'Kunal', '2018-04-07 11:48:36', 'INV1000000076', 1, NULL, '1103.58', '1236.00', 3, NULL, 13, 3, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `counter_order_product_details`
--

CREATE TABLE `counter_order_product_details` (
  `counter_order_product_id` bigint(20) NOT NULL,
  `purchase_amount` decimal(19,2) DEFAULT NULL,
  `purchase_quantity` bigint(20) DEFAULT NULL,
  `selling_rate` decimal(19,2) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `counter_order_id` varchar(255) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `counter_order_product_details`
--

INSERT INTO `counter_order_product_details` (`counter_order_product_id`, `purchase_amount`, `purchase_quantity`, `selling_rate`, `type`, `counter_order_id`, `product_id`) VALUES
(4, '240.00', 12, '20.00', 'NonFree', 'CORD1002', 365),
(5, '0.00', 2, '0.00', 'Free', 'CORD1001', 366),
(6, '420.00', 21, '20.00', 'NonFree', 'CORD1001', 367),
(7, '1400.00', 7, '200.00', 'NonFree', 'CORD1001', 368),
(8, '2200.00', 10, '220.00', 'NonFree', 'CORD1003', 369),
(9, '1704.00', 12, '142.00', 'NonFree', 'CORD1003', 370),
(10, '4400.00', 20, '220.00', 'NonFree', 'CORD1004', 371),
(11, '6000.00', 20, '300.00', 'NonFree', 'CORD1005', 372),
(12, '3900.00', 20, '195.00', 'NonFree', 'CORD1006', 373),
(13, '2400.00', 12, '200.00', 'NonFree', 'CORD1007', 374),
(14, '3820.00', 5, '764.00', 'NonFree', 'CORD1008', 375),
(15, '142.00', 2, '71.00', 'NonFree', 'CORD1009', 376),
(17, '1236.00', 3, '412.00', 'NonFree', 'CORD1010', 378);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `short_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `name`, `short_name`) VALUES
(1, 'India', NULL),
(2, 'USA', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `daily_stock_details`
--

CREATE TABLE `daily_stock_details` (
  `id` bigint(20) NOT NULL,
  `added_stock` bigint(20) DEFAULT NULL,
  `closing_stock` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `dispatched_stock` bigint(20) DEFAULT NULL,
  `opening_stock` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `daily_stock_details`
--

INSERT INTO `daily_stock_details` (`id`, `added_stock`, `closing_stock`, `date`, `dispatched_stock`, `opening_stock`, `product_id`, `company_id`) VALUES
(1, 0, 10, '2018-03-19 10:41:00', 0, 10, 1, NULL),
(2, 0, 10, '2018-03-19 10:41:00', 0, 10, 1, NULL),
(3, 0, 37, '2018-03-19 10:41:00', 0, 37, 2, NULL),
(4, 0, 31, '2018-03-19 10:41:00', 0, 31, 3, NULL),
(5, 0, 37, '2018-03-19 10:41:00', 0, 37, 2, NULL),
(6, 0, 31, '2018-03-19 10:41:00', 0, 31, 3, NULL),
(7, 0, 50, '2018-03-19 10:41:00', 0, 50, 4, NULL),
(8, 0, 61, '2018-03-19 10:41:00', 0, 61, 5, NULL),
(9, 0, 50, '2018-03-19 10:41:00', 0, 50, 4, NULL),
(10, 0, 61, '2018-03-19 10:41:00', 0, 61, 5, NULL),
(11, 12, 14, '2018-03-19 10:41:00', 12, 14, 28, NULL),
(12, 0, 14, '2018-03-19 10:41:00', 0, 14, 28, NULL),
(13, 0, 1, '2018-03-19 10:41:00', 18, 19, 29, NULL),
(14, 15, 37, '2018-03-19 10:41:00', 0, 22, 30, NULL),
(15, 0, 19, '2018-03-19 10:41:00', 0, 19, 29, NULL),
(16, 0, 22, '2018-03-19 10:41:00', 28, 50, 31, NULL),
(17, 0, 22, '2018-03-19 10:41:00', 0, 22, 30, NULL),
(18, 0, 25, '2018-03-19 10:41:00', 0, 25, 32, NULL),
(19, 0, 50, '2018-03-19 10:41:00', 0, 50, 31, NULL),
(20, 0, 22, '2018-03-19 10:41:00', 15, 37, 33, NULL),
(21, 0, 50, '2018-03-19 10:41:00', 0, 50, 34, NULL),
(22, 0, 25, '2018-03-19 10:41:00', 0, 25, 32, NULL),
(23, 0, 50, '2018-03-19 10:41:00', 0, 50, 35, NULL),
(24, 0, 37, '2018-03-19 10:41:00', 0, 37, 33, NULL),
(25, 0, 50, '2018-03-19 10:41:00', 0, 50, 36, NULL),
(26, 0, 50, '2018-03-19 10:41:00', 0, 50, 34, NULL),
(27, 0, 50, '2018-03-19 10:41:00', 0, 50, 37, NULL),
(28, 0, 50, '2018-03-19 10:41:00', 0, 50, 35, NULL),
(29, 0, 50, '2018-03-19 10:41:00', 0, 50, 38, NULL),
(30, 0, 50, '2018-03-19 10:41:00', 0, 50, 36, NULL),
(31, 0, 50, '2018-03-19 10:41:00', 0, 50, 37, NULL),
(32, 0, 50, '2018-03-19 10:41:00', 0, 50, 39, NULL),
(33, 0, 50, '2018-03-19 10:41:00', 0, 50, 38, NULL),
(34, 0, 50, '2018-03-19 10:41:00', 0, 50, 39, NULL),
(35, 0, 50, '2018-03-19 10:41:00', 0, 50, 40, NULL),
(36, 0, 50, '2018-03-19 10:41:00', 0, 50, 40, NULL),
(37, 0, 53, '2018-03-19 10:41:00', 0, 53, 41, NULL),
(38, 0, 53, '2018-03-19 10:41:00', 0, 53, 41, NULL),
(39, 0, 35, '2018-03-19 10:41:00', 0, 35, 42, NULL),
(40, 0, 35, '2018-03-19 10:41:00', 0, 35, 42, NULL),
(41, 0, 36, '2018-03-19 10:41:00', 0, 36, 43, NULL),
(42, 0, 36, '2018-03-19 10:41:00', 0, 36, 43, NULL),
(43, 0, 35, '2018-03-19 10:41:00', 0, 35, 44, NULL),
(44, 0, 35, '2018-03-19 10:41:00', 0, 35, 44, NULL),
(45, 0, 50, '2018-03-19 10:41:00', 0, 50, 45, NULL),
(46, 0, 50, '2018-03-19 10:41:00', 0, 50, 45, NULL),
(47, 0, 47, '2018-03-19 10:41:00', 0, 47, 46, NULL),
(48, 0, 47, '2018-03-19 10:41:00', 0, 47, 46, NULL),
(49, 0, 50, '2018-03-19 10:41:00', 0, 50, 47, NULL),
(50, 0, 50, '2018-03-19 10:41:00', 0, 50, 47, NULL),
(51, 0, 50, '2018-03-19 10:41:00', 0, 50, 48, NULL),
(52, 0, 50, '2018-03-19 10:41:00', 0, 50, 48, NULL),
(53, 0, 46, '2018-03-19 10:41:00', 0, 46, 49, NULL),
(54, 0, 46, '2018-03-19 10:41:00', 0, 46, 49, NULL),
(55, 0, 50, '2018-03-19 10:41:00', 0, 50, 51, NULL),
(56, 0, 50, '2018-03-19 10:41:00', 0, 50, 51, NULL),
(57, 17, 47, '2018-03-19 10:41:00', 0, 30, 52, NULL),
(58, 0, 30, '2018-03-19 10:41:00', 0, 30, 52, NULL),
(59, 0, 46, '2018-03-19 10:41:00', 0, 46, 53, NULL),
(60, 0, 46, '2018-03-19 10:41:00', 0, 46, 53, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `damage_recovery_details`
--

CREATE TABLE `damage_recovery_details` (
  `damage_recovery_details_id` varchar(255) NOT NULL,
  `damage_recovery_given_datetime` datetime DEFAULT NULL,
  `qty_given` bigint(20) DEFAULT NULL,
  `qty_not_claimed` bigint(20) DEFAULT NULL,
  `qty_received` bigint(20) DEFAULT NULL,
  `damage_recovery_receive_datetime` datetime DEFAULT NULL,
  `receive_status` tinyint(1) DEFAULT NULL,
  `damage_recovery_day_wise_id` bigint(20) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `damage_recovery_details`
--

INSERT INTO `damage_recovery_details` (`damage_recovery_details_id`, `damage_recovery_given_datetime`, `qty_given`, `qty_not_claimed`, `qty_received`, `damage_recovery_receive_datetime`, `receive_status`, `damage_recovery_day_wise_id`, `supplier_id`) VALUES
('DR10001', '2018-03-13 00:00:00', 1, 0, 1, '2018-03-13 00:00:00', 1, 2, 'SUP1000000002'),
('DR10002', '2018-03-13 00:00:00', 1, 0, 1, '2018-03-13 00:00:00', 1, 3, 'SUP1000000002'),
('DR10003', '2018-03-13 00:00:00', 1, 0, 0, NULL, 0, 2, 'SUP1000000002'),
('DR10004', '2018-03-27 00:00:00', 2, 1, 1, '2018-03-27 00:00:00', 1, 5, 'SUP1000000001'),
('DR10005', '2018-03-27 00:00:00', 15, 5, 10, '2018-03-27 00:00:00', 1, 6, 'SUP1000000001');

-- --------------------------------------------------------

--
-- Table structure for table `damage_recovery_month_wise`
--

CREATE TABLE `damage_recovery_month_wise` (
  `damage_recovery_id` bigint(20) NOT NULL,
  `datetime` datetime DEFAULT NULL,
  `qty_damage` bigint(20) DEFAULT NULL,
  `qty_given` bigint(20) DEFAULT NULL,
  `qty_not_claimed` bigint(20) DEFAULT NULL,
  `qty_received` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `damage_recovery_month_wise`
--

INSERT INTO `damage_recovery_month_wise` (`damage_recovery_id`, `datetime`, `qty_damage`, `qty_given`, `qty_not_claimed`, `qty_received`, `product_id`) VALUES
(1, '2018-03-13 11:50:10', 0, 0, 0, 0, 46),
(2, '2018-03-13 12:00:58', 2, 2, 0, 1, 51),
(3, '2018-03-13 11:50:10', 1, 1, 0, 1, 52),
(4, '2018-03-13 12:00:58', 2, 0, 0, 0, 53),
(5, '2018-03-23 12:36:34', 2, 2, 1, 1, 5),
(6, '2018-03-26 13:36:09', 21, 15, 5, 10, 1),
(7, '2018-03-24 18:53:06', 8, 0, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `delivered_product`
--

CREATE TABLE `delivered_product` (
  `delivered_product_id` bigint(20) NOT NULL,
  `order_receiver_signature` longblob,
  `order_receiver_signature_status` varchar(255) DEFAULT NULL,
  `order_details_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `delivered_product`
--

INSERT INTO `delivered_product` (`delivered_product_id`, `order_receiver_signature`, `order_receiver_signature_status`, `order_details_id`) VALUES
(1, 0xffd8ffe000104a46494600010100000100010000ffdb004300100b0c0e0c0a100e0d0e1211101318281a181616183123251d283a333d3c3933383740485c4e404457453738506d51575f626768673e4d71797064785c656763ffdb0043011112121815182f1a1a2f634238426363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363ffc0001108025802d003012200021101031101ffc4001b00010002030101000000000000000000000006070304050201ffc4004510010001030202060607050703040300000001020304051106211231415161810713227191a11415233242b1c133526272d124438292d2e1f053a2b244637374c2e2f1ffc40014010100000000000000000000000000000000ffc40014110100000000000000000000000000000000ffda000c03010002110311003f00b000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010ee2de34af47cc9c0c0b56ee645311372bb9bcd34ef1bc44446dbcedb4ff00ce531511a9654e6ea5959531b7aebb557b6fd5bcefb0271c3bc7d772336de2ead6ed534dd98a69bd6e3a3d19fe28eef1ec4fd426363ddcbc9b78f6289aeedcaa29a698ed995f16a9aa8b5453555d2aa29889aa7b67bc1ec0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000062c8bf6f1b1aedfbb3b5bb544d754f74446f2a1169fa46d4be89a1538944ed7332be8ff00829da67e7d18f395638d62e65655ac7b31bdcbb5c514477ccced00b03d19697d0c6c8d4eedb8dee4fabb3331cf68fbd31e133b47f8653b6be0e25ac0c1b1896636b76688a29f1dbb67c5b00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000034b58cfa74bd272b36adbec6dccd313d53575531e73b402afe3cd4beb0e24bd4533bdbc58f514fbe3ef7ce663ca1b5e8e74dfa5eb9566571bdbc3a3a5d9f7eade23e5d29f2844ebaaaaebaabaea9aaaaa779999de665707056953a570f59a6e53317effdb5c89ec99ea8f28db977ee0ef800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000209e93b53f578f8da65bab69b93ebaec46ff007639531ee99dff00cb09d297e23cfaf5be22c8bd662ab915dcf57669a79ef4c728da3c7afcc1b3c17a2fd71ad513769df171b6b97778e53dd4f9cfca257038dc2fa251a1e914589889c8afdbbd5476d5ddee8eafff00aec800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000f9331113333b44768235c79ac7d59a1d566d55b6465ef6e9dbae29fc53f0e5e68d7a37d1a3273ae6a77a8dede37b36b78e535cf6f947e70e3711ea37388b88ea9c789ae9aaa8b18f4c76c6fb47c6677f35ada2e9b6f48d2ac615ada7d5d3ed551f8aaed9f8837800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000113f483ad7d5fa47d0acd5b64666f4cedd74dbfc53e7d5e73dc94debb458b35debb54516edd3355554f54447399535aae6e4f13f114d76a999aafd716ac513f869df688f0ef9f7c83bfe8df46fa4665cd56f53bdbb1ec5adfb6b98e73e513f3f0592d3d274fb5a5699630acf3a6d53b4d5b6dd29ed9f39de5b80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000035752ceb3a6e9f7f332276b7669e94f7cf74478cced0088fa47d6fd462d1a4d8afed2f474ef6dd9476479cfe5e2d7f46da27dfd62fd1df6f1f78f2aaafd3e28b635acbe29e23daa9fb5cab9d2aea8e716e9edf288e51e50b8f171ad61e2dac6b14f46d5aa628a63ba2019800000000000000000000000000000000000000000000043f8af8d6d69bd3c3d32aa6ee644ed5d7d745afeb5787676f7381c3fc71a9c6a966d6a37a3231ef571455bd14d3346f3d7131100b3c0000000000000000000000000000000000000000000000000000000000000000000000000000000015bfa48d6fd7e551a4d8abececcc577a63b6bdb947944fcfc136e20d5ade8ba45eccaf69aa98e8dba67f1573d51fafba255770ce957788b5ffed135576a2a9bd935cf6f3df6f7ccfebdc09a7a3dd0fe81a64ea17e9db232e2269dfae9b7d9f1ebf825ef91111111111111d510fa00000000000000000000000000000000000000000c197978f858f55fcabd459b54f5d55ced080ebfe906e5c9aac68b4faba3aa722b8f6a7f963b3cfe10099eb1aee9fa2dae9e6df8a6a98de9b54f3aeaf747ebd4aef5fe38ced522bb18713878b57298a67dbae3c67b3dd1f368693c3fab711e4557a88ae68aaaf6f26fd53b4cfbe79d53ee6beb963030b27e858172723d4ced7722ae5d3afb6298eca63e73bf398d81cc48782f44bbab6b36aecd3318b8d5c5cbb5cc729db9c53e7f96ee668da55fd6752b5878f1ceae7557b6f1453db54ffcee5d1a6e9f8fa5e0dac3c5a3a36adc6de354f6ccf8c83680000000000000000000000000000000000000000000000000000000000000000000000000000001c4e2dd6a344d16e5da2adb22efd9d98fe29edf2ebf877820de9035afac356fa159ab7c7c499a676eaaae76cf9757c7bd35e0dd13ea6d1688b94ed937f6b97bbe3ba9f28f9cca0dc09a34eabad7d26fd3d2c7c598b956fcfa55fe18fd7cbc56c000000000000000000000000000000000000000e46b9c47a76876ff00b55de95e98de9b1473ae7fa478c83add51bca25aff001e6169fd2b3a774733223974a27ece9f3edf2f8a1baef15ea5af57ea237b38d54ed4e3da999e973e5d29ebaa7e5e0eaf0ff0064657472357aaac7b33ce2cd3fb4abdff00bbf9fb81c2aabd6b8b350dbed72aef6531ca8b71f95309b683c038987d1bfaa551977e39fab8fd9d3feaf3e5e09560e0e2e9d8d4e3e1d8a2cdaa7aa9a63e73df3e32c5abea76348d36f66e44fb36e3953bf3aeaeca63de08ff001cf10468fa7d3a7e155d0cabf4ed1d0e5eaadf56f1dd33d51e7dd0abed5ab97aed16ad5155772baa29a69a6379999ea8866cfccc8d4f3eee5645535debd56f3b7ca23c3b164f0570a46936a9cfcda2273aba7d9a67fb989ecfe6eff877ee1d0e13e1ea341d3b6af6ab2ef6d55eafbbba98f08f9bbc0000000000000000000000000000000000000000000000000000000000000000000000000000000029fe2ed62bd7b5d9a71f7aec5a9f55629a79f4b9f3988f19f96c9b71f6b9f56e93f44b35ed9397134f2eba68fc53e7d5e73dc8e7a39d13e979f56a77e9dece34ed6f7eaaae77f947ce60138e1ad229d1346b389cbd6cfb77aa8edae7afe1d5e4ea8000000000000000000000000000000000f35d745ba2aaee554d14531bd55553b44477c83d35f37371b031eac8ccbf459b54f5d55cede51df3e089ebde9031713a5674aa232af4729bb572b74fbbb6afcbc50ba2deb7c579fbfdae5dc8ebaa79516e27e548243c41e906edee963e8d4cd9b7d53915c7b53fcb1d9f9fb9c5d1786755e21bdf489e95166bab7af26f6f3d2e7ce63b6a9ebfea99683c078581d1bda8cd399911cfa331f674cfbbf179fc12e88888da23688071b41e18d3b43a62ab16fd6e4edcefdc8deaf2ee8f779eeed003e55314d33555311111bcccf62a6e34e229d733e9c7c5999c3b156d46dfde55d5d2fe9feeec71f71474e6bd1f02e7b31cb26e53dbfc11fafc3bce00e18e94d1ac675be51cf1add51d7fc73fa7c7b81bbc19c1f183146a3a9db89ca9e76ad551fb2f19fe2fcbdfd5350000000000000000000000000000000000000000000000000000000000000000000000000000000018efdeb78d62e5fbd5c516add335d754f64473996440bd246b7d0b5468f62af6abdae5fdbb23f0d3faf94778229a8e565714f114d56e899b9915c5bb36ff00729ecf9739f395b9a4e9d674ad36c6158fb96a9db7fde9ed9f39e6877a37d0fa16abd62fd3ed57bd16227b23f155fa7c7bd3d000000000000000000000000000000007135de29d3744a6aa2f5cf5b93b72b16f9d5e7dde7f3575adf166a9ae553662a9b18f54ed162cefed784cf5d5f9780273aef1be9ba5f4ad63cfd332639746dd5ec533e357f4dfc95fea3ad6b1c4b954d9ae6e5de94fb18d6299e8fc3b7df3bbafa1700e6e6f46f6a554e1d89e7d0dbed2a8f77e1f3e7e0b074ad1f0347b1eab071e9b7bfdeafaeaabdf3d72086683e8f2a9e8dfd6abe8c75fd1edcf3ff1551f947c53cc4c4c7c2c7a6c62d9a2cdaa7aa9a236866000004478df8a234ab13818573fb6dd8f6aaa67f634cf6fbe7b3e3dce8715f11dad0307d9dabccbb13166df77f14f847cfe3b56fa168f97c4fabd5d3aeb9a66ae9e45f9ec89fd67b23fa037782f86a75accfa4e5533f41b357b5ffbb57eefbbbffdd6bd34d345314d3114d311b44446d110c58789630712d62e2db8b766d53d1a698ec6700000000000000000000000000000000000000000000000000000000000000000000000000000000006aea79f674cd3afe6e44fd9d9a7a5311d733d91e73b479aa0c1c7cbe28e238a6e55337322e4d776b8e7d0a7b67dd11ca3ca122f493acfadc9b7a4d9abd8b3b5cbdb76d531ca3ca277f3f0767d1e68bf40d2e73ef53b5fcb889a778e74dbecf8f5fc012ac7b16f1b1edd8b344516add314514c764446d10c8000000000000000000000000000f35554d14cd554c534c46f3333b44421bc43c7d8d87d2b1a4c53937a394dd9fd9d3eefdefcbde094ea5a9e1e958d37f3afd3668ecdfaea9ee88eb9577aff001ee666f4ac6971562589e5eb3fbcabcff0f973f171f1b0f5ae2bcfaae47acc9b9d55ddb93b5144776fd51ee8f827dc3fc1181a5f42fe5c46665473deb8f6289f0a7f59f90215a1707ea7ad4c5fae271b1aae7ebaec4ef578d31d73efe51e2b1f44e1ad3744a62716cf4afedb4dfb9ceb9fe9e5b3ae00000000e76b9ac63e89a6d79791ce6395bb713b4d7576436f2f2ace162dcc9c9b916ecdaa7a55553d90a835dd5f2f89f59a7d5d15cd3357abc6b11d913facf6ff00b031534ea5c57aecff0079917e7799eaa6dd3fa531ff0037995b5a2e938fa2e9d462634728e75d731cebabb665a7c2bc3b6b40c0e8ced5e5ddda6f5c8eff00dd8f08f9bb8000000000000000000000000000000000000000000000000000000000000000000000000000000000034b58d46de93a5e466dde716a9de29fdeabaa23ce766eab6f493ac7afcdb5a5d9abecec7b7776edae63947947e7e00e1f0fe9f7789388e9a72266ba6bae6f6457fc3bef3f199dbcd72534c534c534c445311b444754231c03a3fd5ba2464dda76bf99b5c9dfb28fc31f09dfcfc128000000000000000000000001a9a8ea587a5e3cdfcec8a2cdbea8dfaea9ee88eb9f206db89aef1469da1d3345eb9eb7276e562df3abcfba3dff0034335fe3ecbcde958d2e9ab12c4f29b93fb4abfd3e5cfc5a1a0f086a5ae55191777c7c6aa779bd76379aff00963ae7dfd5e20c5ac7126adc477e31e3a54daae76a316c44cf4bbb7edaa7fe6d0eff000ffa3eaaae8e46b557463ae31e8ab9cff34c7e51f14bb44e1fd3f43b5d1c3b5f6931b577abe75d5e7d91e10ea831636359c4b14d8c6b545ab5446d4d1446d10ca000000003e3ea0dc7fc4df47b75691855fdad71fda2b89fbb4cfe1f7cf6f87bc1c4e38e269d5b2be83875ff0062b3573aa3fbdabbfdd1d9f1eeda4bc0dc31f5663c6a19b6f6cdbb4fb14d51ced533facf6f77577b8dc03c33f4ab94ead9b47d8d13f6144c7dfaa3f14f847e7eee76400000000000000000000000000000000000000000000000000000000000000000000000000000000000003475ad4ade93a56466dcda7d5d3ecd33f8aaec8f8aaae1bd3ae711f1247d2666ba26b9bf91577c6fbcc79ccede6ebfa47d67e959f469966aded637b57369ebae63abca3f3949780b47fab344a6fdda76c8cbdae55df14fe18f873f30499f40000000000000000000079aeba6dd15575d514d14c6f55554ed111df2e66bbc4181a158e965dcdeed51bd1668e75d5fd23c6558eb7c49a9f1164459f6a9b3555b5bc5b5bcef3bf2dfb6a9ff009110096710f1fd8c6e9e3e8f4d37eec6f137eafb94fba3f17e5ef4371b1358e29d426aa7d6e55de515ddae7d9b71e33d511d7ca3ca125e1ef47f5dce8646b533451d718d44fb53fcd3d9ee8e7e309f62e2d8c3b14d8c5b3459b54f551446d1008d681c0d83a6746fe6ed99951cfda8fb3a27c23b7df3f084ac00000000000073f5bd5f1f44d3ae65e473db951444ed35d5d9100e7717f11d1a160f42ccd3566de8dad53d7d18fde9f0fce7cd01e16d06f711ea955dc89ae71a8aba79176a99deb99e7d1dfbe5ab6adea3c59afcef3d3bf7a77aaadbd9b5447e511ff39caddd2b4dc7d274fb5878b4ed4511ce67aea9ed99f1906cdab7459b545ab54c51451114d34d31b44447543d800000000000000000000000000000000000000000000000000000000000000000000000000000000000e76bdaa5bd1b48bf9b5ed35514ed6e99fc55cf547fcecddd1561e91b59fa66a74e9d66adece2f3af69e555c9fe91cbdf320e5f0c6997388388a98c899b9474a6fe4553f8a37de7e333b79ae246b80f47faaf43a6f5da76c8cbdaed7df14fe18f84efe693000000000000000000d7cdccc7c0c6af272ef536ad511ceaaa7e5e33e00ce85f1371ddac3e9e269134debfd555feba28f77ef4fcbde8f714719e4eaf3562e174b1f0a794fefddf7f74787c5b7c33c09772fa195abc5566c75d363aabafdffbb1f3f70389a568daaf13e7577699aab89abed726ecced1e7db3e11f2859ba070d606856b7b147acc898dabbf5c7b53eeee8f0fcdd5c7b16b1ac51671edd36ad511b534511b44320000000000000000316564d9c3c6b99191722dd9b74f4aaaa7b214ff00106b393c4dac53eae8ae6df4bd5e359ed8de7f39e5bffb3a9c77c4bf59e54e9f87737c3b357b7553d576b8fce23b3e3dceef0170cfd06cd3aa66d11f49bb4fd8d131cedd33dbef9fcbdf20ebf0a70f5bd074ee8d5b57977769bd5c77feec7847cddd0000000000000000000000000000000000000000000000000000000000000000000000000000000000000073f5dd4e8d2348c8cdab699b74fb14cfe2aa79447c7e4aaf85f4caf5fe22a29c899b9445537f22aaa779aa37de77f7ccc479bbbe93355f5b97634bb757b3663d6ddfe698e51e51cffc4eefa3dd27e81a1fd2ae53b5ecc98af9f6511f763f39f304a8000000000000000114e2be32b3a44558983d1bd9dd533d74daf7f7cf87c7c43a9aff0011616838fd2c8aba77ea8dedd8a67daabfa478ab0cfd4756e2cd4e8b7d1aaed533b5ab16feed11ff003ae64d2f49d538ab52aee74aaafa556f7b26e7553fd67ba23e50b4b43d0b0b43c5f558946f5d5fb4bb57deae7c7c3c01c8e17e0cc6d23a3959bd1c8cdeb8e5bd16fdddf3e3f04ac0000000000000000043b8fb893eafc59d3712bdb2afd3f695475dba27f59fcbc9dfd7f58b3a26977332ec74aa8f66dd1bfdfaa7aa3f5f742aad2b03338ab5f98b9726aaeed5372fdd9fc34f6ff488f703abc09c35f59e57d61996e270ec55ecd35755dae3c3ba3b7e1deb49870f16ce1625ac6c6a22dd9b54c534d31dccc00000000000000000000000000000000000000000000000000000000000000000000000000000000000003165645bc4c5bb937a76b76a89aea9f088de5950ef493a9fd1b47b78144fb79756f57f253b4fe7b7cc107c2b37b89b89a9a6eccf4f2ef4d772627eed3d73b6fdd11cbc973d14536e8a68a298a68a62229a6236888ee417d19697d0b393aa5c8e75fd8daf7473aa7e3b4794a78000000000000002b9e34e31f5feb34cd2aefd8fddbd7e99fbff00c34cf777cf6fbbac3638bf8dbd5facd3f47b9edfddb9934cf578533dfe3f0ef70b85b84f235db9193913559c189e773f15cef8a7fab6b83f83ead5669ced429aa8c289de9a3aa6f7f4a7c567dbb745ab74dbb74534514c44534d31b444774403161e1e3e062d18d896a9b56688da9a69ff009ce7c59c0000000000000000079aeba6dd15575d514d34c6f354ced111def481fa44e21f556fea7c4afdbae22722a89ea8eca7cfae7c36ef046b8a75abbc45acc538f15d5628abd5e35b88e756fdbb77ccfe8b1b85741a341d2e9b53b5593776aafd71db3dd1e11fd67b51bf477c3bb446b3974739de31a99eeedaff0048f3f04fc000000000000000000000000000000000000000000000000000000000000000000000000000000000000000053dc5b9d5eb7c51769b1137229aa31ecd31cfa5b4edcbbf7aa67e2b2b8a353faa741c9c9a6ae8dd9a7a16bbfa73ca3e1d7e4807a3cd33e9daf7d26ba77b5874f4f9c72e9cf2a7f59f20595a4e051a66978d856f6dacd114ccc76cf6cf9cef3e6dc00000000000015f71af186feb34cd2ee77d37efd33f1a699fce41e38df8bbd6facd2f4cb9ec7ddbf7a99fbddf4c7877cf6f57bf4f82f84a752ae9d4350a2630e99de8a27fbd9ff4fe6f9c19c2356a95d39f9f44d3854cef4513ca6f4ffa567d34d345314d3114d34c6d1111b444014d34d14c534c4534c46d1111b4443d0000000000000000000c77ef5bc7b172fdeae28b76e99aaaaa7aa223ae41cbe26d72de83a5d57e7a355fafd9b36e7f155dfee8eb9ff756bc37a3dfe25d6ea9c8aaaaad455eb726ecf5cef3d5ef99fd67b18f5dd532789b5c89b54d734d557abc6b3dd1bf2f39eb9ff65a3c39a2dad0f4ab78b46d55c9f6aed71f8aaedf2ec8074eddba2d5ba6ddba628a2888a69a62368888ea87a0000000000000000000000000000000000000000000000000000000000000000000000000000000000000001e2edda2cdaaeeddaa29b7453355554f544475c82b9f499aa7aeceb1a6dbabd9b14facb9113f8e7aa27dd1ff009249c05a67d5fc3b6ee574c45dca9f5d572e7d19fbb1bfbb9f9cabab34dde25e28a62ae974b32fef56d3bcd146fbcedeea63e4ba28a69a28a68a298a69a6368888e5100f40000000003e4cc446f3ca15cf19719ce4facd3b4ab9b58fbb76fd33fb4fe1a67bbc7b7ddd61978cf8cba5eb34dd26ef2fbb7b2299ebfe1a67f568f07707d5a9d5467ea344d38513bd144f29bbff00ebf9b2706f074e774351d4e89a717ef5ab33d777c67f87f3f775d974d314d314d31114c46d111d500f94534d14534514c534d31b44446d110f4000000000000000000002bbf48baffacb9f53e2d7ec51b5591313d73d714f975cf8eddc97f12eb14e89a35ecbe53767d8b34cf6d73d5f0e73e4ac785f47b9c45ae445f9aebb34cfadc9b93bcccc6fd5bf7ccfeb3d8094fa3be1ef5167eb8caa36b9722631e998fbb4f6d5e7d9e1ef4e9e68a29b74534514c534d31b45311b444773d000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008f71d677d0b85f2629aa69af2262cd3b475efd71fe58a921403d29656d46061d35f299aaed747c2299f9d40d3f4638317754cacdaa22631edc514ef1ce2aabb63ca263cd65a2de8eb13e8fc334dd9da6726ed573c6223d9dbfed9f8a5200000003cd5553453355754534d31bcccced110575d36e8aabb9545345313355554ed111df2abf8c78bebd56bab0702a9a30699daaabaa6f4ffa7c3e20c9c63c6356a135e9fa6d734e2755cbb1ca6ef847753f9b63833837e91eaf52d56dfd8fdeb362a8fbff00c557877476fbbafdf05f077acf57a96ab6fd8fbd66c551f7bbaaaa3bbba16183e3e8000000000000000000000e3f156aff00536877f2699daf55f6767f9e7b7ca379f2057fc7dac7d65adce35aab7b187bdb8f1aff0014fc797978a75c1fa2c68ba2dba2e53b64defb4bd3b7389eca7ca3e7ba07c07a3fd69ae45fbb4ef63136b95efdb57e18f8f3f25b40000000000000000000000000000000034f3f54c1d368e966e5dab1cb788aeaf6a7dd1d73e40dc10bd43d2369f62a9a70b16f656d3b74aa9f574cc77c75cfc621c1c8f48babdcde2cd9c5b31bf298a26a9f9cedf205a42bae18e35d4f335ac7c3cd9b776d5faba3bc51d19a67b3a96280000000000000000000000000000000000000000000000000000a9fd22e4fafe27aededb7d1ed516fdfbfb5ff00e4b614d67edac719dda3a7d2a32337d5c551fbbd2e8c4fc016b6878bf42d13071e69e8556ec511547f16dcfe7bb7c0000079aeba6dd15575d514d14c6f55554ed111df2575d36e8aabb9545345313355554ed111df2abb8cb8baad5abab0702a9a30699f6aaea9bd3fe9ee8f8f8038cb8b6ad5abab0706a9a70699f6aaea9bd31db3e1dd1e7eee8f05f0774bd5ea7aadbe5caab362a8ebeeaaafd21eb8338376f57a96ad6f9f2aacd8aa3feeaa3f2858000000000000000000000000002aaf485ab7d3f5afa1daab7b387bd1cbb6b9fbdf0e51e52b0f887538d1f45c9cce5d3a69dadc4f6d73ca3fafba255a704e973abf115172f44d76b1e7d75d99fc53bf28f39f94482c2e11d23ea7d0acd9ae9dafddfb5bdbf5c553d9e51b43b6000000000000000000000000d0d4b59d3b4aa77cecbb5667ae2999dea9f7531cd12d4bd2459a3a546998555c9db95cbf3d18dff9639cc79c0278d6cbcfc3c1a62acccab36227abd65714efeedfad53667176bda8dce84665cb51555ecdbc68e87944c739f8bee17086bda95ce9d58b5d98aa7dab9933d0f8c4fb5f204d750f481a3e344c637adcbaf6e5d0a7a34efe333fa44a3b9be91b52bdd28c3c6b18f4cc729ab7aea8f3e51f274703d1b5a888ab51cfaea9db9d1629db69fe69df7f824385c21a161ed3469f6eed5b6d355e99b9bf94f2f902b6b9aff116af762d53999776b98dbd5e3c74778f75111bb630b8275ece98aabc78b14d5cfa77ebdbe31ce7e4b6ad5ab762dd36ecdba2ddba79453446d11e4f6082e9fe8db1a888ab50cdb976797b166228889f7cefbfc9ddb7c1fa05aa2aa69d3a898aa3699aabaaa9f8ccf2f277407174de15d1f4bcc8cac5c59a6f46fd19aab9aba3bf76f2ed0000000000000000000000000000000000000000000000000000031655fa71b16f5fafeedaa26b9f7446ea8b822dc5fe2dc18ae3a5115555f9c53331f3d96aeb16eabba3675ba237aabc7b94c478cd32abb806ae8f16e247ef45c8ff00b26416f000316564d9c3c7af2326e536acdb8deaaea9da21e337331f0316e64e5dda6d59b71bd554ff00ce73e0a8f89b897275fc9da666de1d13bdab3bfceaef9fc81bdc5bc5f735999c4c2e95ac189e7bf2aaecf7cf747879cf876b833837d4fabd4b55b7f6bcaab362a8fb9dd555e3e1d9efead5e01e18f5f5d1abe7511eaa99fecf6e63ef4c7e29f08ecf1f773b1800000000000000000000000000015e7a50cf99bd87a7533314c5337eb8ec999de9a7e1b55f1767d1de9d187c3f193553b5dcbaa6b99db69e8c72a63f39ff00120dc6991197c579b344ccc535c5b88f1a62227e712b730b1a9c3c2b18b44ccd366dd36e267b62236fd019c0000000000000001af999b8b81666f66645bb16ff007abaa237f08ef906c3c5cb945ab755cbb5d34514c6f55554ed111df3284eafe9171ed74ade938f37ebff00ab7a269a3ca3ae7e4875fccd6b89b2e2dd555fccaf7de2dd11ecd3d9bed1ca3de09feadc7ba560cd56f17a59b76397d9ced47f9a7f48943b52e35d6b52aa6ddabbf45b754ed1463c6d54ff008bafe1b3ada3fa39bd73a37356c8f534ff00d1b3b4d5e75754796e9ae99a1e9ba4d31185896edd5db72637ae7fc53cc159e9dc19ae6a757acb967e8f455ce6e64cf4667cbafe49669be8ef4dc7daacebd7732afdd8fb3a3e11cfe69880d5c2d3b0b4fa3a3878b66c44c444faba22267df3dbe6da0000000000000000000000000000000000000000000000000000000000000000014c614ce87c5f6e9aae4d34e2e5f42aaff83a5b4cf9c6eb9d577a46d26e62eb1f585344cd8ca88dea88e54d711b6de71113e3cfb8168b4f54d4f1349c2ab2b36ec516e9e511db54f74476cabdd3bd22676358a2d65e2dbcae853d1e9f4e69aaaf199e7bca3baceb399ad664e4665cde63951453ca9a23ba201b5c49c4795afe56f737b78d44fd9d989e51e33df2dde0ee15ab5bbff49cba6aa702dcf3db94dd9fdd8f0ef9f2f766e13e0dbdaad54666a11559c1eba69eaaaf7bbba3c7e1df167d8b36f1ecd166cd14dbb7447469a698da2201ea8a28b76e9b76e9a68a29888a69a6368888ec887a0000000000000000000000000001aba9dfab174bcbc8a7ef5ab35d71ef8a66415269f3f5a71a59b9146f4dfcef5b34ff000f4fa53f25caa8fd1fdbe9f166355bfece8aeaff00b663f55b800000000000c1979b8b856bd665e45ab14764dcae29dfe28bea7e90f4cc6de9c1b77332befdba147c679fc812f71b56e28d2748e953919315dea7fb9b5ed57bf74f6479ecadf54e2ed6b579f55eba6cdbaf97a9c789a7a5d9b6fd73bf76fb36748e04d5b50e8d7934c61599edbb1edf953fd7606deabe9133f237a34eb346251fbf57b75cfe91f09f7b8f89a36bbc477a2ffabbf7a2affd45faa629dbc267afdd1bac4d2783347d33a35fa8fa55e8fef2ff00b5b7ba9ea8fcfc520042749f4758b67a37354bf3915f6dab7bd347c7ae7e4986261e360d88b38962dd8b71f868a6223dfef670000000000000000000000000000000000000000000000000000000000000000000000000061c9c6b1998f5e3e4daa6edaae36aa8aa3789660108cff46f877af4d7859b731a89feeeaa3d6447ba7789f8eed9d1b803034fbfebf32ece7574fdda6aa3a3447be379dff2f04b807c7d00000000000000000000000000000183371e9ccc2c8c6aa6629bd6eab7331d9131b7eace0292d2736f70febd6f22bb73d3c7b9345db73d7b738aa3dfd7e6b2e38e387a6adbe9d311dfea6bdbf271f8c7833233f36ad434b8a2ab973f6b666629de7ab7899e5efdff00540b2f4ecdc1aa69cbc4bd667f8e89889f305c18dc51a1e4c4cdbd4f1e9dbfea55eaff00f2d9b11ae69331bc6a985b7ff628feaa39bf87a26a99f14d58b8191729abaab8a2629ff34f205b391c53a1e37ed353c79ffe3abd67fe3bb43278fb42b14c4dbbd7b2267b2d5a98dbfcdb2114703710553b4e1534477d57a8fd25bf6bd1c6ab54d3eb72b0e8a67af6aaaaa63cba3fa83a39be92a88e95383a7553fbb5deaf6f8d31fd5c0cee38d77377a68bf4e353546dd1b146df39de7e129460fa38c0b3574b372ef64ed3bc534445ba663ba7ae7e1309369fa2e9ba6447d0b0acd9aa3974e29deaff0034f3f982acc6e1ae20d62f7adaf1afef54c74af655534f9fb5ce7cb74934ef46d446d56a79b354f6dbc78da3fcd3d7f084f8073f4cd134dd269db0712ddaab6da6bdb7ae7fc53cdd0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007ffd9, 'Delivered', 'ORD1000000004'),
(2, 0xffd8ffe000104a46494600010100000100010000ffdb004300100b0c0e0c0a100e0d0e1211101318281a181616183123251d283a333d3c3933383740485c4e404457453738506d51575f626768673e4d71797064785c656763ffdb0043011112121815182f1a1a2f634238426363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363ffc0001108025802d003012200021101031101ffc4001b00010003010101010000000000000000000005060704030201ffc4004d10010001030201080607040704090500000001020304051106122131415161718107132291a1c1141523324252b1336272d124438292b2e1f03453a2d2175463738393c2e2f116255574a4ffc40014010100000000000000000000000000000000ffc40014110100000000000000000000000000000000ffda000c03010002110311003f00d000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001f933111bcf3403f456f55e37d1f4e99a2ddd9cbbd1f86c73c478d5d1eedd50d4fd20eab97bd3874dbc2b73f97dbafdf3cdee88069d91936316d7adc9bd6ecdbdf6e55caa298f7ca36ff0014e878f3b57a9e3cff00ddd5cbff000eecbec689afeb95ce4538d93913546febaf55b4551d1cd5553cfe4fdd4385355d330aacbcdb56ecdaa7689deed333333d111113ce0d53035fd2b52b9eaf0f3acdcb93d146fc9aa7c22769949300a6aaa8aa2aa2a9a6aa6778989da625b6f0ee65dd4341c2cabfbfadb96a39533d731cdbf9edb82480000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001f35d74dba2aaebaa29a698de6a99da223b41f4fce88de553d6b8fb4ec1e55ac189cdbd1cdbd33b5b8fed75f97377a8fa8ebdad7115ef5155772ba6b9f671b1e998a67ca39e7cf705fb5ae38d2f4d89b78f57d36fc7e1b557b31e35747bb750b54e21d6388af7a89aab9a2b9da9c6c7a6769f28e7aba3af74d68be8f32aff26eead77e8d6ffdd5b98aab9f19e88f8af9a668f81a4daf57838d45adfef55d3555e333cf20cff46f47b9d95c9bba95c8c4b53cfeae9f6ae4fca3e3e0bb695c31a4693c9ab1f129aaed3fd6ddf6ebdfb79fa3cb64c00326e39e20fadf52fa363d7be1e34cc53313bc5cabaeaf0ea8ff0035c78ef5dfaab499c6b15ed9597134d3b4f3d14fe2abe51e3dcca28a2ab95d34514cd55553b45311bcccf603a74cc0bdaa6a1670b1e226e5dab6899e888eb99ee88e76df878d461e1d8c5b5bf22cdba6dd3bf4ed11b2bdc13c353a2e24e4e547f4dbf4ed547fbba7a793e3d73e5d9cf680000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000071ea5aa616958febf3afd36a8eadfa6a9ec88e99673c41c7799a8f2ac69d1562634f34d513f695c78fe1f08f782e3c41c5fa7e8bcab513f49cb8fea6dcfddfe29eaf0e9ee675a9eb7abf12e4d366b9aee4553ec6358a6793ee8e9f19dd21c3fc119daa4d37f3795898b3cfbd51edd71dd1d5e33f168da4e8b81a358f57838f4d1331ed573cf5d7e33f2e8052345f4777ef445dd5ef7a8a7fdcda989afceae88eae8dfc97ad374ac1d2ad4dbc0c6b76627a66237aaaf199e79f3768000038f55d4b1b49c0b999975f26dd1d111d354f54477bdf2726ce2635cc8c8b94dbb56e39555557444320e28e21bdc41a872a98aa8c5b73b59b5d7e33df3fe40e7d47373b8975af59c89aef5eaa28b56a9e7e4c75531fcfc65a2f0a70858d1223272669bf9d31f7a23d9b7dd4f7f7ff9efe5c13c2f1a463466e651fd3aed3cd4cff554f678cf5fbbb77b6000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e3d4f53c4d2712ac9cdbb16edc734475d53d911d720ebe88de54de22e3dc6c2e563e9514e4dfe89bbd36e8f0fcdfa78f42adc49c5f99add5563d9e563e16fb45aa67daaff008a7afc3a3c7a525c39c057f2b9393ac72ac59e98b11cd5d5e3f963e3e0081c6c4d638af519aa26e64dd9fbf76e4ed45b8f1e888ee8f28685c3dc1783a4722fdf88cacc8e7e5d51ecd13fbb1f39e7f04fe26263e0e3518f8b668b36a88da9a698da3fd77bdc00000007cd75534515575d514d34c6f3333b4443e99af1cf15fd32aaf4bd3ae6f8f4ced7aed33fb49fcb1fbbfaf874870f19f1455ad647d1712a9a702d55cdd5eb6afcd3ddd91e7e133c05c2dc9f57abea16f9fef635baa3a3f7e63f4f7f638f82384be9d551a96a56ff00a2d33bdab5547ed67b67f77f5f0e9d2c00000000000000000000000000000000000000000000000000000000000000000000000000000005238a38e6bd373ebc1d36d5ab972d4f26e5cbbbcc44f64444c742eec6b8bf4cc8d3b5fcaaaf5157aabf76abb6ee7555154efd3db1bed2093a3d22eb34c6d3670aaef9b757caa4a69de92689a793a9e0d513b7dfc69df7fecd53cdef67aeac3d333b3e2aab0f0efdfa69e699b76e6a88f38068d1e91f47dffd9f3a3ff0e8ff0099d51c7ba0cdbe54dfbb13f926d55bff002f8b3ab5c35adddae29a74bca899fcd6e698f7cbb68e06e20aba70e9a3c6f51f2905c2bf48ba3533b459ccabbe2dd3f3a9f3ff0048fa3ffd5f3bff002e8ff995bb5e8ef5ab94ef55cc4b53d95dc9f944baecfa35ceabf6f9f8f47f053555faec0999f48da3ff00d5f367ff000e9ff988f48da3ccff00b3e747fe1d1ff323bfe8ca7ffcbfff00cdff00b89f4653b736afcfff00eb7fee04fd9e38e1fbb4c4ce6d56e67f0d76abde3dd13090b7c41a35da62aa754c3e7eaaaf534cfba6548b9e8d7363f679f8f57f153547f3735df475ac514ccd17b0ee77457544fc69069b6326c64d3cac7bf6ef531d76eb8aa3e0f563b7b833882cd15573a7d55534fe4b94553ee89dde1ebf88746a29debd470edc4fb315f2e9a267c279a41b48ca3138ff005cb133ebabb19313fef2dc46dfddd9358be92ed4cd3197a6d74c75d56ae455f0988fd417d15bc3e39d0b2b68ab22bc7aa6768a6f5b98f8c6f11ef4fe3e4d8cbb5eb71afdbbd6f7db976eb8aa3df00f50000001f8a471471d5bc6e5e268f55376f745591d34d1fc3db3dfd1e209ae25e29c3d06d4d13b5eccaa3d8b34cf477d53d51fab34b97357e2cd5ba2bc8bf574531cd45ba7f488ff005d2e8d0386f50e24caaafdcaaba31e6adee64dce79aa7af6fcd2d4b48d230f46c48c7c2b51447e2ae79eaae7b667ac113c33c1f89a2c537eff0027273b6fda4c7b344feec7cfa7c3a16500000000014ee35e2c8d368ab4fd3ebdf32a8dae5c8feaa27ff57e80e5e3ae2b8b34dcd274eb9f6b3ece45da67eec7e58efedece8e9e888e0be13ab55b94e7e7d1318544fb34cff5d31f2ffe1e3c1fc2f5eb793f4bcc8aa306dd5cf33d3767b23bbb67fd46ad6e8a2d5ba6ddba628a2888a69a698da2223a2200a69a68a629a6229a62368888da221f4000000000000000000000000000000000000000000000000000000000000000000000000000000003cef59b5916e6ddfb545da27a69ae98aa27ca5e80392d697a7d9fd96062dbfe1b34c7c9d4fd00000000000000001c197a2e999b354e4e9f8d72aaba6b9b71caf7f4a172f80344c89ded537f1bbad5cde27fbdbad20338ccf46b93446f85a85abb3bf3d37689a368f18df7f82032346d7f40b9ebe6c64e3f263f6d62ade223beaa679bcdb300cb34df483aae2ed4e5d16b3288e99aa3915fbe39be12b5699c7ba466cd34644dcc3b93b47dac6f46ff00c51facec91d4f86347d539556461d14dd9dfed2d7b156f3d7cdd33e3baa1ac7a3abf669aaee9391f48a63fa9bbb457e55744fc01a2dbb945db74dcb75d35d1546f4d54cef131db12f3cbcab1858d5e4655da6d59b71bd55d53b44322d0f88752e1bcbaaccc5736a2adaee2ddde369ebdbf2cff00a97ceb7aeea1c4d9d451c9abd5cd5b59c6b7bced3f39effd0123c53c677f569af130795630ba2a9e8aaef8f64777bfb23ab85781ee65f233357a6ab763a68b13cd557e3d91f1f04cf0a7055ad3bd5e66a74d377323daa6df4d36a7e757c23abb57107c5ab7459b54dbb545345ba236a69a636888ec887d8000000000ab718715dbd16d4e2624d35e7d71e316627ae7bfb23ce7bc3e78cb8b28d1ed55878554579f5c73cf4c5989eb9efec8f39efa5f0bf0e6471167557afd55c625156f7af4cf3d73d3c989eb99eb9eaf76fe7c3ba065f12ea355772bae2c455cabf7eae79999e7da267a6a96b98589630312de2e2db8b766dc6d4d31feba41f58f62d62d8b762c5ba6ddab74c534d34f4443d40000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000045ebfaee26838537f2679572ae6b56627dab93f28ed9ff00287971271162e8189cab9317326b8fb2b313cf577cf64330a68d578b75a99e7bd7ee74cf4516a9f9531feb79907c64ded478a75a9ae2dfadc9bd3b534511b4534c7e911db2fcb75ea3c2dae6fb7aac9b13b4d33cf4d74cfeb131feb76abc3dc3f89a0e27abb11cbbd547dadeaa3daae7e51dcae7a4fc0a2ac2c4d422222e5173d4d5311cf34cc4cc7ba627de0b7e95a85ad534db19b6378a2f53bed3d53d131e53130eb523d1865cdcd33331277fb1bb15c4f7551d1ff0cfbd770000000015ee2ce26b5a0e2722df26bcebb1f676ff002c7e69eefd7de0f2e2fe2ab7a2589c7c69a6bcfb91ecc74c5b8fcd3f2850341d133389752ae66babd5f2b957f22ae7db7fd665f9a2e919dc51ab57335d53bd5cbc8c8aa378a77f9cf547ca1ade9ba763697856f130edf22d51efaa7ae667ae41f5a7e063e9b876f1712dc5bb56e3688eb9ef9ed974800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000af714714e3e83666dd1c9bd9b5c7b16b7e6a7beaeeeeebf8b8b8b38cad69515e1e9f34ddcde8aaae9a6cff39eef7f6295a0e839fc4d9f5dcaabae2d72b7bd935f3f3f6476c83e34fc0d4f8b757aea9ae6baea9e55ebf5fdda23fd744355d1746c4d130a31f128e79e7aee4fdeae7b65eba669b8ba4e1518b876e28b74f4f6d53db33d72ec015ee3cb715f09664cc6f34722a8eef6e9f96eb0ab9c7b7e8b3c27954d556d55d9a28a23b679513b7ba24155f46176a8d6f2ad44fb35634d531df1553b7eb2d3599fa30b35d5ace5df88fb3a31f9154f64d5544c7f865a60000008fd6b57c6d174faf2f2679a39a8a23a6babaa201cdc4dafd8d03026ed5b57917378b36b7fbd3db3dd0cc34fc1d478af5aab95726bb9727977af55d1453feb9a2099d4b8bb5eff00797eecff0062d511fa531feb7996ada168d8da1e9f4e2e3c6f3d372e4c73d7576cff00207ae93a5e2e918346261d1c9a29e7999e9ae7ae667b5da0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003f005038b78e39ee6068d73f76e65533ef8a3fe6f776b978d38c27326e69ba5dcdb1b9e9bd7a99fdaf747eef7f5f874fc70870655a84519fa9d334624f3dbb5d1377be7b29fd41c5c2bc257f5cae32723956706279ebfc573b629fe6d531312c60e3518d8b6a9b566dc6d4d14f443d28a28b76e9b76e9a68a29888a69a6368888ea887d00000cf7d2867ef5e169f4d53b444dfae3e14ffea684c6b5bc8af8838aeefa898abd7de8b367b26378a627cfa7cc179f475a77d13409caae9dae65d7cadfaf911cd1f39f35b1e58d62de2e35ac7b31b5bb544514c764446d0f5001e77aedbb166bbd7ab8a2dd14cd555554ed1111d60f2d433b1f4dc3b997957228b56e3799edee8ef647ab6a79dc57ad51145baaa9aa7918f629fc31fcfae67e50f6e2ae21bfc45a8536ac457f44a2ae4d8b511cf5cf472a63ae67aa3abdfbde78378669d1313e91934c4e75ea7daffb3a7f2c7cc1d9c31c3f6740c08b71b57937369bd776e99ec8ee84d000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000cf38ef8aa6a9b9a469d73d98f6722ed33d3db444f676fbbb527c73c51f56599d3b06e6d99769f6eba679ed533f39f874f62b3c17c2f3ac64466665331836aae89feb6aecf0edf778076704f08fd3668d4f52b7fd1a277b366a8fdacf6cfeef775f874e93d11b414d314d314d3111111b444753f400000010bc5da9fd55c3d957a9ab9376e53eaad73ed3caab9b78ef88de7c945f473a77d2f5eab2ea8dede251cafed55cd1f0e54f93dbd246adf4ad4ede9d6aadede2c6f5ed3cd35cff0028fd656ae04d2feaee1db55d74ed7b2a7d755d1bed3f763ddb4f9c82c8000cbf8eb8a3eb2bf3a760dcdf0edcfda574cf35daa3f5a63e33cfd899e3fe25fa25aab49c3afedee53f6f5c4fdca67f0f8cfe9e3cd0dc0bc31f595f8d4736def876aafb3a6a8e6bb547eb4c7c679bb4131c07c2df46a28d5b3e8fb6ae37b16e7f044fe29ef9eaecfd2f00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020b8af88ad68181bd3b579776262cdb9ff14f747c5dfac6a98fa3e9d73332aada9a79a9a77e7aeaeaa63bd924cea3c5baff00e7bf7eafecdaa3e511feb9e41efc3da364f13eaf5d57ee5736a2af59917a7a79faa3be5aee363dac4c7b78f8f6e2ddab74f269a63a221cda36958fa369d6f0f1a3d9a79eaabaebabaea97700000000e0d7353b7a3e937f36e6d336e9f6299fc554f447bddecc3d226b5f4dd4a9d3acd7bd8c59f6f69e6aae75fba39bc664111c3ba75de22e22a68bf335d355737b22a9eba77de7df33b79b6588888da39a15ae04d1274ad1a2f5ea76c9cbdae57bf4d34fe1a7e3bf9f72cc02138ab5fb7a0e9937636ab26eef4d9a27ae7b67ba3f9475a4b3f36c69d85772f2abe45ab54ef54fca3be59067656771671044d144cddbd5722d5bdf78b74f678473cccf8c83ef87b46c8e25d62a8bb5d736f95eb326f75f3cf6f6cff9f5360c7c7b58b8f6ec58a22ddab74c534d31d1110e2d0747b1a269b6f12c7b531ed5cb9b73d7575ca4800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007cdcae8b56eab972a8a28a226aaaaaa768888e9997d289e91b5e9b56e347c6af6aeb88ab22627a29eaa7cfa67bb6ed05678ab5dbdc45aac53622a9c6b757231edc44ef56fd7b76cf678340e10e1da342d3f9576989cdbd11376ae9e4f6531dd1f19f2577d1d70f45757d739546f4d3334e3d33dbd135fca3cfb9a1800000000022789b57a745d16f656f1eb67d8b313d75cf47bb9e7c99bf06e8f56b9aef2f277aec599f5b7a6ae7e5cefcd13e33f0897af1d6b53ab6b5f47b1572b1f1666dd1b7e2abf14fcbcbbd7fe13d1a345d12d59ae9db22e7da5e9fde9eaf28e604d3f2662226667688eb7ea85e90389bd5d1568f8572797547f49ae9ea8fc91e3d7eeed04271af11ceb59d18b8954ce158ab6a76feb2ae8e57ca3fcd70e09e1bfa9b0be93934c7d36fd3ed6f1fb3a7f2f8f6ff00920bd1f70dfaeae358cca226dd13fd1e8aa3a6a8fc7e5d5dfe0d140000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001c5abea36b4ad32fe6defbb6a9de23f355d51e73b4324d2f0f2b8a388a28bb5ccd77ab9b97ee47e1a7ae7bbb23c93be91f5afa4e751a5d9ab7b58ded5ddbaeb98e8f28fd67b161e01d17eadd1e32ef53b64e5c4573bc73d347e18f9f9c7602cd62cdbc7b16ec59a228b76e98a68a63aa239a21e8000000002bdc69adfd4da35516aadb2b277b76bb63b6af28f8cc27ee5ca2d5baae5caa28a2889aaaaaa9da2223a6658df106a77b8935f9aecd35554d55459c6b7d7b6fcde7333bf9824bd1fe89f586adf4dbd4ef8f8931546f1cd55cea8f2e9f776b5447683a55bd1b49b3856f69aa98dee551f8ab9e99ff5d510edbd76dd8b35debd5c516edd3355554f444473cc821b8b75ea742d2a6ba26272ef6f458a7b27aea9ee8fd76675c2fa1dde23d5e7d7d55cd8a27d6645c99999ab79e8dfb67f9cbcb59d43278a388395668aaa9b9545ac7b5d94efcdfce7cdaae81a459d134bb5876b6aaa8f6ae57f9eb9e99ff5d5100efb56e8b36a8b56a98a28a2229a69a6368888e8887d8000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000008dd7f55a346d22fe657b4d54c6d6e99fc55cf447cfc22524cb3d216b5f4fd57e8166bdec624ed56dd155cebf7747bc1c3c2ba557c41c411393cab9669aa6f64553f8b9fa267be7e6d8101c17a2fd4fa251eb68e4e4e46d72eef1cf1d94f947c665600000000011daeead6745d2eee65ee79a636b746ff007eb9e88ff5d5120ab7a45d7fd4d88d231abfb4bb1155f989fbb4f553e7d3e1e2e7f46fa14cd756b3914f346f463c4f5cf45557cbdeace958397c51c41c9b95cd55deaa6e5fbbf969eb9f94478363c7b16b171edd8b144516ad5314d14c75447403d540f48faf7262346c7ab9e76af2263b3a69a7e7ee5df3b2ede060dfcbbd3f676689aeaed9da3a23bd8e69b8f77887892d5bbf54d55e55e9aeed513b737deab6f289d8172f475a07a8b1f5be4d1f6976269b1131f769ebabc67a3c3c5797c5bb745ab545bb74c53451114d34c744447443ec00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010fc53ac4689a2dec9a663d7d5f67663f7e7afcb9e7c99df0468f3ac6bb176f472b1f1a62edd99e7e555bfb31e73cfe112fbe3dd63eb2d72ac7b756f630f7b74f7d5f8a7df1b792fdc23a3fd4da1dab55d3b645dfb4bdbf4c553d5e51b47bc136000000000c938db5d9d6756f518f572b131e668b7c9e78aeaebabe51dde2b871f6bdf56699f42c7af6caca898e6e9a28e899f3e88f3ec577d1e683f4ccd9d53229dec6355b5a89fc573b7cbf5dbb016de0ed06344d263d6d3fd2efed5de9fcbd94f97eb32b000293e933539b1a758d3adef1391572eb9dff000d3d11e73b4f929fc27ac59d1359a72b22ccdcb755336e663a68de63da8ede8f8af5c7fa15dd534eb7958b4cd77f13799a23a6ba276df6ed98da27decac1bedbb945db545cb55d35d15c4554d54cef1313d130fb621a66bdaa693134e0e65cb544fe09daaa7dd3bc2d5a6fa48bd4cc53a9e1d3729e6fb4b13c9988fe19e699f38068a2b58fc79a05ea37af26e589fcb72d5533ff000c4bbf1f89744c9a3956f54c688ffb4ae289f755b025879d9bd6afdb8b966e517289e8aa8aa263df0f40000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000115c4daac68fa1e46544c45ddb916bbeb9e68f774f92559cfa4ed4797958ba6d157b36e9f5d72227f14f346fdf11bff78111c0da57d69c416ebbb4f2ac62fdb57bf44cfe18f7f3f844b5d55bd1f699f41e1fa722ba76bb9757ac9de369e4f4531e1d33fda5a4000000073e76659c0c2bd97935726d5aa66aaa7e51dee866be9135ff00a56546938d5ef66c4ef7a627ef57d9e5faf8020abab378b78939bf6b915ed11d316a88f9447bfcdaf69f85674ec1b3878d4f26d59a7934f6cf6ccf7ccf3ab3e8ff0041fabf4efac3228db272a9f677fc36fa63dfd3ee5bc000040eb1c23a4eb177d75eb5559bd33bd572c4c533578f34c4f8edba7806759de8d6fc4ef819f6eb899fbb7e99a768f18df7f7423a7d1f6b7154c47d1a623ae2ef34fc1ab00c7af705f105aa6aaa7026aa63ae8b944efe5beee29e1ed66276faab33ff0026afe4db806271c3dadd3ed53a6664785aab77d4699c434c6d4e16a711dd6ae7f26d40319b789c4d66ae55ac7d5edcc75d345d828d7b88b02e6f56766d15765f99abe156ed9806518de9035cb3fb4ab1f23fef2d6dfe1d92b89e92eb88a69cdd3a999dfdaaecdcdb9bba998f9ae991a36999533391a7e2dcaaae9aaab54efefdb74365f016859111eaad5ec698ebb572677fef6e0f6d3f8d743ceda9fa4ce35733f77229e4fc79e9f8a7addca2edba6e5bae9ae8aa378aa99de27cd9c67fa37ccb51cac1ccb57e3a7937299a27c23a627e082b989c41c395d55f232f0e378e5576e67913e331cd20d9c65da7fa43d571e629ccb5672e8eb9db915cf9c737c16bd2f8e747cf98a2edcab0eecf55fe6a667baae8f7ec0b30f9a6aa6ba62aa662aa66378989de261f40000000000000000000000000000000000000000000000000000000000000000000000000000000000000000317d46ed7c43c55726d55fed59116edccc7453bc534ccf944352e29cefabb87737229998afd5f228989da62aabd9898f0df7f250bd1be07d275fab2aa89e462db9aa27ab9557b311eee57b81a7d9b5463d8b766d5314dbb74c514d31d5111b443d000000079dfbd6f1ec5cbd7ab8a2ddba66aaaa9e8888e99043717ebb1a1e935576ea8fa55ede8b11d93d75797ebb33ee0dd0e75dd626bc889ab16c4facbd333f7e7aa9f39e9ee89736b7a964f136bdcbb545557acaa2d63dae8da9df9a3c67a67c5aa70fe916b44d26d61dbdaaae3dabb5fe7ae7a67e51dd100927e8000000000000000000000000021352e13d1751899b98545ab931cd72c7b131dfcdcd33e312a86ade8eb2ecf2ae697914e4d1d56ae6d457e1bf44fc1a500c631353d6f86727d55355ec6989de6c5da6793577ed3e1d31ef5cf47f487859314dbd4ed558b73fde53bd544fce3e3e2b666e0e2ea1626ce658b77edcfe1ae37dbbe3b254bd67d1d5baf957748bfeae7a7d4de9de9f2aba63cf7f105df1f22ce559a6f63dda2edaaba2ba2a8989f387ab15ffef5c2f9bfd7e15d9feed7b7fc3547bd77e1de3dc6cd9a71f558a316fcf345d8fd9d5e3f97f4f005cc7e74c6f0fd0000000000000000000000000000000000000000000000000000000000000000000000000000000000513d2867727170f02998debae6ed7db1111b47bf79f73b7d1be0fd1b40ab2aa88e5e55c9989fdda79a3e3caf7a9dc719b39fc53914d13caa6c6d628888ece98fef4d4d574cc48c0d33171236fb1b54d1331d7311cf3ef07500000033ef48daf6f31a36357cd1b579131efa69f9cf977adbc47ac5bd1348bb9756d373ee5aa67f1573d1fcfc219770ee937b8935ddafd55556e6a9bb9373af6df9fce679be3d40b4fa39d03d55b9d63268f6eb89a71e263a29ebabcfa23bb7ed5f1f16edd16add36edd31451444534d34c6d1111d110fb00000000000000000000000000000000007865e263e763d58f97668bd6aae9a6b8de3ff9ef67bc49c057b1b9793a3f2afdae99c79e7ae9fe1fcd1f1f16920326e1be31ccd12aa71b2a2ac8c389da68aa7dbb7fc3bfe93f069da76a38baa625393857a9bb6aae6e6e9a67b263aa509c4fc1f8bad5356463f271f3b6fbfb7b373baafe7d3e2cf74ed4352e15d5eb8e4d56ee513c9bd62be8ae3bfe53de0da070e91aa636b181465e257bd1573554cf4d1575c4f7bb8000000000000000000000000000000000000000000000000000000000000000000000000000079dfbd463d8b97aeced6edd335d53d9111bcbd10fc5d93f45e17d46e6d13bda9b7cff00bd3c9f98332e1ab75eabc5f8955eab7aee5f9bf5ccc74cc6f5cfbf66cacbbd19d98af882f5caa9dfd5e3d5b4f64cd54c7e9bb5100000154e3ed77eacd33e8762bdb2b2e2639a79e8a3ae7cfa23cfb014de33d6aad735af558d335e3589f576629e7e5cf5d51dbbcf47744342e13d0e9d0f48a2d5511f49bbeddfabbfb3c23f9f6a9de8ef42fa5e64eab9146f671e76b5131cd557dbe5facf734c000000000000000000000000000000000000000015ce2fe19a35dc4f5b629a69ceb51f6757472e3f2cfcbb3deb180c6341d6b2f86f54aaae455c9df919162ae6df69f84c35ed3f3f1f52c3b79789722bb57239a7ae3ba7b2557e38e168d4acd5a8e0dbfe996e3ed28a639eed31ff00aa3e3d1d8aaf046bd5e93ab518f76bfe879554515c4f453574455dddfdde100d6c0000000000000000000000000000000000000000000000000000000000000000000000000015ce3fb94d1c25974cf4dcaadd31e3cb89f92c6a8fa4aaa69e1bb71bfdec9a63fe1aa7e408cf4574c6faa55b46f1eaa227fbed0544f45b6f6c4d42ef555728a7dd13fcd7b000078e5e559c2c4bb93915c516ad5335553dd0c7722ee5f15f126f4c7dae4dce4d14ccef16e88f94473cf9acbe9235ce55ca347c7afd9a76af236eb9e9a69f9fbbb1dde8e743fa2e1d5aadfa76bb911c9b5131f768edf39f8447682d9a760d9d3702ce1e346d6ad53c98dfa67b667be679dd200000000000000000000000000000000000000000000323e3bd1e34bd72ab96a9e4e3e544dda368e68abf147bf9fce1ae2ade90f4f8cce1dab2298dee6257172368de7933cd547c627c8123c2baa7d6fa0636455572aed31eaeef3f3f2e39a667c79a7cd30ce3d1867d546665e0553ecdca22ed3bcf44c4ed3b78c4c7b9a380000000000000000000000000000000000000000000000000000000000000000000000000a77a4eaa2340c7a77e79caa676fecd5fcd71503d29dcf674db51574cdcaa637fe1dbe60eef4631b70f644f5ce555fe0a17153fd194efc3b7fbb2aaff000d0b800e0d6b53b5a3e957f36eed3eae9f669dfef553d11ef77b30f48bad7d3351a74db35ef671677af6e8aae7f947378cc821f43d3eff0012f10d36ef55555eb6b9bb917239a629df799eedf7da3c61b2dbb745ab74dbb74c51451114d34c46d1111d10ad701e8b3a568d17ef53b64e5ed5d513f869fc31f1dfcfb96700000000000000000000000000000000000000000000001e19d8d4e660e462d5334d37edd56e663aa2636f9bdc0633c2191f42e2bc0aaba679eefaa98efaa269fd65b331be28b1734ae2dcaaadccd3545e8bf6eadb6e9f6a36f099dbc9afe35fa32b16d645bfb97688ae9dfb2637807a800000000000000000000000000000000000000000000000000000000000000000000000332f49f5cceb589475463ef1e754ff00269acebd2959a69c9d3afc4cf2aba2ba27c299898ff148243d17dfa6ad23331e3ef517f973e1553111fe195d59d7a2dbf4d395a863cfdeae8a2b8f0a66627fc50d14113c4babd3a268d7b2b78f5b31c8b34cf5d73d1eee9f266fc1da3d5aeebbcbc989b98f667d6df9abf14f544f8cfc225e9c79acfd69ad5562d55be3e26f6e9dba2aabf14fbf9bcbbd7fe12d1a345d12d5aae9db22efda5e9ebe54f5794737bfb41360000000000000000000000000000000000000000000000000033bf4a385b6460e75313ed53559aa7aa369de3f5abdc9fe00ce9cce18b34553335e355559999eee78f84c4793eb8f70be99c2f7eaa69aaaaf1eaa6f5311ddcd3ff0cccab5e8bf33919f99853fd6db8b94ef3d74ced31e7caf8034800000000000000000000000000000000000000000000000000000000000000000000000052fd27d8a6ad1f1323f1dbbfc88f0aa9999ff000c2e8a7fa4d99ffe9db1b75e553bff0076b056fd1b5ea6d712d544f4ddc7ae88e7ebde2af94af5c5babfd4da15ebf455b5fb9f6767f8a7afca379f2647a6675dd3351b19b627ed2cd715446fd31d71e131bc79a678d35ea35dd4edfd1a66716cd1116f78da666769aa76f747903ef80b47facf5ca6fdca77b187b5cabbeafc31ef8dfc9ad21b857478d1744b362a8dafd7f697a7f7a7abcb9a3c9320000000000000000000000000000000000000000000000000000f3c8b36f271eed8bb1cab77689a2a8ed898da58ee9176ae1fe2db5ebe623e8f7e6d5d9eadb9e999f74eed9995fa46c0fa2f10464d34cc51956e2adf6e6e54734c4797267cc1aa087e14d47eb4e1ec4c8aaadee5347abb9cfbcf2a9e6999f1e9f34c0000000000000000000000000000000000000000000000000000000000000000000000085e30c28cfe19cdb7cdcab747ada6663a269e7e6f18898f34d3e6ba29b96eaa2b88aa9aa262627ae0180a7f8274d9d4b8931e26266d63cfafae7f87a3fe2dbe290cff0047baa5bcdaa8c29b57b1a67d8aeaaf9334c7ef476f82efc31c3f6740c0f554cc5cc8b9b4debbb74cf544774026800000000000000000000000000000000000000000000000000000156f485a67d3b409c8b74ef770eaf591b473f23a2a8fd27fb2b4be6e514ddb755bb94c55455134d54cf44c4f5033af467aa7aaccc8d32e55ecde8f5b6a27f3474c79c7f85a3b16cdb37f86789aaa6d4fb78b7a2ab7333f7a9e98dfc6279fcdb16165dacec3b39562ae55abd44574cf8f54f783dc00000000000000000000011daceb585a2627afccb9b6ff0072dd3cf5573d9100e8d433f1b4dc4af2732ed36ad53d333d73d911d72cc388b8db3b55aaab3873562626f31b533b575c7ef4fca3e28bd7f5ecbd7b37d7e44f26dd3bc5ab313ecdb8f9cf6cff009427785f822fe7d5465ea94d56313a62d4f35773f947c7b3b4135e8d2de7469f957b22aae716e554fa9e5cccef31bf2a63bba23cbb9757c5bb7459b74dbb5453451444534d34c6d1111d1110fb000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050fd2669335d9c7d56d53bfabfb2bdb764cfb33efde3ce1cbe8f38862cd7f53e5d7b5172adf1ea99e8aa7a69f3e98efdfb57fccc5b39d897717229e55abb4cd354774fcd896a9817f48d52f625d998b966ae6aa39b78e98aa3f506e82b3c1dc4f6f5ac4a71f26b8a73ed53b5513cdeb223f147cd66000000000000001f372ba2d5155772aa68a298de6aaa7688807d3f2aaa29a66aaa6222237999ea55b57e3cd2b03956f166736f4736d6e76a227beafe5ba89aaf116afc43762c5755534573b538d6299da67c3a6ae8ebdc171e21e3ec7c4e563e9314e4dee89bd3fb3a7c3f37e9e2a56361eb1c53a8d55d3eb326eccfb776b9da9a23be7a223ba3ca161e1ee00bd7e69c8d6666cdae698c7a67dbabf8a7abf5f0685898b630b1e8c7c5b545ab5446d4d1446d00aff000e70660e8fc9bf7f6cacc8da797547b344feec7ce79fc16600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000103c4fc318fc41629aa6af53976e36b7776df9bf2d5db1fa7bf79e0187ea5a66a1a0e7c5bc8a6ab37699e55bb944f355b75d32b870efa4088a69c7d6e2778e68c9a29dffbd11fac7bbad75d4b4dc4d5716ac6cdb34ddb73cf1bf4d33db13d52cff57f4779962aaabd2ef53936faaddc98a6b8eedfa27c79bc01a2e2e563e6598bd8b7eddeb73d155baa2a87b30eae8d5343c9f6a32b06f7444c6f44d51dd3d7099c4e3fd731e998b95d8c9ec9bb6f698feecc035819953e923538fbf87893e11547cdf55fa49d4663d8c2c589ed9e54fcc1a58ca327d206b97a222dce3e3f7dbb5bff008a651793c4dade555355dd4f26378db6b75f223dd4ed00d93272f1b0e88af2b22d58a26768aaed714c4fbd059bc73a1626f14e457935c4edc9b344cfc6768f8b30c4d2f52d4ebe562e2646472aada6b8a26637efaba23cd67d2fd1d675fdabd46fd18b4fe4a3dbaff947be41f7a8fa47ccbb13469f896f1e2778e5dc9e5d5e311cd11f157aabdae712dfe4cd5959b56f1ecc44f22899ebda3d9a7e0d274fe0ad0f076abe8b3935c7e2c8ab97f0e8f827addba2cdba6ddaa29a28a636a69a636888ee80679a3fa3abd73937357c8f554ffb9b3313579d5d11e5baf1a668fa7e936f91838b6ed6f1b4d511bd5578d53cf2ee00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001e77ec59c9b536afdaa2edbaba68ae98aa27ca50d7b83b87efdc9b95e9d444cf5515d5447ba26213a02b77781340afeee2dcb7fc37aaf9ccb9e3d1e68b156fcaca98ecf591b7e8b60080b3c19a0599a6634fa6baa23a6bb9555bf8c4cec94c7d330312be5e360e359abf35bb54d33f087580000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fffd9, 'Delivered', 'ORD1000000003');
INSERT INTO `delivered_product` (`delivered_product_id`, `order_receiver_signature`, `order_receiver_signature_status`, `order_details_id`) VALUES
(3, 0xffd8ffe000104a46494600010100000100010000ffdb004300100b0c0e0c0a100e0d0e1211101318281a181616183123251d283a333d3c3933383740485c4e404457453738506d51575f626768673e4d71797064785c656763ffdb0043011112121815182f1a1a2f634238426363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363ffc0001108025802d003012200021101031101ffc4001b00010003010101010000000000000000000005060704030201ffc4004210010001030202050a020804060300000000010203040511061207213171a1131422414251618191b124a215233252638292c1364372b23334355373d162b3e1ffc40014010100000000000000000000000000000000ffc40014110100000000000000000000000000000000ffda000c03010002110311003f00d0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000101ac718693a3e479bdeaee5ebd1fb5458a62a9a3be66623e4f7e28d6a8d0f47bb91bc797afd0b14cfaea9f5f7476fcbe2c66baeab95d55d754d55553bcd533bcccfbc1a15ee932cd37662c6975d76fd55577a2999f9444fdd37a0f17e9daddd8c7a39ec64cc6f16ee7b5efe598edf0640edd169bd5eb3854e36fe57cbd1cbb7aa7706e6000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fcaaa8a699aaa988a6237999ec87ea9bd226b9e65a7d3a658ab6bf951bdc98f66dfff00b3d5dd120a6f176bb3ae6af5574553e6b677a2c475f67aeadbdf3f6dbdcf6e0ad0bf4cead15dea37c4c6dabbbbf6553eaa7e7ebf844a071ec5cc9c8b762cd135ddb954534531eb99ec6d3c3fa45bd1349b5876f6aaa8f4ae571ed573db3fda3e1100e5cfe10d0f3baebc1a2cd7b6d1558fd5edf28eafac3d747e19d2f45b937712c4cdedb6f2b7279aa88f87bbe49800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001f172e516ad5772e551451444d555533b444476cb10d6f52af57d5b2336bde3cad7e8d33ecd31d54c7d36697d206a5383c3b5daa2adaee5d516a369da629edabe5b46dfcccd345d36e6afaae3e0da9da6ed5e955fbb4c75ccfd0172e8df42eaab59c8a7df463c4fd2aabfb7d5a03cb1ec5ac5c7b78f629e4b56a98a28a7dd111b43d4000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000007e4cc5313354c44475cccfa8198f4979de5f5ab3874d5bd38d6b798dbb2aabae7c2294bf469a4f91c3bdaa5da7d3bffabb5fe889eb9f9cc7e553b226ef12f14d7e477e6cbbfb51331fb347aa67ba98f06c98b8f6f1316d63d9a796d5aa228a63dd111b03d400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000101c6f9fe61c339534ced72fc790a3abb79bb7f2f327d42e94af574d8d3ac455e85755caeaa7e3114c44fe69047f4678117f56c8cdae22631adf2d3bfaaaabd7f48aa3e6d3555e8e717c870cd37a7699c8bb5d7f1da3d1dbf2cfd56a000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000019b74a333fa4b0a3d5e467eed259bf4a31f8fc09fe155f705a781a36e11c0eeaff00df527d03c11fe12c0ff4d5fefa93c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000338e9467f1d811fc2abef0d1d9bf4a3ff0050c1ff00c557dc169e07ebe11c0eeaff00df527d5fe05ff08e07757ffd952c0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000033de94e888b9a657ed4c5d89ee8e5ff00dcb4252ba4fc58b9a462e56d33559bdc9dd15475f8d30093e02ae9ab8470e22626699b913f09e7aa7fbc2c4a57461934d7a465e375f3dabfcf3dd553111e34caea000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000022b89f0a750e1dcec7a6266b9b735d1111bccd54fa511f398d92a032be8e33bcdb882ac6aa67932ad4d311eae68f4a27e9157d5aa319d46d55c3bc5b73c9d3b462e445cb74efdb46f15447d2766c76aed17acd176dd515515d3155331eb89ec07d8000000000000000000000000000000000000000000000000000000000000000000000000e7cacdc4c3889cbcab36227b26edc8a77faa2b238c740c6aeaa2ad428aeaa7fedd155713dd311b78827453323a47d328a67c862e55dae3b39a29a627e7bccf822f27a4bcaaa23cd74eb36e7d7376e4d7f6d81a38c93278ef5ebf54cd1916ec44fb36ad46df9b7972cdce25d5edcf5ea7936aaede58ae689fa750359ccd574fc0998cbcdc7b15446fcb5dc88ab6eeed7de0ea189a8d99bb85916efd113b4cd13bed3f1f7328b5c13c4177967cc3922af5d57688dbe5beebcf04f0e65683632aaccb96e6e644d3e85133314c53bfafdfd7e00b3800000000000000000000000000000000000000000000000ce7a4fd3f932f1350a29eab94cdaae623ab78eb8dfe33133fd2b17016a1e7dc35668aeadee62ccd9ab79f5475d3f2da623e4f7e33d3bf4970de5514c44dcb31e5a8dfdf4f5cfcf6de3e6a57471a9c626b55e1dcaa22de653b46ffbf4f5c7873477cc03520000000000000000000000000000000000000000000000000000000001f933111bcf542273389f44c2eabfa958df7db6b73e5263be29df604b8a4e67491816fab0f0afdf9dfafca5516e3e5dbf641667487ac5f89a71e8c7c68dfaa69a39aa88fe6de3c01a9b9f2b3b0f0f6f3bcbb1637ecf2b7229dfeac82ad5788759b95d14e4e764cd51e95bb3cdb6dfe9a7abc1d189c15af65f2d5387e468abdabd5c53b77c76f802fb95c6fa0e3455b65cdfaa3d9b56e677f9ced1e287cbe92b1a99db0f4fbb7236edbb5c51e11bfddc989d1a5faa9df3351b76e77fd9b56e6bde3be76fb26f13a3fd12c4ef7a32327abae2e5cda3f2ec0aa65f483ad5f8dacc63e375f6d16f79fcd331e08e9d4788f599af92fea19313fb54d9e6e5fe9a7a9aae2683a4e1c53e6fa7635134f65536e26afacf5a4418f63f066bf9314d7e63345357aee5ca6998ef899dfc12f8dd1b67d55ed959d8d6a9f7da8aab9fa4f2b4a0149c6e8db028a67ceb3b26ed5ea9b714d11f49dd2b8fc13a0588a7f05e56aa7dab972a9dfbe37dbc16101cb8da6e0e1d5cd8b858f62af7dbb54d33e10ea0000000000000000000000000000000000000000000000000000018df12e997787b886a8b1336e8e68bd8d5c7aa37de36ee9eaf936441f16e834ebba54dba22232acef5d8aa7dfeba67e13ffaf703a787b57b7ade936b2e8da2b9f46ed11ecd71db1ddeb8f84c24d8ef0c6b97b86f57aa2fd15458aeaf27916a6369a769eddbdf1d7e30d7ed5ca2f5aa2edaae2bb75c4554d54cef1313d9300fb00000000000000000000000000000000000000000007cdcb945aa2aaee574d14531bcd554ed1100fa15fd438d343c0de3cefce6b8dbd0c78e7dfe7fb3e2ab6a3d23e5ddde8d3b12dd8a7ae39eecf3d5f0988ea88f106928acee24d1f4fde32750b31544ed34513cf544fc629de63e6caeee76bdc4372ab73772f337da6ab76e26698f8f2d3d51dfb25703a3ed632769c99b3894efd715d5cd56def88a7abc6013d9bd2460dbeac3c2bf7e77edb9545b8f976cfd95ecde3fd6b23aac5567169fe1dbde67be6adfc3659b03a3ad32c6d5665fbf95544f5c44f2533f28ebf15870743d2f4fe59c4c0b16eaa7b2be489ae3f9a7afc4194538bc43afcd3572676653333cb557357244fc267aa12b85d1e6af7f96ac9b98f8b4ccf5c4d5cf5447747578b52014bc3e8e34fb511397979191544efb51116e998f74c75cf8a771386344c389f23a6d89de77dee53e5263ba6adf64b80fca6229a629a62222236888f53f40000000000000000000000000000000000000000000000000000000000000000000000147e3ee18f39b756ad8347eba88fd7d111fb74c7b51f18f5fc3bbae3380f89e30ae53a5e757b63dc9fd4d733d56ea9f54fc27c27bda532fe3ae18fd1b913a8e15bfc1ddabd3a698eab554ff0069f0ecf7035114ae05e29f3db74e979f5fe2688dacdcaa7fe2531ea9f8c78c78dd400000000000000000000000000001e1939b8b87113959366c44f64ddb914fdd1393c61a0e3555535ea3457547aad5355713f388dbc413a29795d2469d4513e6b87937abdfdbe5a299f9ef33e088cae92750aea9f34c2c7b346dfe64d55cc7ce36fb034b795fbf671ad4ddc8bb6ed5b8edaae5514c47ce59064715ebf9d316e750bd13bf553622289fcb113259e19e21d4aed572ac1c99ae67d2af2279267faa62641a366f19685873544e7537ab8f66cd335efdd31d5e282cce92ac5333185a7dcb9d5d555eae29da7ba37dfeae0c3e8db36e4ef999d62cc6dbed6e99b93dd3bedfdd3f87d1f68b8f3155ff002f953b75c5caf969dff9769f10547378f35ccaeab576d6353d9b59b7d73f3ab7f0715bd2f8875dae9b9363332b78f46e5e99e5dbe1555d4d670b48d3b0369c4c2b166a8ecaa9a239bebdaed066981d1c67dddaacecbb38f131bf2d11372a8f84f6478cacfa7703e89833155762acaae2778aafd5bc7f4c6d131df12b200f8b56add8b54dab36e8b76e98da9a28a62223ba21f6000000000000000000000000000000000000000000000000000000000000000000000000000000000003cefd8b59362bb17e88b96ae5334d54cf64c4bd0063fc51c3b91c3b9f176ccd73895d5bd9bd1db4cf6f2ccfaa63c7b7dfb5b38578dece653461ead5d36726236a6f4f55173bfdd3e13e0b76562d8ccc6af1f26d5376cdc8daaa2a8ea9671c43c05958955791a4f364d8edf23fe651ddfbd1e3f09ed069a320d178b755d0ea8b154cdeb144ed362f6fe8fc227b69eeecf82f9a471ae91a9c5345cbbe697e7d8bd3b46ff0abb3ed3f0058c7e44c4c6f1d70fd007cd5553453355554534c75cccced10e2b9ade956a662e6a7874cc76c4dfa77fb83bc415de31e1fb5572d5a8d133ffc68aea8f08705ee90b44b556d44655e8f7d16e223c66016c144c8e92f1e9abf0fa6ddb94fbee5d8a27e9112e0c8e9273eaaf7c6c1c6b74fbae4d55cfd62601a50c8eff1debf76e4d5464dbb113ecdbb34cc47f544cb8e8d4388b549ae2ce4ea593bfed536aaae63e91d40d8b23271f16df3e4dfb5668fdeb95c531f5945e4f166858b572dcd4ecd53fc2dee47d698966b8fc21afe553cf4e9f729899ff36aa689fa5531297c6e8df52aeaa7ce32f16d513dbcb3557547cb688f104ee4748ba4dbe68b3632af551d93cb14d33f399dfc11393d25e4554ed8ba6dab73efbb726bf088877e2f46b8546fe759f91767d5e4e98a3efba5b1b82740c78a77c29bb547b572e553bf7c6fb78028b93c79af5faa668bf6b1e27d9b56a36fcdbcb9bcbf13ead4d534d7a9e45babb7939f93c3a9ad6369b838757362e163d8abdf6ed534cf843a819063f04ebf9134cce1c5aa6af6aedca636ef8df7f04b63746b9d555f8acfc7b54fbed535573e3cad24052b1ba36d3e8a3f139b9376af7db8a688fa4c4fdd318bc1fa0e2d54d7469f45754476ddaaaae27be2676f04e80f1c6c5c7c4b7e4f1ac5ab1476f2dba2298fa43d80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000117abf0fe9bacd3f8cc6a66e6db45da7d1ae3e7ebee9ea52356e8eb32c4cd7a5dfa7268f55bb9314571f3ec9f069603169ab5fd0bd0df3f0a8dfb37aa9a267ed27e9ed7f32b8a28d4336bae7b29b55d5133fd2da406356f86f88b52b9557560e5575faeac89e499f9d731ba42c747baddda77ae716ccfbabb9333f9625aa80ce31fa35cbabfe6751b36ff00f1d135fdf676d8e8d31a9abf11a95db94fbaddb8a27c6657a0153b3d1e68b6eadebab2af47eed772223c221df6783f40b15c57469b44cc7efd75571f499984e80e4c7d2f4fc5b91731b0316cd71ed5bb34d33f58875800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003ffd9, 'Delivered', 'ORD1000000006'),
(4, 0xffd8ffe000104a46494600010100000100010000ffdb004300100b0c0e0c0a100e0d0e1211101318281a181616183123251d283a333d3c3933383740485c4e404457453738506d51575f626768673e4d71797064785c656763ffdb0043011112121815182f1a1a2f634238426363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363ffc0001108025802d003012200021101031101ffc4001b00010003010101010000000000000000000005060704030201ffc4004310010001030202060608050205040300000001020304051106311221415161710713228191b11423325272a1c1d1153342e1f024624363a2b2c2265382d27492f1ffc40014010100000000000000000000000000000000ffc40014110100000000000000000000000000000000ffda000c03010002110311003f00d00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001c1a9eb3a76934d339f97459e972a6779aa7dd1d6f9d7758b1a1e9b5e5dff006a63d9b76e2769aeaec8ff003b18c67e6dfd4732ee5e55735ddbb56f333d9e11e100da34cd6f4dd5fa5f40cba2f4d3d734ed34d51ee9da520c4b86afdfc7e22c0af1e67a737e8a2623b6267698f84b6d00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000153e35e298d26c4e0e157139d729ebaa3fe0d33dbe73d9f1eedc2a3c7daacea1afd7628ab7b187f554c7fbbfaa7e3d5ffc55fc3c4bf9d956f1b16dcdcbd76ae8d34c76ff009def7d2f4ccbd633e8c5c4a3a776beb9aa79531db5553dcd5b86b8631387eccd54cfaecbae36aef4c6dd5dd11d91f3f86c1c9c33c198da25ca72afdcfa466447555b6d4dbde3af68ededeb9f842d000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000022f8835bc7d0b4eab26ff00b5727d9b56a27aebabf6ef9fec0e4e2ce24b5a0e1f46898af36ec7d55beeff0074f87cfe3b64f6e8cbd57508a688af232b22bf39aa65f5999797abea355fbd355ec8bf56d114c6fbcf28a623f28869dc1bc2f4e898df49ca8a6acfbb4fb5db16a3eec78f7cff00921dbc31c3f6740d3e2dc6d5e4dcda6f5c8ed9ee8f084d0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fcaaa8a699aaa988a62379999ea8807867e6d8d3b0eee5e557145ab51bd53fa478b1bd7f59c9d7b53ab26ec4c53f66d5a8eb8a29ec8f19ef94971a7124eb79bf47c6aa7e8362af63fe655f7bf6feeb1f04f08fd1228d4f53b7fea27dab366a8fe5c7de9ff007787679f20f7e0ae138d328a750d42dc4e6551ec513ff0a3ff00b7c9700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000019cf1df157afaabd274fb9f554ced917699fb73f763c3bfbfe721c73c57f43a2bd2f4faffd4551b5ebb4cff2e3ba3c67f2f3e505c11c2d3aadf8cfcda3fd15aabd9a663f9d54767e18edf877ec125c09c291545bd5f50b7bc7dac7b5547fd73fa7c7b9a0bf23aa3687e80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ac719f13c6898bf47c5989cebd4fb3dbeae9fbd3e3ddfe6f21c4baed9d074d9bf5ed55faf7a6cdb9feaabf68edfeecbb4cc0cfe2ad6ea8aae4d572e4f4ef5eaa378a29eff00d223e40e8e17e1ec8e23d42ab97aaae3168aba57ef4cef354f3e8c4f6ccfe5cfbb7d72c59b58d628b3628a6ddab74c534d34c75443c74dd3f1f4bc1b789894746d5b8ede733db33e32ea00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001cda86758d370aee5e557d0b56a379ef9f08f197bd5553453355754534d31bcccced110c9b8c388ebd7b3e31f166a9c3b556d6a988fe655cba5b7cbfb838b50cccee2bd7a268a26ab976ae859b513d5453ddfaccf9b53e1ed0f1f41d3a9c7b5b5576af6aedddbaebabf6ee8fee8ee0be1aa745c2fa464d3139d7e9f6ffe5d3f77f7fecb3000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000aaf1b713468f8bf43c4abfd75ea79c4ff2a9fbde7ddf1f3086e3fe279aaaaf47c1b9ecc7564d74f6cfdcfdfe1defbe00e18da28d633adf8e35157fdf31f2f8f721b82f86e75bcd9c9caa67e8566af6ff00e655f77f7feed622229888a622223aa223b01fa0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e7ceccb18187772f26b8a2cdaa7a554cfcbcfb01c3c49ae59d074dab22bdaabd57b366dccfdbabf68edfef0cbb4cc0cee2ad76a8aee4d55dcabd65fbd572a29eff00d223cbb1f9aaea19bc53ae44d16e6aaee55eaec59a7fa69ec8fd667f46a5c37a1d9d074da71e8daabd5fb57aec7f5d5fb4767f79077606158d3b0ad6262d1145ab54ed4c7eb3e32e80000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000019471c7127f17cdfa262d5fe8b1eaea989fe655f7bcbbbe3dab17a40e23fa1e3ce95895c7afbd4fd75513f6289ecf39f979a1f80386fe9b911aa66513f46b357d4d33cae571dbe51f3f2904ff0002f0d7f0bc58cfccb7319b7a9f669ab9daa3bbce7b7e1deb700000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000089e24d6ed685a5d7935ed55eabd9b36e7faaafda39ca4afdeb78d62e5fbd5c516add3355554f288863daeea993c51ae47a9a2baa99abd5e359ee8dfe73ce7fb01a1e9795c51ae55176baa62aabd664deee8dfe73ca3fb360c6c7b5898d6f1f1e88b76add314d34c764423b86f44b5a169746351b557aaf6af5c8feaabf68e50960000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000015ae35e228d174ff538f5c7d37222628efa29edabf6f1f20573d21711fd22f4e91895fd55b9defd513f6aafbbe51dbe3e493f47fc3bf43c68d572e8ff00517a9fa9a663ec513dbe73f2f356f82787a75ad47e95954cce1d8ab7af7ff895738a7f59feed601fa0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000fcaaa8a699aaa988888de667b01c7ab6a56348d3aee664ced45b8eaa639d53d911e32c8e9a73f8b3887afaef6455bccff4daa3f688ff0037976718f1055aeea516b1a6a9c3b33d1b54c47db9edab6f1ecf0f395eb837876344d3bd65fa63e9b7e226e4fdc8eca7f7f1f2804c699a7d8d2f02ce1e353b5bb54edbf6d53db33e32eb0000000000000000000000000000000000000000007e4f546f2a7e7fa43d3b1b2aab38d8f772a8a6769b94cc534cf977fe40b88e1d2355c5d67069cbc3ae66899da62a8da699ee9f17700000000000000000000000000000000000000000000000000000000000000000000000000000000000a27a43e22f536e747c4ae62e571be45513ca9eca7dfdbe1e6b17146bb6f41d2eabfecd59173d9b144f6d5dfe51ce7e1daccf87747c8e25d6a62ed55cdbe97acc9bddbb4cf7f7cff7ec04ff00a3ce1df5f76358cba226ddb9db1e9aa3ed551ceaf77678f934679d9b56ec59a2cd9a228b76e98a69a6394447287a00000000000000000000000000000000000000000fc998a6266662223ae664998889999da23b59af1bf16c674d5a6e9b737c68eabd7699fe6cf747fb7c7b7cb9874f1671c5172ddfd3b4a88ae8ae99b77322794c4f54c53fbfc3b25414e70ff0b67ebb5c556e9f538bbfb57eb8eaff00e31fd53fe6ebc5af47ba2d1344d739373a3f6a26e44455e7b47c81e1e8cf12ed9d1f2326e44c519177eae3be29ea99f8ef1ee5cde766cdbc7b3459b345345ba2229a69a63688887a00000000000000000000000000000000000000000000000000000000000000000000000000000000f3bf7ade3d8b97af57145bb74cd55553ca2239cbd19efa46d7fa55468d8d5f546d5644c77f38a7f59f77882b7af6a993c4dae74ad535cd355516b1ad76c46fd5ef9e73fd9a870de896b42d2e8c6a7a355eabdabd723faaafda3942b5e8ef87fd55bfe319544c575c4d38f4cc72a794d5efe51e1bf7af6000000000000000000000000000000000000000039751d47134bc5ab2736f536adc756f3ce67ba23b650dc49c5f85a253559b731939bb755aa67aa8fc53d9e5cfcb9b39b97357e2bd5395cc9bd3ca9a7aa8b71f2a601dfc4bc6197add5563634558f8533b7ab89f6ae7e2fdbe696e15e05aae7433759a269a3ed518d3ce7f1f7797c7b939c31c1b8da3747272a69c9cdecab6f66dfe1dfb7c7e4b403e68a29b74534514c534531b534d31b4447743e80000000000000000000000000000000000000000000000000000000000000000000000000000000007e55314d33555311111bcccf6022789b5ab7a16937326769bd57b16689feaabf68e6ccf86b47bdc47adcfaf9aeab5157adc9bb3ce779e5bf7ccfeb3d8fae29d66e7116b9118f1557668abd563511bfb5bcf3dbbe67f4693c31a251a16936f1f689bf5fb77ab8edabbbca397ff00d04b5ba28b76e9b76e98a68a62229a698da2223b21f40000000000000000000000000000000000002bfc43c5b81a24556b7fa465f659a2797e29ecf9f8026b2b2ac6163d79195768b56a88deaaea9da219d71271e5fcb9af1b48e958b1ca6fcf5575f97dd8fcfc9059da8eadc53a8d1455d3bf72a9dadd8b51b534f947eb3ef95d386b812c61c5195ab4539191ce2cf3a28f3fbd3f979f3056b86f83b335a9a7272a6ac7c399dfa731eddcfc313f39fcda6e99a5e1e938b18f85669b5447398e754f7ccf6cbadfa0000000000000000000000000000000000000000000000000000000000000000000000000000000000000297e9135efa261c6978f5ed7b229deecc4fd9b7ddeff0096fdeb46aba8d8d2b4ebd9b913ec5aa77dbb6a9ec88f199649858d97c59c493172a9e9dfafa776b88eab74473f846d11ee058fd1ce83eb2e4eb19347b144cd38f131ce794d5eee5e7bf7344796363dac4c6b78f6288a2d5aa629a698ec887a800000000000000000000000000000000f1cacab1878f55fcabd459b54f3aeb9da01ece4d4752c3d2f1fd7e76451668ece94f5d5e51ce7dca4eb9e9127dab3a35ada397d22ec7fdb4feb3f0563134fd678a336abb4c5dc9ae676aefdc9f629f0df9473e51ee804d71071ee566f4ac6951562d89ea9b93fccabff00afbbafc5c7c3fc19a86b134e464f4b1716aebf595c7b75fe18fd67f35c787f823074be8dfcbdb2f2a3ae26a8f6289f08edf39fc969047e91a2e068d8feab06c451bc474eb9ebaebf39ff0021200000000000000000000000000000000000000000000000000000000000000000000000000000000000000002b9c6baf7f05d2a68b15ed9993bd16f6e74c76d5eeecf19053f8ff5efe23a8fd031ebdf1b16af6a639577394cfbb97c56ee07d07f83e9517afd1b65e4c4555ef1d7453d94feb3e3e4a7701683fc5353fa66451be2e2cc4f5c75575f647bb9cfbbbdab0000000000000000000000000000000039f37331b031aac8cbbd459b54f3aaa9fcbc67c19d711f1e64664d58fa4f4b1ac729bdcae57e5f763f3f2e40b5f11717e0e891559a67e9399ffb544f553f8a7b3cb9b3ac9ccd678af51a68dabc8b93f62cdb8da8b71e5ca23c67e2efe1ce0ccdd6269c8cae96361cf5f4aa8f6ee47fb63f59fcda5e97a5e1e938d18f83629b5476cf39aa7be67b4156d0bd1f6363c537b57afe91779fa9a26628a7ce79d5d9dd1e6b9dab56ec5aa6d59b74dbb74c6d4d1446d111e110fb00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001e593916b171ee645fae28b56a99aabaa7b2218e6a39797c55c43d2b744cd77eb8b766dfdca7b23e7333e72b37a47d77a55d3a363d5d51b579131db3ce9a7f5f83abd1ce83ea31e757c8a3eb2f44d362263ecd1db57bfe5e60b568da659d1f4cb38563ae2dc7b556db4d7576ccbb800000000000000000000000000079debd6b1ed5576f5ca6ddba237aabae76888f1907a2b9c47c5f85a245566ded939bffb54cf551f8a7b3cb9fcd5ae26e3cb991d2c5d1aaaad5aeb8ab236dababf0f7478f3f24570e708e6eb95537eeef8f8733d77aa8ebaff000c76f9f2f3e40e3c8cad638af52a699f59937677e85aa236a2dc7847288e5d73ef95eb87381b174ee8e4ea3d0caca8eb8a36dedd1ee9e73e33f0ed58749d230b47c58b18366288eae9553d75573df33daee000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011daf6ab6f46d26fe6dcda6aa636b74cff5573ca3f7f08948b2df487acfd3b558c0b35ef631378ab69eaaae76fc397c4115a069b7b89388228bd555545754dec8b9dbd1dfafdf333b7bdb2dbb745ab74dbb74c51451114d34d31b4444728857781b45fe13a2d372ed134e4e56d72e6fce98fe9a7e1d7e732b2000000000000000000000000000aa71471a63693d2c5c2e8e466f29ebde8b7e7df3e1f104ceb7ae616878bebb32e7b557d8b54f5d55cf847eacb75be20d4b8972e9b5d1aa2d4d5f558b6b79ebecdfef4ff91b3e3074fd5b8af52aeb89aaf5c99dee5fb93b53447f9ca21a6f0f70ce16836b7b51eb726a8dabbf5475cf8477402bfc33c076ec7432f5aa69b977aa69c6df7a69fc5dfe5cbcd798888888888888e510fd0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000119c45aad3a368d9199331eb223a36a27b6b9e5fbf944b32e0fd26ad6f5fa26fc4d766d4faebd3575f4bafaa27bf79fcb7497a48d5be95aa5bd3ad55f558b1bd7b729ae7f68dbe32b5702e91fc3341b772e53b64656d76bef88fe98f875f9cc82c8000000000000000000000000f3c8bf6b1ac577b22e536ad511bd55d53b4443835bd770b43c5f5d9773daabec5aa7aebae7c23bbc5976b3aeea7c4f9b4dae8d5d09abea716d6f3113facf8f9f204cf13f1d5dcce9e26913559c7e555fe55d71e1f763f3f27370c705646abd0cacfe963e1cf5c472aeec7877478fc3bd3fc2fc0d6b0ba197ab534dec8e74d9e745bf3fbd3f979f35d41e185878f818d4636259a6d5aa2368a698ff00379f17b800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e3d5b50b7a5e97919b77ae9b344cc47de9ec8f7ced0ec67de93356de71f4ab5572fadbd11ff4c7ce7e00ae70e69f7388789688c8f6e9aab9bf9154f6c6fbcfc66623ded9152f477a4fd0b459ccb94ed77327a51bf6511f67e3d73ef85b4000000000000000000007cd75534515575d514d34c6f3333b44403e955e29e32c7d1e2ac5c3e8e4677298df7a6d7e2f1f0f8f8c2f1571d55726bc2d16e4d3472af263aa67c28eef3f877a1f8638472b5caa9c8bf3558c1dfaee4c7b573be29fdf979f20716160eabc55aa57544d57eed53bdcbd727d9a23c7bbc223dcd3f87b86b0b41b3f531eb726a8dabbf5475cf84774787cd2381818ba6e2d38d87669b56a9ec8ed9ef99ed9f17480000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003e2f5da2c59aeeddaa28b76e99aaaaa7944475ccb1ab717b8a38a622ade2accbdbcf6cd147f6a63f2689c7b9d385c317e9a6662bc8aa9b3131e3d73f944c7bd5bf461831733b2f3aa8fe4d116e8de3b6aeb9989ef888fcc1a2dab7459b545ab74c534514c534d31ca223943ec00000000000000000045ebbaf616858beb72abdee551f576699f6ab9fd23c41d99d9b8da762d79397769b5668e7555f28ef9f0659c51c5d93ae5538d8f1558c189eab7fd573ba6afdbe6e3d4752d538af54a28e8d572aaa76b38f6fecd11fe7399f92fbc2bc1b63478a72b3629bf9dce3b69b5e5e3e3f0f1086e15e059ae68cdd6a898a7ed518d3ce7c6bfdbe3dcd069a69a288a28a629a698da222368887d00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a17a53bd5459d3ac455ecd555caeaa7c63a3113f9ca4fd1ce3d5638622e4cf55fbd5dc8f2eaa7ff00156bd275de96bd8f6fa5bc518d13b774cd557ed0bbf09637d1785f4eb7befbd98b9ffeded7ea0980000000000000001f93311133331111ce6541e2be39dbd660e8b5f5fd9af2a3f38a3f7f87782638a38c31b45a6ac6c6e8dfcedbecefecdbfc5e3e1f2503034fd578b354aebe95576b99deedfb9f6688ff0039443af86784f2b5ebbf49c99aece1f4b7aaecfdab93dbd1dfe7f36a783858da762d18d896a9b5668e54d3f39ef9f1070e81c3f85a0e37abc7a7a77aa8facbd547b55fed1e0960000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001f9ca379063fc6b77e97c5d9916e66adaaa6dd31e314c44c7c776b98d629c6c6b562dfd8b544514f9446cc83479ab56e34c7bbd1ebbd99ebeaa7c3a5d39fca25b200000000000000f2c9c9b3898f5e464dca6d5ab71bd55d53b4439f55d531348c3ab2b36ec5144728feaae7ba23b6594f107116771265d16e29aa9b1156d671a8ebde67aa267beaff201dbc57c637b57aaac4c29aace0c754f65577cfba3c3e3e1dfc25c115647433b58a269b3cede3cf54d7e357747876fce4f84b82a8c09a33b54a69b9951d76ecf3a6d4f7cf7d5f942e80f9a28a6dd14d145314d34c6d14c46d111dcfa0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011bc4793189c3da85e9aa6998b1545331d954c6d1f9cc2495af4837e9b3c299144f3bd5d14479f4a2aff00c6414ef4718d17f89a2e4ccc7a8b35dc8f199da9ff00ca5ab33cf4596a99bba95e9a7dba69b74c4f84f4a67e50d0c000000000042f1171261e81637bd3eb726a8dedd8a67aeaf19ee8f145714f1ad9d33a789a6cd37f339555f3a2d4feb3e1f1ee52748d1753e29d42bbb35d5553357d764ddde623f79dbb3cb9403cf232355e2bd5a2262abf7eafb16e9eaa6dd3e1dd1e32d1b85f84f1b42b717aef46fe74c7b5776eaa3c29fdf9cfe491d1344c2d0f17d4e1dbf6aafb772aebaab9f19fd124000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000029fe9367ff4ed8fff002a9ffb2b5c14cf49f5eda262dbfbd93157c29abf707cfa30b314e8f977fb6bc8e84fba989ffc975543d19c6dc3b7bc72aaff00b695bc0000044ebbc4381a159e9655ce95e98de8b3475d757ed1e32092bf7ed6359aef5fb94dbb5446f55754ed110cdf8a38e6ee6f4f0f499aace372aaf72aee4787dd8fcfcb921b5ad7b52e25cca6dcd357ab9abea716d6f31bf67e29f1f92ddc2dc0d4624d399ac534ddbfce8b1ce9a3f177cfe5e7d8109c2bc177b54e8666a11559c39eba69e555d8fd23c7e1ded33171ac61e3d18f8d6a9b566dc6d4d14c6d10f600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000523d285357f0cc2aa227a317a6267b37e8f57ca577567d2061ce5f0c5dae9de6ac7ae9bd1111cfb27f2aa67dc0e6f4673bf0e5e8eecaaa3fe9a56f677e8bb336c8cec19999e9534dea63b2369da7e74fc16fd5f88b4cd1a998ccc9a7d6c46f1668f6ab9f7767bf6804abc32f2f1f0accdecbbf6ecdb8feab95444339d5bd2267644d54699668c5b7d95d7115d73fa4796d3e6a9e464e567e47acc8bd7722f55d5135d5354cf8402f7c43e906988ab1f448de7b726ba7aa3f0d33f39f82b3a4681aaf12e5557a26a9a2aabeb32af4ccc6ff3aa7cbf259385b81699a29ccd6edcef3d74634f56de357edf1ee5faddba2d5ba6ddba29a28a636a69a636888ee88044681c3581a15afa8a3d6644c6d5dfae3da9f2ee8f08f7ee99000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001e3978f465e25ec6b9bf42f5baadd5b774c6d2f6018359bf95819155562eddc7bd4ef44d545534d51df1bc3c7daaebebdeaaaa9f39996afacf0369baa6657974dcbb8d76e4f4ae7abda69aa7b6769ed75e89c25a5e8b73d759a2abd911caede9899a7ca3947cfc4143d2381756d4269af2288c2b33ceabd1edfba9e7f1d97dd0b8574dd1222bb36e6f64f6dfbbd75479777cfc653800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003ffd9, 'Delivered', 'ORD1000000007');
INSERT INTO `delivered_product` (`delivered_product_id`, `order_receiver_signature`, `order_receiver_signature_status`, `order_details_id`) VALUES
(5, 0xffd8ffe000104a46494600010100000100010000ffdb004300100b0c0e0c0a100e0d0e1211101318281a181616183123251d283a333d3c3933383740485c4e404457453738506d51575f626768673e4d71797064785c656763ffdb0043011112121815182f1a1a2f634238426363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363ffc00011080314043803012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f40a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28aa9a8ea369a5dab5cdeccb14438c9ea4fa01dcd005baa777aa69f62c56eef6de06033b649006c7d3ad79d6bfe3cbdbe2d0e99bacedff00be0fef1bf1fe1fc3f3ae561867bb9fcb82292795b9da8a598fe02803d962f136892b61754b507fda902ff3ab31eb1a64adb62d46d1cfa2cea7fad78e7f60eb1ff40abeff00c077ff000aad7167756a71736d3427d2442bfce803de01046472296bc06391e270f1bb230e8ca70456c5978b35cb27ca6a12ca0904ace7cc07db9e47e18a00f66a2b85d27e23432158f55b63093ff2d61cb2fe2a791f866bb1b1bfb4d4201359dc473c67ba1ce3d8fa1f63401668a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a006c8e9146d248c151016663d001debc5bc47adcdae6a925c3b110a92b0c7fdc5edf89ef5e8de3ed40d8f86e58d09125d308460f40796fd011f8d79a68361fda9adda59904a4920de01c7ca396fd01a00def0c78227d5634bcbf66b7b4619451f7e41ea3d07bf7fd6bd1f4fd36cf4bb7f22c6dd214efb472df53d4fe3565555142a80aa060003000a75001451450056b9d3ecaf08375676f391d0cb12b63f31581a8f80b46bb5260492d24e48689b233ee0e78f618aea28a00f26d5fc0bab69fb9edd45ec23f8a11f363dd7afe59ac0b6babad3ee7ccb7965b7990e0952548f63fe15ef1597abf87b4cd654fdb2d94c98e264f95c7e3dfe87228038dd1be22cf16d8b5783ce5ff009ed1001bf15e87f0c5775a76a765aac1e758dc24c9df69e57ea3a8fc6bce75cf015fd86e9b4f26f601ced518917f0eff0087e55ccd9dedd69d74b3da4cf04c9fc4a71f81f51ec6803de28ae4fc2be338356d9697db60bde8a7a24bf4f43edf97a0eb2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2803ccbe26def9dabdbd9ab02b6f16e2076663cfe817f3a77c31b212ea77578c0110461173fde63d47e0a47e358fe377f33c5b7e718c32afe48a3fa5769f0ce3d9e1e99f032f72dcf7202a8ff1a00ebe8a28a0028a28a0028a28a0028a28a002b9cf12f846cf5b479a20b6f7d8e250387f661dfebd7eb8c57474500784ea1a7dd69778d6d7913452af383dc7a83dc5771e11f1beff002ec35893e6fbb1dcb1ebecff00e3f9fad757aee8767aed9f91749875e63957ef21f6f6f6af25d7742bcd0af3c8ba5ca3731cabf75c7b7bfb5007b6d15e61e12f1a49a76cb1d4d9a4b3fba92756887f55fe5fa57a647224b1ac91baba380caca72083dc1a007d14514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451401e3fe3c4dbe2ebde301b611ff7c2d767f0d5f7786dc7f72e187e8a7fad73bf1361d9af5bca17024b7193ea4337f4c56c7c2fb80da5dedb6398e61267fde5c7fecb401dbd145140051451400514514005145140051451400555d4b4eb5d52cded6f221244df9a9f507b1ab5450078d789bc3773a05d61b325ac87f75363afb1f435a5e09f14b6953ad85ec99b191be5663fea58f7ff0074f71f8fae7d2751b0b7d4eca5b4ba4df148307d47a11ee2bc675cd227d175392ce7e71ca3e301d7b1ff003df3401ee145719f0fbc43f6db4fecbba7ff0048b75fdd13fc69e9f51fcbe86bb3a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a00e0be2940c6df4eb80bf2233a31f72011ffa09aa5f0bee4a6a9796bc6d9610ff008a9c7fecc6ba6f1f591bcf0bceca199edd9660147a707f2049fc2bce3c2d7eba6f88acee6420461f63927002b0da49fa673f85007b5d1451400514514005145140051451400514514005145140056178b7405d774b2a800bb872d037a9eebf438fe55bb4500783db5c5ce997e93c45a1b881fb8c10475047e8457b3e85ab43ad6991de41f2eef95d33928c3a8ff3d88ae2fe22787fca93fb62d53e47216e140e8dd9bf1e87df1eb58be0cd78e8bab013362d2e3092ff00b3e8df87f226803d7e8a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028ac3d77c57a6689ba3964335c8ff0096117247d4f41fcfdabcff0058f1b6ada9ee8e29059c078d909f988f76ebf962803d2750f10693a63f97797d14720ea809661f5032452e9dafe95a9becb3be8a473c04276b1fa03826bc469558ab06524303904751401eff004563f852fa7d47c39677372499594ab31fe2c3119fc715b14005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514011cf0c77104904cbba3914a3afa8230457876a9612699a95c594df7a172b9c6370ec7f1183f8d7bad701f12f47cac3ab429f77f753e076fe163fcb3f4a00e8fc1fab7f6be810c8ed99e1fdd4b93c923bfe2307eb9adcaf28f87fabff0067eb82da43886f008cfb3ff09fe63f1af57a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a008ae6de2bab696de740f14aa51d4f706bc4b5ad324d2355b8b294e7cb6f95b1f794f20fe55ee55c3fc4bd2bceb387548d7e680f972ffb84f07f03ff00a150069f80f56fed3d052291b33da6226f52bfc27f2e3f035d2d792780b54fecef10242ec443763ca619e377f09fcf8ff811af5ba0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a2a3134465f284a9e60fe0dc33f9500494514500145145001451450014514500145145001451450014514500145145001451581e21f1658686ad193f68bbed021e9fef1edfce80362eeeedec6d9ee6ea558a14196663fe79f6af3af11f8f27bcdd6da4efb683a198f123fd3fba3f5fa74ae775ad76fb5bb8f36f25f957ee44bc227d07f5eb5a5e1ef06dfeb3b67941b5b33cf98e3e671fec8eff5e9f5a00e7006770002ccc7a0e4935d0e9de08d6efd779b75b542383707693ff01c13f98af48d1bc39a668ab9b48332e306693e673f8f6fc315ad401e7507c349d97371a9c71b7a471171f9922a787e19a2cca66d519e21d4243b58fe3938aefa8a0086d6da2b3b68ededd02451285551d854d451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400556d42ce2d42c67b49c663990a9e3a7bfd475ab34500783de5b4da7df4b6d2e56681ca923d41ea2bd8fc33ab0d6b4482ec91e6e364c07671d7f3ebf8d721f12f47d9343ab42bc4988a6c7a8fba7f2e3f0159df0fb59fecfd5fec733620bcc28cff0c9fc27f1e9f88f4a00f55a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800aaf7f691dfd8cf6930fddcc850f1d33dfea2ac51401e0d7304d617b240ff2cd04854907a107b57b4e83a90d5b46b5bd18dd227ce07661c37ea0d703f1274cfb36ab15fc630974b87ff7d78fd463f2356be196a9b27b9d2e46e241e7459fef0e187e583f81a00f44a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800aa1ac6ad69a35935d5e390a38555e59cfa0157abc73c5dad36b5ad49223936b0931c033c60756fc7afe43b5003f5ef176a3ad3326f36d6a7810c6dd47fb47bff002f6ac0ad3d23c3fa96b2e059dbb18c9c199fe58c7e3dfe8326bd074cf006916d6e05eabde4c7ab166451f400ff003cd0079bdb6afa95a285b6bfb98947f0a4ac07e59c574fa37c42beb79026aa82ea13d5d142c8bf9707e9c7d6ba2bdf87fa2dc03f67135ab638d8fb867d486c9fd45719af783752d1c34ca05d5aaf3e6c63951fed2f51fa8f7a00f55b0bfb5d4ad56e6ce659626eebd8fa11d8fb559af0fd1b5abdd12ebceb2976e71be36e55c7a11fd7ad7a9f877c5363af26c43e4dd019681cf3f553dc5006ed145140051451400514514005145140051451400532491228da491d511465998e001ea4d53d5f57b2d1ad0dc5ecbb07f0a0e59cfa01debcb3c47e2abdd764284982d01f96053d7dd8f73fa5006ef89fc78d2efb4d158a2746b9c618ff00bbe9f5ebe98eb5c5dadadd6a376b0db46f3cf21e83924fa9ff00135abe1df0bdeebd26f41e4daa9c34ec38fa28ee6bd4f46d12c744b6f26ca2c16fbf2372ee7dcff4e9401cf786bc0b6f61b2e754d97373d445d634ff00e28fe9fcebb2a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a00a9a9d845a9e9d3d94ff7264db9f43d8fe0707f0af12beb49b4fbe9ad671b6585ca9c7b771eddc57bc570ff0011340fb440357b64fdec236ce147de4ecdf87f2fa5006df8435c1ade8e8eedfe950e239871927b37d0ff003cfa56ed78a786b5a9342d5a3ba5cb42df24c83f894ff51d457b3c134771047342e1e391432b0e841e868024a28a2800a28a2800a28a2800a28a2800acdd475fd2b4b93cbbdbd8e29319d9cb30fc064d4dab5cbd9e93797518cbc30bbaf19e402457874b2c93caf2cae5e47259989e493de803dcac353b2d4e33258dcc73aaf5d8791f51d455baf09d3351b9d2afa3bbb47d9221fc1877047715ec3e1ed76db5eb0f3e0f9254c096227943fd41ec7ffaf4010f8c34cfed5f0edcc4abba58879d16012772f603d48c8fc6bc9749bf7d3354b6bd8f3985c3100e370ee3f1191f8d7bad789f8974f1a5ebf796aa008d5f7460740adc81f8038fc2803da62912689258983c6ea19587420f434fae6bc03a81bef0d448e4992d58c2493d40e57f0c103f0ae96800a28a2800a28a2800a28a2800a28a2800a28a2800a28a28039bf1d6ac74cd024489b13dd1f2579e4023e63f971f88af31d134d9357d5adeca338f35be66feea8e49fcb35d37c4ebb32eaf6b68082b043b8e3a8663ce7f055fceadfc2fd3c1379a8baf4c4119cfe2dffb2d0077b6f0476d6f1c10a848a35088a3b01d2a4a28a0028a28a00e33c4de0682f83dd694a905cf53174493e9fdd3fa7f3af3974b9d3ef0ab892dee616f756422bde6b9cf17f8663d72d0cb02aadfc43f76dd378fee9fe9e9f9d0043e0df158d6a2fb25e155bf8c67238128f51e87d47e23d07535e0d0cd73a75eacb19786e607e38c1561d88fe95ec7e1bd721d7b4c5b84dab32fcb3463f81bfc0f6ffeb1a00d6a28a2800a28a2800a28a4a005ae7fc4de2ab5d0a131a959af587c9083f77ddbd07ea7f518fe29f1d476dbecf4765926e8f71d553fddf53efd3eb5e7d14775a8de848d64b8b999bdd9989ea4ff008d0049a9ea577aade35d5eca6490f03d147a01d85759e16f033dcedbcd615a387aa5bf467f76f41edd7e9df6bc2be0a874cf2ef3510b35e0f9953aa447fa9f7eddbd6bafa006451470c4b1c48a91a0c2aa8c003d853e8a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a6b2aba14750cac3041190453a8a00f1ef17e80da16a67ca07ec73e5a13d71eaa7e9fcb15b9f0efc43e549fd8f74e023926dd89e8ddd7f1ea3df3eb5daeb9a4c3ad6972d9cdc6ee51f1928c3a1ff003d89af17bab6b9d32fe48260d15c40f83838208e841fd41a00f78a2b0bc25af2ebba507723ed50e12651ebd9be87fc6b76800a28a2800a28a2800a28a280192c49342f14aa1a3752aca7a107822bc6fc4fa049a06a3e497f320941785fbe33d0fb8fea2bd9eb8ef89763e7e890ddaa92d6d2f273d11b83fa85a00f30ad4f0deaefa2eb305d027cacec980ee87afe5d47b81542d6da5bbb84b781774b21c2ae7193e94c92378a468e54647425595860823a822803df410464722bcfbe285861ecb515079060739e3fbcbffb37e5577c03e25fb6c0ba55e30fb444b8858ffcb441dbea07e9f435b1e33b117fe18bc5da0bc4be7213d8af271f8647e3401c87c32be10ead7366c540b88c32e7a9653d07e058fe15e995e1fa05f7f676b965765b6ac728de719f94f0dfa135ee14005145140051451400514514005145140051451400514556d4ae4d96997574a03182179003df009fe9401e3be28bcfb7f88efe70415f34a295e8557e507f215e9be09b3fb1785ecc1501e60666c77dc720ff00df38af2086279e648a352d248c1540ea493815ef304296f6f1c110c471a8451e800c0a00928a28a0028a28a0028a28a00e1bc7fe1afb444dabd9a7ef631fe90807de51fc5f51dfdbe9cf17e1fd667d0f534ba8be64fbb2c7fdf5ee3ebe95ed9d460d79078d3431a2eb2de4ae2d6e01922f45f55fc0fe845007ad5adcc3796b15cdbb878a550c8c3b83535709f0cb55325bdc6972364c5fbd8b3fdd270c3e80907fe046bbba0028a2b0fc45e28b2d062dae7ceba61f2c0a79fab1ec280352fafadb4eb56b9bc9961897ab37f21ea7dabcc3c4fe33b9d6375b59eeb6b23c119f9e4ff7bd07b7f3ac6d635abed6ae7cfbd977633b235e1507a01fe4d6df85fc1771ab6cbabedd6f64791d9e51ede83dff002a00c7d0f41bdd76ebcab44c22ff00ac95beea0f7f7f6af56d03c3b65a0dbedb75df330c493b0f99bfc07b56859d9dbd85b25b5a42b0c2830aaa3fce4fbd4f4005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400571bf107c3ff6db3fed3b64ff0048b75fde81fc71fafd47f2cfa0aeca92803c4fc3fac4ba26ab1ddc7964fbb2a7f7d0f51fd47bd7b3dadcc3796d1dc5bb878a550cac3b835e53e35f0fff0062ea7e6c0a459dc12d1fa21eebfe1edf4ad2f87be22fb2dc0d22e9cf93337ee189fb8e7f87e87f9fd6803d2a8a28a0028a28a0028a28a002a9eab62ba96977364f8c4d195048ce0f63f81c1fc2ae51401e090cb2d9dda4a9949a170c33d981ff00115df78c7434d634c8bc41a727ef1a2592541d5d319cfd40fd3e95cdf8e2c3ec1e27bac0212e0f9eb939ceeebff8f6eaecfe1bea1f69d09ed188df6921007fb2dc8fd777e5401e610cb241324d0b94911832b29c1047435ec3e17d762f10e965dd545c47f24f1f6cfa8f63cfea3b5711e3af0d7f655d7dbace302ca76e5547faa7f4fa1edf97a561e81abcda26a91de45f328f9644fefa1ea3fcf702802bea7686c352b9b4249f26564048ea01e0d7b2f872f4ea3a0595d331677880727a961c31fcc1af37f1e47149ac45a8dabf996f7d02c8ae07048f948fc80fceba5f8637a65d2eeacdb24c1287193d98741f8a9fce803b5a28a2800a28a2800a28a2800a28a2800a28a2800ae7fc773795e13bcf9b6b3ec41ef97191f966ba0ae33e274caba25b439f9dee0301ea02b67f98a00e2bc236df6bf1469f1e71b651267fdc1bbff65af68af2bf86d6eb2f88da461fea606653ee485fe44d7aa50014514500145145001451450015cef8e74a1a9f87a5645ccd6bfbe4f5c0fbc3f2cfe20574549401e21a06a4748d66daf464ac6df381dd4f07f435edb1c892c6b246c1918065607820f7af15f11e9eba5ebd796698d91be500ecac3701f802054d71e27d466d12df4a590c7044851c83f348327009f403031edf90075de28f1da5beeb4d19d649b90f71d553fddf53efd3ebdbcf956e6feef0a25b8b899b3dd99cff003357344d0afb5cb9f2ace3f957efcadc227d4fafb57aaf87fc3763a0c38817ccb861879dc7ccdec3d07b7f3a00c2f0c78162b4db77ac2acd3e01583aa27d7fbc7f4fad76d451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514014359d2e0d634d96cae0001c655f1928dd987f9f515e2d7f673e9b7d2da5caec9a16c1fe847b1eb5ef15caf8e3c37fdaf67f6cb5426f6dd78503fd627f77ea3a8fc477a0093c15e231ad5879170e3edd6e007e79917b37f8fbfd6ba6af09d36fee34bbf8af2d5b6cb136467a11dc1f635ecda26af6fad69b1de5bf19e1d33928ddc1a00d0a28a2800a28a2800a28a280387f89ba779b616da822e5a06f2e4217f85ba127d0118ff81573df0ff52fb0f8892176c4576a62393c6eeaa7eb918ff8157a76ab609a9e9973652636cc8541233b4f63f81c1fc2bc43f7f6577fc515c4127d0a329fe608a00f74bbb582f6d64b6b98c490cabb594f715e37e24d0a7d07516824cb42ff003432ff007d7fc477ff00eb8af5fd2af9353d32daf63c62640c403d0f71f81c8a875dd1edf5bd39ed2e383d63900c946ec6803c69ef1e4d3a3b3909658642f1127ee06fbc3f1214fe07d6b77e1ede7d97c4d1c471b6e63688e4f43f787fe838fc6b0350b29f4ebd96d2e576cb13608ec7d08f63d692c6e5acafadee9006682459003df073401ef34535595d43210cac3208e8453a800a28a2800a28a2800a28a2800a28a2800af3af8a570ad77a7db0fbc88f21fa31007fe826bd16bcabe244eb2f89422f5860446fae4b7f261401a9f0b221bb5298af2046a0ff00df44ff00215e835c6fc31451a0dcbe3e66b9209f501571fccd765400514514005145140051452741cd002d64ebfe20b2d0adbccb96dd330fddc2a7e67ff01eff00feaac7f12f8ded74e8cc1a6bc775767f894e523fa91d4fb0fc7d0f99dddd4f7b72f71752b4b34872cec793400fd4af64d4751b8bc9461e672f8ce71e83f01c56ff0084fc212eb78bbba630d8838047de908ea07a0f7ff239eb2b49efeee3b5b58cc934adb5547f9e95ed9a3e9e9a56956d6319dc214c16fef1ea4fe2493401359d9dbd85b25b5a42b0c28301547f9c9f7a9e8a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2803cebc7de183148fabd8a663639b88c0fba7fbe3d8f7fcfe9ce78675e9b41d444cbb9ede4c2cd103f787afd476ff00ebd7b33aaba323a865618208c822bca3c67e186d16ebed36aa4d8ccdf2f7f2dbfba7dbd2803d4ed2ea1bdb58ee6da4124322ee561dc54d5e51e0bf139d1ae7ecb76c4d8ccdc9ff009e4dfdefa7afe7f5f56560ca1948208c823bd002d14562eafe2ad27482c93dc79932f5861f9987d7b0fc48a00daa8e69a2b789a59e548a35fbceec140fc4d79bea7f116fa7ca69d6e96abd9dfe77ff0001f91ae52f750bcd425f32f6e659d8742ec4e3e83b7e1401ea57fe3ad12cd8a24d25d30241102640fc4e01fc335e69aeea116a9abcf7b05bfd9d663929bb3ce393d3bf5a9f4ef0c6b3a900d6f63208ce0f9927c8a41ee33d7f0cd6b5f780afac348b9bd9ae6179205dfe544090cbdce4e318193d3b50066e99e2bd574ad3c5959ca8b1862c0b26e2338e0678c7e1dcd3a6f18ebf32ed6d45c0ff0061154fe60565580b76bfb75bc2c2d8c804a54e085cf26bd5e2f04787e3500d89723f89a57e7f5c5007935dde5cdeca25bbb8967900c0691cb103d39a86bdb21f0e68b0a6d4d2ed08ff006e20c7f339af36f1de990699e202b6a8238a78c4a1146021c9040fcb3f8d007394574de08f117f636a1f67b9722cae080f93c46dd9bfa1f6fa57ad5007cfe091d0e2a54b99e3ff00573c89feeb915eeb25a5bca732dbc4e7d5901a80e91a637ded3ad0fd605ff0a00f184d5f538ffd5ea376bfeeccc3fad49fdbdac7fd056fbff021ff00c6bd80e87a41eba5d91ffb774ff0a6bf87f46752a74ab200ff00760507f302803c9e1f136b703064d52e491fdf7de3f5cd5b5f1c78855b26f830f430a7f415e82fe0cf0fb9c9d3947fbb238fe46a9dcfc3fd0e66cc6b716e3d239723ff001e06803994f88fab8c06b6b2603afc8c09ff00c7aaf47f131c2012694acddcadc607e5b4d5ab9f86b64d8fb2ea13c5ebe6a87fe58aa927c33902662d5559bd1a0da3f3dc6802c47f12edcffadd3655ff007650dfd0571be24d4a2d5f5cb8be851d23976e15f1918500f4f715bedf0df5407e5bbb323dd987fecb5c95d40d6b7735bb32bb44ec8590e54907191ed401d97853c5fa5e87a325a4f0ddb4c5d9dca2295c93c632c3b015b3ff000b1b47ff009f7beffbf69ffc5573169e01d56eeca0ba8a7b3d9346b22a97607046467e5ebcd23f8035d5ce2381ff00dd947f5a00ea7fe16368dff3c2f7fefdafff001549ff000b1f47ff009f7beffbf69ffc557227c0de201ff2e6a7fedb27f8d4b0f8075d918078a1887abca0e3f2cd0074371f12ac94ff00a369f3c9ff005d1c27f2cd51b8f89772c3fd1b4d8a33eb24a5ff009015593e1beaa586fbab20bdc86727ff0041a9eefc056ba6da1bad475a1144bf7b10753e83e6e4fe14019d75e3dd76e3fd5cb0db0f48a21cff00df59ac2bcd4efefc0179793ce01c859242c01f61daa3ba36df686fb189442385f3482c7dce381f4fd4d68f87fc3b7baf5cecb75d9029c493b0f953fc4fb7f2a00c8ae9744f04ea9aa159264fb1db9fe3947cc47b2f5fcf15e8da3f8774dd1a2416d6e8665eb3ba83213df9edf41c56ad0065687e1ed3f4288ada464cac30f33f2edf8f61ec2b568a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a86eed60bdb592dae6312432aed653dea6a2803c5fc4da04da0ea2616dcf6f264c3291f787a7d477ff00ebd6ff0082bc5e9631ff00676a92e2dd4130cc72767fb27dbd3f2fa751e369f4b8f42923d4fe6327fa845fbe5c7423d319e4fa1f7c1f20a00ebfc4be38b9d44bdb69a5edad3a171c4927e3d87b7ffaab99b0d3eef52b810595bbcf21eca3a7b93d00f73573c37a65b6afabc56975742dd1ba1c72e7fba3b027dff5af61d3b4eb4d2ed45b5940b0c439c0ea4fa93dcd0070ba57c3891c2c9aadd08c1e7ca8393f8b1e01fa03f5aebf4cf0e693a510d696518907fcb47f99f3ea09e9f856ad1400535d1648da391432302195864107b53a8a00f0bd62c1f4bd56e6c9f27c972a09eebd41fc460d7abf83352fed3f0e5bbb1ccb0fee64faaf4fcc60fe35cbfc4ed3364f6da9c6bf2c83c9908007cc3953ee48c8ff00808aa9f0df53fb2eb1258c87e4bb5f97d9d7247e633fa5007a85705f14ad89834fba0a36ab3c6cddf90081ff008eb577b5cd7c41b65b8f0b4ee73981d245c7ae76ff00263401e495e99f0ffc442f6d469776e3ed102fee49eb220edf51fcbe86bceec2d5afaf23b5460af29da99eedd87e2703f1a4866b9d3af5658cbc37303f19182ac3b11fd2803de68ac9f0e6b90ebba625c4642ccbf2cd18fe06ff0003dbff00d75ad400514514005145140051451400c92458a2791ce11016627b015e08ccd24859892cc727dc9af67f15dc8b4f0cea12919cc2631f56f947f3af22d1a15b8d66c617fbb25c46a7e8580a00f6eb3b75b4b382d94e5618d6307d80c54d451400514571de28f1bc3a779969a6159eec7cad27548cff0053fa7f2a00d9f10788ecb41b7dd3b79970c331c0a7e66f73e83dff009d7946b5addeeb775e7de49903ee46bc2a0f61fd6aabbdd6a37a598c97173337bb331af42f0bf8163b5db77ac2acb3f55b7eaa9fef7a9f6e9f5a00c2f0b782e7d5765ddf8682c8f2a3a3cbf4f41eff0097ad7a75a5ac1656c96f6b12c50c630a8a381535140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140054179770d8da4b7572e1218977331a9ebccbe216be6eef0e956cff00b8b76fde91fc727a7d07f3cfa0a00e775dd5e6d6f5396f26c80788d33908bd87f9ef9ae8b41f0249a8e90f7777334124a99b74c74f466f63e9f8d54f037877fb5efcdd5ca66ced882c08e246ecbf4ee7f0f5af58a00f07bcb4b9d36f5edae51a29e26c11e9e847f8d7a3782fc5cba8a269fa8c805e2f11c87fe5a8ff00e2bf9d6af8a3c3506bf6bda2bc8c7eea5c7fe3adedfcbf307c92f2d2e74ebc7b7b98da19e23c83d47b83fd6803de28ae2fc1be3117e134ed4dc0bae914a7a4bec7fdafe7f5ebda50014514500676bfa62eafa35cd91c6e75cc64f671ca9fcff4cd78ac32cd677692a6639a070c32395607fc457bdd794fc41d23ec1adfdae35c437997e3b38fbdfd0fe26803d2f4bbe8f53d36def61fb93206c673b4f71f81c8fc29358b6379a3de5b2a86696075507d4838fd715c5fc33d5b89f4995bfe9ac39fc987f23f9d7a0500783595c359df5bdcafde8645907d41cff4aef7c7fe1a13c6dacd8ae5d466e117f887f7c7d3bfb73db9e1b56b6167ab5e5b2e76c33ba2e7d03102bd8bc39722f3c3b6136edd9815589eec060fea0d0079378775b9f42d496e62cb467e5963ecebfe3e95ecb657705fda45756d2092195772b0ff003d6bcc3c6fe1afec8bbfb65a27fa14edd07fcb26feefd3d3f2fab7c15e263a35dfd96e989b19db93ff003c9bfbdf4f5fcfb7201eb14520208041041e8452d001451450014514500727f1227787c34114f134e88df4c16fe6a2b84f07c1f68f14e9e9e92eff00fbe416fe95d27c52b8532e9f6c1be65579197d8e003fa35647c3cb769bc53148bd208de43f4c6dff00d985007ad54377750595b3dc5d4ab142832cec700566ebfe24b1d061cdc379970cb98e043f337d7d07bfe59af2bd775fbed76e3ccba9311a9fddc2bc2a7f89f7a00dbf1478de7d477da69a5a0b43c33f47907f41edd7d7d2b9fd1f46bdd6aec5bd945b8ff1b9e1507a935afe18f075d6b456e2e375bd967ef91f349fee8febd3eb5ea1a7e9f6ba65aadb59c2b144bd87527d49ee68033bc3be18b2d062cc63ceba61879d873f403b0adba28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a00a5ac5f2e99a4dd5e311fb98cb2e7a16ec3f13815e20ab2dcdc0550d24d2be00ea5989fe79af48f89b7a61d26dacd4b03712966c1e0aa8e87f12a7f0ae43c136c975e2ab259065518c9d7ba8247ea05007aae8da6c7a4e976f6516311afccc3f89bb9fc4d5ea28a002b17c49e1cb5d7ed76b811dca0fdd4c0723d8fa8adaa2803c2751d3eeb4abd7b5bb8cc72a1fc08ec41ee2bbff0678c45d84d375593171f7619d8ff00acff0065bfdaf7eff5ebd0f88340b5d7acbc99c6c957262980e50ff51ea2bc8755d2eeb47be7b5bc4db22f208e8e3b107b8a00f74a2b83f06f8cc4a23d3b5693127dd8ae18fdef663ebefdfebd7bca002b17c59a47f6ce853408b99d3f790ffbc3b7e2323f1adaa2803c274cbe974cd4a0bc87efc2e1b19c64771f88c8fc6bdc2d2e62bcb58ae603ba29503a9f62335e51e3ad23fb2f5e79235c5bdd6654f407f887e7cfd08ae93e1aeafe6dacba54adf343fbc87fdd2791f8139fc7da8039bf1f5b1b7f155cb6ddab32a48beff2e09fcc1aecfe1cdcb4fe1911b63104cf18fa1c37fecc6b0be28db05beb0bacf3244d1e3d369cff00ecf52fc2db8024d42d8b7ccc12455fa6413faad007777b6905f5a4b6b73189219576b29ff3d6bc6bc43a1dc685a8b5b4c0b46df3432f675ff1f51ffd6af6cacfd6b47b5d6ac5ad6e978ea8e3ef21f51401c47827c5e2d42699aa49883eec33b1ff0057fecb1feefa1edf4e9e8d5e27aee8779a15e182e9728d931ca07cb20f6f7f6addf09f8d24d302596a45a5b31c249d5a21fd57dbb76f4a00f50a2a38268ae2149a091648dc655d4e41152500145154b55d52d748b27babc942228f9573f339f403b9a00f2ef1e5efdb3c5170158325b858548f6e48ff00be8b550d175a9b44fb4cb688bf6999046b2b72235ce4f1dcf03f2e86a84f33dc5c493cadba4958bb1f524e4d5fd1341bed72e3cbb38fe45fbf2b7089f53ebedd6802913737f779265b8b895bdd99cff335e81e18f01a41b6ef5a55925e0adb7555ff007bd7e9d3eb5d0787fc3363a0c5fb95f36e5861e771f31f61e83dbf3cd6d50020000000c01da968a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a280380f8a6a4ae98d8f9419413ff7c7f85715a45fbe99aa5b5ec79cc2e1881dd7b8fc4647e35eb1e2ed19b5bd11e087fe3e23612c4338cb0cf1f8827f1c578ec91bc5234722323a12acac30411d88a00f78b4ba82f6d92e2d65596190655d4f06a6af10d1f5dd434598bd8ce5558fcf1b0ca3fd47f51cfbd779a47c43b0b8409a9c6d692e397505d0fe5c8fa60fd6803b3a2aa596a763a80ff43bc82738dc56370481ee3a8ab74005666bda1da6bb646dee461d7263940f990ff87a8ad3a2803c3b58d22ef46bd6b5bb4c11cab8fbae3d41aeafc1fe3436fb34fd5e4cc3f762b863f73d9bdbdfb7d3a76fac69169acd935b5e4791d51c7de43ea0d7916bfa0dde8379e4dcaee89b98a651f2b8fe87d47ffae803dac10c010410790477a5af2af0978c26d2196d2f8b4b627807ab43ee3d47b7e5e87d42dae21bbb749ede4596271957539045006278d748fed6d064f2d73716ff00bd8f8e4e3a8fc476f502bcaf48d464d2b54b7bd8b9689b257fbc3a11f88c8af74af1df19e8ff00d8faec8b1ae2da7fdec581c007aafe07b7a62803acf88be4def86ecafa1f9d4caa51ff00d86527fa2d73bf0eae7c8f142478cfda2278fe98f9bff65a7699a9b5e782753d1df992051343fee070587e1c9fc7dab2bc2b70f6de26d3a44ea6758ff06f94fe84d007b5d14514015750b0b6d4ed1ed6f2259626ec7b1f507b1af2af13f852eb4295a54dd3d893f2ca072becde87dfa1fd2bd7e9ae8b22323a8656182a46411e9401e2da2788b51d0e426ce5cc44e5a193946f7c763ee2bbdd27e206997984bd57b290f76f990fe23a7e23f1a66b5e00d3aeb7cf6329b17e58ae331fe5dbf0e07a579b5dc02dae6485678a70871e6444956fa1205007ab6bfe32d3f4ab7ff46963bcb9719448dc151eec476f6ea7f5af2fd5354bcd5eedae6f653239e83f8507a01d8553a74514934ab1448d248e70aaa3249f402802c69764fa96a76d6699ccd205240ce0773f80c9fc2bdc2d2d60b2b64b7b5896285061514600ae53c11e14934acea1a828176eb88e3ebe503d49f73fa0faf1d8d001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450015cff0088bc2763ae8f349fb3ddf6991739ff007877fe75d051401e457de07d72ccb15b65b94519df0383fa1c1fd2b02e2de7b594c5730c90c83aa48a548fc0d7be543736b6f771f97750453a673b6440c3f23401e0ca4a90549047208ed5d069fe35d72c405fb50b941fc370bbff00f1efbdfad7a05cf83341b92cc6c16366ef13b2e3e801c7e95c8f89fc0dfd9764f7ba74d2cf147cbc6e01655eed91d40fa74a00e83c33e378357992d2f635b6ba6e1083f2487d067a1f6fd6badaf00562ac19490c0e411d457b27843577d67428a798e678c98a538c658639fc4107eb9a00dbaad7f636da8dabdb5e44b2c4fd54f6f71e87deacd1401e3fe27f0adce853191374d62c7e4971cafb37a1f7e87f4a67867c4d75a05c60665b373fbc849ffc797d0ff3fcb1ec12c51cd13452a2bc6e30cac3208f422bcc7c5be0c934bdf7ba70692cbab275687fc47bf6efeb401e8da6ea36baa59a5d59ca2489bf353e84763593e35d17fb63447f29375d5be648b0393eabf88edea0579a681af5de837a27b73ba36e25849f95c7f43e87ff00d55ebda46ad69acd92dd59bee53c329fbc87d08f5a00f1086692072f131562ac848f460411f882696da67b6b98a78ce1e270ea7d083915d0f8e744fec9d64cb0ae2d6eb32201d15bf897f5cfd0fb5735401f405154f4899ae748b29dce5a5823727dca83572800aaba8ea16ba65a35cdeccb144bdcf527d00ee6b37c47e27b2d061c487cdba6194814f3f53e82bcab58d66f75abaf3ef65dd8cec41c2a0f403fc9a00d6f13f8beeb5b66b78375bd8e7fd5e7e693ddbfc3a7d6a9787fc397baf4f8817cb814e249d87cabec3d4fb7f2ad8f0b78266d4765e6a61a1b43cac7d1e51fd07ea7b7ad7a5db5bc3696e905bc6b144830a8a300500635a783b44b6b25b67b249c8396964e5d8fd7b7d0715a565a5d869fff001e7670407182c880311ee7a9ab9450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500148ca194ab00548c107a1a5aa9aadc35a69579729f7a181e41f50a4ff004a00f0c936f98fb3eee4e3e95e9df0c948f0fdc31ce1ae9b1ff7cad797d7aff8121f2bc2767f2ed67dee7df2e707f2c5007434514500149d460d2d1401e75e31f05f91bf51d223cc5f7a5b751f73dd7dbdbb7d3a729a26b377a25e8b9b46ebc3c67eeb8f435ee15c0f8cfc1bbfccd4b498fe6e5a6b751d7d5947afb5006bdc3d978dfc352adab013ae19558fcd1483a67d8f233e84d794491bc52347229474255948c10475156b4ad52eb48bd4bbb3936baf041fbae3d08ee2a6f105fc1aa6ad2df5bc5e509c2b347fdd6c00df5e4673df3401ea5e0cb9fb5785ac58b02c88633cf4da4803f202b1bc53e398ecf75a690cb2dc7469faa47f4fef1fd3eb5c0db6ad7d69a7cf636f70f1dbdc1cc8abdf8c1e7b67bfae29da3e8f7bad5d8b7b28b7118dee78541ea4d005702e750bbc0f32e2e666f766735e8de16f03c561b2f355559aebaac5d5233efea7f41efd6b63c3be19b2d0213e50f36e5861e761c9f61e83dab6e800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800ae77c7b722dbc2b74376d698ac6bee4b648fc81ae8ab80f8a37df258d82b8e4999d71cfa29ff00d0a803cfabdd749b66b3d26ced9c00f0c088d8f50a01af1cf0e58ff696bd656a54323ca0b8271951cb7e80d7b750014514500145145001451581e26f14da683094189af187c9083d3ddbd07ea7f5001cdfc42f0fd95ba7f6a5bcb1c12c8d8780f1e69fef28f5f5eddfaf5e0aadea7a95dead78d757b299246e07a28f403b0a9aff0044bed36c2daeef22f296e09088df78600e48ed9cfe94019d5ebbe04b9b3b8f0e442d21485e33b2655ea5ff00bc7d72307f4ed5e455bfe0cd6ffb1b595f35f16b7188e6c9e07a37e07f426803d868a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002bc6fc697e6ffc4d76c189485bc9407b05e0e3db393f8d7abeb17eba6693757ad8fdcc64a83d0b7403f13815e2114725cce91c6a6496560aa0756626803bbf863a67375aa483fe9847fa163ffa0feb5e8354b46d3d34ad26dac9307ca4c311ddbab1fc4926aed0014514500145473cd15bc2f34f22c71a0cb3b1c002bcdbc55e3792ff00ccb2d299a2b53c3cbd1a51ede83f53fa5006c78abc6f1d96fb3d25965b9e8f37558fd87a9fd07bd79da25d6a378150497173337bb331ab1a3e8d7bad5df916516ec637b9e1507a935eb1e1ef0dd968306211e65c30c493b0e5bd87a0f6fe740193e15f0543a608ef3510b35e8f9953aac47fa9f7eddbd693e268cf87adce3a5d2ffe80f5d85735f1060597c2970e4f30ba38faee0bff00b3500794db5b4b75218e042ee14b6d1d4803271f8026a2ae83c07ff237d8ff00db4ffd16d5b5e39f0979064d574d4fdd1f9a78947dcff687b7a8edf4e801b7e00d73fb4b4afb14ef9b9b401793cba763f8743f87ad7575e1ba2ea9368daa437b0f250fcc99c6f53d457b4e9f7d6fa95947776afbe290641ee3d8fbd0059a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a6c8e9146d248c11141666638000ee680387f89baa7976d6fa646d8694f9b260ff0008e147e2727fe03587f0ef4cfb6ebdf6975cc566bbfa71bcf0bfd4fe158dafea6dabeb3737a7215db1183d90703f4fd6bd2fc07a5ff67787a2775c4d747ce6fa1fba3f2e7f13401d251451400552d5754b4d22cdae6f650883a0eace7d00ee6b3fc4be27b4d061dad89aed87c9003fab7a0fe7fcbca755d56f358bc6b9bd94bb9fbaa38541e807614017bc47e26bcd7a6c487cab55394814f03dcfa9a97c31e15bad7a5123661b253f34a472decbea7dfa0fd2b5bc2be077bc55bdd5d5a380f29074671eade83f53eddfd1e28d21896389152340155546001e8050057d374eb5d2ed16d6ca211c6be9d58fa93dcd5ba28a002b03c72a5bc237e073c21fc9d4d6fd65f896312f86f5256e82d9dbf204ff004a00f29f0aca61f1369cc09199d578f7e3fad7b49008208041ea0d788787b8f10e99ff005f517fe862bdc2803c83c67a0ff626ac4c2b8b4b8cbc5fecfaafe1fc88a67853c4936817986dd259ca7f7b18edfed2fbff003fc88f4ff10e911eb7a4cb66e4073f344e7f85c743fd0fb135e2b3c325bcf243321492362aca7a823822803de6de78ae608e781c49148a19587420d495e5be06f139d32e469f7b27fa14adf2331e2263fc81eff9fad7a95001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450015c6fc45d6bec7a72e9b0b627ba197c1e563cff0053c7d01aeb2eee62b3b596e6e1b645129763ec2bc4b58d4a5d5b539ef66e0cadc2e7eeaf61f80a00b3e18d21b5ad6a1b5233083e64c7d1075fcf81f8d7b48000000000e805735e05d0ff00b2748134c98babac3b823955fe15febf8fb574923a451b49232a2282cccc70001d493400eae37c57e358f4e3259698565bb1f2bcbd5623e9eedfa0efdc56478b3c70d75bacb47768e0e925c0e19fd97d07bf53eddf90b1b2b9d42e92dace169667e8abfccfa0f7a006169ef6eb24c93dc4adeeccec7f526bd1fc25e0a4b0297daaaac97439487aac47d4fa9fd0568f85fc276da146b34bb67be23e6971c27a85ff001ea7dba57474005145140051451400550d786740d441e86d65ff00d04d5facff00107fc8bda97fd7acbffa01a00f18d367fb36a56b3938f2a647cfd0835eef5f3fd7bbe9d706ef4eb5b93c19a1490fe201a00b35e7df11f41e46b36ebc7097007e4adfd3f2af41a8ee208ee6de482640f148a5594f706803c0ebd37c01e24fb75b0d2eedc7da615fdd313cc883b7d47f2fa1ae0f5fd264d17569ace4c9507746e7f890f43fd3ea0d53b6b896d2e63b8b7731cb1b06561d8d007bdd1595e1cd6e1d774c4b98f0b2afcb347fdc6ff03d456ad001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145154f56d461d2b4d9ef67fb912e40fef1ec3f1381401c67c49d6b0b1e8f03f5c493e0ff00df2a7f9fe5585e07d0c6afac09265cdadae1e407a31fe15fd3f206b0aeae27d42fa4b894979e772c703a927a0af4bb7bab1f037876182e487bd90798d121f99dcff203a67da803a5bfbfb5d36d5ae6f2658a25ee7b9f403b9f6af2af13f8b6eb5c76822dd05883f2c59e5fd0b7f8741efd6b3b5bd6ef75cbbf3ef1f85e238d78541ec3fad69f85fc2375ae32cf36e82c41e64c7ccfea17fc7a7d7a50067685a15e6bb77e4da2611799256fba83dfdfdabd6341d06cf41b4f26d97748dfeb2661f339fe83d07ffaeae58585ae9b6ab6d670ac512f451dfdc9ee7deacd00145145001451450014514500158fe2d9bc8f0bea2f9c66129ff7d71fd6b62b9ff1d1c7846fff00e01ffa316803c7ebda7c273fda7c31a73e3188427fdf3f2ff4af16af60f02ffc8a363ff03ffd18d401d051451401cb78fb45fed2d18dd44b9b8b3cb8c7f127f10febf87bd79457bfd78e78bf453a2eb52471ae2da6fde427b007aafe078fa63d68022f0d6b92e85aa25c2ee681fe59a31fc4bfe23a8ffebd7b2dbcf15cc11cf03878a450c8c3b83d2bc0ebbef871ae9576d1ae1be56cbdb93d8f565febf9fad007a151451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005796fc40d7bfb47501616ef9b6b56f988e8f27427f0e9f9d745e2ef185bd85bcd63a7cbe65eb0285d0f10faf3fdef61d3bf4c1f2fa00b16376f63771dd44a8658ce5378c856ec71ea3a8f7a6dcdcdc5f5cbcf7123cd34879663926a1af4cf04f84bec0a9a96a51ff00a530cc5130ff00543d4ffb5fcbebd00287857c0a58a5eeb4985fbc96a7a9f77ff0fcfd2bd051163454450aaa30140c003d29d45001451450014514500145145001451450015cff008eff00e450beff00b67ffa316ba0ae63e215d25bf85e589b96b891235fa83bb3f92feb401e4d5ec1e05ff9146c7e8fff00a1b578fd7acfc3dbb4b8f0c450af0f6cef1b03ee7703f937e86803a7a28a2800ac1f1868635bd1dd235cdd43fbc84f1c9eebf88fd71e95bd45007cff00d2a482692da78e7858a491b07461d883906badf885a0fd86ff00fb4add7fd1ee9bf783fbb2753f9f5fae7dab8ea00f70d0b558f59d260bd8f00b8c48a3f81c751fe7b62b42bca7c03ae7f66eabf6399b16d7642f3d15fb1fc7a7e5e95ead400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400579a78c7c63733dccfa6e9ce61b74252491786908ea01ec3e9d7e8715e975e27e23d2ae349d5e78a74608ce5a373d1d49e083401974e8e3796458e24677721555464b13d00157f46d0eff005bb8f2aca1ca8fbf237089f53fd3ad7a8f877c2b63a1461d479f767ef4ee391eca3b0fd680327c21e0b1a7ba5fea8aad74398a1ea223ea7d5bf97d7a7694514005145140051451400514514005145140051451400579bfc50ba2da8d95a63e58e23267d771c7fec9fad7a45797fc4eff009186dffebd17ff00437a00e3ebb7f85f72cba9dedae06d9211213dc156c7fece6b88aec3e18ffc8c371ff5e8dffa1a5007a851451400514514015b51b18752b09acee41314cbb4e3a8f423dc1c1af14d5b4d9f49d466b2b81f3c6786ecc3b11f5af74ae0fe2859836d657caaa0ab9899b1c9c8c8fc061bf3a00f3baf67f09eabfdb1a0c170edba64fdd4dfef0eff0088c1fc6bc62b6bc33e219f40bddeb97b69081345ea3d47b8a00f66a2a386549e08e68983472286561dc11906a4a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002a39a08a74d93c4922ff75d411fad4945003238d224091a2a28e8146053e8a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800ae43c79e1c9f56822bcb25df73002ad18eaebd78f71e9ef5d7d1401e0bf64b9f3fc8fb3cde6ff00cf3d8777e55e99e03f0e4da4412de5eaecb99c0558cf544ebcfb9f4f6aeba8a0028a28a0028a28a002a96afa6c3ab6993d94fc2cabc37753d41fc0d5da2803c2f55d32eb48be7b4bc8f6c8bc823a38ec41ee2aac68f2c8b1c6a59d88555032493dabdcb51d2ecb5484457d6c93a8e46eea3e84722ab69be1bd234b9bceb3b24497b3b12c47d324e2802c68d6af65a359db4bfeb228515b9cf20735768a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2803ffd9, 'Delivered', 'ORD1000000016');
INSERT INTO `delivered_product` (`delivered_product_id`, `order_receiver_signature`, `order_receiver_signature_status`, `order_details_id`) VALUES
(6, 0xffd8ffe000104a46494600010100000100010000ffdb004300100b0c0e0c0a100e0d0e1211101318281a181616183123251d283a333d3c3933383740485c4e404457453738506d51575f626768673e4d71797064785c656763ffdb0043011112121815182f1a1a2f634238426363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363ffc00011080314043803012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f40a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a64b2c70c4d2caeb1c6832ccc7000f526801f4d7758d0bbb0555192c4e0015c5eb5f10edadcb45a4c5f6971ff2d64c88c7d0753dfd3f1ae1b54d7352d5dbfd3aede45ce4463e541ff011c77ebd6803d1359f1e69961ba3b3ff004e9871f21c20ff008177fc335c36a1e2ed6efe6de6f648173c25b92807e5c9fc49a8348f0eea9acb03696cde51eb33fca83f1eff00864d77ba2f8074eb25126a07edb3f5c1e235fa0eff008fe5401a1e0cd42ef53f0f453dee5a40cc81c8c6f03bff004fc2b7a991c690c6b1c48a88a30aaa3000f4029f4005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145731e28f185b68a1adadb6cf7d8fb9fc31ff00bdfe1fca8034f5cd76cb42b5f3aedf2edfeae25fbce7dbdbdebcaf5ff125febb31f3dfcbb70729021f957ebea7dcfe9542eeeaef54bd335c48f3dc4ad8f524f6007f415dcf85fc061425e6b4b96eab6bd87bb7f87e7e9401cbe85e17d4b5c21e08c476f9c19e4e17f0f5fc3f1c57a068fe07d274e01e74fb6ce3f8a61f28fa2f4fcf35d2222c68a88a1554602818007a53a80100000000007402968a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a6bbac68ceec151465989c003d6abea3a8dae9768d7579288e25e327a93e807735e57e27f16dd6b8ed0c5ba0b1078881e5fddbfc3a0f7eb401b9e29f1d96df67a2b903eebdd773fee7f8fe5eb5c5d8d8ddea9782ded2279a67e78fe64f61ef57bc3de1bbdd7a7c40be5dba9c493b0f957d87a9f6fe55eafa2e8b65a25a791671e09c6f91b9673ee7fa50066785fc236da1aacf36d9ef88e64c7ca9ecbfe3d7e9d2ba4a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002b335ed72d342b2fb45c9dcedc45129f9a43ededea7b7e546bdaddb68560d73707731e23881e646f4fa7a9af1ed5754bad5ef9eeef24dd237000e883b003b0a00975bd6ef35cbbf3eedf85c88e35fba83d87f5adcf0a782e6d50a5e6a01a1b2fbcabd1a5fa7a0f7fcbd6b47c1fe0a0e23d43588f8fbd15b30ebe85bfc3f3f4af42a008adada1b4b74b7b68d628a3185451802a5a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002aaea5a85be99632de5d3ed8a31938ea4f603dcd587758d19e460a8a0966638007a9af23f18788db5cbff2e1622c6138897a6f3fde3fd3d07e34019faf6b371ae6a2f7571f2af48e3072117d3fc4d75be05f09ee11eada8c7c70d6f130ebe8e47f2fcfd2b3fc0de17fed39c6a17d1e6ca26f9118712b0ffd9477f5e9eb5ea1400b45145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014515ccf8d3c48ba2d89b7b761f6e9d709ff4cd7a6eff000f7fa500617c40f12ef66d1eca4f941ff4975ee7fb9fe3f97ad739e16f0fcbaf6a223e52da2c34d20ec3d07b9aceb0b2b8d4efa2b5b652f34ad819fd49f6ef5ecfa2693068ba6c7676e33b7977c60bb7726802e410c56d024302048a350aaa3a002a4a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a2a2b9b88ad6de4b8b8711c51a96763d80a00a5af6b10687a6bddcff311f2c7183cbb761fe7b578cdfdecfa8decb7774fbe595b24ff00203d874ad0f136bb2ebda9b4edb9604f9618cff0afa9f73dff00fad5b5e00f0efdbaeffb4eed3fd1a06fdd03d247f5fa0fe7f8d0074de07f0eff0063d87daae5317b70bf302398d7b2fd7b9ffeb575145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005798f8f7c4bf6fb83a659c87ecb0b7ef587491c76f703f9fd01ae87c77e23fecbb3fb0da3917970bcb2ff00cb34f5fa9e83f13e95e58012400324f6a00d0d0b499b5ad522b387e50dcc8f8cec51d4ff009ee457b4d9da43636915adb20486250aaa3fcf5ac5f06e80343d2f3328fb65c61a63fddf45fc3f993ed5d0d001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500159fae6ad0e8ba5cb79360ede113382ec7a0ff3db357c9001248007526bc83c63e203ae6a6444dfe87012b08c6377ab1faff2c7bd00635f5e4fa85e4b7772e5e595b731fe83d874aeb7e1e787fed777fdab729fb881b1083fc4febf41fcfe86b98d1b4c9b57d4e1b2838321f99b1908bdd8fd2bdaec6ce1b0b38ad2d90245128551fd4fb9eb401628a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a2a8eb1a9c3a469935ecfcac63e55ce0bb7651f53401cbfc43d7fecb6bfd936cff00be9d733107944f4fc7f97d6bcd6a7bebc9f50bd96eee5cbcb2b6e63fd07b0e82b6fc13a18d67580d32e6d2db0f2e7a31fe15fc48fc81a00ecfc05a0ff66697f6c9d31757603723944ec3f1ea7f0f4aeae8a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a280296abaa5a69166d757b2ec8c700752c7d00ee6b85bff0089174d291a7d9c51c7c80d3e598fbe01007d39acaf1dea8fa8788668839305a1f2917b023ef1fae78fa014cf0a785a4f103c923cde45b42c033019663e83f0efee383401a107c47d55197ceb5b491475c2b293f8e48fd2b62d7e25593e7ed7617111ede5b093f9e292e7e1ad9301f65bfb888f7f3555ff0096dac6bbf875ab4419ade6b6b803a0dc558fe0463f5a00ec6d7c69a0dc941f6df29dbf865465c7d4e31fad6cdade5ade296b4b9867553826270c07e55e3b75e17d72d0812e997072339897cc1ff8ee6b2d1e48250c8cf1c88720824153401efb45792e93e39d5f4f2a93c82f611fc337dec7b375cfd735dc68de33d27550a8d2fd9273ff002ce620027d9ba1fd0fb50074545145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450015e57e3fd77fb4b53fb0c0f9b5b4620e09c3c9d09fc3a0fc7d6bb3f19eb9fd8ba3b794d8bab8cc70ff00b3eadf80fd48af20a007451bcd2a451233c8ec1555464927a015ed3e1ad1d744d1e2b418329f9e661fc4e7afe1d07e15c57c38d0fed376faadc20314076c20f77ee7f01fa9f6af4aa0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0029aeeb1a33b9c2a82493d853aa96b2acfa2df2c60976b7902e3d769a00f0f96469a57964397762cc4f726bd2fe18c78d06e5f9cb5c91f92aff8d798d7aef806344f095a328c191a466f73bc8fe4050074745145001552fb4db1d463d97b6b14e30402ea095cfa1ea3f0ab7450070dabfc39b797749a55c181ba88a5cb27d03751f8e6b86d5346d434897cbbeb678b3f75baab7d08e2bdcaa39e08ae616867892589c6191d4107f0a00f1cd1fc53ab68fb560b832423fe58cdf32fe1dc7e18af42d03c69a7eb0c904bfe8976c7023739573fecb77edc1c75ef585e24f00140f75a2648032d6cc727fe024ff23f9f6ae0dd1a3764914a3a9219586083e86803dfa8af33f0af8e24b1d967ab334b6d9c24dd5a31efea3f51efc0af498a48e6896589d5e371956539047a83400fa28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a6c922451b49230444059998e0003a934eae23e22ebbf66b55d26ddf12ce374c41fba9d87e3fc87bd00719e27d65b5cd625bae442bf242a7b20e9f89ebf8d52d3ac66d4afe1b3b7199666da3d07a93ec07355abd27e1ce87f67b46d56e1312ce36c208fba9dcfe27f41ef401d769d630e9b610d9db8c450aed19ea7d49f7279fc6acd145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145007866b56074cd5eeac88388a42173d4af553f88c57a8f80995bc236401c953203ec77b1feb5c87c4ab4f275f8ee15085b88412deaca483fa6dad6f85f7cad6b79a7b10191c4ca33c90460fe0303f3a00eee8a28a0028a28a0028a28a002b9bf147846db5c56b884ac17c071263e57c740dfe3d7ebd2ba4a2803c1efacae74fba7b6bc85a2993aab7f31ea3deb5bc37e29bcd065d833359b1f9a163d3dd4f63fa1af4fd6f42b2d72d7c9bb8fe65ff572af0c87d8fa7b57966bfe18d4343909993cdb6cfcb3a0f94fd7fba7ebfad007ad697a9da6af64b75652078db823ba9f423b1ab95e1fa36b379a2ddfda2ce4c678746e55c7a115eb5e1ed7ed75fb2f3a0fddca9c4b0939287fa8f43401ad451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450053d5750874ad3a7bdb83f244b9c7763d80fa9e2bc4f50bd9b51be9af2e5b74b336e6f41e807b01c0aea3e216bbf6fd4069d0366ded58ef23f8a4e87f2e9f5cd71f401ade19d19f5cd622b5e442bf3ccc3b20ebf89e9f8d7b4471a451ac71aaa2200aaaa30001d00ac1f06685fd8ba3af9ab8bbb8c3cdede8bf803f9935d05001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014515cf6a9e33d1f4bb868249649e54386581776d3e99240a00e868acad1bc45a6eb5b96ce7fdea8c989c6d603d71dff000ad5a0028a28a00e5be20e92750d0bed112e66b3264f729fc43f91ff0080d79d787f557d1b5882f172514ed9147f121ea3fafd40af6d2010411907b578ef8bf433a26b0e91affa2cdfbc84fa0eebf87f2c5007b0452a4d0a4b13078dd432b0e841e869f5c27c39d7c490ff0063dcb7ef132d6e49fbcbd4afe1d7e99f4aeee800a28a2800a28a2800a28a2800a6baaba323a865618208c8229d4500703e2ef042794f7fa347b4ae5a4b65e847729fe1f97a1e2b47d52e747d423bcb56c32f0ca7a3af753ed5ee75e6fe3df0bfd9a47d5ac53f72e733c607dc3fde1ec7bfa1faf001dde91aa5b6b1a7c7796ad947e0a9ea8ddc1f7abb5e33e17f104da06a024f99eda4c09a31dc7a8f715ec16b730de5b47716d2092191772b0ee28026a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800ac0f18ebbfd89a43189b1773e521f6f56fc3f9915b934a9042f34ac1238d4b331e800e49af17f126b2fae6af25d364443e4850ff000a0e9f8f7fc6803289c9c9eb5d6fc3fd07fb4751fb7dc266dad581507a3c9d40fc3afe55cd69f6536a37d0d9db2ee9666dabe83d49f6039af6cd274e8749d3a1b2b71f244b827bb1ee4fd4d005ca28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a280327c53792d87872fae202448a98561d464819fc335e2bd4e4d7bc5f5a457f6535a4e098e6428d8ebcfa578cebba2dd6877cd6d72b953cc7201c48bea3fa8a00a56d7335a5c47716f23472c672aebd41af59f09789e2d7adbcb976c77d10cc883a30fef0f6fe55e435359ddcf63771dd5ac8639a26dcac3fcf4f6a00f7aa2b33c3facc3ae6971ddc5f2bfdd953fb8e3a8fa771ec6b4e802b5fdf5b69b68f7577288e14ea4ff00203b9af3ef1278af48d7ec5ad5adae22743ba19d954ed6f700f43dfafae0e2ac7c51b97f32c2d46e11e1a43cf0c7803f2e7f3ae06801f0cb241324b1394911832b29e411d0d7aef84bc491ebd67b642a97b10fdea7f787f787b7f2fcb3e3f5674fbeb8d36f62bbb57d92c6720f63ea0fb5007bbd1597e1fd72df5dd3d6e203b645e25889e51bfc3d0d6a50014514500145145001451450014d74591192450e8c0865619047a1a751401e43e31f0db6877de640ac6c673fbb6ebb0ff709fe5ea3e86b43e1ff00885acaf469772e4db5c362227f8243dbe87f9e3debd0b56d360d5b4e9acae07c920e1bba9ec47d0d789df5a4da75f4d6b70bb6685f69ff0011edde803de28ac5f09eb1fdb5a1c33bb6674fddcdfef0eff88c1fc6b6a800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28aced7b531a468d737c543346bf229eec4e07e193401c8fc46d7b0a346b66e4e1ee08f4eaabfd4fe1ef5e7d524f349713c93cce5e4918b331ea49ea6b6bc1fa09d735602553f64830f31f5f45fc7f966803aef879a0fd8eccea972989ee171103fc31fafe3fcb1eb5da5200140000007000ed4b4005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400550d6348b5d66c5ad6ed32a79571f790fa8abf450078a6bfe1fbcd06ecc570bbe163fbb9d47cae3fa1f6fe9cd6557bd5d5b41796cf6f7312cb0c830c8c320d79bf8b3c12da644f7da69696d54e6488f2d10f507b8fd47bf268031bc31afcba06a1e705325bc802cd183d47a8f71fd4fad7afd8dedbea1691dd5a482486419561fcbd8d783d6df863c4771a05e64664b490fef62fea3dff009d0076ff0011b4a6bdd192f63fbf64492bea8d807f2c03f4cd796d7bac52da6b1a66e8dc4d6b73195241eaa4608f63d4578aeaba7cba5ea53d94ff007e26c67fbc3b1fc460d005ad03429f5f9e582d678639634dfb65246e19c1e80f4c8fce99ad6857fa1ce23bd88056fb92a1ca3fd0ff0043cd58f086a1fd9de24b394b111c8de53f38186e39f60707f0af5ebdb2b6d42d5edaee2596171cab7f9e0fbd0078c685acdce87a82dd5b9c8e92464f0ebe87fc6bd8f4ad4ed757b04bcb37dd1b7041ea87b823b1af29f1578667d02e8329696ca53fba948e41feeb7bff003fcc083c37afdc6817fe7479781f0268b3c30ff11da803da68aaf617b6fa8d9c7756b20922906411fc8fbd58a0028a28a0028a28a0028a28a002b80f895a36562d5e15e46229f03fef96fe9f9577f55b50b28b51b09ece7198e642a7dbd08f71d6803cc7e1feaffd9fad8b591b10de6233ecff00c27f523f1af57af05b9825b1bd96093e59a090a9c1e8c0f635ed1e1ed4c6afa25b5e1c6f75c4807671c1fd467e868034a8a28a008ae6e23b5b696e266db144a5d8fa015e33ab788751d56f5e792e658d49f92247215076031fceba5f889e20f3a5fec7b57fddc6435c303f79bb2fe1d4fbe3d2b85a00f49f87be20b9d43ced3af64699e24f323918e4edc80413dfa8aedebcabe1bac8de2562870ab6ee5fdc640fe78af55a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002a86b9a626afa4dc58bb6cf357e56feeb03907f3157e8a00f23ff84175efb5f93f664db9c79de62ecc7afafe99af49f0fe8d0e87a5c76911dcdf7a47fefb1ea7fa7e15a7450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500149d460d2d1401e6de34f07fd8fccd4b4b8ffd1bef4d0aff00cb3f565ff67d476fa74e22bdfebcff00c59e073992fb468ffda7b551f994ff00e27f2f4a00c1f08f8924d0af7cb9599ac663fbd4ebb4ff007c7bfafa8fc2bbdf137872dbc496293c0e8b72a998671caba9e40247553d8f6cfd73e4241048230476aee3e1ff00897ecf2ae9178dfb991bf70e4fdc63fc3f43dbdfebc00719756d359dcc96f71198e689b6b29ec6bd5fc17e211ad69be5cee3edb6e02c83bb8ecdfe3eff005159bf10bc3a6eedff00b5ad133342b89940e5d07f17d47f2fa5703a4ea773a45fc77968fb644ea0f471dc11e9401ed97d6706a1672da5d2078655c303fcfeb5e3be23d06e341bf304b9785f261971c38ff11dc57aa683e20b2d76d4496ce16603f79031f990ff0051efff00eaa9f59d2adb59d3ded2e97e56e5587546ec4500795f85fc4b3e81747832da487f7b167ff1e5f7fe7fa8f5bb1bdb7d42d23bab494490c832187f23e86bc5758d2ae747d424b3ba5c32f2add9d7b30f6ab1e1ef105de8377e64077c2c7f7b093c38fe87de803daa8aa9a5ea56daad8c77766fbe37ec7aa9ee08ec6add00145145001451450014514500796fc49b1fb3ebc974aa42dd440927a165e0fe9b6b63e175cb3d95fda90364722480fbb020ff00e8029bf14a226db4e9bb23bafe601ffd96b3fe184846b3771766b7dc7f061fe3401e9958de29d6d743d2249c106e1fe4854f763dfe83afe9deb6188552cc40006493dabc77c5fae1d7357678d89b5872900e704776c7bff2c7a500623bb49234923167624b331c924f734dab12db186ce099f869c928bdf6838cfd09c8ff00809aaf401e95f0cb4ff274cb8bf75f9e77d8848fe15ee3ea49fcabb6aa1a1d8ff6668d6967801a28c07c1c8ddd5bf526afd001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451505d5ddb594266bb9e3823071ba460a33e9cf7a009e8acdb0d7b4ad465f2acefa2964ec99c13f407ad69500145145001451450014514500145145001515d5c25a5acd732e7cb851a46c0e70064ff2a96b3f5eb77bbd0afe0883191e070a17a938e07e3d2803cb754f17eb1a85d34a9772dac59f92285ca851ee475fc6b77c25e379125167ad4fba36e23b86eaa7d18fa7bf6efc74e128a00f7e8dd258d648d95d1865594e411ea0d3abc5f42f136a3a1be2de4f320272d049ca9fa7a1fa7eb5ea3a07886cb5eb6df6edb2651fbc818fcc9fe23dff0095006bd145140051451401ccf89bc1d6bad06b8b72b6d7b8fbe07cb27fbc3faf5fad797ea1a7dd69976d6d7b0b452af383dc7a83dc57bb565788341b5d7ac4c17036c8bcc5301f321fea3d47ff0058d00739e0cf188bad9a6eab262e3eec53b1ff0059fecb7fb5efdfebd62f14f8104864bdd15406272f6bd01f529e9f4fcbd2b88d574bbad1ef9ed2f23d922f208fbae3b107b8aedfc19e321208f4dd5a4f9feec370c7ef7a2b1f5f7ff240384866bad36f049134b6d731363bab29ee08fe95df681f1061942c1acaf94fd3ed0832a7ea3b7e1fa5741aff00862c35d8c9993cab9030b3a0f987d7d47f9e2bcdb5bf096a9a3967684dc5b0e44d10c803dc751fcbde803d235dd26cbc4fa4811cb13b0cb5bdc210c14fd47638c1ff00eb57905e5a4f63772dadcc6639a26daca7b54961a8de69b379b6573240fc676370d8f51d0fe356b58d6e5d69227bd863fb5c636f9f18dbbd7d1874cfb8c77e3d002c7857c432e81a86e6cbda4b813463d3fbc3dc7eb5ec104f15cc093c0e248a450cac3a106bc0ebb1f01f894e9f72ba65e49fe89337eed9bfe5939fe87f9f3eb401e9f4514500145145001451450071df1388fec0b71c67ed4b8ffbe1eb9bf86f3ac3e2528dd6681d07d721bf929ad8f8a52910e9d083c3348c47d3681fccd71de1ed49748d621be742e2257c28ee4a301fa91401dcfc42f107d92d3fb2ad9ff7f3afef883f713d3ea7f97d6b88f0e68d26b9ab4768b9118f9a571fc283afe3d87b9aa7713dcea77ef349ba5b8b87ce00c9627a003f402bd3b4eb04f07784ae6e64dbf6bf2f7c8c7905fa22fd01207e24d00705e2cb98ee35f9e3806db7b6c5bc29d95538c0f6ce4fe34be10d3ffb47c49671104c71bf9afc646179e7d89c0fc6b16bd0be17e9e04579a8b019622043dc0182dffb2fe5401df514514005145140051451400514514005145140051451400514514005145140051451400514571be3af13cfa498ec2c1825c489bde4c64a2f418f7383fe4d007654578a41e24d6a0b8599753ba66539daf2b329fa82715ebfa4dfaea7a5db5ea2ed132062be87b8fcf3401728a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002bc63c57abcdab6b770eee4c313b470a76550719fa9c64d7b3d786eb712c1ae5fc4a0854b891573e818e2802923b46eae8c5594e4303820fad7a0786fc7c9e5a5aeb590c385b9519047fb43fa8ff00ebd79f55bbbd32f6ca3492e6d658e390029215f958119186e878a00f728a58e78965864592361957439047b114faf10d1f5dd43459b7d94e5509cb44dca37d47f5eb5e97e1ef19586b3b20948b5bc3c796e7873fec9eff004ebf5a00e928a28a0028a28a0028a28a0028a28a00f1df1ae9a34df125c2a2ed8a7fdf20f66ebf4f983560d7a87c46d25af34a8efa142d2da13bc01d633d4fe0403ec335e5f401dbdff83a3d4f498356d0f0ad3461ded776541c7cc109f439e0ff00f5ab90b79eef4cbd12c2f25bdcc2df42a7b823f9835e93f0dafbed1a0c96acc0b5aca40503a2b723f5dd577c4fe13b5d7636993105f01f2ca070decdebf5ea3f4a00ade17f1a5beadb2d6fb6dbdef0179c24a7dbd0fb7e55d5d784ea3a75d69776d6d7b13452af383d08f507b8aee3c19e32691e3d3755932c70b0cec7927b2b1fe46803bea28a2800a28a2803335ed0ed75db136f7236bae4c5281f3467fc3d477af20d6349bbd1af5ad6f130c39561f75c7a835ee559fad68f69ad591b6bb4cf7471f790fa8a00e2bc1be33f27cbd3b5797f77f762b863f73d031f4f7eddf8e9e895e25aee8977a1de9b7ba5cab64c7281f2c83d47bfa8ae87c1de326d3f6586a6e5ad7a4729e4c5ec7d57f97d2803b4d57c2da46abb9a7b45499b27ce87e46c9ee71c13f506b8bd5fe1e5f5b6e934d956ee3ff9e6df2b8fe87f4fa57a5a32ba2ba3065619041c8229d401e053432dbcad14f1bc5229c323a9047e06995ee1abe8b61acc062bd8039c61641c3a7d0ff4e95e39ac6993e8fa94d65703e643f2b63875ecc3eb401ea5e0ad64eb1a2279cfbae6dff772e4e49f46fc477f506ba1af09d3f51bcd32e45c58cef0c9d095e847a11d08fad777a3fc45864db1eaf6e626e9e74232bf52bd47e19a00eee8aa761aad86a481acaee19b8ced56f980f71d47e3572800a2a39e78ade232cf2a451af57760a07e26b9cd5fc6fa4595bcab6b73f69b90a422c4bb97763825ba633e84d00713e3cd446a1e249950831db010291dc8ceefd491f857394acc598b312589c927bd6d7853417d7b5411b645ac5869dc7a7651ee7fc4f6a00e8fe1df877711acdda70322dd4faf42ff00d07e3ed56fe26ea1e569f6d608d869dcc8f86fe15e808f7273ff0001aed238d228d638d422200aaaa30001d00af22f1c5ffdbfc4f738394b7c40b918c6debff8f16a00e7ebdbfc3fa77f656876966461e38fe7e73f31e5bf526bcbfc11a6ff00697892df70cc56ff00bf7ff80f4ffc7b1f866bd86800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800af11f116a1fda9aede5da9ca3c8421f551c2fe8057a778cf5b4d1f4691558fda6e54c710070471cb7e19fcf15e3f40057b6f86ec5b4ef0fd95ab821d2305c1ea18fcc47e64d79378634f1a9f882ced9802864dce08e0aaf247e2063f1af6ca0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002bcafe22e98f69aefdb157f7376a0820600703047f23f8d7aa551d634ab7d674f92cee97e56e5587546ec47bd00786d7b7e8403f8774e0e0306b488107907e415e37aae9f3697a94f653e37c2d8c8e8c3a83f88c1af4ff00016ac351d0120723ceb3c44c3fd9fe13f971f81a00835ff0258ea0ad2e9c12cee7d00fddb7d40e9f51f91af37d4b4cbcd2ae8dbdec0d1483919e8c3d41ee2bdd6aa6a5a6da6ab6a6daf6159633c8cf553ea0f63401e73e18f1c5c69ec96ba9b3dc5a74127578ff00c47b75f4e98af4bb5b986f2dd2e2da559627195753906bca7c4fe11bad119a78775c58e78931f327a061fd7a7d3a555f0e7892f3409ff747cdb673992063c1f71e87de803d9a8aa1a46af67acd98b9b29372ff00121e190fa1157e800a28a2800a28a2801ae8b22323a8656182a46411e95e49e30f0cbe877666814b584adfbb6ebb0ff74ff4f5af5daaf7f650ea36335a5caee8a55dac3fa8f71d6803cbbc01abae9bae793336d82ec08c9cf01f3f293fa8fc6bd66bc2b55d3e6d2b529ecae3efc4d8c8e8c3b11f51cd7a87827c40359d30433be6f2d8059327975ecdfe3eff0051401a5af68769aed91b7b95c3ae4c5281f3467dbdbd477af1fd5f4abad1efded2ed36baf2ac3a3af623dabdceb23c47a0dbebda79825c24c9930cb8e50ff81ee280303c0fe2cfb622699a8c9fe92a310cac7fd60f43fed7f3faf5edabc1eeed6e74cbe7b79d5a2b885b9ec41ec41fd41af4ff000678a46b36ff0064bc602fe21c9e8255fef0f7f51f8fd003a9a28a2800a28a28029eaba5daeaf64f69791ef8db90470c87b107b1af21f10f87eef41bcf2a71be163fba980e1c7f43ed5ed555750b0b6d4acded6f221244fd41ea0fa8f43401e69e0ff1749a43ad9df334960c783d4c27d47a8f51f88f43ea51c89346b244eaf1b80caca72083d0835e3de27f0d5ce8173ce65b490feea6c7fe3a7d0ff3fcf16fc21e2c9745945add1692c1cf4ea6227b8f6f51fe4807acd72de3bd07fb574cfb55ba66eed416181cba775fea3f1f5ae9619a3b8852685d648dc6e5653904549401f3fd6a58f87b52d474f6bdb183cf891ca30461b81001e87af51d33573c6da42691af489000209d7ce451fc392723f307f0c56dfc31d4238ee2ef4f91b0d2812443b1233b87d718fc8d007133413da4de5cf14904abfc2ea5587e06ada6bdac22ed5d52f0281803cf6e3f5af6b9ede1b988c5710c73467aac8a181fc0d654fe12d0677defa6c40ffb04a0fc9481401e3f737571772799753cb3c9d3748e58fe66927b59edb67da219222ebb943a95247a8cf6f7af5ebd4d17c2da73df258dbc4d18db1ed501dd8f450dd7ff00ad9af26d4afee354bf96f2e9b74b29c9c7403b01ec2801965673dfde456b6c85e695b6a81fe7a57b3e83a3c3a26991d9c27711f348f8fbec7a9ff3dab1bc0fe1afec8b3fb65d2117b3afdd23fd52ff0077ea7a9fcbb73d5d0054d52f574ed32e6f1b0443197009c64e381f89c0af0b666772ce4b331c927a935e9df12aff00ecfa24566a70d7527231d55793fa95af3cd22c1f54d52daca3c833385240ced1d49fc064d007a3fc3ad2cd9688d7722912de36ee73f7070bfd4fd08aeb6a38628e08638625091c6a15547600600a92800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a86eae61b3b596e6e1c2451296663d80a9abcd3e217887ed572749b573e4c2dfbf20fdf71fc3f41fcfe9401ce6bfac4dadea925dcbf2a9f9634fee20e83fa9f7359b451401dff00c2fd3fe6bcd4581c0020439fa3371ff7cd7a0d64f85f4dfecad02d2d9976cbb37c99183b9b920fd338fc2b5a800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a280383f897a46f861d5a25e63fdd4d8f43f74fe7c7e22b96f08eb3fd8badc72c8d8b697f7737b29eff81e7e99af5fbbb686f6d65b6b840f14aa5587b1af14d6f4a9b46d4e6b29f2761ca3e301d7b30ff3d722803dc01046472296b8ef87daf7dbac3fb36e1bfd22d57e43fdf8fb7e5d3e98f7aec6801aeaae8c8ea195860823208af3bf16f821a02f7da3c65a2eb25b8e4afbafa8f6eddbdbd1a8a00f0bd2f54bbd22f16eaca52920e08ecc3d08ee2bd67c35e25b5d7ed8ecc457518cc9093c8f71ea2b17c5fe0b5bedf7fa5a04bafbd2423812fb8f46fe7f5ebe796d7173a75eacd03bc1710b707182a475047e841a00f78a2b03c2de26835fb5dadb62bd8c7ef62f5ff697dbf97e44efd001451450014514500725e3df0f9d4ec05f5b216bbb65e547574ea47d4751f8d79ce8faa5c68fa8c5796cdf321f9973c3af753f5af73af33f1cf857ec3236a7a7c7fe8ae732c6a38898f71fec9fd3f90077fa4ea76dabd8477968fb91fa83d51bba9f71576bc67c31e219b40bff00300325b49813459ea3d47b8af5fb3bb82fad63b9b5916486419561de8030bc63e1a4d72cbcd81545f423f767a6f1fdd27f97a1fa9af2a826b9d3ef566899e1b881f20e30548ec47f4af79ae03e21786f706d66ca3e47fc7caafa767c7f3fcfd4d00753e1bd6e1d774c4b94c2ccbf2cd18fe16ff03dab5abc5fc31ae49a16aa93e58dbbfcb3a0fe25f5fa8ea3f2ef5ecb14a93c492c4e1e3750cac0e4107a1a007d14514005145140105e5a417d6b25b5d44b2c320c32b77af25f14f8627d02e772ee96ca43fbb971d3fd96f7fe7f981ec350dd5ac17b6d25b5cc6b2c320c3237422803cafc21e2b9344985b5d1692c1cf23a988fa8f6f51fe4fab43347710a4d0bac91b8dcaca7208af23f15f8626d06e77a6e96ca43fbb97ba9feeb7bff003fcc09fc21e2b9345985add167b073cf7311f51edea3fc900b5f138ffc4fed87fd3a8ffd0dab96b0bc9b4fbe86eedce2585c32fbfb1f63d2ba1f889770ddf8823681d5d12d906e5390724b023f0615ce9b49858adeedfdc34a62dde8c00383f81fd0d007b7e9b7d16a5a7c17901cc732061edea3ea0e454f2489146d248c11101666638000ea4d79c7c39d73ecd74da55c36229cee8493d1fb8fc47ea3deac7c42f11ee63a359bfca39b961dcf64fea7f2f5a00e7bc59e207d7b522c995b4872b0afa8eec7dcff00856d7c3ff0dfda655d5ef13f731b7ee108fbec3f8be83f9fd2b0fc2ba049af6a623395b58b0d3b8ec3b01ee7fc4d7b14514704491448123450aaaa30001d05003e8a2a0bdba8ecaca7ba9b3e5c285db1d70066803cbbe215ff00db3c46d0a9ca5aa08c60e46eea4feb8fc2b5fe196979373aa483a7ee62fd0b1fe43f3ae1a7965bdbc9267cbcd3c858e075627fc4d7b668ba7ae97a45ad92e331200c4776eac7f326802f5145140051451400514514005145140051451400514514005145140051451400514555d4afe0d32c25bcb96db144b93ea4f603dcd0062f8d3c42345d3bca81c7db6e0111faa0eedfe1eff435e55676b2df5da411637b924b37450392c7d80c93536b1a9cfabea52dedc1f99cf0a3a22f602bb2b3d1bfe11ef04ea37f731ffa6dd41e59561831a390a07d79c9fc076a00e02b6fc21a5ff6af886de2650d0c47ce972011b57b11e84e07e35895ea3f0e74afb1e8ef7d2a912dd9cae7b20e9f99c9fa62803afa28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800ae77c67e1f5d6f4c2f0a0fb6db82d11eec3bafe3dbdff001ae8a8a00f07b1bc9f4dbe8aeadd8a4d0b6467f507dbb57b5e8fa9c1abe9b15edb9f9641f32e7946ee0fd2b82f885e1efb2dc7f6b5aa7ee666c4c00e15fd7e87f9fd6b3fc11e213a3ea3f67b87c595c901f2788dbb37f43edf4a00f5aa28a2800ae4fc61e114d5d1af2c42a5fa8e474130f43e87d0fe07d47594500783c135d6997cb244cf6f73037a60a91d411fd2bd63c2be27835fb7d8fb62bd8c7ef22cf047f797dbf97eb557c61e134d66237766aa97e83e82503b1f7f43f81f6f308a5bad36f43c6d25bdcc0ff46523820ff85007bcd15cf7853c5106bd6fe5c9b62be8c7cf1f661fde5f6f6ed5d0d001451450014d74591191d43230c32b0c823d0d3a8a00f29f197851b4698de59ab358487a753093d8fb7a1fc0fbd6f09789a5d0aefcb94b3d8ca7f789fdd3fde1effcff002af5b9a18ee2178664578dc1565619041af24f16f86a5d06f37c40bd8ca7f74ffdd3fdd3effcff003a00f5b8268ee2049a170f1c8a19597a1069ce8b22323a86461865619047a1af2af06f8adb46985a5e33358487af5309f51edea3f11efeaa8e9246b246cae8c0156539041ee0d0078ff8bb406d0b542b1826d26cb42de83bafd47f2c5741f0efc4214ff63ddc9804e6d98faf75fea3f1f6aec35ed221d6f4b96ce5c027e68df1f71c743fe7b135e337105c69b7cf0ca1a1b881f07070548ee0feb9a00f78a2b0bc25afaebba5877205d43859d7dfb30f63fe35bb40051451400514514010dd5b43796d25bdcc6b24320c32b7435e49e2af0ccda0dd6f4dd259487f7727a7fb2deffcebd86b2bc4f2c10f872fe4ba8d648c4446d619058f0bfa91401e295e95e1ad023d43c02d6d2f0d76ef32311f71c1daa7ff001d1f8135e6b5d4f843c5cfa2936b79be5b16c90072d11f6f63dc7e3eb900e6d967b2bb2a7745710498e0e0a329fe608a92d2dae753bf4b78434b713be064e4927a927f5269fabea0daa6a9717ae8b1999b76d5e80741fa77ef5d6fc2e5b63797c5c0fb4845f2c9ebb727763f1db401db685a441a26991d9c1c91cc8f8c176ee7fcf6ad1a28a002b8ef893a8fd9b468ec90e1eedfe6ff0071793faedfd6bb1af20f1cea5fda3e249c29cc76dfb85fc33bbff1e27f0c5002f8134efb7f892066198edbf7edf51f77ff001e23f235ebd5c7fc36d3becda2c97ac3e7bb7e39fe05c81faeefd2bb0a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002bc9fc71e23fed8befb2db3e6caddbe520f123742df4ec3ffaf5d2f8ff00c47f62b53a5da3ff00a4cebfbd61d6343dbea7f97d45707a0e9136b7aa45670f00fcd23e33b10753fe7b91401bfe00f0efdbef3fb4ae93fd1addbf760ff1b8fe83f9e3deba0f89773e56830c01b0d34e323d54024febb6ba9b2b486c6d22b5b6409144bb540ff3d6bcdfe25dff00da35986c94e56d63c9e3a33727f40b401cde8ba73eadab5b594648f35f0cc3f854724fe0335ee10c51c10a4312848e350aaa3a003802b84f861a70f2eef5271f313e4467d0705bff0065fc8d77d40051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514010dddac37b692db5c2078a552aca7d2bc5b5dd226d175496ce6f982fcd1be31bd0f43fe7b835edf581e2ff0f8d774cc4407dae0cb427a67d54fd7f9e28032fe1ff8885eda8d2eedc7da605fdd13ff002d1076fa8fe5f435d9d7835bcf73a75ea4d133c37103e46460ab0ec41fe46bd93c3badc3aee989731e1655f9668ffb8dfe07b5006ad1451400572be31f09a6b3135e59aaa5fa0e9d04c0763efe87f03edd551401e0d0cd73a75eacb133c17303f07186561d411fd2bd6bc2be25875fb4c36d8ef621fbd8bd7fda5f6fe5f91343c69e131aac6d7f62805f20f9947fcb603ff66f4fcbd2bcd6ceeee74dbd8ee6ddda29e26c83e9ea0ff2c5007bc51591e1bd7edf5eb01347849d30268b3ca1ff0003dab5e800a28a2800aaf7d6706a16925add462486418653fe7ad58a2803c63c4be1fb8d06f8c6db9ed9ce61971f787a1f715a5e0ff173e8eeb677c59ec18f07a984fa8f51ea3f11e87d2f51d3edb53b37b5bc884913f63d41f507b1af27f13785eeb4198bf3359b1f92603a7b37a1fe7fa000f5f8658e78965864592371956539047b1ae37e21787bed76bfdab6a83cf817f7c00e5d3d7ea3f97d05723e1bf145e6832ed5fdf5a31cbc0c7f553d8ff3af54d2757b2d6ad04f672875fe243c321f461401e41a06af2e89aac579165947cb227f7d0f51fd47b815ed36f3c5756f1dc40e1e29543a30ee0f4af23f18e87fd8bac3089716b3e6487d07aafe07f422ba5f86bacef8a5d2266e53324193dbf897f3e7f13401de51451400514514005711f13750f2b4eb6b046c34efe63807f857b1fa923fef9aedebc73c69a8ff00697892e595b31c27c98fe8bd7f5c9fc68026f03e8916b3aac82e90b5b43112e3d49e00cf63d4ff00c06b1b54b782d352b8b7b69c4f0c72154907f10ff3deb72db52fec2f09986ddb6dfea64b338eb1c23e51f8921b1ec7e958fa36973eb3a945676e3973967c6422f763fe7da80288c739fc2ba4f00de7d93c5102960ab70ad1313ee323f502b2f5bd22e344d45ed2e4671ca381c3af622ab595cb59df5bdd20cb4322c801f5073401ef3451450050d73501a568d757a71ba28cecc8c82c785fd48af13b7865bcbb8a08fe69669022e4f56271fd6bbcf89fa9612d34c43c9fdfc9f4e42ffecdf90acbf871a6fdaf5d6bb7198ecd3776fbedc0fd371fc05007a6595ac7656505ac3feae1408b9ea4018a9e8a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800acbf10eb30e87a5c9752fcd21f9628ff00befd87d3b9ad19a58e085e695c2471a96663d001d4d78df8a75e935ed51a6f996da3cac1193d17d7ea7bfe03b50066cf35c6a37af2ca5a6b89df27032598f6007f2af5bf08e80ba169616400ddcd8699bd0f651ec3f9e6b9bf879e1cdc46b378830322d94faf42ff00d07e27d2bd0a80109001248007526bc2f55bd6d4754babc6cfefa466009ce06781f80c0af5af195e8b1f0c5ebe577c89e4a827a96e0e3df049fc2bca344b1fed2d66cecf6b32cb2a870bd76f563f966803d73c2d63fd9fe1cb280821fcb0ef9183b9be620fd338fc2b5a8a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a280380f881e19dc1f58b18f91ff1f28bff00a1e3f9fe7ea6b93f0e6b93685a9adcc79689be59a3fefaff0088ed5ed4ca194ab00411820f7af26f1a7868e8b79f68b653f619dbe5ff00a66dfddff0ff00eb5007aa5a5d437b6b1dcdb48248655dcac3b8a9abcb3c0be26fecbba1617920165337cacdff002c9bd7e87bfe7eb5ea7400514514005711e37f08fdb03ea7a6c7fe9006668547fac1fde1fed7b77faf5ede8a00f0cd2754b9d1efd2f2d1f6baf054f475eea7dabd8b43d62db5bb05bab66c1e92464f28de86b90f1cf84c624d5b4e8ffdab88947e6e3fafe7eb5c8e83addce857eb736e7729e24889e245f4fafa1a00f6ea2a9e95a9daead631ddd9bee8dfa83d54f7523b1ab94005145140053258a39a268a54592371865619047a114fa2803ce7c4de037877dde8aa648fab5b672cbfeefafd3afd6b8fb1bebbd2eec4f692bc33271c77f623b8f635eed5ce7893c2165ad869a3c5bdee3895470ffef0eff5ebfca80399bef10d9f8ab427b4bc54b5d4a2fde4273f248c07201ed919183df1c9ae4f4bbf974cd4adef61fbf0b86c7a8ee3f119152eafa2dfe8d3f957d01404fcb20e51fe87fc9acfa00f7bb69e3bab68ae216dd1ca81d4fa82322a5af3ff0002f8a6ced74f1a6ea5702128ff00b9771f2953ce09ed839e4fad77b1c89346b244eae8c32aca7208f63400fa28a28032bc4daa0d1f43b9ba0c04bb7645d3973c0ebd71d7e80d78a5763f11b58fb66a69a742f986d3efe0f0643d7f21c7d49ae3a802496579e5dce4b310140c938006001ec0003f0af59f067878689a6f993a8fb6dc00d29fee0ecbf877f7fa0ae63e1ef87bed572356ba4fdcc2dfb8047df7fef7d07f3fa57a550064789343b6d734d68672b1c9182d14c7fe599ff0f5ff00eb578b57b6f88ee05af87b5098b15c40caa47f788c0fd48af16b6824bab98ade15dd2cae110671924e05007aa782bc4abacd98b5b9602fa0501b9ff58bfdefafafff005eba7af08b79eeb4cbf59622f05cc0fdc60a91c1047e8457a16a5e2f82f7c153dc44425dcb8b678baec660727e854310680386f10ea3fdadadddde039477c47c63e41c2fe8057a5f80b4dfecff000e4523ae25ba3e7374ce0fddfc3183f89af31d174f6d5357b5b25ce25900623a85eac7f000d7b822aa204450aaa300018005003a8a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a2b9bf19f888689a7f956ee3edb382231fdc1ddff00c3dfe868039df885e22f3e43a3da3feee339b8607ef30fe1fc3bfbfd2b0bc25e1f7d7b530af91690e1a661e9d947b9ff001acbb1b3b8d4efa2b5b752f34cd819fd49f6ef5ecfa1e93068ba64567073b7977c60bb7727fcf4c5005d8e348a358e35544401555460003a014fa28a00f3ff008a17ff00f1e5a729f59dc63eaabcff00df5543e1a5879faccd78ca0adac78539e8cdc0fd03561789b51fed4d7eeee95b3197db191d36af00fe38cfe35e8fe02d3bec1e1b8a475c49744ccd91d8f0bf86003f8d0074b451450014514500145145001451450014514500145145001451450014514500155350d4acb4c844b7d731c087a6e3c9fa0ea6b0fc55e2f8344ff46b6559ef88c9527e58c762defedfcb8cf975f5fdd6a372d717b3bcd2b7f131e83d00ec3d85007b3699af697ab314b1bc495c0c94c156c7d0806b4abc1f4fb89ad2fe0b8b6244d1b8298ee7d2bde2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800aaf7f6506a1672da5d2078a55c30fea3deac51401e23af68d3e87a93da4df32fde8e4c70ebd8d76fe00f12fdae25d26f5f33c6bfb8727efa8fe1fa8fe5f4adff1368516bda6180ed5b84f9a1908fbade87d8f7ffeb578f48973a75e9470f05cc0feb82ac0d007bcd1583e12f1147af69ff390b79080264f5ff687b1fd3f9ef5001451450015e69e38f09fd899f53d3a3c5b31ccb128ff00567d47fb3fcbe9d3d2e9aeaae8c8ea195860823208a00f19f0debf71a05f8963cbc0f813459fbc3d7ea3b57b058de41a85a47756b209219065587f2fad79878cfc2ada34e6f2cd4b584add3a989bd0fb7a1fc0fbd6f0978965d06f364859eca53fbd8ffba7fbc3dff98fc3001ec145470cd1dc429342eaf1ba8656539041ef52500145145001451450045716f0dd42d0dc4492c4dd51d4303f81ae17c53e06b4b7b09eff004b3246615ded013b94a8eb827918193debbfa460194ab00411820f7a00f00abda66b1a869326eb1ba922cf2541ca9faa9e0d49e21d30e91addcd9f3b11b319f543c8fd38fa835d5786b44d2bc4fe1fc4d1f917d6c7cb696120123aa92bd0f5c67a9da79a00b5a3fc458252b1ead6fe4b7fcf68412bf8af51f866ba1d5bc436967a04ba9db4f14e31b62dac08673d07f523ae01af3cd6fc15aa695ba4893ed96e39f3221f301eebd47e1915cde4e319e3d2801d248f2c8d248c5ddc9666272493d4d68f87f469b5cd523b48be54fbd2bff710753f5ec3dcd6657a4781f51d034dd3043f6f8d2f26f9e7328d9cf6504f181f5ee7d6803b2b5b686ceda3b6b7409144a15547602a6a647224b1ac913aba372194e41fc6a3bcbcb6b0b66b8bb9921897ab31c7e03d4fb500733f122efc8f0e881586eb99954a9ea547cc4fe617f3ae37c076bf6af14da929b9210d2b7b60707fefa2299e2ff107f6fea2ad102b6b002b12b0e4e7ab1fae07e42ba5f863a6324575a9c8a4093f73113dc03963f4ce07e068027f1ef863ed7136ab631fefe3199d00ff0058a3f8bea3f51f4af35af79bcb98ecace6ba98911c285db1d70066bc2a794cf3c931555323162aa30064e700761401dcfc30d3774975a9baf0bfb98cf1d7ab7e9b7f335e875e43e0df11b687a87973b9fb0ce7128ebb0f671fd7d47d057ae2b2ba86460cac320839045003a8a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a29188552cc40006493da8029eada95be93a7cb7972d848c703bb1ec07b9af18d5751b8d5b5096f2e4e6490f007451d80f615afe32f1136b7a8797031fb140488c74de7bb1fe9edf535a7f0ffc39f6a986ad769fb989bf70a47df71fc5f41fcfe9401d0f823c37fd8f65f6aba4c5edc2f208e635feefd7b9ff00eb5753451400561f8c353fecbf0edccaad89651e4c7fef377fc064fe15b95e63f12753fb46ad15821f92d572deeedcff002c7e668039ad174f6d5357b5b25cfef5c0623b2f563f9035ee288b1c6a88a151400aa07000ed5e7ff0c74ccbdd6a6e385fdcc7f5e0b1fe5f99af42a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002b94f1a78abfb1a3fb1d9106fa45ceeea225f5fafa0fc7ebd5d78bf8b7ccff84a351f3bef79a71feef6fd3140192eed23b3c8c59d892ccc7249f534da2b73c2fe1b9f5fbbe77476719fdecb8ffc757dff0097e84035be1ff878dede0d52e53fd1addbf740ff001c83fa0fe78f7af4ea86d2d60b2b68edada358e18d76aaaf61535001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450015c6f8efc33fda101d4aca3cddc4bfbc55eb2a0fe647ea38f4aeca8a00f0bd2753b9d23508ef2d1f0e9c107a3af753ed5ecda3eab6face9d1de5ab7cadc329ea8ddd4fbd709e3af0a9b577d574f8ff70c733c6a3fd59fef0f63fa7d3a73fe1bd7a7d075013479781f0268b3f787f88ed401ed34557b1bc8350b48eead64124320cab0ff003d6ac5001451450045716f15d5bc905c46248a45dacadd08af22f15f86e6d02f32b992ce53fba93d3fd96f7fe7f981ec555751b0b7d4eca4b4bb4df148307d41ec47bd0079b7823c5274a9c585ec9fe83237cacdff002c58f7ff0074f7fcfd73ea4082323915e29e20d0ee742d41ade71ba36e62940e1d7fc7d45757e02f14e0c7a46a1271f76da463ff008e13fcbf2f4a00f41a28a2800a28a2800a28a280382f89fa76e86d35241ca1f2643cf4392bf91ddf98ac8f873a81b5d7cdab13e5dda15c76dcbc83f96e1f8d7a17886c3fb4f42bcb400b3c9192801c65872bfa815e2f6572f657b05d4601786459141e84839a00f68f106ad1e8ba44d78f82e06d894ff139e83fa9f606bc4e491e591a4918b3b92ccc7a927a9ae8fc6fe201ad6a422b6726cedf84f476eedfd07d3deaa7857427d7b54588e45b45879dc761e9f53fe27b5004da5f83b55d534cfb75b0882127623b156703b8e31f991d2a95f787758b0dc6e74f9d55464baaef503eab915ed514690c49144a11100555518000e8053e803c16daeee6d24f32d6e2581fa6e8dca9fcc52dd5e5d5eb87bbb99ae180c032b96207e35edb73a569d76e5ee6c2da672305a48958fe64536d746d32cdd5edb4fb589d7eebac4370fc7ad0079bf873c1379aa3a4f7caf6b67c1f9861e41ec3b7d4feb5ea36d6d0da5b476f6f188e28d76aa8ec2a5a28038ef893a97d9b468ec50fcf76ff0037fb8bc9fd76feb5c6783b474d675d8e19d775bc6a6494671903803f323f0cd49e37d47fb47c4971b5b315bfee13fe03d7ff001ecd761f0df4dfb2e8b25eb8fde5dbf1fee2f03f5ddfa500709e23d0e6d0b536b7932d0b7cd0c9fdf5ff0011debacf87de25c85d1efa4e47fc7b3b7fe819fe5f97a0aeabc45a241aee9ad6d2e1645f9a293ba37f87a8af1cbab7b9d36f9e0995a1b881f9e70411d083fa83401ef145737e0df12aeb965e4dc3017d08fde0e9bc7f780fe7eff5ae92800a28a2800a28a2800a28a2800a28a2800a28a2800ae13e217888c319d1ed1c891c66e181e8a7a2fe3dfdbeb5d1789f5d8f41d2da7f95ae1fe58633fc4debf41d4fe5debc7d45c6a17a00dd35cdc49f8bb13fe3401a1e19d0e4d77545b71b9604f9a6907f0aff0089e83ffad5ecb6f0456d04704081228d42aa8ec0566f86b448b42d2d2d970d337cd3483f89bfc07415ad4005145140105f5d4763653dd4c7f770a176f7c0e9f5af0cb99e5bebd96793e69a790b9c0ea49ed5e89f12f54f234e874d8dbe7b86df2007a22f4c8f73ff00a09ae67c07a67f68788a291c662b51e737d47dd1f9f3f81a00f4cd074e1a4e8d6b643ef469f39ce72c796fd49ad0a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002b9cf13f84adf5e227493ecf76abb43e32187a11fd6ba3a2803ceecbe1b4de7837d7d1f920f22104b30fa9e9fad77965676fa7da476b6912c50c630aa3fcf26ac51400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400d655752ac032b0c10464115e57e33f0a368f31bcb252d6121e4753093d8fb7a1fc3ebead51cd1473c2f0cc8af1b82acac3208f4a00f23f09f89a5d06ef649b9eca53fbd8ff00ba7fbcbeff00cff2af5cb79e2b981278245922906e5653904579378bfc31268575e7400bd8cadf237f70ff0074ff0043dea4f0778a9f459c5b5db33584879ee623fde1edea3fc900f59a2991c892c6b246eae8e032b29c820f420d3e800a28a28033b5cd1edb5bd3ded2e463ba381ca37622bc7355d36e748bf92d2ed36c89d08e8c3b11ed5ee958be27f0f43afd8797958ee63c986523a1f43ec680323c0fe2afed28974ebe7ff4c8d7e4727fd6a8ff00d987ebd7d6bb1af069a2b9d36f9a3903c17303fae0ab0ee0ff005af55f07f89935db4f2a72ab7d08fde28e378fef0feb401d251451400514514005789f89ac974ff10df5b26d08b21650bd02b7cc07e00815e95e2ff12268563b21656be987ee97aed1fdf3fd3d4fd0d791c923cb23492bb3bb92cccc72493d493400d1cf4af64f08e8a345d16389d71732fef263df71edf80e3f3f5af3ff0000d8437de248cce462dd0ccaa7f8981007e44e7f0af5ca0028a28a0028a28a002b3b5fd44695a25dde670d1a7c9fef1e17f522b46bcffe276a5ff1eba621ff00a6f27ea17ff66fd280386b3b692faf61b68b9927902027d49c57b9dadbc769690db44311c281173e8060579b7c35d37ed1abcb7ce3e4b54c2ffbed91fcb77e62bd3e800ae63c67e191ad5a7da2d540be857e5ffa68bfdd3efe9ffd7ae9e8a00f08b2bbb9d2efe3b9818c53c2ddc7e608fd08af62f0f6b96faf69e2e2121655c0962cf28dfe1e87ff00af5cd78f3c2df6857d5ac13f7aa333c6a3ef8fef0f7f5f5fe7c468babdce8ba825ddb1e470e87a3af706803dc68aa3a3eab6bacd825dda3e54f0ca7ef21ee0fbd5ea0028a28a0028a28a0028a28a002a39e68ede092699c2471a96663d001d4d495e77f113c43e63ff00635ab828a435c30ee7a85fc3a9f7c7a1a00e67c4badc9aeeaaf72772c2bf2c319fe15ff13d4fff005abb0f877e1ef222fed8ba4c4920c5bab0fbaa7ab7e3dbdbeb5cbf843413ae6aa16407ec9061e63ea3b2fe3fcb35ec0aaa8a154055518000c002801d45145001451581e35d53fb2fc3d394389ae3f731fb67a9fc067f1c50079a78a354fed7d76e6e55b3103e5c5cff0000e07e7c9fc6bd07e1f697f61d005cbae26bc3e61c8c10a3851fccff00c0abcd746d3db55d5adac9723cd70188ea17ab1fc0035ee11c69146b1c6a11100555030001d05003e8a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0086eed60bdb592dae6359219176b29ee2bc87c51e1c9f40bcc0dd25a487f7329ffd04fb8fd7f97b2556d42c2df52b392d2ee30f148304771ee3de803ccfc1de2d6d1e41657cccf60e783d4c24f71edea3f11df3ea71ba4b1ac91b2ba380caca72083d0835e37e25f0e5ce817587064b573fba9b1c1f63e86aef84fc5d3688c2daeb74d604fddea6227bafb7b7f9201eb345436b7505edb47716d2acb0c832aea783535001451450072fe33f0b8d6adbed568a05f42bc76f357fba7dfd0ff91e5d6b7373a75ea4f03b43710b641e841ee08fd08af79ae1bc77e15fb423eada7a7ef946678947df1fde1ee3bfafd7a80743e1ad7a1d7b4e132ed49d30b3440fdd3ebf43dbff00ad5b15e1ba36ab73a36a11de5ab7ccbc3293c3af7535ecba4ea76dabd8477968fb91fa83d51bba9f7140176a96afa9dbe91a74b7972d848c70bdddbb28f73576bc97c6de21fed9d47c8b773f62b624260f123776fe83dbeb40189aa6a371aadfcb7974d99243d07451d80f61555119ce114b1c138033c01927f2a4af47f00786d23b36d4efa20cf708562475e919ea79fef7f2fad00717e1bbf1a66bf6774cc151640ae4f653c13f91cd7b6d7846a568d61a8dcda3124c1232648c670783f8d7b1785efcea5e1eb2b9624b98f6396392597e524fd719fc6803568a28a0028a28a006bb2a233bb055519249c002bc435cd44eabac5d5e9ce257f9011821470a3f202bd2bc7faaff0067e80f023626bc3e52faedfe23f971ff0002af3af0d699fdafaedada15cc45b74bd7ee0e4fd33d3f1a00f4df0569bfd9be1cb75618967fdfbfd5ba7e98fc735bf494b40051451400579bf8dfc23f6669354d323fdc1f9a78547fabff00687fb3ea3b7d3a7a4521008c1e4500789e81ae5d6837c2e2d8ee46e25889f9641fd0fa1edf98af61d2b53b5d5ec52eecdf746dc107aa1ee08ec6bcfbc6de12fecf67d474e8ff00d118e648947faa3ea3fd9fe5587e1ad7e7d02fc4c99781f0268b3f787afd476a00f69a2a0b3bb82fad23bab590490c8372b0ff003d6a7a0028a28a0028a2a2b89e2b5b79279dc2451a96763d80a00c9f15ebaba1694d2a906e65f92043ebdcfd07f80ef5e41147717f78b1a069ae277c0c9c96627b9abfe23d6a5d775592e9f2b10f96143fc2bdbf1ee6bb2f877e1ff221fed8ba4fde4a3102b0fbabddbf1ededf5a00e9bc3da3c5a269515a47867fbd2bff007dcf53f4ec3d8569d145001451450015e55f11355fb76b82d236062b35d9c73973cb7f41f81af48d63504d2b49b9be7008853201eedd00fc4902bc47f7d7975fc52cf3bfd59d89fe649a00eebe18e97ff1f3aa483fe98c5fa163fc87e75e8354b46d3d34ad26dac9307c940188e85bab1fc4926aed0014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450041776905f5ac96d7512cb0c830c8ddff00cfad795f8a7c2371a2335cdbee9ec49fbffc51fa06ff001fe55eb74d74591191d432b0c1523208f4a00f1bf0e7892ef40b8cc47cdb673992063c1f71e87debd6349d5ad358b35b9b2943a9fbca7ef21f423b1ae1fc59e08680bdf68f1968bac96e392beebea3dbfc8e474bd52ef48bc5b9b294c6e3823a861e84771401ee94573de1bf16596b88b13116f7b8e6163f7bdd4f7fa75fe75d0d0014514500799f8ebc2c2c646d4ec23c5ab9fdec6a38898f71ec7f4358de15f10cba06a1bc82f6b2e04d18f4fef0f715ec724692c6d1c8aae8e0ab2b0c820f506bc93c61e1a7d0ef3cd8159ac663fbb6ebb0ff00749fe5ea3e86803adf1cf8816df41863b19431d4010245ff009e78e71ee7207e75e5f563cf967b78ad1e4f9236262dc7014b63233d81c0f6fccd5fb4f0beb57772b0ae9f3c79382f2a1451ef9340167c1da01d73541e72ff00a1c18698f4dde8bf8ff2cd7af80140000007000aa1a1e930e8ba6456507cdb7977c60bb1ea4ff9e805685007957c47b5107897ce5cff00a442ae73d3232bfc947e75bff0bee43e97796b8398a612673d98631ff8e9fceaa7c53519d31b033fbd04ff00df3ffd7a67c2d97173a8c38e5d11b3f4247fecd401e894514500145159be21d48693a25d5e020488988ffdf3c0e3bf273f406803cd3c77aaff0069788658d1b30dafee53af51f78fe7c7d00ae97e19e95e5594fa9c8bf3ce7cb8891fc03a9fc4f1ff0001af3cb6825bcbb8ade21ba599c22e4f524e057b9e9f67169f6105a423f770a041c75c773ee7ad0058a28a2800a28a2800a28a2801acaae851d432b0c10464115e55e33f0b1d1ae3ed768a4d84a781d7ca6fee9f6f43f87d7d5ea1bab686f2da4b7b98c490c8bb594f71401e57e0bf131d16f3ecf72c4d8ccdf37fd336fef0febffd6af58565750ca432919041c822bc6bc4fa04ba0ea261397b7932d0c87f887a1f71deba4f00789f63268f7d27ca78b6918f43fdc3fd3f2f4a00f43a28a2800af35f883e23fb54e749b473e4c4dfbf6078761fc3f41fcfe95d078dfc4c348b436768dfe9d3af507fd52ff7bebe9f9fd7ccf4db0b8d52fa2b3b55dd2ca7033d00ee4fb0a00d6f07f87db5cd4c1957fd0e021a63d377a28faff2cfb57af2aaa285501540c000600154b45d2a0d1b4d8acedc70a32ef8c176eec6afd0014514500145151cf3476d6f24f336d8e252eede800c93401c0fc4dd5416b7d2a36076fefa5c7af2147f338f71599f0ef4afb76b86ee45062b31bf900e5cf0bfd4fe02b9fd56fe4d4f53b8bd97ef4ce5b19ced1d87e0303f0af58f06e94749f0fc11ba9134dfbe941ec4f41ed8181f5cd006ed1451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005723e29f0541aa79977a7ed82f4f2cbd1253efe87dff3f5aeba8a00f06bab5bad3aeda1b989e09e33c83c11ee3fc4575de1df1f4f6bb6df570d7108e04e3fd62fd7fbdfcfeb5de6ada358eb36fe4df401f19d8e38643ea0ff00915e6fe20f045f697be7b4cde5a8c9ca8f9d07b8eff51fa5007a7d95edb6a16cb7167324d1374653fa1f43ed562bc334ad5af747b913d8cc636fe25eaae3d08ef5e93e1ef1bd8ea9b60bddb697478018fc8e7d8f6fa1fd6803aaaaf7d6706a1672da5d2078655c30febf5ab14500789f88743b8d0b516b79b2d137cd14b8e1d7fc7d45757e03f157fabd235093fd9b6918fe487fa7e5e95d7ebba35beb9a73dadc7cadd63900e51bd7ff00ad5e37a9e9d73a55f4969769b658cf51d187623d8d007bb515c7781fc53fda510d3afe4cde463f76ec799547f361fa8e7d6bb1a00e0be2981e469a7bee93f92d50f861ff00218bb1ff004eff00fb30ab1f14ae11a7d3adc1f9d15dd87b1200ff00d04d55f8619fedcba1dbecc7ff00425a00f4ea28a2800af3df89fa892f69a6a9e0033b8c7d42ff00ecdf98af42af12f126a0353d7af2e94828d215423ba8e14fe405006efc36d37ed3acc97ae3e4b44e3fdf6c81fa6efd2bd42b9df02e9ff60f0d4058624b9ccedf8fddff00c740ae8a800a28a2800a28a2800a28a2800a28a28033f5cd22df5ad364b3b8e33ca3e3251bb115e31a858dc6997d2da5caec9a26c1c77f423d8f5af77ae67c6be1c1acd87da2dd47dbadd7e4e3fd62f75fea3dfeb400df047893fb66ccdadd37fa6dbafcc49ff58bd377d7d7ff00af5a3e24d760d074e69e4c34cf95862fefb7f80eff00fd7af1eb0bd9f4dbe8aeed9b6cd0b6467f507d8f4ab1adeaf71adea2f7772719e1101e117b014015ee6e2e752be79a6679ae277e78c9627a003f402bd57c1be1b5d0ec7cdb85537d38fde1ebb07f701fe7eff41593e03f0b7d9d1356bf4fdf30cc119fe007f88fb9ede9fcbb9a0028a28a0028a28a002b8ef88faa8b5d212c236fdeddb7cdec8bc9fcce3f5aec6bc5fc57aaff006c6bd7170adba153e5c3fee0e87f1393f8d003bc23a4ff006bebf042eb9863fdecb9eea3b7e2703f1af66ae4be1d695f62d15af245c4b78770cf641f77f3e4fd08aeb6800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a280398f117832c7570f3db816b787277a8f95cffb43fa8e7eb5e69ab6917da3dcf917d018c9ced71cab8f507bd7b954179676f7d6ed6f770a4d13755719fc7d8fbd00795681e33d47470b0ca7ed76a3811c8dca8ff65bb7d391f4af45d17c49a66b4a05ace166c73049f2b8fc3bfe19ae4f5cf876ea5a6d1a5dc3afd9e53823d95bfc7f3ae26ead2eb4fb9f2ae6192de65e70c0a9fa8ff1a00f79ac2f1578721d7ecb0b88ef2204c321ff00d04fb1fd3f43c3e8be3cd4b4f2b1deff00a6c03fbe7120fa377fc735e81a3f88b4cd6547d92e079a4730c9f2b8fc3bfe191401e34e97161785183c17103f63864606bd5fc29e278759d358dcc891dd5bae67c90a0aff007c7b7afa7e5553c71e181aadb9bfb28ffd3a21f32aff00cb551dbea3b7e5e95e5e923c7bbcb764dc0ab6d38c83d41f6a00d3f136ae75ad6a6bb19117dc881ec83a7e7c9fc6badf85d644457d7cca30c56246cf3c72dfcd6b85d3eca7d4afa1b3b65dd2cadb47a0f527d80e6bdb349d3a2d2b4d82ca0e5225c64ff11ea4fe2726802e5145140195e27befecdf0f5edc824388caa10790cdf283f8139fc2bc7b4bb26d4753b6b34ce6690292067033c9fc064d775f13efc2db59e9ea465d8ccfea00e07e793f9565fc34b0fb46b72de30cadac7c1f466e07e9ba803d391163454450aaa3000e8053a8a2800a28a2800a28a2800a28a2800a28a2800a86eae61b3b692e6e6458e18c6e666ec29f24890c4d24aea8880b3331c0007735e4fe30f143eb973f67b6256c626f9474321fef1fe82803275dbe8752d66eaf2de0f22395f704eff53ee7a9fad2e833d95b6b56b2ea31f996aaff00383c81e848ee01e71df15afe0df0b36b571f69bb565b08cf3d8ca7fba3dbd4ff009143c4fa14ba0ea6d01dcd6eff0034121fe25f4fa8eff9f7a00f664759115d1832b0c8607208f5a75701f0f7c4990ba3dec9c8ff008f676ffd03fc3f2f4aefe800a28a2800a28a28039ff1aeabfd97e1e98a3626b8fdcc7ed9ea7f019fc715e59a369efaaeab6d64991e6b80c47f0af527f0009addf885aafdbf5dfb2c6d986cc6cfab9fbdfd07e15b1f0cb4ada971aacabcb7ee62cfa7563fc87e06803bb862482148a250b1c6a1554740070053e8a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800aad7d6169a8c061bdb78e78cf671d3dc1ec7dc559a2803cef5df87922169b4693cc5ebf6791b0c3fdd6e87f1fccd713756b71672f9575049049d76c8a54feb5ef54c9628e78cc7346922375575041fc0d00787c5ac6a7046238751bb8d074549d801f80354d99a4767625989c924e493eb5ede742d20f5d2ac7ff0001d3fc2a58b4bd3e18a58a1b2b78a39576c8b1c4aa1c7a1c0e6803c9bc21ad2689ad2cf3283048be5c8d8c95048e47e207e15ec48eb222ba30656190c0e411eb5e37e2ad064d075368802d6d2e5a073dc7a1f71fe07bd74df0f7c49f7746bc93febd9dbff40ff0fcbd2803d028a2ab6a1742c74eb9bb237082269319c67009c50079378daf8dff0089eec824a407c8507b6debff008f6eaee3e1d58fd97c382e081beea42f9c73b47ca07e84fe35e58ab25c4e1543492c8d81dcb126bddac6d56cac6ded50e5618d6304f7c0c668027a28a2800a28a2800a28a2800a28a2800a42400493803bd2d79cf8e3c5bf68f334ad364fdc8f967994fdff00f647b7a9eff4ea0153c6be2b3aa4ad6162f8b243f3b8ff0096c47feca3f5ebe959de14f0dcdafdefcdba3b388fef641dff00d91ee7f4fc81ade1ed0ae35ed405bc395897065971c22ff8fa0af62d3ec2df4db28ed2d1364518c01dcfb9f7a0092dade1b4b78edede358e28d76aaaf402a8f88346875cd31ed25f95fef4527f71bb1fa7ad69d1401e0f756d73a65fbc130686e207e7070411d083fa835eafe10f1126bba7ed9580bd84012af4dde8c3ebfa1fc2aaf8e7c37fdad67f6db44cdec0bd00e654f4fa8edf9579b695a95c693a845796ad8910f20f461dc1f63401ee94552d2353b7d5f4e8af2d5b28e395ee8ddd4fbd5da002a86b7a8ae95a45cdeb633126541eec7851f9e2afd79e7c4dd537496fa5c6dc2fefa5c7af451f964fe22803888d26bebc545dd2cf3c98193cb331ff00135edfa5d847a669b6f6517dd8502e718dc7b9fc4e4fe35e75f0df4afb56aef7f22e63b45f973dddb81f90cfe95ea140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140199afe8f0eb9a5c9692fcadf7a37fee30e87fc7dabc6ae20b9d3af9e1943437103e0e0e0a91dc1fd41af78ae2fe20f8785e5a1d56d53fd2205fde81fc69ebf51fcbe8280357c21afaebba60321ff004b830b30f5f461f5fe79a83e20dc8b7f0b4c8739b89123523b73bbf929af36d03579744d562bc8b2ca3e5913fbe87a8fea3dc0aecfe245fc571a369a2060f15c399958770178ff00d0e80395f06da8bbf14d846d90164f3323fd90587ea057b35797fc328f76bf3c8541096c7048e84b2ff4cd7a850014514500145145001451450014535d9510bbb055519249c002bcefc5fe35371bf4fd224221fbb2dc0eafecbedefdfe9d4027f1b78bc012697a5c993f767994f4f5553fccd719a369373acea09696abf31e59cf445ee4d2693a55deb17c969671ee76e598fdd41ea4f615ebfa06876ba1588b7b61b9db996523e691bfa0f41dbf334012e8da4db68da7a5a5aafca39663d5dbb9357e8a2800a28a2800af34f1ff0086fec73b6ab669fe8f2b7ef940fb8e7bfd0ff3fad7a5d473c11dcc124132078a452aca7a106803c8bc23e217d0b51fde126ce6204cbd71e8c3dc7ea3f0af5f8dd258d648d95d1c0656539041e841af1af146832e83a9187e66b6932d04847de1e87dc77fc0f7ae8be1ff00897ca75d1ef5c796c7fd1dc9e87fb9f8f6f7e3b8a00f429a5482179a56db1c6a598fa01c9af0dd56fe4d4f53b8bd933ba672c0139da3b0fc0607e15ee173025d5acb6f26764a851b1e8460d711a6fc3bfb36a714f7378935bc4fb8208f05f07807dbd6803a2f09695fd91a05bc0ebb6671e6cbc73b8f6fc0607e15b545140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140052751834b450078f78cb42fec4d5d844b8b49f2f0fb7aafe1fc88aca9afe69f4fb6b290e63b66768fd46ec647d3233f89af5ff001368cbade8f2db70265f9e163d9c74fc0f4fc6bc624478a468e4528e84ab2b0c1047506803bdf8588a5b537206e02200fb7cd9fe42bd06b87f85d1634ebe9b07e79557f219ff00d9abb8a0028a28a0028a29924890c6d24aea88a32ccc7000f73400faa1ab6b163a35b79d7d304ce76a0e59cfa01fe4572be20f8830c3bedf4651349d3ed0e3e55ff7477fc78fad701757575a8dd99ae2592e2790e32dc93ec3fc280367c49e2ebdd70b4299b7b2cf10a9e5bdd8f7fa74fe7553c3fe1ebdd7ae425baec814e249d87ca9fe27dbf975ae83c39e019ae4a5ceb1ba0878220070edfef7f747ebf4af45b6b686d2dd2deda258a241854518028029e8ba359e89662decd319e5e46fbce7d49ad1a28a0028a28a0028a28a0028a28a00ced7747835bd364b39fe5cfcd1b81928dd8ff9ed5e337f6571a65f4b6b72bb2689b071fa11eddebddeb9af19f86c6b763e75ba8fb7403e43fdf5fee9fe9eff005a006f827c48359b2fb35cb7fa6dba8dd93feb17fbdf5f5ffebd74f5e11637973a5ea11dcc04c73c2d9c11f9823f435ecba16b106b7a6a5dc1f293f2c91e7251bb8a00d2a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800af2bf88da7a5a6ba971147b12ea3dec7b170483fa6d3f8d7aa563f8934083c41602091bcb9633ba2940ced3dc7d0ff008500703e06f1247a35ccb6d7b214b29be6dd827638ef81ea38fcabd2ecf53b1bff00f8f3bc82738ce2390123ea3a8af11beb39b4fbd9ad2e176cb0b156f7f71ec7ad41401f40536491228da491d511465998e001ee6bc3a1d5f5382311c1a8ddc683a2a4ec00fc01a86e6f2eaf1835d5ccd3b0e0195cb11f9d007a7eb1e3bd2ec0347684dece3802338407ddbbfe19af3dd6bc43a8eb7266f263e50395853845fc3bfd4e4d1a5787b54d5997ec968e633ff2d5c6d4fccf5fc39aefb41f01d8d86d9b5022f6e3aed23f76a7e9dff1fca80389d07c2ba96b643c51f936dde790614ffbbfdeff003c8af49d07c2fa7686a1e14f36e71ccf20cb7e1fdd1f4fd6b68000600c014b400514514005145140051451400514514005145140051451401e7ff103c33c36b1631fbdca2ffe878fe7f9fad72de1ad766d075213ae5a07f9668c7f12fafd476ffebd7b3b2aba9560195860823208af25f19f86ce897de75ba9363392633d761fee1fe9edf43401ead6b730de5b47736d2092191772b0ee2a6af2cf03789ce97722c2f2402ca66e198ffaa6f5fa1eff009fae7d4e800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2803cff00e25e8e310ead0af39114f81d7fbadd3ea327fd9ac0f045c5b26ba96b7b0c335bdd0f2caca81807fe13823ae78ff8157a9eaf609a9e957364f8c4d19504f66ec7f03835e1e0cd69720fcd14d0bfd0ab03fcc11401ec73784f429df73e99083fec6507e408a9ed740d22d369834db6565390c630cc0fd4f35674ebc4d434eb7bb8f1b668d5f00e7191c8fc3a559a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002aaea3616fa9d8cb6774bba29460e3a8f423dc55aa2803c3f5cd227d135292cee39c728f8c075ec4576fe00f137da235d22f5ff7a83fd1dc9fbca3f87ea3b7b7d39e87c4da0c3af69a606da93c79686423ee9f43ec7bff00f5abc7a686e74dbe68e45782e607f5c1561d083fae6803de68ae7fc21e224d774fc4a40bd84012af4dde8c3d8fe87f0ae82800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800af1cf1b5ba5b78aef9635daaccb27e2ca09fd49af63af2cf8971aa788e3651cc96cacdf5dcc3f901401a9f0e35dca9d1ae1b9197b73fab2ff33f9fb577f5e0b67752d95e437501c490b875cf4c835eeb6b3a5d5ac371112639503ae7d08c8a00968a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002b93f1bf8606ad6c6f6cd3fd3a15e547fcb55f4fa8edf97a63aca2803c2b4bd46e749d423bcb56db2467907a30ee0fb1af66d1b55b7d674e8ef2d8fcadc3293ca37706b88f1ff00867c976d5ec93f76c7fd2107f093fc5f43dfdfeb5cff0085bc412e81a8893e67b6970b3463b8f51ee3ff00ad401ecd45476f3c5730473c0e1e291432b0e841a92800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800af2ff89dff00230dbffd7aaffe86f5ea15e41e3bbd179e28b80ac1920021523b63a8ff00be8b50073d5ee1e1ff00f917b4cffaf48bff004015e1f5eeba4c5e46916509eb1c08bf92814016e8a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a006c91a4b1b4722874705595864107a835e43e2ff0e3e857fba2526ca624c4dd76ff00b27dc7ea3f1af60aa7aa69b6dab584967769ba371d47553d88f71401e77e04f13ff675c0d3af65c59ca7f76cc78898ff00207f43cfad7a857876b5a4dc68da8c96772395e51c0e1d7b115ddf80bc4ff6c8974abe7ff488c7ee5c9ff58a3f84fb8fd47d3900eda8a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a00a3acea51e93a55c5ec983e52e554ff00137403f138af1096579a5796562f23b16663d493d4d765f127573717f1e97137eeedf0f27bb91c7e40ff00e3c6b8aa0092de07b9b98a0886e925708a3d493815ef95e47e02d30dff0088a2959730da0f3989ce323ee8fae79fc0d7ae500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450063f89b41875fd38c2d849e3cb43211f74fa1f63dfff00ad5e3f34375a65fb4722bc17303fd0ab0e841fd735ef15caf8dbc32357b5fb65a2ff00a742bd07fcb55feefd7d3f2fa0059f08f8923d76cb64ac16fa11fbd4e9b87f787b7f23f857435e11a7df5c6997d1dddabec9a2391e87d41f6af64d035bb6d774f5b983e571c4b113ca37a7d3d0d0069d14514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005437970967673dcc809486369180eb80327f954d55753b4fb7e997569900cd132027b12300d007875d5c49777535ccc7324ce5d88f52726a2a9aeed67b2b992dae6368e58ce1958569f85f44975bd5a28bcb26d91834edd82fa67d4f4a00f43f02e9234cd02391d713dde2573df07ee8fcb9fa935d25252d0014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450079f78f7c2c7326b1611e41f9ae6351ff8f81fcff3f5ae4f40d6ee742d416e6dfe643c4b113c3afa7d7d0d7b65799f8dbc25f6067d4b4e8ffd158e658947faa3ea3fd9fe5f4e801e85a66a36daad8c77768fba371f8a9ee0fb8ab75e2de1ef10de68175be0fde42e47990b1e1bfc0fbff3af58d1358b6d72c05ddaee033b5d1baa37a1a00d1a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2802ade69d657f8fb65a41391c03246188fa1352dbdb416b17956d0c70c639d91a851f90a968a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0029ae8ae8c8ea195860a919047a53a8a00f3cf107c3f95aebced176794ff7a176c6c3ec7d2ba4f07e832683a63c53c8af3ccfbdf69e178c002b7e8a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a00ffd9, 'Delivered', 'ORD1000000017');
INSERT INTO `delivered_product` (`delivered_product_id`, `order_receiver_signature`, `order_receiver_signature_status`, `order_details_id`) VALUES
(7, 0xffd8ffe000104a46494600010100000100010000ffdb004300100b0c0e0c0a100e0d0e1211101318281a181616183123251d283a333d3c3933383740485c4e404457453738506d51575f626768673e4d71797064785c656763ffdb0043011112121815182f1a1a2f634238426363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363636363ffc00011080314043803012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00f40a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28aa1ab6b363a35bf9d7d384cfdd41cb3fd05005fa2b0342f16e9dae5d35b5bacd14c14b059540dc3db04d6fd00145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014c9658e18da495d638d465998e001ea4d63f883c4f61a1465656f36e48cac0879fc7fba3fc806bcc35cf116a1ae4b9ba976c20e5604e117f0ee7dcd00761e20f881143bedf46512bf4370e3e51fee8eff53c7d6b81b8b8bad46eccb3c925c5c487193f3127b01fe15a5a0f86351d71c3431f956d9f9a790617f0fef1fa7e38af4bd07c2fa768681a14f36e71f34f20cb7be3fba3e9f8e680303c0fe13bad3ee9753d43f74fb088e1fe219eede9c76f7ed5dd514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145154f53d4ed349b46b9bd984683a0eec7d00ee6802db30552cc405032493c015c1f89fc78a9bed34560cdd1ae71c0ff7477fafe5eb5cf7897c5d79adb3431e6decb3c440f2feec7bfd3a7d7ad55f0ff86efb5e9f102f976ea7124ec3e55f61ea7dbf9500674515d6a378123596e2e666cf76663dc9ff001af41f0e78061b7d973aced9a5e08b707e45ff0078ff0017d3a7d6ba4d0f40b1d0e0d9691e6461f3ccdcbbfe3d87b56a500351163454450aaa30140c003d29d451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514d7654467760aaa324938005703e28f1dfdfb3d15bfd97baffe23fc7f2f5a00def12f8b6cf43468531717b8e2253c2fbb1edf4ebfcebcb754d52f356bb6b9bd94c8e7803b28f403b0a8ed6d6eb53bc10db46f3dc487381c93ea49fea6bd37c31e0bb6d2365d5eedb8bd1caff7233edea7dff2a00e7fc31e0596ef65deaead0c1d560e8eff005fee8fd7e95e8f04315b4290c11ac7120c2a28c002a4a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800aa9a96a569a55a35cdeccb1463819eac7d00ee6b37c49e28b3d062dac44d76c3e4814f3f563d87f3af2ad5b57bcd62ecdc5eca5dbf85470a83d00ed401a9e26f16ddeb8cd0c79b7b1cf1103cbfbb1eff4e9fceaaf87fc397baf4f8817cbb753892761f2afd3d4fb7f2ad9f0b78226d4365e6a81a1b4eab1747907f41fa9fd6bd2adede1b58120b78d6289061514600a00a3a268765a1daf93691fccdfeb256e59cfb9fe95a745140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051454375730d9dbbdc5ccab14318cb3b1c0140135711e29f1cc76bbacf4775967e8f3f554f65f53fa7d7b6278abc6b36a9e659e9c5a1b23f2b3747947f41edf9fa560e8da35eeb577e459479c7df76e1507a934015912e750bcdaa24b8b999bdd99cd7a4785bc11169db2f3530b35d8e563ea911fea7f41dbd6b63c3de1bb2d061fdc8f36e186249d87cc7d87a0f6fe75b3400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145739e27f16dae868d045b67be2388b3c27a16ff000ea7dbad00696b3ad5968969e7de498ce7646bcb39f402bc9fc41e23bdd7ae374ede5c0a731c0a7e55f73ea7dff9551d4350bad4eedae6f6669656e327b0f403b0aec7c2be05798ade6b48523ea96c7866f76f41edd7e9dc031fc31e12bad75d6693741620f32e397f65ff001e83dfa57aa69da75ae9768b6d650ac512f381d49f527b9ab088b1a2a46a111400aaa30001d853a800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a4e839a86f2eedec6d9ee6ee558a1419666ff3c9f6af30f14f8cae3582d6b67badecba11d1a5ff007bd07b7e7ec01b5e2af1d2c41ecf4570d2747b91c85f65f5faf4f4f51c1410dd6a378228564b8b999ba7566279249fd49ab7a26877bae5d793671fcabcc92370a83dcff4af57f0ff00876cb41b7db6ebbe761892761f337f80f6fe740195e15f0641a484bbbf0b3df7551d562fa7a9f7fcbd4f5945140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400564ebde21b2d0adcbdcbee998663814fccff00e03dff00fd558be28f1bc1a6f9969a6959ef07cacfd5223fd4fb7e7e95e6d3cf73a8de1966792e2e266ea79663d001fcb1401775dd7af75dbaf36e9f11affab857eea0fea7deb4bc2fe0fb9d68adc5c6eb7b1cfdfc7cd27fbbfe3d3eb5bbe16f022a6cbcd69433e7296bd40ff7fd7e9f9fa57780000000003a014015ec6c6db4eb55b6b38561897a2aff0033ea7deacd14500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145158fe20f1258e830e676f32e19731c0a7e66f73e83dfebd68034aeaea0b3b77b8ba9562850659d8e00af35f13f8e27d477da6985aded0f0d2747907f41faff002ac3d775fbed76e7ccba7c46bfeae15e153fc4fbd68f867c1f77ad159e7dd6d659ff005847ccff00ee8febd3eb4018fa569379ac5d8b7b288bb7f131e1507a93dabd4fc37e14b3d0a3121027bd23e69987ddf651d87ea7f4ad5d374db4d2ed16daca158a31c9c7563ea4f7356e800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a42400492001d49aa7aa6a967a4da1b9bd984683a0eacc7d00ee6bcc3c49e30bcd68b41166dacbfe79a9e5ffde3fd3a7d6803a3f1378f23b7dd6ba332cb2f46b8eaabfeefa9f7e9f5af3fff0049d42eff00e5adc5cccdeecce7fad5ed0bc3f7daedc6cb54db129f9e67fba9fe27dabd4f40f0dd8e830e205f32e1861e771f337b0f41edfce8039ef0cf80e3b7db75acaacb2f056dfaaaff00bdea7dba7d6bb80000001803b52d140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051454179796f616cf717732431275663fe727da8027ae5fc4be34b4d1f75bdaedbabd1c1507e48cffb47d7d87e95cc7897c75717c5edb4a2f6d6dd0cbd247ffe247ebfcab9bd2f4abdd5ee85bd8c2d237f11e8aa3d49ed400cd4350bbd56ecdc5e4cd34adc0cf403d00ec2bacf0d780e6badb73ac06860eab00e1dfebfdd1fafd2ba5f0df83ecf450b3cd8b9bdc7fac61f2a1ff647f5ebf4e95d2d00456d6f0da40905b44b144830a8a30054b4514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005150dd5cc1676ef71732a45120cb3b9c015e79e23f1f4d71bedb46dd0c5c83707876ff77fbbf5ebf4a00ea7c45e2db1d0d5a2cfda2f3b4287eeff00bc7b7f3af2fd635abed6ae7cebd97763ee46bc227d07f935059d9ddea77620b589e79dce7039fc49edf535e91e1bf035b69bb6e751d97574390b8cc69f81ea7dcfe5de80398f0d782aef56db7179bad6cce08247cf20f61d87b9fd6bd334ed3ad34bb55b6b285628c761d49f527b9ab54500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145155afefed34db73717b3a4310eec7a9f403a93ec2802cd73de21f17586881a207ed1783a4287eeffbc7b7d3ad725e21f1edd5eee834a0f6b07432e7f78dff00c4fe1cfbf6ae56c6c6eb52ba16f670bcf33765fe64f61ee6802ceb3ae5feb73f997b36547dc897844fa0febd6b4fc39e0ebdd64acf366dacfaf98c3e671fec8febd3eb5d5f877c076b63b6e354db7571d447d634ff00e28fd78f6ef5d850052d2748b2d1edbc8b1842038dcc79673ea4f7abd45140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140052741cd636bbe27d3b4352b3c9e65c638823e5bf1f41f5fc335e6daf78af51d6cb46efe45a9e904678233fc47f8bb7b7b0a00ecf5ff001ed9d8ee874c0b797038df9fddafe3fc5f871ef5e75a8ea579aa5c9b8be9de693a0cf451e80741f854fa3685a86b5379765092a0e1a56e113ea7fa0e6bd27c3fe0cb0d1f6cd301777639f31c7ca87fd91fd7afd28038ef0f781ef754db3deeeb3b53c8c8f9dc7b0edf53f91af49d2f4ab2d26d8416302c4bfc47ab31f527bd5da2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a2992c91c31b492bac71a8cb331c003d49ae27c41f10628775be8ca2693a1b871f20ff7477fa9e3eb401d76a3a9d96976fe7df5c2429db3d5be83a9fc2bcef5ff001f5ddeee834b56b480f06427f78dff00c4fe1cfbd72d797b75a8dc99eee679e66fe2639fc07a0f615d3683e02bdbfdb36a45acedff00b847ef5bf03f77bf5fca80398b7b7b9bfba115bc524f3c87eea8249f53ff00d7aeef40f87aabb67d69f71ea2da36e07fbcc3f90fccd761a5e9163a441e4d8dbac40fde6eacdf53d4d5ea008e0862b7856182348a34185441803f0a928a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28aa3aa6ad63a45bf9d7d3ac6a7eeaf5663e8077a00bd5ceebfe30d3b46dd12b7daaec71e4c67853fed1edf4ebed5c67883c737da96e82c7759db1e320fef1c7b9edf41f99ae7f4ed36f354b8f22c6dde693a9c7403d49e83f1a00b5ad788751d6e4cddcd8881cac29c22fe1dcfb9a9b41f0bea3ae307863f2adb3f34f270bf87f78fd3f1c5767a07806d2cb6cfaa32ddce391181fbb5ff00e2bf1e3dabb1550aa15400a06001d050061e83e14d37440af1c7e7dd0eb3c83247fba3f87f9fbd6ed1450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014d7758d19dd82a28c96638005636bfe28d3f4242b33f9b7247cb0467e6fc7fba3fc8cd799ebde26d435c9089e4f2edf3f2c119c28fafa9fafe9401d7f887e204306fb7d1c09a5e86761f22fd077fe5f5af3fb9babbd4aeccb71249717121c64f24fa003fa0ad2d07c2fa8eb8e1a18fcab6cfcd3c83e5fc3fbc7e9fa57a6683e18d3b4340d0c7e6dce399e4196fc3d07d3f5a00e3bc3fe00b8badb71abb35b43d442bfeb1bebfddfe7f4af42b1b0b5d3ad96deca048621fc2a3afb93dcfb9ab3450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014515cdf88fc6363a286822c5cde0ff966a7843fed1edf4ebf4a00ddbbbbb7b2b77b8ba992189072ce702bcf7c45e3f9ae77db68e1a08ba19dbefb7d07f0ff003fa572fabeb37dacdc79d7d317c67620e153e83fc9ad7f0ef82ef757d93dc66d6ccf3bd87cce3fd91fd4feb401836b6b77a95d88ada292e2790e703927dc9fea6bd07c3de0082d825c6b05679ba8814fc8bf5fef7f2fad751a4e9163a3db7916308407ef31e59cfa93deafd00351551151142aa8c00060014ea28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002abdede5b585b35c5dcc90c49d598fe9ee7dab13c47e2fb1d1034298b9bcc71129e17fde3dbe9d7f9d798eafacdf6b373e7decc5f19d8838541e807f93401d1f88fc79737dbadf4adf6d6fd0cbd247ff00e247ebfcab96d3f4fbbd4ee85bd940f34a79c2f61ea4f61ee6b7bc37e0cbcd636dc5ceeb5b3382188f9a41fec8fea7f5af4dd334bb3d26d45bd8c2b1a773d4b1f527bd0073de1cf035a699b6e350d9777439008fdda7d01ea7dcfe55d6d1450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014515c9f883c7363a68686c76de5c8e32a7f7687dcf7fa0fcc5007497b7b6da7db35c5e4c90c4bd598fe83d4fb0af3af1178f6e2f375be93bada0e8663c48df4fee8fd7e9d2b99d5356bdd5ee4cf7d3b48dfc2bd1507a01dab6bc3be0bbdd5f64f739b5b3383b987cee3fd91fd4faf7a00c0b2b2bad46e96dece179e66fe151fa93d87b9af48f0df816db4fdb73a9ecbaba1c84c6634ff00e28fd78f6ef5d1693a45968f6de458c2231c6e6eace7d49ef57a800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28acfd575ab0d1e1f32fae16324655072edf41fe4500685636b7e26d374452b732ef9f1c411f2ff8fa7e35c3ebbe3ebebedd0e9aa6ce03c6fcfef187d7f87f0e7deb97b6b5bad42e8456d149713b9ce14649f73fe3401b3af78bf51d6b745bbecd6a7fe58c67a8ff0068f7fe5ed59fa468b7faccfe558c05c03f3487844fa9fe9d6bb2d07e1e28db3eb526e3d7ecf1b71ff026fe83f3aee6dede1b58561b78922897eea22e00fc28039bf0ff00822c74ad93dde2f2ec60e587c887d877fa9f4ed5d4d145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145145001451450014514500145154752d5ec34a8bccbeba8e107a293966fa01c9a00bd54f51d52c74b87cdbeb94857b02796fa0ea7f0ae1359f889712ee8b4987c84ff9ed2805ff0001d07eb5c6cb35d6a175ba5796e6e242002c4b331ec2803b0d6fe21dc4fba2d222fb3a74f3a400b9fa0e83f5fc2b8e26eafeeb9f36e6e253eeeee7f99aeaf43f005ede6d9b526fb1c279d9d6461f4e8bf8f3ed5df693a269fa3c5b2c6dd5188c3487976fa9fe9d280386d0fe1edcdc6d9b5690db47d7c9420b9fa9e83f5fc2bbed374cb2d2adfc8b1b74853be3ab7b93d4fe35728a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a2a29ee21b688cb713470c63abc8c140fc4d004b457357fe3ad12cc95499ee9c1c1102647e67008fa66b98d43e23dfcc0ad8dac56c08c6e73e6303ea3a0fcc1a00f4b240193c01581a9f8cb45d3b2a6e7ed320fe0b7f9ff5e9fad796ea1ac6a3a9926faf2698139d85b0a0fb28e07e5469da36a3aa362c6ce598671bc0c283eec781f9d00741ab7c40d4ef034764896519e32bf33fe67a7e033ef5cb93717d75cf9b71712b7bbbb9fe64d771a57c387387d56ec28ff9e50727f1623fa1fad767a668fa7e93194b0b54873c161cb37d58f26803cfb45f87f7f79b65d45fec709e7675908fa741f8f3ed5dee91a069da2a62cadc2b918695b976fc7fa0e2b4e8a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a2b3b50d774ad3091797d0c6e3aa67738ff00808c9fd2803468ae1b51f8916b1e574eb39266e46f98ec5f63819247e55cc6a1e34d72fb23ed5f6643fc36e3663f1fbdfad007ab5eea565a7a6fbdba86018c8dee013f41d4fe15cc6a3f1134cb7cad9432ddb0fe2ff5687f13cfe95e6a91cf793ed8d249e673d1416663fccd741a778175bbdc3490a5a4646774ed83ff007c8c9cfd71400fd47c7bacde656178ed2339188972d8fa9cfe98ae72e2e6e2ee5f32e6696790f1ba462c7f335e91a7fc39d3e0c35f5ccd74c0fdd5fdda9f6ee7f515d2e9fa3e9da601f62b386120637aafcc47bb1e4fe7401e4b61e15d6b501986c2554e3e797f76307b8ce33f866ba6d3fe1ab921b51bf5033ca5bae723fde3d3f2af42a28030b4ff0008689a78056c96770305e7f9c9fc0f00fd056e0000000c01da968a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028acebed774ad3b70bbbf823653864ddb9c7fc0473fa573d7ff11b4d8372d9dbcf74c0e01388d08f5c9c9fd2803b2a6b32a296760aaa324938005796df7c41d62e0916c21b45ce46c4ded8f425b23f415ce5e6a1797ec1af2ea69c8e9e6396c7d33d2803d6750f18e8760086bd59dc0c84b7f9f3f88e3f335cd6a1f126424ae9d60aa33c3dc36723fdd18c7e66b91d3f44d4f53c7d8aca6954e407db84ff00be8f1fad74da7fc38be9406bebb8adc119da83cc6fa1e83f2268030350f136b1a902b737f2ec2082919d8a47a10319fc6a85a595d5ec863b4b796771c911a1623f2af55d3bc0fa25890cd035d38390d70db87fdf23008fa835d0430c56f12c50469146bd1114003f01401e5fa77c3ed5ee4837662b34cf3b9b7b63d80e3f322ba7d3be1f6916b86ba696f1f1cee6d8b9f50073f9935d6d140105ad9dad947e5da5bc5021eab1a0507f2a9e8a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a2b3ee75cd26d7709f51b5464eabe682c3f01cd515f19787d9c20d45327d63703f3231401bd45558352b0b804c17b6d281d4a4aad8fc8d4a9710c8711cd1b9f456068025a28a2800a28a2800a28a6b32a296621540c924e001400ea2b26ebc4da25a2e66d4edcf38c46de61fc9726b0ef7e236990ee1696f7172c0f04808a7f13cfe9401d9515e5d7df113559f72dac305aa9e871bd87e278fd2b02fb5cd5350dc2eefe7915baa6fc2ff00df238a00f60bed734bd3f70bbbf82365ea9bf2e3fe0239fd2b9fbef889a541b96d219ee987438d887f13cfe95e6f6b63777ac56d2d669c8ea228cb63f2adeb2f01eb975cc90c56ca46419a41cfe0b93f9d005abef88baa4fb96d2082d54f438dee3f13c7e95cfdf6b9aa6a3b85ddfcf22b754dd843ff00011c7e95dad97c35b75c1bed424938fbb0a04c1fa9ce7f215bf65e11d0acb052c2391b182d3664cfbe0f1f90a00f22b5b3babc729696d34ec3922242c47e55bd65e04d72eb99218ad571906693afb617241fae2bd663448d152355445180aa3000a750070d65f0dad139bebe9a63c7cb128403d7939cfe95d258f86f46d3c836da7c2181dc1dc6f607d8b648fc2b568a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a0028a28a002b87f88dadcf671c3a6dac863332979594e0edce00fa1e7f2aee2b87f88da1cf791c3a95ac66430a14955464edce41fa0e7f3a00f37a28a7c30cb3cab1431bc9231c2a229249f60280194574761e07d72f63f30c096ca46479edb49fc0648fc40ab4df0ef5a51c4968dec246ff00e268039682e27b690496f3490c83a346c548fc456a5af8ab5db524c7a9ced9ff009ea7ccff00d0b35a91fc3cd69ce19ad631ead21fe80d5bb5f86b78cc7ed77f0443b794a5f3f9eda00caff84e7c41b71f6d5cfaf9299fe5514be31f104cbb5b51703fd84453f9815da5b7c3bd22251e7cb733b77cb851f901fd6af8f04f8787fcc3f3f59a4ffe2a803cc0ebfac1393aadf7e170e3fad5092592572f2bb3b1ea58e4d7b3dbf85f43b7ff0057a65b9edfbc5dff00fa166ae5b6996168fbed6c6da07fef4712a9fd050078ada695a85f006d2cae265ce3724648fcfa56f59f8035bb8ff5c90da81ff3d24c93f4db9af57a280387b3f86d669cdedf4d31f489420fd739fd2b7ecbc29a1d973169d0bb631ba51e61fafcd9c7e15b3450022aaa285501540c000702968a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a2800a28a280294ba3e9934864974eb491cf56681493f8e2acc30c50462386348e31d1514003f01525140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451400514514005145140051451401fffd9, 'Delivered', 'ORD1000000018');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `department_id` bigint(20) NOT NULL,
  `department_added_datetime` datetime DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `short_name_for_employee_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`department_id`, `department_added_datetime`, `details`, `name`, `short_name_for_employee_id`) VALUES
(1, '2017-09-07 00:00:00', NULL, 'SalesMan', 'SM'),
(2, '2017-09-07 00:00:00', NULL, 'DeliveryBoy', 'DB'),
(3, '2017-09-07 14:18:28', NULL, 'GateKeeper', 'GK'),
(4, '2017-09-07 00:00:00', NULL, 'Admin', 'AD');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` bigint(20) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `department_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`employee_id`, `password`, `user_id`, `department_id`, `company_id`) VALUES
(8, 'admin', 'admin', 4, 0),
(9, 'sachin', 'sachin', 1, 2),
(10, 'tushar', 'tushar', 2, 2),
(11, 'rohan', 'rohan', 3, 2),
(12, 'nehal', 'nehal', 1, 4),
(13, 'parag', 'parag', 3, 4),
(14, 'ali', 'ali', 2, 4),
(15, 'kpu', 'kpu', 1, 2),
(16, 'khush', 'khush', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `employee_areas`
--

CREATE TABLE `employee_areas` (
  `employee_areas_details_id` bigint(20) NOT NULL,
  `area_id` bigint(20) DEFAULT NULL,
  `employee_details_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_areas`
--

INSERT INTO `employee_areas` (`employee_areas_details_id`, `area_id`, `employee_details_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(5, 2, 3),
(6, 1, 3),
(7, 3, 3),
(8, 6, 4),
(9, 7, 4),
(10, 6, 5),
(11, 8, 5),
(12, 9, 5),
(13, 6, 6),
(14, 7, 6),
(15, 8, 6),
(16, 2, 2),
(17, 3, 2),
(18, 1, 2),
(19, 4, 7),
(20, 3, 8);

-- --------------------------------------------------------

--
-- Table structure for table `employee_basic_salary_status`
--

CREATE TABLE `employee_basic_salary_status` (
  `employee_basic_salary_status_id` bigint(20) NOT NULL,
  `basic_salary` decimal(19,2) DEFAULT NULL,
  `end_datetime` datetime DEFAULT NULL,
  `start_datetime` datetime DEFAULT NULL,
  `employee_details_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_basic_salary_status`
--

INSERT INTO `employee_basic_salary_status` (`employee_basic_salary_status_id`, `basic_salary`, `end_datetime`, `start_datetime`, `employee_details_id`) VALUES
(1, '10000.00', '2018-04-07 18:17:44', '2018-03-05 15:52:44', 1),
(2, '12000.00', '2018-04-07 18:17:38', '2018-03-05 15:53:40', 2),
(3, '13000.00', '2018-04-07 18:17:39', '2018-03-05 15:54:22', 3),
(4, '12000.00', '2018-04-10 13:28:20', '2018-03-07 11:04:29', 4),
(5, '15000.00', '2018-04-10 13:28:17', '2018-03-07 11:05:45', 5),
(6, '15000.00', '2018-04-10 13:28:17', '2018-03-07 11:06:41', 6),
(7, '20000.00', '2018-04-07 18:17:39', '2018-03-26 12:47:34', 7),
(8, '20000.00', '2018-04-07 18:17:39', '2018-03-26 12:55:54', 8);

-- --------------------------------------------------------

--
-- Table structure for table `employee_chat_status`
--

CREATE TABLE `employee_chat_status` (
  `employee_chat_status_id` bigint(20) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `typing_status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_details`
--

CREATE TABLE `employee_details` (
  `employee_details_id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `basic_salary` decimal(19,2) DEFAULT NULL,
  `employee_details_added_datetime` datetime DEFAULT NULL,
  `employee_details_disable_datetime` datetime DEFAULT NULL,
  `employee_details_gen_id` varchar(255) DEFAULT NULL,
  `employee_details_updated_datetime` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `contact_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_details`
--

INSERT INTO `employee_details` (`employee_details_id`, `address`, `basic_salary`, `employee_details_added_datetime`, `employee_details_disable_datetime`, `employee_details_gen_id`, `employee_details_updated_datetime`, `name`, `status`, `token`, `contact_id`, `employee_id`) VALUES
(1, 'Malad', '10000.00', '2018-03-05 15:52:43', '2018-03-05 15:52:43', 'SM1000000001', '2018-03-05 15:52:43', 'Sachin', 0, '', 4, 9),
(2, 'Kandivali', '12000.00', '2018-03-05 15:53:40', '2018-03-05 15:53:40', 'DB1000000001', '2018-03-07 11:33:47', 'Tushar', 0, 'efiYEw5N_DA:APA91bFJSqNzkVbfiEMlJ4CKQlNzzCf7K2FFkOLy0FXL9ynTMTew2sLHtYe_GO_8zIGbRM5RoWxLlC-7X6qe_W8P540zID5crSuJbDWfWj5jBAWg2MJDHfMvJlaWsxodzf5nVnT9zawt', 5, 10),
(3, 'Bhoiser', '13000.00', '2018-03-05 15:54:21', '2018-03-05 15:54:21', 'GK1000000001', '2018-03-05 15:54:21', 'Rohan', 0, 'cjpi_76Uni0:APA91bHFU4DkVZlhBqPJGniT6KUuNkzwJQa3Pha41C7_9PU0Ld15iKsAYCt2bEhFqv0kABSUgMBqmjFVxm1kMnYT3iJ8ZGrv1XuuMQ43JAmMBETscTr9K_V3M00V6Ht-EEXQtbREZRTm', 6, 11),
(4, 'kandivali', '12000.00', '2018-03-07 11:04:29', '2018-03-07 11:04:29', 'SM1000000002', '2018-03-07 11:04:29', 'Nehal', 0, 'dkV9qcVJ_rc:APA91bFBVs1EgBy9cpF0zP0VB7IxvRjPzXRiEMk_t1SG3nTTmHOZXguJZqmZHQMUSz2eDQqO3guMAiVQSygQS6lPUBX4_oKen55ISuM711MlAsGIipvggS7i58kLRGVQx433_1Ee0NGm', 10, 12),
(5, 'dfds', '15000.00', '2018-03-07 11:05:45', '2018-03-07 11:05:45', 'GK1000000002', '2018-03-07 11:05:45', 'Parag', 0, 'c0Ry4gNkZjQ:APA91bE0Rjc15O8c9kWXC9QgbbsSFCa3n5-npDJzHjbwjLRYv9hBCfShylQTgt-ut-yZqFYgVu0OBL19LyMXDei86Z35BlqYI7Z4yao32VMBsUOngBRVpjBZJixd3vF8NHr_Yq_1mv6o', 11, 13),
(6, 'abc', '15000.00', '2018-03-07 11:06:41', '2018-03-07 11:06:41', 'DB1000000002', '2018-03-07 11:06:41', 'Ali', 0, '', 12, 14),
(7, 'abc', '20000.00', '2018-03-26 12:47:34', '2018-03-26 12:47:34', 'SM1000000003', '2018-03-26 12:47:34', 'kunal', 0, '', 18, 15),
(8, 'abc', '20000.00', '2018-03-26 12:55:54', '2018-03-26 12:55:54', 'SM1000000004', '2018-03-26 12:55:54', 'khush', 0, 'cORdmTRwADs:APA91bHJlpzDSsJfnm0icfTPRBUsPewjhysSrgNqPPMEyt9KVPxtSyHJWql4gbDfc_UxGdrUNrOwFjNfR1Pa0RjybeRnS7tZ28CZfvZmmrZLK-Z7ahf4lkZxDZW7LYbYG6XrlKuBLCI7', 19, 16);

-- --------------------------------------------------------

--
-- Table structure for table `employee_feedback`
--

CREATE TABLE `employee_feedback` (
  `feedBack_id` bigint(20) NOT NULL,
  `feedback_date` datetime DEFAULT NULL,
  `feedback_reply_date` datetime DEFAULT NULL,
  `feedback_msg` varchar(255) DEFAULT NULL,
  `reply_status` tinyint(1) DEFAULT NULL,
  `employeeDetails_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_feedback_reply`
--

CREATE TABLE `employee_feedback_reply` (
  `feedback_reply_id` bigint(20) NOT NULL,
  `feedback_reply_date` datetime DEFAULT NULL,
  `feedback_reply_msg` varchar(255) DEFAULT NULL,
  `feedback_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee_holiday`
--

CREATE TABLE `employee_holiday` (
  `employee_holiday_id` bigint(20) NOT NULL,
  `from_date` datetime DEFAULT NULL,
  `given_holiday_date` datetime DEFAULT NULL,
  `paid_holiday` tinyint(1) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `employee_details_id` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_holiday`
--

INSERT INTO `employee_holiday` (`employee_holiday_id`, `from_date`, `given_holiday_date`, `paid_holiday`, `reason`, `to_date`, `employee_details_id`, `status`) VALUES
(1, '2018-04-02 00:00:00', '2018-04-02 16:08:01', 0, 'abc', '2018-04-03 00:00:00', 4, 1),
(2, '2018-04-04 00:00:00', '2018-04-02 15:49:16', 1, 'sss', '2018-04-05 00:00:00', 4, 1),
(3, '2018-04-02 00:00:00', '2018-04-02 18:42:06', 1, 'ss', '2018-04-03 00:00:00', 4, 1),
(4, '2018-04-02 00:00:00', '2018-04-02 18:42:49', 1, 'tt', '2018-04-03 00:00:00', 4, 1),
(5, '2018-04-02 00:00:00', '2018-04-02 18:46:11', 1, 'ff', '2018-04-03 00:00:00', 4, 1),
(6, '2018-04-07 00:00:00', '2018-04-07 16:26:49', 1, 'sd', '2018-04-09 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_incentives`
--

CREATE TABLE `employee_incentives` (
  `employee_incentives_id` bigint(20) NOT NULL,
  `incentive_amount` decimal(19,2) DEFAULT NULL,
  `incentive_given_date` datetime DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `employee_details_id` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_incentives`
--

INSERT INTO `employee_incentives` (`employee_incentives_id`, `incentive_amount`, `incentive_given_date`, `reason`, `employee_details_id`, `status`) VALUES
(1, '100.00', '2018-04-02 11:15:51', 'abc', 4, 1),
(2, '50.00', '2018-04-02 11:16:02', 'xyz', 4, 1),
(3, '100.00', '2018-04-02 15:42:56', 'aaa', 4, 1),
(4, '20.00', '2018-04-02 15:43:33', 'aaa', 4, 1),
(5, '30.00', '2018-04-02 15:44:57', 'h', 4, 1),
(6, '50.00', '2018-04-02 17:46:09', 'a', 4, 1),
(7, '100.00', '2018-04-02 17:47:18', 'd', 4, 1),
(8, '100.00', '2018-04-02 17:48:58', 'j', 4, 1),
(9, '50.00', '2018-04-02 17:49:09', 'm', 4, 1),
(10, '10.00', '2018-04-02 17:49:16', 'm', 4, 1),
(11, '100.00', '2018-04-02 17:53:08', 'aa', 4, 1),
(12, '50.00', '2018-04-02 17:53:15', 'xx', 4, 1),
(13, '10.00', '2018-04-02 17:53:23', 'vc', 4, 1),
(14, '50.00', '2018-04-02 17:56:33', 'f', 4, 1),
(15, '10.00', '2018-04-02 17:56:41', 'y', 4, 1),
(16, '50.00', '2018-04-02 18:57:41', 'a', 4, 1),
(17, '100.00', '2018-04-02 18:57:48', 'd', 4, 1),
(18, '50.00', '2018-04-03 19:11:12', 'aa', 4, 1),
(19, '100.00', '2018-04-03 19:11:22', 'gdghfg', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `employee_location`
--

CREATE TABLE `employee_location` (
  `employee_location_id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `new_route_status` tinyint(1) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL,
  `location_info` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `employee_details_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_location`
--

INSERT INTO `employee_location` (`employee_location_id`, `address`, `datetime`, `new_route_status`, `latitude`, `location_info`, `longitude`, `employee_details_id`) VALUES
(1, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-05 17:42:10', 1, '19.2315943', NULL, '72.8621629', NULL),
(2, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-05 17:50:53', 0, '19.2315943', 'Order Taken from abc shop', '72.8621629', 1),
(3, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-05 18:07:02', 0, '19.2315619', NULL, '72.8621594', 1),
(4, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-05 18:21:49', 1, '19.2315755', NULL, '72.8621617', 3),
(5, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-05 18:41:32', 0, '19.2315784', NULL, '72.8621536', 3),
(6, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 11:01:35', 1, '19.231578', NULL, '72.8621444', 3),
(7, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 11:40:16', 1, '19.231563', NULL, '72.8621522', NULL),
(8, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 11:49:04', 0, '19.2315637', NULL, '72.8621517', 1),
(9, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 11:58:03', 0, '19.2315815', NULL, '72.8621487', 3),
(10, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 11:58:19', 0, '19.2315676', NULL, '72.8621482', 1),
(11, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:06:08', 1, '19.2315628', NULL, '72.8621522', 1),
(12, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:08:20', 0, '19.2315839', NULL, '72.8621485', 3),
(13, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:09:16', 1, '19.2315702', NULL, '72.862143', 1),
(14, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:10:57', 1, '19.2315656', NULL, '72.8621576', 1),
(15, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:26:09', 1, '19.2315689', NULL, '72.8621534', 1),
(16, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:41:54', 0, '19.2315656', NULL, '72.8621576', 3),
(17, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:45:32', 0, '19.2315656', NULL, '72.8621576', 3),
(18, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:47:13', 1, '19.2315656', NULL, '72.8621576', 3),
(19, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:48:41', 1, '19.231564', NULL, '72.8621515', 3),
(20, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:54:52', 1, '19.2315656', NULL, '72.8621576', 3),
(21, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 12:57:06', 1, '19.2315656', NULL, '72.8621576', 3),
(22, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 13:19:48', 0, '19.2315656', NULL, '72.8621576', 3),
(23, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 13:20:47', 1, '19.2315656', NULL, '72.8621576', 3),
(24, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 13:22:40', 1, '19.2315656', NULL, '72.8621576', 3),
(25, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 13:27:23', 1, '19.2315779', NULL, '72.8621473', 3),
(26, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 13:45:38', 1, '19.2315656', NULL, '72.8621576', 3),
(27, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 14:15:32', 0, '19.2315727', NULL, '72.8621491', 3),
(28, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:03:42', 1, '19.2315723', NULL, '72.8621699', 3),
(29, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:11:21', 1, '19.2315764', NULL, '72.8621504', 3),
(30, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:13:37', 1, '19.2315824', NULL, '72.8621364', 3),
(31, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:17:43', 0, '19.2315683', NULL, '72.8621519', 3),
(32, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:20:10', 1, '19.2315752', NULL, '72.8621585', 3),
(33, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:21:24', 0, '19.2316779', NULL, '72.8622312', 3),
(34, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:25:05', 1, '19.2315656', NULL, '72.8621576', 3),
(35, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:33:15', 1, '19.2315656', NULL, '72.8621576', 3),
(36, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:36:08', 1, '19.2315696', NULL, '72.8621551', 3),
(37, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:40:34', 1, '19.2315656', NULL, '72.8621576', 3),
(38, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:41:48', 1, '19.231581', NULL, '72.8621446', 3),
(39, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:46:33', 0, '19.2315654', NULL, '72.8621501', 3),
(40, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:52:38', 1, '19.2315656', NULL, '72.8621576', 3),
(41, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 15:56:46', 1, '19.2315656', NULL, '72.8621576', 3),
(42, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:00:44', 1, '19.2315721', NULL, '72.8621475', 3),
(43, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:03:50', 0, '19.2315795', NULL, '72.8621479', 3),
(44, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:06:59', 0, '19.2315692', NULL, '72.8621464', 3),
(45, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:09:19', 0, '19.2315681', NULL, '72.8621489', 3),
(46, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:11:21', 1, '19.2315682', NULL, '72.8621488', 3),
(47, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:12:42', 0, '19.231574', NULL, '72.8621551', 3),
(48, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:14:44', 1, '19.2315654', NULL, '72.86215', 3),
(49, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:16:44', 0, '19.2315761', NULL, '72.8621625', 3),
(50, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:23:21', 1, '19.2315681', NULL, '72.8621588', 3),
(51, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:38:35', 1, '19.2315777', NULL, '72.8621483', 3),
(52, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:47:44', 1, '19.2315771', NULL, '72.8621524', 3),
(53, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 16:51:56', 1, '19.2315691', NULL, '72.8621481', 3),
(54, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 17:00:47', 1, '19.2315656', NULL, '72.8621576', 3),
(55, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 17:20:08', 1, '19.2315774', NULL, '72.862145', 3),
(56, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 17:32:37', 1, '19.2315656', NULL, '72.8621576', 3),
(57, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 17:42:06', 0, '19.231569', NULL, '72.8621445', 3),
(58, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 17:50:04', 0, '19.2315772', NULL, '72.8621433', 3),
(59, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 18:08:46', 0, '19.2315724', NULL, '72.8621391', 3),
(60, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 18:24:00', 1, '19.2315743', NULL, '72.8621548', 3),
(61, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 18:46:48', 1, '19.2315646', NULL, '72.8621504', 3),
(62, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 18:55:21', 0, '19.2315673', NULL, '72.8621487', 3),
(63, '', '2018-03-06 19:13:29', 0, NULL, 'Order Taken from abc shop', NULL, 1),
(64, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 19:14:43', 1, '19.2315636', NULL, '72.8621501', 3),
(65, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 19:34:38', 0, '19.2315656', NULL, '72.8621576', 3),
(66, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 19:38:13', 0, '19.2315656', NULL, '72.8621576', 3),
(67, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-06 19:50:26', 1, '19.2315743', NULL, '72.862152', 3),
(68, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 10:55:55', 1, '19.2315656', NULL, '72.8621576', 3),
(69, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 10:57:33', 1, '19.2315656', NULL, '72.8621576', 3),
(70, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:02:22', 0, '19.2315656', 'New Business Added', '72.8621576', 1),
(71, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:04:31', 0, '19.2315656', 'Order Taken from Natraj Electronics', '72.8621576', 1),
(72, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:09:03', 0, '19.2315656', NULL, '72.8621576', 1),
(73, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:10:11', 1, '19.2315656', NULL, '72.8621576', 1),
(74, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:17:43', 1, '19.2315814', NULL, '72.8621556', 1),
(75, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:35:34', 0, '19.2315704', 'Order took for Delivery - Natraj Electronics', '72.8621432', 2),
(76, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:35:45', 0, '19.2315704', 'Order Delivered for ORD1000000003', '72.8621432', 2),
(77, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:37:05', 0, '19.2315704', 'Order Delivered for ORD1000000003', '72.8621432', 2),
(78, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:37:15', 0, '19.2315704', 'Order Delivered for ORD1000000003', '72.8621432', 2),
(79, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:37:50', 0, '19.2315704', 'Order Delivered for ORD1000000003', '72.8621432', 2),
(80, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:38:07', 0, '19.2315704', 'Order Delivered for ORD1000000003', '72.8621432', 2),
(81, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:38:47', 0, '19.2315704', 'Order Delivered for ORD1000000003', '72.8621432', 2),
(82, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:39:13', 1, '19.2315751', NULL, '72.8621503', 2),
(83, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:39:18', 0, '19.2315751', 'Order Delivered for ORD1000000003', '72.8621503', 2),
(84, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:43:09', 0, '19.2315751', 'Order Delivered for ORD1000000003', '72.8621503', 2),
(85, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:46:38', 1, '19.2315656', NULL, '72.8621576', 1),
(86, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:54:48', 0, '19.2315656', NULL, '72.8621576', 1),
(87, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 11:58:15', 1, '19.2315776', NULL, '72.8621503', 1),
(88, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:01:54', 0, '19.2315656', NULL, '72.8621576', 1),
(89, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:08:01', 0, '19.2315656', 'Payment made for ORD1000000003', '72.8621576', 1),
(90, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:11:43', 1, '19.231576', NULL, '72.8621455', 1),
(91, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:12:16', 0, '19.231576', 'Payment made for ORD1000000003', '72.8621455', 1),
(92, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:20:54', 0, '19.2315656', NULL, '72.8621576', 1),
(93, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:25:09', 1, '19.2315686', NULL, '72.8621483', 1),
(94, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:30:48', 0, '19.2315686', 'Payment made for ORD1000000003', '72.8621483', 1),
(95, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:43:47', 1, '19.2315766', NULL, '72.8621506', 1),
(96, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:46:11', 1, '19.2315683', NULL, '72.8621487', 1),
(97, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:48:04', 1, '19.2315728', NULL, '72.8621506', 1),
(98, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:51:31', 1, '19.2315636', NULL, '72.8621518', 1),
(99, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 12:55:21', 1, '19.2315656', NULL, '72.8621576', 1),
(100, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:06:44', 1, '19.2315656', NULL, '72.8621576', 1),
(101, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:12:40', 0, '19.2315791', NULL, '72.8621505', 1),
(102, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:13:46', 1, '19.2315638', NULL, '72.8621515', 1),
(103, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:18:17', 0, '19.2315638', 'New Business Added', '72.8621515', 4),
(104, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:19:04', 0, '19.2315713', NULL, '72.8621445', 3),
(105, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:20:14', 0, '19.231571', NULL, '72.8621437', 4),
(106, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:21:57', 1, '19.2316528', NULL, '72.862141', 2),
(107, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:30:03', 0, '19.2315713', 'Order Taken from Shop Name', '72.8621445', 4),
(108, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:30:42', 0, '19.2316528', 'Order took for Delivery - Shop Name', '72.862141', 6),
(109, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 13:30:55', 0, '19.2316528', 'Order Delivered for ORD1000000004', '72.862141', 6),
(110, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 14:07:16', 0, '19.2315687', NULL, '72.862148', 1),
(111, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 14:44:04', 1, '19.2315901', NULL, '72.8621409', 4),
(112, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:03:48', 0, '19.2315656', 'New Business Added', '72.8621576', 4),
(113, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:07:31', 0, '19.2315656', 'Order Taken from Bhima Electronic s', '72.8621576', 4),
(114, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:11:09', 1, '19.231582', NULL, '72.8621434', 4),
(115, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:12:40', 0, '19.2315787', NULL, '72.8621418', 5),
(116, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:14:02', 0, '19.2315787', 'Order took for Delivery - Bhima Electronic s', '72.8621418', 6),
(117, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:18:40', 0, '19.231582', 'Order Taken from Bhima Electronic s', '72.8621434', 4),
(118, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:18:57', 0, '19.231582', 'Order took for Delivery - Bhima Electronic s', '72.8621434', 6),
(119, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:20:13', 0, '19.2315849', NULL, '72.8621428', 4),
(120, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:23:51', 0, '19.2315849', 'Order Delivered for ORD1000000005', '72.8621428', 6),
(121, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:23:53', 0, '19.2315849', 'Order Delivered for ORD1000000006', '72.8621428', 6),
(122, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:23:56', 0, '19.2315849', 'Order Delivered for ORD1000000005', '72.8621428', 6),
(123, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:24:02', 0, '19.2315849', 'Order Delivered for ORD1000000006', '72.8621428', 6),
(124, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:32:00', 1, '19.2315749', NULL, '72.862167', 4),
(125, '', '2018-03-07 15:37:50', 0, NULL, 'Order Delivered for ORD1000000005', NULL, 6),
(126, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:38:55', 1, '19.2315691', NULL, '72.8621479', 6),
(127, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:40:39', 1, '19.2315656', NULL, '72.8621576', 6),
(128, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:40:43', 0, '19.2315656', 'Order Delivered for ORD1000000005', '72.8621576', 6),
(129, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:44:48', 0, '19.2315656', NULL, '72.8621576', 6),
(130, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 15:54:16', 0, '19.2315851', NULL, '72.8621443', 5),
(131, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:01:37', 0, '19.2315685', NULL, '72.8621484', 5),
(132, '', '2018-03-07 16:08:58', 0, NULL, 'Order Delivered for ORD1000000005', NULL, 6),
(133, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:09:52', 1, '19.2315656', NULL, '72.8621576', 6),
(134, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:10:14', 0, '19.2315694', NULL, '72.8621466', 5),
(135, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:14:34', 0, '19.2315691', NULL, '72.862148', 5),
(136, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:17:56', 1, '19.2315656', NULL, '72.8621576', 4),
(137, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:19:13', 0, '19.2315656', 'Order Taken from Bhima Electronic s', '72.8621576', 4),
(138, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:20:29', 0, '19.2315656', 'Order took for Delivery - Bhima Electronic s', '72.8621576', 6),
(139, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:20:54', 0, '19.2315656', 'Order Delivered for ORD1000000007', '72.8621576', 6),
(140, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:27:17', 0, '19.2315656', 'Replacement Done for Bhima Electronic s', '72.8621576', 4),
(141, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:29:15', 0, '19.2315656', 'Return Taken for ORD1000000007', '72.8621576', 4),
(142, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:45:15', 0, '19.2315656', 'Return Taken for ORD1000000007', '72.8621576', 4),
(143, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:47:57', 0, '19.2315656', 'Return Taken for ORD1000000005', '72.8621576', 4),
(144, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:53:54', 0, '19.2315656', 'Return Taken for ORD1000000005', '72.8621576', 4),
(145, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 16:58:57', 0, '19.2315688', NULL, '72.8621444', 4),
(146, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 17:08:00', 1, '19.2315656', NULL, '72.8621576', 5),
(147, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 17:10:00', 1, '19.2315791', NULL, '72.8621424', 5),
(148, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 17:20:53', 0, '19.2315938', NULL, '72.8621404', 5),
(149, '', '2018-03-07 17:40:54', 0, NULL, 'Order Taken from abc shop', NULL, 1),
(150, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 17:41:28', 1, '19.2315923', NULL, '72.8621539', 1),
(151, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 17:42:21', 0, '19.2315923', 'Order Taken from Natraj Electronics', '72.8621539', 1),
(152, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 17:44:37', 0, '19.2315923', 'Order took for Delivery - abc shop', '72.8621539', 2),
(153, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 18:03:17', 1, '19.2315656', NULL, '72.8621576', 3),
(154, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 18:05:27', 1, '19.2315862', NULL, '72.862151', 3),
(155, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 18:33:32', 0, '19.2315862', 'Order Delivered for ORD1000000001', '72.862151', 2),
(156, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 19:29:41', 1, '19.2315829', NULL, '72.8621422', 2),
(157, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-07 19:30:48', 0, '19.2315829', 'Order Taken from abc shop', '72.8621422', 1),
(158, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:15:48', 1, '19.2315818', NULL, '72.8621473', 1),
(159, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:16:30', 0, '19.2315818', 'Return Taken for ORD1000000004', '72.8621473', 4),
(160, 'Sunayar Building, Overipada, Parbat Nagar, Borivali East, Mumbai, Maharashtra 400068, India', '2018-03-08 10:17:49', 0, '19.2419484', NULL, '72.8620234', 5),
(161, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:19:49', 0, '19.2315795', NULL, '72.8621506', 5),
(162, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:25:50', 1, '19.2315656', NULL, '72.8621576', 5),
(163, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:27:59', 1, '19.2315656', NULL, '72.8621576', 5),
(164, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:34:06', 1, '19.2315679', NULL, '72.8621485', 5),
(165, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:38:06', 1, '19.2315656', NULL, '72.8621576', 5),
(166, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:43:24', 0, '19.2315656', 'Replacement Done for Shop Name', '72.8621576', 6),
(167, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:45:22', 0, '19.2315656', 'Order Taken from Bhima Electronic s', '72.8621576', 4),
(168, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:46:13', 0, '19.2315656', 'Order took for Delivery - Bhima Electronic s', '72.8621576', 6),
(169, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:46:17', 0, '19.2315656', 'Order Delivered for ORD1000000011', '72.8621576', 6),
(170, '', '2018-03-08 10:54:02', 0, NULL, 'Return Taken for ORD1000000011', NULL, 4),
(171, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:54:49', 1, '19.2315656', NULL, '72.8621576', 4),
(172, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:58:34', 0, '19.2315656', 'Order Taken from Bhima Electronic s', '72.8621576', 4),
(173, '4, Kasthurba Rd, Chinchpada, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-08 10:59:12', 1, '19.2307243', NULL, '72.8620712', 6),
(174, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:59:34', 0, '19.2315656', 'Order took for Delivery - Bhima Electronic s', '72.8621576', 6),
(175, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 10:59:50', 0, '19.2315656', 'Order Delivered for ORD1000000012', '72.8621576', 6),
(176, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:03:19', 1, '19.2315656', NULL, '72.8621576', 5),
(177, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:09:14', 1, '19.2315679', NULL, '72.862149', 4),
(178, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:10:24', 0, '19.2315656', 'Order Taken from Bhima Electronic s', '72.8621576', 4),
(179, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:11:19', 0, '19.2315679', 'Order took for Delivery - Bhima Electronic s', '72.862149', 6),
(180, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:11:23', 0, '19.2315679', 'Order Delivered for ORD1000000013', '72.862149', 6),
(181, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:12:34', 1, '19.2315656', NULL, '72.8621576', 6),
(182, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:15:12', 0, '19.2315656', 'Order Taken from Shop Name', '72.8621576', 4),
(183, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:15:31', 0, '19.2315656', 'Order took for Delivery - Shop Name', '72.8621576', 6),
(184, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:15:35', 0, '19.2315656', 'Order Delivered for ORD1000000014', '72.8621576', 6),
(185, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:17:50', 0, '19.2315656', 'Order Delivered for ORD1000000014', '72.8621576', 6),
(186, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:18:08', 0, '19.2315656', 'Order Delivered for ORD1000000014', '72.8621576', 6),
(187, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:18:40', 0, '19.2315656', 'Order Delivered for ORD1000000014', '72.8621576', 6),
(188, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:24:59', 0, '19.2315656', 'Return Taken for ORD1000000013', '72.8621576', 4),
(189, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:25:43', 0, '19.2315656', 'Replacement Done for Bhima Electronic s', '72.8621576', 4),
(190, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:25:55', 0, '19.2315656', 'Replacement Done for Bhima Electronic s', '72.8621576', 4),
(191, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:35:31', 0, '19.2315656', NULL, '72.8621576', 4),
(192, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:35:33', 0, '19.2315656', 'Replacement Done for Bhima Electronic s', '72.8621576', 4),
(193, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 11:46:49', 1, '19.2315656', NULL, '72.8621576', 4),
(194, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 14:33:47', 1, '19.2316635', NULL, '72.8620845', 4),
(195, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 14:42:09', 1, '19.2315656', NULL, '72.8621576', 5),
(196, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 14:44:10', 0, '19.2315779', NULL, '72.8621461', 5),
(197, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 15:11:49', 1, '19.2315828', NULL, '72.8621529', 5),
(198, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 15:14:17', 0, '19.2315828', 'Order Taken from Bhima Electronic s', '72.8621529', 4),
(199, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 16:14:20', 1, '19.2315722', NULL, '72.86214', 4),
(200, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 16:14:56', 0, '19.2315722', 'Order Taken from Bhima Electronic s', '72.86214', 4),
(201, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 16:16:59', 0, '19.2315722', 'Order took for Delivery - Bhima Electronic s', '72.86214', 6),
(202, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 16:17:51', 0, '19.2315722', 'Order Delivered for ORD1000000015', '72.86214', 6),
(203, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 19:03:53', 1, '19.2315682', NULL, '72.8621483', 5),
(204, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 19:04:42', 0, '19.2315682', 'Order Taken from Bhima Electronic s', '72.8621483', 4),
(205, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 19:07:08', 1, '19.2315768', NULL, '72.8621434', 5),
(206, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 19:11:26', 1, '19.2315694', NULL, '72.8621488', 5),
(207, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 19:25:22', 0, '19.2315824', NULL, '72.8621504', 5),
(208, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-08 19:33:28', 0, '19.2315767', NULL, '72.8621476', 4),
(209, 'Shivam Building, 31, Mahatma Gandhi Rd, Chinchpada, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-09 12:08:14', 1, '19.2316793', NULL, '72.8621214', 5),
(210, 'Shivam Building, 31, Mahatma Gandhi Rd, Chinchpada, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-09 12:09:12', 0, '19.2316793', 'Order Taken from Bhima Electronic s', '72.8621214', 4),
(211, 'Shivam Building, 31, Mahatma Gandhi Rd, Chinchpada, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-09 12:14:06', 0, '19.2316793', 'Order Taken from Bhima Electronic s', '72.8621214', 4),
(212, '', '2018-03-09 12:29:51', 1, '19.2308659', NULL, '72.8635481', 4),
(213, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 12:31:16', 0, '19.2315837', NULL, '72.8621426', 5),
(214, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 12:33:01', 0, '19.2315671', NULL, '72.8621488', 5),
(215, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 12:43:44', 0, '19.2315633', NULL, '72.8621509', 5),
(216, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 12:45:36', 0, '19.2315769', NULL, '72.8621728', 5),
(217, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 12:50:56', 0, '19.2315656', NULL, '72.8621576', 5),
(218, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:03:51', 0, '19.2315809', NULL, '72.8621476', 5),
(219, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:10:15', 0, '19.2315995', NULL, '72.8621693', 5),
(220, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:14:31', 0, '19.2315656', NULL, '72.8621576', 5),
(221, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:16:30', 0, '19.2315743', NULL, '72.862152', 5),
(222, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:24:45', 0, '19.2315656', NULL, '72.8621576', 5),
(223, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:30:25', 0, '19.231563', NULL, '72.8621513', 5),
(224, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:32:27', 0, '19.2315656', NULL, '72.8621576', 5),
(225, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:33:26', 1, '19.2315656', NULL, '72.8621576', 5),
(226, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:37:58', 1, '19.2315656', NULL, '72.8621576', 5),
(227, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:38:33', 0, '19.2315996', NULL, '72.8621671', 5),
(228, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 13:41:44', 0, '19.2315639', NULL, '72.8621517', 5),
(229, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 14:01:05', 1, '19.2315689', NULL, '72.8621546', 5),
(230, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 14:30:59', 1, '19.2315696', NULL, '72.8621453', 5),
(231, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 14:34:32', 1, '19.2315631', NULL, '72.8621522', 5),
(232, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 14:36:16', 1, '19.2315656', NULL, '72.8621576', 5),
(233, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 14:42:02', 0, '19.2315656', NULL, '72.8621576', 5),
(234, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 14:43:47', 1, '19.2315696', NULL, '72.8621455', 5),
(235, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 14:49:14', 1, '19.2315871', NULL, '72.8621396', 5),
(236, 'Gokul Apartments, Mahatma Gandhi Rd, Chinchpada, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-09 15:03:05', 1, '19.231138', NULL, '72.8619476', 5),
(237, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 15:06:27', 1, '19.2315633', NULL, '72.8621509', 5),
(238, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 15:43:22', 1, '19.2315685', NULL, '72.8621608', 5),
(239, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 15:47:39', 1, '19.2315656', NULL, '72.8621576', 5),
(240, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 15:50:57', 0, '19.2315637', NULL, '72.8621516', 5),
(241, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:00:34', 1, '19.2315634', NULL, '72.862152', 5),
(242, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:02:08', 0, '19.2315656', NULL, '72.8621576', 5),
(243, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:13:09', 1, '19.23157', NULL, '72.8621447', 5),
(244, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:18:22', 1, '19.2315642', NULL, '72.8621508', 5),
(245, 'Sunayar Building, Overipada, Parbat Nagar, Borivali East, Mumbai, Maharashtra 400068, India', '2018-03-09 16:21:44', 0, '19.2419484', NULL, '72.8620234', 5),
(246, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:24:43', 0, '19.2315678', NULL, '72.8621448', 5),
(247, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:39:13', 0, '19.2315656', NULL, '72.8621576', 5),
(248, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:41:07', 0, '19.2315656', NULL, '72.8621576', 5),
(249, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:46:54', 1, '19.2315634', NULL, '72.862152', 5),
(250, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:47:00', 1, '19.2315829', NULL, '72.8621474', 5),
(251, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 16:59:16', 0, '19.2315656', NULL, '72.8621576', 5),
(252, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:00:54', 0, '19.2315656', NULL, '72.8621576', 5),
(253, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:04:57', 0, '19.2315656', NULL, '72.8621576', 5),
(254, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:08:36', 0, '19.231578', NULL, '72.8621428', 5),
(255, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:16:02', 0, '19.2315656', NULL, '72.8621576', 5),
(256, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:16:26', 0, '19.2315656', NULL, '72.8621576', 5),
(257, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:16:26', 0, '19.2315656', NULL, '72.8621576', 5),
(258, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:22:57', 1, '19.2315765', NULL, '72.8621443', 5),
(259, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:27:21', 0, '19.2315656', NULL, '72.8621576', 5),
(260, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:35:34', 1, '19.2315656', NULL, '72.8621576', 5),
(261, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:35:56', 0, '19.2315685', NULL, '72.8621436', 5),
(262, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:43:10', 0, '19.2315694', NULL, '72.8621643', 5),
(263, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 17:55:36', 1, '19.2315656', NULL, '72.8621576', 5),
(264, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:02:43', 1, '19.2315656', NULL, '72.8621576', 5),
(265, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:09:46', 1, '19.2315656', NULL, '72.8621576', 5),
(266, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:19:34', 1, '19.2315656', NULL, '72.8621576', 5),
(267, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:22:35', 0, '19.2315656', NULL, '72.8621576', 5),
(268, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:24:59', 1, '19.2315641', NULL, '72.8621514', 5),
(269, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:32:42', 0, '19.2315656', NULL, '72.8621576', 5),
(270, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:35:00', 1, '19.2315656', NULL, '72.8621576', 5),
(271, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:41:03', 1, '19.2315636', NULL, '72.8621518', 5),
(272, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:51:27', 1, '19.2315656', NULL, '72.8621576', 5),
(273, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:53:00', 1, '19.2315713', NULL, '72.8621504', 5),
(274, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 18:56:12', 0, '19.2315656', NULL, '72.8621576', 5),
(275, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:00:12', 1, '19.2315642', NULL, '72.8621515', 5),
(276, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:01:20', 1, '19.2315655', NULL, '72.8621499', 5),
(277, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:18:23', 1, '19.2315656', NULL, '72.8621576', 5),
(278, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:21:46', 1, '19.2315637', NULL, '72.8621505', 5),
(279, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:23:15', 1, '19.2315642', NULL, '72.8621501', 5),
(280, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:29:26', 1, '19.2315635', NULL, '72.8621507', 5),
(281, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:37:14', 0, '19.2315639', NULL, '72.8621504', 5),
(282, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:46:52', 0, '19.2315743', NULL, '72.862152', 5),
(283, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-09 19:51:53', 1, '19.2315656', NULL, '72.8621576', 5),
(284, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 10:10:33', 0, '19.2315683', NULL, '72.8621485', 5),
(285, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 10:28:51', 1, '19.2315656', NULL, '72.8621576', 5),
(286, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 10:34:35', 1, '19.2315686', NULL, '72.8621484', 5),
(287, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 10:38:58', 1, '19.2315656', NULL, '72.8621576', 5),
(288, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 10:43:04', 1, '19.2315656', NULL, '72.8621576', 5),
(289, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 10:50:29', 1, '19.2315656', NULL, '72.8621576', 5),
(290, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 10:55:37', 1, '19.2315695', NULL, '72.8621463', 5),
(291, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:01:16', 0, '19.2315679', NULL, '72.8621488', 5),
(292, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:04:37', 1, '19.2315681', NULL, '72.8621469', 5),
(293, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:08:02', 1, '19.2315706', NULL, '72.8621418', 5),
(294, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:15:20', 1, '19.2315656', NULL, '72.8621576', 5),
(295, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:26:39', 1, '19.2315656', NULL, '72.8621576', 5),
(296, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:30:33', 1, '19.2315701', NULL, '72.8621453', 5),
(297, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:31:25', 1, '19.2315687', NULL, '72.862148', 5),
(298, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:38:35', 1, '19.2315687', NULL, '72.8621477', 5),
(299, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:41:39', 1, '19.2315704', NULL, '72.8621449', 5);
INSERT INTO `employee_location` (`employee_location_id`, `address`, `datetime`, `new_route_status`, `latitude`, `location_info`, `longitude`, `employee_details_id`) VALUES
(300, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 11:46:45', 1, '19.2315677', NULL, '72.8621487', 5),
(301, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 12:06:09', 1, '19.231578', NULL, '72.862143', 5),
(302, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 12:15:38', 1, '19.2315673', NULL, '72.862147', 5),
(303, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 12:18:43', 0, '19.231569', NULL, '72.8621461', 5),
(304, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 12:31:25', 0, '19.2315639', NULL, '72.8621516', 5),
(305, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 12:38:04', 1, '19.2315656', NULL, '72.8621576', 5),
(306, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 12:51:58', 0, '19.2315656', NULL, '72.8621576', 5),
(307, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 13:02:32', 1, '19.2315779', NULL, '72.862141', 5),
(308, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 13:18:36', 0, '19.2315656', NULL, '72.8621576', 5),
(309, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 13:40:25', 1, '19.2315754', NULL, '72.8621412', 4),
(310, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 13:40:43', 0, '19.2315754', 'Order Taken from Bhima Electronic s', '72.8621412', 4),
(311, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 13:45:39', 0, '19.2315754', 'Order took for Delivery - Bhima Electronic s', '72.8621412', 6),
(312, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 13:52:48', 0, '19.2315656', NULL, '72.8621576', 6),
(313, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 13:55:47', 0, '19.2315656', NULL, '72.8621576', 5),
(314, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 14:00:32', 1, '19.231568', NULL, '72.8621483', 5),
(315, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 14:06:35', 0, '19.2315688', NULL, '72.8621479', 5),
(316, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 15:13:02', 1, '19.2315656', NULL, '72.8621576', 5),
(317, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 15:26:20', 1, '19.2315653', NULL, '72.8621497', 5),
(318, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 15:30:22', 1, '19.2315715', NULL, '72.8621505', 5),
(319, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 15:48:01', 1, '19.2315702', NULL, '72.8621457', 5),
(320, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 15:53:54', 1, '19.231573', NULL, '72.8621549', 5),
(321, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 15:59:48', 1, '19.2315656', NULL, '72.8621576', 5),
(322, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 16:09:10', 1, '19.2315656', NULL, '72.8621576', 5),
(323, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 16:23:50', 1, '19.2315656', NULL, '72.8621576', 5),
(324, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 16:27:04', 1, '19.2315719', NULL, '72.8621479', 5),
(325, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 16:33:50', 1, '19.2315683', NULL, '72.8621486', 5),
(326, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 16:42:25', 1, '19.2315767', NULL, '72.862145', 5),
(327, '', '2018-03-10 16:54:27', 0, NULL, 'Order Taken from Bhima Electronic s', NULL, 4),
(328, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 16:55:02', 1, '19.2316868', NULL, '72.8621052', 4),
(329, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 17:05:17', 0, '19.2315733', NULL, '72.8621506', 4),
(330, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 17:28:15', 1, '19.2315656', NULL, '72.8621576', 5),
(331, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 17:36:17', 0, '19.2315847', NULL, '72.8621449', 5),
(332, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 17:42:06', 1, '19.2315656', NULL, '72.8621576', 5),
(333, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 17:56:10', 0, '19.2315656', NULL, '72.8621576', 5),
(334, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 18:08:54', 1, '19.2315685', NULL, '72.8621481', 5),
(335, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 18:18:10', 0, '19.2315805', NULL, '72.8621557', 5),
(336, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 18:36:28', 0, '19.2315799', NULL, '72.8621518', 5),
(337, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 18:38:06', 1, '19.2315656', NULL, '72.8621576', 5),
(338, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 18:39:03', 1, '19.2315656', NULL, '72.8621576', 5),
(339, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 18:42:56', 0, '19.2315678', NULL, '72.8621486', 5),
(340, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 18:49:19', 1, '19.2315656', NULL, '72.8621576', 5),
(341, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-10 18:52:53', 1, '19.2315656', NULL, '72.8621576', 5),
(342, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 10:42:43', 1, '19.2315686', NULL, '72.8620731', 5),
(343, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 10:44:08', 1, '19.2315755', NULL, '72.8620752', 5),
(344, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 10:57:56', 1, '19.2315751', NULL, '72.8620753', 5),
(345, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:08:55', 1, '19.2315742', NULL, '72.8620739', 5),
(346, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:22:00', 0, '19.2315739', NULL, '72.8620752', 5),
(347, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:27:15', 1, '19.2315885', NULL, '72.8620881', 5),
(348, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:30:01', 0, '19.2315647', NULL, '72.8620718', 5),
(349, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:35:54', 0, '19.2315647', 'Order Taken from Bhima Electronic s', '72.8620718', 4),
(350, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:43:17', 1, '19.2315777', NULL, '72.8620756', 5),
(351, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:51:57', 1, '19.2315744', NULL, '72.8620748', 5),
(352, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:53:55', 1, '19.2315731', NULL, '72.8620749', 5),
(353, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 11:58:34', 1, '19.2315811', NULL, '72.8620769', 5),
(354, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:18:39', 1, '19.2315647', NULL, '72.8620718', 5),
(355, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:20:08', 1, '19.2315647', NULL, '72.8620718', 5),
(356, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:25:06', 1, '19.2315729', NULL, '72.8620728', 5),
(357, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:27:05', 1, '19.2315647', NULL, '72.8620718', 5),
(358, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:36:23', 1, '19.2315679', NULL, '72.8620705', 5),
(359, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:40:14', 0, '19.2315725', NULL, '72.8620747', 5),
(360, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:46:35', 1, '19.2315647', NULL, '72.8620718', 5),
(361, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:48:34', 1, '19.2315786', NULL, '72.8620779', 5),
(362, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 12:59:23', 0, '19.2315839', NULL, '72.8620799', 5),
(363, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 13:17:21', 1, '19.2315647', NULL, '72.8620718', 5),
(364, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 13:22:42', 0, '19.2315762', NULL, '72.8620723', 5),
(365, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 13:35:48', 0, '19.2315647', NULL, '72.8620718', 5),
(366, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 13:42:02', 1, '19.2315787', NULL, '72.8620782', 5),
(367, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 13:48:10', 0, '19.2315697', NULL, '72.8620741', 5),
(368, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 13:52:18', 0, '19.2315647', NULL, '72.8620718', 5),
(369, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 14:22:11', 1, '19.2315811', NULL, '72.8620792', 5),
(370, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 14:23:13', 1, '19.2315762', NULL, '72.8620767', 5),
(371, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 14:43:50', 0, '19.2315759', NULL, '72.8620763', 5),
(372, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 15:22:10', 1, '19.2315704', NULL, '72.8620745', 5),
(373, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 15:31:29', 1, '19.2315647', NULL, '72.8620718', 5),
(374, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 15:32:31', 1, '19.2315828', NULL, '72.8620795', 5),
(375, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 15:36:14', 1, '19.2315748', NULL, '72.8620757', 5),
(376, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 15:47:24', 1, '19.2315787', NULL, '72.862077', 5),
(377, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:02:03', 0, '19.2315647', NULL, '72.8620718', 5),
(378, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:06:19', 1, '19.2315682', NULL, '72.8620716', 5),
(379, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:17:08', 1, '19.2315698', NULL, '72.8620739', 5),
(380, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:19:06', 0, '19.2315647', NULL, '72.8620718', 5),
(381, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:27:56', 1, '19.2315747', NULL, '72.8620761', 6),
(382, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:29:50', 1, '19.2315693', NULL, '72.8620745', 6),
(383, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:32:55', 1, '19.2315758', NULL, '72.8620761', 6),
(384, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:34:28', 1, '19.2315647', NULL, '72.8620718', 6),
(385, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:35:22', 1, '19.2315692', NULL, '72.8620745', 6),
(386, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:36:43', 1, '19.2315694', NULL, '72.862074', 6),
(387, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:40:22', 1, '19.2315697', NULL, '72.8620749', 6),
(388, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:47:16', 1, '19.2315704', NULL, '72.8620744', 5),
(389, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:54:19', 1, '19.2315647', NULL, '72.8620718', 4),
(390, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 16:57:52', 1, '19.2315755', NULL, '72.8620743', 4),
(391, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:00:48', 1, '19.2315647', NULL, '72.8620718', 4),
(392, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:02:38', 1, '19.231569', NULL, '72.8620746', 6),
(393, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:06:02', 1, '19.2315698', NULL, '72.8620739', 6),
(394, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:11:52', 1, '19.2315574', NULL, '72.8620935', 4),
(395, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:13:41', 1, '19.2315605', NULL, '72.8620996', 4),
(396, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:16:00', 1, '19.2315647', NULL, '72.8620718', 5),
(397, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:20:17', 1, '19.2315642', NULL, '72.8620907', 5),
(398, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:24:03', 0, '19.2315647', NULL, '72.8620718', 5),
(399, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:32:06', 1, '19.2315654', NULL, '72.86208', 5),
(400, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:40:33', 1, '19.2315787', NULL, '72.8620765', 5),
(401, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:41:29', 1, '19.2315805', NULL, '72.8620782', 5),
(402, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 17:59:51', 0, '19.23159', NULL, '72.8620843', 6),
(403, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:01:40', 0, '19.23159', 'Order Delivered for ORD1000000020', '72.8620843', 6),
(404, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:11:04', 0, '19.23159', 'Order Taken from Bhima Electronics', '72.8620843', 4),
(405, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:11:54', 0, '19.23159', 'Order took for Delivery - Bhima Electronics', '72.8620843', 6),
(406, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:15:02', 0, '19.23159', 'Order took for Delivery - Bhima Electronics', '72.8620843', 6),
(407, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:15:10', 0, '19.23159', 'Order Delivered for ORD1000000023', '72.8620843', 6),
(408, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:15:17', 0, '19.23159', 'Order Delivered for ORD1000000023', '72.8620843', 6),
(409, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:15:19', 0, '19.23159', 'Order Delivered for ORD1000000023', '72.8620843', 6),
(410, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:17:02', 0, '19.23159', 'Order Delivered for ORD1000000023', '72.8620843', 6),
(411, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:24:24', 0, '19.23159', 'Order Delivered for ORD1000000023', '72.8620843', 6),
(412, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:26:19', 0, '19.2315764', NULL, '72.8620765', 6),
(413, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:32:56', 0, '19.2315697', NULL, '72.8620748', 6),
(414, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 18:54:24', 1, '19.2316629', NULL, '72.8621543', 5),
(415, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:07:28', 0, '19.2315799', NULL, '72.8620756', 5),
(416, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:08:16', 0, '19.2315799', 'Order Taken from Bhima Electronics', '72.8620756', 4),
(417, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:25:10', 0, '19.231569', NULL, '72.8620746', 4),
(418, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:27:12', 1, '19.2316482', NULL, '72.8621048', 4),
(419, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:28:38', 1, '19.2316575', NULL, '72.8621359', 4),
(420, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:32:17', 0, '19.2316482', 'Order Taken from Bhima Electronics', '72.8621048', 4),
(421, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:38:13', 0, '19.2316575', 'Order Taken from Shop Name', '72.8621359', 4),
(422, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:41:04', 0, '19.2316575', 'Order Taken from Bhima Electronics', '72.8621359', 4),
(423, 'Shivam Building, 31, Mahatma Gandhi Rd, Chinchpada, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-12 19:46:16', 0, '19.2316864', NULL, '72.8621388', 5),
(424, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 19:56:09', 1, '19.2315735', NULL, '72.8620754', 5),
(425, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-12 20:04:54', 0, '19.2316575', 'Order Taken from Bhima Electronics', '72.8621359', 4),
(426, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 11:00:42', 0, '19.2315742', 'Order Taken from Bhima Electronics', '72.8620773', 4),
(427, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 11:02:43', 0, '19.2315742', 'Order Taken from Shop Name', '72.8620773', 4),
(428, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 11:03:36', 0, '19.2315742', 'Order Taken from Bhima Electronics', '72.8620773', 4),
(429, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 11:42:05', 1, '19.231574', NULL, '72.8620728', 4),
(430, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 11:48:45', 0, '19.231574', 'Order took for Delivery - Bhima Electronics', '72.8620728', 6),
(431, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 11:49:29', 0, '19.231574', 'Order Delivered for ORD1000000003', '72.8620728', 6),
(432, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 11:54:50', 0, '19.231574', 'Order took for Delivery - Shop Name', '72.8620728', 6),
(433, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:30:40', 1, '19.2315749', NULL, '72.8620746', 4),
(434, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:32:09', 0, '19.2315749', 'Order Taken from Bhima Electronics', '72.8620746', 4),
(435, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:32:55', 0, '19.2315749', 'Order took for Delivery - Bhima Electronics', '72.8620746', 6),
(436, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:33:00', 0, '19.2315749', 'Order Delivered for ORD1000000004', '72.8620746', 6),
(437, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:33:22', 0, '19.2315749', 'Order Delivered for ORD1000000003', '72.8620746', 6),
(438, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:33:36', 0, '19.2315749', 'Order Delivered for ORD1000000003', '72.8620746', 6),
(439, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:33:44', 0, '19.2315749', 'Order Delivered for ORD1000000003', '72.8620746', 6),
(440, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:34:43', 0, '19.2315647', NULL, '72.8620718', 6),
(441, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:36:50', 1, '19.231574', NULL, '72.8620755', 4),
(442, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:37:58', 1, '19.2315926', NULL, '72.8620944', 4),
(443, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:38:42', 0, '19.2315647', 'Order Delivered for ORD1000000002', '72.8620718', 6),
(444, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:42:01', 0, '19.2315926', 'Payment made for ORD1000000004', '72.8620944', 4),
(445, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 12:43:37', 0, '19.2315926', 'Payment made for ORD1000000004', '72.8620944', 4),
(446, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 13:21:00', 1, '19.2315695', NULL, '72.8620738', 5),
(447, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 13:30:22', 0, '19.2315647', NULL, '72.8620718', 5),
(448, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 13:54:20', 0, '19.2315796', NULL, '72.8620769', 5),
(449, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 14:38:10', 1, '19.2315696', NULL, '72.862074', 5),
(450, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 14:41:55', 0, '19.2315675', NULL, '72.8620727', 5),
(451, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 14:43:56', 1, '19.2315768', NULL, '72.8620887', 5),
(452, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 14:45:39', 1, '19.2315753', NULL, '72.8620851', 5),
(453, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 14:49:11', 1, '19.2315759', NULL, '72.862086', 5),
(454, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 14:53:33', 1, '19.2315748', NULL, '72.8620835', 5),
(455, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 14:59:06', 1, '19.2315761', NULL, '72.8620875', 5),
(456, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:00:25', 1, '19.2315789', NULL, '72.8620943', 5),
(457, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:02:25', 0, '19.2315686', NULL, '72.8620734', 5),
(458, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:15:24', 1, '19.2315698', NULL, '72.8620728', 5),
(459, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:25:19', 1, '19.2315698', NULL, '72.8620736', 5),
(460, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:30:50', 1, '19.2315696', NULL, '72.8620738', 5),
(461, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:39:25', 1, '19.2315673', NULL, '72.8620723', 5),
(462, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:52:29', 1, '19.2315647', NULL, '72.8620718', 5),
(463, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:54:11', 0, '19.2315751', NULL, '72.8620829', 5),
(464, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:55:20', 1, '19.231575', NULL, '72.8620829', 5),
(465, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 15:59:45', 0, '19.2315709', NULL, '72.8620746', 5),
(466, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:01:24', 1, '19.23157', NULL, '72.8620737', 5),
(467, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:04:45', 1, '19.2315697', NULL, '72.8620741', 5),
(468, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:08:58', 1, '19.2315695', NULL, '72.8620734', 5),
(469, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:12:12', 1, '19.2315755', NULL, '72.862083', 5),
(470, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:15:45', 1, '19.2315688', NULL, '72.8620742', 5),
(471, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:23:53', 1, '19.2315647', NULL, '72.8620718', 2),
(472, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:26:01', 1, '19.2315647', NULL, '72.8620718', 2),
(473, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:27:55', 1, '19.2315697', NULL, '72.8620735', 3),
(474, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:28:45', 1, '19.2315771', NULL, '72.8620766', 3),
(475, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:43:12', 1, '19.2315647', NULL, '72.8620718', 1),
(476, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:45:15', 0, '19.2315728', NULL, '72.8620792', 1),
(477, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:46:35', 1, '19.2315732', NULL, '72.8620823', 1),
(478, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:47:59', 0, '19.2315647', NULL, '72.8620718', 1),
(479, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:49:06', 1, '19.2315647', NULL, '72.8620718', 1),
(480, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:51:10', 0, '19.2315647', NULL, '72.8620718', 1),
(481, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:53:07', 1, '19.2315693', NULL, '72.8620734', 1),
(482, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:57:03', 1, '19.2315647', NULL, '72.8620718', 1),
(483, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 16:58:36', 1, '19.2315697', NULL, '72.8620739', 1),
(484, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:00:05', 0, '19.2315697', 'Order Taken from Natraj Electronics', '72.8620739', 1),
(485, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:00:37', 0, '19.2315717', NULL, '72.8620722', 3),
(486, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:07:24', 1, '19.2315647', NULL, '72.8620718', 3),
(487, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:14:46', 1, '19.2315647', NULL, '72.8620718', 3),
(488, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:18:51', 0, '19.2315647', 'Order took for Delivery - Natraj Electronics', '72.8620718', 2),
(489, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:18:56', 0, '19.2315647', 'Order Delivered for ORD1000000005', '72.8620718', 2),
(490, '', '2018-03-13 17:24:27', 0, NULL, 'Order Delivered for ORD1000000005', NULL, 2),
(491, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:24:54', 1, '19.2315697', NULL, '72.862074', 1),
(492, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:25:09', 0, '19.2315697', 'Order Taken from Natraj Electronics', '72.862074', 1),
(493, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:27:49', 0, '19.2315697', 'Order took for Delivery - Natraj Electronics', '72.862074', 2),
(494, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:27:53', 0, '19.2315697', 'Order Delivered for ORD1000000006', '72.862074', 2),
(495, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:29:19', 0, '19.2315697', 'Order Taken from abc shop', '72.862074', 1),
(496, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:29:54', 0, '19.2315697', 'Order took for Delivery - abc shop', '72.862074', 2),
(497, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:29:58', 0, '19.2315697', 'Order Delivered for ORD1000000007', '72.862074', 2),
(498, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:39:32', 1, '19.2315092', NULL, '72.862073', 1),
(499, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:40:47', 1, '19.2315766', NULL, '72.8620751', 1),
(500, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:44:37', 1, '19.2315861', NULL, '72.8620862', 1),
(501, '', '2018-03-13 17:46:08', 0, NULL, 'Payment made for ORD1000000006', NULL, 1),
(502, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:46:48', 1, '19.2315695', NULL, '72.8620748', 1),
(503, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:47:06', 0, '19.2315695', 'Payment made for ORD1000000006', '72.8620748', 1),
(504, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:55:37', 1, '19.2315694', NULL, '72.8620735', 1),
(505, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 17:58:30', 1, '19.2315698', NULL, '72.8620744', 1),
(506, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:00:42', 0, '19.231571', NULL, '72.8620737', 1),
(507, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:12:56', 0, '19.2315672', NULL, '72.8620725', 1),
(508, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:16:07', 0, '19.2315677', NULL, '72.8620725', 1),
(509, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:17:50', 1, '19.2315678', NULL, '72.8620726', 1),
(510, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:21:23', 1, '19.2315747', NULL, '72.862076', 1),
(511, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:24:55', 1, '19.2315783', NULL, '72.8620742', 3),
(512, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:33:32', 0, '19.2315689', NULL, '72.8620733', 1),
(513, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:35:14', 1, '19.2315647', NULL, '72.8620718', 1),
(514, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:39:04', 1, '19.2315697', NULL, '72.8620741', 3),
(515, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:41:24', 1, '19.2315691', NULL, '72.8620735', 3),
(516, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 18:57:03', 0, '19.231569', 'Order Taken from Natraj Electronics', '72.8620746', 1),
(517, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:13:01', 1, '19.2315844', NULL, '72.8620808', 6),
(518, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:13:35', 0, '19.2315844', 'Order Taken from Bhima Electronics', '72.8620808', 4),
(519, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:14:18', 0, '19.2315844', 'Order Taken from Bhima Electronics', '72.8620808', 4),
(520, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:14:25', 0, '19.2315779', NULL, '72.8620802', 2),
(521, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:16:48', 0, '19.2315779', 'Order took for Delivery - Bhima Electronics', '72.8620802', 6),
(522, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:16:52', 0, '19.2315779', 'Order Delivered for ORD1000000009', '72.8620802', 6),
(523, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:27:15', 0, '19.2315706', NULL, '72.8620746', 6),
(524, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:28:19', 1, '19.2315704', NULL, '72.8620714', 4),
(525, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:54:16', 1, '19.2315713', NULL, '72.8620737', 5),
(526, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 19:57:01', 1, '19.2315865', NULL, '72.8620861', 4),
(527, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-13 20:11:27', 1, '19.2315828', NULL, '72.8620797', 5),
(528, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-14 10:25:08', 0, '19.2315647', NULL, '72.8620718', 5),
(529, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-14 10:30:16', 1, '19.2315677', NULL, '72.8620723', 5),
(530, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-14 15:19:37', 1, '19.2315715', NULL, '72.8620736', 1),
(531, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-14 15:20:15', 0, '19.2315715', 'Payment made for ORD1000000006', '72.8620736', 1),
(532, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-14 15:26:57', 0, '19.2315647', NULL, '72.8620718', 1),
(533, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-14 15:36:25', 1, '19.2315714', NULL, '72.8620729', 1),
(534, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-14 15:40:31', 1, '19.2315695', NULL, '72.8620733', 1),
(535, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-14 18:39:59', 1, '19.2315674', NULL, '72.862073', 1),
(536, '', '2018-03-16 11:45:34', 1, '19.2315647', NULL, '72.8620718', 5),
(537, '', '2018-03-16 11:52:11', 0, '19.2315647', 'Order Taken from abc shop', '72.8620718', 1),
(538, '', '2018-03-16 13:07:16', 0, '19.2315647', 'Order Taken from abc shop', '72.8620718', 1),
(539, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 16:41:30', 1, '19.2315851', NULL, '72.8620815', 1),
(540, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 16:49:44', 1, '19.2315717', NULL, '72.8620724', 3),
(541, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 16:58:50', 0, '19.2315693', NULL, '72.8620734', 3),
(542, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 16:59:45', 1, '19.2315696', NULL, '72.8620736', 3),
(543, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 17:30:55', 1, '19.2315647', NULL, '72.8620718', 1),
(544, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 18:36:17', 0, '19.2315681', NULL, '72.8620718', 1),
(545, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 18:37:59', 1, '19.2315702', NULL, '72.8620734', 1),
(546, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:01:12', 1, '19.2315717', NULL, '72.862077', 1),
(547, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:03:37', 1, '19.2315759', NULL, '72.8620852', 1),
(548, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:06:54', 1, '19.2315673', NULL, '72.8620732', 1),
(549, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:08:20', 1, '19.2315707', NULL, '72.8620736', 1),
(550, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:13:20', 1, '19.2315699', NULL, '72.862074', 5),
(551, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:14:12', 1, '19.2315699', NULL, '72.862074', 1),
(552, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:21:37', 1, '19.2315699', NULL, '72.8620729', 3),
(553, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:24:46', 1, '19.2315691', NULL, '72.8620747', 1),
(554, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:28:32', 1, '19.2315761', NULL, '72.8620752', 1),
(555, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:31:28', 1, '19.2315753', NULL, '72.8620751', 1),
(556, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:31:53', 0, '19.2315753', 'Payment made for ORD1000000007', '72.8620751', 1),
(557, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:35:10', 1, '19.2315697', NULL, '72.8620728', 1),
(558, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:38:52', 1, '19.2315744', NULL, '72.8620747', 1),
(559, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-16 19:50:58', 0, '19.2315744', 'Payment made for ORD1000000007', '72.8620747', 1),
(560, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 10:29:23', 1, '19.231588', NULL, '72.8620867', 1),
(561, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 10:31:23', 1, '19.2315803', NULL, '72.8620763', 1),
(562, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 10:43:51', 0, '19.2315722', NULL, '72.862073', 1),
(563, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 10:46:17', 0, '19.2315722', 'Payment made for ORD1000000003', '72.862073', 4),
(564, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 10:46:53', 0, '19.2315722', 'Payment made for ORD1000000003', '72.862073', 4),
(565, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 10:47:28', 0, '19.2315722', 'Payment made for ORD1000000003', '72.862073', 4),
(566, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 10:51:11', 0, '19.2315722', 'Payment made for ORD1000000003', '72.862073', 4),
(567, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 11:20:51', 0, '19.2315722', 'Payment made for ORD1000000003', '72.862073', 4),
(568, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 11:21:05', 1, '19.2315772', NULL, '72.8620737', 1),
(569, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 11:31:18', 0, '19.2315792', NULL, '72.8620754', 4),
(570, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 11:36:09', 0, '19.2315784', NULL, '72.8620804', 4),
(571, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 11:49:40', 0, '19.2315865', NULL, '72.8620794', 1),
(572, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 11:53:33', 0, '19.2315725', NULL, '72.8620756', 2),
(573, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 13:13:14', 1, '19.2315698', NULL, '72.8620732', 4),
(574, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 13:31:05', 0, '19.2315698', 'Payment made for ORD1000000003', '72.8620732', 4),
(575, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 13:38:33', 0, '19.2315698', 'Payment made for ORD1000000003', '72.8620732', 4),
(576, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 13:40:10', 0, '19.2315698', 'Payment made for ORD1000000003', '72.8620732', 4),
(577, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 13:43:29', 0, '19.2315698', 'Payment made for ORD1000000003', '72.8620732', 4),
(578, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 13:43:39', 0, '19.2315698', 'Payment made for ORD1000000003', '72.8620732', 4),
(579, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 13:43:47', 0, '19.2315698', 'Payment made for ORD1000000003', '72.8620732', 4),
(580, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 16:41:40', 0, '19.2315647', 'Payment made for ORD1000000004', '72.8620718', 4),
(581, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 16:42:04', 0, '19.2315647', 'Payment made for ORD1000000004', '72.8620718', 4),
(582, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 16:42:23', 0, '19.2315647', 'Payment made for ORD1000000004', '72.8620718', 4),
(583, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 16:49:48', 1, '19.2316219', NULL, '72.8621118', 4),
(584, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 16:57:20', 0, '19.2316575', NULL, '72.8621359', 4),
(585, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 17:36:30', 1, '19.2316575', NULL, '72.8621359', 4),
(586, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:13:39', 1, '19.2315799', NULL, '72.8620765', 5),
(587, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:14:38', 1, '19.2315694', NULL, '72.8620746', 4),
(588, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:23:07', 1, '19.2315741', NULL, '72.8620735', 4),
(589, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:25:45', 0, '19.2315741', 'New Business Added', '72.8620735', 4),
(590, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:28:19', 1, '19.2315695', NULL, '72.8620749', 5),
(591, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:35:08', 0, '19.2316681', NULL, '72.8621214', 5),
(592, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:40:24', 1, '19.2315807', NULL, '72.8620754', 5),
(593, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:41:43', 1, '19.231577', NULL, '72.8620765', 5),
(594, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:42:56', 1, '19.2315827', NULL, '72.862086', 5),
(595, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:44:11', 1, '19.2315774', NULL, '72.8620768', 4),
(596, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:47:51', 1, '19.2315701', NULL, '72.8620738', 4),
(597, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:53:48', 1, '19.23158', NULL, '72.8620779', 5),
(598, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:55:54', 1, '19.2315647', NULL, '72.8620718', 5),
(599, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:56:44', 1, '19.2315759', NULL, '72.8620765', 5);
INSERT INTO `employee_location` (`employee_location_id`, `address`, `datetime`, `new_route_status`, `latitude`, `location_info`, `longitude`, `employee_details_id`) VALUES
(600, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-17 18:58:44', 1, '19.2315705', NULL, '72.862074', 5),
(601, 'Mahatma Gandhi Rd, Rajendra Nagar Society, Sukarwadi, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-19 10:06:35', 0, '19.2317195', 'Order Taken from Bhima Electronics', '72.8621339', 4),
(602, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-19 10:41:33', 1, '19.2316094', NULL, '72.8621142', 5),
(603, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-20 11:19:23', 1, '19.2316097', NULL, '72.8621164', 5),
(604, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-20 11:20:23', 0, '19.2316097', 'Order Taken from Bhima Electronics', '72.8621164', 4),
(605, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-20 11:22:25', 0, '19.2316097', 'Order took for Delivery - Bhima Electronics', '72.8621164', 6),
(606, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-20 11:22:44', 0, '19.2316097', 'Order Delivered for ORD1000000014', '72.8621164', 6),
(607, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-20 11:57:39', 0, '19.2316082', NULL, '72.862114', 6),
(608, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-20 12:33:28', 0, '19.2316084', NULL, '72.8621021', 6),
(609, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-20 13:55:13', 1, '19.2316057', NULL, '72.8621156', 5),
(610, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-21 18:57:34', 1, '19.2316056', NULL, '72.8621157', 1),
(611, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-22 19:42:29', 1, '19.2316053', NULL, '72.8621156', 1),
(612, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-23 10:31:08', 1, '19.2316065', NULL, '72.8621019', 6),
(613, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-23 10:33:11', 0, '19.2316137', NULL, '72.8621181', 3),
(614, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 10:43:34', 0, '19.2316002', 'Order Taken from abc shop', '72.8621122', 1),
(615, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 10:44:38', 0, '19.2316002', 'Order Taken from Natraj Electronics', '72.8621122', 1),
(616, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 11:52:57', 0, '19.2316002', 'Order took for Delivery - Natraj Electronics', '72.8621122', 2),
(617, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 12:03:45', 0, '19.2316002', 'Order Delivered for ORD1000000016', '72.8621122', 2),
(618, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 12:05:35', 0, '19.2316002', 'Order Delivered for ORD1000000016', '72.8621122', 2),
(619, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 12:07:17', 0, '19.2316002', 'Order Delivered for ORD1000000016', '72.8621122', 2),
(620, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 12:08:17', 0, '19.2316002', 'Order Delivered for ORD1000000016', '72.8621122', 2),
(621, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 12:09:10', 0, '19.2316002', 'Order Delivered for ORD1000000016', '72.8621122', 2),
(622, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 12:10:56', 0, '19.2316002', 'Order Delivered for ORD1000000016', '72.8621122', 2),
(623, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 12:44:23', 0, '19.2316002', 'Replacement Done for Natraj Electronics', '72.8621122', 2),
(624, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 12:44:36', 0, '19.2316002', 'Replacement Done for Natraj Electronics', '72.8621122', 2),
(625, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 13:57:53', 0, '19.2316036', NULL, '72.8621033', 2),
(626, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 14:27:43', 0, '19.2316036', 'Order Taken from Natraj Electronics', '72.8621033', 1),
(627, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 14:28:04', 0, '19.2316036', 'Order Taken from abc shop', '72.8621033', 1),
(628, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 14:28:22', 0, '19.2316036', 'Order Taken from Natraj Electronics', '72.8621033', 1),
(629, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 14:38:37', 1, '19.2316041', NULL, '72.862104', 3),
(630, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 14:39:08', 0, '19.2316041', 'Order took for Delivery - Natraj Electronics', '72.862104', 2),
(631, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 14:39:14', 0, '19.2316041', 'Order Delivered for ORD1000000019', '72.862104', 2),
(632, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 14:41:46', 0, '19.2316041', 'Order Delivered for ORD1000000019', '72.862104', 2),
(633, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:02:05', 0, '19.2316041', 'Order took for Delivery - abc shop', '72.862104', 2),
(634, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:02:11', 0, '19.2316041', 'Order Delivered for ORD1000000018', '72.862104', 2),
(635, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:19:00', 0, '19.2316041', 'Order took for Delivery - Natraj Electronics', '72.862104', 2),
(636, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:19:08', 0, '19.2316041', 'Order Delivered for ORD1000000018', '72.862104', 2),
(637, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:19:16', 0, '19.2316041', 'Order Delivered for ORD1000000018', '72.862104', 2),
(638, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:19:18', 0, '19.2316041', 'Order Delivered for ORD1000000017', '72.862104', 2),
(639, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:19:20', 0, '19.2316041', 'Order Delivered for ORD1000000018', '72.862104', 2),
(640, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:19:23', 0, '19.2316041', 'Order Delivered for ORD1000000017', '72.862104', 2),
(641, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:21:06', 0, '19.2316041', 'Order Delivered for ORD1000000018', '72.862104', 2),
(642, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:46:47', 1, '19.2316039', NULL, '72.8621036', 1),
(643, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 15:48:27', 0, '19.2316039', 'Payment made for ORD1000000016', '72.8621036', 1),
(644, 'India(IN)_Maharashtra_Mumbai', '2018-03-23 16:19:43', 0, '19.2316039', 'Return Taken for ORD1000000017', '72.8621036', 1),
(645, 'India(IN)_Maharashtra_Mumbai', '2018-03-24 18:51:55', 0, '19.2316002', 'Order Delivered for ORD1000000018', '72.8621122', 2),
(646, 'India(IN)_Maharashtra_Mumbai', '2018-03-24 18:53:35', 0, '19.2316002', 'Replacement Done for abc shop', '72.8621122', 1),
(647, 'India(IN)_Maharashtra_Mumbai', '2018-03-24 18:53:47', 0, '19.2316002', 'Replacement Done for abc shop', '72.8621122', 1),
(648, 'India(IN)_Maharashtra_Mumbai', '2018-03-24 18:54:04', 0, '19.2316002', 'Replacement Done for abc shop', '72.8621122', 1),
(649, 'India(IN)_Maharashtra_Mumbai', '2018-03-26 12:10:21', 1, '19.231607', NULL, '72.8621287', 1),
(650, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-26 12:31:12', 1, '19.2316054', NULL, '72.8621284', 1),
(651, 'India(IN)_Maharashtra_Mumbai', '2018-03-26 17:49:26', 0, '19.2316098', 'Order Taken from abc shop', '72.862128', 1),
(652, 'India(IN)_Maharashtra_Mumbai', '2018-03-26 17:49:44', 0, '19.2316098', 'Order Taken from Natraj Electronics', '72.862128', 1),
(653, 'India(IN)_Maharashtra_Mumbai', '2018-03-27 12:43:07', 1, '19.2316098', NULL, '72.862128', 1),
(654, '', '2018-03-27 13:11:53', 1, '19.2316084', NULL, '72.8621295', 2),
(655, '', '2018-03-27 13:20:29', 0, '19.2316084', 'Order Taken from Bhima Electronics', '72.8621295', 4),
(656, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 13:29:22', 0, '19.2316083', NULL, '72.8621258', 4),
(657, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 13:40:31', 0, '19.2316094', NULL, '72.8621288', 4),
(658, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 13:48:00', 1, '19.2316075', NULL, '72.862128', 4),
(659, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 13:51:28', 1, '19.2316091', NULL, '72.8621289', 4),
(660, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 13:53:31', 1, '19.2316074', NULL, '72.8621285', 4),
(661, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 13:58:30', 1, '19.231611', NULL, '72.8621289', 4),
(662, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 15:14:42', 1, '19.2316098', NULL, '72.862128', 4),
(663, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 15:16:42', 1, '19.2316098', NULL, '72.862128', 4),
(664, 'India(IN)_Maharashtra_Mumbai', '2018-03-27 15:23:57', 1, '19.2316092', NULL, '72.8621291', 3),
(665, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 15:32:26', 0, '19.2316165', NULL, '72.8621282', 4),
(666, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 15:39:16', 1, '19.2316098', NULL, '72.862128', 4),
(667, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 15:41:38', 1, '19.2316158', NULL, '72.8621273', 4),
(668, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 15:49:15', 1, '19.2316098', NULL, '72.862128', 4),
(669, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 16:00:42', 1, '19.231613', NULL, '72.862129', 4),
(670, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 16:19:41', 1, '19.2316186', NULL, '72.8621303', 4),
(671, 'India(IN)_Maharashtra_Mumbai', '2018-03-27 16:25:32', 0, '19.2316092', 'Order took for Delivery - Natraj Electronics', '72.8621291', 2),
(672, 'India(IN)_Maharashtra_Mumbai', '2018-03-27 16:25:35', 0, '19.2316092', 'Order took for Delivery - abc shop', '72.8621291', 2),
(673, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 16:39:59', 0, '19.2316112', NULL, '72.8621292', 4),
(674, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 16:46:36', 0, '19.231611', NULL, '72.8621273', 4),
(675, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 17:24:50', 0, '19.2316145', NULL, '72.8621272', 4),
(676, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 17:35:18', 0, '19.231617', NULL, '72.8621306', 4),
(677, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 17:53:02', 1, '19.2316244', NULL, '72.8621287', 4),
(678, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 17:54:11', 1, '19.2316135', NULL, '72.8621269', 4),
(679, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 17:59:51', 1, '19.2316228', NULL, '72.8621325', 4),
(680, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 18:05:39', 1, '19.2316114', NULL, '72.8621289', 4),
(681, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 18:09:01', 1, '19.2316134', NULL, '72.8621294', 4),
(682, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 18:15:01', 1, '19.2316149', NULL, '72.8621262', 4),
(683, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 18:22:59', 1, '19.2316122', NULL, '72.8621278', 4),
(684, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 18:52:31', 1, '19.2316146', NULL, '72.8621289', 4),
(685, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 19:06:13', 1, '19.2316203', NULL, '72.862136', 4),
(686, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 19:18:39', 1, '19.2316087', NULL, '72.8621277', 4),
(687, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 19:25:51', 1, '19.2316071', NULL, '72.8621284', 4),
(688, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 19:31:26', 1, '19.2316135', NULL, '72.8621274', 4),
(689, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-27 19:35:11', 1, '19.2316109', NULL, '72.8621275', 4),
(690, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:01:18', 1, '19.2316146', NULL, '72.8621281', 1),
(691, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:08:29', 1, '19.2316069', NULL, '72.8621265', 1),
(692, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:09:23', 1, '19.231607', NULL, '72.8621262', 1),
(693, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:14:25', 1, '19.2316104', NULL, '72.8621273', 1),
(694, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:19:51', 1, '19.231612', NULL, '72.8621276', 1),
(695, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:20:13', 1, '19.2316087', NULL, '72.8621291', 3),
(696, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:26:00', 0, '19.2316098', NULL, '72.862128', 1),
(697, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:27:01', 1, '19.2316145', NULL, '72.8621254', 1),
(698, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:27:17', 1, '19.2316119', NULL, '72.8621299', 3),
(699, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 11:39:25', 0, '19.2316127', NULL, '72.8621261', 1),
(700, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 12:21:12', 1, '19.231611', NULL, '72.8621273', 3),
(701, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 12:44:44', 1, '19.2316143', NULL, '72.8621283', 3),
(702, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 12:53:38', 0, '19.2316117', NULL, '72.8621273', 1),
(703, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 13:04:59', 1, '19.2316058', NULL, '72.8621281', 3),
(704, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 13:07:01', 1, '19.231614', NULL, '72.8621269', 1),
(705, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 13:11:31', 0, '19.2316129', NULL, '72.8621294', 1),
(706, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 13:44:24', 1, '19.2316105', NULL, '72.8621274', 1),
(707, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 13:48:30', 1, '19.2316186', NULL, '72.8621316', 1),
(708, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:00:21', 0, '19.2316146', NULL, '72.8621287', 1),
(709, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:02:21', 1, '19.2316105', NULL, '72.8621292', 1),
(710, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:03:43', 1, '19.2316068', NULL, '72.862129', 1),
(711, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:06:40', 1, '19.2316106', NULL, '72.8621282', 1),
(712, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:09:29', 1, '19.231611', NULL, '72.8621261', 1),
(713, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:12:57', 1, '19.2316162', NULL, '72.8621149', 1),
(714, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:31:32', 1, '19.2316111', NULL, '72.8621297', 1),
(715, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:33:36', 1, '19.2316319', NULL, '72.8621285', 1),
(716, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:35:14', 1, '19.2316126', NULL, '72.862128', 1),
(717, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:47:21', 0, '19.2316098', NULL, '72.862128', 1),
(718, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:54:02', 1, '19.2316148', NULL, '72.8621267', 1),
(719, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 14:56:04', 1, '19.2316125', NULL, '72.8621295', 1),
(720, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 15:02:14', 0, '19.2316132', NULL, '72.8621297', 1),
(721, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 15:16:36', 0, '19.2316108', NULL, '72.8621277', 1),
(722, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 15:32:21', 0, '19.2316098', NULL, '72.862128', 1),
(723, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 15:33:15', 1, '19.2316145', NULL, '72.8621293', 1),
(724, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 15:38:21', 1, '19.2316195', NULL, '72.8621306', 1),
(725, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 15:46:47', 1, '19.2316207', NULL, '72.8621361', 1),
(726, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 15:53:55', 1, '19.2316095', NULL, '72.8621291', 1),
(727, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 15:59:55', 1, '19.2316108', NULL, '72.8621255', 1),
(728, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 16:07:48', 1, '19.2316112', NULL, '72.8621298', 1),
(729, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 16:27:19', 0, '19.2316095', NULL, '72.8621267', 1),
(730, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:16:10', 1, '19.2316163', NULL, '72.8621277', 1),
(731, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:17:09', 1, '19.2316211', NULL, '72.862134', 1),
(732, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:18:53', 1, '19.2316147', NULL, '72.8621269', 1),
(733, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:21:22', 1, '19.2316162', NULL, '72.8621271', 1),
(734, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:28:32', 1, '19.2316118', NULL, '72.8621242', 1),
(735, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:32:14', 1, '19.2316364', NULL, '72.8621276', 1),
(736, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:38:53', 1, '19.2316113', NULL, '72.8621283', 3),
(737, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:45:57', 1, '19.2316136', NULL, '72.8621263', 3),
(738, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:46:53', 1, '19.231611', NULL, '72.8621272', 3),
(739, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:52:43', 1, '19.2316084', NULL, '72.8621291', 3),
(740, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 17:57:46', 1, '19.231615', NULL, '72.8621293', 3),
(741, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:01:30', 1, '19.231612', NULL, '72.8621258', 3),
(742, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:12:47', 1, '19.2316165', NULL, '72.8621226', 3),
(743, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:22:38', 1, '19.2316151', NULL, '72.8621277', 3),
(744, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:25:07', 1, '19.2316194', NULL, '72.8621274', 3),
(745, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:30:21', 1, '19.2316087', NULL, '72.8621299', 4),
(746, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:31:19', 1, '19.2316142', NULL, '72.8621274', 3),
(747, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:32:28', 1, '19.2316155', NULL, '72.86213', 1),
(748, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:35:20', 1, '19.231612', NULL, '72.8621299', 1),
(749, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:36:42', 0, '19.2316103', NULL, '72.8621305', 3),
(750, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:44:13', 1, '19.2316106', NULL, '72.8621299', 1),
(751, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:45:46', 1, '19.2316098', NULL, '72.862128', 1),
(752, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:47:00', 1, '19.2316151', NULL, '72.8621303', 1),
(753, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 18:52:43', 0, '19.2316133', NULL, '72.8621295', 3),
(754, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 19:05:45', 1, '19.2316137', NULL, '72.8621302', 3),
(755, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 19:08:25', 1, '19.2316126', NULL, '72.86213', 3),
(756, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-28 19:19:04', 1, '19.2316139', NULL, '72.8621298', 3),
(757, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:08:39', 1, '19.2316121', NULL, '72.8621279', 1),
(758, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:12:23', 0, '19.2316138', NULL, '72.8621293', 1),
(759, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:18:52', 0, '19.2316156', NULL, '72.8621301', 1),
(760, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:19:15', 1, '19.2316114', NULL, '72.8621302', 2),
(761, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:21:15', 1, '19.2316098', NULL, '72.862128', 2),
(762, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:26:02', 1, '19.2316098', NULL, '72.862128', 2),
(763, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:31:55', 1, '19.2316076', NULL, '72.8621297', 2),
(764, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:34:50', 1, '19.2316098', NULL, '72.862128', 2),
(765, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-29 12:37:44', 1, '19.2316098', NULL, '72.862128', 2),
(766, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 15:10:15', 1, '19.2316098', NULL, '72.862128', 1),
(767, 'Mahatma Gandhi Rd, Rajendra Nagar Society, Sukarwadi, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-31 15:20:44', 1, '19.2312105', NULL, '72.8626378', 1),
(768, 'Mahatma Gandhi Rd, Rajendra Nagar Society, Sukarwadi, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-31 15:49:24', 1, '19.2316881', NULL, '72.8621269', 1),
(769, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 17:00:33', 0, '19.2316098', 'Order Taken from Bhima Electronics', '72.862128', 4),
(770, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 17:08:01', 1, '19.231603', NULL, '72.8621293', 4),
(771, 'Mahatma Gandhi Rd, Rajendra Nagar Society, Sukarwadi, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-31 18:26:08', 1, '19.2316944', NULL, '72.8621292', 1),
(772, 'Mahatma Gandhi Rd, Rajendra Nagar Society, Sukarwadi, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-31 18:29:17', 1, '19.2316881', NULL, '72.8621267', 1),
(773, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 18:34:20', 1, '19.2316303', NULL, '72.8620817', 1),
(774, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 18:39:49', 1, '19.2316144', NULL, '72.8621281', 1),
(775, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 18:45:36', 0, '19.231618', NULL, '72.8621331', 1),
(776, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 18:53:08', 1, '19.2316091', NULL, '72.8621282', 1),
(777, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 19:12:38', 1, '19.2316074', NULL, '72.8621284', 1),
(778, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 19:15:28', 1, '19.2316112', NULL, '72.8621284', 1),
(779, 'Mahatma Gandhi Rd, Rajendra Nagar Society, Sukarwadi, Borivali East, Mumbai, Maharashtra 400066, India', '2018-03-31 19:25:00', 1, '19.2316935', NULL, '72.8621288', 1),
(780, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-03-31 19:33:50', 1, '19.2316099', NULL, '72.8621279', 1),
(781, '', '2018-04-02 10:25:42', 0, NULL, 'Order Taken from abc shop', NULL, 1),
(782, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-04 17:04:16', 0, '19.2316117', 'Payment made for ORD1000000006', '72.8621288', 1),
(783, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-04 17:57:21', 1, '19.2316135', NULL, '72.8621161', 1),
(784, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-04 18:52:18', 1, '19.2316086', NULL, '72.8621295', 1),
(785, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-06 12:19:12', 1, '19.231607', NULL, '72.8621294', 1),
(786, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-06 12:20:30', 1, '19.2316075', NULL, '72.8621307', 1),
(787, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-06 12:31:58', 0, '19.2316075', 'Payment made for ORD1000000007', '72.8621307', 1),
(788, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-06 12:34:28', 0, '19.2316075', 'Payment made for ORD1000000004', '72.8621307', 4),
(789, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-06 12:47:25', 0, '19.2316096', NULL, '72.8621298', 4),
(790, 'Shreeji Arcade, Mahatma Gandhi Rd, Sukarwadi, Borivali, Mumbai, Maharashtra 400066, India', '2018-04-06 13:16:22', 0, '19.2316066', NULL, '72.8621299', 4);

-- --------------------------------------------------------

--
-- Table structure for table `employee_salary`
--

CREATE TABLE `employee_salary` (
  `employee_salary_id` bigint(20) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `cheque_date` datetime DEFAULT NULL,
  `cheque_number` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `paying_amount` decimal(19,2) DEFAULT NULL,
  `paying_date` datetime DEFAULT NULL,
  `employeeDetails_id` bigint(20) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_salary`
--

INSERT INTO `employee_salary` (`employee_salary_id`, `bank_name`, `cheque_date`, `cheque_number`, `comment`, `paying_amount`, `paying_date`, `employeeDetails_id`, `status`) VALUES
(1, 'SBI', '2018-04-03 18:04:39', '123456', 'ghgh', '1000.00', '2018-03-24 18:44:16', 4, 1),
(2, 'sbi', '2018-04-02 11:28:25', '123456', 'salary for february', '5000.00', '2018-04-02 11:28:25', 5, 0),
(3, NULL, NULL, NULL, '', '300.00', '2018-04-03 17:06:08', 4, 1),
(4, NULL, NULL, NULL, 'ggg', '1000.00', '2018-04-03 18:07:34', 4, 1),
(5, NULL, NULL, NULL, 'gggrr', '500.00', '2018-04-03 18:07:46', 4, 0),
(6, NULL, NULL, NULL, 'Incentive', '550.00', '2018-04-06 12:11:28', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `expense_id` varchar(255) NOT NULL,
  `add_datetime` datetime DEFAULT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `cheque_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `update_datetime` datetime DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `expense_type_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense`
--

INSERT INTO `expense` (`expense_id`, `add_datetime`, `amount`, `bank_name`, `cheque_number`, `description`, `reference`, `type`, `update_datetime`, `company_id`, `employee_id`, `expense_type_id`) VALUES
('EXP1001', '2018-04-06 11:08:39', '1000.00', NULL, NULL, 'LIght Bill', 'Reliance', 'Cash', NULL, 4, NULL, NULL),
('EXP1002', '2018-04-06 11:09:20', '700.00', NULL, NULL, 'Water Bill', 'BMC', 'Cash', '2018-04-06 11:14:25', 4, NULL, NULL),
('EXP1003', '2018-04-06 11:12:29', '1489.00', 'SBI', '113456', 'Employee Lunch', 'Office', 'Cheque', NULL, 4, NULL, NULL),
('EXP1004', '2018-04-09 13:28:52', '700.00', NULL, NULL, 'LIght Bill', 'TATA', 'Cash', NULL, NULL, 13, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `expense_type`
--

CREATE TABLE `expense_type` (
  `expense_type_id` bigint(20) NOT NULL,
  `details` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `expense_type_added_date` datetime DEFAULT NULL,
  `expense_type_updated_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `inventory_transaction_id` varchar(255) NOT NULL,
  `inventory_added_datetime` datetime DEFAULT NULL,
  `inventory_payment_datetime` datetime DEFAULT NULL,
  `paid_status` tinyint(1) DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `total_amount_tax` decimal(19,2) DEFAULT NULL,
  `total_quantity` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`inventory_transaction_id`, `inventory_added_datetime`, `inventory_payment_datetime`, `paid_status`, `total_amount`, `total_amount_tax`, `total_quantity`, `company_id`, `employee_id`, `supplier_id`) VALUES
('TSN1001', '2018-03-19 10:46:54', '2018-03-19 00:00:00', 0, '3606.41', '4100.00', 44, NULL, 13, 'SUP1000000002'),
('TSN1002', '2018-03-23 14:33:21', '2018-03-23 00:00:00', 0, '18659.00', '20900.00', 900, NULL, 11, 'SUP1000000001'),
('TSN1003', '2018-03-23 14:33:58', '2018-03-23 00:00:00', 0, '0.00', '1140.00', 60, NULL, 11, 'SUP1000000001'),
('TSN1004', '2018-03-31 17:25:47', '2018-04-30 00:00:00', 0, '529.65', '625.00', 5, NULL, 13, 'SUP1000000002'),
('TSN1005', '2018-03-31 17:26:21', '2018-04-12 00:00:00', 1, '1059.30', '1250.00', 10, NULL, 13, 'SUP1000000002');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_details`
--

CREATE TABLE `inventory_details` (
  `inventory_details_id` bigint(20) NOT NULL,
  `amount` decimal(19,2) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `rate` decimal(19,2) DEFAULT NULL,
  `inventory_id` varchar(255) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory_details`
--

INSERT INTO `inventory_details` (`inventory_details_id`, `amount`, `quantity`, `rate`, `inventory_id`, `product_id`) VALUES
(12, '1700.00', 17, '100.00', 'TSN1001', 174),
(13, '1200.00', 12, '100.00', 'TSN1001', 175),
(14, '1200.00', 15, '80.00', 'TSN1001', 176),
(15, '10500.00', 500, '21.00', 'TSN1002', 214),
(16, '10400.00', 400, '26.00', 'TSN1002', 215),
(18, '1140.00', 60, '19.00', 'TSN1003', 244),
(19, '625.00', 5, '125.00', 'TSN1004', 359),
(20, '1250.00', 10, '125.00', 'TSN1005', 360);

-- --------------------------------------------------------

--
-- Table structure for table `ledger`
--

CREATE TABLE `ledger` (
  `ledger_id` bigint(20) NOT NULL,
  `balance` decimal(19,2) DEFAULT NULL,
  `cheque_date` datetime DEFAULT NULL,
  `credit` decimal(19,2) DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `debit` decimal(19,2) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `pay_mode` varchar(255) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `employee_salary_id` bigint(20) DEFAULT NULL,
  `expense_id` varchar(255) DEFAULT NULL,
  `payment_order_id` bigint(20) DEFAULT NULL,
  `payment_counter_id` bigint(20) DEFAULT NULL,
  `payment_supplier_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ledger`
--

INSERT INTO `ledger` (`ledger_id`, `balance`, `cheque_date`, `credit`, `date_time`, `debit`, `description`, `pay_mode`, `reference`, `company_id`, `employee_salary_id`, `expense_id`, `payment_order_id`, `payment_counter_id`, `payment_supplier_id`) VALUES
(7, '-1000.00', NULL, '0.00', '2018-04-06 11:08:39', '1000.00', 'LIght Bill', 'Cash', 'Reliance', 4, NULL, 'EXP1001', NULL, NULL, NULL),
(8, '-1700.00', NULL, '0.00', '2018-04-06 11:09:20', '700.00', 'Water Bill', 'Cash', 'BMC', 4, NULL, 'EXP1002', NULL, NULL, NULL),
(9, '-3189.00', NULL, '0.00', '2018-04-06 11:12:29', '1489.00', 'Employee Lunch', 'Cheque', 'Office', 4, NULL, 'EXP1003', NULL, NULL, NULL),
(14, '-3489.00', NULL, '0.00', '2018-04-06 11:52:35', '300.00', 'TSN1005 Payment', 'Cash', 'Supplier1', 4, NULL, NULL, NULL, NULL, 28),
(15, '-3989.00', NULL, '0.00', '2018-04-06 11:52:44', '500.00', 'TSN1005 Payment', 'Cash', 'Supplier1', 4, NULL, NULL, NULL, NULL, 29),
(16, '-4539.00', NULL, '0.00', '2018-04-06 12:11:28', '550.00', 'Incentive', 'Cash', NULL, 4, 6, NULL, NULL, NULL, NULL),
(18, '-4389.00', NULL, '150.00', '2018-04-06 12:33:59', '0.00', 'ORD1000000004 Payment', 'Cash', 'Bhima Electronics', 4, NULL, NULL, 25, NULL, NULL),
(19, '-485.00', NULL, '3904.00', '2018-04-06 13:04:39', '0.00', 'CORD1003 Payment', 'Cash', 'Sachin Pawar', 4, NULL, NULL, NULL, 4, NULL),
(20, '1515.00', NULL, '2000.00', '2018-04-06 13:06:00', '0.00', 'CORD1004 Payment', 'EEE-122222', NULL, 4, NULL, NULL, NULL, 5, NULL),
(21, '4515.00', NULL, '3000.00', '2018-04-06 13:08:08', '0.00', 'CORD1005 Payment', 'Cash', 'Tushar Goavnkar', 4, NULL, NULL, NULL, 6, NULL),
(22, '2515.00', NULL, '0.00', '2018-04-06 13:08:45', '0.00', 'CORD1006 Payment', 'Cash', NULL, 4, NULL, NULL, NULL, 7, NULL),
(23, '2065.00', NULL, '0.00', '2018-04-06 15:48:14', '450.00', 'TSN1005 Payment', 'Cash', 'Supplier1', 4, NULL, NULL, NULL, NULL, 30),
(24, '4465.00', NULL, '2400.00', '2018-04-07 11:41:56', '0.00', 'CORD1007 Payment', 'Cash', 'Shop Name', 4, NULL, NULL, NULL, 8, NULL),
(25, '8285.00', NULL, '3820.00', '2018-04-07 11:46:24', '0.00', 'CORD1008 Payment', 'Cash', 'sac pwr', 4, NULL, NULL, NULL, 9, NULL),
(26, '8427.00', NULL, '142.00', '2018-04-07 11:47:31', '0.00', 'CORD1009 Payment', 'Cash', 'f', 4, NULL, NULL, NULL, 10, NULL),
(27, '9251.00', NULL, '824.00', '2018-04-07 11:48:36', '0.00', 'CORD1010 Payment', 'Cash', 'sdds', 4, NULL, NULL, NULL, 11, NULL),
(28, '9663.00', NULL, '412.00', '2018-04-07 11:56:01', '0.00', 'CORD1010 Payment', 'Cash', 'Kunal', 4, NULL, NULL, NULL, 12, NULL),
(29, '-100.00', NULL, '0.00', '2018-04-07 18:36:46', '100.00', 'TSN1003 Payment', 'Cash', 'Supplier 1', 2, NULL, NULL, NULL, NULL, 31),
(30, '8963.00', NULL, '0.00', '2018-04-09 13:28:52', '700.00', 'LIght Bill', 'Cash', 'TATA', 4, NULL, 'EXP1004', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `order_id` varchar(255) NOT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `confirm_date` datetime DEFAULT NULL,
  `confirm_total_amount` decimal(19,2) DEFAULT NULL,
  `confirm_total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `confirm_total_quantity` bigint(20) DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `invoice_number` varchar(255) DEFAULT NULL,
  `issue_date` datetime DEFAULT NULL,
  `issued_total_amount` decimal(19,2) DEFAULT NULL,
  `issued_total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `issued_total_quantity` bigint(20) DEFAULT NULL,
  `order_details_added_datetime` datetime DEFAULT NULL,
  `order_details_payment_take_datetime` datetime DEFAULT NULL,
  `packed_date` datetime DEFAULT NULL,
  `paid_status` tinyint(1) DEFAULT NULL,
  `payment_period_days` bigint(20) DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `total_quantity` bigint(20) DEFAULT NULL,
  `business_name_id` varchar(255) DEFAULT NULL,
  `employee_id_cancel` bigint(20) DEFAULT NULL,
  `employee_id_sm` bigint(20) DEFAULT NULL,
  `order_status_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`order_id`, `cancel_date`, `confirm_date`, `confirm_total_amount`, `confirm_total_amount_with_tax`, `confirm_total_quantity`, `delivery_date`, `invoice_number`, `issue_date`, `issued_total_amount`, `issued_total_amount_with_tax`, `issued_total_quantity`, `order_details_added_datetime`, `order_details_payment_take_datetime`, `packed_date`, `paid_status`, `payment_period_days`, `total_amount`, `total_amount_with_tax`, `total_quantity`, `business_name_id`, `employee_id_cancel`, `employee_id_sm`, `order_status_id`) VALUES
('ORD1000000001', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-14 00:00:00', 'INV1000000009', '1970-01-01 05:30:00', '17710.86', '20617.00', 92, '2018-03-13 11:00:41', '1970-01-01 05:30:00', '2018-03-14 10:16:38', 0, 1, '17710.86', '20617.00', 96, '1004', NULL, 12, 4),
('ORD1000000002', '2018-03-13 12:44:52', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-13 00:00:00', 'INV1000000002', '2018-03-13 11:54:55', '10521.97', '11992.00', 74, '2018-03-13 11:02:44', '1970-01-01 05:30:00', '2018-03-13 11:54:45', 0, 1, '10521.97', '11992.00', 74, '1003', 14, 12, 1),
('ORD1000000003', '1970-01-01 05:30:00', '2018-03-13 12:33:53', '761.36', '870.00', 13, '2018-03-13 00:00:00', 'INV1000000001', '2018-03-13 11:48:47', '761.36', '870.00', 13, '2018-03-13 11:03:36', '2018-03-17 00:00:00', '2018-03-13 11:35:35', 1, 1, '3301.04', '3739.00', 42, '1004', NULL, 12, 3),
('ORD1000000004', '1970-01-01 05:30:00', '2018-03-13 12:33:02', '501.90', '589.00', 14, '2018-03-13 00:00:00', 'INV1000000003', '2018-03-13 12:32:56', '807.14', '589.00', 14, '2018-03-13 12:32:09', '2018-04-06 00:00:00', '2018-03-13 12:32:35', 0, 1, '70.00', '589.00', 14, '1004', NULL, 12, 3),
('ORD1000000005', '2018-03-13 17:24:30', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-13 00:00:00', 'INV1000000004', '2018-03-13 17:18:53', '2166.48', '2430.00', 17, '2018-03-13 17:00:00', '1970-01-01 05:30:00', '2018-03-13 17:17:11', 0, 1, '2141.07', '2430.00', 17, '1002', 10, 9, 1),
('ORD1000000006', '1970-01-01 05:30:00', '2018-03-13 17:27:57', '2408.93', '2730.00', 19, '2018-03-13 00:00:00', 'INV1000000005', '2018-03-13 17:27:50', '267.86', '300.00', 2, '2018-03-13 17:25:09', '2018-04-04 00:00:00', '2018-03-13 17:25:33', 0, 5, '250.00', '300.00', 2, '1002', NULL, 9, 3),
('ORD1000000007', '1970-01-01 05:30:00', '2018-03-13 17:30:00', '2678.60', '3000.00', 17, '2018-03-13 00:00:00', 'INV1000000006', '2018-03-13 17:29:55', '2678.60', '3000.00', 17, '2018-03-13 17:29:18', '2018-04-06 00:00:00', '2018-03-13 17:29:37', 0, 1, '2678.60', '3000.00', 17, '1001', NULL, 9, 3),
('ORD1000000008', '2018-03-13 19:12:14', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-13 00:00:00', 'INV1000000007', '1970-01-01 05:30:00', '357.14', '400.00', 2, '2018-03-13 18:57:03', '1970-01-01 05:30:00', '2018-03-13 18:57:29', 0, 7, '357.14', '400.00', 2, '1002', 11, 9, 1),
('ORD1000000009', '2018-03-13 19:17:14', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-13 00:00:00', 'INV1000000008', '2018-03-13 19:16:50', '3960.59', '4545.00', 23, '2018-03-13 19:13:36', '1970-01-01 05:30:00', '2018-03-13 19:16:22', 0, 1, '3960.59', '4545.00', 23, '1004', 14, 12, 1),
('ORD1000000010', '2018-03-13 19:16:10', '1970-01-01 05:30:00', '0.00', '0.00', 0, '1970-01-01 05:30:00', NULL, '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-13 19:14:19', '1970-01-01 05:30:00', '1970-01-01 05:30:00', 0, 2, '4724.55', '5575.00', 14, '1004', 12, 12, 1),
('ORD1000000011', '2018-03-16 13:04:46', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-16 00:00:00', 'INV1000000010', '1970-01-01 05:30:00', '4017.91', '4500.00', 29, '2018-03-16 11:52:12', '1970-01-01 05:30:00', '2018-03-16 11:52:41', 0, 1, '10183.73', '11540.00', 93, '1001', 11, 9, 1),
('ORD1000000012', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-16 00:00:00', 'INV1000000011', '1970-01-01 05:30:00', '8794.67', '9850.00', 47, '2018-03-16 13:07:17', '1970-01-01 05:30:00', '2018-03-16 13:07:42', 0, 1, '9913.31', '11170.00', 55, '1001', NULL, 9, 4),
('ORD1000000013', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-19 00:00:00', 'INV1000000012', '1970-01-01 05:30:00', '9382.65', '10825.00', 73, '2018-03-19 10:06:33', '1970-01-01 05:30:00', '2018-03-19 10:54:35', 0, 1, '9382.65', '10825.00', 75, '1004', NULL, 12, 4),
('ORD1000000014', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-20 00:00:00', 'INV1000000013', '2018-03-20 11:22:39', '2577.47', '3020.00', 17, '2018-03-20 11:20:22', '1970-01-01 05:30:00', '2018-03-20 11:21:25', 0, 2, '3848.67', '4520.00', 22, '1004', NULL, 12, 2),
('ORD1000000015', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '1970-01-01 05:30:00', NULL, '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-23 10:43:32', '1970-01-01 05:30:00', '1970-01-01 05:30:00', 0, 1, '8928.60', '10000.00', 40, '1001', NULL, 9, 6),
('ORD1000000016', '1970-01-01 05:30:00', '2018-03-23 12:11:13', '372.88', '440.00', 2, '2018-03-23 00:00:00', 'INV1000000025', '2018-03-23 11:53:15', '932.20', '1100.00', 5, '2018-03-23 10:44:36', '2018-03-24 00:00:00', '2018-03-23 11:05:40', 1, 1, '5396.45', '6100.00', 30, '1002', NULL, 9, 5),
('ORD1000000017', '1970-01-01 05:30:00', '2018-03-23 15:20:38', '520.00', '4120.00', 15, '2018-03-23 00:00:00', 'INV1000000028', '2018-03-23 15:19:01', '4425.89', '4960.00', 20, '2018-03-23 14:27:41', '2018-03-23 15:20:38', '2018-03-23 15:18:40', 0, 1, '4425.89', '4960.00', 20, '1002', NULL, 9, 5),
('ORD1000000018', '1970-01-01 05:30:00', '2018-03-24 18:52:18', '803.57', '900.00', 4, '2018-03-23 00:00:00', 'INV1000000027', '2018-03-23 15:02:04', '3571.45', '4000.00', 15, '2018-03-23 14:28:02', '2018-03-25 18:52:18', '2018-03-23 15:01:40', 0, 1, '3571.45', '4000.00', 15, '1001', NULL, 9, 5),
('ORD1000000019', '2018-03-23 14:48:16', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-23 00:00:00', 'INV1000000026', '2018-03-23 14:39:09', '246.37', '280.00', 5, '2018-03-23 14:28:21', '1970-01-01 05:30:00', '2018-03-23 14:38:47', 0, 1, '246.37', '280.00', 5, '1002', 10, 9, 1),
('ORD1000000020', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-27 00:00:00', 'INV1000000066', '2018-03-27 16:25:36', '4285.68', '4800.00', 24, '2018-03-26 17:49:24', '1970-01-01 05:30:00', '2018-03-27 16:15:43', 0, 1, '9642.88', '10800.00', 44, '1001', NULL, 9, 2),
('ORD1000000021', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-03-27 00:00:00', 'INV1000000065', '2018-03-27 16:25:33', '5553.85', '6500.00', 30, '2018-03-26 17:49:43', '1970-01-01 05:30:00', '2018-03-27 16:13:49', 0, 1, '5553.85', '6500.00', 30, '1002', NULL, 9, 2),
('ORD1000000022', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '1970-01-01 05:30:00', NULL, '1970-01-01 05:30:00', '2551.84', '2964.00', 14, '2018-03-27 13:20:24', '1970-01-01 05:30:00', '1970-01-01 05:30:00', 0, 1, '2551.84', '2964.00', 14, '1004', NULL, 12, 4),
('ORD1000000023', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '1970-01-01 05:30:00', NULL, '1970-01-01 05:30:00', '1528.44', '1750.00', 12, '2018-03-31 17:00:31', '1970-01-01 05:30:00', '1970-01-01 05:30:00', 0, 2, '1799.62', '2070.00', 14, '1004', NULL, 12, 4),
('ORD1000000024', '1970-01-01 05:30:00', '1970-01-01 05:30:00', '0.00', '0.00', 0, '1970-01-01 05:30:00', NULL, '1970-01-01 05:30:00', '0.00', '0.00', 0, '2018-04-02 10:25:40', '1970-01-01 05:30:00', '1970-01-01 05:30:00', 0, 2, '3928.54', '4400.00', 22, '1001', NULL, 9, 6);

-- --------------------------------------------------------

--
-- Table structure for table `order_product_details`
--

CREATE TABLE `order_product_details` (
  `id` bigint(20) NOT NULL,
  `confirm_amount` decimal(19,2) DEFAULT NULL,
  `confirm_quantity` bigint(20) DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `issue_amount` decimal(19,2) DEFAULT NULL,
  `issued_quantity` bigint(20) DEFAULT NULL,
  `purchase_amount` decimal(19,2) DEFAULT NULL,
  `purchase_quantity` bigint(20) DEFAULT NULL,
  `selling_rate` decimal(19,2) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `order_details_id` varchar(255) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product_details`
--

INSERT INTO `order_product_details` (`id`, `confirm_amount`, `confirm_quantity`, `details`, `issue_amount`, `issued_quantity`, `purchase_amount`, `purchase_quantity`, `selling_rate`, `type`, `order_details_id`, `product_id`) VALUES
(19, '0.00', 0, NULL, '2130.00', 15, '2130.00', 15, '142.00', 'NonFree', 'ORD1000000002', 20),
(20, '0.00', 0, NULL, '1540.00', 7, '1540.00', 7, '220.00', 'NonFree', 'ORD1000000002', 21),
(21, '0.00', 0, NULL, '1600.00', 8, '1600.00', 8, '200.00', 'NonFree', 'ORD1000000002', 22),
(22, '0.00', 0, NULL, '1232.00', 14, '1232.00', 14, '88.00', 'NonFree', 'ORD1000000002', 23),
(23, '0.00', 0, NULL, '720.00', 12, '720.00', 12, '60.00', 'NonFree', 'ORD1000000002', 24),
(24, '0.00', 0, NULL, '4770.00', 18, '4770.00', 18, '265.00', 'NonFree', 'ORD1000000002', 25),
(35, '236.00', 4, NULL, '236.00', 4, '236.00', 4, '59.00', 'NonFree', 'ORD1000000003', 39),
(36, '0.00', 3, NULL, '0.00', 3, '0.00', 6, '0.00', 'Free', 'ORD1000000003', 40),
(37, '530.00', 2, NULL, '530.00', 2, '2915.00', 11, '265.00', 'NonFree', 'ORD1000000003', 41),
(38, '80.00', 2, NULL, '80.00', 2, '480.00', 12, '40.00', 'NonFree', 'ORD1000000003', 42),
(39, '24.00', 2, NULL, '24.00', 2, '108.00', 9, '12.00', 'NonFree', 'ORD1000000003', 43),
(40, '60.00', 2, NULL, '60.00', 2, '60.00', 2, '30.00', 'NonFree', 'ORD1000000004', 46),
(41, '352.00', 4, NULL, '352.00', 4, '352.00', 4, '88.00', 'NonFree', 'ORD1000000004', 47),
(42, '177.00', 3, NULL, '177.00', 3, '177.00', 3, '59.00', 'NonFree', 'ORD1000000004', 48),
(43, '0.00', 2, NULL, '0.00', 2, '0.00', 2, '0.00', 'Free', 'ORD1000000004', 49),
(44, '0.00', 3, NULL, '0.00', 3, '0.00', 3, '0.00', 'Free', 'ORD1000000004', 50),
(51, '0.00', 0, NULL, '630.00', 3, '630.00', 3, '210.00', 'NonFree', 'ORD1000000005', 66),
(52, '0.00', 0, NULL, '1800.00', 12, '1800.00', 12, '150.00', 'NonFree', 'ORD1000000005', 67),
(53, '0.00', 0, NULL, '0.00', 2, '0.00', 2, '0.00', 'Free', 'ORD1000000005', 68),
(54, '630.00', 3, NULL, '300.00', 2, '300.00', 2, '150.00', 'NonFree', 'ORD1000000006', 69),
(55, '2100.00', 14, NULL, '2100.00', 14, '2100.00', 14, '150.00', 'NonFree', 'ORD1000000007', 70),
(56, '900.00', 3, NULL, '900.00', 3, '900.00', 3, '300.00', 'NonFree', 'ORD1000000007', 71),
(57, '0.00', 0, NULL, '400.00', 2, '400.00', 2, '200.00', 'NonFree', 'ORD1000000008', 73),
(58, '0.00', 0, NULL, '2145.00', 11, '2145.00', 11, '195.00', 'NonFree', 'ORD1000000009', 74),
(59, '0.00', 0, NULL, '2400.00', 12, '2400.00', 12, '200.00', 'NonFree', 'ORD1000000009', 75),
(60, '0.00', 0, NULL, '0.00', 0, '1755.00', 9, '195.00', 'NonFree', 'ORD1000000010', 76),
(61, '0.00', 0, NULL, '0.00', 0, '3820.00', 5, '764.00', 'NonFree', 'ORD1000000010', 77),
(91, '0.00', 0, NULL, '1755.00', 13, '1755.00', 13, '135.00', 'NonFree', 'ORD1000000001', 107),
(92, '0.00', 0, NULL, '10696.00', 14, '10696.00', 14, '764.00', 'NonFree', 'ORD1000000001', 108),
(93, '0.00', 0, NULL, '2860.00', 13, '2860.00', 13, '220.00', 'NonFree', 'ORD1000000001', 109),
(94, '0.00', 0, NULL, '2730.00', 14, '2730.00', 14, '195.00', 'NonFree', 'ORD1000000001', 110),
(95, '0.00', 0, NULL, '2400.00', 12, '2400.00', 12, '200.00', 'NonFree', 'ORD1000000001', 111),
(96, '0.00', 0, NULL, '0.00', 13, '0.00', 13, '0.00', 'Free', 'ORD1000000001', 112),
(97, '0.00', 0, NULL, '0.00', 11, '0.00', 11, '0.00', 'Free', 'ORD1000000001', 113),
(98, '0.00', 0, NULL, '176.00', 2, '176.00', 2, '88.00', 'NonFree', 'ORD1000000001', 114),
(135, '0.00', 0, NULL, '4200.00', 14, '4200.00', 14, '300.00', 'NonFree', 'ORD1000000011', 151),
(136, '0.00', 0, NULL, '300.00', 3, '300.00', 3, '100.00', 'NonFree', 'ORD1000000011', 152),
(137, '0.00', 0, NULL, '0.00', 12, '0.00', 12, '0.00', 'Free', 'ORD1000000011', 153),
(138, '0.00', 0, NULL, '0.00', 0, '2640.00', 12, '220.00', 'NonFree', 'ORD1000000011', 154),
(139, '0.00', 0, NULL, '0.00', 0, '4400.00', 22, '200.00', 'NonFree', 'ORD1000000011', 155),
(148, '0.00', 0, NULL, '4000.00', 20, '4000.00', 20, '200.00', 'NonFree', 'ORD1000000012', 164),
(149, '0.00', 0, NULL, '2250.00', 15, '2250.00', 15, '150.00', 'NonFree', 'ORD1000000012', 165),
(150, '0.00', 0, NULL, '3600.00', 12, '3600.00', 12, '300.00', 'NonFree', 'ORD1000000012', 166),
(151, '0.00', 0, NULL, '0.00', 0, '1320.00', 6, '220.00', 'NonFree', 'ORD1000000012', 167),
(152, '0.00', 0, NULL, '4200.00', 14, '4200.00', 14, '300.00', 'NonFree', 'ORD1000000013', 168),
(153, '0.00', 0, NULL, '2400.00', 12, '2400.00', 12, '200.00', 'NonFree', 'ORD1000000013', 169),
(154, '0.00', 0, NULL, '2025.00', 15, '2025.00', 15, '135.00', 'NonFree', 'ORD1000000013', 170),
(155, '0.00', 0, NULL, '2200.00', 10, '2200.00', 10, '220.00', 'NonFree', 'ORD1000000013', 171),
(156, '0.00', 0, NULL, '0.00', 14, '0.00', 14, '0.00', 'Free', 'ORD1000000013', 172),
(157, '0.00', 0, NULL, '0.00', 8, '0.00', 10, '0.00', 'Free', 'ORD1000000013', 173),
(158, '0.00', 0, NULL, '1120.00', 7, '1120.00', 7, '160.00', 'NonFree', 'ORD1000000014', 177),
(159, '0.00', 0, NULL, '1500.00', 5, '3000.00', 10, '300.00', 'NonFree', 'ORD1000000014', 178),
(160, '0.00', 0, NULL, '400.00', 2, '400.00', 2, '200.00', 'NonFree', 'ORD1000000014', 179),
(161, '0.00', 0, NULL, '0.00', 3, '0.00', 3, '0.00', 'Free', 'ORD1000000014', 180),
(162, '0.00', 0, NULL, '0.00', 0, '4000.00', 20, '200.00', 'NonFree', 'ORD1000000015', 203),
(163, '0.00', 0, NULL, '0.00', 0, '6000.00', 20, '300.00', 'NonFree', 'ORD1000000015', 204),
(164, '0.00', 0, NULL, '0.00', 0, '5000.00', 25, '200.00', 'NonFree', 'ORD1000000016', 205),
(165, '440.00', 2, NULL, '1100.00', 5, '1100.00', 5, '220.00', 'NonFree', 'ORD1000000016', 206),
(166, '20.00', 1, NULL, '60.00', 3, '60.00', 3, '20.00', 'NonFree', 'ORD1000000017', 207),
(167, '200.00', 1, NULL, '400.00', 2, '400.00', 2, '200.00', 'NonFree', 'ORD1000000017', 208),
(168, '3900.00', 13, NULL, '4500.00', 15, '4500.00', 15, '300.00', 'NonFree', 'ORD1000000017', 209),
(169, '600.00', 3, NULL, '1000.00', 5, '1000.00', 5, '200.00', 'NonFree', 'ORD1000000018', 210),
(170, '300.00', 1, NULL, '3000.00', 10, '3000.00', 10, '300.00', 'NonFree', 'ORD1000000018', 211),
(171, '0.00', 0, NULL, '80.00', 4, '80.00', 4, '20.00', 'NonFree', 'ORD1000000019', 212),
(172, '0.00', 0, NULL, '200.00', 1, '200.00', 1, '200.00', 'NonFree', 'ORD1000000019', 213),
(173, '0.00', 0, NULL, '4800.00', 24, '4800.00', 24, '200.00', 'NonFree', 'ORD1000000020', 296),
(174, '0.00', 0, NULL, '0.00', 0, '6000.00', 20, '300.00', 'NonFree', 'ORD1000000020', 297),
(175, '0.00', 0, NULL, '1000.00', 5, '1000.00', 5, '200.00', 'NonFree', 'ORD1000000021', 298),
(176, '0.00', 0, NULL, '5500.00', 25, '5500.00', 25, '220.00', 'NonFree', 'ORD1000000021', 299),
(177, '0.00', 0, NULL, '0.00', 0, '1800.00', 6, '300.00', 'NonFree', 'ORD1000000022', 304),
(178, '0.00', 0, NULL, '0.00', 0, '284.00', 4, '71.00', 'NonFree', 'ORD1000000022', 305),
(179, '0.00', 0, NULL, '0.00', 0, '880.00', 4, '220.00', 'NonFree', 'ORD1000000022', 306),
(180, '0.00', 0, NULL, '0.00', 0, '480.00', 3, '160.00', 'NonFree', 'ORD1000000023', 355),
(181, '0.00', 0, NULL, '0.00', 0, '590.00', 5, '118.00', 'NonFree', 'ORD1000000023', 356),
(182, '0.00', 0, NULL, '0.00', 0, '1000.00', 5, '200.00', 'NonFree', 'ORD1000000023', 357),
(183, '0.00', 0, NULL, '0.00', 0, '0.00', 1, '0.00', 'Free', 'ORD1000000023', 358),
(184, '0.00', 0, NULL, '0.00', 0, '4400.00', 22, '200.00', 'NonFree', 'ORD1000000024', 361);

-- --------------------------------------------------------

--
-- Table structure for table `order_product_issue_details`
--

CREATE TABLE `order_product_issue_details` (
  `order_issue_id` bigint(20) NOT NULL,
  `issued_to_db` bigint(20) DEFAULT NULL,
  `issued_by_gk` bigint(20) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product_issue_details`
--

INSERT INTO `order_product_issue_details` (`order_issue_id`, `issued_to_db`, `issued_by_gk`, `order_id`) VALUES
(1, 14, 13, 'ORD1000000003'),
(2, 14, 13, 'ORD1000000002'),
(3, 14, 13, 'ORD1000000004'),
(4, 10, 11, 'ORD1000000005'),
(5, 10, 11, 'ORD1000000006'),
(6, 10, 11, 'ORD1000000007'),
(7, 10, 11, 'ORD1000000008'),
(8, 14, 13, 'ORD1000000009'),
(9, 14, 13, 'ORD1000000001'),
(10, 10, 11, 'ORD1000000011'),
(11, 10, 11, 'ORD1000000012'),
(12, 14, 13, 'ORD1000000013'),
(13, 14, 13, 'ORD1000000014'),
(14, 10, 11, 'ORD1000000016'),
(15, 10, 11, 'ORD1000000019'),
(16, 10, 11, 'ORD1000000018'),
(17, 10, 11, 'ORD1000000017'),
(18, 10, 11, 'ORD1000000021'),
(19, 10, 11, 'ORD1000000020');

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `order_status_id` bigint(20) NOT NULL,
  `Order_status_added_datetime` datetime DEFAULT NULL,
  `details` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`order_status_id`, `Order_status_added_datetime`, `details`, `status`) VALUES
(1, NULL, NULL, 'Cancelled'),
(2, NULL, NULL, 'Issued'),
(3, NULL, NULL, 'Delivered'),
(4, NULL, NULL, 'Packed'),
(5, NULL, NULL, 'Delivered Pending'),
(6, NULL, NULL, 'Booked');

-- --------------------------------------------------------

--
-- Table structure for table `order_used_brand`
--

CREATE TABLE `order_used_brand` (
  `brand_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_used_brand`
--

INSERT INTO `order_used_brand` (`brand_id`, `name`) VALUES
(19, 'BLUE SQUARE1_2'),
(20, 'BLUE SQUARE1_2'),
(21, 'BLUE SQUARE1_2'),
(22, 'BLUE SQUARE1_2'),
(23, 'BLUE SQUARE1_2'),
(24, 'BLUE SQUARE1_2'),
(25, 'BLUE SQUARE1_2'),
(36, 'BLUE SQUARE1_2'),
(37, 'BLUE SQUARE1_2'),
(38, 'BLUE SQUARE1_2'),
(39, 'BLUE SQUARE1_2'),
(40, 'BLUE SQUARE1_2'),
(41, 'BLUE SQUARE1_2'),
(42, 'BLUE SQUARE1_2'),
(43, 'BLUE SQUARE1_2'),
(44, 'BLUE SQUARE1_2'),
(45, 'BLUE SQUARE1_2'),
(46, 'BLUE SQUARE1_2'),
(47, 'BLUE SQUARE1_2'),
(48, 'BLUE SQUARE1_2'),
(49, 'BLUE SQUARE1_2'),
(50, 'BLUE SQUARE1_2'),
(57, 'BLUE SQUARE1_2'),
(58, 'BLUE SQUARE1_2'),
(59, 'BLUE SQUARE1_2'),
(66, 'BLUE SQUARE'),
(67, 'BLUE SQUARE'),
(68, 'BLUE SQUARE'),
(69, 'BLUE SQUARE'),
(70, 'BLUE SQUARE'),
(71, 'BLUE SQUARE'),
(72, 'BLUE SQUARE'),
(73, 'BLUE SQUARE'),
(74, 'BLUE SQUARE1_2'),
(75, 'BLUE SQUARE1_2'),
(76, 'BLUE SQUARE1_2'),
(77, 'BLUE SQUARE1_2'),
(107, 'BLUE SQUARE1_2'),
(108, 'BLUE SQUARE1_2'),
(109, 'BLUE SQUARE1_2'),
(110, 'BLUE SQUARE1_2'),
(111, 'BLUE SQUARE1_2'),
(112, 'BLUE SQUARE1_2'),
(113, 'BLUE SQUARE1_2'),
(114, 'BLUE SQUARE1_2'),
(151, 'BLUE SQUARE'),
(152, 'BLUE SQUARE'),
(153, 'BLUE SQUARE'),
(154, 'BLUE SQUARE'),
(155, 'BLUE SQUARE'),
(164, 'BLUE SQUARE'),
(165, 'BLUE SQUARE'),
(166, 'BLUE SQUARE'),
(167, 'BLUE SQUARE'),
(168, 'BLUE SQUARE1_2'),
(169, 'BLUE SQUARE1_2'),
(170, 'BLUE SQUARE1_2'),
(171, 'BLUE SQUARE1_2'),
(172, 'BLUE SQUARE1_2'),
(173, 'BLUE SQUARE1_2'),
(174, 'BLUE SQUARE1_2'),
(175, 'BLUE SQUARE1_2'),
(176, 'BLUE SQUARE1_2'),
(177, 'BLUE SQUARE1_2'),
(178, 'BLUE SQUARE1_2'),
(179, 'BLUE SQUARE1_2'),
(180, 'BLUE SQUARE1_2'),
(181, 'BLUE SQUARE1_2'),
(182, 'BLUE SQUARE1_2'),
(183, 'BLUE SQUARE1_2'),
(184, 'BLUE SQUARE1_2'),
(185, 'BLUE SQUARE1_2'),
(186, 'BLUE SQUARE1_2'),
(187, 'BLUE SQUARE1_2'),
(193, 'BLUE SQUARE1_2'),
(194, 'BLUE SQUARE1_2'),
(195, 'BLUE SQUARE1_2'),
(196, 'BLUE SQUARE1_2'),
(197, 'BLUE SQUARE1_2'),
(198, 'BLUE SQUARE1_2'),
(203, 'BLUE SQUARE'),
(204, 'BLUE SQUARE'),
(205, 'BLUE SQUARE'),
(206, 'BLUE SQUARE'),
(207, 'BLUE SQUARE'),
(208, 'BLUE SQUARE'),
(209, 'BLUE SQUARE'),
(210, 'BLUE SQUARE'),
(211, 'BLUE SQUARE'),
(212, 'BLUE SQUARE'),
(213, 'BLUE SQUARE'),
(214, 'BLUE SQUARE'),
(215, 'BLUE SQUARE'),
(226, 'BLUE SQUARE1_2'),
(227, 'BLUE SQUARE1_2'),
(228, 'BLUE SQUARE1_2'),
(229, 'BLUE SQUARE1_2'),
(230, 'BLUE SQUARE1_2'),
(233, 'BLUE SQUARE1_2'),
(244, 'BLUE SQUARE'),
(252, 'BLUE SQUARE1_2'),
(262, 'BLUE SQUARE1_2'),
(263, 'BLUE SQUARE1_2'),
(264, 'BLUE SQUARE1_2'),
(265, 'BLUE SQUARE'),
(266, 'BLUE SQUARE'),
(267, 'BLUE SQUARE'),
(268, 'BLUE SQUARE'),
(269, 'BLUE SQUARE'),
(270, 'BLUE SQUARE'),
(271, 'BLUE SQUARE'),
(272, 'BLUE SQUARE'),
(273, 'BLUE SQUARE'),
(274, 'BLUE SQUARE'),
(275, 'BLUE SQUARE'),
(276, 'BLUE SQUARE'),
(277, 'BLUE SQUARE'),
(278, 'BLUE SQUARE'),
(279, 'BLUE SQUARE'),
(280, 'BLUE SQUARE'),
(281, 'BLUE SQUARE'),
(282, 'BLUE SQUARE'),
(283, 'BLUE SQUARE'),
(284, 'BLUE SQUARE'),
(285, 'BLUE SQUARE'),
(286, 'BLUE SQUARE'),
(287, 'BLUE SQUARE'),
(288, 'BLUE SQUARE'),
(289, 'BLUE SQUARE'),
(290, 'BLUE SQUARE'),
(291, 'BLUE SQUARE'),
(292, 'BLUE SQUARE'),
(293, 'BLUE SQUARE'),
(294, 'BLUE SQUARE'),
(295, 'BLUE SQUARE'),
(296, 'BLUE SQUARE'),
(297, 'BLUE SQUARE'),
(298, 'BLUE SQUARE'),
(299, 'BLUE SQUARE'),
(300, 'BLUE SQUARE'),
(301, 'BLUE SQUARE'),
(302, 'BLUE SQUARE'),
(303, 'BLUE SQUARE'),
(304, 'BLUE SQUARE1_2'),
(305, 'BLUE SQUARE1_2'),
(306, 'BLUE SQUARE1_2'),
(307, 'BLUE SQUARE'),
(317, 'BLUE SQUARE1_2'),
(320, 'BLUE SQUARE1_2'),
(321, 'BLUE SQUARE1_2'),
(322, 'BLUE SQUARE1_2'),
(323, 'BLUE SQUARE1_2'),
(324, 'BLUE SQUARE1_2'),
(325, 'BLUE SQUARE1_2'),
(326, 'BLUE SQUARE1_2'),
(327, 'BLUE SQUARE1_2'),
(328, 'BLUE SQUARE1_2'),
(329, 'BLUE SQUARE1_2'),
(330, 'BLUE SQUARE1_2'),
(331, 'BLUE SQUARE1_2'),
(335, 'BLUE SQUARE1_2'),
(338, 'BLUE SQUARE1_2'),
(342, 'BLUE SQUARE1_2'),
(346, 'BLUE SQUARE1_2'),
(347, 'BLUE SQUARE1_2'),
(348, 'BLUE SQUARE1_2'),
(354, 'BLUE SQUARE1_2'),
(355, 'BLUE SQUARE1_2'),
(356, 'BLUE SQUARE1_2'),
(357, 'BLUE SQUARE1_2'),
(358, 'BLUE SQUARE1_2'),
(359, 'BLUE SQUARE1_2'),
(360, 'BLUE SQUARE1_2'),
(361, 'BLUE SQUARE'),
(365, 'BLUE SQUARE'),
(366, 'BLUE SQUARE'),
(367, 'BLUE SQUARE'),
(368, 'BLUE SQUARE'),
(369, 'BLUE SQUARE1_2'),
(370, 'BLUE SQUARE1_2'),
(371, 'BLUE SQUARE1_2'),
(372, 'BLUE SQUARE1_2'),
(373, 'BLUE SQUARE1_2'),
(374, 'BLUE SQUARE1_2'),
(375, 'BLUE SQUARE1_2'),
(376, 'BLUE SQUARE1_2'),
(378, 'BLUE SQUARE1_2');

-- --------------------------------------------------------

--
-- Table structure for table `order_used_categories`
--

CREATE TABLE `order_used_categories` (
  `category_id` bigint(20) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `cgst` decimal(19,2) DEFAULT NULL,
  `hsn_code` varchar(255) DEFAULT NULL,
  `igst` decimal(19,2) DEFAULT NULL,
  `sgst` decimal(19,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_used_categories`
--

INSERT INTO `order_used_categories` (`category_id`, `category_name`, `cgst`, `hsn_code`, `igst`, `sgst`) VALUES
(19, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(20, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(21, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(22, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(23, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(24, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(25, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(36, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(37, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(38, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(39, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(40, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(41, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(42, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(43, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(44, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(45, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(46, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(47, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(48, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(49, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(50, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(57, 'SPIKE GUARD', '6.00', '8536', '12.00', '6.00'),
(58, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(59, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(66, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(67, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(68, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(69, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(70, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(71, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(72, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(73, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(74, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(75, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(76, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(77, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(107, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(108, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(109, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(110, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(111, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(112, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(113, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(114, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(151, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(152, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(153, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(154, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(155, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(164, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(165, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(166, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(167, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(168, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(169, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(170, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(171, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(172, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(173, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(174, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(175, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(176, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(177, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(178, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(179, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(180, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(181, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(182, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(183, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(184, 'SPIKE GUARD', '6.00', '8536', '12.00', '6.00'),
(185, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(186, 'SPIKE GUARD', '6.00', '8536', '12.00', '6.00'),
(187, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(193, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(194, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(195, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(196, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(197, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(198, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(203, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(204, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(205, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(206, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(207, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(208, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(209, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(210, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(211, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(212, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(213, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(214, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(215, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(226, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(227, 'SPIKE GUARD', '6.00', '8536', '12.00', '6.00'),
(228, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(229, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(230, 'SPIKE GUARD', '6.00', '8536', '12.00', '6.00'),
(233, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(244, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(252, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(262, 'SPIKE GUARD', '6.00', '8536', '12.00', '6.00'),
(263, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(264, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(265, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(266, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(267, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(268, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(269, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(270, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(271, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(272, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(273, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(274, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(275, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(276, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(277, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(278, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(279, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(280, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(281, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(282, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(283, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(284, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(285, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(286, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(287, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(288, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(289, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(290, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(291, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(292, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(293, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(294, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(295, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(296, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(297, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(298, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(299, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(300, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(301, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(302, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(303, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(304, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(305, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(306, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(307, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(317, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(320, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(321, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(322, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(323, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(324, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(325, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(326, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(327, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(328, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(329, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(330, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(331, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(335, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(338, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(342, 'SPIKE GUARD', '6.00', '8536', '12.00', '6.00'),
(346, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(347, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(348, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(354, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(355, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(356, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(357, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(358, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(359, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(360, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(361, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(365, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(366, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(367, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(368, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(369, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(370, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(371, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(372, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(373, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(374, 'KEYBOARD', '6.00', '8471', '12.00', '6.00'),
(375, 'MOUSE', '9.00', '8472', '18.00', '9.00'),
(376, 'CABLES', '9.00', '8544', '18.00', '9.00'),
(378, 'SPIKE GUARD', '6.00', '8536', '12.00', '6.00');

-- --------------------------------------------------------

--
-- Table structure for table `order_used_product`
--

CREATE TABLE `order_used_product` (
  `product_id` bigint(20) NOT NULL,
  `current_quantity` bigint(20) DEFAULT NULL,
  `damage_quantity` bigint(20) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `product_content_type` varchar(255) DEFAULT NULL,
  `product_image` longblob,
  `product_name` varchar(255) DEFAULT NULL,
  `rate` decimal(19,2) DEFAULT NULL,
  `threshold` bigint(20) DEFAULT NULL,
  `brand_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `product_entity_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_used_product`
--

INSERT INTO `order_used_product` (`product_id`, `current_quantity`, `damage_quantity`, `product_code`, `product_content_type`, `product_image`, `product_name`, `rate`, `threshold`, `brand_id`, `category_id`, `product_entity_id`) VALUES
(19, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 19, 19, 28),
(20, 9, 10, '', NULL, NULL, 'Blue Power Code 5 Mtr', '120.00', 0, 20, 20, 44),
(21, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 21, 21, 29),
(22, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 22, 22, 28),
(23, 50, 31, '75', NULL, NULL, 'Blue Power Code 3  Mtr', '75.00', 10, 23, 23, 43),
(24, 43, 0, NULL, NULL, NULL, 'Blue Power Code 1.5  Mtr', '50.00', 0, 24, 24, 42),
(25, 50, 1, '8471', NULL, NULL, 'Blue keyboard USB MINI', '236.61', 0, 25, 25, 52),
(36, 47, 2, '12121', NULL, NULL, 'MouseTest', '10.00', 1, 36, 36, 51),
(37, 46, 0, NULL, NULL, NULL, 'Blue Power Code Laptop 1.5 Mtr', '60.00', 0, 37, 37, 46),
(38, 50, 1, '8471', NULL, NULL, 'Blue keyboard USB MINI', '236.61', 0, 38, 38, 52),
(39, 44, 3, NULL, NULL, NULL, 'Blue USB Printer Cable 1.5 Mtr', '50.00', 0, 39, 39, 49),
(40, 46, 0, NULL, NULL, NULL, 'Blue Power Code Laptop 1.5 Mtr', '60.00', 0, 40, 40, 46),
(41, 50, 1, '8471', NULL, NULL, 'Blue keyboard USB MINI', '236.61', 0, 41, 41, 52),
(42, 60, 2, NULL, NULL, NULL, 'Blue LAN Cable 1.5 Mtr  Cat 5', '33.90', 0, 42, 42, 53),
(43, 47, 2, '12121', NULL, NULL, 'MouseTest', '10.00', 1, 43, 43, 51),
(44, 60, 2, NULL, NULL, NULL, 'Blue LAN Cable 1.5 Mtr  Cat 5', '33.90', 0, 44, 44, 53),
(45, 47, 2, '12121', NULL, NULL, 'MouseTest', '10.00', 1, 45, 45, 51),
(46, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 46, 46, 28),
(47, 50, 0, '', NULL, NULL, 'Blue USB Printer Cable 3 Mtr', '75.00', 10, 47, 47, 50),
(48, 43, 0, NULL, NULL, NULL, 'Blue Power Code 1.5  Mtr', '50.00', 0, 48, 48, 42),
(49, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 49, 49, 28),
(50, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 50, 50, 30),
(57, 50, 0, '', NULL, NULL, 'Blue Spike Guard 116 ( 6x6 ) 5 Mtr', '420.37', 10, 57, 57, 41),
(58, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 58, 58, 30),
(59, 47, 2, '12121', NULL, NULL, 'MouseTest', '10.00', 1, 59, 59, 51),
(66, 48, 2, 'M22', 'image/png', 0x89504e470d0a1a0a0000000d4948445200000014000000140803000000ba57ed3f0000014a504c54450000006565653268cc3269d13665bd3269d03269d16565653765b93269d13e62a56565653765b94861903b65b34f61844e62863268cc3269cf3268ca3b66b64d648e3a64b33268cc3268cd3268cc3d64ac3b66b73c66b63269d23269cf3368ca3368ca3467c43766be326ad2326ad3346acc356acc3469cb3569ca3269ce3269ce326bd4326cd7326ad33e71cf326ad33e70cf3471e33472e43472e53572e43676eb3776eb3873e03b80ff3c81ff3d82ff4083ff4084ff447ce54486fc4686fc4687fc4887fc4888fc4980e64a7fe24a80e45183e05789e45a8ae35b88dc5e88d56494ec658edb6696ee6c94e06ca0ff6e95dc6e99e8749de9799fe57aa1ea7ea3e685a9ec8baae48cace58dace78eade88fade690b1ec92afe697b3e4a1bae9a3bceaa8c2f5b7caeeb8cceeb9ccf0c4d4f1cbd9f3cddaf4d4e0f4dce6f6e1e9f8e4ebf8ebf0faf2f5fbdf9991190000003174524e53000608080a0c0c0e111819191e2023333b3d3d3f404a5a616162647e80888c8e9999a7d8d8dcdde3e3ebecf0f0f3f3f4f48cf54e62000000d24944415418d363602015300b49480ab3a008b1c9ea18bb791a692bf020c438559c13b373737352dc35456062accade1959609019acc5051594734e4d8782340f4546b01893764c320c24c5e9f2820505cde313a020ccdfc2500c2c28ee12111d0b04e1818181fe16a6d2604149dbc0a0904030b0b7b73493010b0a9bd8c38195b59114589045dbd1c9c9c90604ec1c1cf5f9214e9277f5850357256e882087aa5f2814f8a90b30425dcfa7e613190504913e1aa2ec70cf7329ea79050478192809b023051323af98b48c143f372389210e0072a3352d01fe58fd0000000049454e44ae426082, 'Mouse 22', '186.44', 10, 66, 66, 5),
(67, 50, 0, 'K23', 'image/png', 0x89504e470d0a1a0a0000000d4948445200000014000000140803000000ba57ed3f0000014a504c54450000006565653268cc3269d13665bd3269d03269d16565653765b93269d13e62a56565653765b94861903b65b34f61844e62863268cc3269cf3268ca3b66b64d648e3a64b33268cc3268cd3268cc3d64ac3b66b73c66b63269d23269cf3368ca3368ca3467c43766be326ad2326ad3346acc356acc3469cb3569ca3269ce3269ce326bd4326cd7326ad33e71cf326ad33e70cf3471e33472e43472e53572e43676eb3776eb3873e03b80ff3c81ff3d82ff4083ff4084ff447ce54486fc4686fc4687fc4887fc4888fc4980e64a7fe24a80e45183e05789e45a8ae35b88dc5e88d56494ec658edb6696ee6c94e06ca0ff6e95dc6e99e8749de9799fe57aa1ea7ea3e685a9ec8baae48cace58dace78eade88fade690b1ec92afe697b3e4a1bae9a3bceaa8c2f5b7caeeb8cceeb9ccf0c4d4f1cbd9f3cddaf4d4e0f4dce6f6e1e9f8e4ebf8ebf0faf2f5fbdf9991190000003174524e53000608080a0c0c0e111819191e2023333b3d3d3f404a5a616162647e80888c8e9999a7d8d8dcdde3e3ebecf0f0f3f3f4f48cf54e62000000d24944415418d363602015300b49480ab3a008b1c9ea18bb791a692bf020c438559c13b373737352dc35456062accade1959609019acc5051594734e4d8782340f4546b01893764c320c24c5e9f2820505cde313a020ccdfc2500c2c28ee12111d0b04e1818181fe16a6d2604149dbc0a0904030b0b7b73493010b0a9bd8c38195b59114589045dbd1c9c9c90604ec1c1cf5f9214e9277f5850357256e882087aa5f2814f8a90b30425dcfa7e613190504913e1aa2ec70cf7329ea79050478192809b023051323af98b48c143f372389210e0072a3352d01fe58fd0000000049454e44ae426082, 'KeyBoard 23', '133.93', 10, 67, 67, 3),
(68, 50, 8, 'K22', 'image/png', 0x89504e470d0a1a0a0000000d4948445200000014000000140803000000ba57ed3f0000014a504c54450000006565653268cc3269d13665bd3269d03269d16565653765b93269d13e62a56565653765b94861903b65b34f61844e62863268cc3269cf3268ca3b66b64d648e3a64b33268cc3268cd3268cc3d64ac3b66b73c66b63269d23269cf3368ca3368ca3467c43766be326ad2326ad3346acc356acc3469cb3569ca3269ce3269ce326bd4326cd7326ad33e71cf326ad33e70cf3471e33472e43472e53572e43676eb3776eb3873e03b80ff3c81ff3d82ff4083ff4084ff447ce54486fc4686fc4687fc4887fc4888fc4980e64a7fe24a80e45183e05789e45a8ae35b88dc5e88d56494ec658edb6696ee6c94e06ca0ff6e95dc6e99e8749de9799fe57aa1ea7ea3e685a9ec8baae48cace58dace78eade88fade690b1ec92afe697b3e4a1bae9a3bceaa8c2f5b7caeeb8cceeb9ccf0c4d4f1cbd9f3cddaf4d4e0f4dce6f6e1e9f8e4ebf8ebf0faf2f5fbdf9991190000003174524e53000608080a0c0c0e111819191e2023333b3d3d3f404a5a616162647e80888c8e9999a7d8d8dcdde3e3ebecf0f0f3f3f4f48cf54e62000000d24944415418d363602015300b49480ab3a008b1c9ea18bb791a692bf020c438559c13b373737352dc35456062accade1959609019acc5051594734e4d8782340f4546b01893764c320c24c5e9f2820505cde313a020ccdfc2500c2c28ee12111d0b04e1818181fe16a6d2604149dbc0a0904030b0b7b73493010b0a9bd8c38195b59114589045dbd1c9c9c90604ec1c1cf5f9214e9277f5850357256e882087aa5f2814f8a90b30425dcfa7e613190504913e1aa2ec70cf7329ea79050478192809b023051323af98b48c143f372389210e0072a3352d01fe58fd0000000049454e44ae426082, 'KeyBoard 22', '267.86', 10, 68, 68, 2),
(69, 50, 0, 'K23', 'image/png', 0x89504e470d0a1a0a0000000d4948445200000014000000140803000000ba57ed3f0000014a504c54450000006565653268cc3269d13665bd3269d03269d16565653765b93269d13e62a56565653765b94861903b65b34f61844e62863268cc3269cf3268ca3b66b64d648e3a64b33268cc3268cd3268cc3d64ac3b66b73c66b63269d23269cf3368ca3368ca3467c43766be326ad2326ad3346acc356acc3469cb3569ca3269ce3269ce326bd4326cd7326ad33e71cf326ad33e70cf3471e33472e43472e53572e43676eb3776eb3873e03b80ff3c81ff3d82ff4083ff4084ff447ce54486fc4686fc4687fc4887fc4888fc4980e64a7fe24a80e45183e05789e45a8ae35b88dc5e88d56494ec658edb6696ee6c94e06ca0ff6e95dc6e99e8749de9799fe57aa1ea7ea3e685a9ec8baae48cace58dace78eade88fade690b1ec92afe697b3e4a1bae9a3bceaa8c2f5b7caeeb8cceeb9ccf0c4d4f1cbd9f3cddaf4d4e0f4dce6f6e1e9f8e4ebf8ebf0faf2f5fbdf9991190000003174524e53000608080a0c0c0e111819191e2023333b3d3d3f404a5a616162647e80888c8e9999a7d8d8dcdde3e3ebecf0f0f3f3f4f48cf54e62000000d24944415418d363602015300b49480ab3a008b1c9ea18bb791a692bf020c438559c13b373737352dc35456062accade1959609019acc5051594734e4d8782340f4546b01893764c320c24c5e9f2820505cde313a020ccdfc2500c2c28ee12111d0b04e1818181fe16a6d2604149dbc0a0904030b0b7b73493010b0a9bd8c38195b59114589045dbd1c9c9c90604ec1c1cf5f9214e9277f5850357256e882087aa5f2814f8a90b30425dcfa7e613190504913e1aa2ec70cf7329ea79050478192809b023051323af98b48c143f372389210e0072a3352d01fe58fd0000000049454e44ae426082, 'KeyBoard 23', '133.93', 10, 69, 69, 3),
(70, 50, 0, 'K23', 'image/png', 0x89504e470d0a1a0a0000000d4948445200000014000000140803000000ba57ed3f0000014a504c54450000006565653268cc3269d13665bd3269d03269d16565653765b93269d13e62a56565653765b94861903b65b34f61844e62863268cc3269cf3268ca3b66b64d648e3a64b33268cc3268cd3268cc3d64ac3b66b73c66b63269d23269cf3368ca3368ca3467c43766be326ad2326ad3346acc356acc3469cb3569ca3269ce3269ce326bd4326cd7326ad33e71cf326ad33e70cf3471e33472e43472e53572e43676eb3776eb3873e03b80ff3c81ff3d82ff4083ff4084ff447ce54486fc4686fc4687fc4887fc4888fc4980e64a7fe24a80e45183e05789e45a8ae35b88dc5e88d56494ec658edb6696ee6c94e06ca0ff6e95dc6e99e8749de9799fe57aa1ea7ea3e685a9ec8baae48cace58dace78eade88fade690b1ec92afe697b3e4a1bae9a3bceaa8c2f5b7caeeb8cceeb9ccf0c4d4f1cbd9f3cddaf4d4e0f4dce6f6e1e9f8e4ebf8ebf0faf2f5fbdf9991190000003174524e53000608080a0c0c0e111819191e2023333b3d3d3f404a5a616162647e80888c8e9999a7d8d8dcdde3e3ebecf0f0f3f3f4f48cf54e62000000d24944415418d363602015300b49480ab3a008b1c9ea18bb791a692bf020c438559c13b373737352dc35456062accade1959609019acc5051594734e4d8782340f4546b01893764c320c24c5e9f2820505cde313a020ccdfc2500c2c28ee12111d0b04e1818181fe16a6d2604149dbc0a0904030b0b7b73493010b0a9bd8c38195b59114589045dbd1c9c9c90604ec1c1cf5f9214e9277f5850357256e882087aa5f2814f8a90b30425dcfa7e613190504913e1aa2ec70cf7329ea79050478192809b023051323af98b48c143f372389210e0072a3352d01fe58fd0000000049454e44ae426082, 'KeyBoard 23', '133.93', 10, 70, 70, 3),
(71, 50, 8, 'K22', 'image/png', 0x89504e470d0a1a0a0000000d4948445200000014000000140803000000ba57ed3f0000014a504c54450000006565653268cc3269d13665bd3269d03269d16565653765b93269d13e62a56565653765b94861903b65b34f61844e62863268cc3269cf3268ca3b66b64d648e3a64b33268cc3268cd3268cc3d64ac3b66b73c66b63269d23269cf3368ca3368ca3467c43766be326ad2326ad3346acc356acc3469cb3569ca3269ce3269ce326bd4326cd7326ad33e71cf326ad33e70cf3471e33472e43472e53572e43676eb3776eb3873e03b80ff3c81ff3d82ff4083ff4084ff447ce54486fc4686fc4687fc4887fc4888fc4980e64a7fe24a80e45183e05789e45a8ae35b88dc5e88d56494ec658edb6696ee6c94e06ca0ff6e95dc6e99e8749de9799fe57aa1ea7ea3e685a9ec8baae48cace58dace78eade88fade690b1ec92afe697b3e4a1bae9a3bceaa8c2f5b7caeeb8cceeb9ccf0c4d4f1cbd9f3cddaf4d4e0f4dce6f6e1e9f8e4ebf8ebf0faf2f5fbdf9991190000003174524e53000608080a0c0c0e111819191e2023333b3d3d3f404a5a616162647e80888c8e9999a7d8d8dcdde3e3ebecf0f0f3f3f4f48cf54e62000000d24944415418d363602015300b49480ab3a008b1c9ea18bb791a692bf020c438559c13b373737352dc35456062accade1959609019acc5051594734e4d8782340f4546b01893764c320c24c5e9f2820505cde313a020ccdfc2500c2c28ee12111d0b04e1818181fe16a6d2604149dbc0a0904030b0b7b73493010b0a9bd8c38195b59114589045dbd1c9c9c90604ec1c1cf5f9214e9277f5850357256e882087aa5f2814f8a90b30425dcfa7e613190504913e1aa2ec70cf7329ea79050478192809b023051323af98b48c143f372389210e0072a3352d01fe58fd0000000049454e44ae426082, 'KeyBoard 22', '267.86', 10, 71, 71, 2),
(72, 43, 21, 'K21', 'image/png', 0x89504e470d0a1a0a0000000d4948445200000014000000140803000000ba57ed3f000000ab504c54450000001313131c47881e78a31f49aa1f58b01f7cb0206bbd206dbc2074bd2088bd2164ca2183ca218eca2277d7236bd82463eb2470e52481e52488e42a85ed2a92ed367fee368eee3876974487ef4495ef4a7eef5290f0529bf05690b356a9b35fa3f16492f16ba1f278a9f378b2f380a6f485b1f491b9f593c2f598b6f69ec9cd9fc1f6a0cdf6a5bff7aad7dcaccaf7acd1f7b3c7f8b8d1f8bae3f8f5f5f5f8dbb0f8dcc7f8e3bafbece14990287b0000000174524e530040e6d8660000009c4944415418d3a5d0b10a02410c84e17f6541116c14b4116c2d6c7cff57b11341c1ea404e379b89857b9cd64ef9312421f0571270007a0a02e00219a8f3c76206403e55800940eb9023ba117db0b28b86f669e6881436366b3315fdcccc11497885cff612eb4b8d28517ca501114b93c2c2e423ca3a80e9cbbd6f2899b8a523f7c27753a42d6013c580aeeb739f8df3541a1ec2c6fc0538d16efe2f6f80405bac17f4208c0000000049454e44ae426082, 'KeyBoard 21', '178.57', 10, 72, 72, 1),
(73, 43, 21, 'K21', 'image/png', 0x89504e470d0a1a0a0000000d4948445200000014000000140803000000ba57ed3f000000ab504c54450000001313131c47881e78a31f49aa1f58b01f7cb0206bbd206dbc2074bd2088bd2164ca2183ca218eca2277d7236bd82463eb2470e52481e52488e42a85ed2a92ed367fee368eee3876974487ef4495ef4a7eef5290f0529bf05690b356a9b35fa3f16492f16ba1f278a9f378b2f380a6f485b1f491b9f593c2f598b6f69ec9cd9fc1f6a0cdf6a5bff7aad7dcaccaf7acd1f7b3c7f8b8d1f8bae3f8f5f5f5f8dbb0f8dcc7f8e3bafbece14990287b0000000174524e530040e6d8660000009c4944415418d3a5d0b10a02410c84e17f6541116c14b4116c2d6c7cff57b11341c1ea404e379b89857b9cd64ef9312421f0571270007a0a02e00219a8f3c76206403e55800940eb9023ba117db0b28b86f669e6881436366b3315fdcccc11497885cff612eb4b8d28517ca501114b93c2c2e423ca3a80e9cbbd6f2899b8a523f7c27753a42d6013c580aeeb739f8df3541a1ec2c6fc0538d16efe2f6f80405bac17f4208c0000000049454e44ae426082, 'KeyBoard 21', '178.57', 10, 73, 73, 1),
(74, 24, 0, '', NULL, NULL, 'Blue USB Mouse B -201', '165.25', 0, 74, 74, 32),
(75, 17, 1, '', '', NULL, 'Blue Keyboard USB', '178.57', 0, 75, 75, 28),
(76, 24, 0, '', NULL, NULL, 'Blue USB Mouse B -201', '165.25', 0, 76, 76, 32),
(77, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 77, 77, 30),
(107, 50, 0, NULL, NULL, NULL, 'Blue USB Mouse B -202', '114.41', 0, 107, 107, 33),
(108, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 108, 108, 30),
(109, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 109, 109, 29),
(110, 24, 0, '', NULL, NULL, 'Blue USB Mouse B -201', '165.25', 0, 110, 110, 32),
(111, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 111, 111, 28),
(112, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 112, 112, 30),
(113, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 113, 113, 29),
(114, 50, 0, NULL, NULL, NULL, 'Blue USB Printer Cable 3 Mtr', '75.00', 0, 114, 114, 50),
(151, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 151, 151, 2),
(152, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 152, 152, 3),
(153, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 153, 153, 1),
(154, 48, 2, 'M22', NULL, NULL, 'Mouse 22', '186.44', 0, 154, 154, 5),
(155, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 155, 155, 1),
(164, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 164, 164, 1),
(165, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 165, 165, 3),
(166, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 166, 166, 2),
(167, 48, 2, 'M22', NULL, NULL, 'Mouse 22', '186.44', 0, 167, 167, 5),
(168, 26, 7, NULL, NULL, NULL, 'Blue Wireless Mouse 2.4GHZ', '254.24', 0, 168, 168, 31),
(169, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 169, 169, 28),
(170, 50, 0, NULL, NULL, NULL, 'Blue USB Mouse B -202', '114.41', 0, 170, 170, 33),
(171, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 171, 171, 29),
(172, 26, 7, NULL, NULL, NULL, 'Blue Wireless Mouse 2.4GHZ', '254.24', 0, 172, 172, 31),
(173, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 173, 173, 29),
(174, 50, 1, '8471', NULL, NULL, 'Blue keyboard USB MINI', '236.61', 0, 174, 174, 52),
(175, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 175, 175, 28),
(176, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 176, 176, 30),
(177, 50, 43, '', NULL, NULL, 'Blue USB Mouse B -203', '135.59', 10, 177, 177, 34),
(178, 26, 7, NULL, NULL, NULL, 'Blue Wireless Mouse 2.4GHZ', '254.24', 0, 178, 178, 31),
(179, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 179, 179, 28),
(180, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 180, 180, 28),
(181, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 181, 181, 28),
(182, 37, 0, '', NULL, NULL, 'Blue  USB Mouse Mini BS 220', '100.00', 0, 182, 182, 36),
(183, 47, 2, '12121', NULL, NULL, 'MouseTest', '10.00', 1, 183, 183, 51),
(184, 50, 0, '', NULL, NULL, 'Blue Spike Guard 116 ( 6x6 ) 5 Mtr', '420.37', 10, 184, 184, 41),
(185, 43, 0, NULL, NULL, NULL, 'Blue Power Code 1.5  Mtr', '50.00', 0, 185, 185, 42),
(186, 31, 0, '', NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 2 Mtr 2 USB', '367.70', 10, 186, 186, 39),
(187, 47, 2, '12121', NULL, NULL, 'MouseTest', '10.00', 1, 187, 187, 51),
(193, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 193, 193, 30),
(194, 9, 10, '', NULL, NULL, 'Blue Power Code 5 Mtr', '120.00', 0, 194, 194, 44),
(195, 50, 1, '8471', NULL, NULL, 'Blue keyboard USB MINI', '236.61', 0, 195, 195, 52),
(196, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 196, 196, 30),
(197, 50, 43, '', NULL, NULL, 'Blue USB Mouse B -203', '135.59', 10, 197, 197, 34),
(198, 50, 0, '', NULL, NULL, 'Blue USB TO USB Cable 2.0', '75.00', 10, 198, 198, 47),
(203, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 203, 203, 1),
(204, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 204, 204, 2),
(205, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 205, 205, 1),
(206, 48, 2, 'M22', NULL, NULL, 'Mouse 22', '186.44', 0, 206, 206, 5),
(207, 17, 0, 'M21', 'image/png', NULL, 'Mouse 21', '16.95', 10, 207, 207, 4),
(208, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 208, 208, 1),
(209, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 209, 209, 2),
(210, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 210, 210, 1),
(211, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 211, 211, 2),
(212, 17, 0, 'M21', 'image/png', NULL, 'Mouse 21', '16.95', 10, 212, 212, 4),
(213, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 213, 213, 1),
(214, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 214, 214, 1),
(215, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 215, 215, 3),
(226, 50, 6, '', NULL, NULL, 'Blue USB Mouse m 10', '150.00', 0, 226, 226, 35),
(227, 31, 0, '', NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 2 Mtr 2 USB', '367.70', 10, 227, 227, 39),
(228, 50, 31, '75', NULL, NULL, 'Blue Power Code 3  Mtr', '75.00', 10, 228, 228, 43),
(229, 46, 0, NULL, NULL, NULL, 'Blue Power Code Laptop 1.5 Mtr', '60.00', 0, 229, 229, 46),
(230, 50, 10, '', NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 5 Mtr', '311.61', 10, 230, 230, 38),
(233, 50, 31, '75', NULL, NULL, 'Blue Power Code 3  Mtr', '75.00', 10, 233, 233, 43),
(244, 48, 2, 'M22', NULL, NULL, 'Mouse 22', '186.44', 0, 244, 244, 5),
(252, 50, 0, NULL, NULL, NULL, 'Blue USB Mouse B -202', '114.41', 0, 252, 252, 33),
(262, 50, 10, '', NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 5 Mtr', '311.61', 10, 262, 262, 38),
(263, 37, 0, '', NULL, NULL, 'Blue  USB Mouse Mini BS 220', '100.00', 0, 263, 263, 36),
(264, 50, 6, '', NULL, NULL, 'Blue USB Mouse m 10', '150.00', 0, 264, 264, 35),
(265, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 265, 265, 1),
(266, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 266, 266, 1),
(267, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 267, 267, 1),
(268, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 268, 268, 1),
(269, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 269, 269, 1),
(270, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 270, 270, 1),
(271, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 271, 271, 1),
(272, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 272, 272, 1),
(273, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 273, 273, 1),
(274, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 274, 274, 1),
(275, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 275, 275, 1),
(276, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 276, 276, 1),
(277, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 277, 277, 1),
(278, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 278, 278, 1),
(279, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 279, 279, 1),
(280, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 280, 280, 1),
(281, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 281, 281, 1),
(282, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 282, 282, 2),
(283, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 283, 283, 2),
(284, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 284, 284, 2),
(285, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 285, 285, 2),
(286, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 286, 286, 3),
(287, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 287, 287, 3),
(288, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 288, 288, 3),
(289, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 289, 289, 3),
(290, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 290, 290, 3),
(291, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 291, 291, 3),
(292, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 292, 292, 3),
(293, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 293, 293, 3),
(294, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 294, 294, 3),
(295, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 295, 295, 3),
(296, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 296, 296, 1),
(297, 50, 8, 'K22', NULL, NULL, 'KeyBoard 22', '267.86', 0, 297, 297, 2),
(298, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 298, 298, 1),
(299, 48, 2, 'M22', NULL, NULL, 'Mouse 22', '186.44', 0, 299, 299, 5),
(300, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 300, 300, 1),
(301, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 301, 301, 1),
(302, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 302, 302, 1),
(303, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 303, 303, 1),
(304, 26, 7, NULL, NULL, NULL, 'Blue Wireless Mouse 2.4GHZ', '254.24', 0, 304, 304, 31),
(305, 46, 0, NULL, NULL, NULL, 'Blue Power Code Laptop 1.5 Mtr', '60.00', 0, 305, 305, 46),
(306, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 306, 306, 29),
(307, 50, 0, 'K23', NULL, NULL, 'KeyBoard 23', '133.93', 0, 307, 307, 3),
(317, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 317, 317, 30),
(320, 44, 3, NULL, NULL, NULL, 'Blue USB Printer Cable 1.5 Mtr', '50.00', 0, 320, 320, 49),
(321, 50, 0, '', NULL, NULL, 'Blue USB TO USB Cable 3.0', '175.00', 0, 321, 321, 48),
(322, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 322, 322, 28),
(323, 24, 0, '', NULL, NULL, 'Blue USB Mouse B -201', '165.25', 0, 323, 323, 32),
(324, 9, 10, '', NULL, NULL, 'Blue Power Code 5 Mtr', '120.00', 0, 324, 324, 44),
(325, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 325, 325, 28),
(326, 47, 2, '12121', NULL, NULL, 'MouseTest', '10.00', 1, 326, 326, 51),
(327, 9, 10, '', NULL, NULL, 'Blue Power Code 5 Mtr', '120.00', 0, 327, 327, 44),
(328, 60, 2, NULL, NULL, NULL, 'Blue LAN Cable 1.5 Mtr  Cat 5', '33.90', 0, 328, 328, 53),
(329, 44, 3, NULL, NULL, NULL, 'Blue USB Printer Cable 1.5 Mtr', '50.00', 0, 329, 329, 49),
(330, 46, 0, NULL, NULL, NULL, 'Blue Power Code Laptop 1.5 Mtr', '60.00', 0, 330, 330, 46),
(331, 9, 10, '', NULL, NULL, 'Blue Power Code 5 Mtr', '120.00', 0, 331, 331, 44),
(335, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 335, 335, 30),
(338, 43, 0, NULL, NULL, NULL, 'Blue Power Code 1.5  Mtr', '50.00', 0, 338, 338, 42),
(342, 31, 0, '', NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 2 Mtr 2 USB', '367.70', 10, 342, 342, 39),
(346, 37, 0, '', NULL, NULL, 'Blue  USB Mouse Mini BS 220', '100.00', 0, 346, 346, 36),
(347, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 347, 347, 30),
(348, 9, 10, '', NULL, NULL, 'Blue Power Code 5 Mtr', '120.00', 0, 348, 348, 44),
(354, 26, 7, NULL, NULL, NULL, 'Blue Wireless Mouse 2.4GHZ', '254.24', 0, 354, 354, 31),
(355, 50, 43, '', NULL, NULL, 'Blue USB Mouse B -203', '135.59', 10, 355, 355, 34),
(356, 37, 0, '', NULL, NULL, 'Blue  USB Mouse Mini BS 220', '100.00', 0, 356, 356, 36),
(357, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 357, 357, 28),
(358, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 358, 358, 29),
(359, 60, 2, NULL, NULL, NULL, 'Blue LAN Cable 1.5 Mtr  Cat 5', '33.90', 0, 359, 359, 53),
(360, 60, 2, NULL, NULL, NULL, 'Blue LAN Cable 1.5 Mtr  Cat 5', '33.90', 0, 360, 360, 53),
(361, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 361, 361, 1),
(365, 17, 0, 'M21', 'image/png', NULL, 'Mouse 21', '16.95', 10, 365, 365, 4),
(366, 48, 2, 'M22', NULL, NULL, 'Mouse 22', '186.44', 0, 366, 366, 5),
(367, 17, 0, 'M21', 'image/png', NULL, 'Mouse 21', '16.95', 10, 367, 367, 4),
(368, 43, 21, 'K21', NULL, NULL, 'KeyBoard 21', '178.57', 0, 368, 368, 1),
(369, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 369, 369, 29),
(370, 9, 10, '', NULL, NULL, 'Blue Power Code 5 Mtr', '120.00', 0, 370, 370, 44),
(371, 20, 0, '', NULL, NULL, 'Blue Keyboard PS/2', '196.43', 0, 371, 371, 29),
(372, 26, 7, NULL, NULL, NULL, 'Blue Wireless Mouse 2.4GHZ', '254.24', 0, 372, 372, 31),
(373, 24, 0, '', NULL, NULL, 'Blue USB Mouse B -201', '165.25', 0, 373, 373, 32),
(374, 17, 1, '', NULL, NULL, 'Blue Keyboard USB', '178.57', 0, 374, 374, 28),
(375, 31, 0, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', '647.32', 0, 375, 375, 30),
(376, 46, 0, NULL, NULL, NULL, 'Blue Power Code Laptop 1.5 Mtr', '60.00', 0, 376, 376, 46),
(378, 31, 0, '', NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 2 Mtr 2 USB', '367.70', 10, 378, 378, 39);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `payment_id` bigint(20) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `cheque_date` datetime DEFAULT NULL,
  `cheque_number` varchar(255) DEFAULT NULL,
  `due_amount` decimal(19,2) DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `paid_amount` decimal(19,2) DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `pay_type` varchar(255) DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`payment_id`, `bank_name`, `cheque_date`, `cheque_number`, `due_amount`, `due_date`, `paid_amount`, `paid_date`, `pay_type`, `total_amount`, `order_id`, `status`) VALUES
(6, NULL, NULL, NULL, '2000.00', '2018-03-16 00:00:00', '1000.00', '2018-03-16 19:31:54', 'Cash', '3000.00', 'ORD1000000007', 0),
(7, NULL, NULL, NULL, '1500.00', '2018-04-05 00:00:00', '500.00', '2018-03-16 19:50:59', 'Cash', '3000.00', 'ORD1000000007', 0),
(16, NULL, NULL, NULL, '670.00', '2018-03-17 00:00:00', '200.00', '2018-03-17 13:43:30', 'Cash', '870.00', 'ORD1000000003', 0),
(17, NULL, NULL, NULL, '570.00', '2018-03-17 00:00:00', '100.00', '2018-03-17 13:43:40', 'Cash', '870.00', 'ORD1000000003', 0),
(18, NULL, NULL, NULL, '0.00', '2018-03-17 13:43:48', '570.00', '2018-03-17 13:43:48', 'Cash', '870.00', 'ORD1000000003', 0),
(19, NULL, NULL, NULL, '539.00', '2018-03-17 00:00:00', '50.00', '2018-03-17 16:41:38', 'Cash', '589.00', 'ORD1000000004', 0),
(20, NULL, NULL, NULL, '500.00', '2018-03-17 00:00:00', '39.00', '2018-03-17 16:42:04', 'Cash', '589.00', 'ORD1000000004', 0),
(21, 'RBI', '2018-03-31 00:00:00', '123456', '250.00', '2018-03-17 00:00:00', '250.00', '2018-03-17 16:42:22', 'Cheque', '589.00', 'ORD1000000004', 0),
(22, 'SBI', '2018-04-12 00:00:00', '123456', '0.00', '2018-04-05 12:40:23', '1100.00', '2018-03-23 15:48:28', 'Cheque', '1100.00', 'ORD1000000016', 0),
(23, NULL, NULL, NULL, '50.00', '2018-04-05 00:00:00', '250.00', '2018-04-04 17:04:16', 'Cash', '300.00', 'ORD1000000006', 0),
(24, NULL, NULL, NULL, '1000.00', '2018-04-06 00:00:00', '500.00', '2018-04-06 12:31:59', 'Cash', '3000.00', 'ORD1000000007', 0),
(25, 'AAA', '2018-04-06 00:00:00', '111111', '100.00', '2018-04-06 00:00:00', '150.00', '2018-04-06 12:33:59', 'Cheque', '589.00', 'ORD1000000004', 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment_counter`
--

CREATE TABLE `payment_counter` (
  `payment_counter_id` bigint(20) NOT NULL,
  `balance_amount` decimal(19,2) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `cheque_date` datetime DEFAULT NULL,
  `cheque_number` varchar(255) DEFAULT NULL,
  `current_amount_paid` decimal(19,2) DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `pay_type` varchar(255) DEFAULT NULL,
  `total_amount_paid` decimal(19,2) DEFAULT NULL,
  `counter_order_id` varchar(255) DEFAULT NULL,
  `current_amount_refund` decimal(19,2) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_counter`
--

INSERT INTO `payment_counter` (`payment_counter_id`, `balance_amount`, `bank_name`, `cheque_date`, `cheque_number`, `current_amount_paid`, `due_date`, `paid_date`, `pay_type`, `total_amount_paid`, `counter_order_id`, `current_amount_refund`, `status`) VALUES
(1, '2820.00', 'DDI', '2018-04-06 00:00:00', '456789', '2000.00', '2018-04-06 00:00:00', '2018-04-05 12:49:17', 'Cheque', '2000.00', 'CORD1001', '0.00', 0),
(2, '10.00', NULL, NULL, NULL, '230.00', '2018-04-05 00:00:00', '2018-04-05 12:50:41', 'Cash', '230.00', 'CORD1002', '0.00', 0),
(3, '0.00', NULL, NULL, NULL, '0.00', NULL, '2018-04-05 13:00:47', 'Cash', '1820.00', 'CORD1001', '180.00', 0),
(4, '0.00', NULL, NULL, NULL, '3904.00', NULL, '2018-04-06 13:04:39', 'Cash', '3904.00', 'CORD1003', '0.00', 0),
(5, '2400.00', 'EEE', '2018-04-06 00:00:00', '122222', '2000.00', '2018-04-06 00:00:00', '2018-04-06 13:06:00', 'Cheque', '2000.00', 'CORD1004', '0.00', 0),
(6, '3000.00', NULL, NULL, NULL, '3000.00', '2018-04-06 00:00:00', '2018-04-06 13:08:08', 'Cash', '3000.00', 'CORD1005', '0.00', 0),
(7, '1900.00', NULL, NULL, NULL, '2000.00', '2018-04-06 00:00:00', '2018-04-06 13:08:45', 'Cash', '2000.00', 'CORD1006', '0.00', 0),
(8, '0.00', NULL, NULL, NULL, '2400.00', NULL, '2018-04-07 11:41:56', 'Cash', '2400.00', 'CORD1007', '0.00', 0),
(9, '0.00', NULL, NULL, NULL, '3820.00', NULL, '2018-04-07 11:46:24', 'Cash', '3820.00', 'CORD1008', '0.00', 0),
(10, '0.00', NULL, NULL, NULL, '142.00', NULL, '2018-04-07 11:47:31', 'Cash', '142.00', 'CORD1009', '0.00', 0),
(11, '412.00', NULL, NULL, NULL, '824.00', NULL, '2018-04-07 11:48:36', 'Cash', '824.00', 'CORD1010', '0.00', 0),
(12, '0.00', NULL, NULL, NULL, '412.00', NULL, '2018-04-07 11:56:01', 'Cash', '1236.00', 'CORD1010', '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment_pay_supplier`
--

CREATE TABLE `payment_pay_supplier` (
  `payment_pay_supplier_id` bigint(20) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `cheque_date` datetime DEFAULT NULL,
  `cheque_number` varchar(255) DEFAULT NULL,
  `due_amount` decimal(19,2) DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `paid_amount` decimal(19,2) DEFAULT NULL,
  `paid_date` datetime DEFAULT NULL,
  `pay_type` varchar(255) DEFAULT NULL,
  `inventory_id` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_pay_supplier`
--

INSERT INTO `payment_pay_supplier` (`payment_pay_supplier_id`, `bank_name`, `cheque_date`, `cheque_number`, `due_amount`, `due_date`, `paid_amount`, `paid_date`, `pay_type`, `inventory_id`, `status`) VALUES
(28, NULL, NULL, NULL, '450.00', '2018-04-06 11:53:06', '300.00', '2018-04-06 11:53:06', 'Cash', 'TSN1005', 0),
(29, NULL, NULL, NULL, '750.00', '2018-04-06 11:52:44', '500.00', '2018-04-06 11:52:44', 'Cash', 'TSN1005', 0),
(30, NULL, NULL, NULL, '0.00', '2018-04-06 15:48:14', '450.00', '2018-04-06 15:48:14', 'Cash', 'TSN1005', 0),
(31, NULL, NULL, NULL, '1040.00', '2018-04-07 18:36:46', '100.00', '2018-04-07 18:36:46', 'Cash', 'TSN1003', 0),
(32, NULL, NULL, NULL, '1000.00', '2018-04-07 19:13:26', '40.00', '2018-04-07 19:13:26', 'Cash', 'TSN1003', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` bigint(20) NOT NULL,
  `current_quantity` bigint(20) DEFAULT NULL,
  `damage_quantity` bigint(20) DEFAULT NULL,
  `free_quantity` bigint(20) DEFAULT NULL,
  `product_added_datetime` datetime DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `product_content_type` varchar(255) DEFAULT NULL,
  `product_description` varchar(255) DEFAULT NULL,
  `product_image` longblob,
  `product_name` varchar(255) DEFAULT NULL,
  `product_quantity_updated_datetime` datetime DEFAULT NULL,
  `rate` decimal(19,2) DEFAULT NULL,
  `threshold` bigint(20) DEFAULT NULL,
  `brand_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `current_quantity`, `damage_quantity`, `free_quantity`, `product_added_datetime`, `product_code`, `product_content_type`, `product_description`, `product_image`, `product_name`, `product_quantity_updated_datetime`, `rate`, `threshold`, `brand_id`, `category_id`, `company_id`) VALUES
(1, 43, 21, 0, NULL, 'K21', NULL, NULL, NULL, 'KeyBoard 21', NULL, '178.57', 0, 1, 1, 2),
(2, 50, 8, 0, NULL, 'K22', NULL, NULL, NULL, 'KeyBoard 22', NULL, '267.86', 0, 1, 1, 2),
(3, 50, 0, 0, NULL, 'K23', NULL, NULL, NULL, 'KeyBoard 23', NULL, '133.93', 0, 1, 1, 2),
(4, 17, 0, 0, '2018-03-06 12:58:19', 'M21', 'image/png', NULL, NULL, 'Mouse 21', '2018-03-07 12:34:58', '16.95', 10, 1, 2, 2),
(5, 48, 2, 0, NULL, 'M22', NULL, NULL, NULL, 'Mouse 22', NULL, '186.44', 0, 1, 2, 2),
(28, 17, 1, 3, NULL, '', NULL, NULL, NULL, 'Blue Keyboard USB', NULL, '178.57', 0, 2, 11, 4),
(29, 20, 0, 11, NULL, '', NULL, NULL, NULL, 'Blue Keyboard PS/2', NULL, '196.43', 0, 2, 11, 4),
(30, 31, 0, 0, NULL, NULL, NULL, NULL, NULL, 'Blue Wireless Combo Keyboard + mouse', NULL, '647.32', 0, 2, 12, 4),
(31, 26, 7, 24, NULL, NULL, NULL, NULL, NULL, 'Blue Wireless Mouse 2.4GHZ', NULL, '254.24', 0, 2, 12, 4),
(32, 24, 0, 0, NULL, '', NULL, NULL, NULL, 'Blue USB Mouse B -201', NULL, '165.25', 0, 2, 12, 4),
(33, 50, 0, 0, NULL, NULL, NULL, NULL, NULL, 'Blue USB Mouse B -202', NULL, '114.41', 0, 2, 12, 4),
(34, 50, 43, 25, '2017-12-13 11:17:05', '', NULL, NULL, NULL, 'Blue USB Mouse B -203', '2017-12-21 18:07:56', '135.59', 10, 2, 12, 4),
(35, 50, 6, 0, NULL, '', NULL, NULL, NULL, 'Blue USB Mouse m 10', NULL, '150.00', 0, 2, 12, 4),
(36, 37, 0, 0, NULL, '', NULL, NULL, NULL, 'Blue  USB Mouse Mini BS 220', NULL, '100.00', 0, 2, 12, 4),
(37, 50, 0, 0, NULL, NULL, NULL, NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 2 Mtr', NULL, '315.02', 0, 2, 14, 4),
(38, 50, 10, 7, '2017-12-13 11:19:59', '', NULL, NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 5 Mtr', '2017-12-21 18:44:11', '311.61', 10, 2, 14, 4),
(39, 31, 0, 5, '2017-12-13 11:20:32', '', NULL, NULL, NULL, 'Blue Spike Guard 114 ( 4x4 ) 2 Mtr 2 USB', '2017-12-13 11:20:32', '367.70', 10, 2, 14, 4),
(40, 50, 1, 0, '2017-12-13 11:21:17', '', NULL, NULL, NULL, 'Blue Spike Guard 116 ( 6X6 ) 2 Mtr', '2017-12-13 11:21:17', '367.70', 10, 2, 14, 4),
(41, 50, 0, 12, '2017-12-13 11:22:36', '', NULL, NULL, NULL, 'Blue Spike Guard 116 ( 6x6 ) 5 Mtr', '2017-12-13 11:22:36', '420.37', 10, 2, 14, 4),
(42, 43, 0, 4, NULL, NULL, NULL, NULL, NULL, 'Blue Power Code 1.5  Mtr', NULL, '50.00', 0, 2, 13, 4),
(43, 50, 31, 0, '2017-12-13 11:25:56', '75', NULL, NULL, NULL, 'Blue Power Code 3  Mtr', '2017-12-13 11:25:56', '75.00', 10, 2, 13, 4),
(44, 9, 10, 0, NULL, '', NULL, NULL, NULL, 'Blue Power Code 5 Mtr', NULL, '120.00', 0, 2, 13, 4),
(45, 50, 36, 0, NULL, '', NULL, NULL, NULL, 'Blue Power Code 10 Mtr', NULL, '250.00', 0, 2, 13, 4),
(46, 46, 0, 3, NULL, NULL, NULL, NULL, NULL, 'Blue Power Code Laptop 1.5 Mtr', NULL, '60.00', 0, 2, 13, 4),
(47, 50, 0, 0, '2017-12-13 11:29:41', '', NULL, NULL, NULL, 'Blue USB TO USB Cable 2.0', '2017-12-13 11:29:41', '75.00', 10, 2, 13, 4),
(48, 50, 0, 0, NULL, '', NULL, NULL, NULL, 'Blue USB TO USB Cable 3.0', NULL, '175.00', 0, 2, 13, 4),
(49, 44, 3, 0, NULL, NULL, NULL, NULL, NULL, 'Blue USB Printer Cable 1.5 Mtr', NULL, '50.00', 0, 2, 13, 4),
(50, 50, 0, 0, NULL, NULL, NULL, NULL, NULL, 'Blue USB Printer Cable 3 Mtr', NULL, '75.00', 0, 2, 13, NULL),
(51, 47, 2, 0, '2017-12-13 12:21:37', '12121', NULL, NULL, NULL, 'MouseTest', '2017-12-13 12:21:37', '10.00', 1, 2, 12, 4),
(52, 50, 1, 0, NULL, '8471', NULL, NULL, NULL, 'Blue keyboard USB MINI', NULL, '236.61', 0, 2, 11, 4),
(53, 60, 2, 0, NULL, NULL, NULL, NULL, NULL, 'Blue LAN Cable 1.5 Mtr  Cat 5', NULL, '33.90', 0, 2, 13, 4);

-- --------------------------------------------------------

--
-- Table structure for table `region`
--

CREATE TABLE `region` (
  `region_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `city_id` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region`
--

INSERT INTO `region` (`region_id`, `name`, `city_id`, `company_id`) VALUES
(1, 'Western', 1, 2),
(2, 'Harber', 2, 2),
(3, 'Kothrud-Aundh', 2, 4),
(4, 'Balewadi-Swargate', 2, 4),
(5, 'Central', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `reissue_order_details`
--

CREATE TABLE `reissue_order_details` (
  `reissue_order_id` bigint(20) NOT NULL,
  `reissue_date` datetime DEFAULT NULL,
  `reissue_delivered_date` datetime DEFAULT NULL,
  `reissue_delivery_date` datetime DEFAULT NULL,
  `order_reissue_status` varchar(255) DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `total_quantity` bigint(20) DEFAULT NULL,
  `reissued_by_gk` bigint(20) DEFAULT NULL,
  `reissued_to_db` bigint(20) DEFAULT NULL,
  `return_order_product_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reissue_order_details`
--

INSERT INTO `reissue_order_details` (`reissue_order_id`, `reissue_date`, `reissue_delivered_date`, `reissue_delivery_date`, `order_reissue_status`, `total_amount`, `total_amount_with_tax`, `total_quantity`, `reissued_by_gk`, `reissued_to_db`, `return_order_product_id`) VALUES
(1, '2018-03-23 12:36:34', NULL, '2018-03-23 00:00:00', 'Pending', '372.88', '440.00', 2, 11, 10, 'RTN1001'),
(2, '2018-03-24 18:53:06', NULL, '2018-03-24 00:00:00', 'Pending', '2321.45', '2600.00', 9, 11, 9, 'RTN1003');

-- --------------------------------------------------------

--
-- Table structure for table `reissue_order_product_details`
--

CREATE TABLE `reissue_order_product_details` (
  `reissue_id` bigint(20) NOT NULL,
  `details` varchar(255) DEFAULT NULL,
  `issued_quantity` bigint(20) DEFAULT NULL,
  `reissue_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `reissue_quantity` bigint(20) DEFAULT NULL,
  `return_quantity` bigint(20) DEFAULT NULL,
  `selling_rate` decimal(19,2) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `reIssue_order_details_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reissue_order_product_details`
--

INSERT INTO `reissue_order_product_details` (`reissue_id`, `details`, `issued_quantity`, `reissue_amount_with_tax`, `reissue_quantity`, `return_quantity`, `selling_rate`, `product_id`, `reIssue_order_details_id`) VALUES
(1, NULL, 5, '440.00', 2, 3, '220.00', 206, 1),
(2, NULL, 0, '0.00', 0, 0, '200.00', 205, 1),
(3, NULL, 5, '200.00', 1, 2, '200.00', 210, 2),
(4, NULL, 10, '2400.00', 8, 9, '300.00', 211, 2);

-- --------------------------------------------------------

--
-- Table structure for table `return_from_delivery_boy`
--

CREATE TABLE `return_from_delivery_boy` (
  `id` bigint(20) NOT NULL,
  `damage_quantity` bigint(20) DEFAULT NULL,
  `delivery_quantity` bigint(20) DEFAULT NULL,
  `issued_quantity` bigint(20) DEFAULT NULL,
  `non_damage_quantity` bigint(20) DEFAULT NULL,
  `return_quantity` bigint(20) DEFAULT NULL,
  `selling_rate` decimal(19,2) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `return_from_deliveryboy_main_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `return_from_delivery_boy`
--

INSERT INTO `return_from_delivery_boy` (`id`, `damage_quantity`, `delivery_quantity`, `issued_quantity`, `non_damage_quantity`, `return_quantity`, `selling_rate`, `type`, `product_id`, `return_from_deliveryboy_main_id`) VALUES
(1, 2, 5, 9, 2, 4, '12.00', 'NonFree', 36, 1),
(2, 0, 3, 6, 3, 3, '0.00', 'Free', 37, 1),
(3, 1, 2, 5, 2, 3, '265.00', 'NonFree', 38, 1),
(4, 2, 2, 4, 0, 2, '40.00', 'NonFree', 44, 2),
(5, 0, 2, 5, 3, 3, '12.00', 'NonFree', 45, 2),
(6, 0, 0, 15, 0, 15, '142.00', 'NonFree', 20, 3),
(7, 0, 0, 7, 0, 7, '220.00', 'NonFree', 21, 3),
(8, 0, 0, 8, 0, 8, '200.00', 'NonFree', 22, 3),
(9, 0, 0, 14, 0, 14, '88.00', 'NonFree', 23, 3),
(10, 0, 0, 12, 0, 12, '60.00', 'NonFree', 24, 3),
(11, 0, 0, 18, 0, 18, '265.00', 'NonFree', 25, 3),
(12, 0, 0, 3, 0, 3, '210.00', 'NonFree', 66, 4),
(13, 0, 0, 12, 0, 12, '150.00', 'NonFree', 67, 4),
(14, 0, 0, 2, 0, 2, '0.00', 'Free', 68, 4),
(15, 0, 0, 11, 0, 11, '195.00', 'NonFree', 74, 5),
(16, 0, 0, 12, 0, 12, '200.00', 'NonFree', 75, 5),
(17, 0, 0, 4, 0, 4, '20.00', 'NonFree', 212, 6),
(18, 0, 0, 1, 0, 1, '200.00', 'NonFree', 213, 6);

-- --------------------------------------------------------

--
-- Table structure for table `return_from_delivery_boy_main`
--

CREATE TABLE `return_from_delivery_boy_main` (
  `id` bigint(20) NOT NULL,
  `date` datetime DEFAULT NULL,
  `received_status` tinyint(1) DEFAULT NULL,
  `total_damage_quantity` bigint(20) DEFAULT NULL,
  `total_delivery_quantity` bigint(20) DEFAULT NULL,
  `total_issued_quantity` bigint(20) DEFAULT NULL,
  `total_non_damage_quantity` bigint(20) DEFAULT NULL,
  `total_return_quantity` bigint(20) DEFAULT NULL,
  `deliveryboy_id` bigint(20) DEFAULT NULL,
  `gatekeeper_id` bigint(20) DEFAULT NULL,
  `order_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `return_from_delivery_boy_main`
--

INSERT INTO `return_from_delivery_boy_main` (`id`, `date`, `received_status`, `total_damage_quantity`, `total_delivery_quantity`, `total_issued_quantity`, `total_non_damage_quantity`, `total_return_quantity`, `deliveryboy_id`, `gatekeeper_id`, `order_id`) VALUES
(1, '2018-03-13 11:49:22', 1, 5, 24, 47, 18, 23, 14, 13, 'ORD1000000003'),
(2, '2018-03-13 11:55:15', 1, 2, 4, 9, 3, 5, 14, 13, 'ORD1000000003'),
(3, '2018-03-13 12:44:52', 0, 0, 0, 74, 0, 74, 14, 13, 'ORD1000000002'),
(4, '2018-03-13 17:24:30', 0, 0, 0, 17, 0, 17, 10, 11, 'ORD1000000005'),
(5, '2018-03-13 19:17:25', 0, 0, 0, 23, 0, 23, 14, 13, 'ORD1000000009'),
(6, '2018-03-23 14:48:16', 0, 0, 0, 5, 0, 5, NULL, NULL, 'ORD1000000019');

-- --------------------------------------------------------

--
-- Table structure for table `return_order_product`
--

CREATE TABLE `return_order_product` (
  `return_order_product_id` varchar(255) NOT NULL,
  `reissue_status` varchar(255) DEFAULT NULL,
  `return_order_product_datetime` datetime DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `total_quantity` bigint(20) DEFAULT NULL,
  `order_details_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `return_order_product`
--

INSERT INTO `return_order_product` (`return_order_product_id`, `reissue_status`, `return_order_product_datetime`, `total_amount`, `total_amount_with_tax`, `total_quantity`, `order_details_id`) VALUES
('RTN1001', 'Completed', '2018-03-23 12:11:13', '559.32', '660.00', 3, 'ORD1000000016'),
('RTN1002', 'Pending', '2018-03-23 16:19:44', '748.19', '840.00', 5, 'ORD1000000017'),
('RTN1003', 'Completed', '2018-03-24 18:52:18', '2767.88', '3100.00', 11, 'ORD1000000018');

-- --------------------------------------------------------

--
-- Table structure for table `return_order_product_details`
--

CREATE TABLE `return_order_product_details` (
  `id` bigint(20) NOT NULL,
  `issued_quantity` bigint(20) DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `return_quantity` bigint(20) DEFAULT NULL,
  `return_total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `selling_rate` decimal(19,2) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `return_order_product_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `return_order_product_details`
--

INSERT INTO `return_order_product_details` (`id`, `issued_quantity`, `reason`, `return_quantity`, `return_total_amount_with_tax`, `selling_rate`, `product_id`, `return_order_product_id`) VALUES
(1, 5, 'demage quantity', 3, '660.00', '220.00', 206, 'RTN1001'),
(2, 2, 'rt', 1, '200.00', '200.00', 208, 'RTN1002'),
(3, 15, 'abc', 2, '600.00', '300.00', 209, 'RTN1002'),
(4, 3, 'tt', 2, '40.00', '20.00', 207, 'RTN1002'),
(5, 5, 'faga', 2, '400.00', '200.00', 210, 'RTN1003'),
(6, 10, 'aga', 9, '2700.00', '300.00', 211, 'RTN1003');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` bigint(20) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `code`, `name`, `country_id`) VALUES
(1, '025', 'Maharashtra', 1),
(2, '026', 'Karnatak', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `gstin_no` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `supplier_added_datetime` datetime DEFAULT NULL,
  `supplier_updated_datetime` datetime DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL,
  `contact_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `address`, `gstin_no`, `name`, `supplier_added_datetime`, `supplier_updated_datetime`, `company_id`, `contact_id`) VALUES
('SUP1000000001', 'sda', 'aaaaaaaaaaaaaaa', 'Supplier 1', '2018-03-05 17:04:35', '2018-03-08 14:41:23', 2, 7),
('SUP1000000002', 'nnnnn', 'sdddddddddddddd', 'Supplier1', '2018-03-08 13:37:01', '2018-03-13 12:05:56', 4, 15);

-- --------------------------------------------------------

--
-- Table structure for table `supplier_order`
--

CREATE TABLE `supplier_order` (
  `supplier_order_id` varchar(255) NOT NULL,
  `supplier_order_added_datetime` datetime DEFAULT NULL,
  `supplier_order_update_datetime` datetime DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `total_quantity` bigint(20) DEFAULT NULL,
  `company_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_order_details`
--

CREATE TABLE `supplier_order_details` (
  `supplier_order_details_id` bigint(20) NOT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `supplier_rate` decimal(19,2) DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `total_amount_with_tax` decimal(19,2) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL,
  `supplier_order_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier_product_list`
--

CREATE TABLE `supplier_product_list` (
  `supplier_product_list_id` bigint(20) NOT NULL,
  `supplier_rate` decimal(19,2) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier_product_list`
--

INSERT INTO `supplier_product_list` (`supplier_product_list_id`, `supplier_rate`, `product_id`, `supplier_id`) VALUES
(12, '18.75', 1, 'SUP1000000001'),
(13, '23.21', 3, 'SUP1000000001'),
(14, '100.00', 2, 'SUP1000000001'),
(15, '50.00', 4, 'SUP1000000001'),
(16, '16.10', 5, 'SUP1000000001'),
(17, '89.29', 28, 'SUP1000000002'),
(18, '67.80', 30, 'SUP1000000002'),
(19, '13.39', 41, 'SUP1000000002'),
(20, '89.29', 52, 'SUP1000000002'),
(21, '105.93', 53, 'SUP1000000002'),
(22, '31.36', 51, 'SUP1000000002');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address`
--
ALTER TABLE `address`
  ADD PRIMARY KEY (`address_id`),
  ADD KEY `FKBB979BF4C8668BF4` (`area_id`);

--
-- Indexes for table `app_version`
--
ALTER TABLE `app_version`
  ADD PRIMARY KEY (`app_version_id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`area_id`),
  ADD KEY `FK2DD08DC2A70D00` (`company_id`),
  ADD KEY `FK2DD08D6FB2B014` (`region_id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`brand_id`),
  ADD KEY `FK59A4B87C2A70D00` (`company_id`);

--
-- Indexes for table `business_name`
--
ALTER TABLE `business_name`
  ADD PRIMARY KEY (`business_name_id`),
  ADD KEY `FK258B4BAAC2A70D00` (`company_id`),
  ADD KEY `FK258B4BAAC8668BF4` (`area_id`),
  ADD KEY `FK258B4BAAFD93C620` (`contact_id`),
  ADD KEY `FK258B4BAAE47AD2F4` (`businesstype_id`);

--
-- Indexes for table `business_type`
--
ALTER TABLE `business_type`
  ADD PRIMARY KEY (`businesstype_id`),
  ADD KEY `FK258E6059C2A70D00` (`company_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `FK4D47461CC2A70D00` (`company_id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`chat_id`),
  ADD KEY `FK2E935851D366C5` (`employee_id_from`),
  ADD KEY `FK2E935876D34656` (`employee_id_to`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `FK2E996BF7460600` (`state_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`company_id`),
  ADD KEY `FK38A73C7DFD93C620` (`contact_id`);

--
-- Indexes for table `company_cities`
--
ALTER TABLE `company_cities`
  ADD PRIMARY KEY (`company_city_id`),
  ADD KEY `FKDD4DE54BC2A70D00` (`company_id`),
  ADD KEY `FKDD4DE54B23B67734` (`city_id`);

--
-- Indexes for table `complainreply`
--
ALTER TABLE `complainreply`
  ADD PRIMARY KEY (`complain_Reply_Id`),
  ADD KEY `FK35FE1BA1ABDA1809` (`employee_reply_id`),
  ADD KEY `FK35FE1BA17FEC9728` (`employee_complain_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `counter_order`
--
ALTER TABLE `counter_order`
  ADD PRIMARY KEY (`counter_order_id`),
  ADD KEY `FKF893D34B76BA448D` (`employee_gk_id`),
  ADD KEY `FKF893D34BD3206FBF` (`business_id`),
  ADD KEY `FKF893D34B9FC8331D` (`order_status_id`);

--
-- Indexes for table `counter_order_product_details`
--
ALTER TABLE `counter_order_product_details`
  ADD PRIMARY KEY (`counter_order_product_id`),
  ADD KEY `FKB9C139FE88997C9B` (`counter_order_id`),
  ADD KEY `FKB9C139FE83D7F309` (`product_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `daily_stock_details`
--
ALTER TABLE `daily_stock_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKE5FBAA731290DFC0` (`product_id`),
  ADD KEY `FKE5FBAA73C2A70D00` (`company_id`);

--
-- Indexes for table `damage_recovery_details`
--
ALTER TABLE `damage_recovery_details`
  ADD PRIMARY KEY (`damage_recovery_details_id`),
  ADD KEY `FKC4FF668AA70D9FF` (`damage_recovery_day_wise_id`),
  ADD KEY `FKC4FF66853F25294` (`supplier_id`);

--
-- Indexes for table `damage_recovery_month_wise`
--
ALTER TABLE `damage_recovery_month_wise`
  ADD PRIMARY KEY (`damage_recovery_id`),
  ADD KEY `FKA9FB991D1290DFC0` (`product_id`);

--
-- Indexes for table `delivered_product`
--
ALTER TABLE `delivered_product`
  ADD PRIMARY KEY (`delivered_product_id`),
  ADD KEY `FK1A951034DAA6E837` (`order_details_id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`),
  ADD KEY `FK4722E6AE7F4FA47D` (`department_id`),
  ADD KEY `FK4722E6AEC2A70D00` (`company_id`);

--
-- Indexes for table `employee_areas`
--
ALTER TABLE `employee_areas`
  ADD PRIMARY KEY (`employee_areas_details_id`),
  ADD KEY `FK5B0350B5C8668BF4` (`area_id`),
  ADD KEY `FK5B0350B5C9263C83` (`employee_details_id`);

--
-- Indexes for table `employee_basic_salary_status`
--
ALTER TABLE `employee_basic_salary_status`
  ADD PRIMARY KEY (`employee_basic_salary_status_id`),
  ADD KEY `FKD8245AA5C9263C83` (`employee_details_id`);

--
-- Indexes for table `employee_chat_status`
--
ALTER TABLE `employee_chat_status`
  ADD PRIMARY KEY (`employee_chat_status_id`),
  ADD KEY `FKD26FF6A851E18F54` (`employee_id`);

--
-- Indexes for table `employee_details`
--
ALTER TABLE `employee_details`
  ADD PRIMARY KEY (`employee_details_id`),
  ADD UNIQUE KEY `employee_details_gen_id` (`employee_details_gen_id`),
  ADD KEY `FK30C8F1B151E18F54` (`employee_id`),
  ADD KEY `FK30C8F1B1FD93C620` (`contact_id`);

--
-- Indexes for table `employee_feedback`
--
ALTER TABLE `employee_feedback`
  ADD PRIMARY KEY (`feedBack_id`),
  ADD KEY `FK9EA5D9F654050B80` (`employeeDetails_id`);

--
-- Indexes for table `employee_feedback_reply`
--
ALTER TABLE `employee_feedback_reply`
  ADD PRIMARY KEY (`feedback_reply_id`),
  ADD KEY `FKF4EE7F61E96C02E2` (`feedback_id`);

--
-- Indexes for table `employee_holiday`
--
ALTER TABLE `employee_holiday`
  ADD PRIMARY KEY (`employee_holiday_id`),
  ADD KEY `FK150527C7C9263C83` (`employee_details_id`);

--
-- Indexes for table `employee_incentives`
--
ALTER TABLE `employee_incentives`
  ADD PRIMARY KEY (`employee_incentives_id`),
  ADD KEY `FK1A882699C9263C83` (`employee_details_id`);

--
-- Indexes for table `employee_location`
--
ALTER TABLE `employee_location`
  ADD PRIMARY KEY (`employee_location_id`),
  ADD KEY `FK1B5F8E26C9263C83` (`employee_details_id`);

--
-- Indexes for table `employee_salary`
--
ALTER TABLE `employee_salary`
  ADD PRIMARY KEY (`employee_salary_id`),
  ADD KEY `FK2331A07B54050B80` (`employeeDetails_id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`expense_id`),
  ADD KEY `FKB1F4C858C2A70D00` (`company_id`),
  ADD KEY `FKB1F4C85851E18F54` (`employee_id`),
  ADD KEY `FKB1F4C858EAF9FC71` (`expense_type_id`);

--
-- Indexes for table `expense_type`
--
ALTER TABLE `expense_type`
  ADD PRIMARY KEY (`expense_type_id`),
  ADD KEY `FK4B63F6C1C2A70D00` (`company_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`inventory_transaction_id`),
  ADD KEY `FK8790195CC2A70D00` (`company_id`),
  ADD KEY `FK8790195C51E18F54` (`employee_id`),
  ADD KEY `FK8790195C53F25294` (`supplier_id`);

--
-- Indexes for table `inventory_details`
--
ALTER TABLE `inventory_details`
  ADD PRIMARY KEY (`inventory_details_id`),
  ADD KEY `FKD867965FD59E6B20` (`inventory_id`),
  ADD KEY `FKD867965F83D7F309` (`product_id`);

--
-- Indexes for table `ledger`
--
ALTER TABLE `ledger`
  ADD PRIMARY KEY (`ledger_id`),
  ADD KEY `FKBE09AD69C2A70D00` (`company_id`),
  ADD KEY `FKBE09AD69A06CE43D` (`payment_supplier_id`),
  ADD KEY `FKBE09AD69CBBAB651` (`employee_salary_id`),
  ADD KEY `FKBE09AD69631ABB20` (`expense_id`),
  ADD KEY `FKBE09AD699CCE6031` (`payment_order_id`),
  ADD KEY `FKBE09AD6980287007` (`payment_counter_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_id`),
  ADD UNIQUE KEY `invoice_number` (`invoice_number`),
  ADD KEY `FK521CF25176D34635` (`employee_id_sm`),
  ADD KEY `FK521CF2516744C4D5` (`employee_id_cancel`),
  ADD KEY `FK521CF2519FC8331D` (`order_status_id`),
  ADD KEY `FK521CF25197146395` (`business_name_id`);

--
-- Indexes for table `order_product_details`
--
ALTER TABLE `order_product_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKF948801DAA6E837` (`order_details_id`),
  ADD KEY `FKF94880183D7F309` (`product_id`);

--
-- Indexes for table `order_product_issue_details`
--
ALTER TABLE `order_product_issue_details`
  ADD PRIMARY KEY (`order_issue_id`),
  ADD KEY `FK2BFE635BF3C30640` (`issued_by_gk`),
  ADD KEY `FK2BFE635BF4BC2136` (`issued_to_db`),
  ADD KEY `FK2BFE635B8DACD61A` (`order_id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`order_status_id`);

--
-- Indexes for table `order_used_brand`
--
ALTER TABLE `order_used_brand`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `order_used_categories`
--
ALTER TABLE `order_used_categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `order_used_product`
--
ALTER TABLE `order_used_product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `FKFB9403BEB5B04409` (`category_id`),
  ADD KEY `FKFB9403BE86729CFC` (`product_entity_id`),
  ADD KEY `FKFB9403BE17956BC9` (`brand_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`payment_id`),
  ADD KEY `FKD11C32068DACD61A` (`order_id`);

--
-- Indexes for table `payment_counter`
--
ALTER TABLE `payment_counter`
  ADD PRIMARY KEY (`payment_counter_id`),
  ADD KEY `FK920BC8C388997C9B` (`counter_order_id`);

--
-- Indexes for table `payment_pay_supplier`
--
ALTER TABLE `payment_pay_supplier`
  ADD PRIMARY KEY (`payment_pay_supplier_id`),
  ADD KEY `FKDCEEE91CD59E6B20` (`inventory_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `FKED8DCCEF779307F2` (`category_id`),
  ADD KEY `FKED8DCCEFC2A70D00` (`company_id`),
  ADD KEY `FKED8DCCEFA6401A40` (`brand_id`);

--
-- Indexes for table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`region_id`),
  ADD KEY `FKC84826F4C2A70D00` (`company_id`),
  ADD KEY `FKC84826F423B67734` (`city_id`);

--
-- Indexes for table `reissue_order_details`
--
ALTER TABLE `reissue_order_details`
  ADD PRIMARY KEY (`reissue_order_id`),
  ADD KEY `FK92566458A2385FF3` (`reissued_by_gk`),
  ADD KEY `FK92566458A3317AE9` (`reissued_to_db`),
  ADD KEY `FK92566458234D5616` (`return_order_product_id`);

--
-- Indexes for table `reissue_order_product_details`
--
ALTER TABLE `reissue_order_product_details`
  ADD PRIMARY KEY (`reissue_id`),
  ADD KEY `FK8E1B030883D7F309` (`product_id`),
  ADD KEY `FK8E1B03083ECCBA62` (`reIssue_order_details_id`);

--
-- Indexes for table `return_from_delivery_boy`
--
ALTER TABLE `return_from_delivery_boy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKFAD802783D7F309` (`product_id`),
  ADD KEY `FKFAD8027C5105B31` (`return_from_deliveryboy_main_id`);

--
-- Indexes for table `return_from_delivery_boy_main`
--
ALTER TABLE `return_from_delivery_boy_main`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK68BD3ED1C9FA906A` (`deliveryboy_id`),
  ADD KEY `FK68BD3ED16D315C25` (`gatekeeper_id`),
  ADD KEY `FK68BD3ED18DACD61A` (`order_id`);

--
-- Indexes for table `return_order_product`
--
ALTER TABLE `return_order_product`
  ADD PRIMARY KEY (`return_order_product_id`),
  ADD KEY `FKFD63082FDAA6E837` (`order_details_id`);

--
-- Indexes for table `return_order_product_details`
--
ALTER TABLE `return_order_product_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK2FD4023283D7F309` (`product_id`),
  ADD KEY `FK2FD40232234D5616` (`return_order_product_id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`),
  ADD KEY `FK68AC491B8F88A60` (`country_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`),
  ADD KEY `FK9CDBF9CCC2A70D00` (`company_id`),
  ADD KEY `FK9CDBF9CCFD93C620` (`contact_id`);

--
-- Indexes for table `supplier_order`
--
ALTER TABLE `supplier_order`
  ADD PRIMARY KEY (`supplier_order_id`),
  ADD KEY `FKC225B1BC2A70D00` (`company_id`);

--
-- Indexes for table `supplier_order_details`
--
ALTER TABLE `supplier_order_details`
  ADD PRIMARY KEY (`supplier_order_details_id`),
  ADD KEY `FKAA37A91E21D83D87` (`supplier_order_id`),
  ADD KEY `FKAA37A91E83D7F309` (`product_id`),
  ADD KEY `FKAA37A91E53F25294` (`supplier_id`);

--
-- Indexes for table `supplier_product_list`
--
ALTER TABLE `supplier_product_list`
  ADD PRIMARY KEY (`supplier_product_list_id`),
  ADD KEY `FKD3F215811290DFC0` (`product_id`),
  ADD KEY `FKD3F2158153F25294` (`supplier_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `address`
--
ALTER TABLE `address`
  MODIFY `address_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_version`
--
ALTER TABLE `app_version`
  MODIFY `app_version_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `area_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `brand_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `business_type`
--
ALTER TABLE `business_type`
  MODIFY `businesstype_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `chat_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `company_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `company_cities`
--
ALTER TABLE `company_cities`
  MODIFY `company_city_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `complainreply`
--
ALTER TABLE `complainreply`
  MODIFY `complain_Reply_Id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `counter_order_product_details`
--
ALTER TABLE `counter_order_product_details`
  MODIFY `counter_order_product_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `country_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `daily_stock_details`
--
ALTER TABLE `daily_stock_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `damage_recovery_month_wise`
--
ALTER TABLE `damage_recovery_month_wise`
  MODIFY `damage_recovery_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `delivered_product`
--
ALTER TABLE `delivered_product`
  MODIFY `delivered_product_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `department_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employee_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `employee_areas`
--
ALTER TABLE `employee_areas`
  MODIFY `employee_areas_details_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `employee_basic_salary_status`
--
ALTER TABLE `employee_basic_salary_status`
  MODIFY `employee_basic_salary_status_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `employee_chat_status`
--
ALTER TABLE `employee_chat_status`
  MODIFY `employee_chat_status_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_details`
--
ALTER TABLE `employee_details`
  MODIFY `employee_details_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `employee_feedback`
--
ALTER TABLE `employee_feedback`
  MODIFY `feedBack_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_feedback_reply`
--
ALTER TABLE `employee_feedback_reply`
  MODIFY `feedback_reply_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_holiday`
--
ALTER TABLE `employee_holiday`
  MODIFY `employee_holiday_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `employee_incentives`
--
ALTER TABLE `employee_incentives`
  MODIFY `employee_incentives_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `employee_location`
--
ALTER TABLE `employee_location`
  MODIFY `employee_location_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=791;
--
-- AUTO_INCREMENT for table `employee_salary`
--
ALTER TABLE `employee_salary`
  MODIFY `employee_salary_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `expense_type`
--
ALTER TABLE `expense_type`
  MODIFY `expense_type_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `inventory_details`
--
ALTER TABLE `inventory_details`
  MODIFY `inventory_details_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `ledger`
--
ALTER TABLE `ledger`
  MODIFY `ledger_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `order_product_details`
--
ALTER TABLE `order_product_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;
--
-- AUTO_INCREMENT for table `order_product_issue_details`
--
ALTER TABLE `order_product_issue_details`
  MODIFY `order_issue_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `order_status_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `order_used_brand`
--
ALTER TABLE `order_used_brand`
  MODIFY `brand_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=379;
--
-- AUTO_INCREMENT for table `order_used_categories`
--
ALTER TABLE `order_used_categories`
  MODIFY `category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=379;
--
-- AUTO_INCREMENT for table `order_used_product`
--
ALTER TABLE `order_used_product`
  MODIFY `product_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=379;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `payment_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `payment_counter`
--
ALTER TABLE `payment_counter`
  MODIFY `payment_counter_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `payment_pay_supplier`
--
ALTER TABLE `payment_pay_supplier`
  MODIFY `payment_pay_supplier_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `region`
--
ALTER TABLE `region`
  MODIFY `region_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `reissue_order_details`
--
ALTER TABLE `reissue_order_details`
  MODIFY `reissue_order_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reissue_order_product_details`
--
ALTER TABLE `reissue_order_product_details`
  MODIFY `reissue_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `return_from_delivery_boy`
--
ALTER TABLE `return_from_delivery_boy`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `return_from_delivery_boy_main`
--
ALTER TABLE `return_from_delivery_boy_main`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `return_order_product_details`
--
ALTER TABLE `return_order_product_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `supplier_order_details`
--
ALTER TABLE `supplier_order_details`
  MODIFY `supplier_order_details_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `supplier_product_list`
--
ALTER TABLE `supplier_product_list`
  MODIFY `supplier_product_list_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `address`
--
ALTER TABLE `address`
  ADD CONSTRAINT `FKBB979BF4C8668BF4` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`);

--
-- Constraints for table `area`
--
ALTER TABLE `area`
  ADD CONSTRAINT `FK2DD08D6FB2B014` FOREIGN KEY (`region_id`) REFERENCES `region` (`region_id`),
  ADD CONSTRAINT `FK2DD08DC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `brand`
--
ALTER TABLE `brand`
  ADD CONSTRAINT `FK59A4B87C2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `business_name`
--
ALTER TABLE `business_name`
  ADD CONSTRAINT `FK258B4BAAC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `FK258B4BAAC8668BF4` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`),
  ADD CONSTRAINT `FK258B4BAAE47AD2F4` FOREIGN KEY (`businesstype_id`) REFERENCES `business_type` (`businesstype_id`),
  ADD CONSTRAINT `FK258B4BAAFD93C620` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`contact_id`);

--
-- Constraints for table `business_type`
--
ALTER TABLE `business_type`
  ADD CONSTRAINT `FK258E6059C2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `FK4D47461CC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `chat`
--
ALTER TABLE `chat`
  ADD CONSTRAINT `FK2E935851D366C5` FOREIGN KEY (`employee_id_from`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK2E935876D34656` FOREIGN KEY (`employee_id_to`) REFERENCES `employee` (`employee_id`);

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `FK2E996BF7460600` FOREIGN KEY (`state_id`) REFERENCES `state` (`state_id`);

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `FK38A73C7DFD93C620` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`contact_id`);

--
-- Constraints for table `company_cities`
--
ALTER TABLE `company_cities`
  ADD CONSTRAINT `FKDD4DE54B23B67734` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `FKDD4DE54BC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `complainreply`
--
ALTER TABLE `complainreply`
  ADD CONSTRAINT `FK35FE1BA17FEC9728` FOREIGN KEY (`employee_complain_id`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK35FE1BA1ABDA1809` FOREIGN KEY (`employee_reply_id`) REFERENCES `employee` (`employee_id`);

--
-- Constraints for table `counter_order`
--
ALTER TABLE `counter_order`
  ADD CONSTRAINT `FKF893D34B76BA448D` FOREIGN KEY (`employee_gk_id`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FKF893D34B9FC8331D` FOREIGN KEY (`order_status_id`) REFERENCES `order_status` (`order_status_id`),
  ADD CONSTRAINT `FKF893D34BD3206FBF` FOREIGN KEY (`business_id`) REFERENCES `business_name` (`business_name_id`);

--
-- Constraints for table `counter_order_product_details`
--
ALTER TABLE `counter_order_product_details`
  ADD CONSTRAINT `FKB9C139FE83D7F309` FOREIGN KEY (`product_id`) REFERENCES `order_used_product` (`product_id`),
  ADD CONSTRAINT `FKB9C139FE88997C9B` FOREIGN KEY (`counter_order_id`) REFERENCES `counter_order` (`counter_order_id`);

--
-- Constraints for table `daily_stock_details`
--
ALTER TABLE `daily_stock_details`
  ADD CONSTRAINT `FKE5FBAA731290DFC0` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `FKE5FBAA73C2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `damage_recovery_details`
--
ALTER TABLE `damage_recovery_details`
  ADD CONSTRAINT `FKC4FF66853F25294` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`),
  ADD CONSTRAINT `FKC4FF668AA70D9FF` FOREIGN KEY (`damage_recovery_day_wise_id`) REFERENCES `damage_recovery_month_wise` (`damage_recovery_id`);

--
-- Constraints for table `damage_recovery_month_wise`
--
ALTER TABLE `damage_recovery_month_wise`
  ADD CONSTRAINT `FKA9FB991D1290DFC0` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`);

--
-- Constraints for table `delivered_product`
--
ALTER TABLE `delivered_product`
  ADD CONSTRAINT `FK1A951034DAA6E837` FOREIGN KEY (`order_details_id`) REFERENCES `order_details` (`order_id`);

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `FK4722E6AE7F4FA47D` FOREIGN KEY (`department_id`) REFERENCES `department` (`department_id`),
  ADD CONSTRAINT `FK4722E6AEC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `employee_areas`
--
ALTER TABLE `employee_areas`
  ADD CONSTRAINT `FK5B0350B5C8668BF4` FOREIGN KEY (`area_id`) REFERENCES `area` (`area_id`),
  ADD CONSTRAINT `FK5B0350B5C9263C83` FOREIGN KEY (`employee_details_id`) REFERENCES `employee_details` (`employee_details_id`);

--
-- Constraints for table `employee_basic_salary_status`
--
ALTER TABLE `employee_basic_salary_status`
  ADD CONSTRAINT `FKD8245AA5C9263C83` FOREIGN KEY (`employee_details_id`) REFERENCES `employee_details` (`employee_details_id`);

--
-- Constraints for table `employee_chat_status`
--
ALTER TABLE `employee_chat_status`
  ADD CONSTRAINT `FKD26FF6A851E18F54` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`);

--
-- Constraints for table `employee_details`
--
ALTER TABLE `employee_details`
  ADD CONSTRAINT `FK30C8F1B151E18F54` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK30C8F1B1FD93C620` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`contact_id`);

--
-- Constraints for table `employee_feedback`
--
ALTER TABLE `employee_feedback`
  ADD CONSTRAINT `FK9EA5D9F654050B80` FOREIGN KEY (`employeeDetails_id`) REFERENCES `employee_details` (`employee_details_id`);

--
-- Constraints for table `employee_feedback_reply`
--
ALTER TABLE `employee_feedback_reply`
  ADD CONSTRAINT `FKF4EE7F61E96C02E2` FOREIGN KEY (`feedback_id`) REFERENCES `employee_feedback` (`feedBack_id`);

--
-- Constraints for table `employee_holiday`
--
ALTER TABLE `employee_holiday`
  ADD CONSTRAINT `FK150527C7C9263C83` FOREIGN KEY (`employee_details_id`) REFERENCES `employee_details` (`employee_details_id`);

--
-- Constraints for table `employee_incentives`
--
ALTER TABLE `employee_incentives`
  ADD CONSTRAINT `FK1A882699C9263C83` FOREIGN KEY (`employee_details_id`) REFERENCES `employee_details` (`employee_details_id`);

--
-- Constraints for table `employee_location`
--
ALTER TABLE `employee_location`
  ADD CONSTRAINT `FK1B5F8E26C9263C83` FOREIGN KEY (`employee_details_id`) REFERENCES `employee_details` (`employee_details_id`);

--
-- Constraints for table `employee_salary`
--
ALTER TABLE `employee_salary`
  ADD CONSTRAINT `FK2331A07B54050B80` FOREIGN KEY (`employeeDetails_id`) REFERENCES `employee_details` (`employee_details_id`);

--
-- Constraints for table `expense`
--
ALTER TABLE `expense`
  ADD CONSTRAINT `FKB1F4C85851E18F54` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FKB1F4C858C2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `FKB1F4C858EAF9FC71` FOREIGN KEY (`expense_type_id`) REFERENCES `expense_type` (`expense_type_id`);

--
-- Constraints for table `expense_type`
--
ALTER TABLE `expense_type`
  ADD CONSTRAINT `FK4B63F6C1C2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `FK8790195C51E18F54` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK8790195C53F25294` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`),
  ADD CONSTRAINT `FK8790195CC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `inventory_details`
--
ALTER TABLE `inventory_details`
  ADD CONSTRAINT `FKD867965F83D7F309` FOREIGN KEY (`product_id`) REFERENCES `order_used_product` (`product_id`),
  ADD CONSTRAINT `FKD867965FD59E6B20` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`inventory_transaction_id`);

--
-- Constraints for table `ledger`
--
ALTER TABLE `ledger`
  ADD CONSTRAINT `FKBE09AD69631ABB20` FOREIGN KEY (`expense_id`) REFERENCES `expense` (`expense_id`),
  ADD CONSTRAINT `FKBE09AD6980287007` FOREIGN KEY (`payment_counter_id`) REFERENCES `payment_counter` (`payment_counter_id`),
  ADD CONSTRAINT `FKBE09AD699CCE6031` FOREIGN KEY (`payment_order_id`) REFERENCES `payment` (`payment_id`),
  ADD CONSTRAINT `FKBE09AD69A06CE43D` FOREIGN KEY (`payment_supplier_id`) REFERENCES `payment_pay_supplier` (`payment_pay_supplier_id`),
  ADD CONSTRAINT `FKBE09AD69C2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `FKBE09AD69CBBAB651` FOREIGN KEY (`employee_salary_id`) REFERENCES `employee_salary` (`employee_salary_id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `FK521CF2516744C4D5` FOREIGN KEY (`employee_id_cancel`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK521CF25176D34635` FOREIGN KEY (`employee_id_sm`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK521CF25197146395` FOREIGN KEY (`business_name_id`) REFERENCES `business_name` (`business_name_id`),
  ADD CONSTRAINT `FK521CF2519FC8331D` FOREIGN KEY (`order_status_id`) REFERENCES `order_status` (`order_status_id`);

--
-- Constraints for table `order_product_details`
--
ALTER TABLE `order_product_details`
  ADD CONSTRAINT `FKF94880183D7F309` FOREIGN KEY (`product_id`) REFERENCES `order_used_product` (`product_id`),
  ADD CONSTRAINT `FKF948801DAA6E837` FOREIGN KEY (`order_details_id`) REFERENCES `order_details` (`order_id`);

--
-- Constraints for table `order_product_issue_details`
--
ALTER TABLE `order_product_issue_details`
  ADD CONSTRAINT `FK2BFE635B8DACD61A` FOREIGN KEY (`order_id`) REFERENCES `order_details` (`order_id`),
  ADD CONSTRAINT `FK2BFE635BF3C30640` FOREIGN KEY (`issued_by_gk`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK2BFE635BF4BC2136` FOREIGN KEY (`issued_to_db`) REFERENCES `employee` (`employee_id`);

--
-- Constraints for table `order_used_product`
--
ALTER TABLE `order_used_product`
  ADD CONSTRAINT `FKFB9403BE17956BC9` FOREIGN KEY (`brand_id`) REFERENCES `order_used_brand` (`brand_id`),
  ADD CONSTRAINT `FKFB9403BE86729CFC` FOREIGN KEY (`product_entity_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `FKFB9403BEB5B04409` FOREIGN KEY (`category_id`) REFERENCES `order_used_categories` (`category_id`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `FKD11C32068DACD61A` FOREIGN KEY (`order_id`) REFERENCES `order_details` (`order_id`);

--
-- Constraints for table `payment_counter`
--
ALTER TABLE `payment_counter`
  ADD CONSTRAINT `FK920BC8C388997C9B` FOREIGN KEY (`counter_order_id`) REFERENCES `counter_order` (`counter_order_id`);

--
-- Constraints for table `payment_pay_supplier`
--
ALTER TABLE `payment_pay_supplier`
  ADD CONSTRAINT `FKDCEEE91CD59E6B20` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`inventory_transaction_id`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FKED8DCCEF779307F2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`category_id`),
  ADD CONSTRAINT `FKED8DCCEFA6401A40` FOREIGN KEY (`brand_id`) REFERENCES `brand` (`brand_id`),
  ADD CONSTRAINT `FKED8DCCEFC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `FKC84826F423B67734` FOREIGN KEY (`city_id`) REFERENCES `city` (`city_id`),
  ADD CONSTRAINT `FKC84826F4C2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `reissue_order_details`
--
ALTER TABLE `reissue_order_details`
  ADD CONSTRAINT `FK92566458234D5616` FOREIGN KEY (`return_order_product_id`) REFERENCES `return_order_product` (`return_order_product_id`),
  ADD CONSTRAINT `FK92566458A2385FF3` FOREIGN KEY (`reissued_by_gk`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK92566458A3317AE9` FOREIGN KEY (`reissued_to_db`) REFERENCES `employee` (`employee_id`);

--
-- Constraints for table `reissue_order_product_details`
--
ALTER TABLE `reissue_order_product_details`
  ADD CONSTRAINT `FK8E1B03083ECCBA62` FOREIGN KEY (`reIssue_order_details_id`) REFERENCES `reissue_order_details` (`reissue_order_id`),
  ADD CONSTRAINT `FK8E1B030883D7F309` FOREIGN KEY (`product_id`) REFERENCES `order_used_product` (`product_id`);

--
-- Constraints for table `return_from_delivery_boy`
--
ALTER TABLE `return_from_delivery_boy`
  ADD CONSTRAINT `FKFAD802783D7F309` FOREIGN KEY (`product_id`) REFERENCES `order_used_product` (`product_id`),
  ADD CONSTRAINT `FKFAD8027C5105B31` FOREIGN KEY (`return_from_deliveryboy_main_id`) REFERENCES `return_from_delivery_boy_main` (`id`);

--
-- Constraints for table `return_from_delivery_boy_main`
--
ALTER TABLE `return_from_delivery_boy_main`
  ADD CONSTRAINT `FK68BD3ED16D315C25` FOREIGN KEY (`gatekeeper_id`) REFERENCES `employee` (`employee_id`),
  ADD CONSTRAINT `FK68BD3ED18DACD61A` FOREIGN KEY (`order_id`) REFERENCES `order_details` (`order_id`),
  ADD CONSTRAINT `FK68BD3ED1C9FA906A` FOREIGN KEY (`deliveryboy_id`) REFERENCES `employee` (`employee_id`);

--
-- Constraints for table `return_order_product`
--
ALTER TABLE `return_order_product`
  ADD CONSTRAINT `FKFD63082FDAA6E837` FOREIGN KEY (`order_details_id`) REFERENCES `order_details` (`order_id`);

--
-- Constraints for table `return_order_product_details`
--
ALTER TABLE `return_order_product_details`
  ADD CONSTRAINT `FK2FD40232234D5616` FOREIGN KEY (`return_order_product_id`) REFERENCES `return_order_product` (`return_order_product_id`),
  ADD CONSTRAINT `FK2FD4023283D7F309` FOREIGN KEY (`product_id`) REFERENCES `order_used_product` (`product_id`);

--
-- Constraints for table `state`
--
ALTER TABLE `state`
  ADD CONSTRAINT `FK68AC491B8F88A60` FOREIGN KEY (`country_id`) REFERENCES `country` (`country_id`);

--
-- Constraints for table `supplier`
--
ALTER TABLE `supplier`
  ADD CONSTRAINT `FK9CDBF9CCC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`),
  ADD CONSTRAINT `FK9CDBF9CCFD93C620` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`contact_id`);

--
-- Constraints for table `supplier_order`
--
ALTER TABLE `supplier_order`
  ADD CONSTRAINT `FKC225B1BC2A70D00` FOREIGN KEY (`company_id`) REFERENCES `company` (`company_id`);

--
-- Constraints for table `supplier_order_details`
--
ALTER TABLE `supplier_order_details`
  ADD CONSTRAINT `FKAA37A91E21D83D87` FOREIGN KEY (`supplier_order_id`) REFERENCES `supplier_order` (`supplier_order_id`),
  ADD CONSTRAINT `FKAA37A91E53F25294` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`),
  ADD CONSTRAINT `FKAA37A91E83D7F309` FOREIGN KEY (`product_id`) REFERENCES `order_used_product` (`product_id`);

--
-- Constraints for table `supplier_product_list`
--
ALTER TABLE `supplier_product_list`
  ADD CONSTRAINT `FKD3F215811290DFC0` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`),
  ADD CONSTRAINT `FKD3F2158153F25294` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
